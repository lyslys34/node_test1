/**
 *
 * 组织架构图的模版
 * author jipeng@meituan.com
 * 初始化数据跟树形下拉菜单一样
 * [{id,pid,name...}]
 * juicer模版引擎
 * */

define(['lib/juicer'],function(){

     var tplInit ={
         tpl :'<ul>',
         deep:7,//树形菜单深度，默认7层
         init : function(trees){
             for(var i = 0;i < trees.length; i++){
                tplInit.treeInit(trees[i],trees);
             }
            return tplInit.tpl+"</ul>";
         },
         appendTree:function(d, depth) {
             if (depth > this.deep || d == null || d == undefined) {
                 return '';
             }

             var sub = '';

             if (depth == 1) {
                 sub += '<li length="' + (d.children ? d.children.length : 0) + '">';
             } else if (depth == (this.deep - 1)) {
                 sub += '<li class="J-finalNode" length="' + (d.children ? d.children.length : 0) + '}">';
             } else {
                 sub += '<li class="bottomNode" length="' + (d.children ? d.children.length : 0) + '}">';
             }

             if (d['users'] && d['users'].length > 0) {
                 sub += '<a href="javascript:;" class="btn-node" tlevel="' + d.level + '"  nodeid="' + d.id + '">' + d.name + '<br>' + '(' + d['users'][0].name + ')</a>';
             } else {
                 sub += '<a href="javascript:;" class="btn-node" tlevel="' + d.level + '"  nodeid="' + d.id + '">' + d.name + '</a>';
             }

             if (d['children'] && d['children'].length > 0) {
                 if (depth == 1) {
                     //sub += '<div class="addtree-div"><button class="btn-addtree"><i class="fa fa-minus"></i></button></div>';
                     sub += '<ul style="display:block">';
                 } else {
                     sub += '<div class="addtree-div"><button class="btn-addtree"><i class="fa fa-plus"></i></button></div>';
                     sub += '<ul class="J-treetaggle" style="display:none">';
                 }

                 for (var i = 0; i < d['children'].length; i++) {
                     sub += this.appendTree(d['children'][i], depth + 1);
                 }
                 sub += '</ul>';
             }
             sub += '</li>';

             return sub;
         },
         initThree:function(trees,toggleClass){
             var html = '';
             html += '<ul>';
             html += this.appendTree(trees.data[0], 1);  //此处采用递归
             html += '</ul>';

             return html;
         },
         initEmployThree:function(trees,toggleClass){
             //TODO 此处需要重构，这样写层级太死了，需要根据this.deep灵活扩展
             var temp =' <ul>'+
                 '{@each data as node1,index}'+
                     '<li>'+
                         '<a href="javascript:;" class="btn-node" tlevel="${node1.level}"  nodeid="${node1.employNode.uid}">${node1.employNode.name},${node1.employNode.pos}</a>'+
                        '{@if node1.children.length > 0}'+
                         '<ul>'+
                         '{@each node1.children as node2}'+
                              '<li class="bottomNode">'+
                                  '<a href="javascript:;" class="btn-node" tlevel="${node2.level}" nodeid="${node2.employNode.uid}">${node2.employNode.name},${node2.employNode.pos}</a>'+
                                 '{@if node2.children.length > 0}'+
                                 '<button class="btn-addtree"><i class="fa fa-plus"></i></button>'+
                                 '<ul class="J-treetaggle" style="display:none">'+
                                     '{@each node2.children as node3}'+
                                     '<li class="J-finalNode" length="${node2.children.length}">'+
                                         '<a href="javascript:;"  tlevel="${node3.level}"  class="btn-node"nodeid="${node3.employNode.uid}">${node3.employNode.name},${node3.employNode.pos}</a>'+
                                         '{@if node3.children.length > 0}'+
                                         '<button class="btn-addtree"><i class="fa fa-plus"></i></button>'+
                                         '<ul class="J-treetaggle" style="display:none">'+
                                             '{@each node3.children as node4}'+
                                             '<li class="J-finalNode" length="${node3.children.length}">'+
                                                '<a href="javascript:;"  tlevel="${node4.level}"  class="btn-node"nodeid="${node4.employNode.uid}">${node4.employNode.name},${node4.employNode.pos}</a>'+
                                             '</li>'+
                                             '{@/each}'+
                                         '</ul>'+
                                         '{@/if}'+
                                     '</li>'+
                                     '{@/each}'+
                                 ' </ul>'+
                                 '{@/if}'+
                              '</li>'+
                         '{@/each}'+
                         '</ul>'+
                       '{@/if}'+
                     '</li>'+
                 '{@/each}'+
                 '</ul>';

             return juicer(temp,trees);
         },

         //递归完成N层次的树形结构(暂时没有用)
         treeInit:function(node,trees){
             if(!node) return;
             tplInit.deep = tplInit.deep -1;

             var flag = true;
             tplInit.tpl += '<li><a href="javascript:;">'+node.name+'</a><ul>';
             for(var i = 0;i < trees.length; i++){
                 if(trees[i].pId == node.id){
                     flag = false;
                     tplInit.tpl += '<li><a href="javascript:;">'+trees[i].name+'</a>';
                     tplInit.treeInit(trees[i+1],trees);//递归处
                     tplInit.tpl +='</li>';
                 }
             }
             tplInit.tpl += "</ul></li>";
             if(flag){
                 return '';
             }

         }
     }
    return tplInit;
});
