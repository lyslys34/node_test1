require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/root','module/popmsg','module/ajaxManager','module/validator','lib/jquery.tmpl'],function(t,popmsg,ajaxManager,validator){
	var TIPS = {
			PRICE_NOT_EXIST:'金额不能为空！',
			PRICE_IS_ZERO:'金额不能小于等于0！',
		    PRICE_NOT_REPEAT:'充值金额不能重复',
			NOT_NUMBER:'金额必须是数字！',
			INVALIAD:'保存不成功！输入格式有误。',
			BIGGER_THAN_NEXT:'保存不成功！ 终止点需要大于起始点。',
			PRICE_GT_TEN:'金额不能大于累计赠送金额上限',
			FIXED_TWO:'金额小数点后不能多于两位'
	}

	var AJAXURL = {
		GET:'/settlePlan/rechargeGift/query',
		SAVE:'/settlePlan/rechargeGift/saveOrUpdate'
	}

	initPage();

	$('#add').bind('click',function(){
		var str = $('#pro').html();
		$('#pro').html(str+$('#lineTmpl').html());
	})


	$('body').on('click','.del',function(){
		var $dom = $(this).parents('.div').parents('.row');
		$dom.remove();

	})

	$('body').on('click','.que',function(){
		$("#que-modal").modal();
	})

	function validateAmount($domList){

		for(var i=0;i<$domList.length;i++){
			var val = $domList.eq(i).val();
			if(validator.isBlank(val)){
				alert(TIPS.PRICE_NOT_EXIST);
				return false;
			}
			if(isNaN(val)){
				alert(TIPS.NOT_NUMBER);
				return false;
			}
			if(val<=0){
				alert(TIPS.PRICE_IS_ZERO);
				return false;
			}
			if(!validateFixedTwo(val)){
				alert(TIPS.FIXED_TWO);
				return false;
			}
		}

		return true;
	}

	function validateRecharge($domList){
		var ary = new Array();
		for(var i=0;i<$domList.length;i++){
			var val = $domList.eq(i).val();
			ary.push(val);
		}
		var nary=ary.sort();
		for(var i=0;i<nary.length;i++) {
			if (nary[i] == nary[i + 1]) {
				alert(TIPS.PRICE_NOT_REPEAT);
				return false;

			}
		}
		return true;
	}

	function validateGift($domList){
		for(var i=0;i<$domList.length;i++){
			var val = $domList.eq(i).val();
			var gift = $("#limit").val();
			if(parseFloat(val)>parseFloat(gift)){
				alert(TIPS.PRICE_GT_TEN);
				return false;
			}
		}
		return true;
	}



	$('#submit').bind('click',function() {
		var $dom = $('#proWrap');
		var limitGiftAmount = $('#limit').val();
		var props = {};
		props.id = $('#settingId').attr('data-id') || void 0;
		;
		props.limitGiftAmount = limitGiftAmount;
		props.rule = {};
		props.rule.rule = [];
		$dom.find('#pro').children('.row').each(function () {
			var $inputs = $(this).find('input');
			var rule = {};
			rule.chargeAmount = $.trim($inputs.eq(0).val());
			rule.giftAmount = $.trim($inputs.eq(1).val());
			props.rule.rule.push(rule);
		})
		props.rule = JSON.stringify(props.rule);
		ajaxManager.getServerData(AJAXURL.SAVE, props, function (data) {
			if (data.success) {
				//$("#save-modal").modal("close");

				initPage(true);
			} else {
				alert(data.message || '保存失败！');
			}
		})
		$("#cancel").click();

	})

	$('#save').bind('click',function(){
		var $dom = $('#proWrap');
		var $amountList = $dom.find('.amount');
		var $giftList = $dom.find('.gift');
		var $rechargeList = $dom.find('.recharge');
		if (validateAmount($amountList)&&validateGift($giftList)&&validateRecharge($rechargeList)) {
			$("#save-modal").modal();
		}
	})



	function validateFixedTwo(val){
		var pattern =/^[0-9]+([.]\d{1,2})?$/;
		if(!pattern.test(val)){
		    return false;
		}
		return true;
	}

	function isNumber(val){
		return /^[0-9]+$/.test(val);
	}

	function initPage(showLoading){
		ajaxManager.getServerData(AJAXURL.GET,{},function(data){
			if(data.success){
				var str = JSON.stringify(data).split('\\"').join('"').split('"{').join('{').split('}"').join('}');
				$('#proWrap').html($('#proTmpl').tmpl(JSON.parse(str)));
			}
		},showLoading?true:false);
	}
})