require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root', 'module/validator',], function (t, validator) {
    var text = $("#content").text();
    var texts = text.split("\n");
    if(texts.length>0){
        $("#content").empty();
        $.each(texts,function(i,t){

            if(t.trim() == ""){
                $("#content").append("<br >");
            }else{
                $("#content").append('<p id="p'+i+'"></p>');
                $("#p"+i).text(t);
            }
        });
    }

});