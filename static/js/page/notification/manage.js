require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root', 'module/validator','module/cookie','lib/datepicker'], function (t, validator,cookie,datepicker) {
    datepicker.set();
    $("#offlineTime").attr("disabled","disabled").addClass("readonly");
    //查询
    $("#search").click(function(){
        $("#fm").submit();
    });

    //新建
    $("#create").click(function(){
        _showEditPanel(true);
    });

    //查看详情
    $("[name=detail]").click(function(){
        var id = $(this).parent().attr("id");
       $("#area :checkbox").prop("checked",false);
        getNotice(id,false);//获取通知信息
    });
    //编辑
    $("[name=edit]").click(function(){
        var id = $(this).parent().attr("id");
        $("#area :checkbox").prop("checked",false);
        getNotice(id,true);
    });
    //发布
    $("[name=publish]").click(function(){
        var id = $(this).parent().attr("id");
        changeStatus(id,2);
    });
    //下线
    $("[name=offline]").click(function(){
        var id = $(this).parent().attr("id");
        changeStatus(id,3);
    });

    //删除
    $("[name=delete]").click(function(){
        if (!confirm('确定要删除该条通知吗?')) return;
        var id = $(this).parent().attr("id");
        var postData={id:id};
        var url="/notification/delete.ajax";
        post(url,postData);
    });

    //定时下线
    $("#setOfflineTime").click(function(){
        if($("#setOfflineTime")[0].checked){
           $("#offlineTime").attr("disabled",false).removeClass("readonly");
        }else{
            $("#offlineTime").attr("disabled","disabled").addClass("readonly");
        }
    });
    function _showEditPanel(edit) {
         var body = $("#create-container");
         body.removeClass("hide");
         var buttons = [];
         if(edit){
            buttons =  [
                   t.ui.createDialogButton('ok','保存', function () {
                       $("#status").val("1");
                       save();
                       return false;
                   }),

                   t.ui.createDialogButton('ok','保存并发布', function () {        // 确定
                        $("#status").val("2");
                        save();
                        return false;
                   }),

                   t.ui.createDialogButton('close','取消', function(){
                    closePanel();
                    return true;
                   })
               ];
         }else{
            disablePanel();
            buttons =  [
               t.ui.createDialogButton('close','关闭', function(){
                closePanel();
               })
               ];
         }


         var dlg = t.ui.showModalDialog({
             body: body, buttons: buttons,
             style:{".modal-content":{"width":"530px"}}
         });
     }


    function closePanel(){
        $("#create-container :text").val("").attr("readonly",false);
        $("#create-container").find("textarea").val("").attr("readonly",false);
        $("#create-container :checkbox").prop("checked",false).attr("disabled",false);
        $("#area :checkbox").prop("checked",true);
        $("#offlineTime").attr("disabled",false).removeClass("readonly");
        $("#create-container :hidden").val("0");
    }
    function disablePanel(){
        $("#create-container :text").attr("readonly","readonly");
        $("#create-container").find("textarea").attr("readonly","readonly");
        $("#create-container :checkbox").attr("disabled","disabled");
        $("#offlineTime").attr("disabled","disabled").addClass("readonly");
    }
    //将通知信息放到表单中
    function setNotice(notice,edit){
        if(notice != null){
            $.each(notice,function(name,value){
                var $this = $("#create-container").find("#"+name);
                if($this.attr("type") == "checkbox"){
                    if(value){
                        $this.prop("checked","checked");
                    }
                }else{
                    $this.val(value);
                }
            });
            if(notice.offlineTime != null && notice.offlineTime != ""){
                $("#setOfflineTime").prop("checked","checked");
                $("#offlineTime").attr("disabled",false).removeClass("readonly");
            }
        }
        _showEditPanel(edit);
    }
    //获取通知信息
    function getNotice(id,edit){
        var url = "/notification/detail.ajax";
        var postData = {"id":id};
        t.utils.post(url, postData, function (r) {
          if (r.httpSuccess) {
              if(r.data.code == 0) {
                  setNotice(r.data.data,edit);
                  return false;
              } else {
                  showMsg("提交失败！" + r.data.msg);
                  return false;
              }

          } else {
              showMsg("提交失败,系统错误!");
              return false;
          }
        });
    }

    //提交表单
    function save(){
        var url = $("#noticeForm").attr("action");
        var form = $("#noticeForm").serializeArray();
        var postData = {};
        var valid = true;
        $.each(form,function(i,field){
            var $this = $("[name="+field.name+"]");
            if($this.attr("required") && field.value == ""){
                showMsg($this.attr("placeholder"));
                valid = false;
                return false;
            }
            postData[field.name] = field.value;
        });
        if(!valid){
            return false;
        }
        if($("#setOfflineTime")[0].checked && $("#offlineTime").val() == ""){
            showMsg("请选择定时下线时间！");
            return false;
        }
        var areaChoose = false;
        $("#area :checkbox").each(function(){
            if($(this)[0].checked){
                areaChoose = true;
            }
        });

        if(!areaChoose){
            showMsg("请选择配送范围！");
            return false;
        }

        t.utils.post(url, postData, function (r) {
          if (r.httpSuccess) {
              if(r.data.code == 0) {
                  location.reload(true);
                  return false;
              } else {
                  showMsg("提交失败！" + r.data.msg);
                  return false;
              }

          } else {
              showMsg("提交失败,系统错误!");
              return false;
          }
        });
    }
    function showMsg(msg,reload){
        var body = $('<div style="height: 100px;padding: 15px;text-align: center;">'+msg+'</div>');
        t.ui.showModalDialog({
            body: body, buttons: [
                t.ui.createDialogButton('close','关闭', function () {     // 取消
                    if(reload){
                        location.reload(true);
                    }
                })
            ],
            style:{".modal-dialog":{"padding":"100px 110px"},
                   ".modal-footer":{"padding":"10px"},
                   ".btn-default":{"font-size":"12px"}
            }
        });
    }

    function changeStatus(id,status){
        var postData = {id:id,status:status};
        post("/notification/changeStatus.ajax",postData);
    }

    function post(url,postData){
        t.utils.post(url, postData, function (r) {
          if (r.httpSuccess) {
              if(r.data.code == 0) {
                  location.reload(true);
                  return false;
              } else {
                  showMsg("提交失败！" + r.data.msg);
                  return false;
              }

          } else {
              showMsg("提交失败,系统错误!");
              return false;
          }
        });
    }
});