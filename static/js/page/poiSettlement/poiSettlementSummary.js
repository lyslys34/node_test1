require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['page/finance/common'], function (common) {

    //点击页码
    $(document).delegate('a[name=pageCode]', 'click', function () {
        var page = $.trim($(this).text());
        window.location.href = "/partner/poiSettlement?currentPage=" + page + "&" + getQueryStr();
    });

    //点击上一页
    $(document).delegate('#prev', 'click', function () {
        if ($(this).parent().hasClass('disabled')) {
            return false;
        }
        var page = parseInt($.trim($(this).attr('data-page'))) - 1;
        window.location.href = "/partner/poiSettlement?currentPage=" + page + "&" + getQueryStr();
    });

    //点击下一页
    $(document).delegate('#next', 'click', function () {
        if ($(this).parent().hasClass('disabled')) {
            return false;
        }
        var page = parseInt($.trim($(this).attr('data-page'))) + 1;
        window.location.href = "/partner/poiSettlement?currentPage=" + page + "&" + getQueryStr();
    });

    //点击查看详情
    $(document).delegate('a[name=poiDetail]', 'click', function () {
        var poiId = $.trim($(this).attr('data-poiId'));
        window.open("/partner/poiSettlement?poiId=" + poiId + "&startDate=" + $('#query-start-date').val() + "&endDate=" + $('#query-end-date').val());// + "&type=" + $('#poiType option:selected').val());
    });

    $(document).delegate('#query-commit', 'click', function () {
        var startDate = common.str2Date($('#query-start-date').val());
        var endDate = common.str2Date($('#query-end-date').val());
        if (startDate > endDate) {
            alert("开始时间必须大于结束时间");
            return false;
        }
        if ((endDate - startDate) / 1000 / 24 / 60 / 60 > 59) {
            alert("时间间隔不能超过60天");
            return false;
        }
        window.location.href = "/partner/poiSettlement?" + getQueryStr();
    });

    function getQueryStr() {
        var poiId = $('#poiId option:selected').val();
        var startDate = $('#query-start-date').val();
        var endDate = $('#query-end-date').val();
        var type = $('#poiType option:selected').val();
        var str = "poiId=" + poiId + "&startDate=" + startDate + "&endDate=" + endDate;// + "&type=" + type;;
        return str;
    }

    //导出表格
    $(document).delegate('a[name=excelReport]', 'click', function () {
        window.location.href = "/partner/poiSettlement/excelReport?" + getQueryStr();
    });

});
