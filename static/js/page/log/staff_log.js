/**
 * Created by lixiangyang on 15/7/16.
 */


require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        },
    }
});
require(['module/root', 'module/validator',  'page/common'], function (t, validator, datapicker){
    $("#formValidate1").submit(function(){
        return validate();
    });

    mySelect2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
        opTypeSelect.select2({
            matcher: oldMatcher(matchPinyin)
        });
        opUserSelect.select2({
            matcher: oldMatcher(matchPinyin)
        });
    });

    function validate() {
        var startTimeStr = $(".js_date_start").val();
        var endTimeStr = $(".js_date_end").val();
        var startTime = new Date(Date.parse(startTimeStr.replace(/-/g,   "/")));
        var endTime = new Date(Date.parse(endTimeStr.replace(/-/g,   "/")));

        if(startTime > endTime) {
            _showPop({
                type:2,
                content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + "开始时间不能晚于结束时间" + "</div>"
            });
            return false;
        }

        return true;
    }
});