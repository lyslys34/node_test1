/**
 * Created by yangyaozong on 15/08/31.
 */


require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/root', 'module/validator',  'page/common'], function (t, validator, datapicker){
	$(document).delegate(".js_submit_btn", "click", function() {
        submit();
    });

    function submit(){
        if(validate()) {
            $("#formValidate1").submit();
        }
    }

    function validate() {
        var startTimeStr = $(".js_date_start").val();
        var endTimeStr = $(".js_date_end").val();
        var startTime = new Date(Date.parse(startTimeStr.replace(/-/g,   "/")));
        var endTime = new Date(Date.parse(endTimeStr.replace(/-/g,   "/")));
        
        if(startTime > endTime) {
            alert("开始时间不能晚于结束时间");
            return false;
        }

        return true;
    }
});