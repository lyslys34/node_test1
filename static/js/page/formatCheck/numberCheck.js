var isMoneyFormat = function(number) {
    if (number != null) {
        if (/^\d*(.[0-9]\d?)?$/.test(number)) {
            return true;
        }
        return false;
    }
    return false;
}