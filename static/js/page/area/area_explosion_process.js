require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu',
        ],
        'lib/autocomplete' : {
            deps : ['page/common']
        },
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});
require(['module/root', 'module/validator', 'lib/autocomplete'], function (t, validator, autocomplete) {
    var cityNameMap = {};
    var cityIdMap = {};
    var id;
    var oldOrgId;
    var oldOrgName;
    var areaNameMap = {};
    var areaIdMap = {};
    var levelArr = ["关闭爆单", "A", "B", "C", "D", "E", "F"];
    document.title = '配送区域列表';
    
    // 模糊匹配 自动补全 城市的列表
    function initSearchCity() {
        myPOST('/org/citySearchList', {}, function (r) {
             if (r.httpSuccess) {
                if(r.data && r.data.code == 0) {    
                    $(".js_city_name").autocomplete({
                        delay:100,
                        source : r.data.data,
                        focus: function(event, ui ) {
                            $( ".js_city_name" ).val( ui.item.value );
                            $( ".js_city_id" ).val( ui.item.id );
                            return false;
                        },
                        select: function(event, ui) {
                            $(".js_city_name").val(ui.item.value );
                            $(".js_city_id").val(ui.item.id );
                        },
                        change: function(event, ui) {
                            var cityName = $(".js_city_name").val();
                            if(validator.isBlank(cityName)) {
                                $(".js_city_id").val(0);
                            }
                        },
                        open: function(event, ui) {
                            $(".js_city_id").val('0');
                        }
    
                    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                        return $( "<li>" )
                            .append( "<a>" + item.value + "</a>" )
                            .appendTo( ul );
                    };
                    if(r.data.data) {
                        var cityId = $(".js_city_id").val();
                        $.each(r.data.data, function(index, item) {
                            cityNameMap[item.value] = item;
                            cityIdMap[item.id] = item;
                            if(cityId) {
                              if(cityId == item.id) {
                                $(".js_city_name").val(item.value);
                              }
                            }
                        });
                        
                    }
    
                    var riderId = $(".js_city_id").val();
                    $(".js_city_name").val(cityIdMap[riderId] != null && typeof cityIdMap[riderId] != 'undefined' ? cityIdMap[riderId].value : "");
                } else {
                   console.log("获取城市信息失败，" + r.data.msg);
                }
            } else {
                console.log('获取城市信息失败，系统错误');
            }
        });
    }

    // 模糊匹配 自动补全 区域的列表
    function initSearchArea() {

        myPOST('/org/areaSearchList', {}, function (r) {
            if (r.httpSuccess) {
                if(r.data && r.data.code == 0) { 
                    $(".js_area_name").autocomplete({
                        delay:100,
                        source : r.data.data,
                        focus: function( event, ui ) {
                            $( ".js_area_name" ).val( ui.item.value );
                            $( ".js_area_id" ).val( ui.item.id );
                            return false;
                        },
                        select: function(event, ui) {

                            $(".js_area_name").val(ui.item.value );
                            $(".js_area_id").val(ui.item.id );
                        },
                        change: function(event, ui) {
                            var areaName = $(".js_area_name").val();
                            if(validator.isBlank(areaName)) {
                                $(".js_area_id").val(0);
                            }
                        },
                        open: function(event, ui) {
                            $(".js_area_id").val('0');
                        }

                    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                        return $( "<li>" )
                            .append( "<a>" + item.value + "</a>" )
                            .appendTo( ul );
                    };
                    if(r.data.data) {
                        $.each(r.data.data, function(index, item) {
                        	areaNameMap[item.value] = item;
                        	areaIdMap[item.id] = item;
                        });
                        
                    }

                    var riderId = $(".js_area_id").val();

                } else {
                    console.log("获取区域信息失败，" + r.data.msg);
                }
            } else {
                console.log('获取区域信息失败，系统错误');
            }
        });
    }
    
    function myPOST(url, data, callback){  // 发送POST请求
        if(typeof(data) == 'function') { callback = data; data = null; }
        $.ajax({ 
            type: 'POST', 
            url: url, 
            data: data,
            traditional: true,
            success: function(r) {
                callback({ httpSuccess: true, data: r });
            },
            error: function(XmlHttpRequest, textStatus, errorThrown) {
                callback({ httpSuccess: false, statusCode:XmlHttpRequest.status });
            } 
        });
    }

    function isBlank(value) {
        if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
                return this.replace(/^\s+|\s+$/g, '');
            }  
        }
        return !_valueExist(value) || value.trim() == "";
    }
    // 输入有误 提示
    function showError(errMsg) {
        $("#alert_error").empty();
        $("#alert_error").show();
        $("#alert_error").append("<div class='alert alert-danger' role='alert' data-dismiss='alert'>" + errMsg + "</div>");
        $("#alert_error").fadeOut(2000);
    }
    // 修改失败 提示
    function showBindError(errMsg) {
        $("#alert_bind_error").empty();
        $("#alert_bind_error").append("<div class='alert alert-danger' role='alert' data-dismiss='alert'>" + errMsg + "</div>");
    }
    // 区域配置的修改 传进来一个数组
    function bindOrg(sendId, strategyNum) {
        $("#alert_error").empty();
        var sendData = new Object();
        sendData = {
            areaIds: sendId,
            strategyNum: strategyNum,
            operatorMisId: "",
            operatorName: ""
        };
        myPOST('/deliveryExplosion/modifyAreaConfig', sendData, function (r) {
            if (r.httpSuccess) {
                if(r.data && r.data.code == 0) {
                    $("#resMsg").html("<h4>区域配置修改成功："+ r.data.msg +",即将刷新页面</h4>");//绑定爆单处理级别成功
                    $("#bindOrgModal").modal('hide');
                    $("#showBindRes").modal();
                    setTimeout(function(){
                        window.location.reload();
                    }, 2000);
                } else {
                    $("#alert_error").show();
                    $("#alert_error").append("<div class='alert alert-danger' role='alert' data-dismiss='alert'>区域配置修改失败：" + r.data.msg + "</div>");
                }
            } else {
                $("#alert_error").show();
                $("#alert_error").append("<div class='alert alert-danger' role='alert' data-dismiss='alert'>网络连接失败</div>");
            }
        });
    }

    // 搜索
    $("#search").click(function() {
        fnSearchMsg(1);
    });
    $("#type").change(function() {
        fnSearchMsg(1);
    });
    /**
     * @param cityName
     * @param areaName
     * @param strategyNum
     * @param pageNum 当前页 没有默认传0
     * @param pageSize == 直接是50条
     */
    function fnSearchMsg(pageNum) {
        var sendData = new Object();
        var pageSize = 50;
        var cityName = $("#cityName").val();
        var areaName = $("#areaName").val();
        var strategyNum = $("#type").val();
            
        sendData = {
            cityName: cityName,
            areaName: areaName,
            strategyNum: strategyNum,
            pageSize: pageSize,
            pageNum: pageNum
        }
        myPOST('/deliveryExplosion/searchAreaConfig', sendData, function (r) {
             if (r.httpSuccess) {
                if(r.data && r.data.code == 0) {    
                    var res = r.data,
                        data = res.data;
                    var areaTable = $("#areaSetting table"),
                        areaHtml = "";
                    areaTable.find('tbody').empty();
                    var explosionConfigs = data.explosionConfigs;
                    for (var i = 0; i < explosionConfigs.length; i++) {
                        var dataTemp = explosionConfigs[i];
                        areaHtml += "<tr>";
                        areaHtml += '<td><input type="checkbox" id="'+ dataTemp.areaId +'" /></td>'
                                  + '<td>'+ dataTemp.cityName +'</td>'
                                  + '<td class="area-name">'+ dataTemp.areaName +'</td>'
                                  + '<td>'+ dataTemp.areaId +'</td>'
                                  + '<td class="area-level">'+ levelArr[dataTemp.strategyNum] +'</td>'
                                  + '<td>'+ fnFormmatTime(dataTemp.utime) +'</td>'
                                  + '<td>'+ dataTemp.operatorName +'</td>'
                                  + '<td><a data-toggle="tooltip" title="修改配送级别" data-placement="top" class="bind_link modify" rel="'+ dataTemp.areaId +'" >'
                                  +     '<i class="fa fa-edit fa-lg opration-icon"></i>'
                                  +  '</a></td>';
                        areaHtml += "</tr>";
                    };
                    areaTable.find('tbody').append(areaHtml);
                    pagerConfig(pageNum, data.totalPages );
                } else {
                   console.log("数据查询失败" + r.data.msg);
                }
            } else {
                console.log('网络连接失败');
            }
        });
    }
    // 分页 
    // @locPageNum 当前页数
    // @allPageCount 总页数
    function pagerConfig(locPageNum, allPageCount) {
        var pageHtml = "";
        if ( allPageCount <= 0) {
            $("#areaPage").empty();
            return;
        };
        pageHtml += '<div class="panel-footer clearfix">'
                 +    '<ul class="pagination pagination-xs m-top-none pull-right">';
        if (locPageNum == 1){
            pageHtml += '<li class="disabled"><a href="javascript:;">上一页</a></li>';
        }else{
            pageHtml += '<li page="prev"><a href="javascript:;">上一页</a></li>';
        }
        
        var star = 1, end = locPageNum + 1;
        if ( locPageNum > 4 ) {
            star = locPageNum - 1;
            pageHtml += '<li page="1"><a href="javascript:;">1</a></li>'
                 + '<li page="2"><a href="javascript:;">2</a></li>';
            pageHtml += '<li class="disabled"><a href="javascript:;">...</a></li>';
        }
        if ( end > allPageCount ) { end = allPageCount };
        for (var i = star; i <= end; i++) {
            if ( locPageNum == i ){
                pageHtml += '<li class="active" ><a href="javascript:;">'+i+'</a></li>';
            }else{
                pageHtml += '<li page='+ i +'><a href="javascript:;">'+i+'</a></li>';
            }
        };

        if ( end < allPageCount-2 ) {
            pageHtml += '<li class="disabled"><a href="javascript:;">...</a></li>';
        }
        if ( end < allPageCount-1 ) {
            pageHtml += '<li page='+ (allPageCount-1) +'><a href="javascript:;">'+ (allPageCount-1) +'</a></li>';
        };
        if ( end < allPageCount ) {
            pageHtml += '<li page='+ allPageCount +'><a href="javascript:;">'+allPageCount+'</a></li>';
        };
        if ( locPageNum == allPageCount ) {
            pageHtml += '<li class="disabled"><a href="javascript:;">下一页</a></li>';
        }else{
            pageHtml += '<li page="next"><a href="javascript:;">下一页</a></li>';
        }
        pageHtml += "</ul></div>";
        $("#areaPage").empty();
        $("#areaPage").append(pageHtml);
    }
    // 分页点击
    $("#areaPage").on('click', 'li', function() {
        var _this = $(this), page = _this.attr("page");
        var locPage = Math.floor( _this.siblings('.active').text() );
        if ( page ) {
            if ( page == "next" ) {
                fnSearchMsg(locPage+1);
            }else if( page == "prev" ){
                fnSearchMsg(locPage-1);
            }else{
                fnSearchMsg(page);
            }
        };
    });
    // 最近修改时间的处理 传进来的应该是一个时间戳
    function fnFormmatTime(utime){
        var time = new Date(utime*1000),
            res = "";
        var month = (time.getMonth()+1) >= 10 ? (time.getMonth()+1) : "0"+(time.getMonth()+1);
        var day = time.getMonth() >= 10 ? time.getMonth() : "0"+time.getMonth();
        var hours = (time.getHours()+1) >= 10 ? (time.getHours()+1) : "0"+(time.getHours()+1);
        var mis = (time.getMinutes()+1) >= 10 ? (time.getMinutes()+1) : "0"+(time.getMinutes()+1);
        var sec = (time.getSeconds()+1) >= 10 ? (time.getSeconds()+1) : "0"+(time.getSeconds()+1);
        res += time.getFullYear() +"-";
        res += month +"-";
        res += day+" ";
        res += hours + ":"+mis+":"+sec;
        return res;
    }
    // 阈值查询
    function fnSearchLevel() {
        myPOST('/deliveryExplosion/searchAreaThreshold', {}, function (r) {
             if (r.httpSuccess) {
                if(r.data && r.data.code == 0) {    
                    var res = r.data,
                        data = res.data;
                    var thresholdTable = $("#thresholdSetting table"),
                        thresholdHtml = "";
                    for (var i = 0; i < data.length; i++) {
                        var dataTemp = data[i];
                        thresholdHtml += "<tr>";
                        thresholdHtml += "<td>策略"+ levelArr[dataTemp.level] +"</td>"
                                      +  "<td><input type='text' disabled value="+ dataTemp.overloadOneParam +" /></td>"
                                      +  "<td><input type='text' disabled value="+ dataTemp.overloadTwoParam +" /></td>"
                                      +  "<td><input type='text' disabled value="+ dataTemp.overloadThreeParam +" /></td>"
                                      +  "<td><input type='text' disabled value="+ dataTemp.overloadFourParam +" /></td>"
                                      +  "<td><input type='text' disabled value="+ dataTemp.overloadFiveParam +" /></td>"
                                      +  "<td><input type='text' disabled value="+ dataTemp.overloadSixParam +" /></td>"
                                      +  "<td>"
                                      +     "<a data-toggle='tooltip' title='修改配送级别' data-placement='top' class='modify' data-save='change'>修改</a>"
                                      +     "<a data-toggle='tooltip' title='取消修改' data-placement='top' class='modify hidden' data-save='cancel'>取消</a>"
                                      +"</td>";
                        thresholdHtml += "</tr>";
                    };
                    thresholdTable.find("tbody").empty();
                    thresholdTable.find("tbody").append( thresholdHtml );
                } else {
                   console.log("数据查询失败" + r.data.msg);
                }
            } else {
                console.log('网络连接失败');
            }
        });
    }
    // 阈值修改 按钮事件
    $("#thresholdTable tbody").on('click', '.modify', function(event) {
        event.preventDefault();
        var _this = $(this);
        var tdInput = _this.parent().siblings('td').find('input');
        if ( _this.attr("data-save") == "change" ) {// 修改的
            _this.siblings("a").removeClass('hidden');
            _this.attr("data-save", "save");
            _this.text("保存");

            $("#thresholdTable tbody").find('input').attr("disabled", "disabled");
            $("#thresholdTable tbody").find('input').removeClass('focus');
            tdInput.removeAttr('disabled');
            tdInput.addClass('focus');
        }else if( _this.attr("data-save") == "cancel" ){// 取消
            _this.addClass('hidden');
            _this.attr("data-save", "cancel");
            _this.siblings('a').attr("data-save", "change");
            _this.siblings('a').text("修改");

            tdInput.attr('disabled', "disabled");
            tdInput.removeClass('focus');
        }else{// 保存按钮之后的操作
            _this.attr("data-save", "change");
            _this.text("修改");
            _this.siblings('a').addClass('hidden');
            var sendData = new Object();
            sendData = {
                strategyNum: (_this.parents("tr").index() + 1),
                newOverloadOneParam: _this.parents("tr").find('input:eq(0)').val(),
                newOverloadTwoParam: _this.parents("tr").find('input:eq(1)').val(),
                newOverloadThreeParam: _this.parents("tr").find('input:eq(2)').val(),
                newOverloadFourParam: _this.parents("tr").find('input:eq(3)').val(),
                newOverloadFiveParam: _this.parents("tr").find('input:eq(4)').val(),
                newOverloadSixParam: _this.parents("tr").find('input:eq(5)').val(),
            };
            fnCheckLevel( sendData );
            // fnSaveLevel(sendData);

            tdInput.attr('disabled', "disabled");
            tdInput.removeClass('focus');
        }
    });
    // 阈值 保存之前的检查
    function fnCheckLevel( sendData ){
        var send = {
            strategyNum: sendData.strategyNum
        };
        myPOST('/deliveryExplosion/explosionStrategyInUse', send, function (r) {
             if (r.httpSuccess) {
                if(r.data && r.data.code == 0) {
                    if ( r.data.data ){
                        var sbody = "<h4 class='process red-div'>当前策略有区域正在使用，是否保存修改？</h4>";
                        detailDia = t.ui.showModalDialog({
                            title: '', body: sbody, buttons: [
                                t.ui.createDialogButton('sure','保存', function () {//确定
                                    fnSaveLevel(sendData);
                                    detailDia.modal('hide');
                                }),
                                t.ui.createDialogButton('close','取消',function(){//取消
                                })
                            ],
                            style:{
                                ".modal-body":{"padding-top":"8px"},
                                ".modal-footer":{"border-top":"none", "padding-top":"2px"},
                                ".modal-dialog":{"top":"150px", "width":"420px"},
                                ".btn-default":{"width":"160px","float":"left","margin":"0"},
                                ".btn:nth-child(odd)":{"margin":"0 25px 0 20px"}
                            }
                        });
                    }else{
                        fnSaveLevel(sendData);
                    }
                } else {
                    $("#thresholdAlert").show();
                    $("#thresholdAlert").append("<div class='alert alert-danger' role='alert' data-dismiss='alert'>检查请求失败：" + r.data.msg + "</div>");
                }
            } else {
                $("#thresholdAlert").show();
                $("#thresholdAlert").append("<div class='alert alert-danger' role='alert' data-dismiss='alert'>网络连接失败</div>");
            }
        });
    };
    // 阈值 保存爆单等级的函数
    function fnSaveLevel(send) {
        $("#thresholdAlert").empty();
        myPOST('/deliveryExplosion/modifyAreaThreshold', send, function (r) {
             if (r.httpSuccess) {
                if(r.data && r.data.code == 0) {
                    $("#resMsg").html("<h4>阈值修改成功："+ r.data.msg +"</h4>");//绑定爆单处理级别成功
                    $("#bindOrgModal").modal('hide');
                    $("#showBindRes").modal();

                    setTimeout(function(){
                        $("#showBindRes").modal('hide');
                        fnSearchLevel();
                    }, 1000);
                } else {
                    $("#thresholdAlert").show();
                    $("#thresholdAlert").append("<div class='alert alert-danger' role='alert' data-dismiss='alert'>阈值修改失败：" + r.data.msg + "</div>");
                }
            } else {
                $("#thresholdAlert").show();
                $("#thresholdAlert").append("<div class='alert alert-danger' role='alert' data-dismiss='alert'>网络连接失败</div>");
            }
        });
    }

    // 修改 按钮
    $("#responsiveTable tbody").on('click', '.modify', function(event) {
        event.stopPropagation();
        $("#alert_bind_error").empty();
        var name = $(this).parent().siblings(".area-name").text();
        var level = $(this).parent().siblings(".area-level").text();
        var sendId = new Array();
        sendId.length = 0;
        sendId.push( $(this).attr('rel') );
        var nameArr = new Array(), levelArr = new Array();
        levelArr.push(level.trim());
        nameArr.push(name.trim());

        var _idArr = new Array();
        var tbody = $("#responsiveTable tbody"),
            selected = tbody.find("input");
        for (var i = 0; i < selected.length; i++) {
            if ( tbody.find("input:eq("+i+")").is(":checked") ) {
                var locSelect = tbody.find("input:eq("+i+")");
                _idArr.push( locSelect.attr("id") );
            };
        };
        if ( _idArr.length > 1 ) {
            alert("你已选中多个，已帮你清空选中的多项。");
            selected.prop("checked", false);
        };
        showDlg(nameArr, levelArr, sendId);
    });
    // 点击tr可以反选
    $("#responsiveTable tbody").on('click', 'tr', function() {
        var _this = $(this),
            _thiCheck = _this.find('input[type="checkbox"]'),
            CheckFlag = _thiCheck.checked;
        _this.find('input[type="checkbox"]').prop("checked", !_thiCheck.is(":checked"));
    });
    $("#responsiveTable thead").on('click', 'input', function() {
        var _this = $(this), _thisVal = _this.val();
        fnCancalOrCheckAll(_thisVal);//是否选中所有 取消选中所有
        if(_thisVal == 1 ){
            _this.val("0");
        }else{
            _this.val("1");
        }
    });
    // 传入一个是否选中的参数 1为选中所有，0为取消所有
    function fnCancalOrCheckAll(arg){
        var tbodyCheckBox = $("#responsiveTable tbody").find('input[type="checkbox"]');
        if ( arg == 1 ) {
            tbodyCheckBox.prop("checked", true);
        }else{
            tbodyCheckBox.prop("checked", false);
        }
    }
    $("#batchModify").click(function() {
        var tbody = $("#responsiveTable tbody"),
            selected = tbody.find("input");
        var _idArr = new Array(), nameArr = new Array(), levelArr = new Array();
        for (var i = 0; i < selected.length; i++) {
            if ( tbody.find("input:eq("+i+")").is(":checked") ) {
                var locSelect = tbody.find("input:eq("+i+")");
                nameArr.push( locSelect.parents("tr").children(".area-name").text().trim() );
                _idArr.push( locSelect.attr("id") );
            };
        };
        showDlg(nameArr, levelArr, _idArr);
    });
    $("#pressIdModify").click(function(event) {
        showDlg("id");
    });
    // 点击修改之后出现的弹窗 单独修改出现的弹窗
    function showDlg(nameArr, levelArr, _idArr){
        var detailDia;
        if (detailDia) {
            detailDia.remove();
            detailDia = null;
        };

        var body = '<div class="process">';
        if ( typeof nameArr == "string" ) {
            body = body + '<div class="process-label"><label class="pull-left" for="forIdModify">区域&nbsp;ID:&nbsp;&nbsp;&nbsp;</label>';
            body += '<textarea id="forIdModify" cols="30" rows="5" placeholder="区域id用逗号隔开：123,6,3935"></textarea></div>';
        }else{
            if ( _idArr.length == 0 ) {
                showError("你没有选中任何，请选中！");
                return;
            }else if ( _idArr.length == 1 ) {
                body = body + '<div class="process-label">当前级别:&nbsp;'+ levelArr[0] + '</div>';
                body = body + '<div class="process-label">区域名称:&nbsp;'+ nameArr[0] + '</div>';
            }else{
                if ( _idArr.length > 2 ) {
                    body = body + '<div class="process-label">区域名称:&nbsp;'+ nameArr[0]+"、"+nameArr[1] + '等'+_idArr.length+'个区域</div>';
                }else{
                    body = body + '<div class="process-label">区域名称:&nbsp;'+ nameArr[0]+"、"+nameArr[1] +'</div>';
                }
            } 
        }
        body = body + '<div class="process-label"><span>处理级别:&nbsp;</span>';
        body = body + '<select id="selectClass" class="input-sm">';
        body = body + '<option value="0">关闭爆单</option>';
        body = body + '<option value="1" selected="selected">A</option>';
        body = body + '<option value="2">B</option>';
        body = body + '<option value="3">C</option>';
        body = body + '<option value="4">D</option>';
        body = body + '<option value="5">E</option>';
        body = body + '<option value="6">F</option>'+'</select></div>';
        body = body + '</div>';

        var sbody = $(body);

        detailDia = t.ui.showModalDialog({
            title: '爆单处理级别修改', body: sbody, buttons: [
                t.ui.createDialogButton('sure','保存', function () {   //保存
                    var strategyNum = $("#selectClass").val();
                    if ( typeof nameArr == "string" ) {
                        var idStr = $("#forIdModify").val().trim();
                        idStr = idStr.replace(/[\r\n]/g, "");//清空换行和空格的
                        var tempArr = new Array();
                        idStr = idStr.replace(/，/g, ",");//转换中文，为英文,
                        var numberExp = new RegExp(/^[,0-9,]+$/g);
                        if ( numberExp.test( idStr ) ) {
                            tempArr = idStr.split(",");
                            bindOrg(tempArr, strategyNum);
                            detailDia.modal('hide');
                        }else{
                            alert("输入的包含非法字符！");
                        }
                    }else{
                        bindOrg(_idArr, strategyNum);
                        detailDia.modal('hide');
                    }
                }),

                t.ui.createDialogButton('close','取消',function(){//取消
                })
            ],
            style:{
                ".modal-body":{"padding-top":"8px"},
                ".modal-footer":{"border-top":"none", "padding-top":"2px"},
                ".modal-dialog":{"top":"150px", "width":"420px"},
                ".btn-default":{"width":"160px","float":"left","margin":"0"},
                ".btn:nth-child(odd)":{"margin":"0 25px 0 20px"}
            }
        });

        detailDia.on("hidden.bs.modal", function(){
            this.remove();
            detailDia = null;
        });
    }

    initSearchCity();
    initSearchArea();
    fnSearchMsg(1);
    fnSearchLevel();
});

