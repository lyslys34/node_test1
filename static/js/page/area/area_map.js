require.config({
  baseUrl: MT.STATIC_ROOT + '/js',
  urlArgs: 'ver=' + pageVersion,
  shim: {
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core': ['page/common'],
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget': ['page/common'],
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position': ['page/common'],
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu': ['page/common'],
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete': ['page/common',
      'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
      'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
      'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
      'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu'
    ],
    'lib/autocomplete': {
      deps: ['page/common']
    },
    'lib/bootstrap': {
      deps: ['lib/jquery']
    },
  }
});
require(['module/root', 'module/validator', 'module/cookie',
    'lib/autocomplete', 'page/area/area_util', 'lib/clipper',
    'page/area/area_constants'
  ],
  function(t, validator, cookie, autocomplete, util, clipper, constants) {

    // alert(1);

    // 定义外卖（蜂窝）
    var WM_ZOOM_LEVEL = 13;

    /*
     * 标识页面对应的配送区域类型
     * 0 - no-type
     * 1 - 自建
     * 2 - 加盟
     * 4 - 众包
     * 7 - 城市代理
     */
    var pageType = window.pageType;   // 标识当前页面选中的配送区域类型
    var isAdmin = false;              // 标识当前页面是否为总部员工
    var orgType;                      // 标识当前页面中的角色

    var map;
    var polygon;
    var shippingPolygon;
    var editorTool;
    var shippingEditorTool;
    var id;
    var originArea;
    var windowsArr = [];
    var marker = [];
    var cityNameMap = {};
    var cityIdMap = {};
    var existAreas;
    //  var allExistAreas = [];
    var allExistAreasMap = {
      0: [],
      1: [],
      3: [],
      2: [],
      4: [],
      5: [],
      7: []
    };
    var noIntersectAreaType = [0, 1, 2, 7];
    var tipInfo;
    var wmMarkers = [];
    var wmAreas = [];
    //  var deliveryAreas = [];
    var selfSupportArea = [];
    var partnerArea = [];
    var crowdSourcingArea = [];
    var deliveryAreaMap = {
      0: [], // No type area ?
      1: [], // 自建
      2: [], // 加盟
      4: [], // 众包
      7: []  // 城市代理
    };
    var mapCityId = 0;
    var mouseToolListener;
    var isOrgManager;
    var eachShippingPolygon;
    var managerRoleCodeArr = [2001, 2100, 2101, 2201, 2202, 2203, 2301, 2302, 2031];

    // 标识当前正在提交中，避免用户多次提交
    var isSubmiting = false;
    // 定义一个全局的roleCode
    var roleCode = +($('#roleCode') && $('#roleCode').val());
    var areaMapping = constants.AREA_TYPE_MAP;


    $(document).ready(function() {
      // $('[type="checkbox"]').bootstrapSwitch();
      isOrgManager = $("#isOrgManager").val();
      // 如果没有isOrgManager字段或者其值为0，那么当前为总部员工
      if (!isOrgManager || isOrgManager == 0) {
        isAdmin = true;
      }
      orgType = $('#initType').val();

      var allowChange = $("#allowChange").val();

      initAreaType();

      if (!allowChange || allowChange != 1) {

        var initType = $("#type").val();
        // TODO 是不是根据type权限获取一次派送策略列表？
        if (initType > 0) {
          var countBindBmOrg = $("#countBindBmOrg").val();
          if (!countBindBmOrg || countBindBmOrg <= 1) {
            $("#type").change();
          }
        }
      }

      initMenu();
      var tipName = $("#tipName").val();
      if (tipName) {
        $("#commonMenu li").each(function() {
          if (tipName == $(this).attr("value")) {
            $(this).click();
          }
        })
      } else {
        $("#commonMenu li").first().click();
      }

      if ($.inArray(roleCode, managerRoleCodeArr) == -1) {
        initSearchCity();
      }

      initialize();
      initTipSearch();
      initStartEditBtn();
      initEndEditBtn();
      initAddBtn();
      initCancelBtn();
      getAllCity();
      initCheckBox();

      initTypeChangeIntersectModal();
      initCommitIntersectModal();
      initCommitCityCheckModal();

      // 如果为新建区域，那么不显示地图绘制提示层
      var id = $('#id').val();
      if (!id) {
        $('#map_draw_tip').hide();
      }

      // 解决部分控件的tooltip不及时销毁的问题
      $('label[data-toggle=tooltip]').on('mouseout', function() {
        $('div.tooltip.top').remove();
      });

    });

    function clearAreaMap() {
      allExistAreasMap = {
        0: [],
        1: [],
        3: [],
        2: [],
        4: [],
        5: []
      };

      // 循环清除所有区域数据
      for (var mapKey in deliveryAreaMap) {
        deliveryAreaMap[mapKey].forEach(function (item) {
          item.setMap();
        });
      }

      deliveryAreaMap = {0: [], 1: [], 2: [], 4: [], 7: []};

    }

    function cleanWmAreaMap() {
      for (var i = 0; i < wmMarkers.length; i++) {
        wmMarkers[i].setMap();
      }
      wmMarkers = [];

      for (var i = 0; i < wmAreas.length; i++) {
        wmAreas[i].setMap();
      }
      wmAreas = [];
    }

    function initialize() {
      id = $("#id").val().trim();
      originArea = $("#originArea").val().trim();

      // 初始化地图对象，加载地图
      map = new AMap.Map("mapContainer", {
        resizeEnable: true,
        view: new AMap.View2D({
          resizeEnable: true,
          zoom: 13 //地图显示的缩放级别
        }),
        keyboardEnable: false
      });

      //在地图中添加ToolBar插件
      map.plugin(["AMap.ToolBar"], function() {
        toolBar = new AMap.ToolBar();
        //    toolBar.setOffset(new AMap.Pixel(10,50));
        map.addControl(toolBar);
      });

      //加载比例尺插件
      map.plugin(["AMap.Scale"], function() {
        scale = new AMap.Scale();
        map.addControl(scale);
      });

      //地图类型切换
      // map.plugin(["AMap.MapType"], function() {
      //  var type = new AMap.MapType({defaultType:0});//初始状态使用2D地图
      //  map.addControl(type);
      // });

      if (originArea != null && originArea != "") {
        var cityId = $(".js_city_id").val();
        // alert(cityId);

        // drawExistArea();

        var arr = new Array(); //构建多边形经纬度坐标数组
        //json解析之前保存的配送区域originArea
        var areas = JSON.parse(originArea);
        for (var i in areas) {
          arr.push(new AMap.LngLat(areas[i].y, areas[i].x));
        }

        var polygonObj = $.extend(constants.EDITING_AREA_POLY_OPTS, {
          map: map,
          path: arr
        });
        polygon = new AMap.Polygon(polygonObj);
        addAreaChangeListener(polygon);
        changeShippingArea(polygon.getPath());

        // document.getElementById('resultInfo').innerHTML = polygon.getPath();

        $("#area").val(polygon.getPath());
        $("#initArea").val(polygon.getPath());
        //添加编辑控件
        map.plugin(["AMap.PolyEditor"], function() {
          editorTool = new AMap.PolyEditor(map, polygon);
          openMapEditor();
        });
        map.setZoomAndCenter(14, new AMap.LngLat(areas[0].y, areas[0].x));
        getWaimaiArea(cityId);
        getExistArea(cityId);
        var lineArr = polygon.getPath();
        lineArr.push(lineArr[0]);
        var polyline = new AMap.Polyline({
          path: lineArr
        });
        // alert(polyline.getLength());
        // polyline.setMap(map);
        showAreaLength(polyline.getLength());
        polyline.setPath(lineArr.pop());
      }

      AMap.event.addListener(map, 'moveend', function() {
        map.getCity(function(data) {
          setSurrentCity(data);
        });
        if (editorTool) {
          openMapEditor();
        }
      });

      AMap.event.addListener(map, 'zoomchange', function(e) {
        showCheckedArea();
      });
    }


    // TODO showCheckedArea()和initCheckBox()两个函数中的功能可以抽象
    function showCheckedArea() {
      var zoom = map.getZoom();
      $(".area_type").each(function() {
        if ($(this).is(":checked")) {
          var type = $(this).val();

          showTypeArea(type, true);
          // 重新绘制没有类型信息区域
          showTypeArea(0, true);
        }
      });
    }

    function addShippingAreaChangeListener(polygon) {
      AMap.event.addListener(polygon, 'change', function() {
        var lineArr = shippingPolygon.getPath();
        lineArr.push(lineArr[0]);
        var polyline = new AMap.Polyline({
          path: lineArr
        });
        showShippingAreaLength(polyline.getLength());
        polyline.setPath(lineArr.pop());
      });
    }

    function addAreaChangeListener(thisPolygon) {
      AMap.event.addListener(thisPolygon, 'change', function() {
        var lineArr = polygon.getPath();
        lineArr.push(lineArr[0]);
        var polyline = new AMap.Polyline({
          path: lineArr
        });

        showAreaLength(polyline.getLength());
        polyline.setPath(lineArr.pop());

        changeShippingArea(polygon.getPath());
      });
    }

    // 绘制配件区域至地图
    function changeShippingArea(thisPolygon) {
      // 为"1"标识为新建送件区域，为"0"标识为编辑送件区域
      var isSelfCreateShippingArea = $("#isSelfCreateShippingArea").val();
      if (!isSelfCreateShippingArea || isSelfCreateShippingArea == 0) {

        if (!shippingPolygon) {
          var initShippingArea = $("#initShippingArea").val().trim();
          var shippingArea = JSON.parse(initShippingArea);
          var arr = new Array();
          for (var i in shippingArea) {
            arr.push(new AMap.LngLat(shippingArea[i].y, shippingArea[i].x));
          }

          var polygonObj = $.extend(constants.EDITING_SHIPPING_AREA_POLY_OPTS, {
            map : map,
            path : arr
          });
          shippingPolygon = new AMap.Polygon(polygonObj);

          // 显示送件范围的公里数
          var lineArr = shippingPolygon.getPath();
          lineArr.push(lineArr[0]);
          var polyline = new AMap.Polyline({
            path: lineArr
          });
          showShippingAreaLength(polyline.getLength());
        }

        return;
      }


      // 下面这一堆code都是在处理没有送件区域时，取件区域和送件区域的联动？
      var biggerPolygons = new Array();
      var biggerPolygon = new Array();
      // var lineArr2 = polygon.getPath();
      for (var i = 0; i < thisPolygon.length; i++) {
        var point = {
          "X": thisPolygon[i].getLat() * 1000000,
          "Y": thisPolygon[i].getLng() * 1000000
        };
        biggerPolygon.push(point);
      }

      biggerPolygons.push(biggerPolygon);

      var co = new ClipperLib.ClipperOffset();
      // co.AddPaths(biggerPolygons, true);
      // co.Clear();
      co.AddPaths(biggerPolygons, ClipperLib.JoinType.jtMiter, ClipperLib.EndType.etClosedPolygon);
      co.MiterLimit = 2;
      co.ArcTolerance = 0.25;
      var offsettedPaths = new ClipperLib.Paths();
      co.Execute(offsettedPaths, 10000);

      var arr = new Array(); //构建多边形经纬度坐标数组
      for (var i in offsettedPaths[0]) {
        arr.push(new AMap.LngLat(offsettedPaths[0][i].Y / 1000000, offsettedPaths[0][i].X / 1000000));
      }

      if (shippingPolygon) {
        shippingPolygon.setMap();
      }

      var polygonObj = $.extend(constants.EDITING_SHIPPING_AREA_POLY_OPTS, {
        map : map,
        path : arr
      });
      shippingPolygon = new AMap.Polygon(polygonObj);

      // 显示送件范围的公里数
      var lineArr = shippingPolygon.getPath();
      lineArr.push(lineArr[0]);
      var polyline = new AMap.Polyline({
        path: lineArr
      });
      showShippingAreaLength(polyline.getLength());
      polyline.setPath(lineArr.pop());

    }

    function setSurrentCity(data) {

      var countBindBmOrg = $("#countBindBmOrg").val();

      if (countBindBmOrg && countBindBmOrg > 0) {
        return false;
      }

      if (isOrgManager && isOrgManager == 1 && countBindBmOrg && countBindBmOrg == 1) {
        return false;
      }

      var city;
      if (data['province'] && typeof data['province'] === 'string') {
        if (data['city'] != '') {
          city = data['city'];
        } else {
          city = data['province'];
        }

        //因义乌为县级市，但地图获取城市到地级市，而在我们系统中需要使用，为避免改动过大，这里单独处理义乌
        var yiwuName = "义乌市";
        if (data['district'] && typeof data['district'] === 'string' && data['district'] == yiwuName) {
          city = data['district'];
        }
      }

      if (typeof city === 'undefined') {
        $(".js_city_name").val("无法获取当前城市");
        // $(".js_city_id").val(0);
        changeCityId(0);
        return;
      }

      for (var eachCity in cityNameMap) {
        if (city.length >= eachCity.length) {
          if (city.substr(0, eachCity.length) == eachCity) {
            $(".js_city_name").val(eachCity);
            changeCityId(cityNameMap[eachCity].id);
            return;
          }
        }
      }

      $(".js_city_name").val("无法找到当前城市与系统对应城市");
      changeCityId(0);
    }



    function initCancelBtn() {
      $("#B_cancel").on("click", function() {
        window.location.reload();
      });
    }

    function initAddBtn() {
      // 用于提交时的各种有效性验证
      $('#B_add').on('click', function() {

        var areaName = $("#areaName").val().trim();
        if (areaName == "") {
          submitCheckDialog('区域名称未填写提示', '区域名称未填写，请填写后提交！');
          return;
        }

        if (!validator.checkInput(areaName)) {
          submitCheckDialog('区域名称有非法字符提示', '区域名称有非法字符，请更正后提交！');
          return;
        }

        var cityId = $("#cityId").val().trim();
        var cityName = $("#cityName").val().trim();
        if (cityId == 0 && cityName != "") {
          submitCheckDialog('城市名称未填写提示', '城市名称未填写，请填写后提交！');
          return;
        }

        if (cityId == 0) {
          submitCheckDialog('城市名称不正确提示', '城市名称不正确，请更正后提交！');
          return;
        }
        if (cityId == "") {
          ssubmitCheckDialog('城市名称未填写提示', '城市名称未填写，请填写后提交！');
          return;
        }

        var type = $('#area_type input:checked').val();
        if (typeof type == 'undefined' || type < 1) {
          submitCheckDialog('配送区域类型未选择提示', '配送区域类型未选择，请选择后提交！');
          return;
        }

        var dispatchStrategy = $('#dispatch_mode input:checked').val();
        if (typeof dispatchStrategy == 'undefined' || dispatchStrategy < 1) {
          submitCheckDialog('调度模式未选择提示', '调度模式未选择，请选择后提交！');
          return;
        }

        if (!editorTool) {
          submitCheckDialog('配送区域未绘制提示', '配送区域未绘制，请绘制后提交！');
          return;
        }

        editorTool.close();
        shippingEditorTool && shippingEditorTool.close();

        $('#resultInfo').html(polygon && polygon.getPath());
        $("#area").val(polygon && polygon.getPath());
        var area = $("#area").val().trim();
        if (area == "") {
          submitCheckDialog('配送区域未绘制提示', '配送区域未绘制，请绘制后提交！');
          openMapEditor();
          return;
        }

        if (!shippingPolygon) {
          showError("无法获取送件范围");
          return;
        }

        $("#shippingArea").val(shippingPolygon && shippingPolygon.getPath());
        var shippingArea = $("#shippingArea").val().trim();
        if (shippingArea == "") {
          showError("无法获取配送区域对应送件范围");
          openMapEditor();
          return;
        }

        var intersect = $("#intersect").val();
        var allowIntersect = 1;
        var initType = $("#initType").val();
        if (typeof initType == 'undefined' || initType == '') {
          showError("无法获取配送区域所属类型");
          openMapEditor();
          return;
        }


        if (allowIntersect != intersect) {
          var isInNoIntersect = false;
          var noIntersectLen = !noIntersectAreaType ? 0 : noIntersectAreaType.length;
          for (var i = 0; i < noIntersectLen; i++) {
            if (type == noIntersectAreaType[i]) {
              isInNoIntersect = true;
            }
          }

          if (isInNoIntersect) {
            // 对于非众包类型不能和其他非众包类型的区域重合
            for (var i = 0; i < noIntersectLen; i++) {
              if (true == checkIsIntersect(polygon.getPath(), allExistAreasMap[noIntersectAreaType[i]])) {
                var sourceType = areaMapping[type];
                var existingType = areaMapping[noIntersectAreaType[i]];
                showCheckIntersectDialog(sourceType, existingType);
                // showError("配送区域不能重叠");
                openMapEditor();
                return;
              }
            }
          } else {
            // 对于众包类型不能和众包类型其他区域重合
            if (true == checkIsIntersect(polygon.getPath(), allExistAreasMap[type])) {
              var sourceType = areaMapping[type];
              showCheckIntersectDialog(sourceType, sourceType);
              openMapEditor();
              return;
            }
          }

        }

        if (!id) {
          id = 0;
        }

        var lineArr = polygon.getPath();
        lineArr.push(lineArr[0]);
        var polyline = new AMap.Polyline({
          path: lineArr
        });
        var length = polyline.getLength();
        polyline.setPath(lineArr.pop());
        $("#save_id").val(id);
        $("#save_city_id").val(cityId);
        $("#save_area_name").val(areaName);
        $("#save_area").val(area);
        $("#save_type").val(type);
        $("#save_dispatchStrategy").val(dispatchStrategy);
        $("#save_shipping_area").val(shippingArea);

        /*
         * Modified by liyishan 2016.2.17
         * 与韩菁确认，保存时不再校验公里数
         */
        checkAffectPoi(id, area);
      });

      $("#B_check_continue").click(function() {
        $("#saveOutAreaModal").modal("hide");
        addChangeComment();
      });

      $("#B_check_cancel").click(function() {
        $("#saveOutAreaModal").modal("hide");
        openMapEditor();
        return;
      });

      $("#editChangeCommentSubmit").click(function() {
        var changeComment = $("#change_comment").val();

        if (validator.isBlank(changeComment)) {
          showEditChangeCommentError("修改原因不能为空");
          return false;
        }
        $("#editChangeCommentModal").modal("hide");
        saveData();
      });

      $("#cancelChangeCommentSubmit").click(function() {
        $("#editChangeCommentModal").modal("hide");
        openMapEditor();
        return false;
      });
    }

    function checkAffectPoi(id, deliveryAreaCoordinates) {
      if (id < 1) {
        addChangeComment();
      } else {

        $.ajax({
          dataType: 'json',
          type: 'post',
          url: "/delivery/checkAffectPoi",
          data: {
            id: id,
            deliveryAreaCoordinates: deliveryAreaCoordinates
          },
          success: function(r) {
            if (r.success) {
              if (r.data && r.data.length > 0) {
                var areaPoiList = r.data;
                var pageData = [];
                $.each(areaPoiList, function(n, value) {
                  pageData.push("<tr>");
                  pageData.push("<td>" + value.merchantpoi + "</td>");
                  pageData.push("<td>" + value.name + "</td>");
                  pageData.push("</tr>");
                });
                $("#affectPoi").empty();
                $("#affectPoi").append(pageData.join(''));
                $("#saveOutAreaModal").modal();
              } else {
                addChangeComment();
              }


            } else {
              showError(r.errMsg);
            }
          },
          error: function(XMLHttpRequest, errMsg) {
            alert("网络连接失败");
            // alert(errMsg);
          }
        });
      }
    }

    function addChangeComment() {
      var oldArea = $("#initArea").val();
      var area = $("#save_area").val();
      var id = $("#save_id").val();

      /*
       * Modified by liyishan 2016.2.17
       * 与韩菁确认，保存时不再弹出comment对话框
       * 先不删除原有的逻辑，之后有机会再重构
       */
       /*
      if (id && id > 0 && oldArea != area) {
        $("#editChangeCommentModal").modal();
      } else {
        saveData();
      } */
      saveData();
    }

    function saveData() {
      isSubmiting = true;
      var id = $("#save_id").val();
      var cityId = $("#save_city_id").val();
      var areaName = $("#save_area_name").val();
      var area = $("#save_area").val();
      var type = $("#save_type").val();
      var dispatchStrategy = $("#save_dispatchStrategy").val();
      var changeComment = $("#change_comment").val();
      var shippingArea = $("#save_shipping_area").val();

      $.ajax({
        dataType: 'json',
        type: 'post',
        url: "/delivery/createOrUpdateArea",
        data: {
          id: id,
          bmCityId: cityId,
          deliveryAreaName: areaName,
          deliveryAreaDesc: "",
          deliveryAreaCoordinates: area,
          type: type,
          dispatchStrategy: dispatchStrategy,
          changeComment: changeComment,
          shippingAreaCoordinates: shippingArea
        },
        success: function(data) {
          if (data.success) {
            showSaveSuccess();
            isSubmiting = false;
          } else {
            showError(data.errMsg);
            isSubmiting = false;
          }
        },
        error: function(XMLHttpRequest, errMsg) {
          alert("网络连接失败");
          isSubmiting = false;
          // alert(errMsg);
        }
      });
    }

    function resetEditingMap () {
      $('#map_draw_tip').hide();

      // whichPolygon原来是标识是编辑取件范围还是送件范围
      var whichPolygon = $("#B_start_edit_area").attr('rel');
      if (whichPolygon) {
        if (whichPolygon == 0) {
          if (id) {
            // 这里有可能initShippingArea的数据为空
            var initShippingArea = $("#initShippingArea").val().trim();
            var areas = JSON.parse(initShippingArea);
            var arr = new Array();
            for (var i in areas) {
              arr.push(new AMap.LngLat(areas[i].y, areas[i].x));
            }

            shippingPolygon.setPath(arr);
          } else {
            if (polygon && polygon.getPath() != '') {
              $("#isSelfCreateShippingArea").val(1);
              changeShippingArea(polygon.getPath());
              $("#B_start_edit_area").attr('rel', 1);
              // 这里如果有打开送件范围的editor先关闭
              shippingEditorTool && shippingEditorTool.close();
              // $("#B_start_edit_area").click();
              // return false;
            }
          }
        }
      }

      if (id) {
        // location.reload();
        var originArea = $("#originArea").val().trim();
        var areas = JSON.parse(originArea);
        var arr = new Array();
        for (var i in areas) {
          arr.push(new AMap.LngLat(areas[i].y, areas[i].x));
        }

        polygon.setPath(arr);
        return false;
      }

      if (!editorTool) {
        return false;
      }

      if (polygon && polygon.getPath() != '') {
        polygon.setPath('');
        polygon = '';

        if (shippingPolygon && shippingPolygon.getPath != '') {
          shippingPolygon.setPath('');
          shippingPolygon = '';
        }

        $("#area").val('');
        //设置多边形的属性
        var polygonOption = constants.EDITING_AREA_POLY_OPTS;

        //在地图中添加MouseTool插件
        map.plugin(["AMap.MouseTool"], function() {
          var mouseTool = new AMap.MouseTool(map);
          //使用鼠标工具绘制多边形
          mouseTool.polygon(polygonOption);
          AMap.event.addListener(mouseTool, "draw", function(e) {
            $('#map_draw_tip').show();
            //obj属性就是绘制完成的覆盖物对象
            polygon = e.obj;
            $("#isSelfCreateShippingArea").val(1);
            changeShippingArea(polygon.getPath());
            var lineArr = polygon.getPath();
            lineArr.push(lineArr[0]);
            var polyline = new AMap.Polyline({
              path: lineArr
            });
            // alert(polyline.getLength());
            // polyline.setMap(map);
            showAreaLength(polyline.getLength());
            polyline.setPath(lineArr.pop());

            addAreaChangeListener(polygon);
            //获取节点个数
            var pointsCount = e.obj.getPath().length;
            //添加编辑控件
            map.plugin(["AMap.PolyEditor"], function() {
              editorTool = new AMap.PolyEditor(map, polygon);
              openMapEditor();
            });
            $('#resultInfo').html(polygon.getPath());
            $("#area").val(polygon.getPath());
            mouseTool.close();
          });
        });
      }
    }

    function initStartEditBtn() {
      $("#B_start_edit_area").on('click', function() {
        if (shippingPolygon) {
          // editorTool.close();
          // shippingEditorTool && shippingEditorTool.close();
          var whichPolygon = $(this).attr('rel');
          if (whichPolygon) {
            // 1为联合编辑，点击后分开编辑
            // 0为分开编辑，根据新的需求，点击之后无反应
            if (whichPolygon == 1) {
              editorTool.close();
              shippingEditorTool && shippingEditorTool.close();

              // 同时编辑取件范围及送件范围
              editorTool = new AMap.PolyEditor(map, polygon);
              shippingEditorTool = new AMap.PolyEditor(map, shippingPolygon);
              addShippingAreaChangeListener(shippingPolygon);
              shippingEditorTool.open();

              // 标识当前的状态
              $(this).attr('rel', 0);
              // $(this).text("编辑取件范围");
              // $(this).removeClass("btn-danger").addClass("btn-success");
              var isSelfCreateShippingArea = $("#isSelfCreateShippingArea").val();
              if (isSelfCreateShippingArea && isSelfCreateShippingArea == 1) {
                $("#isSelfCreateShippingArea").val(0);
              }
            }
          }

          editorTool.open();
        }

        if (id) {
          openMapEditor();
          return false;
        }

        if (mouseToolListener) {
          return false;
        }

        //设置多边形的属性
        var polygonOption = constants.EDITING_AREA_POLY_OPTS;
        //在地图中添加MouseTool插件
        map.plugin(["AMap.MouseTool"], function() {
          var mouseTool = new AMap.MouseTool(map);
          //使用鼠标工具绘制多边形
          mouseTool.polygon(polygonOption);
          mouseToolListener = AMap.event.addListener(mouseTool, "draw", function(e) {
            $('#map_draw_tip').show();

            //obj属性就是绘制完成的覆盖物对象
            polygon = e.obj;
            changeShippingArea(polygon.getPath());

            var lineArr = polygon.getPath();
            lineArr.push(lineArr[0]);
            var polyline = new AMap.Polyline({
              path: lineArr
            });
            // alert(polyline.getLength());
            // polyline.setMap(map);
            showAreaLength(polyline.getLength());
            polyline.setPath(lineArr.pop());

            addAreaChangeListener(polygon);
            //获取节点个数
            var pointsCount = e.obj.getPath().length;
            //添加编辑控件
            map.plugin(["AMap.PolyEditor"], function() {
              editorTool = new AMap.PolyEditor(map, polygon);
              openMapEditor();
            });
            // document.getElementById('resultInfo').innerHTML = polygon.getPath();
            $("#area").val(polygon.getPath());
            mouseTool.close();
          });
        });
      });

      $('#B_refresh_edit_area').on('click', function() {
          showResetMapModal()
          .done(function() {
            resetEditingMap();
          });
      });

      $("#B_init_edit_area").on('click', function() {
        var isInit = $(this).attr('rel');

        if (isInit && isInit == 0) {
          $(this).attr('rel', 1);
          $("#B_start_edit_area").click();
          $("#B_init_edit_area").hide();
          $("#B_start_edit_area").show();
        }

      });
    }

    function initEndEditBtn() {

      // $('#B_return_edit_area').on('click', function(){
      $('#resetZoomBtn').on('click', function() {
        if (typeof polygon == 'undefined' || polygon == '') {
          map.setZoomAndCenter(11, null, null);
          return;
        }
        var a = polygon.getPath();
        if (typeof a == 'undefined' || a == '') {
          map.setZoomAndCenter(11, null, null);
          return;
        }
        polygon.setPath('');
        document.getElementById('resultInfo').innerHTML = '';
        $("#area").val('');
        var polygonObj = $.extend(constants.EDITING_AREA_POLY_OPTS, {
          map : map,
          path : a
        });
        polygon = new AMap.Polygon(polygonObj);

        var lineArr = polygon.getPath();
        lineArr.push(lineArr[0]);
        var polyline = new AMap.Polyline({
          path: lineArr
        });
        // alert(polyline.getLength());
        // polyline.setMap(map);
        showAreaLength(polyline.getLength());
        polyline.setPath(lineArr.pop());
        addAreaChangeListener(polygon);

        editorTool = new AMap.PolyEditor(map, polygon);
        // editorTool.open(map, polygon);
        openMapEditor();
        // map.setZoomAndCenter(14, new AMap.LngLat(a[0], a[1]));
        $('#resultInfo').html(polygon.getPath());
        $("#area").val(polygon.getPath());
        map.setZoomAndCenter(14, a[0]);

      });
    }

    function redrawPolygon() {
      if (typeof polygon == 'undefined' || polygon == '') {
        return;
      }
      var a = polygon.getPath();
      if (!a) {
        return;
      }

      polygon.setPath('');
      $('#resultInfo').html('');
      $("#area").val('');

      var polygonObj = $.extend(constants.EDITING_AREA_POLY_OPTS, {
        map : map,
        path : a
      });
      polygon = new AMap.Polygon(polygonObj);

      addAreaChangeListener(polygon);

      editorTool = new AMap.PolyEditor(map, polygon);
      // editorTool.open(map, polygon);
      openMapEditor();
      // map.setZoomAndCenter(14, new AMap.LngLat(a[0], a[1]));
      $('#resultInfo').html(polygon.getPath());
      $("#area").val(polygon.getPath());
    }

    function showError(errMsg) {
      $("#alert_error").empty();
      $("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
    }

    function showEditChangeCommentError(errMsg) {
      $("#alert_edit_change_comment_error").empty();
      $("#alert_edit_change_comment_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
    }

    // 显示取件范围的公里数
    function showAreaLength(length) {
      var length = Number(Number(length) / 1000).toFixed(5);
      $(".fetch-mile").empty();
      $(".fetch-mile").html(length);
      // 超出推荐范围显示
      if (length > 12 || length < 8) {
        $('.overrange-tip').show();
      } else {
        $('.overrange-tip').hide();
      }
    }

    // 显示配件范围的公里
    function showShippingAreaLength(length) {
      var length = Number(Number(length) / 1000).toFixed(5);
      $(".dispatch-mile").empty();
      $(".dispatch-mile").html(length);
    }

    //输入提示框鼠标滑过时的样式
    function openMarkerTipById(pointid, thiss) { //根据id打开搜索结果点tip
      thiss.style.background = '#CAE1FF';
    }

    //输入提示框鼠标移出时的样式
    function onmouseout_MarkerStyle(pointid, thiss) { //鼠标移开后点样式恢复
      thiss.style.background = "";
    }

    //从输入提示框中选择关键字并查询
    function selectResult(index) {
      if (index < 0) {
        return;
      }
      if (navigator.userAgent.indexOf("MSIE") > 0) {
        document.getElementById("keyword").onpropertychange = null;
        document.getElementById("keyword").onfocus = focus_callback;
      }
      //截取输入提示的关键字部分
      var text = document.getElementById("divid" + (index + 1)).innerHTML.replace(/<[^>].*?>.*<\/[^>].*?>/g, "");
      var cityCode = document.getElementById("divid" + (index + 1)).getAttribute('data');
      document.getElementById("keyword").value = text;
      document.getElementById("result1").style.display = "none";
      //根据选择的输入提示关键字查询
      map.plugin(["AMap.PlaceSearch"], function() {
        var msearch = new AMap.PlaceSearch(); //构造地点查询类
        AMap.event.addListener(msearch, "complete", placeSearch_CallBack); //查询成功时的回调函数
        msearch.setCity(cityCode);
        msearch.search(text); //关键字查询查询
      });

    }

    //定位选择输入提示关键字
    function focus_callback() {
      if (navigator.userAgent.indexOf("MSIE") > 0) {
        document.getElementById("keyword").onpropertychange = autoSearch;
      }
    }

    //输出关键字查询结果的回调函数
    function placeSearch_CallBack(data) {
      //清空地图上的InfoWindow和Marker
      windowsArr = [];
      marker = [];
      map.clearMap();
      var resultStr1 = "";
      var poiArr = data.poiList.pois;
      var resultCount = poiArr.length;
      for (var i = 0; i < resultCount; i++) {
        resultStr1 += "<div id='divid" + (i + 1) + "' onmouseover='openMarkerTipById1(" + i + ",this)' onmouseout='onmouseout_MarkerStyle(" + (i + 1) + ",this)' style=\"font-size: 12px;cursor:pointer;padding:0px 0 4px 2px; border-bottom:1px solid #C1FFC1;\"><table><tr><td><img src=\"http://webapi.amap.com/images/" + (i + 1) + ".png\"></td>" + "<td><h3><font color=\"#00a6ac\">名称: " + poiArr[i].name + "</font></h3>";
        resultStr1 += TipContents(poiArr[i].type, poiArr[i].address, poiArr[i].tel) + "</td></tr></table></div>";
        addmarker(i, poiArr[i]);
      }
      map.setFitView();

      // TODO 仅在更换城市时才调用redrawPolygon方法？
      redrawPolygon();

      map.getCity(function(data) {
        setSurrentCity(data);
      });
    }

    // 鼠标滑过查询结果改变背景样式，根据id打开信息窗体
    function openMarkerTipById1(pointid, thiss) {
      thiss.style.background = '#CAE1FF';
      windowsArr[pointid].open(map, marker[pointid]);
    }

    //添加查询结果的marker&infowindow
    function addmarker(i, d) {
      var lngX = d.location.getLng();
      var latY = d.location.getLat();
      var markerOption = {
        map: map,
        icon: "http://webapi.amap.com/images/" + (i + 1) + ".png",
        position: new AMap.LngLat(lngX, latY)
      };
      var mar = new AMap.Marker(markerOption);
      marker.push(new AMap.LngLat(lngX, latY));

      var infoWindow = new AMap.InfoWindow({
        content: "<h3><font color=\"#00a6ac\">  " + (i + 1) + ". " + d.name + "</font></h3>" + TipContents(d.type, d.address, d.tel),
        size: new AMap.Size(300, 0),
        autoMove: true,
        offset: new AMap.Pixel(0, -30)
      });
      windowsArr.push(infoWindow);
      var aa = function(e) {
        infoWindow.open(map, mar.getPosition());
      };
      AMap.event.addListener(mar, "mouseover", aa);
    }

    //infowindow显示内容
    function TipContents(type, address, tel) { //窗体内容

      type = !type ? "暂无" : type;
      address = !address ? "暂无" : address;
      tel = !tel ? "暂无" : tel;
      var str = "  地址：" + address + "<br />  电话：" + tel + " <br />  类型：" + type;

      return str;
    }

    function searchMapChange(event) {
      var key = (event || window.event).keyCode;
      var result = document.getElementById("result1")
      var cur = result.curSelect;
      if (key === 40) { //down
        if (cur + 1 < result.childNodes.length) {
          if (result.childNodes[cur]) {
            result.childNodes[cur].style.background = '';
          }
          result.curSelect = cur + 1;
          result.childNodes[cur + 1].style.background = '#CAE1FF';
          document.getElementById("keyword").value = result.tipArr[cur + 1].name;
        }
      } else if (key === 38) { //up
        if (cur - 1 >= 0) {
          if (result.childNodes[cur]) {
            result.childNodes[cur].style.background = '';
          }
          result.curSelect = cur - 1;
          result.childNodes[cur - 1].style.background = '#CAE1FF';
          document.getElementById("keyword").value = result.tipArr[cur - 1].name;
        }
      } else if (key === 13) {
        var res = document.getElementById("result1");
        if (res && res['curSelect'] !== -1) {
          selectResult(document.getElementById("result1").curSelect);
        }
      } else {
        autoSearch();
      }
    }

    //输入提示
    function autoSearch() {
      var keywords = document.getElementById("keyword").value;
      var auto;
      //加载输入提示插件
      AMap.service(["AMap.Autocomplete"], function() {
        var autoOptions = {
          city: "" //城市，默认全国
        };
        auto = new AMap.Autocomplete(autoOptions);
        //查询成功时返回查询结果
        if (keywords.length > 0) {
          auto.search(keywords, function(status, result) {
            autocomplete_CallBack(result);
          });
        } else {
          document.getElementById("result1").style.display = "none";
        }
      });
    }

    //输出输入提示结果的回调函数
    function autocomplete_CallBack(data) {
      var resultStr = "";
      var tipArr = data.tips;
      if (tipArr && tipArr.length > 0) {
        for (var i = 0; i < tipArr.length; i++) {
          // resultStr += "<div id='divid" + (i + 1) + "' ref='"+(i+1)+"' onmouseover='openMarkerTipById(" + (i + 1)
          //   + ",this)' onclick='selectResult(" + i + ")' onmouseout='onmouseout_MarkerStyle(" + (i + 1)
          //   + ",this)' style=\"font-size: 13px;cursor:pointer;padding:5px 5px 5px 5px;\"" + "data=" + tipArr[i].adcode + ">" + tipArr[i].name + "<span style='color:#C1C1C1;'>"+ tipArr[i].district + "</span></div>";
          resultStr += "<div id='divid" + (i + 1) + "' ref='" + (i + 1) + "' style=\"font-size: 13px;cursor:pointer;padding:5px 5px 5px 5px;\"" + "data=" + tipArr[i].adcode + ">" + tipArr[i].name + "<span style='color:#C1C1C1;'>" + tipArr[i].district + "</span></div>";

        }
      } else {
        resultStr = " π__π 亲,人家找不到结果!<br />要不试试：<br />1.请确保所有字词拼写正确<br />2.尝试不同的关键字<br />3.尝试更宽泛的关键字";
      }
      document.getElementById("result1").curSelect = -1;
      document.getElementById("result1").tipArr = tipArr;
      document.getElementById("result1").innerHTML = resultStr;
      document.getElementById("result1").style.display = "block";
    }

    function initTipSearch() {
      $(document).on("mouseover", "div[id^=divid]", function() {

        var value = $(this).attr("ref");
        openMarkerTipById(value, this);
      });
      $(document).on("click", "div[id^=divid]", function() {

        var value = $(this).attr("ref");
        selectResult(value - 1);
      });
      $(document).on("mouseout", "div[id^=divid]", function() {

        var value = $(this).attr("ref");
        onmouseout_MarkerStyle(value, this);
      });

      // 之前是用keydown事件，导致输入法的结果有些异常，改为input事件
      $("#keyword").on("input", function(event) {
        searchMapChange(event);
      });
    }

    function showSaveSuccess() {
      var successMsg = "保存成功";
      $("#alert_success_massage").empty();
      $("#alert_success_massage").append("<div class='alert alert-success' role='alert'>" + successMsg + "</div>");
      $("#saveSuccessModal").modal();
      setTimeout(function() {
        $("#saveSuccessModal").modal("hide");
        if (isOrgManager && isOrgManager == 1) {
          window.location.reload();
        } else {
          window.location.href = "/delivery/list";
        }
      }, 2000);

      $("#B_ok").click(function() {
        window.location.href = "/delivery/list";
      });
    }

    function getAllCity(city) {
      if ($.inArray(roleCode, managerRoleCodeArr) == -1) {
        myPOST('/org/citySearchList', {}, function(r) {
          if (r.httpSuccess) {
            if (r.data && r.data.code == 0) {
              if (r.data.data) {
                var cityId = $(".js_city_id").val();
                $.each(r.data.data, function(index, item) {
                  cityNameMap[item.value] = item;
                  cityIdMap[item.id] = item;

                  if (cityId) {
                    if (cityId == item.id) {
                      $(".js_city_name").val(item.value);
                    }
                  } else {
                    map.getCity(function(data) {
                      setSurrentCity(data);
                    });
                  }
                });
              }
            } else {
              alert("获取城市信息失败，" + r.data.msg);
            }
          } else {
            // alert('获取组织信息失败，系统错误');
          }
        });
      }
    }

    // 发送POST请求
    // 和module/root中utils.post方法一致，没必要重复
    function myPOST(url, data, callback) {
      t.utils.post(url, data, callback);
    }

    // });

    function changeCityId(newCityId) {
      // var oldCityId = $(".js_city_id").val();
      if (mapCityId != 0 && mapCityId == newCityId) {
        return;
      }
      $(".js_city_id").val(newCityId);
      if (newCityId != 0) {
        mapCityId = newCityId;
        // alert(mapCityId);
        getExistArea(newCityId);
        // drawExistArea();
        getWaimaiArea(newCityId);

      }

    }

    function drawExistArea(existAreas) {

      var existAreasData = existAreas && existAreas.data && existAreas.data.data;
      if (existAreasData) {
        var initType = $("#initType").val();
        clearAreaMap();
        $.each(existAreasData, function(n, value) {
          if (!id || (id && id != value.id)) {
            var type = value.type;
            if (type > -1 && allExistAreasMap[type]) {
              if (value.deliveryAreaCoordinates) {
                allExistAreasMap[type].push(JSON.parse(value.deliveryAreaCoordinates));
              }

              if (value.checked != 2 && value.auditDeliveryAreaCoordinates) {
                allExistAreasMap[type].push(JSON.parse(value.auditDeliveryAreaCoordinates));
              }
            }

            drawArea(value);
          }

        });

        showCheckedArea();
      }
    }

    /*
     * 该方法其实只负责构造绘制区域的数据以及多边形的样式设置，
     * 真正绘制地图的方法是用showCheckedArea（）完成
     */
    function drawArea(area) {
      if (area.id == id) {
        return;
      }

      if (!area.deliveryAreaCoordinates) {
        return;
      }

      var areas = JSON.parse(area.deliveryAreaCoordinates);
      var arr = new Array(); //构建多边形经纬度坐标数组
      // TODO 这里的areas有可能为0，而不是一个array，所以areas.map不能直接使用
      for (var i in areas) {
        arr.push(new AMap.LngLat(areas[i].y, areas[i].x));
      }

      // 建立不同类型的区域polygon映射，便于统一维护
      // 不同类型区域相关显示的基本设置请参考page/area/area_constants
      var polygonMap = constants.AREA_POLYGON_MAP;
      for (var key in polygonMap) {
        polygonMap[key]['path'] = arr;
      }

      var type = area.type;
      var polygon2 = new AMap.Polygon(polygonMap[type]);
      deliveryAreaMap[type].push(polygon2);

      AMap.event.addListener(polygon2, 'click', function(e) {
        // map.getCity(function(data){
        //   setSurrentCity(data);
        // });
        if (editorTool) {
          editorTool.open();
        }

        // var pointGeoHash = util.geoHash(e.lnglat.getLat(), e.lnglat.getLng());
        var point = {
          x: e.lnglat.getLat(),
          y: e.lnglat.getLng()
        };

        showTips(area, point);
      });
    }

    function drawWaimaiAreas(waimaiAreas) {
      if (waimaiAreas) {
        if (waimaiAreas.data) {
          if (waimaiAreas.data.data) {
            // alert(JSON.stringify(waimaiAreas.data.data));
            // 每次绘制之前都清除之前的数据
            cleanWmAreaMap();
            $.each(waimaiAreas.data.data, function(n, value) {
              drawWaimaiArea(value);
            });
            showCheckedArea();
          }
        }
      }
    }

    function drawWaimaiArea(area) {
      var arr = new Array(); //构建多边形经纬度坐标数组
      var areas = area.pointString.split(";");
      // alert(areas[1]);
      if (areas) {
        var centerPointLng = 0;
        var centerPointLat = 0;
        var maxX = -180;
        var minX = 180;
        var maxY = -90;
        var minY = 90;
        for (var i = 0; i < areas.length; i++) {
          var eachPoint = areas[i].split(",");
          arr.push(new AMap.LngLat(Number(eachPoint[1]), Number(eachPoint[0])));
          // centerPointLng += Number(eachPoint[1]);
          // centerPointLat +=  Number(eachPoint[0]);
          if (maxX < Number(eachPoint[1])) {
            maxX = Number(eachPoint[1]);
          }
          if (minX > Number(eachPoint[1])) {
            minX = Number(eachPoint[1]);
          }
          if (maxY < Number(eachPoint[0])) {
            maxY = Number(eachPoint[0]);
          }
          if (minY > Number(eachPoint[0])) {
            minY = Number(eachPoint[0]);
          }
        }

        var polygon2 = new AMap.Polygon({
          // map: map,
          path: arr,
          strokeColor: "#ff6564",
          strokeOpacity: 1,
          strokeWeight: 2,
          fillColor: "#ff6564",
          fillOpacity: 0.35
        });
        polygon2.setMap(map);
        wmAreas.push(polygon2);

        // TODO 显示外卖区域的片区名称？
        var m = "<div id = 'wmName'>" +
          "<p style='color:red; font-weight:bold; font-size:15px; z-index:1000;' >" + area.title + "</p></div>";

        var marker = new AMap.Marker({
          position: new AMap.LngLat((minX + maxX) / 2, (minY + maxY) / 2),
          content: m
        });

        // marker.setMap(map);
        wmMarkers.push(marker);
      }

    }

    function getWaimaiArea(cityId) {
      // 如果有roleCode并且存在于managerRoleCodeArr中, 那么不发送请求
      // 否则发送请求
      if ($.inArray(roleCode, managerRoleCodeArr) == -1) {
        myPOST('/delivery/waimaiAoiAreas', {
          cityId: cityId
        }, function(r) {
          if (r.httpSuccess) {
            if (r.data && r.data.code == 0) {
              if (r.data.data) {
                var waimaiAreas = r.data.data;
                drawWaimaiAreas(waimaiAreas);
              }
            } else {
              alert("获取蜂窝区域失败，" + r.data.msg);
            }
          } else {
            alert('获取蜂窝区域失败，系统错误');
          }
        });
      }
    }

    function getExistArea(cityId) {
      var roleCode = +($('#roleCode') && $('#roleCode').val());
      // 如果有roleCode并且存在于managerRoleCodeArr中, 那么不发送请求
      // 否则发送请求
      if ($.inArray(roleCode, managerRoleCodeArr) == -1) {
        myPOST('/delivery/existAreas', {
          cityId: cityId
        }, function(r) {
          if (r.httpSuccess) {
            if (r.data && r.data.code == 0) {
              if (r.data.data) {
                existAreas = r.data.data;
                drawExistArea(existAreas);
              }
            } else {
              alert("获取已有配送区域失败，" + r.data.msg);
            }
          } else {
            alert('获取已有配送区域失败，系统错误');
          }
        });
      }
    }

    // 检测区域是否重叠
    function checkIsIntersect(polygonPoint, existAreaPolygons) {
      var polygon1 = new Array();
      if (polygonPoint) {
        for (var i = 0; i < polygonPoint.length; i++) {
          var point = {
            "x": polygonPoint[i].getLat(),
            "y": polygonPoint[i].getLng()
          };
          polygon1.push(point);
        }

        if (existAreaPolygons) {
          for (var i = 0; i < existAreaPolygons.length; i++) {
            if (existAreaPolygons[i] == null) {
              continue;
            }
            if (true == util.checkIntersect(polygon1, existAreaPolygons[i])) {
              return true;
            }
          }
        }
      }

      return false;
    }

    // 点击多边型时显示对应的tip信息
    function showTips(area, point) {
      var polygon2 = JSON.parse(area.deliveryAreaCoordinates);
      if (true == util.checkPointInPolygon(point, polygon2)) {
        //构建信息窗体中显示的内容
        tipInfo = [];
        tipInfo.push("<div>");
        tipInfo.push("<div style=\"padding:0px 0px 0px 4px;\">区域名称 : ");
        tipInfo.push(area.deliveryAreaName);
        tipInfo.push("所在城市 : ");
        tipInfo.push(area.cityName);
        tipInfo.push("绑定组织 : ");

        var bindOrgNames = area.bmOrgList;
        if (bindOrgNames) {
          for (var i = 0; i < bindOrgNames.length; i++) {
            tipInfo.push(bindOrgNames[i].bmOrgName);
            tipInfo.push(" ");
          }
        } else {
          tipInfo.push("未绑定组织");
        }

        tipInfo.push("</div></div>");

        var infoWindow = new AMap.InfoWindow({
          content: tipInfo.join("<br/>") //使用默认信息窗体框样式，显示信息内容
        });
        infoWindow.open(map, new AMap.LngLat(point.y, point.x));

        if (eachShippingPolygon) {
          eachShippingPolygon.setMap();
        }

        if (area.shippingAreaCoordinates) {
          var shippingArea = JSON.parse(area.shippingAreaCoordinates);
          var arr = new Array();
          for (var i in shippingArea) {
            arr.push(new AMap.LngLat(shippingArea[i].y, shippingArea[i].x));
          }

          eachShippingPolygon = new AMap.Polygon({
            map: map,
            path: arr,
            strokeColor: "#0f5bb0",
            strokeOpacity: 1,
            strokeWeight: 2,
            fillColor: "#ffffff",
            zIndex: 100,
            fillOpacity: 0.4
          });
        }

      }

    }

    // 根据配送类型type在地图上显示对应的多边形
    function showTypeArea(type, showFlag) {
      var zoom = map.getZoom();
      if (type == -1) {
        // 外卖（蜂窝）区域名字标识
        wmMarkers.forEach(function(item, index) {
          // 当地图的zoom level < 13时，不再显示蜂窝区域
          if (zoom < WM_ZOOM_LEVEL || !showFlag) {
            item.setMap();
          } else {
            item.setMap(map);
          }
        });

        // 外卖（蜂窝）区域地图显示
        wmAreas.forEach(function(item, index) {
          // 当地图的zoom level < 13时，不再显示蜂窝区域
          if (zoom < WM_ZOOM_LEVEL || !showFlag) {
            item.setMap();
          } else {
            item.setMap(map);
          }
        });
      } else {
        var areaMap = deliveryAreaMap[type];
        areaMap.forEach(function(item, index) {
          if (showFlag) {
            item.setMap(map);
          } else {
            item.setMap();
          }
        });
      }
    }

    // 设置在地图中显示哪种类型的多边型
    function initCheckBox() {
      $(".area_type").click(function() {
        var type = $(this).val();
        showTypeArea(type, this.checked);
      });
    }

    // 搜索对应的城市区域
    function initSearchCity() {

      var cityNameMap = {};
      var cityIdMap = {};
      t.utils.post('/org/citySearchList', {}, function(r) {
        if (r.httpSuccess) {
          if (r.data && r.data.code == 0) {

            // 构造城市自动填写下拉菜单
            $(".js_city_name").autocomplete({
              delay: 100,
              source: r.data.data,
              focus: function(event, ui) {
                $(".js_city_name").val(ui.item.value);
                $(".js_city_id").val(ui.item.id);
                return false;
              },
              select: function(event, ui) {
                $(".js_city_name").val(ui.item.value);
                $(".js_city_id").val(ui.item.id);
              },
              change: function(event, ui) {
                var cityName = $(".js_city_name").val();
                if (validator.isBlank(cityName)) {
                  $(".js_city_id").val(0);
                }
              },
              open: function(event, ui) {
                $(".js_city_id").val(0);
              }

            }).data("ui-autocomplete")._renderItem = function(ul, item) {
              return $("<li>")
                .append("<a>" + item.value + "</a>")
                .appendTo(ul);
            };

            // 初始化城市input的值
            if (r.data.data) {
              var cityId = $(".js_city_id").val();
              $.each(r.data.data, function(index, item) {
                cityNameMap[item.value] = item;
                cityIdMap[item.id] = item;

                if (cityId) {
                  if (cityId == item.id) {
                    $(".js_city_name").val(item.value);
                  }
                }
              });

            }

            var riderId = $(".js_city_id").val();
            $(".js_org_name").val(cityIdMap[riderId] ? cityIdMap[riderId].value : "");

          } else {
            alert("获取城市信息失败，" + r.data.msg);
          }
        } else {
          // alert('获取信息失败，系统错误');
        }
      });
    }

    // 根据/delivery/list页的操作来显示顶部的tabbar
    function initMenu() {
      $("#commonMenu li").click(function() {
        $("#alert_error").empty();
        var tipName = $(this).attr("value");
        $(this).addClass("active").siblings().removeClass("active");
        $("#" + tipName).show();
        $("#" + tipName).siblings().hide();
        if ("areaOrg" == tipName) {
          areaOrgData(1);
        } else if ("areaPoi" == tipName) {
          areaPoiData(1);
        } else if ("areaOPLog" == tipName) {
          areaOPLogData(1);
        }

        $('html,body').animate({
          scrollTop: 0
        }, 300);
      });

      $("#opSearch").click(function() {
        $("#commonMenu li[value=areaOPLog]").click();
      });

      // $("#resetZoomBtn").click(function() {
      //     window.location.reload();
      // });

      $(document).on("click", "#pagerDiv li a", function() {

        if ($(this).parent("li").hasClass("disabled")) {
          return false;
        }

        var functionName = $(this).attr("ref");
        eval(functionName);
      });

    }

    // 显示‘服务商家列表’
    function areaPoiData(pageNum) {
      var areaId = $("#id").val();
      if (typeof areaId == 'undefined' || areaId == '') {
        showError("无法获取区域id");
      }
      myPOST('/delivery/wmPoiList', {
        areaId: areaId,
        pageNo: pageNum
      }, function(r) {
        if (r.httpSuccess) {
          if (r.data && r.data.code == 0) {
            if (r.data.data) {
              var wmPoiList = r.data.data;
              var pageData = [];
              $.each(wmPoiList.merchantList, function(n, value) {
                pageData.push("<tr>");
                pageData.push("<td>" + value.merchantpoi + "</td>");
                pageData.push("<td>" + value.name + "</td>");
                pageData.push("<td>" + value.address + "</td>");
                pageData.push("<td>" + value.phone + "</td>");
                pageData.push("<td>" + value.ownerName + "</td>");

                if (value.valid == 1) {
                  pageData.push("<td>上线</td>");
                } else if (value.valid == 0) {
                  pageData.push("<td>下线</td>");
                }

                if (value.state == 1) {
                  pageData.push("<td>营业</td>");
                } else if (value.state == 3) {
                  pageData.push("<td>休息</td>");
                }

                pageData.push("</tr>");
              });
            }
            $("#merchantListBody").empty();
            $("#merchantListBody").append(pageData.join(''));
            showPager(pageNum, wmPoiList.pageSize, wmPoiList.recordCount, 6, wmPoiList.tipName);
          } else {
            showError(r.data.msg);
          }
        }

      });
    }

    // 显示‘操作记录’
    function areaOPLogData(pageNum) {
      var areaId = $("#id").val();
      if (typeof areaId == undefined || areaId == '') {
        showError("无法获取区域id");
      }

      var opDesc = $("#opDesc").val();
      var opUname = $("#opUname").val();
      var opType = $("#opType").val();
      var opStartTime = $("#opStartTime").val();
      var opEndTime = $("#opEndTime").val();

      var pattern = /^[\w\u4e00-\u9fa5$=:]+$/;
      if (typeof opDesc != undefined && opDesc != '' && !pattern.test(opDesc)) {
        showError("变更内容有非法字符");
        return false;
      }

      if (typeof opUname != undefined && opUname != '' && !validator.checkInput(opUname)) {
        showError("操作人有非法字符");
        return false;
      }

      if (opStartTime != '' && opEndTime != '') {
        if (opStartTime > opEndTime) {
          showError("开始时间不能大于结束时间");
          return false;
        }
      }


      myPOST('/delivery/areaLogList', {
        areaId: areaId,
        pageNo: pageNum,
        opDesc: opDesc,
        opUname: opUname,
        opType: opType,
        opStartTime: opStartTime,
        opEndTime: opEndTime

      }, function(r) {
        if (r.httpSuccess) {
          if (r.data && r.data.code == 0) {
            if (r.data.data) {
              var areaLogs = r.data.data;
              var pageData = [];
              $.each(areaLogs.deliveryLog, function(n, value) {
                pageData.push("<tr>");
                pageData.push("<td>" + value.id + "</td>");
                pageData.push("<td>" + value.opTypeName + "</td>");
                pageData.push("<td>" + value.description + "</td>");
                pageData.push("<td>" + value.op_uname + "</td>");
                pageData.push("<td>" + value.ctime + "</td>");
                pageData.push("</tr>");
              });
            }
            $("#areaLogBody").empty();
            $("#areaLogBody").append(pageData.join(''));
            showPager(pageNum, areaLogs.pageSize, areaLogs.recordCount, 6, areaLogs.tipName);
          } else {
            // alert("获取区域操作记录失败，" + r.data.msg);
            showError(r.data.msg);
            $("#areaLogBody").empty();
          }
        }
      });
    }

    // 显示‘站点列表’
    function areaOrgData(pageNum) {
      var areaId = $("#id").val();
      if (typeof areaId == 'undefined' || areaId == '') {
        showError("无法获取区域id");
      }
      myPOST('/delivery/areaOrgList', {
        areaId: areaId,
        pageNo: pageNum
      }, function(r) {
        if (r.httpSuccess) {
          if (r.data && r.data.code == 0) {
            if (r.data.data) {
              var areaOrgList = r.data.data;
              var pageData = [];
              $.each(areaOrgList.areaOrg, function(n, value) {
                pageData.push("<tr>");
                pageData.push("<td>" + value.bmOrgId + "</td>");
                pageData.push("<td><a href='/org/stations?orgId=" + value.bmOrgId + "'>" + value.bmOrgName + "</a></td>");
                pageData.push("<td>" + value.bmOrgTypeName + "</td>");
                pageData.push("<td>" + value.bmCityName + "</td>");
                pageData.push("<td>" + value.parentName + "</td>");
                pageData.push("</tr>");
              });
            }
            $("#areaOrgBody").empty();
            $("#areaOrgBody").append(pageData.join(''));
            showPager(pageNum, areaOrgList.pageSize, areaOrgList.recordCount, 6, areaOrgList.tipName);
          } else {
            showError(r.data.msg);
          }
        }
      });
    }

    // ‘站点列表’‘服务商家列表’‘操作记录’页面的主要功能就是table，这里负责table分页
    function showPager(pageNum, pageSize, total, existPageCount, divId) {
      //设置分页信息
      var totalPage = parseInt((total + pageSize - 1) / pageSize);

      if (pageNum < 1) {
        pageNum = 1;
      }
      if (pageNum > totalPage) {
        pageNum = totalPage;
      }

      var functionName = divId + "Data";

      var leftNum = pageNum - (existPageCount % 2 == 0 ? existPageCount / 2 - 1 : existPageCount / 2);
      var rightNum = pageNum + existPageCount / 2;
      if (leftNum < 1) {
        leftNum = 1;
        if (totalPage >= existPageCount) {
          rightNum = existPageCount;
        } else {
          rightNum = totalPage;
        }
      }
      if (rightNum > totalPage) {
        rightNum = totalPage;
        if ((rightNum - existPageCount) > 0) {
          leftNum = rightNum - existPageCount + 1;
        } else {
          leftNum = 1;
        }
      }

      if (totalPage > 1) {
        var pageHtml = '<nav style="float:right;">' +
          '  <ul id="pagerDiv" class="pagination">';
        pageHtml += '<li';
        if (pageNum == 1) {
          pageHtml += ' class="disabled"';
        }

        pageHtml += ' page=' + (pageNum - 1) + '><a href="#" ref=\'' + functionName + '(' + (pageNum - 1) + ')\'' + ' aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
        for (var i = leftNum; i <= rightNum; i++) {
          if (i == pageNum) {
            pageHtml += '<li class="active"><a href="#" ref=\'' + functionName + '(' + i + ')\'' + '>' + i + ' <span class="sr-only">(current)</span></a></li>';
          } else {
            pageHtml += '<li page=' + i + '><a href="#" ref=\'' + functionName + '(' + i + ')\'' + '>' + i + '</a></li>';
          }
        }
        pageHtml += '<li';
        if (pageNum == totalPage) {
          pageHtml += ' class="disabled"';
        }
        pageHtml += ' page=' + (pageNum + 1) + '><a href="#" ref=\'' + functionName + '(' + (pageNum + 1) + ')\'' + ' aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
        pageHtml += '  </ul></nav>';

        $("#" + divId + "Pager").empty().append(pageHtml);
      } else {
        $("#" + divId + "Pager").empty();
      }
    }

    // 根据角色的类型初始化区域选择的radio-group
    function initAreaType() {

      // 使用属性选择符，后面可以统一优化
      // orgType标识用户的身份，pageType标识当前选中的区域类型
      var selector = '#area_type input[value!=' + orgType + ']';
      if (orgType > 0) {
        $(selector).parent().hide();
      }



      $('#area_type').prop('data-val', pageType);
      // 如果pageType > 0 标明两种情况
      // 1 - a端编辑页面
      // 2 - p端新建或者编辑页面
      if (pageType > 0) {

        // countBindBmOrg代表绑定的站点数，如果已经绑定站点，
        // 只显示当前绑定的区域类型
        var countBindBmOrg = $("#countBindBmOrg").val();
        if (countBindBmOrg && countBindBmOrg > 0) {
          selector = '#area_type input[value!=' + pageType + ']';
          $(selector).parent().hide();
        }
        $('#area_type input[value=' + pageType + ']').prop('checked', 'true');
        changeCheckboxStatus();
        changeDispatchStragery();
      }

      $('#area_type input').on('change', function() {
        changeCheckboxStatus();
        changeDispatchStragery();
        checkShowIntersect();
      });
    }


    // 切换配送区域类型时，检测是否重合
    function checkShowIntersect() {
      var oldVal = +($('#area_type').prop('data-val'));
      var type = +($('#area_type input:checked').val());
      if (oldVal == type) {
        return;
      }
      // 如果没有绘制地图，那么直接返回
      if (!polygon) {
        return;
      }

      var intersect = $("#intersect").val();
      var allowIntersect = 1;
      var initType = $("#initType").val();
      if (typeof initType == 'undefined' || initType == '') {
        return;
      }
      if (allowIntersect != intersect) {
        var isInNoIntersect = false;
        var noIntersectLen = !noIntersectAreaType ? 0 : noIntersectAreaType.length;

        for (var i = 0; i < noIntersectLen; i++) {
          if (type == noIntersectAreaType[i]) {
            isInNoIntersect = true;
          }
        }

        if (isInNoIntersect) {
          // 非众包区域重合判断
          for (var i = 0; i < noIntersectLen; i++) {
            if (checkIsIntersect(polygon.getPath(), allExistAreasMap[noIntersectAreaType[i]])) {
              var targetType = areaMapping[noIntersectAreaType[i]];
              showTypeChangeIntersectModal(targetType);
              return;
            }
          }
        } else {
          // 众包区域重合判断
          if (checkIsIntersect(polygon.getPath(), allExistAreasMap[type])) {
            var targetType = areaMapping[type];
            showTypeChangeIntersectModal(targetType);
            return;
          }
        }
      }
    }

    // 点击不同的区域类型时，需要调整各个显示checkbox的可控制性
    function changeCheckboxStatus() {
      var noZhongPaoArr = [1, 2, 7];

      var selectedType = +($('#area_type input:checked').val());
      // 重置状态
      $('.area_type').prop('disabled', '');
      $('.area_type').parent().removeClass('disabled');
      $('.area_type').parent().attr('data-original-title', '');

      if (noZhongPaoArr.includes(selectedType)) {
        // 非众包时
        noZhongPaoArr.forEach(function (item) {
          var selctor = '.area_type[value='+ item + ']';
          $(selctor).prop('disabled', 'true')
          .parent().addClass('disabled')
          .attr('data-original-title', '当前区域类型不可取消展示');
        });

      } else if (selectedType == 4) {
        // 众包时
        $('.area_type[value=4]').prop('disabled', 'true')
        .parent().addClass('disabled')
        .attr('data-original-title', '当前区域类型不可取消展示');
      }
    }

    // 需要发送ajax请求动态从后台获取
    function changeDispatchStragery() {

      var type = $('#area_type input:checked').val();
      var obj = {
        type: 'post',
        url: "/delivery/dispatchstrategy/list",
        data: {
          type: type
        }
      };
      if (type > 0) {
        $.ajax(obj)
        .done(function(r) {
          if (r && r.code == 0) {
            if (r.data) {
              // 先清空整个派送方式列表
              $('#dispatch_mode').empty();
              var initDispatchStrategy = $("#initDispatchStrategy").val();
              var wholeHtml = "";

              // 统一一次添加的效率较高
              $.each(r.data, function(index, value) {
                var startFrag = '<label class="radio-inline" style="margin-left:10px;">' +
                  '<input type="radio" name="inlineRadioOptionsD" value="' + value.id;
                var endFrag = '>' + value.name + '</label>';
                var endFrag1 = '">' + value.name + '</label>';

                // 处理选中状态
                var htmlFrag = initDispatchStrategy == value.id || r.data.length == 1? startFrag + '" checked' + endFrag :
                  startFrag + endFrag1;
                // console.log('调度策略的radio html fragment: ', htmlFrag);
                wholeHtml += htmlFrag;
              });
              $('#dispatch_mode').append(wholeHtml);
            } else {
              // $("#dispatchStrategy").attr("disabled", "disabled");
              $('#dispatch_mode').empty();
              $('#dispatch_mode').append('<label style="margin-left:10px;" >无相应调度模式</label>');
            }

            // TODO 弄清这里的业务逻辑
            var countBindBmOrg = $("#countBindBmOrg").val();
            // 如果只有一个的话就不需要diable了
            if (countBindBmOrg && countBindBmOrg > 1 && $('#dispatch_mode input').length > 1) {
              $("#dispatch_mode input").prop('disabled', true);
              $("#dispatch_mode input").parent().addClass('disabled');
            }
          }
        })
        .fail(function() {
          console.error('无法获取调度策略');
        });
      }
    }

    function submitCheckDialog(title, content) {
      $('#commitCityCheckModal h4').html(title);
      $('#commitCityCheckModal .modal-body').html(content);
      $('#commitCityCheckModal').modal();
    }

    function showCheckIntersectDialog(sourceType, existingType) {
      var content = $('#commitIntersectModal .modal-body').html();

      content = content.replace(/####/, sourceType).replace(/%%%%/, existingType);
      $('#commitIntersectModal .modal-body').html(content);
      $('#commitIntersectModal').modal();
    }

    // 区域类型更改后，有可能出现提示dialog
    function initTypeChangeIntersectModal() {
      $('#confirmTypeChangeIntersectRange').on('click', function () {
        $('#typeChangeIntersectModal').modal('hide');
        resetEditingMap();
      });

      // 取消的话还原原来选择的type值
      $('#cancelTypeChangeIntersectRange').on('click', function () {
        $('#typeChangeIntersectModal').modal('hide');
        var oldTypeVal = $('#area_type').prop('data-val');
        $('#area_type input[value=' + oldTypeVal + ']').prop('checked', 'true');
      });
    }

    function initCommitIntersectModal() {
      $('#commitIntersectModalKnown').on('click', function () {
        $('#commitIntersectModal').modal('hide');
      });
    }

    function initCommitCityCheckModal() {
      $('#commitCityCheckModalKnown').on('click', function () {
        $('#commitCityCheckModal').modal('hide');
      });
    }

    function showTypeChangeIntersectModal(intersectTypeStr) {
      var def = $.Deferred();
      var $dialog = $('#typeChangeIntersectModal');
      var content = "当前范围与现存%%%%区域有重合，如需更改将重置当前已绘制的范围，确定更改吗？"
      content = content.replace(/%%%%/ig, intersectTypeStr);
      $dialog.find('.modal-body').html(content);
      $dialog.find('.btn-success').one('click', function() {
        def.resolve();
        $dialog.modal('hide');
      });
      $dialog.find('.btn-danger, .close').one('click', function() {
        def.reject();
        $dialog.modal('hide');
      });
      $dialog.modal();
      return def.promise();
    }

    /*
     * // TODO 未来可以更好的封装类似的调用和需求
     *一个使用JQuery Promise&Deferred封装的Modal调用示例
     * 避免分散的管理一个modal的各个部分
     * 返回一个Promise对象以支持链式调用
     *
     */
    function showResetMapModal() {
      var def = $.Deferred();
      // 注意命名方式，加上$是为了标识其是一个JQuery对象
      var $dialog = $('#resetMapModal');
      $dialog.modal();
      $dialog.find('#confirmResetMap').one('click', function() {
        def.resolve();
        $dialog.modal('hide');
      })

      $dialog.find('#cancelResetMap, .close').one('click', function() {
        def.reject();
        $dialog.modal('hide');
      });
      return def.promise();
    }

    function openMapEditor() {
      if (!isOrgManager || isOrgManager != 1) {
        editorTool.open();

        if ($('#B_start_edit_area').attr('rel') == '0' && shippingEditorTool) {
          shippingEditorTool.open();
        }
      }
    }

  });
