require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu',
        ],
        'lib/autocomplete' : {
            deps : ['page/common']
        },
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});
require(['module/root', 'module/validator',  'module/cookie', 'lib/autocomplete'], function (t, validator, cookie, autocomplete) {


var orgNameMap = {};
var orgIdMap = {};
var cityNameMap = {};
var cityIdMap = {};
var id;
var oldOrgId;
var oldOrgName;
var areaNameMap = {};
var areaIdMap = {};
var map;
$(document).ready(function(){
	initSearchBtn();
    initSearchArea();
});

function initSearchBtn() {
	$("#search").click(function() {
        
        var areaId = $("#id").val().trim();
        var areaName = $("#name").val().trim();
        if(areaId == 0 && areaName != "") {
          showError("请填写正确的区域名称");
          return false;
        }
        if(areaId == "") {
          showError("请填写区域名称");
          return false;
        }

	    $("#formValidate1").submit();
	});
}

function initSearchArea() {

    myPOST('/org/areaSearchList', {}, function (r) {
         if (r.httpSuccess) {
            if(r.data && r.data.code == 0) {     
                $(".js_area_name").autocomplete({
                    delay:100,
                    source : r.data.data,
                    focus: function( event, ui ) {
                        $( ".js_area_name" ).val( ui.item.value );
                        $( ".js_area_id" ).val( ui.item.id );
                        return false;
                    },
                    select: function(event, ui) {

                        $(".js_area_name").val(ui.item.value );
                        $(".js_area_id").val(ui.item.id );
                    },
                    change: function(event, ui) {
                        var areaName = $(".js_area_name").val();
                        if(validator.isBlank(areaName)) {
                            $(".js_area_id").val(0);
                        }
                    },
                    open: function(event, ui) {
                        $(".js_area_id").val('0');
                    }

                }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                    return $( "<li>" )
                        .append( "<a>" + item.value + "</a>" )
                        .appendTo( ul );
                };
                if(r.data.data) {
                    $.each(r.data.data, function(index, item) {
                    	areaNameMap[item.value] = item;
                    	areaIdMap[item.id] = item;
                    });
                    
                }

                var riderId = $(".js_area_id").val();

            } else {
                alert("获取区域信息失败，" + r.data.msg);
            }
        } 
    });
}

function myPOST(url, data, callback)  // 发送POST请求
            {
                if(typeof(data) == 'function') { callback = data; data = null; }
                $.ajax({ type: 'POST', url: url, data:data, success:function(r) {

                    callback({ httpSuccess: true, data: r });

                }, error: function(XmlHttpRequest, textStatus, errorThrown) {

                    callback({ httpSuccess: false, statusCode:XmlHttpRequest.status });

                } });
            }


                function isBlank(value) {
        if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
                return this.replace(/^\s+|\s+$/g, '');
            }
        }
        return !_valueExist(value) || value.trim() == "";
    }

function showError(errMsg) {
    $("#alert_error").empty();
    $("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

});
