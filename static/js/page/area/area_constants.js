/**
 * User: liyishan
 * Date: 16-1-28
 * 定义绘制配送区域地图所用的各种常量
 */
define(function () {
  var areaMapping = {
    1 : '自建',
    2 : '加盟',
    4 : '众包',
    7 : '城市代理'
  };

  // 建立不同类型的区域polygon映射，便于统一维护
  var areaPolygonMap = {
    0 : {
      zIndex: 200,
      strokeColor: "#878787",
      strokeOpacity: 1,
      strokeWeight: 2,
      fillColor: "#7CCD7C",
      fillOpacity: 0.35
    },
    1 : {
      zIndex: 200,
      strokeColor: "#f8c832",
      strokeOpacity: 1,
      strokeWeight: 2,
      fillColor: "#f8c832",
      fillOpacity: 0.35
    },
    2 : {
      zIndex: 200,
      strokeColor: "#06944b",
      strokeOpacity: 1,
      strokeWeight: 2,
      fillColor: "#06944b",
      fillOpacity: 0.35
    },
    4 : {
      zIndex: 200,
      strokeColor: "#b56fe7",
      strokeOpacity: 1,
      strokeWeight: 2,
      fillColor: "#b56fe7",
      fillOpacity: 0.35
    },
    7 : {
      zIndex: 200,
      strokeColor: "#6c553c",
      strokeOpacity: 1,
      strokeWeight: 2,
      fillColor: "#6c553c",
      fillOpacity: 0.35
    }
  };

  var editingAreaPolygonOptions = {
    strokeColor: "#4788ee",
    strokeOpacity: 1,
    strokeWeight: 2,
    fillColor: "#4788ee",
    zIndex: 201,
    fillOpacity: 0.35
  };

  var editingShippingAreaPolygonOptions = {
    strokeColor: "#4788ee",
    strokeOpacity: 1,
    strokeWeight: 2,
    fillColor: "#0f5bb0",
    strokeStyle: "dashed", //线样式
    zIndex: 100,
    fillOpacity: 0.2
  };



  return {
    AREA_POLYGON_MAP : areaPolygonMap,
    AREA_TYPE_MAP : areaMapping,
    EDITING_AREA_POLY_OPTS : editingAreaPolygonOptions,
    EDITING_SHIPPING_AREA_POLY_OPTS : editingShippingAreaPolygonOptions
  };
});
