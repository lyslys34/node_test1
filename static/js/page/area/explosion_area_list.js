require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu',
        ],
        'lib/autocomplete' : {
            deps : ['page/common']
        },
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});
require(['module/root', 'module/validator',  'module/cookie', 'lib/autocomplete'], function (t, validator, cookie, autocomplete) {


var orgNameMap = {};
var orgIdMap = {};
var cityNameMap = {};
var cityIdMap = {};
var id;
var oldOrgId;
var oldOrgName;
var areaNameMap = {};
var areaIdMap = {};
$(document).ready(function(){
	initDeleteBtn();
	initSearchOrg();
    initSearchCity();
	initSearchBtn();
	initBindOrgBtn();
    initSearchBindOrg();
    initBindOrgSubmitBtn();
    initcancelBindOrgBtn();
    initSearchArea();
    initUnBindOrgModal();
});

function initDeleteBtn() {
	$(".delete_link").click(function() {
	    var actionUrl = $(this).attr('rel');
	    $("#deleteform").attr("action", actionUrl);
	    $("#deleteModal").modal();
	});
}

function initBindOrgBtn() {
    $(".bind_link").click(function() {
        id = $(this).attr('rel');
        oldOrgId = $(this).parents("td").prevAll(".orgId").val();
        oldOrgName = $(this).parents("td").prevAll(".orgName").text();
        if(oldOrgId && oldOrgId != "" && oldOrgName && oldOrgName != "") {
            $("#bindOrgId").val(oldOrgId.trim());
            $("#bindOrgName").val(oldOrgName.trim());
        }

        $("#alert_bind_error").empty();

        $("#bindOrgModal").modal();

    });
}

function initcancelBindOrgBtn() {
    $('#cancelBindOrg').on('click', function() {
        $("#bindOrgId").val('');
        $("#bindOrgName").val('');
        $("#bindOrgModal").modal('hide');
    });
}

function bindOrg() {
//		var orgId = $("#bindOrgId").val().trim();
//        var orgName = $("#bindOrgName").val().trim();
        var explosionLevel = $("#explosionLevel").val();
//        if(typeof id == 'undefined') {
//            showBindError("获取区域错误");
//            return ;
//        }
//        if(orgId == 0 && orgName != "") {
//          showBindError("请填写正确的组织名称");
//          return;
//        }
//        if(orgId==0 && orgName==""){
//            $("#bindOrgModal").modal('hide');
//            $("#unBindOrgModal").modal();
//            // initUnBindOrgModal(id,oldOrgId);
//            return;
//        }
//        if(orgId == "") {
//          showBindError("请填写组织名称");
//          return;
//        }
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/deliveryExplosion/modifyLevel",
            data: {
                  id: id,
                level: explosionLevel
            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4>绑定爆单处理级别成功</h4>");
                    $("#bindOrgModal").modal('hide');
                    $("#showBindRes").modal();
                    setTimeout('$("#search").click()',2000);
                    window.location.reload();
                }else {
                    showBindError(data.errMsg);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });
    
}

function initBindOrgSubmitBtn() {
    $('#bindOrgSubmit').on('click', function(){
        bindOrg();
      });  
}

function initUnBindOrgModal(){
    $('#unBindOrgSubmit').on('click',function(){
        if(oldOrgId==null){
            $("#search").click();
        }else{
           $.ajax({
                               dataType: 'json',
                               type : 'post',
                               url : "/delivery/unBindOrg",
                               data: {
                                     id: id,
                                     orgId:oldOrgId
                               },
                               success : function(data){
                                   if(data.success){
                                       $("#resMsg").html("<h4>解除绑定组织成功</h4>");
                                       $("#unBindOrgModal").modal('hide');
                                       $("#showBindRes").modal();
                                       setTimeout('$("#search").click()',2000);
                                   }else {
                                       alert(data.errMsg);
                                   }
                               },
                               error:function(XMLHttpRequest ,errMsg){
                                   alert("网络连接失败");
                               }
                           });
        }

    })
}

function initSearchBtn() {
	$("#search").click(function() {
        var cityId = $("#cityId").val().trim();
        var cityName = $("#cityName").val().trim();
        if(cityId == 0 && cityName != "") {
          showError("请填写正确的城市名称");
          return false;
        }
        if(cityId == "") {
          showError("请填写城市名称");
          return false;
        }

        var orgId = $("#orgId").val().trim();
        var orgName = $("#orgName").val().trim();
        if(orgId == 0 && orgName != "") {
          showError("请填写正确的组织名称");
          return false;
        }
        if(orgId == "") {
          showError("请填写组织名称");
          return false;
        }
        
        var areaId = $("#id").val().trim();
        var areaName = $("#name").val().trim();
        if(areaId == 0 && areaName != "") {
          showError("请填写正确的区域名称");
          return false;
        }
        if(areaId == "") {
          showError("请填写区域名称");
          return false;
        }

	    $("#formValidate1").submit();
	});
}

function initSearchOrg() {

  	myPOST('/org/leafOrgSearchList', {}, function (r) {
         if (r.httpSuccess) {
            if(r.data && r.data.code == 0) {          
                $(".js_org_name").autocomplete({
                    delay:100,
                    source : r.data.data,
                    focus: function( event, ui ) {
                        $( ".js_org_name" ).val( ui.item.value );
                        $( ".js_org_id" ).val( ui.item.id );
                        return false;
                    },
                    select: function(event, ui) {

                        $(".js_org_name").val(ui.item.value );
                        $(".js_org_id" ).val( ui.item.id );
                    },
                    change: function(event, ui) {
                        var orgName = $(".js_org_name").val();
                        if(validator.isBlank(orgName)) {
                            $(".js_org_id").val(0);
                        } 
                    },
                    open: function(event, ui) {
                        $(".js_org_id").val('0');
                    }

                }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                    return $( "<li>" )
                        .append( "<a>" + item.value + "</a>" )
                        .appendTo( ul );
                };
                if(r.data.data) {
                    var orgId = $(".js_org_id").val();
                    $.each(r.data.data, function(index, item) {
                        orgNameMap[item.value] = item;
                        orgIdMap[item.id] = item;
                        if(orgId) {
                          if(orgId == item.id) {
                            $(".js_org_name").val(item.value);
                          }
                        }
                    });   
                }

                var riderId = $(".js_org_id").val();
                $(".js_org_name").val(orgIdMap[riderId] != null && typeof orgIdMap[riderId] != 'undefined' ? orgIdMap[riderId].value : "");

            } else {
                alert("获取组织信息失败，" + r.data.msg);
            }
        } else {
            //alert(JSON.stringify(r));
            // alert('获取组织信息失败，系统错误');
        }
    });
}

function initSearchBindOrg() {

    myPOST('/org/leafOrgSearchList', {}, function (r) {
         if (r.httpSuccess) {
            if(r.data && r.data.code == 0) {          
                $(".js_bind_org_name").autocomplete({
                    delay:100,
                    source : r.data.data,
                    focus: function( event, ui ) {
                        $( ".js_bind_org_name" ).val( ui.item.value );
                        $( ".js_bind_org_id" ).val( ui.item.id );
                        return false;
                    },
                    select: function(event, ui) {

                        $(".js_bind_org_name").val(ui.item.value );
                        $(".js_bind_org_id" ).val( ui.item.id );
                    },
                    change: function(event, ui) {
                        var orgName = $(".js_bind_org_name").val();
                        if(validator.isBlank(orgName)) {
                            $(".js_bind_org_id").val(0);
                        } 
                    },
                    open: function(event, ui) {
                        $(".js_bind_org_id").val('0');
                    }

                }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                    ul.css("z-index", 2000);
                    return $( "<li>" )
                        .append( "<a>" + item.value + "</a>" )
                        .appendTo( ul );
                };
                if(r.data.data) {
                    $.each(r.data.data, function(index, item) {
                        orgNameMap[item.value] = item;
                        orgIdMap[item.id] = item;
                    });
                }
                var riderId = $(".js_bind_org_id").val();
                $(".js_bind_org_name").val(orgIdMap[riderId] != null && typeof orgIdMap[riderId] != 'undefined' ? orgIdMap[riderId].value : "");

            } else {
                alert("获取组织信息失败，" + r.data.msg);
            }
        } else {
            //alert(JSON.stringify(r));
            // alert('获取组织信息失败，系统错误');
        }
    });
}

function initSearchCity() {

    myPOST('/org/citySearchList', {}, function (r) {
         if (r.httpSuccess) {
            if(r.data && r.data.code == 0) {     
                $(".js_city_name").autocomplete({
                    delay:100,
                    source : r.data.data,
                    focus: function( event, ui ) {
                        $( ".js_city_name" ).val( ui.item.value );
                        $( ".js_city_id" ).val( ui.item.id );
                        return false;
                    },
                    select: function(event, ui) {

                        $(".js_city_name").val(ui.item.value );
                        $(".js_city_id").val(ui.item.id );
                    },
                    change: function(event, ui) {
                        var cityName = $(".js_city_name").val();
                        if(validator.isBlank(cityName)) {
                            $(".js_city_id").val(0);
                        }
                    },
                    open: function(event, ui) {
                        $(".js_city_id").val('0');
                    }

                }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                    return $( "<li>" )
                        .append( "<a>" + item.value + "</a>" )
                        .appendTo( ul );
                };
                if(r.data.data) {
                    var cityId = $(".js_city_id").val();
                    $.each(r.data.data, function(index, item) {
                        cityNameMap[item.value] = item;
                        cityIdMap[item.id] = item;
                        if(cityId) {
                          if(cityId == item.id) {
                            $(".js_city_name").val(item.value);
                          }
                        }
                    });
                    
                }

                var riderId = $(".js_city_id").val();
                $(".js_city_name").val(cityIdMap[riderId] != null && typeof cityIdMap[riderId] != 'undefined' ? cityIdMap[riderId].value : "");

            } else {
                alert("获取组织信息失败，" + r.data.msg);
            }
        } else {
            //alert(JSON.stringify(r));
            // alert('获取组织信息失败，系统错误');
        }
    });
}

function initSearchArea() {

    myPOST('/org/areaSearchList', {}, function (r) {
         if (r.httpSuccess) {
            if(r.data && r.data.code == 0) {     
                $(".js_area_name").autocomplete({
                    delay:100,
                    source : r.data.data,
                    focus: function( event, ui ) {
                        $( ".js_area_name" ).val( ui.item.value );
                        $( ".js_area_id" ).val( ui.item.id );
                        return false;
                    },
                    select: function(event, ui) {

                        $(".js_area_name").val(ui.item.value );
                        $(".js_area_id").val(ui.item.id );
                    },
                    change: function(event, ui) {
                        var areaName = $(".js_area_name").val();
                        if(validator.isBlank(areaName)) {
                            $(".js_area_id").val(0);
                        }
                    },
                    open: function(event, ui) {
                        $(".js_area_id").val('0');
                    }

                }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                    return $( "<li>" )
                        .append( "<a>" + item.value + "</a>" )
                        .appendTo( ul );
                };
                if(r.data.data) {
                    $.each(r.data.data, function(index, item) {
                    	areaNameMap[item.value] = item;
                    	areaIdMap[item.id] = item;
                    });
                    
                }

                var riderId = $(".js_area_id").val();

            } else {
                alert("获取区域信息失败，" + r.data.msg);
            }
        } else {
            //alert(JSON.stringify(r));
            // alert('获取区域信息失败，系统错误');
        }
    });
}

function myPOST(url, data, callback)  // 发送POST请求
            {
                if(typeof(data) == 'function') { callback = data; data = null; }
                $.ajax({ type: 'POST', url: url, data:data, success:function(r) {

                    callback({ httpSuccess: true, data: r });

                }, error: function(XmlHttpRequest, textStatus, errorThrown) {

                    callback({ httpSuccess: false, statusCode:XmlHttpRequest.status });

                } });
            }


                function isBlank(value) {
        if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
                return this.replace(/^\s+|\s+$/g, '');
            }
        }
        return !_valueExist(value) || value.trim() == "";
    }

function showError(errMsg) {
    $("#alert_error").empty();
    $("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

function showBindError(errMsg) {
    $("#alert_bind_error").empty();
    $("#alert_bind_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

});
