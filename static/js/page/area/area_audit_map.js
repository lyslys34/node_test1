require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu'
    ],
    'lib/autocomplete' : {
      deps : ['page/common']
    },
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root', 'module/validator',  'module/cookie', 'lib/autocomplete', 'page/area/area_util'], function (t, validator, cookie, autocomplete, util) {

  var map;
  var editorTool;
  var id;
  var allExistAreasMap = {0:[], 1:[],3:[], 2:[], 4:[], 5:[]};
  var noIntersectAreaType = [0, 1, 2];

  $(document).ready(function(){
   initialize();

   var cityId = $("#cityId").val();
   if (typeof cityId != 'undefined' && cityId !='') {
      getExistArea(cityId);
   }

   initAddBtn();
   initCancelBtn();
 });

function initialize() {


    id = $("#id").val().trim();
    var originArea = $("#originArea").val().trim();
    var auditArea = $("#auditArea").val().trim();

	//初始化地图对象，加载地图
    map = new AMap.Map("mapContainer", {
      resizeEnable: true,
      view: new AMap.View2D({
       resizeEnable: true,
              zoom:14//地图显示的缩放级别
            }),
      keyboardEnable:false
    });

	   //在地图中添加ToolBar插件
  	map.plugin(["AMap.ToolBar"],function(){		
  		toolBar = new AMap.ToolBar();
      //		toolBar.setOffset(new AMap.Pixel(10,50));
      map.addControl(toolBar);		
    });
	
  	//加载比例尺插件
  	map.plugin(["AMap.Scale"], function(){		
  		scale = new AMap.Scale();
  		map.addControl(scale);
  	});
  	
  	if (originArea != null && originArea != "") {
      
  		var arr = new Array(); //构建多边形经纬度坐标数组   
  		//json解析之前保存的配送区域originArea
  		var areas = JSON.parse(originArea);
      for (var i in areas) {
        arr.push(new AMap.LngLat(areas[i].y, areas[i].x));
      }

       polygon = new AMap.Polygon({
         map: map,
         path: arr,
         strokeColor: "#CC0000",
         strokeOpacity: 1,
         strokeWeight: 3,
         fillColor: "#FF3333",
         zIndex: 201,
         fillOpacity: 0.35
       });

       map.setZoomAndCenter(14, new AMap.LngLat(areas[0].y, areas[0].x));
    } 

    if (auditArea != null && auditArea != "") {
      
      var arr = new Array(); //构建多边形经纬度坐标数组   
      //json解析之前保存的配送区域originArea
      var areas = JSON.parse(auditArea);
      for (var i in areas) {
        arr.push(new AMap.LngLat(areas[i].y, areas[i].x));
      }

       polygon = new AMap.Polygon({
         map: map,
         path: arr,
         strokeColor: "#CC0000",
         strokeOpacity: 1,
         strokeWeight: 3,
         fillColor: "#FF3333",
         zIndex: 201,
         fillOpacity: 0.35
       });
      addAreaChangeListener(polygon);
      $("#area").val(polygon.getPath());
      //添加编辑控件
      // map.plugin(["AMap.PolyEditor"], function() {
      //   // editorTool = new AMap.PolyEditor(map, polygon);   
      //   // editorTool.open();
      // });
      map.setZoomAndCenter(14, new AMap.LngLat(areas[0].y, areas[0].x));
      var lineArr = polygon.getPath();
      lineArr.push(lineArr[0]);
      var polyline = new AMap.Polyline({
        path: lineArr
      });
      showAreaLength(polyline.getLength());
      polyline.setPath(lineArr.pop());
    } 

   AMap.event.addListener(map, 'moveend',function() {
    if(editorTool) {
      // editorTool.open();
    }
  });
 }

 function addAreaChangeListener(thisPolygon) {
    AMap.event.addListener(thisPolygon, 'change',function() {
      var lineArr = polygon.getPath();
      lineArr.push(lineArr[0]);
      var polyline = new AMap.Polyline({
        path: lineArr
      });

      showAreaLength(polyline.getLength());
      polyline.setPath(lineArr.pop());
  });
 }




function initCancelBtn() {
  $("#B_cancel").on("click", function(){
    window.location.reload();
  });
}

function initAddBtn() {
  $('#B_checked').click(function(){
        // var type = $("#type").val();
        // var noIntersectLen = typeof noIntersectAreaType == 'undefined' ? 0 : noIntersectAreaType.length;
        // var isInNoIntersect = false;
        // for (var i=0;i<noIntersectLen;i++) {
        //   if (type == noIntersectAreaType[i]) {
        //     isInNoIntersect = true;
        //   }
        // }

        // if (isInNoIntersect) {
        //   for (var i=0;i<noIntersectLen;i++) {
        //     if ( true == checkIsIntersect(polygon.getPath(), allExistAreasMap[noIntersectAreaType[i]])) {
        //       showError("配送区域不能重叠");
        //       return false;
        //     }
        //   }
        // } else {
        //   if ( true == checkIsIntersect(polygon.getPath(), allExistAreasMap[type])) {
        //     showError("配送区域不能重叠");
        //     return false;
        //   }
        // }
      $("#saveInfoModal").modal();
  });

  $("#B_reject").click(function() {
      $("#editRejectReasonModal").modal();
  });
  $("#B_continue_save").click(function() {
      var checked = $(this).attr("rel");
      saveData(checked, "");
  });
  $("#B_cancel_save").click(function() {
      $("#saveInfoModal").modal("hide");
  });

  $("#editRejectReasonSubmit").click(function() {
      var checked = $(this).attr("rel");
      var rejectReason = $("#reject_reason").val();
      if (validator.isBlank(rejectReason)) {
        showEditRejectReasonError("驳回原因不能为空");
        return false;
      }

      saveData(checked, rejectReason);
  });
}

function saveData(checked, rejectReason) {
  var id = $("#id").val();
  $.ajax({
      dataType: 'json',
      type : 'post',
      url : "/delivery/audit/auditArea",
      data: {
       id: id,
       checked: checked, 
       rejectReason: rejectReason
     },
     success : function(data){
      if(data.code == 0){
        $("#editRejectReasonModal").modal("hide");
        showSaveSuccess();
      }else {
       showEditRejectReasonError(data.errMsg);
     }
     },
     error:function(XMLHttpRequest ,errMsg){
      alert("网络连接失败");
                  // alert(errMsg);
     }
     });
}

function getExistArea(cityId) {
  myPOST('/delivery/existAreas', {cityId : cityId}, function (r) {
    if (r.httpSuccess) {
      if(r.data && r.data.code == 0) {
        if(r.data.data) {
          existAreas = r.data.data;
          if(existAreas.data) {
            if(existAreas.data.data) {

              var areaId = $("#areaId").val();
              $.each(existAreas.data.data, function(n, value) {
                if (!value.deliveryAreaCoordinates || value.deliveryAreaCoordinates == '') {
                  return ;
                }
                if(typeof areaId == 'undefined' || areaId == '' || (areaId && areaId != value.id)) {
                  var type = value.type;
                  if(type > -1 && typeof allExistAreasMap[type] != 'undefined') {
                    allExistAreasMap[type].push(JSON.parse(value.deliveryAreaCoordinates));

                    if (value.checked != 2 && value.auditDeliveryAreaCoordinates && value.auditDeliveryAreaCoordinates != '') {
                      allExistAreasMap[type].push(JSON.parse(value.auditDeliveryAreaCoordinates))
                    }
                  }
                } 

                var arr = new Array(); //构建多边形经纬度坐标数组   
                // if (!value.deliveryAreaCoordinates || value.deliveryAreaCoordinates == '') {
                //   return ;
                // }
                var areas = JSON.parse(value.deliveryAreaCoordinates);

                for (var i in areas) {
                  arr.push(new AMap.LngLat(areas[i].y, areas[i].x));
                }
                var polygon2 = new AMap.Polygon({
                  map: map,
                  path: arr,
                  zIndex: 200,
                  strokeColor: "#0000FF",
                  strokeOpacity: 1,
                  strokeWeight: 3,
                  fillColor: "#00FFFF",
                  fillOpacity: 0.35
                });

              });      
            }
          }     
        }
      } else {
        alert("获取已有配送区域失败，" + r.data.msg);
      }
    } 
  });
}

function checkIsIntersect(polygonPoint, existAreaPolygons) {
  var polygon1 = new Array();
  if (polygonPoint) {
    for (var i = 0; i < polygonPoint.length; i++) {
      var point = {
        "x" : polygonPoint[i].getLat(),
        "y" : polygonPoint[i].getLng()
      };
      polygon1.push(point);
    }

    for (var i = 0; i < existAreaPolygons.length; i++) {
      if (existAreaPolygons[i] == null) {
        continue;
      }

      if (true == util.checkIntersect(polygon1, existAreaPolygons[i])) {
        return true;
      }
    }
  }

  return false;
}

function myPOST(url, data, callback)  // 发送POST请求
{
  if(typeof(data) == 'function') { callback = data; data = null; }
  $.ajax({ type: 'POST', url: url, data:data, success:function(r) {

    callback({ httpSuccess: true, data: r });

  }, error: function(XmlHttpRequest, textStatus, errorThrown) {

    callback({ httpSuccess: false, statusCode:XmlHttpRequest.status });

  } });
}


function showError(errMsg) {
	$("#alert_error").empty();
	$("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

function showEditRejectReasonError(errMsg) {
  $("#alert_edit_reject_reason_error").empty();
  $("#alert_edit_reject_reason_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

function showAreaLength(length) {
  var length = Number(Number(length) / 1000).toFixed(5);
  $("#alert_area_length").empty();
  $("#alert_area_length").append("<div class='label label-success' role='label'> 已绘制 " + length + " 公里</div>");
}

function showSaveSuccess() {
  var successMsg = "保存成功";
  $("#alert_success_massage").empty();
  $("#alert_success_massage").append("<div class='alert alert-success' role='alert'>" + successMsg + "</div>");
  $("#saveSuccessModal").modal();
  setTimeout(function(){
    $("#saveSuccessModal").modal("hide");
    window.location.href = "/delivery/audit/list";
  },2000);

  $("#B_ok").click(function() {
      window.location.href = "/delivery/audit/list";
  });
}

});
