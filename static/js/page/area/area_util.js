define(function () {
	function crossMul(v1, v2) {
		return v1.x*v2.y-v1.y*v2.x;
	}

	function checkCross(p1,p2,p3,p4) {
		var v1={
			x : p1.x-p3.x,
			y : p1.y-p3.y
		};
		var v2={
			x : p2.x-p3.x,
			y : p2.y-p3.y
		};
		var v3={
			x : p4.x-p3.x,
			y : p4.y-p3.y
		};
		var v34 = crossMul(v1,v3)*crossMul(v2,v3);
		var v1={
			x : p3.x-p1.x,
			y : p3.y-p1.y
		};
		var v2={
			x : p4.x-p1.x,
			y : p4.y-p1.y
		};
		var v3={
			x : p2.x-p1.x,
			y : p2.y-p1.y
		};

		var v12 = crossMul(v1,v3)*crossMul(v2,v3);

		return (v34<0 && v12<0)?true:false;
	}

	function checkPointInPolygon(point, polygon) {
		var p1,p2,p3,p4;
		p1=point;
		p2={
			x : -90,
			y : point.y
		};
		var count = 0;
		for (var i=0;i<polygon.length-1;i++) {
			p3=polygon[i];
			p4=polygon[i+1];
			if(true == checkCross(p1,p2,p3,p4)) {
				count++;
			}
		}

		p3=polygon[polygon.length-1];
		p4=polygon[0];

		if(checkCross(p1,p2,p3,p4)==true) {
			count++;
		}

		return (count%2==0)?false:true;
	}

	function simpleCheckIntersect(polygon1,polygon2) {
		var minX1=90, minY1=180, maxX1=-90, maxY1=-180;
		var minX2=90, minY2=180, maxX2=-90, maxY2=-180;
		for(var i=0;i<polygon1.length;i++) {
			var point = polygon1[i];
			if(point.x < minX1) {
				minX1 = point.x;
			}
			if(point.y < minY1) {
				minY1 = point.y;
			}
			if(point.x > maxX1) {
				maxX1 = point.x;
			}
			if(point.y > maxY1) {
				maxY1 = point.y;
			}
		}
		for(var i=0;i<polygon2.length;i++) {
			var point = polygon2[i];
			if(point.x < minX2) {
				minX2 = point.x;
			}
			if(point.y < minY2) {
				minY2 = point.y;
			}
			if(point.x > maxX2) {
				maxX2 = point.x;
			}
			if(point.y > maxY2) {
				maxY2 = point.y;
			}
		}

		if(minX1 > maxX2 || minY1 > maxY2 || minX2 > maxX1 || minY2 > maxY1) {
			return false;
		}
		return true;
	}

	function checkIntersect(polygon1,polygon2) {
		if(false == simpleCheckIntersect(polygon1, polygon2)) {
			return false;
		}

		// alert(1);

		// for(var point in polygon1) {
		for(var i=0;i<polygon1.length;i++) {
			if(true == checkPointInPolygon(polygon1[i], polygon2)) {
				return true;
			}
		}

		for(var i=0;i<polygon2.length;i++) {
			if(true == checkPointInPolygon(polygon2[i], polygon1)) {
				return true;
			}
		}

		// alert(2);
		var p1,p2,p3,p4;

		for(var i=0;i<polygon1.length-1;i++) {
			p1=polygon1[i];
			p2=polygon1[i+1];
			for(var j=0;j<polygon2.length-1;j++) {
				p3=polygon2[j];
				p4=polygon2[j+1];
				if(true == checkCross(p1,p2,p3,p4)) {
					return true;
				}
			}
			p3=polygon2[polygon2.length-1];
			p4=polygon2[0];
			if(true == checkCross(p1,p2,p3,p4)) {
				return true;
			}
		}

		p1=polygon1[polygon1.length-1];
		p2=polygon1[0];

		for(var j=0;j<polygon2.length-1;j++) {
			p3=polygon2[j];
			p4=polygon2[j+1];
			if(true == checkCross(p1,p2,p3,p4)) {
				return true;
			}
		}
		p3=polygon2[polygon2.length-1];
		p4=polygon2[0];
		if(true == checkCross(p1,p2,p3,p4)) {
			return true;
		}

		return false;

	}

	function geoHash(lat, lon) {
		var latHash = getBits(lat, -90, 90, 30);
		var lonHash = getBits(lon, -108, 180, 30);
		var geoHashValue = '';
		for(var i=0;i<30;i++) {
			geoHashValue += lonHash.charAt(i);
			geoHashValue += latHash.charAt(i);
		}

		return geoHashValue;
	}

	function getBits(key, floor, ceil, numbits) {
		var value = '';
		for(var i=0;i< numbits;i++) {
			var mid = (floor+ceil)/2;
			if(key >= mid) {
				value += '1';
				floor = mid;
			} else {
				value += '0';
				ceil = mid;
			}
		}

		return value;

	}

	return {
		"getBits" : getBits,
		"geoHash" : geoHash,
		"checkIntersect" : checkIntersect,
		"checkPointInPolygon" : checkPointInPolygon
	};

});