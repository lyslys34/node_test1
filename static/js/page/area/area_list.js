require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu',
        ],
        'lib/autocomplete' : {
            deps : ['page/common']
        },
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});
require(['module/root', 'module/validator',  'module/cookie', 'lib/autocomplete'], function (t, validator, cookie, autocomplete) {


    var orgNameMap = {};
    var orgIdMap = {};
    var cityNameMap = {};
    var cityIdMap = {};
    var id;
    var oldOrgId;
    var oldOrgName;
    var areaNameMap = {};
    var areaIdMap = {};
    var map;
    $(document).ready(function(){
        initDeleteBtn();
        initSearchCity();
        initSearchBtn();
        initSearchBindOrg();
        initBindOrgBtn();
        initBindOrgSubmitBtn();
        initcancelBindOrgBtn();
        initSearchArea();
        initUnBindOrgModal();
        initShowCityArea();
        getExistArea();
    });

function initDeleteBtn() {
	$(".delete_link").click(function() {
	    var deleteId = $(this).attr('rel');
        var deleteName = $(this).attr('data-name');
	    // $("#deleteform").attr("action", actionUrl);
	    // $("#deleteModal").modal();
        $("#delete_id").val(deleteId);
        $('#delAreaNameHidden').val(deleteName);
        $('#delAreaNameInput').val("");
        $('#delAreaName').text(deleteName);
        $("#editChangeCommentModal").modal();
	});

    $("#editChangeCommentSubmit").click(function() {

        if ($.trim($('#delAreaNameInput').val()) != $.trim($('#delAreaNameHidden').val())) {
            alert("请正确填写待删除区域名称");
            return false;
        }

        var changeComment = "";
        var areaId = $("#delete_id").val();

        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/delivery/delete",
            data: {
                id: areaId,
                changeComment: changeComment

            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4>操作成功</h4>");
                    $("#editChangeCommentModal").modal('hide');
                    $("#showBindRes").modal();
                    setTimeout(function(){
                        $("#showBindRes").modal("hide");
                        window.location.href = "/delivery/list";
                    },2000);
                }else {
                    // $("#editChangeCommentModal").modal('hide');
                    showEditDeleteReasonError(data.errMsg);
                }
            }
            // error:function(XMLHttpRequest ,errMsg){
            //     alert("网络连接失败");
            // }
        });
    });

    $("#cancelChangeCommentSubmit").click(function() {
        $("#editChangeCommentModal").modal("hide");
        $("#delete_id").val("");
        $('#delAreaNameHidden').val("");
        $('#delAreaNameInput').val("");
        return false;
    });
}

    function initBindOrgBtn() {
        $(".bind_link").click(function() {
            id = $(this).attr('rel');
            oldOrgId = $(this).parents("td").prevAll(".orgId").val();
            oldOrgName = $(this).parents("td").prevAll(".orgName").val();
            if(oldOrgId && oldOrgId != "" && oldOrgName && oldOrgName != "") {
                $("#bindOrgId").val(oldOrgId.trim());
                $("#bindOrgName").val(oldOrgName.trim());
            }

            $("#alert_bind_error").empty();

            $("#bindOrgModal").modal();

        });
    }

    function initcancelBindOrgBtn() {
        $('#cancelBindOrg').on('click', function() {
            $("#bindOrgId").val('');
            $("#bindOrgName").val('');
            $("#bindOrgModal").modal('hide');
        });
    }

    function bindOrg() {
        var orgId = $("#bindOrgId").val().trim();

        var orgName = $("#bindOrgName").val().trim();
        // var explosionLevel = $("#explosionLevel").val();
        if(typeof id == 'undefined') {
            showBindError("获取区域错误");
            return ;
        }
        if(orgId == 0 && orgName != "") {
            showBindError("请填写正确的组织名称");
            return;
        }
        if(orgId==0 && orgName==""){
            $("#bindOrgModal").modal('hide');
            $("#unBindOrgModal").modal();
            // initUnBindOrgModal(id,oldOrgId);
            return;
        }
        if(orgId == "") {
            showBindError("请填写组织名称");
            return;
        }

        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/delivery/bindOrg",
            data: {
                id: id,
                orgId: orgId

            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4>绑定组织成功</h4>");
                    $("#bindOrgModal").modal('hide');
                    $("#showBindRes").modal();
                }else {
                    showBindError(data.errMsg);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });

    }

    function initBindOrgSubmitBtn() {
        $('#bindOrgSubmit').on('click', function(){
            bindOrg();
        });
    }

    function initUnBindOrgModal(){
        $('#unBindOrgSubmit').on('click',function(){
            if(oldOrgId==null){
                $("#search").click();
            }else{
                $.ajax({
                    dataType: 'json',
                    type : 'post',
                    url : "/delivery/unBindOrg",
                    data: {
                        id: id,
                        orgId:oldOrgId
                    },
                    success : function(data){
                        if(data.success){
                            $("#resMsg").html("<h4>解除绑定组织成功</h4>");
                            $("#unBindOrgModal").modal('hide');
                            $("#showBindRes").modal();
                            setTimeout('$("#search").click()',2000);
                        }else {
                            alert(data.errMsg);
                        }
                    },
                    error:function(XMLHttpRequest ,errMsg){
                        alert("网络连接失败");
                    }
                });
            }

        })
    }

    function initSearchBtn() {
        $("#search").click(function() {
            var cityId = $("#cityId").val();
            var cityName = $("#cityName").val().trim();
            if(cityId == 0 && cityName != "") {
                showError("请填写正确的城市名称");
                return false;
            }
            if(cityId == "") {
                showError("请填写城市名称");
                return false;
            }

            var areaId = $("#id").val().trim();
            var areaName = $("#name").val().trim();
            if(areaId == 0 && areaName != "") {
                showError("请填写正确的区域名称");
                return false;
            }
            if(areaId == "") {
                showError("请填写区域名称");
                return false;
            }

            $("#formValidate1").submit();
        });
    }

    function initSearchBindOrg() {

        myPOST('/org/orgSearchList', {}, function (r) {
            if (r.httpSuccess) {
                if(r.data && r.data.code == 0) {
                    $(".js_bind_org_name").autocomplete({
                        delay:100,
                        source : r.data.data,
                        focus: function( event, ui ) {
                            $( ".js_bind_org_name" ).val( ui.item.value );
                            $( ".js_bind_org_id" ).val( ui.item.id );
                            return false;
                        },
                        select: function(event, ui) {

                            $(".js_bind_org_name").val(ui.item.value );
                            $(".js_bind_org_id" ).val( ui.item.id );
                        },
                        change: function(event, ui) {
                            var orgName = $(".js_bind_org_name").val();
                            if(validator.isBlank(orgName)) {
                                $(".js_bind_org_id").val(0);
                            }
                        },
                        open: function(event, ui) {
                            $(".js_bind_org_id").val('0');
                        }

                    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                        ul.css("z-index", 2000);
                        return $( "<li>" )
                            .append( "<a>" + item.value + "</a>" )
                            .appendTo( ul );
                    };
                    if(r.data.data) {
                        $.each(r.data.data, function(index, item) {
                            orgNameMap[item.value] = item;
                            orgIdMap[item.id] = item;
                        });
                    }
                    var riderId = $(".js_bind_org_id").val();
                    $(".js_bind_org_name").val(orgIdMap[riderId] != null && typeof orgIdMap[riderId] != 'undefined' ? orgIdMap[riderId].value : "");

                } else {
                    alert("获取组织信息失败，" + r.data.msg);
                }
            } else {

            }
        });
    }

    function initSearchCity() {

        myPOST('/org/citySearchList', {}, function (r) {
            if (r.httpSuccess) {
                if(r.data && r.data.code == 0) {
                    $(".js_city_name").autocomplete({
                        delay:100,
                        source : r.data.data,
                        focus: function( event, ui ) {
                            $( ".js_city_name" ).val( ui.item.value );
                            $( ".js_city_id" ).val( ui.item.id );
                            return false;
                        },
                        select: function(event, ui) {

                            $(".js_city_name").val(ui.item.value );
                            $(".js_city_id").val(ui.item.id );
                        },
                        change: function(event, ui) {
                            var cityName = $(".js_city_name").val();
                            if(validator.isBlank(cityName)) {
                                $(".js_city_id").val(0);
                            }
                        },
                        open: function(event, ui) {
                            $(".js_city_id").val('0');
                        }

                    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                        return $( "<li>" )
                            .append( "<a>" + item.value + "</a>" )
                            .appendTo( ul );
                    };
                    if(r.data.data) {
                        var cityId = $(".js_city_id").val();
                        $.each(r.data.data, function(index, item) {
                            cityNameMap[item.value] = item;
                            cityIdMap[item.id] = item;
                            if(cityId) {
                                if(cityId == item.id) {
                                    $(".js_city_name").val(item.value);
                                }
                            }
                        });

                    }

                    var riderId = $(".js_city_id").val();
                    $(".js_city_name").val(cityIdMap[riderId] != null && typeof cityIdMap[riderId] != 'undefined' ? cityIdMap[riderId].value : "");

                } else {
                    alert("获取组织信息失败，" + r.data.msg);
                }
            } else {
                //alert(JSON.stringify(r));
                // alert('获取组织信息失败，系统错误');
            }
        });
    }

    function initSearchArea() {

        myPOST('/org/areaSearchList', {}, function (r) {
            if (r.httpSuccess) {
                if(r.data && r.data.code == 0) {
                    $(".js_area_name").autocomplete({
                        delay:100,
                        source : r.data.data,
                        focus: function( event, ui ) {
                            $( ".js_area_name" ).val( ui.item.value );
                            $( ".js_area_id" ).val( ui.item.id );
                            return false;
                        },
                        select: function(event, ui) {

                            $(".js_area_name").val(ui.item.value );
                            $(".js_area_id").val(ui.item.id );
                        },
                        change: function(event, ui) {
                            var areaName = $(".js_area_name").val();
                            if(validator.isBlank(areaName)) {
                                $(".js_area_id").val(0);
                            }
                        },
                        open: function(event, ui) {
                            $(".js_area_id").val('0');
                        }

                    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                        return $( "<li>" )
                            .append( "<a>" + item.value + "</a>" )
                            .appendTo( ul );
                    };
                    if(r.data.data) {
                        $.each(r.data.data, function(index, item) {
                            areaNameMap[item.value] = item;
                            areaIdMap[item.id] = item;
                        });

                    }

                    var riderId = $(".js_area_id").val();

                } else {
                    alert("获取区域信息失败，" + r.data.msg);
                }
            } else {
                //alert(JSON.stringify(r));
                // alert('获取区域信息失败，系统错误');
            }
        });
    }

    function myPOST(url, data, callback)  // 发送POST请求
    {
        if(typeof(data) == 'function') { callback = data; data = null; }
        $.ajax({ type: 'POST', url: url, data:data, success:function(r) {

            callback({ httpSuccess: true, data: r });

        }, error: function(XmlHttpRequest, textStatus, errorThrown) {

            callback({ httpSuccess: false, statusCode:XmlHttpRequest.status });

        } });
    }


    function isBlank(value) {
        if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
                return this.replace(/^\s+|\s+$/g, '');
            }
        }
        return !_valueExist(value) || value.trim() == "";
    }

    function showError(errMsg) {
        $("#alert_error").empty();
        $("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
    }

    function showBindError(errMsg) {
        $("#alert_bind_error").empty();
        $("#alert_bind_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
    }

function showEditDeleteReasonError(errMsg) {
    $("#alert_edit_delete_reason_error").empty();
    $("#alert_edit_delete_reason_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

function getExistArea() {
    $("#showCityArea").click(function() {
        var cityId = $("#cityId").val().trim();
        if(cityId == 0 && cityName != "") {
            showError("请选择城市");
            return false;
        }
        myPOST('/delivery/existAreas', {cityId : cityId}, function (r) {
            if (r.httpSuccess) {
                if(r.data && r.data.code == 0) {
                    if(r.data.data) {
                        existAreas = r.data.data;
                        if(existAreas.data) {
                            if(existAreas.data.data) {
                                if (typeof map == 'undefined' || map == '') {
                                    //初始化地图对象，加载地图
                                    map = new AMap.Map("mapContainer", {
                                        resizeEnable: true,
                                        view: new AMap.View2D({
                                        resizeEnable: true,
                                            zoom:11//地图显示的缩放级别
                                        }),
                                        keyboardEnable:false
                                    });
                                    //加载比例尺插件
                                    map.plugin(["AMap.Scale"], function(){      
                                        scale = new AMap.Scale();
                                        map.addControl(scale);
                                    });

                                    // //地图类型切换
                                    // map.plugin(["AMap.MapType"], function() {
                                    //     var type = new AMap.MapType({defaultType:0});//初始状态使用2D地图
                                    //     map.addControl(type);
                                    // });
                                    //在地图中添加ToolBar插件
                                    map.plugin(["AMap.ToolBar"],function(){     
                                        toolBar = new AMap.ToolBar();
                                        // toolBar.setOffset(new AMap.Pixel(10,50));
                                        map.addControl(toolBar);      
                                    });
                                    
                                }

                                map.setCity(cityId);
                                map.setZoom(11);

                                $.each(existAreas.data.data, function(n, value) {
                                        var arr = new Array(); //构建多边形经纬度坐标数组   
                                        if (!value.deliveryAreaCoordinates || value.deliveryAreaCoordinates == '') {
                                          return ;
                                        }
                                        var areas = JSON.parse(value.deliveryAreaCoordinates);

                                        for (var i in areas) {
                                            arr.push(new AMap.LngLat(areas[i].y, areas[i].x));
                                        }
                                        var polygon2 = new AMap.Polygon({
                                            map: map,
                                            path: arr,
                                            zIndex: 200,
                                            strokeColor: "#0000FF",
                                            strokeOpacity: 1,
                                            strokeWeight: 3,
                                            fillColor: "#00FFFF",
                                            fillOpacity: 0.35
                                        });

                                    });
                                    $("#showCityAreaModal").modal();
                                } else {
                                    showError("该城市没有配送区域");
                                }

                            }
                        }

                    } else {
                        alert("获取已有配送区域失败，" + r.data.msg);
                    }
                }
            });

        });

        $("#resetZoomBtn").click(function() {
            var cityId = $("#cityId").val().trim();
            if(cityId == 0 && cityName != "") {
                showError("请选择城市");
                return false;
            }
            map.setCity(cityId);
            map.setZoom(11);
        });

    }


    function initShowCityArea() {
        //初始化地图对象，加载地图
        map = new AMap.Map("mapContainer", {
            resizeEnable: true,
            view: new AMap.View2D({
                resizeEnable: true,
                zoom:11//地图显示的缩放级别
            }),
            keyboardEnable:false
        });
        //加载比例尺插件
        map.plugin(["AMap.Scale"], function(){
            scale = new AMap.Scale();
            map.addControl(scale);
        });
        //     //地图类型切换
        // map.plugin(["AMap.MapType"], function() {
        // var type = new AMap.MapType({defaultType:0});//初始状态使用2D地图
        //     map.addControl(type);
        // });
        //在地图中添加ToolBar插件
        map.plugin(["AMap.ToolBar"],function(){
            toolBar = new AMap.ToolBar();
            // toolBar.setOffset(new AMap.Pixel(10,50));
            map.addControl(toolBar);
        });

    }

});