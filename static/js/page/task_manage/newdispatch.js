require.config({
    baseUrl: MT.STATIC_ROOT + '/js',
    urlArgs: 'ver=' + pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        },

    }
});

require(['module/root', 'module/cookie', 'lib/bootstrap-multiselect'], function(r, cookie, muti) {

    var manualCount = 0;
    var assignStatus = -10;
    var notCatchedStatus = 10;
    var reAssignStatus = 15;
    var arrivedStatus = 50;
    var canceledStatus = 99;
    var catchedStatus = 20;
    var onWayStatus = 30;
    var pageSize = 20;
    var notComplete = 100;
    var allStatus = 110;
    var sendbackStatus = -5;

    var curSelOrderId = new Array();
    var curWaybillStatus = reAssignStatus;
    var curRiderList;
    var curWaybillList;
    var curRiderId;
    var riderStatusArr = new Array(10, 1, 2, 0);
    var statusDic = new Array();
    var lastHoverTr;
    var trTimeout;
    var lastPlayTime = 0;
    var docActive = true;
    var orgIdList = new Array();
    var waybillListRequest;
    var riderListRequest;
    var countRequest;
    var explosionMsgList = {};
    var busyString = "";
    var busyMessage = new Object(); //是一个对象 (商家名、序号值)两个值可以为空
    busyMessage.poiName = "";
    busyMessage.poiSeq = "";
    var timerId = null;
    var orAsc = true; // 是否升序
    var deliveryAreaList = {};
    var allOrgList = {};
    var changeOrgList = {};
    var selCity = $('#selectCity');


    /**
     * 点击 商家(4)/收货地址(5)/骑手(9)/剩余时间(2) 的排序
     * 顺便传一个升序还是倒序的参数
     */
    var waybilllistAsc = true;
    var searchRiderName = "";

    // @huanghuijie@meituan.com 2015/8/13 add
    var getRiderName = "", // 骑手名
        inputOrderField = "utime", // 排序的名
        inputOrderSeq = "1", // 升序(1)or降序(-1)
        orderNum = "", // 订单序号
        getCusNum = "", // 顾客手机
        getSendMap = ""; // 送餐地址

    var detailDia;
    var changeDia;
    var mutiSetDia;

    var statusNameDic = new Array();

    statusDic['-5'] = '#sendbackCount';
    statusDic['-10'] = '#manualCount';
    statusDic['15'] = '#notConfirm';
    statusDic['10'] = '#assginedCount';
    statusDic['0'] = '#notAssignCount';
    statusDic['20'] = '#catchedCount';
    statusDic['30'] = '#onWayCount';
    statusDic['50'] = '#arrivedCount';
    statusDic['99'] = '#canceledCount';
    statusDic['100'] = '#notComplete';
    statusDic['110'] = '#allCount';

    statusNameDic['-10'] = '需手工指派';
    statusNameDic['15'] = '派单';
    statusNameDic['0'] = '未调度';
    statusNameDic['10'] = '未接单';
    statusNameDic['20'] = '已接单';
    statusNameDic['30'] = '已取货';
    statusNameDic['50'] = '已送达';
    statusNameDic['99'] = '已取消';
    statusNameDic['100'] = '未完成';
    statusNameDic['110'] = '全部'

    $('.j-toggle-menu').click();
    bindEvents();
    fnSelectDay();
    selectDoBtnTitle();
    resetTitle();
    setSoundIcon();

    getLoginInfo(getStoredCity());

    function getLoginInfo(cityId) {
        $.ajax({
                url: '/partner/dispatch/dispatchHomeLoginInfo',
                type: 'GET',
                dataType: 'json',
                data: {
                    cityId: cityId
                },
            })
            .done(function(result) {
                if (result.code == 0) {
                    $('#main-content').removeClass('hidden');
                    $('#loading-content').addClass('hidden');

                    fillCityList(result.data.bmCityList, result.data.selectCityId);
                    refreshAreaDta(result.data.bmDeliveryAreaList);
                    refreshOrgData(result.data.bmOrgList);
                    fillChangeOrgList(result.data.changeOrgIds);
                    hideNewWaybillLiIfNeeded(result.data.unfetchView);

                    fillAreaList(true);
                    init();
                } else {
                    $('#reload-base').removeClass('hidden');
                    $('#base-info').text(result.msg);
                }
            })
            .fail(function() {
                $('#reload-base').removeClass('hidden');
                $('#base-info').text('加载失败，请重试');
            })
            .always(function() {});
    }

    function hideNewWaybillLiIfNeeded(unfetchView) {
        if (unfetchView != 1) {

            var li = $('#wayBillListDiv ul li[value="10"]');
            li.remove();
        };
    }

    function init() {

        setControlStatus();
        setUrlRiderName();
        waybillLiWidth();
        initRiderListSort();
        // 轮询counts
        getWayBillCounts();
        setInterval(getWayBillCounts, 20 * 1000);

        var map = window.location.hash;
        var status = curWaybillStatus;
        _getRiderList();

        initStatuSelect(10, 0); //默认的状态函数变化
        showChangeDia(); //调度模式变更提醒

        if (map != null && map != "") {
            status = map.substring(1, map.length);
        } else {
            getWayBillList(status, 1);
            return;
        }

        var flag = false;
        $("#waybillStatue li").each(function() {
            if ($(this).attr("value") == status) {
                flag = true;
                $(this).click();
            }
        });
        if (!flag) {
            getWayBillList(status, 1);
        };

        setTimeout(function() {
            $(".select2-search__field").attr("placeholder", "输入关键字");
        }, 1000);
    }

    function setControlStatus() {

        var show = (canDispatch == 1);
        if (!show) {
            $('#sigle-control').remove();
            $('#control-icon').remove();
            $('#getSearchRiderName').css('width', '174');
        };
    }

    function setUrlRiderName() {
        var riderName = r.utils.urlArg('riderName');
        if (riderName) {
            riderName = decodeURIComponent(riderName);
            $("#getRiderName").val(riderName);
            getRiderName = $("#getRiderName").val();
        };
    }

    function refreshOrgData(data) {
        allOrgList = {};
        if (data) {
            for (var i = 0; i < data.length; i++) {
                var orgView = data[i];
                var org = {};
                org.orgid = orgView.orgId;
                org.name = orgView.orgName;
                org.dispatchStrategyDes = orgView.dispatchStrategyDes;
                org.areaId = orgView.areaId;

                allOrgList[orgView.orgId] = org;
            };
        }
    }

    function refreshAreaDta(data) {

        deliveryAreaList = {};
        if (data) {

            for (var i = 0; i < data.length; i++) {
                var deliveryArea = data[i];
                var area = {};
                area.id = deliveryArea.areaId;
                area.cityId = deliveryArea.cityId;
                // area.orgId = deliveryArea.bmOrgId;
                area.name = '-';
                if (deliveryArea.areaName) {
                    area.name = deliveryArea.areaName;
                }
                deliveryAreaList[area.id] = area;
            };
        }
    }

    function fillChangeOrgList(data) {

        for (var i = 0; i < data.length; i++) {
            var changeOrg = data[i];
            changeOrgList[changeOrg.orgId] = changeOrg;
        };
    }

    function fillCityList(data, selCityId) {
        var body = '';
        for (var i = 0; i < data.length; i++) {
            var city = data[i];
            if (selCityId == city.city_id) {
                body += '<option selected="selected" value="' + city.city_id + '">' + city.name + '</option>';
            } else {
                body += '<option value="' + city.city_id + '">' + city.name + '</option>';
            }
        };
        selCity.append(body);
        $.fn.select2.amd.require(['select2/compat/matcher'], function(oldMatcher) {
            selCity.select2({
                matcher: oldMatcher(matchPinyin)
            });
        });
        selCity.select2();

        selCity.on("change", function(e) {
            var cityId = getSelCityid();
            storeCityId(cityId);
            getAreaListWithCity(cityId);
        });
    }

    function getAreaListWithCity(cityId) {
        $.ajax({
                url: '/partner/dispatch/homeLoginInfo',
                type: 'GET',
                dataType: 'json',
                data: {
                    cityId: cityId
                },
            })
            .done(function(result) {
                if (result.code == 0) {

                    refreshAreaDta(result.data.bmDeliveryAreaList);
                    refreshOrgData(result.data.bmOrgList);
                    fillAreaList();
                    refreshLists();
                } else {
                    alert('获取区域信息失败，请重试');
                }
            })
            .fail(function() {
                refreshAreaDta();
                refreshOrgData();
            })
            .always(function() {});
    }

    function addVisibilityListener() {

        var hidden, visibilityChange;
        if (typeof document.hidden !== "undefined") {
            hidden = "hidden";
            visibilityChange = "visibilitychange";
        } else if (typeof document.mozHidden !== "undefined") {
            hidden = "mozHidden";
            visibilityChange = "mozvisibilitychange";
        } else if (typeof document.msHidden !== "undefined") {
            hidden = "msHidden";
            visibilityChange = "msvisibilitychange";
        } else if (typeof document.webkitHidden !== "undefined") {
            hidden = "webkitHidden";
            visibilityChange = "webkitvisibilitychange";
        }

        // 如果浏览器不支持addEventListener或页面可见性API,发出警告
        if (typeof document.addEventListener === "undefined" || typeof hidden === "undefined") {
            console.log("This demo requires a browser such as Google Chrome that supports the Page Visibility API.");
        } else {
            // 注册visibilityChange事件处理函数
            document.addEventListener(visibilityChange, handleVisibilityChange, false);
        }
    }

    function areaIdInArg(areaArg, areaId)
    {
        if (areaArg == areaId) {
            return true;
        };
        var has = false;
        var arr = areaArg.split(',');
        for (var i = arr.length - 1; i >= 0; i--) {
            if (arr[i] == areaId) {
                has = true;
                break;
            };
        };
        return has;
    }

    function fillAreaList(useArg) {
        $('#selectArea').empty();
        var options = '';
        var areaArg = r.utils.urlArg('areaId');
        if (!areaArg && useArg) {
            areaArg = getStoredArea();
        };
        var selected = '';
        var showUrlArg = false;
        for (areaId in deliveryAreaList) {

            if (useArg && areaArg && areaIdInArg(areaArg, areaId)) {
                selected = ' selected="selected"';
                showUrlArg = true;
            };
            var area = deliveryAreaList[areaId];
            options = options + '<option' + selected + ' value=' + area.id;
            options = options + '>' + area.name;
            options = options + '</option>';
            selected = '';
        }
        $('#selectArea').append(options);
        $('#selectArea').multiselect('destroy');

        if (orgRoleType === '1') {
            var canSelectAll = true;
            var firstValue = 0;
            if (Object.keys(deliveryAreaList).length > 10) {
                canSelectAll = false;
                firstValue = Object.keys(deliveryAreaList)[0];
            };
            $('#selectArea').multiselect({
                includeSelectAllOption: canSelectAll,
                selectAllText: '选择全部',
                nonSelectedText: '没有选择',
                nSelectedText: '个区域',
                allSelectedText: '全部区域',
                selectAllValue: '0',
                filterPlaceholder: '输入关键字',
                includeSelectAllIfMoreThan: 1,
                enableFiltering: true,
                numberDisplayed: 1,
                maxHeight: 250,
                onDropdownHidden: function(event) {

                    onSelAreaHidden(event);
                },
            });
            if (!showUrlArg && canSelectAll) {
                $('#selectArea').multiselect('selectAll', false, true);
            }else if(!showUrlArg){
                $('#selectArea').multiselect('select', firstValue);
            }
        } else {
            $('#selectArea').multiselect({
                includeSelectAllOption: false,
                nonSelectedText: '没有选择',
                nSelectedText: '个区域',
                enableFiltering: true,
                maxHeight: 250,
                numberDisplayed: 1,
                onDropdownHidden: function(event) {

                    onSelAreaHidden(event);
                },
            });
        }

        $('#selectArea').multiselect('updateButtonText');
        storeAreaId();
        fillOrgList(useArg);
    }

    function showControlCount() {
        var selCount = $('.rider-select:checked').length;
        var total = $('.rider-select').length;
        $('#control-count').text(selCount + '/' + total);
        if (total > 0 && selCount == total) {
            $('#rider-select-all').prop('checked', 'checked');
        } else if (selCount == 0) {
            $('#rider-select-all').prop('checked', false);
        }
    }

    function onSelAreaHidden() {
        storeAreaId();
        fillOrgList();
        refreshLists();
    }

    function storeAreaId() {
        var userId = cookie.getCookie("userId");
        var key = userId + 'dispatch_selectedArea';

        var op = $('#selectArea option:selected');
        if (op.length == 1) {

            localStorage.setItem(key, op.attr('value'));
        } else if(op.length > 1){
            var storeString = '';
            op.each(function(index, el) {

                if (index == 0) {
                    storeString += $(el).attr('value');
                }else{
                    storeString += ',' + $(el).attr('value');
                }
            });
            localStorage.setItem(key, storeString);
        }else{
            localStorage.removeItem(key);
        }
    }

    function getStoredArea() {
        var areaId = 0;
        var userId = cookie.getCookie("userId");
        var storedArea = localStorage.getItem(userId + 'dispatch_selectedArea');
        if (storedArea) {
            areaId = storedArea;
        };
        return areaId;
    }

    function getStoredCity() {
        var cityId = 0;
        var argCity = r.utils.urlArg('cityId');
        if (argCity) {

            cityId = argCity;
        } else {
            var userId = cookie.getCookie("userId");
            var storedCity = localStorage.getItem(userId + 'dispatch_selectedCity');
            if (storedCity) {
                cityId = storedCity;
            };
        }

        return cityId;
    }

    function storeCityId(cityId) {
        var userId = cookie.getCookie("userId");
        localStorage.setItem(userId + 'dispatch_selectedCity', cityId);
    }

    function getSelCityid() {
        var cityid;
        $('#selectCity option:selected').each(function() {
            cityid = $(this).val();
        });
        return cityid;
    }

    function getSelAreas() {
        var selAreas = new Array();
        $('#selectArea option:selected').each(function() {
            selAreas.push($(this).val());
        });
        return selAreas;
    }

    function areaIdInSel(areaId) {
        var selAreas = getSelAreas();
        var has = false;
        for (var i = 0; i < selAreas.length; i++) {
            if (selAreas[i] == areaId) {
                has = true;
                break;
            };
        };
        return has;
    }

    function fillOrgList(useArg) {
        $('#selectOrg').empty();

        var options = '';
        if (!canDoAssgin()) {
            options = options + '<option' + ' value=' + '-1';
            options = options + '>' + '未分配站点';
            options = options + '</option>'
        };

        var orgArg = r.utils.urlArg('orgId');
        var selected = '';
        var showUrlArg = false;
        for (orgid in allOrgList) {

            var org = allOrgList[orgid];
            if (areaIdInSel(org.areaId)) {

                if (useArg && orgArg && (orgArg == orgid)) {
                    selected = ' selected="selected"';
                    showUrlArg = true;
                };
                options = options + '<option' + selected + ' value=' + org.orgid;
                options = options + '>' + org.name + '[' + org.dispatchStrategyDes + ']';
                options = options + '</option>';
                selected = '';
            };
        }

        $('#selectOrg').append(options);
        $('#selectOrg').multiselect('destroy');
        $('#selectOrg').multiselect({
            includeSelectAllOption: true,
            selectAllText: '选择全部',
            nonSelectedText: '没有选择',
            nSelectedText: '个站选项',
            allSelectedText: '全部选项',
            selectAllValue: '0',
            filterPlaceholder: '输入关键字',
            includeSelectAllIfMoreThan: 1,
            numberDisplayed: 1,
            enableFiltering: true,
            limitTextNumber: true,
            maxTextNumber: 15,
            maxHeight: 500,
            onDropdownHidden: function(event) {

                onSelOrgHidden(event);
                showSelOrgName();
            },
        });
        if (!showUrlArg) {
            $('#selectOrg').multiselect('selectAll', false, true);
        };
        $('#selectOrg').multiselect('updateButtonText');
        getOrgIdList();
        showSelOrgName();
    }

    function getOrgIdList() {
        orgIdList = new Array();
        $('#selectOrg option:selected').each(function() {
            if ($(this).val() === '-1') {
                orgIdList.push(0);
            } else {
                orgIdList.push($(this).val());
            }
        });
    }

    function onSelOrgHidden(event) {
        getOrgIdList();
        refreshLists();
    }

    // 遮罩层消失且输入框清空
    function clearShadeVal() {
        document.getElementById("getSearchRiderName").value = "";
        searchRiderName = '';
        $(".shade").remove();
    }

    // TODO down
    function selectDoBtnTitle() {
        var status = $("#selectStatus").val(),
            date = $("#selectDay").val();
        var statusStr = $("#selectStatus option:eq(" + status + ")").text() + "<span class='caret btn-left-4px'></span>",
            dateStr = $("#selectDay option:eq(" + date + ")").text() + "<span class='caret btn-left-4px'></span>";
        $("#btnClickStatus").html(statusStr);
        $("#btnClickDate").html(dateStr);
    }

    function fnDropLiClick(_this) {
        _this.siblings().removeClass('active');
        _this.addClass('active');
        var select = _this.parents(".dropdown").prev();
        select.find('option').removeAttr('selected');
        select.find('option:eq(' + _this.index() + ')').attr("selected", "selected");
        select.val(_this.index());
        var btnStrTemp = _this.text() + "<span class='caret btn-left-4px'></span>";
        _this.parents(".dropdown").find("button").html(btnStrTemp);
        var selectId = _this.parents(".dropdown").prev().attr("id");
        if (selectId == "selectStatus") {
            clearShadeVal();
            _getRiderList();
        };
        if (selectId == "selectDay") {
            getWayBillList(curWaybillStatus, 1);
            _getRiderList();
            // refreshLists();
        };
    }

    function refreshLists() {
        curSelOrderId = [];
        getWayBillList(curWaybillStatus, 1);
        _getRiderList();
        initStatuSelect(10, 0);
    }

    function showReassignDlg(riderId) {
        if (detailDia) {
            detailDia.remove();
            detailDia = null;
        };

        var body = '<div class="reasonSel"><br>';
        var rider = curRiderList[riderId];

        body = body + '<span style="font-size: 16px;color:red;">注：改派后将发通知给顾客，请勿频繁改派，避免顾客投诉</span><br/><br/>';
        body = body + '<input type="radio" name="reason" id="reason1" class="assign" /><label class="reason" for="reason1" >原骑手不顺路</label><br/>';
        body = body + '<input type="radio" name="reason" id="reason2" class="assign" /><label class="reason" for="reason2">原骑手单多，忙不过来</label><br/>';
        body = body + '<input type="radio" name="reason" id="reason3" class="assign"/><label class="reason" for="reason3">原骑手车没电了/车坏了</label><br/>';
        body = body + '<input type="radio" name="reason" id="reason4" class="assign"/><label class="reason" for="reason4">原骑手手机没电了/手机坏了</label><br/>';
        body = body + '<input type="radio" name="reason" id="reason6" class="assign"/><label class="reason" for="reason6">原骑手取餐经费不足</label><br/>';
        body = body + '<input type="radio" name="reason" id="reason5" class="assign"/><label class="reason" for="reason5">其他</label><br/>';
        body = body + '<span id="re-fail" class="pull-right hidden" style="padding-right: 10px; color: #f00">原因不能为空</span>'
        body = body + '<br><input type="text"  id="re-reason" placeholder="选择‘其他’后可输入(限20个字)" maxlength="20" class="input-sm" readonly="readonly" style="width:440px;background-color: #e8e8e8;box-shadow: none;border-color: #e8e8e8;"></input>';
        body = body + '</div>';
        body = body + '<br><span style = "font-size:12px;">已选中<span style = "color:#00abe4">(' + curSelOrderId.length + ')</span>单，</span>';

        if (curSelOrderId.length == 1) {
            var waybill = curWaybillList[new String(curSelOrderId[0])];
            body = body + '<span style="font-size: 12px">由（' + waybill.ridderNameWithoutPhone + waybill.ridderPhone + '）&nbsp;</span>';
        }
        body = body + '改派给' + '<span style="color: #00abe4">（' + rider.name + rider.mobile + '）</span>';

        var sbody = $(body);
        var $failtTip = $('#re-fail', sbody);
        var $failReason = $('#re-reason', sbody);
        var re_other = $('#reason5', sbody);
        var sInput = $("input[name='reason']", sbody);

        sInput.click(function() {
            if (re_other.is(':checked')) {
                $failReason.removeAttr('readonly');
                $failReason.val('');
                $failReason.attr('placeholder', '可输入其他原因，限20个字');
                $failReason.focus();
                $failReason.css('background-color', '#fff');
            } else {
                $failReason.attr('readOnly', 'readOnly');
                $failReason.attr("placeholder", "选择‘其他’后可输入(限20个字)");
                $failReason.blur();
                $failReason.css('background-color', '#e8e8e8');

            }
        });

        detailDia = r.ui.showModalDialog({
            title: '改派操作',
            body: sbody,
            buttons: [
                r.ui.createDialogButton('close', '取消', function() {

                }), //取消

                r.ui.createDialogButton('sure', '确认改派', function() { // 确认改派

                    var rea = '';

                    if (re_other.is(':checked')) {
                        rea = $failReason.val();

                    } else {

                        rea = $("input[name='reason']:checked").next("label").text();


                    }

                    rea = rea.trim();
                    if (rea.length < 1) {

                        $failtTip.removeClass('hidden');
                    } else {

                        $(".shade").remove(); //改派成功之后遮罩消失
                        if (curSelOrderId.length == 1) {
                            reAssignToRider(riderId, curSelOrderId[0], rea);
                        } else {
                            mulReAssignToRider(riderId, curSelOrderId, rea);
                        }

                        return true;
                    }

                })

            ],

            style: {
                ".modal-body": {
                    "padding-top": "0"
                },
                ".modal-footer": {
                    "border-top": "none",
                    "padding-top": "0"
                },
                ".modal-dialog": {
                    "top": "200px",
                    "width": "480px"
                },
                ".btn-default": {
                    "width": "200px",
                    "float": "left",
                    "margin": "0"
                },
                ".btn:nth-child(odd)": {
                    "margin": "0 25px 0 10px"
                }
            }

        });

        detailDia.on("hidden.bs.modal", function() {
            this.remove();
            detailDia = null;
        });
    }

    function showDispatchChange() {

        var body = '请注意:';
        for (var changeOrg in changeOrgList) {
            var changeTime = changeOrgList[changeOrg].changeTime;
            var date = new Date(changeTime * 1000);
            var timeStr = getFormatedString(date.getMonth()) + '-' + getFormatedString(date.getDate()) + ' ' + getFormatedString(date.getHours()) + ':' + getFormatedString(date.getMinutes());

            body = body + '\n"' + changeOrgList[changeOrg].orgName + '"';
            body = body + '调度模式已于' + timeStr + '变更为';
            body = body + '"推抢结合"模式';
        }
        var noti = r.utils.showDesktopNotification('【烽火台】调度模式变更提醒', body);
    }

    function jumpPoi(tag) {
        var data = explosionMsgList[tag];
        if (data) {
            var url = "/poi?" + "poiType=" + data.poiType + "&workStatus=" + data.workStatus + "&cityId=" + data.cityId + "&areaId=" + data.areaId;
            window.open(url, '_blank');
        };
    }

    function _getRiderList() {

        var cursel = $('#selectStatus').val();
        var status = riderStatusArr[cursel];

        var id = 0;
        if (curSelOrderId.length > 0) {
            id = curSelOrderId[0];
        };
        getRiderList(status, id);
    }

    function isSoundClose() {
        var userId = cookie.getCookie("userId");
        var soundClose = cookie.getCookie("newWaybillSoundClose" + userId);
        if (soundClose != "") {

            return true;
        };
        return false;
    }

    function setSoundIcon() {
        var userId = cookie.getCookie("userId");
        var soundClose = cookie.getCookie("newWaybillSoundClose" + userId);
        var imgSrc = "/static/imgs/sound-close.gif";
        if (soundClose != "") {
            imgSrc = "/static/imgs/sound-open.gif";
        };
        $('#sound-icon').attr({
            src: imgSrc
        });
    }

    function showNewTip() {
        if (!docActive && timerId == null) {

            changeTitle();
            timerId = setInterval(changeTitle, 2 * 1000);
        };
    }

    function changeTitle() {
        document.title = "来新订单了"
        setTimeout(resetTitle, 1 * 1000);
    }

    function resetTitle() {
        document.title = "调度控制台";
    }

    function handleVisibilityChange() {
        if (document[hidden]) {

            docActive = false;
        } else {

            docActive = true;
            clearInterval(timerId);
            timerId = null;
            resetTitle();
        }
    }
    // 订单表头li自适应
    function waybillLiWidth() {
        var liLen = $("#waybillStatue li").length,
            liWdth = 100 / liLen;
        liWdth = liWdth + "%";
        if ($(document).width() > 1024) {
            $("#waybillStatue li").css("width", liWdth);
        } else {
            $("#waybillStatue li").removeAttr('style');
        }
    }
    /**
     * 骑手状态的初始化（有全部骑手／在线／离岗／忙碌）四个选项
     */
    function initStatuSelect(status, waybillId) {
        if (orgIdList.length < 1) {
            return;
        };
        var data = {
            orgList: orgIdList,
            status: status,
            waybillId: waybillId
        };
        var onLineCount = 0;
        var offLineCount = 0;
        var busyCount = 0;
        var totalCount = 0;

        var url = '/partner/dispatch/riderlist';
        if (!canDoAssgin()) {
            url = '/partner/dispatch/riderlist';
            var areaList = getSelAreas();
            data.areaList = areaList;
            data.orgList = null;
        };
        $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data,
                traditional: true,
            })
            .done(function(result) {
                $.each(result, function(index, rider) {
                    rider.workStatus == 1 ? onLineCount++ : onLineCount;
                    rider.workStatus == 0 ? offLineCount++ : offLineCount;
                    rider.workStatus == 2 ? busyCount++ : busyCount;
                });

                var cursel = $('#selectStatus').val();
                totalCount = onLineCount + busyCount + offLineCount;
                var temp = '<option value="0">全部(' + totalCount + ')</option>' + '<option value="1">在岗(' + onLineCount + ')</option>' + '<option value="2">忙碌(' + busyCount + ')</option>' + '<option value="3">离岗(' + offLineCount + ')</option>';
                $('#selectStatus').html(temp);
                $('#selectStatus option').each(function(index, el) {
                    if ($(el).attr('value') == cursel) {
                        $(el).attr('selected', 'selected');
                    };
                });
                selectDoBtnTitle();
            })
            .fail(function() {})
    }

    function showChangeDia() {
        if (JSON.stringify(changeOrgList) != "{}") {

            showDispatchChange(); //调度模式变更提醒
        }
    }

    function hideToolTip() {
        var tooltip = $('.tooltip-frame');
        tooltip.css({
            display: 'none'
        });
        $('#riderWaybillListBody').empty();

        if (lastHoverTr) {

            lastHoverTr.removeClass('success');
            lastHoverTr = null;
        };
    }

    function isOnElement(x, y, element) {

        if (!element) {
            return;
        };

        var top = element.offset().top;
        var left = element.offset().left;
        var width = element.width();
        var height = element.height();

        if (x < left || y < top || x > width + left || y > top + height) {

            return false;
        };

        return true;
    }

    var detailRequest;
    var detailLoading;
    var loadingDia;
    $('#detail_loading').click(function(event) {
        hideDetailLoading();
        if (detailRequest) {
            detailRequest.abort();
        };
    });

    function showDetailLoading() {
        $('#loading_back').removeClass('hidden');
        $('#detail_loading').removeClass('hidden');
    }

    function hideDetailLoading() {
        $('#detail_loading').addClass('hidden');
        $('#loading_back').addClass('hidden');
    }

    function showWaybillDetail(waybillId) {
        if (detailRequest) {
            detailRequest.abort();
        };

        showDetailLoading();

        var body = '<div id="detailCon">这是ID＝' + waybillId + '的订单详情</div>';
        detailRequest = $.ajax({
            url: "/partner/waybill/detailV2.ajax?waybillId=" + waybillId,
            success: function(result) {
                hideDetailLoading();
                body = '<div>';
                body = body + result + '</div>';
                detailDia = r.ui.showModalDialog({
                    title: '订单详情',
                    body: body,
                    buttons: [],
                    style: {
                        ".modal-footer": {
                            "display": "none"
                        },
                        ".modal-title": {
                            "display": "none"
                        },
                        ".modal-dialog": {
                            "width": "900px"
                        },
                        ".modal-body": {
                            "padding-top": "0"
                        },
                        ".modal-header .close": {
                            "margin-right": "-8px",
                            "margin-top": "-10px"
                        },
                        ".modal-header": {
                            "border-bottom": "none"
                        }
                    }
                });
                detailDia.on("hidden.bs.modal", function() {
                    this.remove();
                    detailDia = null;
                });
            },
            error: function() {}
        });
    }

    function getMinutes(timestamp) {
        var args = Array.prototype.slice.call(arguments);
        var time;
        if (args[1]) {

            var fromtime = args[1];
            time = ((timestamp - fromtime) / 60).toFixed(0);
        } else {
            var date = new Date();
            time = ((timestamp - (date.getTime() / 1000)) / 60).toFixed(0);
        }

        return time;
    }

    function getDaystartUtime(dayBefore, start) {
        return r.utils.getDaystartUtime(dayBefore, start, currentServerTime);
    }

    function buildData(data) {

        data.poiName = busyMessage.poiName;
        data.poiSeq = busyMessage.poiSeq;
        reg = new RegExp("^[0-9]*$");
        data.riderName = "";
        data.riderPhone = "";
        // 判断是数字还是字符串
        if (!reg.test(getRiderName) || getRiderName == "") { // 不是数字的
            data.riderName = getRiderName;
        } else {
            data.riderPhone = getRiderName;
        }
    }

    function getFromTime() {
        var selDayBefore = $('#selectDay').val();
        var fromTime = 0;
        if (selDayBefore == 0) {
            var now = new Date();
            fromTime = (now.getTime() - 6 * 3600 * 1000) / 1000;
        } else {
            fromTime = getDaystartUtime(selDayBefore - 1, true) / 1000;
        }
        fromTime = fromTime.toFixed(0);
        return fromTime;
    }

    function getToTime() {
        var selDayBefore = $('#selectDay').val();
        var toTime = 0;
        if (selDayBefore == 0) {
            var now = new Date();
            toTime = (now.getTime() + 6 * 3600 * 1000) / 1000;
        } else {
            toTime = getDaystartUtime(selDayBefore - 1, false) / 1000;
        }
        toTime = toTime.toFixed(0);

        return toTime;
    }

    // @huanghuijie@meituan.com 在2015/8/13增加了判断是否升序及是否可点击
    function getWayBillList(status, page) {
        if (!orgIdList || orgIdList.length < 1) {
            alert('请至少选择一个站点');
            return;
        };

        $(".loading").removeClass("hide");
        $("#content").addClass("hide");
        var fromTime = getFromTime();
        var toTime = getToTime();
        if (waybillListRequest) {
            waybillListRequest.abort();
        }

        if (status == 50 || status == 99 || status == 110) {
            inputOrderSeq = 1;
            inputOrderField = 'utime';
        };
        var data = new Object();
        data = {
            fromTime: fromTime,
            toTime: toTime,
            orgList: orgIdList,
            status: status,
            page: page,
            size: pageSize,
            inputOrderField: inputOrderField,
            inputOrderSeq: inputOrderSeq,
        };

        buildData(data);

        var url = '/partner/dispatch/waybilllistByAreaAndSearchParam';
        data.areaList = new Array();
        if (!canDoAssgin()) {
            url = '/partner/dispatch/waybilllistByAreaAndSearchParam';
            var areaList = getSelAreas();
            data.areaList = areaList;
        };
        waybillListRequest = $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            traditional: true,
            success: function(data) {
                if (data.code == 1) {
                    alert(data.msg);
                    return false;
                }

                var waybillpage = data.data;
                var page = waybillpage.page;
                var waybillData = waybillpage.bmWaybillViewList;
                var serverTime = waybillpage.systemTime;

                //消除红点提示
                if (status == assignStatus) {
                    $('#manualCount').removeClass('badge');
                    saveNewWaybillTime("isNewWaybillLastTime", serverTime);

                    stopNewWaybillSound();
                };

                if (status == reAssignStatus) {
                    $('#notConfirm').removeClass('badge');
                    saveNewWaybillTime("designateOverTimeLastTime", serverTime);
                };
                if (status == sendbackStatus) {
                    $('#sendbackCount').removeClass('badge');
                    saveNewWaybillTime("refundLastTime", serverTime);
                };
                getWayBillCounts();

                $("#waybilllist").empty();
                $("#footer").empty();

                if (page.totalRows) {

                    if (status == assignStatus) {
                        manualCount = page.totalRows;
                    };
                    setTotal(new String(status), page.totalRows);
                };

                var timeTitle = '剩余';

                if (status == canceledStatus) {
                    timeTitle = '取消';
                };
                if (status == arrivedStatus) {
                    timeTitle = '送达';
                };
                var html = "";
                if (status == "50" || status == "99" || status == "110") {
                    html = '<table class="table  table-bordered table-striped-w table-hover" id="responsiveTable">' +
                        '<thead><tr>' +
                        '<th style="width:150px;">状态(已用时)</th>' +
                        '<th>期望时间</th>' +
                        '<th>商家名称</th><th>送货地址</th>' +
                        '<th style="text-align: center;">骑手</th>' +
                        '<th style="min-width:60px;text-align: center;">操作</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody class="singleSelTbody">';
                } else {
                    // 排序的上下符号
                    var sortI = '<img src="/static/imgs/fa-caret-i.png" alt="排序" />';
                    var sortUp = '<img src="/static/imgs/fa-caret-up.png" alt="排序" />',
                        sortDw = '<img src="/static/imgs/fa-caret-down.png" alt="排序" />';

                    html = '<table class="table  table-bordered table-striped-w" id="responsiveTable">' +
                        '<thead><tr>';
                    if (inputOrderSeq == "1") {
                        switch (inputOrderField) {
                            case "delivered_time": // 剩余时间
                                html += '<th style="cursor: pointer;width:150px;">状态(已用时)' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">期望时间' + sortUp + '</th>' +
                                    '<th style="cursor: pointer;">商家名称' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">送货地址' + sortI + '</th>' +
                                    '<th style="cursor: pointer;text-align: center;">骑手' + sortI + '</th>';
                                break;
                            case "utime": // 已用时状态
                                html += '<th style="cursor: pointer;width:150px;">状态(已用时)' + sortUp + '</th>' +
                                    '<th style="cursor: pointer;">期望时间' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">商家名称' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">送货地址' + sortI + '</th>' +
                                    '<th style="cursor: pointer;text-align: center;">骑手' + sortI + '</th>';
                                break;
                            case "rider_name": // 骑手名
                                html += '<th style="cursor: pointer;width:150px;">状态(已用时)' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">期望时间' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">商家名称' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">送货地址' + sortI + '</th>' +
                                    '<th style="cursor: pointer;text-align: center;">骑手' + sortUp + '</th>';
                                break;
                            case "platform_poi_id": // 骑手名
                                html += '<th style="cursor: pointer;width:150px;">状态(已用时)' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">期望时间' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">商家名称' + sortUp + '</th>' +
                                    '<th style="cursor: pointer;">送货地址' + sortI + '</th>' +
                                    '<th style="cursor: pointer;text-align: center;">骑手' + sortI + '</th>';
                                break;
                            case "recipient_address": // 骑手名
                                html += '<th style="cursor: pointer;width:150px;">状态(已用时)' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">期望时间' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">商家名称' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">送货地址' + sortUp + '</th>' +
                                    '<th style="cursor: pointer;text-align: center;">骑手' + sortI + '</th>';
                                break;
                            default:
                                return;
                        }
                    } else if (inputOrderSeq == "-1") {
                        switch (inputOrderField) {
                            case "delivered_time": // 剩余时间
                                html += '<th style="cursor: pointer;width:150px;">状态(已用时)' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">期望时间' + sortDw + '</th>' +
                                    '<th style="cursor: pointer;">商家名称' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">送货地址' + sortI + '</th>' +
                                    '<th style="cursor: pointer;text-align: center;">骑手' + sortI + '</th>';
                                break;
                            case "utime": // 已用时状态
                                html += '<th style="cursor: pointer;width:150px;">状态(已用时)' + sortDw + '</th>' +
                                    '<th style="cursor: pointer;">期望时间' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">商家名称' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">送货地址' + sortI + '</th>' +
                                    '<th style="cursor: pointer;text-align: center;">骑手' + sortI + '</th>';
                                break;
                            case "rider_name": // 骑手名
                                html += '<th style="cursor: pointer;width:150px;">状态(已用时)' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">期望时间' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">商家名称' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">送货地址' + sortI + '</th>' +
                                    '<th style="cursor: pointer;text-align: center;">骑手' + sortDw + '</th>';
                                break;
                            case "platform_poi_id": // 骑手名
                                html += '<th style="cursor: pointer;width:150px;">状态(已用时)' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">期望时间' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">商家名称' + sortDw + '</th>' +
                                    '<th style="cursor: pointer;">送货地址' + sortI + '</th>' +
                                    '<th style="cursor: pointer;text-align: center;">骑手' + sortI + '</th>';
                                break;
                            case "recipient_address": // 骑手名
                                html += '<th style="cursor: pointer;width:150px;">状态(已用时)' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">期望时间' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">商家名称' + sortI + '</th>' +
                                    '<th style="cursor: pointer;">送货地址' + sortDw + '</th>' +
                                    '<th style="cursor: pointer;text-align: center;">骑手' + sortI + '</th>';
                                break;
                            default:
                                return;
                        }
                    } else { //未排序的时候
                        html += '<th style="cursor: pointer;width:150px;">状态(已用时)' + sortDw + '</th>' +
                            '<th style="cursor: pointer;">期望时间' + sortI + '</th>' +
                            '<th style="cursor: pointer;">商家名称' + sortI + '</th>' +
                            '<th style="cursor: pointer;">送货地址' + sortI + '</th>' +
                            '<th style="cursor: pointer;text-align: center;">骑手' + sortI + '</th>';
                    };

                    html += '<th style="min-width:60px;text-align: center;">操作</th>' +
                        '</tr></thead>' +
                        '<tbody class="singleSelTbody">';
                }
                var dis = '-';
                var statusTime = 0;
                var ridername = '-';
                var remark = '-';
                var wantTime = 0;
                var remainTime = 0;
                var opsType = '';
                curWaybillList = new Array();
                $.each(waybillData, function(i, waybill) {
                    curWaybillList[new String(waybill.id)] = waybill;

                    if (waybill.distance < 100000) {
                        dis = waybill.distance / 1000;
                        dis = dis.toFixed(1) + "km";
                    };
                    if (waybill.distance < 0) {
                        dis = '-';
                    };

                    ridername = '-';
                    if (waybill.ridderNameWithoutPhone && waybill.ridderNameWithoutPhone != '待定') {
                        ridername = waybill.ridderNameWithoutPhone;
                    };
                    remark = '-';
                    if (waybill.remark) {
                        remark = waybill.remark;
                    };

                    opsType = '';
                    if (waybill.status == reAssignStatus) {
                        if (waybill.acceptType == 2) {
                            opsType = '[站长]';
                        } else if (waybill.acceptType == 3) {
                            opsType = '[系统]';
                        };
                    };

                    var date = new Date().getTime();

                    var sTime = serverTime - waybill.utime - 300;

                    html += '<tr value="' + waybill.id + '">';

                    statusTime = 0 - getMinutes(waybill.utime, serverTime);
                    statusTime = statusTime + "min";

                    var selectCorner = '<div class="select-corner"></div>';
                    // 状态
                    if (status == assignStatus || status == notCatchedStatus) { //新订单 需手工派单
                        html += '<td style="padding-left:8px;">' + selectCorner;
                        html += opsType;
                        html += statusNameDic[new String(waybill.status)] + '(' + statusTime + ')';

                        var tags = '';
                        var reasonComment = waybill.reasonComment ? waybill.reasonComment : '';
                        if (waybill.deliveryPriority == 100) {
                            tags += '<br/><span class="bottomSpan greySpan" style="max-width:150px;"><span data-toggle="tooltip" title="优先送" data-placement="top">' +
                                '<span class="new-tip-style red-tip-style">优</span>' +
                                '</span>' + reasonComment + '</span>';
                        } else if (waybill.deliveryPriority == 90 || waybill.isStaypoi == 1) {
                            tags += '<br/><span class="bottomSpan greySpan" style="max-width:150px;"><span data-toggle="tooltip" title="驻店" data-placement="top">' +
                                '<span class="new-tip-style">驻</span></span>' + reasonComment + '</span>';
                        } else if (waybill.deliveryPriority == 190) {
                            tags += '<br/><span class="bottomSpan greySpan" style="max-width:150px;">' +
                                '<span data-toggle="tooltip" title="优先送" data-placement="top" style="margin-right:2px;">' +
                                '<span class="new-tip-style red-tip-style">优</span></span>' +
                                '<span data-toggle="tooltip" title="驻店" data-placement="top">' +
                                '<span class="new-tip-style">驻</span></span>' + reasonComment + '</span>';
                        };

                        html += tags;
                        if (tags == '' && waybill.reasonComment) {
                            html += '<br/><span class="bottomSpan greySpan">' + waybill.reasonComment + '</span>';
                        };

                        html = html + '</td>';
                    } else if (status == reAssignStatus && sTime > 0) { //派单后未确认
                        html += '<td style="padding-left:8px;">' + selectCorner;
                        html += opsType + '派单' + '<span class="redSpan">(' + statusTime +
                            ')</sapn>' + '<br/><span class="bottomSpan" style="max-width:150px;">' +
                            '<span class="greySpan">超时未确认' + '</span></span></td>';
                    } else if (status == sendbackStatus) { //待确认订单
                        html += '<td style="padding-left:8px;">' + selectCorner;
                        html += opsType + statusNameDic[new String(waybill.status)] + '(' + statusTime + ')';

                        var backtime = 0 - getMinutes(waybill.refundTime, serverTime);
                        var backReason = '申请退款';
                        if (waybill.refundStatus == 200) {
                            backReason = '申诉退款';
                        };
                        html = html + '<br><span style="max-width:150px;" class="greySpan bottomSpan">' + backReason + '(' + backtime + ')' + '</span>';
                        html = html + '</td>';
                    } else if (status == canceledStatus || status == arrivedStatus) { //已送达 已取消
                        html += '<td style="padding-left:8px;padding-top:28px;">' + selectCorner;
                        html += opsType + statusNameDic[new String(waybill.status)] + '</td>';
                    } else if (status == allStatus && (waybill.status == canceledStatus || waybill.status == arrivedStatus)) {
                        html += '<td style="padding-left:8px;padding-top:28px;">' + selectCorner;
                        html += opsType + statusNameDic[new String(waybill.status)] + '</td>';
                    } else {
                        html += '<td style="padding-left:8px;padding-top:28px;">' + selectCorner;
                        html += opsType + statusNameDic[new String(waybill.status)] + '(' + statusTime + ')' + '</td>';
                    }

                    wantTime = getTimeString(waybill.diliveryTime * 1000);
                    remainTime = waybill.diliveryTime - serverTime;

                    var saveTimeStr = "";
                    if (waybill.isPrebook) {
                        saveTimeStr += '<span class="blueSpan">[预]</span>' + wantTime;
                    } else {
                        saveTimeStr += wantTime;
                    }
                    if (waybill.status == canceledStatus) {

                        saveTimeStr += '<br/><span class="greySpan bottomSpan">' + getTimeString(waybill.utime * 1000) + '取消</span>';
                        html += "<td>" + saveTimeStr + "</td>";
                    } else if (waybill.status == arrivedStatus) {

                        saveTimeStr += '<br/><span class="greySpan bottomSpan">' + getTimeString(waybill.utime * 1000) + '送达</span>';
                        html += "<td>" + saveTimeStr + "</td>";
                    } else {
                        var remainTime = Math.floor(remainTime),
                            minTime = Math.floor(Math.abs(remainTime) / 60);
                        if (remainTime < 0) {
                            saveTimeStr += '<br/><sapn class="redSpan bottomSpan">超出' + minTime + "min</sapn>";
                        } else {
                            if (((waybill.status == 0 || waybill.status == 15 || waybill.status == 10) && minTime <= 20) ||
                                ((waybill.status == 20) && minTime <= 15) ||
                                ((waybill.status == 30) && minTime <= 10)
                            ) {
                                saveTimeStr += '<br/><span class="redSpan bottomSpan">剩余' + minTime + "min</span>";
                            } else {
                                saveTimeStr += '<br/><span class="greySpan bottomSpan">剩余' + minTime + "min</span>";
                            }
                        }
                        html += "<td>" + saveTimeStr + "</td>";
                    }

                    var url = 'http://ditu.amap.com/search?query=';
                    var orgName = waybill.orgName ? waybill.orgName : '-';

                    var senderNameTemp = waybill.senderName,
                        recAddrTemp = waybill.recAddr,
                        payMoney = new Number(waybill.pkgPrice).toFixed(1),
                        orgNameTemp = !waybill.orgName ? "未分配站点" : waybill.orgName;
                    senderNameTemp = senderNameTemp.length > 20 ? senderNameTemp.substr(0, 19) + "..." : senderNameTemp;
                    orgNameTemp = orgNameTemp.length > 6 ? "..." + orgNameTemp.substr(orgNameTemp.length - 6, 6) : orgNameTemp;
                    recAddrTemp = recAddrTemp.length > 30 ? recAddrTemp.substr(0, 29) + "..." : recAddrTemp;
                    if (waybill.poiSeq) {
                        senderNameTemp += '#' + waybill.poiSeq;
                    };
                    remark = remark.length > 30 ? remark.substr(0, 30) + "..." : remark;
                    // 订单路程
                    var billStrTemp = '<td>' +
                        '<span data-toggle="tooltip" title="搜同店" data-placement="top" name="searchBuyName"' +
                        ' data-name="' + waybill.senderName + '" style="font-size:12px;float:left;color:#fbfbfb;margin-right: 4px;">' +
                        '<i class="fa fa-lg fa-search"></i></span>' +
                        '<div class="pull-left">' +
                        '<span class="td-span-title">' + senderNameTemp + '</span><br/>' +
                        '<span class="td-span-main bottomSpan greySpan">' + dis + '&nbsp;';
                    if (waybill.logisticsOfflinePayOrderRecord == 1) {
                        payMoney = new Number(waybill.planPayAmount).toFixed(1);
                        billStrTemp = billStrTemp + '<span class="redSpan">需垫付￥' + payMoney + '</span>';
                    }else if(payMoney >= 80){
                        billStrTemp = billStrTemp + '<span style="color:rgb(254,159,14)">大额单￥' + payMoney + '</span>';
                    }else {
                        billStrTemp = billStrTemp + '￥' + payMoney;
                    }
                    billStrTemp = billStrTemp + '</span>' + '</div>' + '</td>';

                    billStrTemp = billStrTemp + '<td>' +
                        '<span class="td-span-title">' + recAddrTemp + '</span>' + '<span class="td-span-main bottomSpan greySpan">' + remark + '</span>' + '</td>';
                    html += billStrTemp;
                    // 骑手名
                    if (ridername == '-') {
                        html = html + '<td style="padding: 16px 0 0 0;">' + '<span style="padding-left:20px;">' + ridername + '</span>'

                        + '<br><span style="padding-left:20px;" class="td-span-main bottomSpan greySpan">' + orgNameTemp + '</span>' + '</td>';
                    } else {
                        var searchStr = '<span data-toggle="tooltip" title="搜骑手" data-placement="top" name="searchBuyRiderName"' +
                            ' data-name="' + ridername + '" style="color:#fbfbfb;font-size:12px;float:left;margin-right: 4px;">' +
                            '<i class="fa fa-lg fa-search"></i></span>';
                        html = html + '<td style="padding: 16px 0 0 0;">' + searchStr + ridername + '<br><span style="padding-left:20px;" class="td-span-main bottomSpan greySpan">' + orgNameTemp + '</span>' + '</td>';
                    }

                    // 操作
                    html = html + '<td style="text-align: center;padding-top:28px;">' + '<span data-toggle="tooltip" title="查看详情" data-placement="top" name="detail">' + '<img src="/static/imgs/info-cicrle.png" /> </span>';
                    // TODO 暂时不做限制
                    // if (waybill.status != arrivedStatus && waybill.status != canceledStatus) {
                    html = html + '<a target="_blank" data-toggle="tooltip" title="地图派单" data-placement="top" href="' + '/partner/dispatch/mapDispatch?waybillId=' + waybill.id + '" style="margin-left:3px;">' + '<img src="/static/imgs/map-icon.png" /></a>'; //<i class="fa fa-map-marker fa-lg filter-color-icon"></i>
                    // };

                    html = html + '</td></tr>';
                });
                html = html + '</tbody></table>';

                $("#waybilllist").append(html);

                $('[data-toggle="tooltip"]').tooltip();
                $('#responsiveTable').delegate('[name=detail]', 'click', function(event) {

                    if (event && event.stopPropagation) {
                        event.stopPropagation();
                    } else {
                        window.event.cancelBubble = true;
                    }

                    var orderid = $(this).parent().parent().attr('value');
                    showWaybillDetail(orderid);
                });
                $('#responsiveTable').delegate('[name=searchBuyName]', 'click', function(event) {
                    if (event && event.stopPropagation) {
                        event.stopPropagation();
                    } else {
                        window.event.cancelBubble = true;
                    }
                    $("#busyMessage").val($(this).attr("data-name"));
                    if (!!$(this).attr("data-name")) {
                        $("#busyMessage").next().css("color", "#777");
                    };

                    busyMessage.poiName = $(this).attr("data-name");
                    busyMessage.poiSeq = "";

                    getWayBillList(curWaybillStatus, 1);
                    _getRiderList();
                });
                $('#responsiveTable').delegate('[name=searchBuyRiderName]', 'click', function(event) {
                    if (event && event.stopPropagation) {
                        event.stopPropagation();
                    } else {
                        window.event.cancelBubble = true;
                    }
                    $("#getRiderName").val($(this).attr("data-name"));
                    if (!!$(this).attr("data-name")) {
                        $("#getRiderName").next().css("color", "#777");
                    };
                    getRiderName = $("#getRiderName").val();
                    getWayBillList(curWaybillStatus, 1);
                    _getRiderList();
                });

                //order list click
                var selClass = 'success';
                curSelOrderId.length = 0;
                $('#responsiveTable tbody tr').hover(function() {
                    $(this).find('.fa-search').css("color", "#3ac6f4");
                    $(this).css("background", "#f0f0f0");
                }, function() {
                    $(this).css("background", "#fafafa");
                    if ($(this).hasClass('success')) {
                        $(this).find('.fa-search').css("color", "#E9FFFF");
                    } else {
                        $(this).find('.fa-search').css("color", "#fafafa");
                    }
                });
                $('#responsiveTable').delegate('tbody tr', 'click', function(event) {

                    var $this = $(this);
                    if ($(this).hasClass('clicked')) {
                        //double click
                        showWaybillDetail($(this).attr('value'));
                        $this.removeClass('clicked');
                        // return;
                    } else {
                        $this.addClass('clicked');
                        setTimeout(function() {
                                $this.removeClass('clicked');
                            },
                            300
                        );
                    }
                    if ($(this).hasClass(selClass)) {

                        var id = $(this).attr('value');
                        var j = -1;

                        for (var i = 0; i < curSelOrderId.length; i++) {

                            if (id == curSelOrderId[i]) j = i;
                            $(this).removeClass(selClass);

                        }

                        if (j > -1) curSelOrderId.splice(j, 1);

                        if (curSelOrderId.length == 1 || curSelOrderId.length == 0) {
                            _getRiderList();
                        };
                        return;
                    };
                    // 组织一个可多选  现在是可以选一个站点多选
                    var areaList = $('#selectArea').val(),
                        areaLength = areaList.length;
                    if (typeof areaList == "string") {
                        areaLength = 1;
                    };
                    if (areaLength == 1 || orgIdList.length == 1) {
                        $(this).addClass(selClass);
                        curSelOrderId.push($(this).attr('value'));
                    } else {
                        curSelOrderId = [];
                        $(this).parent().children('tr').each(function(index, el) {

                            $(this).removeClass(selClass);
                        });
                        $(this).addClass(selClass);
                        curSelOrderId.push($(this).attr('value'));

                    }

                    if (curSelOrderId.length != 0) $('#btn-clear').removeClass('hidden');
                    if (curSelOrderId.length == 1 || curSelOrderId.length == 0) {
                        _getRiderList();
                    };
                });

                //清除多选项
                $('#btn-clear').click(function() {
                    curSelOrderId = [];
                    clearChooecdNumber();
                    _getRiderList();
                })

                //设置分页信息
                if (page.totalPage > 1) {
                    var pageHtml = '<nav>' +
                        '  <ul class="pagination">';

                    pageHtml += '<li';
                    if (page.currentPage == 1) {
                        pageHtml += ' class="disabled"';
                    }
                    pageHtml += ' page=' + (page.currentPage - 1) + '><a href="#' + status + '" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                    if (page.totalPage > 6) {
                        pageHtml += '<li page="1"><a href="#' + status + '">首页</a></li>';
                    };
                    if (page.totalPage > 6 && (page.totalPage - page.currentPage) <= 3) {
                        pageHtml += '<span class="disabled pull-left pagination-span">...</span>';
                    };
                    for (var i = page.leftNum; i <= page.rightNum; i++) {
                        if (i == page.currentPage) {
                            pageHtml += '<li class="active"><a href="#' + status + '">' + i + ' <span class="sr-only">(current)</span></a></li>';
                        } else {
                            pageHtml += '<li page=' + i + '><a href="#' + status + '">' + i + '</a></li>';
                        }
                    }
                    if (page.totalPage > 6 && (page.totalPage - page.currentPage) > 3) {
                        pageHtml += '<span class="disabled pull-left pagination-span">...</span>';
                    };
                    if (page.totalPage > 6) {
                        pageHtml += '<li page=' + page.totalPage + '><a href="#' + status + '">末页</a></li>';
                    };
                    pageHtml += '<li';
                    if (page.currentPage == page.totalPage) {
                        pageHtml += ' class="disabled"';
                    }
                    pageHtml += ' page=' + (page.currentPage + 1) + '><a href="#' + status + '" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                    pageHtml += '  </ul></nav>';
                    $("#footer").append(pageHtml);
                }
                $(".loading").addClass("hide");
                $("#content").removeClass("hide");
            }
        });
    }

    function overTime(status, time) {
        if ((status == catchedStatus && time <= 15) ||
            (status == onWayStatus && time <= 10) ||
            (status == assignStatus && time <= 20) ||
            (status == reAssignStatus && time <= 20) ||
            (status == notCatchedStatus && time <= 20)) {
            return true;
        } else {
            return false;
        }
    }

    function ajustTipFramePos(theTr) {
        if (!theTr) {
            return;
        };

        var h = theTr.offset().top;
        var divh = $('#main-container').offset().top;

        var tooltip = $('.tooltip-frame');
        var top = h - divh - 20;
        var mainHeight = $('#main-container').innerHeight();
        var th = tooltip.innerHeight();
        if (mainHeight - divh < th + top) {
            top = mainHeight - th - divh;
        };
        var left = 244;
        if (isShowScroll()) {
            left = 236;
        };
        tooltip.css({
            display: 'block',
            top: top,
            left: left
        });
    }

    function isShowScroll() {
        var conHeight = $('#riderListDiv').height();
        var headHeight = $('#riderListHead').innerHeight();
        var tableHeight = $('.riderlist-table').innerHeight();

        return headHeight + tableHeight > conHeight;
    }

    function canAssign() {
        return (curWaybillStatus == assignStatus || curWaybillStatus == reAssignStatus || curWaybillStatus == catchedStatus || curWaybillStatus == onWayStatus || curWaybillStatus == 0 || curWaybillStatus == 10 || curWaybillStatus == 100 || curWaybillStatus == 110);
    }

    function showTipFrame() {
        if (!lastHoverTr) {
            return;
        };
        var theTr = lastHoverTr;
        ajustTipFramePos(theTr);

        var riderId = theTr.attr('value');
        var rider = curRiderList[riderId];

        var orgName = rider.orgName;
        if (orgName.length > 6) {
            orgName = "..." + orgName.substr(orgName.length - 6, 6)
        };

        var riderMobile = "[" + orgName + "]" + rider.name + ":" + rider.mobile;
        $('#rider-mobile').html(riderMobile);

        if (rider.workStatus != 0) {
            $('.riderMap').show();
            var city = getSelCityid();

            var mapUrl = "/partner/dispatch/mapRiderState?id=" + rider.id + '&city=' + city;
            if (orgRoleType != 1) {
                var areaList = getSelAreas();
                if (areaList.length == 1) {
                    mapUrl = mapUrl + '&areaId=' + areaList[0];
                };
            };

            $('#riderMap').attr("href", mapUrl);
        } else {
            $('.riderMap').hide();
        };
        var city = getSelCityid();
        $('#riderOrder').attr("href", "/playback/home?tab=2&phone=" + rider.mobile);
        $('#rider-outTime').empty();
        showRiderStatusOnTip(rider);
        $('.status-menu').addClass('hidden');
        getRiderWaybillList(riderId);
    }

    function showRiderStatusOnTip(rider) {
        $('#sigle-control').attr('rid', rider.id);
        $('#sigle-control').attr('value', rider.workStatus);
        $('#sigle-control').empty();
        $('#sigle-control').append(getRiderStatusString(rider.workStatus) + '<span class="caret"></span>');
    }

    function setRiderStatus(rider, newStatus) {
        var setList = new Array();
        setList.push(rider.id);
        var callback = function(code, data) {
            var msg = '修改失败';
            if (data) {
                msg = data.msg;
            };
            if (code == 0) {
                rider.workStatus = newStatus;
                var riderId = $('#sigle-control').attr('rid');
                if (riderId == rider.id) {
                    showRiderStatusOnTip(rider);
                };
                showTip(msg, 'info');
                initStatuSelect(10, 0);
                _getRiderList();
            } else {
                showTip(msg, 'danger');
            }
        }
        _setRiderStatus(setList, newStatus, callback);
    }

    function showMutiSetAlert(newStatus) {
        if (mutiSetDia) {
            return;
        };
        var setRiderList = new Array();
        var nameStr = '';
        var preStatus = -1;
        var toStatus = getRiderStatusString(newStatus);
        toStatus = '[' + toStatus + ']';

        $('.rider-select').each(function() {

            if (this.checked) {
                var tr = $(this).parents('tr');
                var riderId = tr.attr('value');
                var rider = curRiderList[riderId];
                setRiderList.push(rider);
            };
        });
        if (setRiderList.length < 1) {
            return;
        };
        var setList = new Array();
        for (var i = 0; i < setRiderList.length; i++) {
            var rider = setRiderList[i];
            setList.push(rider.id);
            if (i < 5) {
                if (i != 0) {
                    nameStr += '、';
                };
                nameStr = nameStr + rider.name;
            }
            if (preStatus == -1) {
                preStatus = rider.workStatus;
            } else if (preStatus != rider.workStatus) {
                preStatus = -2;
            }
        };
        if (setList.length > 5) {
            nameStr += '...等';
        };
        if (preStatus == newStatus) {
            alert('与变更后状态相同');
            return;
        };
        if (preStatus == -2) {
            preStatus = '';
        } else {
            preStatus = getRiderStatusString(preStatus);
            preStatus = '由<span style="color:#00abe4">' + preStatus + '</span>';
        }

        var body = '';
        var infoP = '<p style="color:#000;font-size:13px;">将"' + nameStr + '共' + '<span style="color:#00abe4">' + setList.length + '个</span>' + '骑手"' + preStatus + '变更为' + '<span style="color:#00abe4">' + toStatus + '</span>' + '</p>';
        body += infoP;
        body += '<p style="color:#ff5a5a">注：状态变更后骑手将收到消息通知</p>';
        mutiSetDia = r.ui.showModalDialog({
            title: '骑手状态变更',
            body: body,
            buttons: [

                r.ui.createDialogButton('ok', '确认变更', function() {
                    mutiSetRiderStatus(setList, newStatus);
                    return true;
                }),
                r.ui.createDialogButton('close', '取消', function() {
                    $('#riderListDiv').removeClass('status-control');
                })
            ],
            style: {
                ".modal-footer": {
                    "border-top": "none",
                    "text-align": "center",
                    "padding-top": "0",
                },
                ".modal-header": {
                    "padding-top": "10px",
                    "padding-bottom": "10px",
                },
                ".modal-content": {
                    "margin-top": "200px",
                },
                ".btn.btn-success": {
                    "margin": "0 25px 0 10px",
                    "background-color": "#00ABE4",
                    "border": "1px solid #00ABE4",
                },
                ".btn.btn-success:hover": {
                    "background-color": "#00ABE4",
                },
                ".btn-default": {
                    "width": "82px",
                }
            }
        });
        mutiSetDia.on("hidden.bs.modal", function() {
            this.remove();
            mutiSetDia = null;
        });
    }

    function mutiSetRiderStatus(setList, newStatus) {

        var callback = function(code, data) {
            var msg = '修改失败';
            if (data) {
                msg = data.msg;
            };
            if (code == 0) {
                $('#riderListDiv').removeClass('status-control');
                showTip(msg, 'info');
                initStatuSelect(10, 0);
                _getRiderList();
            } else {
                showTip(msg, 'danger');
            }
        }
        _setRiderStatus(setList, newStatus, callback);
    }

    function _setRiderStatus(setList, newStatus, callback) {
        $.ajax({
                url: '/partner/dispatch/changeWorkStatus.ajax',
                type: 'GET',
                dataType: 'json',
                data: {
                    riderList: setList,
                    status: newStatus
                },
                traditional: true
            })
            .done(function(data) {
                if (callback) {
                    callback(data.code, data);
                };
            })
            .fail(function() {
                if (callback) {
                    callback(-1);
                };
            })
            .always(function() {});
    }

    function getRiderList(status, waybillId) {

        if (orgIdList.length < 1) {
            return;
        };

        $('#riderListBody').empty();
        var data = {
            orgList: orgIdList,
            status: status,
            waybillId: waybillId
        };
        if (riderListRequest) {
            riderListRequest.abort();
        };
        var url = '/partner/dispatch/riderlist';
        if (!canDoAssgin()) {
            url = '/partner/dispatch/riderlist';
            var areaList = getSelAreas();
            data.areaList = areaList;
            data.orgList = null;
        };
        riderListRequest = $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data,
                traditional: true,
            })
            .done(function(result) {
                fillRiderList(result, status);
                if (searchRiderName != "") {
                    $("#riderListDiv").scrollTop(0);
                    setTimeout(fnSerachName, 200);
                };
            })
    }

    // hover显示骑手的状态
    function showRiderStatus(_this) {
        var _thisVal = _this.attr('value');
        if (!_thisVal) {
            return;
        };
        if (lastHoverTr) {
            if (lastHoverTr.attr('value') == _thisVal) {
                return;
            };
        };
        $('#riderWaybillListBody').empty();

        if (lastHoverTr) {
            lastHoverTr.removeClass('success');
        };
        $(this).addClass('success');
        lastHoverTr = _this;

        if (trTimeout) {
            clearTimeout(trTimeout);
        };
        trTimeout = setTimeout(showTipFrame, 400);

        // TODO
        var _thiShade = _this.find(".riderlist-shade-tr");
        var riderChooceNumber = $("#responsiveTable .success").length;
        var riderId = _this.attr('value');
        var rider = curRiderList[riderId];

        setAssignBtnClass(rider, _thiShade);

        if (_this.find("button").last().hasClass('riderlist-shade-tr')) {
            _thiShade.removeClass('greyBg');
            _thiShade.removeClass('disabled');
            _thiShade.removeClass('hidden');

            if (riderChooceNumber <= 0) {
                _thiShade.addClass('hidden');
            } else {

                if (allSelectedBelongTo(rider)) { //订单属于该骑手
                    _thiShade.html('订单已属于他');
                    _thiShade.addClass('greyBg');
                    _thiShade.addClass('disabled');
                } else {
                    if (allSelectedHasNoRider(rider)) {
                        _thiShade.html('指派<span style="color:#fad870;font-weight: bold;">' + riderChooceNumber + '</span>单');
                    } else {
                        _thiShade.html('改派<span style="color:#fad870;font-weight: bold;">' + riderChooceNumber + '</span>单');
                    }
                }
            }
        }
        if (canDispatch != 1) {
            _thiShade.addClass('disabled');
            _thiShade.addClass('greyBg');
        };
        if (!shouldShowAssignBtn()) {
            _thiShade.addClass('hidden');
        };
    };

    function shouldShowAssignBtn() {
        var all = false;
        for (var i = 0; i < curSelOrderId.length; i++) {
            var waybill = curWaybillList[new String(curSelOrderId[i])];
            if (waybill.status != arrivedStatus && waybill.status != canceledStatus) {
                all = true;
                break;
            }
        }
        return all;
    }

    // 派单按钮
    function setAssignBtnClass(rider, _thiShade) {
        if (curSelOrderId.length == 0) return;
        _thiShade.removeClass('reassign');

        for (var i = 0; i < curSelOrderId.length; i++) {
            var waybill = curWaybillList[new String(curSelOrderId[i])];
            if (waybill.status == onWayStatus || waybill.status == catchedStatus) {
                _thiShade.addClass('reassign');
                break;
            }
        }
    }

    function allSelectedBelongTo(rider) {
        var to = true;
        for (var i = 0; i < curSelOrderId.length; i++) {
            var waybill = curWaybillList[new String(curSelOrderId[i])];
            if (waybill.ridderNameWithoutPhone != rider.name) {
                return false;
            }
        }
        return to;
    }

    function allSelectedHasNoRider() {
        var has = true;
        for (var i = 0; i < curSelOrderId.length; i++) {
            var waybill = curWaybillList[new String(curSelOrderId[i])];
            if (waybill.ridderNameWithoutPhone && waybill.ridderNameWithoutPhone != '待定') {
                has = false;
                break;
            }
        }
        return has;
    }

    // hover 骑手状态消失的函数
    function hideRiderStatus(_this) {
        var x = event.pageX;
        var y = event.pageY;
        if (!isOnElement(x, y, $(".tooltip-frame"))) {
            hideToolTip();
            _this.find(".riderlist-shade-tr").addClass('hidden');
        };
    };

    function getRiderStatusString(status) {
        var string = '未知';
        switch (status) {
            case 1:
            case "1":
                string = '在岗';
                break;
            case 0:
            case "0":
                string = '离岗';
                break;
            case 2:
            case "2":
                string = '忙碌';
                break;
        }
        return string;
    }
    // 传入一个骑手的数组 最终返回一个tr字符串
    function fillRider(riderArr) {
        var tempStr = "";
        $.each(riderArr, function(index, rider) {
            dis = rider.distant / 1000;
            dis = dis.toFixed(1);
            if (dis > 100) dis = '-';

            var checkBox = '<input type="checkbox" class="check rider-select">';
            var nameTd = checkBox + '<span>';
            var newSpanColorClass = "new-tip-style",
                partSpanColorClass = "part-tip-style";
            if (rider.workStatus == 0) {
                newSpanColorClass = "new-tip-style greySpan greyBoder"
                partSpanColorClass = "part-tip-style greySpan greyBoder";
            };
            if (rider.isNew && rider.jobType != 2) {
                nameTd = checkBox + '<span class="rider-name-td" style="margin-top:-2px;">' +
                    '<span data-toggle="tooltip" title="新手:入职未满7天" ' +
                    'data-placement="right" class="' + newSpanColorClass + '">新</span>' +
                    '</span>';
            };
            if (rider.jobType == 2 && !rider.isNew) {
                nameTd = checkBox + '<span class="rider-name-td" data-placement="right" data-toggle="tooltip" title="兼职">' +
                    '<span style="margin-top:-2px;" class="' + partSpanColorClass + '">兼</span>' +
                    '</span>';
            };
            if (rider.isNew && rider.jobType == 2) {
                nameTd = checkBox + '<span class="rider-name-td" style="margin-top: -8px;">' +
                    '<span data-toggle="tooltip" title="新手：入职未满7天" ' +
                    'data-placement="right" class="' + newSpanColorClass + '">新</span>' +
                    '<br/><span data-toggle="tooltip" title="兼职" data-placement="right" class="' + partSpanColorClass + '">' +
                    '兼</span>' +
                    '</span>';
            };
            var ridername = rider.name;
            ridername = ridername.length > 4 ? "..." + ridername.substr(ridername.length - 4, 4) : ridername;
            nameTd += '<span class="riderName">' + ridername + '</span></span>';
            var shadeTemp = "";
            if (rider.workStatus != 0) {
                shadeTemp = '<button class="riderlist-shade-tr hidden"></button>';
            };
            if (rider.workStatus == 0) {
                tempStr += '<tr value=' + rider.id + ' class="greySpan" style="background:#fbfbfb;">';
            } else {
                tempStr += '<tr value=' + rider.id + '>';
            }

            tempStr = tempStr + '<td>' + nameTd + '</td>';
            if (rider.workStatus == 0) {
                tempStr += '<td style="color:#999"><span>' + rider.currentWaybillCount + '</span>/' + rider.totallWaybillCount + '</td>';
            } else {
                tempStr += '<td> <span style="color:#2a2a2a;font-weight:bold;">' + rider.currentWaybillCount + '</span>/' + rider.totallWaybillCount + '</td>';
            }
            if (rider.online == 0 && rider.workStatus != 0) {
                tempStr += '<td style="color:#ff5a5a;">掉线' + '</td>';
            } else if (rider.workStatus == 0) {
                tempStr += '<td style="color:#999">' + dis + '</td>';
            } else {
                tempStr += '<td>' + dis + '</td>';
            }
            if (rider.recommendDes) {
                tempStr += '<td>' + rider.recommendDes + shadeTemp + '</td>';
            }else{
                tempStr += '<td>' + '-' + shadeTemp + '</td>';
            }

            tempStr += '</tr>';
        });
        return tempStr;
    }

    function _fillRiderList(onlineArr, busyArr, unlineArr, status) {

        if (status == undefined) {
            var cursel = $('#selectStatus').val();
            status = riderStatusArr[cursel];
        };
        var tableBody = $('#riderListBody');
        tableBody.empty();
        var bodyHtml = '';

        if (onlineArr.length != 0) {
            if (status == 10) {
                bodyHtml += "<tr class='sort-remove-tr'><td colspan='4' style='background:#edf6d9;color:#8eba2f;'>在岗（" + onlineArr.length + "）</td></tr>";
            };
            bodyHtml += fillRider(onlineArr);
        };
        if (busyArr.length != 0) {
            if (status == 10) {
                bodyHtml += "<tr class='sort-remove-tr'><td colspan='4' style='background:#ffedeb;color:#ff5a5a;'>忙碌（" + busyArr.length + "）</td></tr>";
            };
            bodyHtml += fillRider(busyArr);
        };
        if (unlineArr.length != 0) {
            if (status == 10) {
                bodyHtml += "<tr class='sort-remove-tr'><td colspan='4' style='background:#e7eaec;color:#999;'>离岗（" + unlineArr.length + "）</td></tr>";
            };
            bodyHtml += fillRider(unlineArr);
        };

        tableBody.append(bodyHtml);
        $('[data-toggle="tooltip"]').tooltip();
    }
    // 侧边骑手列表
    function fillRiderList(riderList, status) {
        hideToolTip();
        curRiderList = new Array();

        var onlineArr = new Array(), //在岗
            busyArr = new Array(), //忙碌
            unlineArr = new Array(); //离岗

        $.each(riderList, function(index, rider) {
            if (rider.workStatus == 1) {
                onlineArr.push(rider);
            } else if (rider.workStatus == 2) {
                busyArr.push(rider);
            } else if (rider.workStatus == 0) {
                unlineArr.push(rider);
            }
            curRiderList[rider.id] = rider;
        });

        sortRiderListIfNeeded(onlineArr);
        sortRiderListIfNeeded(busyArr);
        _fillRiderList(onlineArr, busyArr, unlineArr, status);

        var cursel = $('#selectStatus').val();
        var title = '';
        switch (cursel) {
            case '0':
                title = '全部';
                break;
            case '1':
                title = '在岗';
                break;
            case '2':
                title = '忙碌';
                break;
            case '3':
                title = '离岗';
                break;
        }
        var selOp = $('#selectStatus option:selected');
        selOp.text(title + '(' + riderList.length + ')');
        selectDoBtnTitle();
    }

    function getRiderWaybillList(riderId) {
        $.ajax({
                url: '/partner/dispatch/riderWaybillList',
                type: 'POST',
                dataType: 'json',
                data: {
                    riderId: riderId
                },
            })
            .done(function(result) {
                fillRiderWaybillList(result);
            })
            .fail(function() {})
            .always(function() {});
    }

    function getTimeString(timestamp) {
        var date = new Date(timestamp);
        var str = getFormatedString(date.getHours()) + ':' + getFormatedString(date.getMinutes());
        return str;
    }

    function getFormatedString(value) {
        if (value > 9) {
            return new String(value);
        };

        return '0' + new String(value);
    }

    function shouldFillTipFrame() {
        if ($('.tooltip-frame').css('display') == 'none') {
            return false;
        };
        return true;
    }

    function fillRiderWaybillList(result) {
        var table = $('#riderWaybillListBody');
        var html = '';
        table.empty();

        var code = result.code;
        if (code != 100) {
            return;
        };

        if (!shouldFillTipFrame()) {
            return;
        };

        var waybillList = result.data.waybillList;
        var dis = '-';
        var statusTime = 0;
        $.each(waybillList, function(index, waybill) {

            if (waybill.distant < 100000) {
                dis = waybill.distant / 1000;
                dis = dis.toFixed(1);
            };
            statusTime = 0 - getMinutes(waybill.utime);
            var nowMis = new Date();
            var remainTime = waybill.deliveredTime - nowMis.getTime() / 1000;
            var sysTime = result.data.systemTime;
            var sTime = sysTime - waybill.utime - 300;

            html += '<tr class="waybill">';
            statusTime = statusTime + "min";
            if (waybill.status == 15) {

                opsType = '';
                if (waybill.acceptType == 2) {
                    opsType = '[站长]';
                } else if (waybill.acceptType == 3) {
                    opsType = '[系统]';
                };
                if (sTime > 0) {
                    html += '<td style="padding-top:20px;">' + opsType +
                        '派单' + '<span class="redSpan">(' + statusTime +
                        ')</span></td>';
                } else {
                    html += '<td style="padding-top:20px;">' + opsType +
                        '派单' + '<span>(' + statusTime +
                        ')</span></td>';
                }

            } else {
                html = html + '<td style="padding-top:20px;">' + statusNameDic[new String(waybill.status)] + '(' + statusTime + ')' + '</td>';
            }

            var timeStr = "";
            var wantTime = getTimeString(waybill.deliveredTime * 1000);
            if (waybill.isPrebook) {
                timeStr += '<span class="blueSpan">[预]</span>' + wantTime;
            } else {
                timeStr += wantTime;
            }

            var minTime = Math.floor(Math.abs(remainTime) / 60);

            if (waybill.status != canceledStatus && waybill.status != arrivedStatus) { // 已送达或者已取消
                if (Math.floor(remainTime) < 0) {
                    timeStr += '<br/><sapn class="redSpan bottomSpan">超出' + minTime + "min</sapn>";
                } else if (((waybill.status == 0 || waybill.status == 15 || waybill.status == 10) && minTime <= 20) ||
                    ((waybill.status == 20) && minTime <= 15) ||
                    ((waybill.status == 30) && minTime <= 10)
                ) {
                    timeStr += '<br/><span class="redSpan bottomSpan">剩余' + minTime + "min</span>";
                } else {
                    timeStr += '<br/><span class="greySpan bottomSpan">剩余' + minTime + "min</span>";
                }
            }
            html += "<td>" + timeStr + "</td>";

            // 订单路程
            var senderNameTemp = waybill.senderName,
                recAddrTemp = waybill.recipientAddress,
                payMoney = new Number(waybill.pkgPrice).toFixed(1);
            senderNameTemp = senderNameTemp.length > 8 ? senderNameTemp.substr(0, 8) + "..." : senderNameTemp;
            recAddrTemp = recAddrTemp.length > 12 ? recAddrTemp.substr(0, 12) + "..." : recAddrTemp;
            if (waybill.poiSeq) {
                senderNameTemp += "#" + waybill.poiSeq;
            };
            var remark = "-";
            if (waybill.remark) {
                remark = waybill.remark;
            };

            var billStrTemp = '<td>' +
                // '<div class="td-div-left-bill pull-left">' +
                '<span class="td-span-title">' + senderNameTemp + '</span><br/>' +
                '<span class="td-span-main bottomSpan greySpan">&nbsp;' + dis + 'km</span>' +
                '<td>' +
                '<span class="td-span-title">' + recAddrTemp + '</span>' +
                '<span class="td-span-main bottomSpan greySpan">￥' + payMoney + '</span>' +
                '</td>';
            html += billStrTemp;

            html += '</tr>';

            var sysTime = result.data.systemTime;
            var sTime = sysTime - waybill.utime - 5 * 60;

        });

        table.append(html);

        if (result.data.overTimeCount != 0) {
            $('#rider-outTime').html('[' + result.data.overTimeCount + '单超时]');
        }

        ajustTipFramePos(lastHoverTr);
    }

    function reAssignToRider(riderId, waybillId, reason) {
        $.ajax("/partner/dispatch/reDispatchWaybill.ajax", {
            method: 'GET',
            data: {
                waybillId: waybillId,
                uid: riderId,
                reason: reason
            },
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function(data) {

                if (!data.suc) {

                    showMsg(data.msg);
                } else {

                    clearShadeVal();
                    showMsg('配送单已经成功指派给骑士');
                }
                getWayBillList(curWaybillStatus, 1);
                curSelOrderId = [];
                _getRiderList();
            }
        });
    }

    function mulReAssignToRider(riderId, waybillId, reason) {
        $.ajax("/partner/dispatch/mulReDispatchWaybill.ajax", {
            method: 'GET',
            data: {
                waybillIds: waybillId,
                uid: riderId,
                reason: reason
            },
            dataType: 'json',
            traditional: true,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function(data) {
                if (!data.suc) {
                    showMsg(data.msg);
                } else {

                    clearShadeVal();
                    if (data.failCount == 0) {
                        showMsg('派单成功（' + data.sucCount + '）');
                    } else {
                        showMsg('派单成功（' + data.sucCount + ')，派单失败（' + data.failCount + ')');
                    }
                }
                getWayBillList(curWaybillStatus, 1);
                curSelOrderId = [];
                _getRiderList();
            }
        });
    }

    function assignToRider(riderId, waybillId) {
        hideToolTip();
        $.ajax("/partner/dispatch/appointRider.ajax", {
            method: 'POST',
            data: {
                waybillId: waybillId,
                uid: riderId
            },
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function(data) {
                if (!data.suc) {
                    showMsg(data.msg);
                } else {
                    clearShadeVal();
                    showMsg('配送单已经成功指派给骑士');
                }
                getWayBillList(curWaybillStatus, 1);
                curSelOrderId = [];
                _getRiderList();
            }
        });
    }

    function mulAssignToRider(riderId, waybillIds) {
        hideToolTip();
        $.ajax("/partner/dispatch/mulAppointRider.ajax", {
            method: 'POST',
            data: {
                waybillIds: waybillIds,
                uid: riderId
            },
            dataType: 'json',
            traditional: true,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function(data) {
                if (!data.suc) {
                    showMsg(data.msg);
                } else {
                    clearShadeVal();
                    if (data.failCount == 0) {
                        showMsg('派单成功（' + data.sucCount + '）');
                    } else {
                        showMsg('派单成功（' + data.sucCount + '），派单失败（' + data.failCount + '）');
                    }

                }
                getWayBillList(curWaybillStatus, 1);

                curSelOrderId = [];
                _getRiderList();
            }
        });
    }

    function getAllAreaList() {
        var allList = new Array();
        for (areaId in deliveryAreaList) {
            allList.push(areaId);
        }
        return allList;
    }

    function getWayBillCounts() {

        if (orgIdList.length < 1) {
            return;
        };
        var fromTime = getFromTime();
        var toTime = getToTime();

        if (countRequest) {
            countRequest.abort();
        };
        var explosionAreaList = getAllAreaList();

        var url = '/partner/dispatch/orgPolling';
        var data = {
            fromTime: fromTime,
            toTime: toTime,
            orgList: orgIdList,
            riderName: getRiderName,
            explosionAreaList: explosionAreaList,
            poiName: busyMessage.poiName,
            poiSeq: busyMessage.poiSeq
        };
        if (!canDoAssgin()) {
            url = '/partner/dispatch/areaPolling';
            var areaList = getSelAreas();

            explosionAreaList = new Array();
            explosionAreaList.push(-1);
            data = {
                fromTime: fromTime,
                toTime: toTime,
                orgList: orgIdList,
                riderName: getRiderName,
                areaList: areaList,
                explosionAreaList: explosionAreaList,
                poiName: busyMessage.poiName,
                poiSeq: busyMessage.poiSeq
            };
        };
        buildData(data);

        addNewWaybillProperty(data);
        countRequest = $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data,
                traditional: true,
            })
            .done(function(obj) {
                if (!obj['ccount'] || obj.ccount['w11'] == undefined) {
                    console.log('ccount error');
                    return;
                };
                setTotal('-10', obj.ccount.w11);
                setTotal('-5', obj.ccount.w12);
                setTotal('10', obj.ccount.w13);
                setTotal('20', obj.ccount.w14);
                setTotal('30', obj.ccount.w15);
                setTotal('50', obj.ccount.w16);
                setTotal('99', obj.ccount.w17);
                setTotal('15', obj.ccount.w18);
                setTotal('100', obj.ccount.w19); //未完成
                setTotal('110', obj.ccount.w110); //全部

                setCounts('-10', obj.overTimeCcount.w21); //需手工派单
                setCounts('0', obj.overTimeCcount.w22);
                setCounts('10', obj.overTimeCcount.w23); //未接单
                setCounts('20', obj.overTimeCcount.w24); //已接单
                setCounts('30', obj.overTimeCcount.w25); //已取货
                setCounts('15', obj.overTimeCcount.w28); //派单后未确认
                setCounts('100', obj.overTimeCcount.w29); //未完成

                if (obj.isNew) {
                    $('#manualCount').addClass('badge');
                    playNewWaybillSound();
                    showNewTip();
                } else {
                    stopNewWaybillSound();
                }
                if (obj.designateOverTime) {
                    $('#notConfirm').addClass('badge');
                }
                if (obj.refund) {
                    $('#sendbackCount').addClass('badge');
                };

                showExplosionMsg(obj.explosionMsg);
            })
            .fail(function() {})
            .always(function() {

            });
    }

    function showExplosionMsg(msgList) {
        if (!msgList) {
            return;
        };
        for (var i = 0; i < msgList.length; i++) {

            var msg = msgList[i];
            var body = msg.content;
            if (msg.note && msg.note.length > 0) {
                body = body + '\n' + msg.note;
            };
            var tagStr = msg.poiType + '-' + msg.workStatus + '-' + msg.areaId + '-' + msg.cityId;
            var op = {
                tag: tagStr,
            };
            explosionMsgList[tagStr] = msg;

            var noti = r.utils.showDesktopNotification(msg.title, body, op);
            if (noti && msg.explosionLevel == 3 || msg.explosionLevel == 4) {
                noti.onclick = function() {

                    jumpPoi(this.tag);
                };
            };
        };
    }

    function setCounts(status, count) {
        var liName = statusDic[status];
        $(liName).siblings('span').text('超时' + count);
    }

    function addNewWaybillProperty(data) {
        var userId = cookie.getCookie("userId");
        var lastTime = cookie.getCookie("isNewWaybillLastTime" + userId);
        if (lastTime == "") {
            lastTime = getDaystartUtime(0, true) / 1000;
        }
        lastTime = new Number(lastTime);

        var designateLastTime = cookie.getCookie("designateOverTimeLastTime" + userId);
        if (designateLastTime == "") {
            designateLastTime = getDaystartUtime(0, true) / 1000;
        }
        designateLastTime = new Number(designateLastTime);

        var refundLastTime = cookie.getCookie("refundLastTime" + userId);
        if (refundLastTime == "") {
            refundLastTime = getDaystartUtime(0, true) / 1000;
        }
        refundLastTime = new Number(refundLastTime);

        data.lastTime = lastTime;
        data.designateLastTime = designateLastTime;
        data.refundLastTime = refundLastTime;
    }

    function playNewWaybillSound() {
        if (!isSoundClose()) {
            $('#chatAudio')[0].play();
            $('#chatAudio').attr("loop", "loop");
        }
    }

    function stopNewWaybillSound() {
        $('#chatAudio')[0].pause();
    }

    function saveNewWaybillTime(key, time) {
        var userId = cookie.getCookie("userId");
        cookie.setCookie(key + userId, time, 30 * 24 * 60 * 60);
    }

    function setTotal(status, count) {
        var liName = statusDic[status];
        if (count == 0) {
            $(liName).addClass("hidden");
        } else {
            $(liName).text('  ' + count).removeClass("hidden");
        }
    }

    function showTip(tip, type) {
        var $tip = $('#msgTip');
        if ($tip.length == 0) {
            $tip = $('<span id="msgTip"></span>');
            $('body').append($tip);
        }
        $tip.stop(true).attr('class', 'alert alert-' + type).text(tip).css('margin-left', -$tip.outerWidth() / 2).fadeIn(500).delay(2000).fadeOut(500);
    }

    function showMsg(msg) {
        showTip(msg, 'info');
    }

    function canDoAssgin() {
        return orgRoleCode === "2001" || orgRoleCode === "2100";
    }

    function sortByDis (rider1, rider2) {
        
        var result = 0;
        if (rider1.online == 0 && rider2.online == 0) {
            result = 0;
        } else if (rider1.online == 0) {
            result = 1;
        } else if (rider2.online == 0) {
            result = -1;
        } else {

            result = rider1.distant - rider2.distant;
        }
        return result;
    }

    function sortByReason(rider1, rider2){
        var result = 0;
        if (rider1.recommendDes == rider2.recommendDes) {
            result = 0;
        }else if(rider1.recommendDes){
            result = -1;
        }else{
            result = 1;
        }
        return result;
    }

    function sortRiderArray(riderList, _thisPos, isAsc) {

        riderList.sort(function(rider1, rider2) {

            var muti = 1;
            if (!isAsc) {
                muti = -1;
            };

            var result = 0;

            if (_thisPos == 1) {
                result = rider1.currentWaybillCount - rider2.currentWaybillCount;
                if (result == 0) {
                    muti = 1;
                    result = sortByDis(rider1, rider2);
                    if (result == 0) {
                        result = sortByReason(rider1, rider2);
                    };
                };
            } else if (_thisPos == 2) {
                result = sortByDis(rider1, rider2);
                if (result == 0) {
                    muti = 1;
                    result = rider1.currentWaybillCount - rider2.currentWaybillCount;
                    if (result == 0) {
                        result = sortByReason(rider1, rider2);
                    };
                };
            } else if (_thisPos == 3) {
                if (rider1.recommendDes == rider2.recommendDes) {

                    result = rider1.currentWaybillCount - rider2.currentWaybillCount;
                    if (result == 0) {
                        result = sortByDis(rider1, rider2);
                    };
                }else if(rider1.recommendDes){
                    result = -1;
                }else{
                    result = 1;
                }
                muti = 1;
            }else{
                var pinyin1 = nameToPinyin(rider1.name);
                var pinyin2 = nameToPinyin(rider2.name);
                result = pinyin1.localeCompare(pinyin2);
                muti = 1;
            }

            return result * muti;
        });
    }

    // @haunghuijie@meituan.com add
    /**
     * 传进去点击的当前位置，然后将那一列进行表格排序(只有距离和当前单数)
     * @_thisPos 当前位置
     * @isAsc    点击之时纪录的是否升序
     */
    var fnSortRiderListTable = function(_thisPos, isAsc) {

        var onlineArr = new Array(), //在岗
            busyArr = new Array(), //忙碌
            unlineArr = new Array(); //离岗

        for (riderId in curRiderList) {

            var rider = curRiderList[riderId];
            if (rider.workStatus == 1) {
                onlineArr.push(rider);
            } else if (rider.workStatus == 2) {
                busyArr.push(rider);
            } else if (rider.workStatus == 0) {
                unlineArr.push(rider);
            }
        }

        sortRiderArray(onlineArr, _thisPos, isAsc);
        sortRiderArray(busyArr, _thisPos, isAsc);
        _fillRiderList(onlineArr, busyArr, unlineArr);
    }

    function sortRiderListIfNeeded(riderList) {
        $('#riderListTable th').each(function() {

            var src = $(this).children("img").attr("src");
            var _pos = $(this).index();
            if (src.indexOf('fa-caret-i') == -1 && _pos != 0) {

                sortRiderArray(riderList, $(this).index(), !orAsc);
                return;
            };
        });
    }

    function saveRiderListSort(pos, sortAsc) {

        var userId = cookie.getCookie("userId");
        var posKey = 'sort_rider_pos' + userId;
        var ascKey = 'sort_rider_asc' + userId;

        localStorage.setItem(posKey, pos);
        localStorage.setItem(ascKey, sortAsc);
    }

    function setRiderListSort(th, sortAsc) {
        var _pos = th.index();
        if (sortAsc) {
            $(th).siblings().children("img").attr("src", "/static/imgs/fa-caret-i.png");
            $(th).children("img").attr("src", "/static/imgs/fa-caret-down.png");
        } else {
            $(th).siblings().children("img").attr("src", "/static/imgs/fa-caret-i.png");
            $(th).children("img").attr("src", "/static/imgs/fa-caret-up.png");
        }
        if (_pos == 0 || _pos == 3) {
            $(th).children("img").attr("src", "/static/imgs/fa-caret-up.png");
        };
    }

    function initRiderListSort() {
        var userId = cookie.getCookie("userId");
        var posKey = 'sort_rider_pos' + userId;
        var ascKey = 'sort_rider_asc' + userId;

        var pos = localStorage.getItem(posKey);
        var sortAsc = localStorage.getItem(ascKey);
        if (pos && sortAsc) {
            var th = $('#riderListTable th')[pos];
            setRiderListSort($(th), !sortAsc);
        };
    }

    var fnSerachName = function() {
        var tbodyY = $("#riderListBody").offset().top,
            tbodyX = $("#riderListBody").offset().left,
            tbodyW = $("#riderListBody").width(), //找到第一个的x，y之后累加找到当前的xy
            span = $(".riderName"),
            trLeng = $("#riderListBody tr").length;

        var divY = $("#riderListDiv").offset().top,
            divH = $("#riderListDiv").height();
        var top = tbodyY - divY;


        var successNum = []; // 成功的个数
        $(".shade").remove();
        for (var i = 0; i < trLeng; i++) {
            // 不是标志行
            if ($("#riderListBody tr:eq(" + i + ")").children('td:eq(0)').find('span').hasClass('riderName')) {
                var _this = $("#riderListBody tr:eq(" + i + ")").find(".riderName"), //span[i],
                    nameStr = _this.text().toLowerCase();
                if (nameStr.search(searchRiderName) > -1) { //即匹配成功
                    successNum.push(i);
                }
            };
        };

        if (successNum.length === 0) {
            alert("没有搜索到相应的骑手名！");
            return;
        } else {
            successNum.push(trLeng - 1);
        }

        var html = "";
        var firstShade = "#riderListBody tr:eq(" + successNum[0] + ")";
        firstH = $(firstShade).offset().top - tbodyY; //$(firstShade).height();// 开始的位置

        html += "<div class='shade'" + " " +
            "style='width:" + tbodyW + "px;" +
            "height:" + firstH + "px;" +
            "top:" + top + "px;" +
            "'></div>";

        for (var j = 0; j < successNum.length - 1; j++) {
            var shadeW = 0, //当前遮罩层的宽度
                shadeH = 0,
                shadeX = 0,
                shadeY = 0;

            var eqLoc = "#riderListBody tr:eq(" + successNum[j] + ")",
                eqNext = "#riderListBody tr:eq(" + successNum[j + 1] + ")";


            if (j + 1 === successNum.length - 1) { //最后一个
                shadeH = $(eqNext).offset().top - $(eqLoc).offset().top;
                shadeY = $(eqLoc).offset().top + $(eqLoc).height() - tbodyY;
            } else {
                shadeH = $(eqNext).offset().top - $(eqLoc).offset().top - $(eqLoc).height();
                shadeY = $(eqLoc).offset().top + $(eqLoc).height() - tbodyY;
            }

            shadeW = tbodyW;
            shadeX = tbodyX;

            html += "<div class='shade'" + " " +
                "style='width:" + shadeW + "px;" +
                "height:" + shadeH + "px;" +
                "top:" + (shadeY + top) + "px;" +
                "'></div>";
        };

        $('#riderListDiv').append(html);
        var firstSuccess = "#riderListBody tr:eq(" + successNum[0] + ")";
        var successTop = $(firstSuccess).offset().top - divY;
        $("#riderListDiv").scrollTop(successTop);
    }

    // 清空点击选中中的右边的数量
    function clearChooecdNumber() {
        curSelOrderId.length = 0;

        $('#responsiveTable tbody tr').each(function(index, el) {
            if ($(this).hasClass("success")) {
                $(this).removeClass("success");
            }
        });
        $("#btn-clear").addClass('hidden');
    }
    // 日期格式化
    // @days 距离当天的天数
    function fnDateFormat(days) {
        var myDate = new Date(new Date(currentServerTime).getTime() - 1000 * 60 * 60 * 24 * days),
            myMonth = myDate.getMonth() + 1,
            myDay = myDate.getDate(),
            myWeek = myDate.getDay();
        if (myMonth < 10) {
            myMonth = "0" + myMonth;
        };
        if (myDay < 10) {
            myDay = "0" + myDay;
        };
        var dateObj = "";

        switch (days) {
            case 0:
                dateObj = myMonth + "-" + myDay + "(今天)";
                break;
            case 1:
                dateObj = myMonth + "-" + myDay + "(昨天)";
                break;
            case 2:
                dateObj = myMonth + "-" + myDay + "(前天)";
                break;
            default:
                dateObj = myMonth + "-" + myDay + fnWeekDeal(myWeek);
        }
        return dateObj;
    }

    function fnWeekDeal(week) {
        // 0 1 2 3 4 5 6 
        //天 1 2 3 4 5 6
        var weekStr = "";
        switch (week) {
            case 0:
                weekStr = "日";
                break;
            case 1:
                weekStr = "一";
                break;
            case 2:
                weekStr = "二";
                break;
            case 3:
                weekStr = "三";
                break;
            case 4:
                weekStr = "四";
                break;
            case 5:
                weekStr = "五";
                break;
            case 6:
                weekStr = "六";
                break;
        }
        return "(周" + weekStr + ")";
    }

    function shoudSel6Hours() {
        var now = new Date(currentServerTime);

        return now.getHours() >= 20;
    }

    function fnSelectDay() {
        var selectHtml = "";
        var should = shoudSel6Hours();
        selectHtml += '<option value="0">' + '前后六小时' + '</option>';
        for (var i = 0; i < 7; i++) {
            var option = fnDateFormat(i);
            if (i == 0 && !should) {
                selectHtml += '<option selected="selected" value="' + (i + 1) + '">' + option + '</option>';
            } else {
                selectHtml += '<option value="' + (i + 1) + '">' + option + '</option>';
            }
        };
        $("#selectDay").html(selectHtml);
        $("#selectDay").select2();
        $("#selectDay").on("change", function(e) {
            showSelDay();
            getWayBillList(curWaybillStatus, 1);
            _getRiderList();
        });
        showSelDay();
    }

    function showSelDay() {
        var op = $('#selectDay option:selected').text();
        $('.date-lable').text(op);
    }

    function showSelOrgName() {
        var text = '没有选择';
        var op = $('#selectOrg option:selected');
        if (op.length == 1) {
            text = op.text();
        } else if (op.length > 1) {
            var orgId = op.attr('value');
            var plus = 0;
            if (orgId == -1) {
                orgId = $(op[1]).attr('value');
                plus = -1;
            };
            var org = allOrgList[orgId];
            if (org) {
                var allOp = $('#selectOrg option');
                var len = op.length + plus;
                if (len == 1) {

                    text = org.name + '[' + org.dispatchStrategyDes + ']';
                } else {
                    if (allOp.length == op.length) {
                        text = '全部站点' + '(' + len + ')' + '[' + org.dispatchStrategyDes + ']';
                    } else {
                        text = len + '个站点' + '[' + org.dispatchStrategyDes + ']';
                    }
                }
            };
        }
        $('.org-name-lable').attr('title', text);
        if (text.length > 14) {
            text = '...' + text.substr(text.length - 13, 13);
        };
        $('.org-name-lable').text(text);
    }

    function bindEvents() {

        $('#riderListTable').on('mouseenter', 'tbody tr', function(event) {
            
            showRiderStatus($(this));
        });
        $('#riderListTable').on('mouseleave', 'tbody tr', function(event) {
            
            hideRiderStatus($(this));
        });

        $('#riderListTable').on('mouseenter', '.riderName', function(event) {
            
            if ($('#riderListDiv').hasClass('status-control')) {
                return;
            };
            $(this).addClass('search');
        });
        $('#riderListTable').on('mouseleave', '.riderName', function(event) {
            
            $(this).removeClass('search');
        });

        $('#riderListTable').on('click', '.riderName', function(event) {
            event.preventDefault();
            if ($('#riderListDiv').hasClass('status-control')) {
                return;
            };
            var tr = $(this).parents('tr');
            if (!tr) {
                return;
            };
            var riderId = tr.attr('value');
            var rider = curRiderList[riderId];
            if (!rider || !rider.name) {
                return;
            }

            $('#getRiderName').val(rider.name);
            $("#clearRiderName").css("color", "#777");

            getRiderName = $(this).text();
            var status = notComplete + '';

            $('#waybillStatue li[value=' + status + ']').addClass('active').siblings().removeClass('active');
            curWaybillStatus = status;
            curSelOrderId = [];
            getWayBillList(curWaybillStatus, 1);
            _getRiderList();
        });

        $("#getSearchRiderName").keyup(function(event) {
            var _this = $(this);
            if (_this.val() == "") {
                $("#clearSearchRiderName").css("color", "#fff");
                $(".shade").remove();
            } else {
                $("#clearSearchRiderName").css("color", "#777");
            };
            searchRiderName = _this.val();
            if (event.keyCode == 13) {
                searchRiderName = searchRiderName.toLowerCase(); //全部改成小写 以模糊匹配
                fnSerachName();
            };
        });
        $("#clearSearchRiderName").click(function() {
            $("#clearSearchRiderName").css("color", "#fff");
            $(".shade").remove();
            clearShadeVal();
        });

        //骑手名 输入框的清除
        $("#clearRiderName").on('click', function() {
            var val = "";
            $("#getRiderName").val(val);
            $(this).css("color", "#fff");

            getRiderName = '';
            curSelOrderId.length = 0;
            clearChooecdNumber();
            getWayBillList(curWaybillStatus, 1);
        });

        // 显示提示框的 问题
        $(".qa-icon").hover(function() {
            var _this = $(this);
            // 本来是打算用这top和left来控制的 但是发现好像0不是头部开始
            var top = _this.position().top + 39,
                left = _this.position().left + _this.width() - $("#qaInfoDetail").innerWidth();

            $("#qaInfoDetail").css({
                "top": top + "px",
                "left": left + "px"
            });
            $("#qaInfoDetail").show();
        }, function() {
            $("#qaInfoDetail").hide();
        });
        $("#waybilllist").on('click', 'th', function() {
            var _this = $(this),
                acticeLi = _this.parents("#wayBillListDiv").find('.active'),
                liStatus = acticeLi.attr("value");
            inputOrderSeq = 1;
            if (liStatus !== "50" && liStatus !== "99" && liStatus !== "110") { //若为 "已送达/已取消" 则不排序
                if (waybilllistAsc) { // 升序还是
                    inputOrderSeq = "1";
                } else {
                    inputOrderSeq = "-1";
                }
                waybilllistAsc = !waybilllistAsc;

                var _thisPos = _this.index();
                switch (_thisPos) {
                    case 1:
                        inputOrderField = "delivered_time";
                        break;
                    case 0:
                        inputOrderField = "utime";
                        break;
                    case 2:
                        inputOrderField = "platform_poi_id";
                        break;
                    case 3:
                        inputOrderField = "recipient_address";
                        break;
                    case 4:
                        inputOrderField = "rider_name";
                        break;
                    default:
                        inputOrderField = "";
                        break;
                }
                if (inputOrderField) { // 自己在th上面加的判断属性
                    getWayBillList(liStatus, 1);
                };
            }
        });
        $("#riderListTable").on('click', "th", function() {
            var _pos = $(this).index();
            setRiderListSort($(this), orAsc);
            fnSortRiderListTable(_pos, orAsc);
            saveRiderListSort(_pos, orAsc);
            orAsc = !orAsc;
        });

        //点击页码事件
        $(document).on("click", ".pagination li", function() {
            if ($(this).hasClass("disabled") || $(this).hasClass("active")) {
                return false;
            }
            curSelOrderId = [];
            var status = $("#waybillStatue .active").attr("value");
            getWayBillList(status, $(this).attr("page"));
            _getRiderList();
        });

        $(".tooltip-frame").mouseleave(function(event) {

            var x = event.pageX;
            var y = event.pageY;
            if (!isOnElement(x, y, lastHoverTr)) {

                hideToolTip();
                $('.riderlist-shade-tr').addClass('hidden');
            };
        });
        $('#sigle-control').click(function(event) {
            if ($('.status-menu').hasClass('hidden')) {
                $('.status-menu').css('left', $(this).position().left);
            };
            $('.status-menu').toggleClass('hidden');
        });

        $('#control-icon').click(function(event) {
            $('#riderListDiv').toggleClass('status-control');

            if ($('#riderListDiv').hasClass('status-control')) {
                $('.rider-select').each(function(index, el) {
                    this.checked = false;
                });
            };
            showControlCount();
        });

        $('.status-menu p').click(function(event) {
            $('.status-menu').addClass('hidden');
            var value = $(this).attr('value');
            var curV = $('#sigle-control').attr('value');
            var riderId = $('#sigle-control').attr('rid');
            var rider = curRiderList[riderId];
            if (value != curV) {
                setRiderStatus(rider, value);
            };
        });

        $('#cancel-control').click(function(event) {

            $('#riderListDiv').removeClass('status-control');
        });

        $('#btn-online').click(function(event) {
            showMutiSetAlert(1);
            $(this).blur();
        });

        $('#btn-busy').click(function(event) {
            showMutiSetAlert(2);
            $(this).blur();
        });

        $('#btn-offline').click(function(event) {
            showMutiSetAlert(0);
            $(this).blur();
        });
        $('#riderListDiv').on('click', '.rider-select', function(event) {
            showControlCount();
        });
        $('#rider-select-all').click(function(event) {
            if ($(this).prop('checked')) {
                $('.rider-select').prop('checked', 'checked');
            } else {
                $('.rider-select').prop('checked', false);
            }
            showControlCount();
        });

        //骑手名字的获取和事件触发
        $("#getRiderName").keyup(function() { //赋值给getRiderName
            var riderName = $(this).val();
            if (riderName == "") {
                $("#clearRiderName").css("color", "#fff");
            } else {
                $("#clearRiderName").css("color", "#777");
            }
            getRiderName = riderName ? $.trim(riderName) : "";
        });

        $("#getRiderName").change(function() { //触发获取列表等事件
            if (!!getRiderName) {
                getWayBillList(curWaybillStatus, 1);
                _getRiderList();
            };
        });
        //骑手名字的获取和事件触发
        $("#busyMessage").keyup(function() { //赋值给getRiderName
            busyString = $(this).val();
            if (busyString == "") {
                $("#clearBusyName").css("color", "#fff");
            } else {
                $("#clearBusyName").css("color", "#777");
            }
        });
        //商家名 输入框的清除
        $("#clearBusyName").on('click', function() {
            busyString = "";
            busyMessage.poiName = "";
            busyMessage.poiSeq = "";
            $("#busyMessage").val(busyString);
            $(this).css("color", "#fff");

            curSelOrderId.length = 0;
            clearChooecdNumber();
            getWayBillList(curWaybillStatus, 1);
        });

        $("#busyMessage").change(function() {
            if (!busyString) {
                busyMessage.poiName = "";
                busyMessage.poiSeq = "";
            } else {
                var reg = new RegExp("^[0-9]*$");
                var busyArr = busyString.split(" ");
                if (busyArr.length == 1) {
                    // reg.test( asdad )全都是数字
                    if (!reg.test(busyArr[0])) { // 不全是数字
                        busyMessage.poiName = busyArr[0];
                    };
                    busyMessage.poiSeq = "";
                } else {
                    busyMessage.poiName = busyArr[0];
                    if (!!reg.test(busyArr[1])) { //如果不全是数字 那么直接为空
                        busyMessage.poiSeq = busyArr[1];
                    } else {
                        alert("序号值输入有误，请重新输入！");
                        busyMessage.poiSeq = "";
                    }
                }
            }

            getWayBillList(curWaybillStatus, 1);
            _getRiderList();
        });

        $('#showRiderOnMap').click(function(event) {
            var city = getSelCityid();
            var url = "/partner/dispatch/mapRiderState?city=" + city;
            if (orgRoleType != 1) {
                var areaList = getSelAreas();
                if (areaList.length == 1) {
                    url = url + '&areaId=' + areaList[0];
                };
            };
            window.open(url, '_blank');
        });

        $("#waybillStatue li").click(function() {
            var status = $(this).attr("value");
            $(this).addClass("active").siblings().removeClass("active");
            curWaybillStatus = status;
            curSelOrderId = [];
            getWayBillList(status, 1);
            _getRiderList();
        });

        $('#btn-refresh').click(function(event) {
            getWayBillList(curWaybillStatus, 1);
            _getRiderList();
            // refreshLists();
            // $(this).blur();
        });
        $("#refreshIcon").click(function(event) {
            getWayBillList(curWaybillStatus, 1);
            _getRiderList();
        });
        $(".dropdown").delegate('button', 'click', function(event) {
            var _this = $(this),
                ulList = _this.parent().find('.dropdown-menu'),
                select = _this.parent().prev(),
                liList = select.find('option');

            var ulStr = "",
                tempArr = new Array();

            var selectId = select.attr("id");
            var statusImgArr = new Array();
            statusImgArr = ["/static/imgs/status_all.png", "/static/imgs/status_onjob.png", "/static/imgs/status_busy.png", "/static/imgs/status_unjob.png"];
            for (var i = 0; i < liList.length; i++) {
                var str = select.find('option:eq(' + i + ')').text();
                var left = str.indexOf("("), //左括号
                    right = str.indexOf(")");

                if (left < 0) {
                    str = "<span class='greySpan'>" + str + "</span>";
                } else {
                    str = str.substr(0, left) + "<span class='greySpan'>" + str.substr(left, right) + "</span>";
                }
                if (selectId == "selectStatus") {
                    str = "<img src='" + statusImgArr[i] + "' />" + str;
                };
                if (!!select.find('option:eq(' + i + ')').attr("selected")) {
                    ulStr += "<li class='active'><a href='#'>" + str + "</a></li>";
                    tempArr.push(i);
                } else {
                    ulStr += "<li><a href='#'>" + str + "</a></li>";
                }
            };
            ulList.html(ulStr);
            if (tempArr.length == 0) {
                ulList.find('li:eq(0)').addClass('active');
            };
        });

        $(".dropdown").delegate('li', 'click', function() {
            fnDropLiClick($(this));
        });
        $('#sound-icon').click(function(event) {

            var userId = cookie.getCookie("userId");
            if (isSoundClose()) {

                cookie.setCookie("newWaybillSoundClose" + userId, "", 30 * 24 * 60 * 60);
            } else {

                cookie.setCookie("newWaybillSoundClose" + userId, "1", 30 * 24 * 60 * 60);
                stopNewWaybillSound();
            }
            setSoundIcon();
        });
        // TODO 派单操作
        $(document).delegate('.riderlist-shade-tr', 'click', function(event) {
            if ($(this).hasClass('disabled')) {
                alert("不可点击");
                return;
            };
            var tr = $(this).parents('tr');
            curRiderId = tr.attr('value');

            if ($(this).hasClass('reassign')) {
                hideToolTip();
                showReassignDlg(curRiderId);
            } else {
                if (curSelOrderId.length == 1) {
                    assignToRider(curRiderId, curSelOrderId[0]);
                } else {
                    mulAssignToRider(curRiderId, curSelOrderId);
                }
            }
            $(this).addClass('hidden');
        });

        $('#reload-base').click(function(event) {
            getLoginInfo(getStoredCity());
            $('#base-info').text('加载城市列表中...');
            $(this).addClass('hidden');
        });

        $('#open-selects').click(function(event) {

            $('#select-container').addClass('selects-open');
            $('#select-mask').show();
        });
        $('#close-selects').click(function(event) {

            $('#select-container').removeClass('selects-open');
            $('#select-mask').hide();
        });
        $('#select-mask').click(function(event) {
            $('#select-container').removeClass('selects-open');
            $(this).hide();
        });
        $('#control-icon').hover(function() {
            $('#control-title').show();
        }, function() {
            $('#control-title').hide();
        });
    }
});