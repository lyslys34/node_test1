require.config({
    baseUrl: MT.STATIC_ROOT + '/js',
    urlArgs: 'ver=' + pageVersion,
});

require(['module/root', 'lib/bootstrap-multiselect'], function(r, muti) {
    var orgIdList = new Array();
    var selRiderList = new Array();
    var riderStatusArr = new Array(10, 1, 2, -1);
    var curRiderStatus = 10;
    var fullRiderList;
    var ridersToShow;
    var selectRider = $('#selectRider');
    var offsetDiv = $('#offlineDiv');
    var statusNameDic = new Array();
    var urlRiderId;
    var selCity = $('#selectCity');
    var deliveryAreaList = {};
    var allOrgList = {};

    statusNameDic['-10'] = '需手工指派';
    statusNameDic['15'] = '派单后未确认';
    statusNameDic['0'] = '未调度';
    statusNameDic['10'] = '未接单';
    statusNameDic['20'] = '已接单';
    statusNameDic['30'] = '已取货';
    statusNameDic['50'] = '已送达';
    statusNameDic['99'] = '已取消';

    document.title = "骑手地图";

    //初始化地图对象，加载地图

    var mapStyle = setMapStyle();

    map = new AMap.Map("mapContainer", {
        resizeEnable: true,
        mapStyle: mapStyle,
        view: new AMap.View2D({
            resizeEnable: true,
            // zoom: 14//地图显示的缩放级别
        }),
        keyboardEnable: false
    });

    map.plugin(["AMap.Scale"], function() {
        scale = new AMap.Scale();
        map.addControl(scale);
    });

    //在地图中添加ToolBar插件
    map.plugin(["AMap.ToolBar"], function() {
        toolBar = new AMap.ToolBar({
            direction:false,
        });
        toolBar.hideLocation();
        map.addControl(toolBar);
    });

    urlRiderId = r.utils.urlArg('id');
    var urlCity = r.utils.urlArg('city');

    getLoginInfo(urlCity);

    function getLoginInfo(cityId) {
        if (!cityId) {
            cityId = 0;
        };
        $.ajax({
                url: '/partner/dispatch/homeLoginInfo',
                type: 'GET',
                dataType: 'json',
                data: {
                    cityId: cityId
                },
            })
            .done(function(result) {
                if (result.code == 0) {
                    $('#org-info').removeClass('hidden');
                    $('#loading-content').addClass('hidden');

                    fillCityList(result.data.bmCityList, result.data.selectCityId);
                    refreshAreaDta(result.data.bmDeliveryAreaList);
                    refreshOrgData(result.data.bmOrgList);

                    fillAreaList(true);
                } else {
                    $('#reload-base').removeClass('hidden');
                    $('#base-info').text(result.msg);
                }
            })
            .fail(function() {
                $('#reload-base').removeClass('hidden');
                $('#base-info').text('加载失败，请重试');
            })
            .always(function() {});
    }

    function fillCityList(data, selCityId) {
        var body = '';
        for (var i = 0; i < data.length; i++) {
            var city = data[i];
            if (selCityId == city.city_id) {
                body += '<option selected="selected" value="' + city.city_id + '">' + city.name + '</option>';
            } else {
                body += '<option value="' + city.city_id + '">' + city.name + '</option>';
            }
        };
        selCity.append(body);
        $.fn.select2.amd.require(['select2/compat/matcher'], function(oldMatcher) {
            selCity.select2({
                matcher: oldMatcher(matchPinyin)
            });
        });
        selCity.select2();

        // 城市的下拉框
        selCity.on("change", function(e) {
            sessionStorage.setItem('selectObj', "city-" + selCity.val() + ")org-)rider-");
            urlRiderId = null;
            var cityId = getSelCityid();
            getAreaListWithCity(cityId);
        });
    }

    function refreshAreaDta(data) {

        deliveryAreaList = {};
        if (data) {

            for (var i = 0; i < data.length; i++) {
                var deliveryArea = data[i];
                var area = {};
                area.id = deliveryArea.areaId;
                area.cityId = deliveryArea.cityId;
                // area.orgId = deliveryArea.bmOrgId;
                area.name = '-';
                if (deliveryArea.areaName) {
                    area.name = deliveryArea.areaName;
                }
                deliveryAreaList[area.id] = area;
            };
        }
    }

    function refreshOrgData(data) {
        allOrgList = {};
        if (data) {
            for (var i = 0; i < data.length; i++) {
                var orgView = data[i];
                var org = {};
                org.orgid = orgView.orgId;
                org.name = orgView.orgName;
                org.dispatchStrategyDes = orgView.dispatchStrategyDes;
                org.areaId = orgView.areaId;

                allOrgList[orgView.orgId] = org;
            };
        }
    }

    function getAreaListWithCity(cityId) {
        $.ajax({
                url: '/partner/dispatch/homeLoginInfo',
                type: 'GET',
                dataType: 'json',
                data: {
                    cityId: cityId
                },
            })
            .done(function(result) {
                if (result.code == 0) {

                    refreshAreaDta(result.data.bmDeliveryAreaList);
                    refreshOrgData(result.data.bmOrgList);
                    fillAreaList();
                } else {
                    alert('获取区域信息失败，请重试');
                }
            })
            .fail(function() {
                refreshAreaDta();
                refreshOrgData();
            })
            .always(function() {});
    }

    function fillAreaList(useArg) {
        $('#selectArea').empty();
        var options = '';
        var areaArg = r.utils.urlArg('areaId');
        // if (!areaArg && useArg) {
        //     areaArg = getStoredArea();
        // };
        var selected = '';
        var showUrlArg = false;
        for (areaId in deliveryAreaList) {

            if (useArg && areaArg && (areaArg == areaId)) {
                selected = ' selected="selected"';
                showUrlArg = true;
            };
            var area = deliveryAreaList[areaId];
            options = options + '<option' + selected + ' value=' + area.id;
            options = options + '>' + area.name;
            options = options + '</option>';
            selected = '';
        }
        $('#selectArea').append(options);
        $('#selectArea').multiselect('destroy');

        if (orgRoleType === '1') {
            $('#selectArea').multiselect({
                includeSelectAllOption: true,
                selectAllText: '选择全部',
                nonSelectedText: '没有选择',
                nSelectedText: '个区域',
                allSelectedText: '全部区域',
                selectAllValue: '0',
                filterPlaceholder: '输入关键字',
                includeSelectAllIfMoreThan: 1,
                enableFiltering: true,
                numberDisplayed: 1,
                maxHeight: 250,
                onDropdownHidden: function(event) {

                    onSelAreaHidden(event);
                },
            });
            if (!showUrlArg) {
                $('#selectArea').multiselect('selectAll', false, true);
            };
        } else {
            $('#selectArea').multiselect({
                includeSelectAllOption: false,
                nonSelectedText: '没有选择',
                nSelectedText: '个区域',
                enableFiltering: true,
                maxHeight: 250,
                numberDisplayed: 1,
                onDropdownHidden: function(event) {

                    onSelAreaHidden(event);
                },
            });
        }

        $('#selectArea').multiselect('updateButtonText');
        // storeAreaId();
        fillOrgList(useArg);
    }

    function onSelAreaHidden() {
        fillOrgList();
    }

    function getSelCityid() {
        var cityid;
        $('#selectCity option:selected').each(function() {
            cityid = $(this).val();
        });
        return cityid;
    }

    function getSelAreas() {
        var selAreas = new Array();
        $('#selectArea option:selected').each(function() {
            selAreas.push($(this).val());
        });
        return selAreas;
    }

    function areaIdInSel(areaId) {
        var selAreas = getSelAreas();
        var has = false;
        for (var i = 0; i < selAreas.length; i++) {
            if (selAreas[i] == areaId) {
                has = true;
                break;
            };
        };
        return has;
    }

    function fillOrgList(useArg) {
        $('#selectOrg').empty();

        var options = '';
        // if (!canDoAssgin()) {
        //     options = options + '<option' + ' value=' + '-1';
        //     options = options + '>' + '未分配站点';
        //     options = options + '</option>'
        // };

        var orgArg = r.utils.urlArg('orgId');
        var selected = '';
        var showUrlArg = false;
        for (orgid in allOrgList) {

            var org = allOrgList[orgid];
            if (areaIdInSel(org.areaId)) {

                if (useArg && orgArg && (orgArg == orgid)) {
                    selected = ' selected="selected"';
                    showUrlArg = true;
                };
                options = options + '<option' + selected + ' value=' + org.orgid;
                options = options + '>' + org.name + '[' + org.dispatchStrategyDes + ']';
                options = options + '</option>';
                selected = '';
            };
        }

        $('#selectOrg').append(options);
        $('#selectOrg').multiselect('destroy');
        $('#selectOrg').multiselect({
            includeSelectAllOption: true,
            selectAllText: '选择全部',
            nonSelectedText: '没有选择',
            nSelectedText: '个站点',
            allSelectedText: '全部站点',
            selectAllValue: '0',
            filterPlaceholder: '输入关键字',
            includeSelectAllIfMoreThan: 1,
            numberDisplayed: 1,
            enableFiltering: true,
            limitTextNumber: true,
            maxTextNumber: 15,
            maxHeight: 500,
            onDropdownHidden: function(event) {

                onSelOrgHidden(event);
            },
        });
        if (!showUrlArg) {
            $('#selectOrg').multiselect('selectAll', false, true);
        };
        $('#selectOrg').multiselect('updateButtonText');
        getOrgIdList();
        getRiderListFromServer();
    }

    $("#org-info").on('click', ".status-check", function() {

        refreshStatus();
        fillRiderList();
        fillMap();
    });

    function refreshStatus() {
        var cursel = 1; //当前点击的box

        var check1 = $(".status-check:eq(0)"),
            check2 = $(".status-check:eq(1)");

        if (check1.is(":checked") && check2.is(":checked")) { // 都选中的
            cursel = 0;
        } else if (check1.is(":checked") && !check2.is(":checked")) { // 只选中在岗的
            cursel = 1;
        } else if (!check1.is(":checked") && check2.is(":checked")) { // 只选中忙碌的
            cursel = 2;
        } else { //都没选中的
            cursel = 3;
        }
        curRiderStatus = riderStatusArr[cursel];
    }

    function setMapStyle() {
        var style = 'blue_night';
        if (localStorage.getItem('rider-state')) {
            style = localStorage.getItem('rider-state');
        };
        var selector = '.map_style [value="' + style + '"]';
        var input = $(selector);
        if (input.length > 0) {
            input.prop('checked', 'checked');
        };
        return style;
    }
    $('.map_style').on('click', 'input', function(event) {
        var style = $(this).attr('value');
        map.setMapStyle(style);
        localStorage.setItem('rider-state', style);
    });

    $(".status-check-online").click(function(event) {
        refreshStatus()
        fillRiderList();
        fillMap();
    });

    $('#btn-refresh').click(function(event) {

        getRiderListFromServer();
        $(this).blur();
    });

    $(document).click(function(event) {

        if (offsetDiv.css("display") != 'none') {

            offsetDiv.css({
                display: 'none',
            });
        };
    });

    $('#offline-rider').click(function(event) {

        if (event && event.stopPropagation) {
            event.stopPropagation();
        } else {
            window.event.cancelBubble = true;
        }
        offsetDiv.empty();
        var body = '<ul>'

        for (riderid in fullRiderList) {

            var rider = fullRiderList[riderid];
            if (rider.workStatus == 0) {
                body = body + '<li>';
                body = body + rider.name + '[' + rider.mobile + ']';
                body = body + '</li>';
            };
        }
        body = body + '</ul>'
        offsetDiv.append(body)
        offsetDiv.css({
            display: 'block'
        });
    });

    // orgList 的下拉变化
    function onSelOrgHidden(event) {
        getOrgIdList();
        getRiderListFromServer();
    }
    // riderList 的下拉变化
    function selRiderListChange(event) {
        getSelRiderlist();
        fillMap();
    }


    function getOrgIdList() {
        orgIdList = new Array();
        $('#selectOrg option:selected').each(function() {
            orgIdList.push($(this).val());
        });
    }

    function getRiderListFromServer() {
        if (orgIdList.length < 1) {
            return;
        };
        $.ajax({
                url: '/partner/dispatch/riderCoordinatelist',
                type: 'GET',
                dataType: 'json',
                data: {
                    orgList: orgIdList
                },
                traditional: true,

            })
            .done(function(result) {
                parseRiderList(result);
                refreshCounts();
                refreshStatus();
                fillRiderList();
                fillMap();
            })
            .fail(function() {})
            .always(function() {});
    }

    function refreshCounts() {
        $('#online-count').text(getRiderCount(1));
        $('#busy-count').text(getRiderCount(2));
        $('#offline-count').text(getRiderCount(0));
    }

    function getRiderCount(workStatus) {

        var count = 0;
        for (riderid in fullRiderList) {

            if (fullRiderList[riderid].workStatus == workStatus) {
                count++;
            };
        }
        return count;
    }

    function parseRiderList(riderList) {
        fullRiderList = {};
        $.each(riderList, function(index, rider) {

            fullRiderList[new String(rider.id)] = rider;
        });
    }

    function isSelStatus(workStatus) {
        if (curRiderStatus == 10) {

            return workStatus == 1 || workStatus == 2;
        };

        return workStatus == curRiderStatus;
    }

    $(document).on('click', '.cloce-window', function(event) {
        $("#confirmWindow").hide();
    });

    function fillRiderList() {
        selectRider.empty();
        var options = '';
        var id = urlRiderId;
        if (!id) {
            id = '';
        };

        var offlineChecked = $('.status-check-online').is(":checked");
        var postWorkArr = new Array();

        for (riderid in fullRiderList) {
            var rider = fullRiderList[riderid];
            if (isSelStatus(rider.workStatus)) {
                if (!offlineChecked || rider.online == 0) {
                    options = options + '<option' + ' value=' + rider.id;
                    if (id) { // 是从url开始加载
                        if (id && rider.id == id) options = options + ' selected="selected"';
                    }
                    options = options + '>' + rider.name + '[' + rider.mobile + ']';
                    options = options + '</option>';
                };
            };
        }

        selectRider.append(options);
        selectRider.multiselect('destroy');
        selectRider.multiselect({
            includeSelectAllOption: true,
            selectAllText: '选择全部',
            nonSelectedText: '没有选择',
            nSelectedText: '个骑手',
            allSelectedText: '全部骑手',
            selectAllValue: '0',
            includeSelectAllIfMoreThan: 1,
            numberDisplayed: 2,
            enableFiltering: true,
            maxHeight: 500,
            onChange: function(event) {
                selRiderListChange(event);
            },
        });
        if (!urlRiderId) {
            selectRider.multiselect('selectAll', false, true);
        } else {
            selectRider.multiselect();
        }
        selectRider.multiselect('updateButtonText');

        var windowHtml = "";
        if (postWorkArr.length > 0) {
            windowHtml += '<div class="window-body">';
            windowHtml += '<div class="cloce-window">X</div>' + '<div class="clear-float"></div>';
            windowHtml += '<p>抱歉，您选择的骑手：<span>'

            if (postWorkArr.length > 1) {
                var temp = "";
                for (var i = 0; i < postWorkArr.length; i++) {
                    temp += postWorkArr[i].name + '（' + postWorkArr[i].mobile + '）';
                };
                windowHtml += temp;
                windowHtml += '</span>已经从[在岗]变更为<span>[离岗]</span>由于[离岗]骑手将不再上报实时定位数据，故地图已无法展示该骑手位置。</p>';
                windowHtml += '<br/><p>您可以：</p>';
                windowHtml += '<button class="btn btn-default btn-href pull-left" id="seeAllRider">切回全部骑手</button>' + '<button class="btn btn-default btn-href pull-right" id="otherRiderBtn">看看其他人</button>';
            } else {
                windowHtml += postWorkArr[0].name + '（' + postWorkArr[0].mobile + '）';
                windowHtml += '</span>已经从[在岗]变更为<span>[离岗]</span>由于[离岗]骑手将不再上报实时定位数据，故地图已无法展示该骑手位置。</p>';
                windowHtml += '<br/><p>您可以：</p>';
                windowHtml += '<a href="/partner/dispatch/riderTrack?riderPhone=' + postWorkArr[0].mobile + '" target="_blank"' + 'class="btn btn-default btn-href pull-left">看看他的历史轨迹</a>' + '<button class="btn btn-default btn-href pull-right" id="otherRiderBtn">看看其他人</button>';
            };
            windowHtml += '</div>';
            $("#confirmWindow").html(windowHtml);
            $("#confirmWindow").show();
        }
        getSelRiderlist();
    };
    $("#confirmWindow").on('click', 'button', function(event) {
        $("#confirmWindow").hide();
    });
    $("#confirmWindow").on('click', 'a', function(event) {
        $("#confirmWindow").hide();
    });
    $("#confirmWindow").on('click', '#otherRiderBtn', function() {
        selectRider.multiselect('updateButtonText');
        $(".multiselect-container:eq(1)").show();
        $(".multiselect-container:eq(1)").find('.multiselect-search').focus();
    });
    $("#confirmWindow").on('click', '#seeAllRider', function() {
        selectRider.multiselect('selectAll', false, true);
        selectRider.multiselect('updateButtonText');
    });


    function getSelRiderlist() {
        selRiderList = new Array();
        $('#selectRider option:selected').each(function() {
            selRiderList.push($(this).val());
        });
    }

    function clearMap() {
        map.clearMap();
    }

    function fillMap() {
        clearMap();
        var rider;
        var lastRider, unLineLen = 0;

        for (i in selRiderList) {

            var riderid = selRiderList[i];
            rider = fullRiderList[riderid];
            addRiderMarker(rider);
            lastRider = rider;
            if (rider.workStatus == 1 || rider.workStatus == 2) {
                if (rider.online == 0) {
                    unLineLen++;
                };
            };
        }

        if (lastRider) {
            map.setFitView();
        };
        var str = unLineLen + "人";
        $("#drop-count").html(str);
    }

    function getRiderString(rider) {
        var des = ''
        des = des + r.utils.timeString(rider.coTime * 1000) + ' ' + rider.name + '[' + rider.currentWaybillCount + '单]';
        if (rider.online == 0) {
            des = des + '[掉线]';
        };
        if (rider.overTimeCount != 0) {
            des = des + '[' + rider.overTimeCount + '单超时]';
        }

        return des;

    }

    function addRiderMarker(rider) {
        if (rider.workStatus != 1 && rider.workStatus != 2) {
            return;
        };
        var lng = rider.lng;
        var lat = rider.lat;
        if (lng == 0 || lat == 0) {

            return;
        };
        lng = lng / 1000000;
        lat = lat / 1000000;

        var markerName = "normal-color";
        if (rider.workStatus == 2) {
            markerName = "busy-color";
        }

        //自定义点标记内容   
        var markerContent = document.createElement("div");
        markerContent.className = "bm_marker_container";

        var markerBack = document.createElement("div");
        markerBack.className = "bm_circle_marker " + markerName;
        markerContent.appendChild(markerBack);

        var textContainer = document.createElement("div");
        textContainer.className = "marker-text-container";
        markerContent.appendChild(textContainer);

        var textBack = document.createElement("div");
        textBack.className = "marker-text-back";
        textContainer.appendChild(textBack);
        // 点标记中的文本
        var markerSpan = document.createElement("span");
        markerSpan.innerHTML = getRiderString(rider);
        markerSpan.className = 'marker-span';
        markerSpan.style.width = getRiderString(rider).length * 9 + 'px';
        textContainer.appendChild(markerSpan);

        var marker = new AMap.Marker({
            map: map,
            position: new AMap.LngLat(lng, lat), //基点位置
            offset: new AMap.Pixel(-10, -10), //相对于基点的偏移位置
            draggable: false, //是否可拖动
            topWhenClick: true,
            topWhenMouseOver: true,
            content: markerContent //自定义点标记覆盖物内容
        });
        marker.id = rider.id;
        marker.setMap(map); //在地图上添加点

        AMap.event.addListener(marker, 'click', function() {

            if (marker.infoWindow && marker.infoWindow.getIsOpen()) {

                marker.infoWindow.close();
                marker.infoWindow = null;
            } else {
                getRiderWaybills(marker);
            }
        });

        AMap.event.addListener(marker, 'mouseover', function() {
            $('.bm_marker_container').addClass('opacity3');
            var co = marker.getContent();
            co.className = 'bm_marker_container';
        });
        AMap.event.addListener(marker, 'mouseout', function() {

            $('.bm_marker_container').removeClass('opacity3');
        });
    }

    function getRiderWaybills(marker) {
        var riderid = marker.id;
        var rider = fullRiderList[riderid];
        if (!rider) {
            return;
        };

        $.ajax({
                url: '/partner/dispatch/riderWaybillList',
                type: 'POST',
                dataType: 'json',
                data: {
                    riderId: riderid
                },
            })
            .done(function(result) {

                var infoWindow = new AMap.InfoWindow({
                    isCustom: true, //使用自定义窗体
                    content: createInfoWindow(rider, result),
                    offset: new AMap.Pixel(15, -35) //-113, -140
                });
                marker.infoWindow = infoWindow;
                infoWindow.open(map, marker.getPosition());
            })
            .fail(function() {})
            .always(function() {

            });

    }


    function getPhoneType(type) {
        var str = '未知';
        if (type == 1) {
            str = 'Android';
        } else if (type == 2) {
            str = 'iOS';
        }
        return str;
    }

    //构建自定义信息窗体 
    function createInfoWindow(title, result) {

        var info = document.createElement("div");
        info.className = "info";

        //可以通过下面的方式修改自定义窗体的宽高
        //info.style.width = "400px";

        // 定义顶部标题
        var top = document.createElement("div");
        var titleD = document.createElement("div");
        //var closeX = document.createElement("img");
        var closeX = document.createElement("span");
        top.className = "info-top";

        if (title.workStatus == 2) {
            top.style.backgroundColor = "#DC4D3F";
        } else {
            top.style.backgroundColor = "#00abe4";
        }
        top.style.filter = 'alpha(opacity:80)';
        top.style.opacity = 0.8;
        titleD.innerHTML = title.name + '&nbsp;[' + title.mobile + ']';
        titleD.innerHTML += '&nbsp;[' + title.currentWaybillCount + '单]';
        if (result.data.overTimeCount != 0) {
            titleD.innerHTML += '&nbsp;[' + result.data.overTimeCount + '单超时]';
        }
        if (title.online == 0) {
            titleD.innerHTML += '[掉线]';
        }
        titleD.innerHTML = titleD.innerHTML + '&nbsp;&nbsp;[' + result.data.deviceType + '_' + getPhoneType(result.data.osType) + result.data.appVersion + ']';
        titleD.style.color = '#ffffff';

        closeX.innerHTML = '×'
        closeX.setAttribute("aria-hidden", "true");
        closeX.style.position = 'absolute';
        closeX.style.right = '0';
        closeX.style.color = 'white';
        closeX.style.margin = '2px 10px 0 0';
        closeX.style.fontSize = '20px';
        closeX.style.cursor = 'pointer';

        closeX.onclick = closeInfoWindow;


        // 定义中部内容
        var middle = document.createElement("div");
        middle.className = "info-middle";
        middle.style.backgroundColor = 'white';

        var table = document.createElement("table");
        table.className = "table table-bordered table-striped";
        var thead = document.createElement('thead');

        var titleArr = new Array('状态（已用时）', '剩余时间', '商家', '收货地址');
        var tr = document.createElement("tr");
        for (var j = 0; j < 4; j++) {

            var th = document.createElement("th");
            th.className = "text-center";
            th.innerHTML = titleArr[j];
            tr.appendChild(th);
        };
        thead.appendChild(tr);

        var tbody = document.createElement('tbody');


        var waybillList = result.data.waybillList;
        var remark = '';
        var statusTime = 0;
        var remainTime = 0;


        $.each(waybillList, function(index, waybill) {

            var sysTime = result.data.systemTime;
            var sTime = sysTime - waybill.utime - 300;

            var tr = document.createElement("tr");

            var td = document.createElement("td");
            statusTime = 0 - getMinutes(waybill.utime);
            if (waybill.status == 15 && sTime > 0) {
                remark = '派单后超时未确认(' + statusTime + ')';
            } else {
                remark = statusNameDic[new String(waybill.status)] + '(' + statusTime + ')';
            }

            td.innerHTML = remark;
            tr.appendChild(td);

            td = document.createElement("td");
            remainTime = getMinutes(waybill.deliveredTime);
            td.innerHTML = remainTime + '';
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = waybill.senderName + '#' + waybill.poiSeq;
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = waybill.recipientAddress;
            tr.appendChild(td);

            if (waybill.overTime == 1 || (waybill.status == 15 && sTime > 0)) {
                tr.style.color = 'red';
            }

            tbody.appendChild(tr);

        });
        top.appendChild(titleD);
        top.appendChild(closeX);
        info.appendChild(top);

        table.appendChild(thead);
        table.appendChild(tbody);
        middle.appendChild(table);
        info.appendChild(middle);

        // 定义底部内容
        var bottom = document.createElement("div");
        bottom.className = "info-bottom";
        bottom.style.position = 'relative';
        bottom.style.top = '0px';
        bottom.style.margin = '0 auto';
        var sharp = document.createElement("img");
        sharp.src = "http://webapi.amap.com/images/sharp.png";
        bottom.appendChild(sharp);
        info.appendChild(bottom);
        return info;
    }

    function closeInfoWindow() {
        map.clearInfoWindow();
    }
    // function getMinies
    function getMinutes(timestamp) {
        var date = new Date();
        var time = ((timestamp - (date.getTime() / 1000)) / 60).toFixed(0);
        return time;
    }

});