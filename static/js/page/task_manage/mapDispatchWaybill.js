require.config({
    baseUrl: MT.STATIC_ROOT + '/js',
    urlArgs: 'ver=' + pageVersion
});
require(['module/root', 'page/mapUtil'], function(r, mapUtil) {
    var map;
    var drawLineMarkers = new Array();
    var statusNameDic = new Array();
    statusNameDic['-10'] = '需手工指派';
    statusNameDic['15'] = '派单后未确认';
    statusNameDic['0'] = '未调度';
    statusNameDic['10'] = '未接单';
    statusNameDic['20'] = '已接单';
    statusNameDic['30'] = '已取货';
    statusNameDic['50'] = '已送达';
    statusNameDic['99'] = '已取消';

    setOrderNumberSel();
    for (var i in wayBill) { //先对订单进行判空
        if (!wayBill[i]) {
            wayBill[i] = "";
        }
    };
    var billHtml = "";
    var deliveredTime = parseFloat(wayBill.deliveredTime) * 1000;
    deliveredTime = new Date(deliveredTime);
    var tempTimeStr = deliveredTime.getHours() >= 10 ? deliveredTime.getHours() : "0" + deliveredTime.getHours();
    tempTimeStr += ":";
    tempTimeStr += deliveredTime.getMinutes() >= 10 ? deliveredTime.getMinutes() : "0" + deliveredTime.getMinutes();
    var orgNameTemp = wayBill.orgName;
    orgNameTemp = orgNameTemp.length >= 15 ? orgNameTemp.substr(0, 10) + "..." : orgNameTemp;
    billHtml += "<span>[" + orgNameTemp + "]</span>";
    billHtml += (wayBill.isPrebook == "0" ? "<b style='color:red;'>[即]</b>" : "<b>[预]</b>") +
        "<span>" + tempTimeStr + "</span>&nbsp;&nbsp;" +
        "<span>取:";
    var sendName = wayBill.senderName,
        recipientName = wayBill.recipientName;
    if (sendName.length >= 15) {
        sendName = sendName.substr(0, 10) + "...";
    };
    billHtml += sendName + "#" + wayBill.poiSeq + "->&nbsp;送:";
    if (recipientName.length >= 15) {
        recipientName = recipientName.substr(0, 10) + "...";
    };
    billHtml += recipientName +
        "&nbsp;&nbsp;距离:<i class='bill-distance'>" + 3.54 + "</i>&nbsp;￥" + wayBill.pkgPrice +
        "&nbsp;<i class='top-rider-name'></i></span>";
    $(".list-msg").html(billHtml);

    var mapCenter = new Object();
    var start_x = Math.floor(wayBill.senderLng) / 1000000,
        start_y = Math.floor(wayBill.senderLat) / 1000000,
        end_x = Math.floor(wayBill.recipientLng) / 1000000,
        end_y = Math.floor(wayBill.recipientLat) / 1000000;
    if (start_x == 0 || start_y == 0 || end_x == 0 || end_y == 0) {
        alert("返回的经纬度有问题哦！");
        return;
    };

    addNaviTypeDiv();

    var start_xy = new AMap.LngLat(start_x, start_y), //取餐坐标
        end_xy = new AMap.LngLat(end_x, end_y);
    mapCenter.Lng = (start_x + end_x) / 2;
    mapCenter.Lat = (start_y + end_y) / 2;
    // 地图初始化
    var mapStyle = setMapStyle();
    setMapContainerClass(mapStyle);
    map = new AMap.Map('mapContainer', {
        resizeEnable: true,
        mapStyle: mapStyle,
        view: new AMap.View2D({
            // center: [mapCenter.Lng, mapCenter.Lat],
            // zoom: 14
        })
    });
    // 在地图上显示放大缩小的工具
    map.plugin(["AMap.ToolBar"], function() {
        toolBar = new AMap.ToolBar({
            direction: false,
        });
        toolBar.hideLocation();
        map.addControl(toolBar);
    });
    // 在地图上加比例尺
    map.plugin(["AMap.Scale"], function() {
        scale = new AMap.Scale();
        map.addControl(scale);
    });

    addMapEvents();

    function fnDrawWayBill() {
        map.clearMap();
        //根据起点、终点  画订单的路径
        var type = setNaviType();
        if (type == 2) {
            AMap.service('AMap.Walking', function() {
                var walking = new AMap.Walking();
                walking.search(start_xy, end_xy, function(status, data) {
                    if (status === 'complete') {
                        // 画订单的路径
                        fnDrawBillWay(data);
                        fnGetRiderList();
                    } else {
                        map.clearMap();
                        console.log("高德地图没有能为你规划好路径，请刷新重试！")
                    }
                });
            });
        } else {
            AMap.service('AMap.Driving', function() {
                var walking = new AMap.Driving();
                walking.search(start_xy, end_xy, function(status, data) {
                    if (status === 'complete') {
                        // 画订单的路径
                        fnDrawBillWay(data);
                        fnGetRiderList();
                    } else {
                        map.clearMap();
                        console.log("高德地图没有能为你规划好路径，请刷新重试！")
                    }
                });
            });
        }
    }

    function addMapEvents() {

        map.on('moveend', function(e) {
            reDrawArcsIfNeeded();
        });

        map.on('zoomend', function(e) {
            reDrawArcsIfNeeded();
        });

        map.on('resize', function(e) {
            mapUtil.removeCanvas();
            reDrawArcsIfNeeded();
        });

        map.on('dragstart', function(e) {
            mapUtil.clearCanvas();
        });

        map.on('dragend', function(e) {});

        map.on('dragging', function(e) {});
    }

    function reDrawArcsIfNeeded() {
        mapUtil.clearCanvas();
        for (var i = 0; i < drawLineMarkers.length; i++) {
            var marker = drawLineMarkers[i];
            drawMarkerArcLines(marker);
        };
    }

    function drawMarkerArcLines(marker) {
        var lineArr = marker.arcLines;
        if (lineArr && lineArr.length > 0) {
            mapUtil.createCanvasIfNeeded();
            for (var i = 0; i < lineArr.length; i++) {
                var line = lineArr[i];
                drawCurve(line.startX, line.startY, line.endX, line.endY, line.lineColor);
            };
        };
    }

    //画订单的路线
    function fnDrawBillWay(data) {
        var distance = 0;
        distance = data.routes[0].distance / 1000;
        var tempStr = distance.toFixed(2) + "km";
        $(".bill-distance").html(tempStr); //头部的距离是这样添加上去

        map.clearMap();
        var steps = data.routes[0].steps;
        var ways = data.waypoints;
        //添加起点、终点

        var startmarker = new AMap.Marker({
            position: start_xy,
            offset: new AMap.Pixel(-16, -41), //相对于基点的偏移位置
            map: map,
            icon: '/static/imgs/marker-start.png',
        });

        var endmarker = new AMap.Marker({
            position: end_xy,
            offset: new AMap.Pixel(-16, -41), //相对于基点的偏移位置
            icon: '/static/imgs/marker-end.png',
            map: map
        });

        //起点到路线的起点 路线的终点到终点 绘制无道路部分
        var startPath = [];
        startPath.push(start_xy);
        startPath.push(steps[0].path[0]);
        var startLine = new AMap.Polyline({
            map: map,
            path: startPath,
            strokeColor: "green",
            strokeWeight: 6,
            strokeDasharray: [10, 5]
        });
        var endPath = [];
        var path_xy = steps[(steps.length - 1)].path;
        endPath.push(end_xy);
        endPath.push(path_xy[(path_xy.length - 1)]);
        var endLind = new AMap.Polyline({
            map: map,
            path: endPath,
            strokeWeight: 6,
            strokeColor: "green",
            strokeDasharray: [10, 5]
        });
        //绘制服务返回路径 有道路的部分
        var path = [];
        for (var s = 0; s < steps.length; s++) {
            path.push.apply(path, steps[s].path);
        }
        var polyline = new AMap.Polyline({
            map: map,
            path: path,
            strokeWeight: 6,
            strokeColor: "green",
            strokeDasharray: [10, 5]
        });
    }

    //获取url中的参数
    function getUrlParam(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
        var r = window.location.search.substr(1).match(reg); //匹配目标参数
        if (r != null) return unescape(r[2]);
        return null; //返回参数值
    }

    function getRecommandOnly() {
        var check = $("#riderRecommand");
        return check.is(':checked');
    }

    function getBusyYesOrNo() {
        var check = $("#riderBusy");
        var arr = new Array();
        if (check.is(":checked")) { // 被选中了的
            arr.push(1);
        } else {
            arr.push(1);
            arr.push(2);
        }
        return arr;
    };

    function fnGetRiderList() {
        var riderNum = $("#riderNum").val(),
            waybillId = getUrlParam("waybillId"),
            riderBusy = getBusyYesOrNo();
        $.ajax({
            url: '/partner/dispatch/mapDispatchRider',
            type: 'GET',
            dataType: 'json',
            traditional: true,
            data: {
                riderNum: riderNum,
                waybillId: waybillId,
                workStatuses: riderBusy
            },
            success: function(res) {
                var only = getRecommandOnly();
                var maxUnfinish = 65535;
                if ($('#order-number').val() != 0) {
                    maxUnfinish = $('#order-number').val();
                };
                var recommendCount = 0;
                for (var i = 0, n = res.length; i < n; i++) {
                    var rider = res[i];
                    if (rider.recommendDes && rider.recommendDes == '顺路') {
                        recommendCount++;
                    };
                    if (!only || rider.recommendDes == '顺路') {
                        if (rider.currentWaybillCount < maxUnfinish) {
                            (function(arg) {
                                fnAddRiderMarker(arg);
                            })(rider)
                        };
                    };
                };
                $('#recommendCount').text(recommendCount);
            },
            complete: function() {
                map.setFitView();
            },
            error: function() {
                console.log("切换骑手数，请求失败！");
            }
        })
    }

    function saveOrderNumberSel()
    {
        var number = $('#order-number').val();
        localStorage.setItem('map_dispatch_order_number', number);
    }

    function setOrderNumberSel(){
        var number = localStorage.getItem('map_dispatch_order_number');
        if (number) {
            $('#order-number').val(number);
        };
    }

    // 选择骑手数的时候直接请求数据
    $("#riderNum").change(function() {
        init();
    });
    $("#order-number").change(function() {
        saveOrderNumberSel();
        init();
    });
    // 刷新的按钮 
    $("#btnRefresh").click(function() {
        init();
    });
    // 点击忙碌是否的
    $("#riderBusy").click(function() {
        init();
    });

    $("#riderRecommand").click(function() {
        init();
    });

    function addNaviTypeDiv() {
        var body = '<div class="navi_type unselectable">' + '<input type="radio" name="navi-type" value="1"><span>驾车</span>' + '<input type="radio" name="navi-type" value="2"><span>步行</span>' + '</div>';

        $('#mapContainer').append(body);
    }

    function setNaviType() {
        var type = 1;
        if (localStorage.getItem('navi-type')) {
            type = localStorage.getItem('navi-type');
        };
        var selector = '.navi_type [value="' + type + '"]';
        var input = $(selector);
        if (input.length > 0) {
            input.prop('checked', 'checked');
        };
        return type;
    }

    $(document).on('click', '.navi_type input', function(event) {

        var style = $(this).attr('value');
        localStorage.setItem('navi-type', style);
        init();
    });

    function setMapStyle() {
        var style = 'blue_night';
        if (localStorage.getItem('rider-state')) {
            style = localStorage.getItem('rider-state');
        };
        var selector = '.map_style [value="' + style + '"]';
        var input = $(selector);
        if (input.length > 0) {
            input.prop('checked', 'checked');
        };
        return style;
    }
    $('.map_style').on('click', 'input', function(event) {
        var style = $(this).attr('value');
        map.setMapStyle(style);
        setMapContainerClass(style);
        localStorage.setItem('rider-state', style);
    });

    function setMapContainerClass(style)
    {
        if (style == 'blue_night') {
            $('#mapContainer').addClass('map-blue-night');
        }else{
            $('#mapContainer').removeClass('map-blue-night');
        }
    }


    // 每个骑手的路径都是差不多的 只有经纬度不一样
    function fnAddRiderMarker(rider) {

        drawLineMarkers = new Array();
        mapUtil.removeCanvas();

        var markerName = "normal-color";
        if (rider.workStatus == 2) {
            markerName = "busy-color";
        }
        if (rider.id == wayBill.riderId && wayBill.status >= 15) {
            var str = "当前骑手:" + rider.name;
            $(".top-rider-name").html(str);
            markerName = 'current_color';
        };

        var scale = 1000000;

        var Lng = Math.floor(rider.lng),
            Lat = Math.floor(rider.lat);
        if (Lng == 0 || Lat == 0) {
            return;
        }
        Lng = Lng / 1000000;
        Lat = Lat / 1000000;

        //自定义点标记内容   
        var markerContent = document.createElement("div");
        markerContent.className = "bm_rider_marker_container";

        var markerBack = document.createElement("div");
        markerBack.className = "bm_circle_marker " + markerName;
        markerContent.appendChild(markerBack);

        var textContainer = document.createElement("div");
        textContainer.className = "marker-text-container";
        markerContent.appendChild(textContainer);

        var textBack = document.createElement("div");
        textBack.className = "marker-text-back";
        textContainer.appendChild(textBack);

        var markerSpan = document.createElement("span"); //点标记中的文本
        markerSpan.className = "marker-span";
        markerSpan.Lat = Lat;
        markerSpan.Lng = Lng;
        markerSpan.rider = rider;
        if (rider.id == wayBill.riderId && wayBill.status >= 15) {

            markerSpan.canClick = 'none';
        };

        var markerHtml = "",
            coTime = new Date(Math.floor(rider.coTime) * 1000);
        markerHtml += (coTime.getHours() >= 10 ? coTime.getHours() : "0" + coTime.getHours()) + ":" +
            (coTime.getMinutes() >= 10 ? coTime.getMinutes() : "0" + coTime.getMinutes()) + "&nbsp;" +
            rider.name;
        if (rider.online == 0) {
            markerHtml += "[掉线]";
        };
        if (rider.jobType == 2) {
            markerHtml += "[兼职]";
        };
        markerHtml += "[" + rider.currentWaybillCount + "单]";
        if (rider.overTimeCount != 0) {
            markerHtml += "[" + rider.overTimeCount + "单超时]";
        }
        if (rider.recommendDes) {
            markerHtml += "[" + rider.recommendDes + "]";
        };
        markerSpan.innerHTML = markerHtml;
        textContainer.appendChild(markerSpan);

        var marker = new AMap.Marker({
            map: map,
            position: new AMap.LngLat(Lng, Lat), //基点位置
            offset: new AMap.Pixel(-10, -10), //相对于基点的偏移位置
            draggable: false, //是否可拖动
            topWhenClick: true,
            topWhenMouseOver: true,
            content: markerContent //自定义点标记覆盖物内容
        });
        marker.id = rider.id;
        markerSpan.marker = marker;
        marker.setMap(map); //在地图上添加点

        var sendMarker, getMarker;
        marker.marker1 = new Array();
        marker.marker2 = new Array();

        AMap.event.addListener(marker, 'mouseover', function() {
            // 当前骑手的位置
            $('.bm_rider_marker_container').addClass('opacity3');
            var co = marker.getContent();
            co.className = 'bm_rider_marker_container';

            if (hasMarkerId(marker.id)) {
                return;
            };
            marker.arcLines = new Array();
            mapUtil.createCanvasIfNeeded();
            drawLineMarkers.push(marker);

            var riderX = Math.floor(rider.lng) / 1000000,
                riderY = Math.floor(rider.lat) / 1000000;

            var lineColor = '#ff0000';
            var lineIndex = 0;
            // 已取餐
            var fetchArr = rider.fetchedWaybillTraceList;
            if (rider.fetchedWaybillTraceListSize != 0) {

                for (var i = 0; i < fetchArr.length; i++) {

                    var recipientX = Math.floor(fetchArr[i].recipientLng) / 1000000,
                        recipientY = Math.floor(fetchArr[i].recipientLat) / 1000000;

                    lineColor = mapUtil.getArcLineColor(lineIndex);
                    lineIndex++;
                    curve = drawCurve(recipientX, recipientY, riderX, riderY, lineColor);
                    if (curve) {
                        marker.arcLines.push(curve);
                    };
                    // 开始位置
                    getMarker = addPositionMarker(recipientX, recipientY, lineColor);
                    marker.marker1.push(getMarker);
                };
            };
            // 骑手到送餐的路线  派单为确认和未取餐同一状态===都是未取餐
            // 未取餐
            var unfetchArr = new Array(),
                desigArr = new Array();
            if (rider.unfetchWaybillTraceList && rider.unfetchWaybillTraceList.length > 0) {
                unfetchArr = rider.unfetchWaybillTraceList;
            };
            if (rider.designateWaybillTraceList && rider.designateWaybillTraceList.length > 0) {
                desigArr = rider.designateWaybillTraceList;
            };
            unfetchArr.concat(desigArr);
            for (var i = 0; i < unfetchArr.length; i++) {

                lineColor = mapUtil.getArcLineColor(lineIndex);
                lineIndex++;

                var recipientX = Math.floor(unfetchArr[i].recipientLng) / 1000000,
                    recipientY = Math.floor(unfetchArr[i].recipientLat) / 1000000;
                var getX = Math.floor(unfetchArr[i].senderLng) / 1000000,
                    getY = Math.floor(unfetchArr[i].senderLat) / 1000000;

                curve = drawCurve(riderX, riderY, getX, getY, lineColor);
                if (curve) {
                    marker.arcLines.push(curve);
                };
                curve = drawCurve(getX, getY, recipientX, recipientY, lineColor);
                if (curve) {
                    marker.arcLines.push(curve);
                };
                getMarker = addPositionMarker(getX, getY, lineColor, '取');
                marker.marker2.push(getMarker);
                sendMarker = addPositionMarker(recipientX, recipientY, lineColor);
                marker.marker2.push(sendMarker);
            };
        });

        AMap.event.addListener(marker, 'mouseout', function() {

            $('.bm_rider_marker_container').removeClass('opacity3');
            if (marker.infoWindow && marker.infoWindow.getIsOpen()) {

            } else {
                clearPositionMakerAndLines(marker);
            }
        });

        if (wayBill.riderId != rider.id) {
            var pos = marker.getPosition();
            AMap.event.addListener(marker, 'click', function() {

                if (marker.infoWindow && marker.infoWindow.getIsOpen()) {

                    marker.infoWindow.close();
                    marker.infoWindow = null;
                    clearPositionMakerAndLines(marker);
                } else {
                    getRiderLocBills(pos, rider, marker);
                }
            });
        };
    };

    function getDomPos(lng, lat) {
        var lll = new AMap.LngLat(lng, lat);
        return map.lngLatToContainer(lll);
    }

    function drawCurve(startX, startY, endX, endY, lineColor) {

        if (!startX || !startY || !endX || !endY) {
            return null;
        };
        var sPos = getDomPos(startX, startY);
        var ePos = getDomPos(endX, endY);

        mapUtil.drawCurve(sPos, ePos, lineColor);
        return {
            startX: startX,
            startY: startY,
            endX: endX,
            endY: endY,
            lineColor: lineColor,
        };
    }

    function addPositionMarker(x, y, lineColor, text) {
        var markerContent = mapUtil.getSendMarkerContent(lineColor, text);

        var marker = new AMap.Marker({
            map: map,
            position: new AMap.LngLat(x, y), //基点位置
            offset: new AMap.Pixel(0, 0), //相对于基点的偏移位置
            draggable: false, //是否可拖动
            content: markerContent //自定义点标记覆盖物内容
        });
        marker.setMap(map);
        return marker;
    }

    function clearPositionMakers(marker) {
        for (var i = 0; i < marker.marker1.length; i++) {
            if (marker.marker1[i]) marker.marker1[i].setMap(null);
        }
        for (var i = 0; i < marker.marker2.length; i++) {
            if (marker.marker2[i]) marker.marker2[i].setMap(null);
        }

        marker.marker1 = new Array();
        marker.marker2 = new Array();
    }

    function clearPositionMakerAndLines(marker) {

        clearPositionMakers(marker);
        removeArcMarker(marker);
        redrawOrRemoveCanvas();
    }

    function removeArcMarker(marker) {
        var newArr = new Array();
        for (var i = 0; i < drawLineMarkers.length; i++) {
            var other = drawLineMarkers[i];
            if (marker.id != other.id) {
                newArr.push(other);
            };
        };
        drawLineMarkers = newArr;
    }

    function redrawOrRemoveCanvas() {
        if (drawLineMarkers.length < 1) {
            mapUtil.removeCanvas();
            return;
        };
        reDrawArcsIfNeeded();
    }

    // 获取当前骑手的手中的订单
    function getRiderLocBills(pos, rider, marker) {
        $.ajax({
                url: '/partner/dispatch/riderWaybillList',
                type: 'POST',
                dataType: 'json',
                data: {
                    riderId: rider.id
                },
            })
            .done(function(result) {
                var infoWindow = new AMap.InfoWindow({
                    isCustom: true, //使用自定义窗体
                    content: createInfoWindow(rider, result),
                    offset: new AMap.Pixel(13, -35) //-113, -140
                });
                marker.infoWindow = infoWindow;
                infoWindow.open(map, pos);
                if (clearOtherMarker(marker)) {
                    drawLineMarkers = new Array();
                    drawLineMarkers.push(marker);
                    reDrawArcsIfNeeded();
                };
            })
            .fail(function() {
                console.log("获取当前骑手的手中的订单失败！");
            })
    };

    function getPhoneType(type) {
        var str = '未知';
        if (type == 1) {
            str = 'Android';
        } else if (type == 2) {
            str = 'iOS';
        }
        return str;
    }

    function clearOtherMarker(marker) {
        var has = false;
        for (var i = 0; i < drawLineMarkers.length; i++) {
            var other = drawLineMarkers[i];
            if (other.id != marker.id) {
                has = true;
                clearPositionMakers(other);
            };
        };
        return has;
    }

    function hasMarkerId(markerId) {
        var has = false;
        for (var i = 0; i < drawLineMarkers.length; i++) {
            var other = drawLineMarkers[i];
            if (other.id == markerId) {
                has = true;
                break;
            };
        };
        return has;
    }

    //构建自定义信息窗体 
    function createInfoWindow(rider, result) {
        var info = document.createElement("div");
        info.className = "info";

        //可以通过下面的方式修改自定义窗体的宽高
        // 定义顶部标题
        var top = document.createElement("div");
        var titleD = document.createElement("div");
        var closeX = document.createElement("span");
        top.className = "info-top";
        top.style.backgroundColor = "#00abe4";
        top.style.filter = 'alpha(opacity:80)';
        top.style.opacity = 0.8;
        titleD.innerHTML = rider.name;
        if (rider.jobType == 2) {
            titleD.innerHTML += "[兼职]";
        };
        titleD.innerHTML += '&nbsp;[' + rider.mobile + ']';
        titleD.innerHTML += '&nbsp;[共' + rider.currentWaybillCount + "单]";
        if (rider.overTimeCount != 0) {
            titleD.innerHTML += '[' + rider.overTimeCount + '单超时]';
        };
        titleD.innerHTML = titleD.innerHTML + '&nbsp;&nbsp;[' + result.data.deviceType + '_' + getPhoneType(result.data.osType) + result.data.appVersion + ']';
        closeX.innerHTML = '×';
        closeX.setAttribute("aria-hidden", "true");
        closeX.style.position = 'absolute';
        closeX.style.right = '0';
        closeX.style.color = 'white';
        closeX.style.margin = '2px 10px 0 0';
        closeX.style.fontSize = '20px';
        closeX.style.cursor = 'pointer';
        closeX.onclick = closeInfoWindow;

        // // 定义中部内容
        var middle = document.createElement("div");
        middle.className = "info-middle";
        middle.style.backgroundColor = 'white';

        var table = document.createElement("table");
        table.className = "table table-striped";
        table.style.margin = "0px";
        var thead = document.createElement('thead');

        var titleArr = new Array('状态', '剩余', '商家', '收货地址', '距离', "金额");
        var tr = document.createElement("tr");
        for (var j = 0; j < 6; j++) {
            var th = document.createElement("th");
            th.className = "style-min-width";
            th.innerHTML = titleArr[j];
            tr.appendChild(th);
        };
        thead.appendChild(tr);

        var tbody = document.createElement('tbody');
        var waybillList = result.data.waybillList;
        var remark = '';
        var statusTime = 0;
        var remainTime = 0;
        $.each(waybillList, function(index, waybilTemp) {
            var tr = document.createElement("tr");

            var sysTime = result.data.systemTime;
            var sTime = sysTime - waybilTemp.utime - 300;

            var td = document.createElement("td");
            statusTime = 0 - getMinutes(waybilTemp.utime);
            if (waybilTemp.status == 15 && sTime > 0) {
                remark = '派单后超时未确认(' + statusTime + ')';
            } else {
                remark = statusNameDic[new String(waybilTemp.status)] + '(' + statusTime + ')';
            }

            td.innerHTML = remark;
            tr.appendChild(td);

            td = document.createElement("td");
            remainTime = getMinutes(waybilTemp.deliveredTime);
            td.innerHTML = remainTime + '';
            if (waybilTemp.isPrebook) {
                td.innerHTML = '<span class="blueSpan">[预]</span>' + remainTime;
            };
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = waybilTemp.senderName + '#' + waybilTemp.poiSeq;
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = waybilTemp.recipientAddress;
            tr.appendChild(td);

            td = document.createElement("td");
            var distant = parseFloat(waybilTemp.distant) / 1000;
            td.innerHTML = distant.toFixed(2);
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = waybilTemp.pkgPrice;
            tr.appendChild(td);

            if (waybilTemp.overTime == 1 || (waybilTemp.status == 15 && sTime > 0)) {
                tr.style.color = 'red';
            }
            tbody.appendChild(tr);
        });

        var appointRider = document.createElement("div");
        appointRider.className = "info-appointRider";
        appointRider.style.backgroundColor = "#fff";
        appointRider.style.padding = "10px 0 10px 10px";
        var appointRiderHtml = document.createElement("button");
        var str = "";

        appointRiderHtml.className = "btn btn-default";
        appointRiderHtml.status = 0;
        var locBillId = getUrlParam("waybillId");
        appointRiderHtml.riderId = rider.id; //存的是当前的rider的id  后面可以通过这个id来找当前的骑手信息

        appointRiderHtml.className = "btn btn-default btn-appoint";
        appointRiderHtml.status = 10;
        str = '指派给"' + rider.name + '"';
        // 默认为派单吧 因为派单直接点击就行 不用弹窗
        if (wayBill.status >= 20) {
            // 默认定义为改派
            appointRiderHtml.className = "btn btn-default btn-repay";
            appointRiderHtml.status = 20;
            str = '改派给&nbsp;"' + rider.name + '"';
        };

        if (wayBill.status == 15) {
            appointRiderHtml.className = "btn btn-default btn-appoint";
            appointRiderHtml.status = 10;
            str = '改派给&nbsp;"' + rider.name + '"';
        };
        appointRiderHtml.innerHTML = str;
        appointRider.appendChild(appointRiderHtml);

        // 定义底部内容
        var bottom = document.createElement("div");
        bottom.className = "info-bottom";
        bottom.style.position = 'relative';
        bottom.style.top = '0px';
        bottom.style.margin = '0 auto';
        var sharp = document.createElement("img");
        sharp.src = "http://webapi.amap.com/images/sharp.png";

        top.appendChild(titleD);
        top.appendChild(closeX);
        info.appendChild(top);

        table.appendChild(thead);
        table.appendChild(tbody);
        middle.appendChild(table);

        info.appendChild(middle);
        info.appendChild(appointRider);

        bottom.appendChild(sharp);
        info.appendChild(bottom);
        return info;
    }

    $('body').delegate('.btn-repay', 'click', function() {
        map.clearInfoWindow();
        var _this = $(this);
        var riderNum = $("#riderNum").val(),
            waybillId = getUrlParam("waybillId");
        var status = _this[0].status,
            riderBusy = getBusyYesOrNo();
        $.ajax({
            url: '/partner/dispatch/mapDispatchRider',
            type: 'GET',
            dataType: 'json',
            traditional: true,
            data: {
                riderNum: riderNum,
                waybillId: waybillId,
                workStatuses: riderBusy
            },
            success: function(res) {
                for (var i = 0, n = res.length; i < n; i++) {
                    var rider = res[i];
                    if (rider.id == _this[0].riderId) {
                        showReassignDlg(rider, status);
                        break;
                    };
                }
            }
        })
    });

    $(document).delegate('.marker-span', 'click', function() {
        var _this = $(this)[0];

        var Lat = parseFloat(_this.Lat),
            Lng = parseFloat(_this.Lng),
            can = _this.canClick; //是否可以点击 none不可以
        var pos = new AMap.LngLat(Lng, Lat);

        if (can != "none") {
            getRiderLocBills(pos, _this.rider, marker);
        };
    });

    $('body').delegate('.btn-appoint', 'click', function() {
        var _this = $(this);
        var riderNum = $("#riderNum").val(),
            waybillId = getUrlParam("waybillId"),
            riderBusy = getBusyYesOrNo();
        var status = _this[0].status;
        $.ajax({
            url: '/partner/dispatch/mapDispatchRider',
            type: 'GET',
            dataType: 'json',
            traditional: true,
            data: {
                riderNum: riderNum,
                waybillId: waybillId,
                workStatuses: riderBusy
            },
            success: function(res) {
                for (var i = 0, n = res.length; i < n; i++) {
                    var rider = res[i];
                    if (rider.id == _this[0].riderId) {
                        assignToRider(rider.id);
                        break;
                    };
                }
            }
        })
    });


    function closeInfoWindow() {
        map.clearInfoWindow();
        for (var i = drawLineMarkers.length - 1; i >= 0; i--) {
            var marker = drawLineMarkers[i];
            clearPositionMakers(marker);
        };
        drawLineMarkers = new Array();
        mapUtil.removeCanvas();
    }

    function getMinutes(timestamp) {
        var date = new Date();
        var time = ((timestamp - (date.getTime() / 1000)) / 60).toFixed(0);
        return time;
    }

    // 点击改派 成功与否
    function getRiderWaybills(rider, status) {
        $.ajax({
            url: "/partner/dispatch/appointRider.ajax",
            method: 'POST',
            data: {
                waybillId: getUrlParam("waybillId"),
                uid: rider.id
            },
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function(data) {
                if (!data.suc) {
                    showMsg(data.msg);
                } else {
                    showMsg('配送单已经成功指派给骑士');
                }
            }
        });
    }

    // 显示是否改派的信息
    function showMsg(msg) {
        showTip(msg, 'info');
    }

    function showTip(tip, type) {
        var $tip = $('#msgTip');
        if ($tip.length == 0) {
            $tip = $('<span id="msgTip" style="z-index:1050;"></span>');
            $('body').append($tip);
        }
        $tip.stop(true).attr('class', 'alert alert-' + type).text(tip).css('margin-left', -$tip.outerWidth() / 2).fadeIn(500).delay(2000).fadeOut(500);
    }

    var detailDia;

    function showReassignDlg(rider, status) {
        if (detailDia) {
            detailDia.remove();
            detailDia = null;
        };

        var body = '<div class="reasonSel"><br>';
        body += '<span style="font-size: 16px;color:red;">注：改派后将发通知给顾客，请勿频繁改派，避免顾客投诉</span><br/>';
        body += '<input type="radio" name="reason" id="reason1" class="assign" /><label class="reason-class" for="reason1" >原骑手不顺路</label><br/>';
        body += '<input type="radio" name="reason" id="reason2" class="assign" /><label class="reason-class" for="reason2">原骑手单多，忙不过来</label><br/>';
        body += '<input type="radio" name="reason" id="reason3" class="assign"/><label class="reason-class" for="reason3">原骑手车没电了/车坏了</label><br/>';
        body += '<input type="radio" name="reason" id="reason4" class="assign"/><label class="reason-class" for="reason4">原骑手手机没电了</label><br/>';
        body += '<input type="radio" name="reason" id="reason5" class="assign"/><label class="reason-class" for="reason5">其他</label><br/>';
        body += '<span id="re-fail" class="pull-right hidden" style="padding-right: 10px; color: #f00">原因不能为空</span>'
        body += '<br><input type="text"  id="re-reason" placeholder="选择‘其他’后可输入(限20个字)" maxlength="20" class="input-sm" readonly="readonly" style="width:440px;background-color: #e8e8e8;box-shadow: none;border-color: #e8e8e8;"></input>';
        body += '</div>';

        body += '改派给&nbsp;' + '<span style="color: #00abe4">（' + rider.name + rider.mobile + '）</span>';

        var sbody = $(body);
        var $failtTip = $('#re-fail', sbody);
        var $failReason = $('#re-reason', sbody);
        var re_other = $('#reason5', sbody);
        var sInput = $("input[name='reason']", sbody);

        sInput.click(function() {
            if (re_other.is(':checked')) {
                $failReason.removeAttr('readonly');
                $failReason.val('');
                $failReason.attr('placeholder', '可输入其他原因，限20个字');
                $failReason.focus();
                $failReason.css('background-color', '#fff');
            } else {
                $failReason.attr('readOnly', 'readOnly');
                $failReason.attr("placeholder", "选择‘其他’后可输入(限20个字)");
                $failReason.blur();
                $failReason.css('background-color', '#e8e8e8');

            }
        });
        detailDia = r.ui.showModalDialog({
            title: '改派操作',
            body: sbody,
            buttons: [
                r.ui.createDialogButton('close', '取消', function() {}), //取消
                r.ui.createDialogButton('sure', '确认改派', function() { // 确认改派
                    var rea = '';
                    if (re_other.is(':checked')) {
                        rea = $failReason.val();
                    } else {
                        rea = $("input[name='reason']:checked").next("label").text();
                    }
                    rea = rea.trim();
                    if (rea.length < 1) {
                        $failtTip.removeClass('hidden');
                    } else {
                        // 改派 现在只有改派的时候是弹窗的
                        var riderId = rider.id;
                        if (status == 20) {
                            reAssignToRider(riderId, rea);
                        }
                    }
                    return true;
                })
            ],
            style: {
                ".modal-body": {
                    "padding-top": "0"
                },
                ".modal-footer": {
                    "border-top": "none",
                    "padding-top": "0"
                },
                ".modal-dialog": {
                    "top": "150px",
                    "width": "480px"
                },
                ".btn-default": {
                    "width": "200px",
                    "float": "left",
                    "margin": "0"
                },
                ".btn:nth-child(odd)": {
                    "margin": "0 25px 0 10px"
                }
            }
        });
        detailDia.on("hidden.bs.modal", function() {
            this.remove();
            detailDia = null;
        });
    }
    // 指派
    function assignToRider(riderId) {
        var billId = getUrlParam("waybillId");
        $.ajax({
            url: "/partner/dispatch/appointRider.ajax",
            method: 'POST',
            data: {
                waybillId: billId,
                uid: riderId
            },
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function(data) {
                if (!data.suc) {
                    showMsg(data.msg);
                } else {
                    map.clearInfoWindow();
                    wayBill.riderId = riderId;
                    init();
                    showMsg('配送单已经成功指派给骑士');
                }
            }
        });
    }
    // 改派
    function reAssignToRider(riderId, reason) {
        var billId = getUrlParam("waybillId");
        $.ajax({
            url: "/partner/dispatch/reDispatchWaybill.ajax",
            method: 'GET',
            data: {
                waybillId: billId,
                uid: riderId,
                reason: reason
            },
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function(data) {
                if (!data.suc) {
                    showMsg(data.msg);
                } else {
                    map.clearInfoWindow();
                    wayBill.riderId = riderId;
                    init();
                    showMsg('配送单已经成功指派给骑士');
                }
            },
            error: function() {
                closeInfoWindow();
                showMsg('请求失败，请重试！');
            }
        });
    }

    function init() {
        fnDrawWayBill();
    };
    init();

});