require.config({
	baseUrl: MT.STATIC_ROOT + '/js',
	urlArgs: 'ver=' + pageVersion,
	shim: {
		'lib/jquery-slider': {
			deps: [
				"lib/jslider/tmpl",
				"lib/jslider/jquery-dependClass",
				"lib/jslider/draggable"
			]
		}
	}
});

require(['module/root', 'lib/jquery-slider', 'lib/datepicker', 'module/cookie'], function(r, slider, datepicker, cookie) {
	document.title = '骑手轨迹';
	var cityListRequest;
	var allOrgList = {};
	var selCity = $('#selectCity');
	var mySelect2 = $.fn.select2;
	var showPolygon;
	// 设置日历不能选当天之后的日期
	var maxDate = new Date();
	var minDate = new Date(Date.now() - 1000 * 90 * 24 * 3600);
	datepicker.set({
		maxDate: maxDate,
		minDate: minDate
	});
	$('.j-toggle-menu').click();
	// ajax loading
	var $ajaxbg = $("#background,#progressBar");
	// init select2
	$riderSelect.select2({
		width: '100%'
	});
	$orderSelect.select2({
		width: '100%'
	});

	// 骑手电话，起止时间
	var phone, orderId, time = fnGetTime(),
		isAuto = false;

	// 选择站点
	selOrg.on('change', function() {

		})
		// 通过下拉选择骑手
	$riderSelect.on('change', function() {
			// 先清空下级select
			$orderSelect[0].innerHTML = '';
			$riderInput.val('');
			$orderSelect.val('');
			phone = $(this).val();
			orderId = null;
			if (phone < 0) {
				alert('请选择骑手');
			} else {
				fnGetOrderList(phone, time);
			}
		})
		//获取url中的参数
	function getUrlParam(name) {
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
		var r = window.location.search.substr(1).match(reg); //匹配目标参数
		if (r != null) return unescape(r[2]);
		return null; //返回参数值
	};
	// 默认加载的
	(function init() {
		var riderPhone = getUrlParam("riderPhone");
		var date = getUrlParam('date');
		if (date) {
			$('#datepickerInput').val(date);
		}
		if (!!riderPhone) {
			if (riderPhone < 0) {
				alert('请选择骑手');
			} else {
				$riderInput.val(riderPhone);
				fnGetOrderList(riderPhone, fnGetTime());
			}
		};

		var platformOrderId = getUrlParam("platformOrderId");
		if (platformOrderId) {
			$orderInput.val(platformOrderId);
			fnSearchOrder(platformOrderId);
		};

		getLoginInfo();

	}());
	// 通过手动输入骑手电话
	$riderInput.on('change', function() {
			var value = $(this).val();
			if (!/^\d+$/.test(value)) {
				alert('请输入正确手机号');
				return false;
			}
			phone = value;
		})
		// 选择日期
	$('#datepickerInput').on('change', function() {
			time = fnGetTime();
			fnGetOrderList(phone, time);
		})
		// 滑块选择时间
	$('#jSliderTime').slider({
		from: 0,
		to: 1440,
		step: 1,
		dimension: '',
		scale: ['0:00', '3:00', '6:00', '9:00', '12:00', '15:00', '18:00', '21:00', '24:00'],
		limits: false,
		calculate: function(value) {
			var hours = Math.floor(value / 60);
			var mins = (value - hours * 60);
			return (hours < 10 ? "0" + hours : hours) + ":" + (mins == 0 ? "00" : mins);
		},
		callback: function(value) {
			time = fnGetTime();
			fnGetOrderList(phone, time);
		}
	});
	// 通过下拉选择订单号
	$orderSelect.on('change', function() {
		var id = $(this).val();
		orderId = (id > 0) ? id : null;
		$orderInput.val('');
		fnSearchOrder(id);
	})
	$orderInput.on('change', function() {
			var value = $(this).val();
			orderId = value;
		})
		// searchBtn
	$('#searchBtn').on('click', function() {
		time = fnGetTime();
		!orderId && (orderId = $orderInput.val());
		if (orderId) {
			$riderInput.val('');
			fnSearchOrder(orderId);
		} else {
			fnGetOrderList(phone, time);
		}
	})

	// functions
	function fnGetTime() {
		var date = $('#datepickerInput').val(),
			dArr = date.split(/-/),
			y = dArr[0],
			m = dArr[1] - 1,
			d = dArr[2];
		var time = $('#jSliderTime').val(),
			tArr = time.split(/;/),
			stime = parseInt(tArr[0] * 60),
			etime = parseInt(tArr[1] * 60);

		var theday = new Date();
		theday.setFullYear(y);
		theday.setMonth(m);
		theday.setDate(d);
		theday.setHours(0);
		theday.setMinutes(0);
		theday.setSeconds(0);
		var stamp = Math.round(theday.getTime() / 1000);
		var res = {
			stime: stamp + stime,
			etime: stamp + etime
		}
		return res;
	}

	function fnGetOrderList(p, t) {
		if (p < 0 || !p) {
			alert('请选择骑手或输入骑手手机号');
			return false;
		}
		$('.time-chooce').removeClass('hidden');
		var isId = p.indexOf("#") > 0;
		var data = {
			fromTime: t.stime,
			toTime: t.etime
		}
		if (isId) {
			data.useId = parseInt(p);
		} else {
			data.mobile = p;
		}
		$.ajax({
			url: '/partner/dispatch/riderTrackDetail',
			type: 'POST',
			dataType: 'json',
			data: data,
			beforeSend: function() {
				$ajaxbg.show();
			},
			success: function(res) {

				clearTrackCar();
				map.clearMap();
				showPolygon = null;
				var data = JSON.parse(res.detail);
				fnFillRiderPath(data);
				fnDrawArea(data.areaCoordinate, data.areaName);
				fnOrderSelectRender(res.platformOrderIds.match(/\d+/g));
			},
			complete: function() {
				$ajaxbg.hide()
			},
			error: function() {
				alert('未能获得骑手在对应时间段内的信息');
			}
		})
	}

	function fnSearchOrder(id) {
		$('.time-chooce').addClass('hidden');
		$.ajax({
			url: '/partner/dispatch/waybillTrack',
			type: 'POST',
			dataType: 'json',
			data: {
				platformOrderId: id
			},
			beforeSend: function() {
				$ajaxbg.show();
			},
			success: function(res) {
				clearTrackCar();
				map.clearMap();
				showPolygon = null;
				fnFillOrderPath(res);
				fnDrawArea(res.areaCoordinate, res.areaName);
				fnAddArrivedPoint(res.progressCoordinate);
				fnfnAddGrabPoint(res.grabCoordinate);
				if (res.errorMsg && res.errorMsg.length > 0) {
					alert(res.errorMsg);
				};
			},
			complete: function() {
				$ajaxbg.hide()
			},
			error: function() {
				alert('error:waybillTrack')
			}
		})
	}

	function fnRiderSelectRender(list) {
		var html = '';
		html += '<option value="-1" selected>请选择骑手</option>';
		if (list) {
			var n = list.length;
			for (var i = 0; i < n; i++) {
				if (list[i].jobStatus == 1) {
					html += '<option value="' + list[i].mobile + '">';
					html += list[i].name + '[' + list[i].mobile + ']';
				} else {
					html += '<option value="' + list[i].id + '#">';
					html += list[i].name + '[' + list[i].mobile + ']' + '(离职)';
				}
				html += '</option>';
			}
		} else {
			alert('该站点下骑手列表信息为空')
		}
		$riderSelect[0].innerHTML = html;
		$riderSelect.select2({
			width: '100%'
		});
	}

	function fnOrderSelectRender(list) {
		var html = '';
		html += '<option value="-1">请选择订单号</option>';
		if (list) {
			var n = list.length;
			for (var i = 0; i < n; i++) {
				html += '<option value="' + list[i] + '">' + list[i] + '</option>';
			}
		}
		$orderSelect[0].innerHTML = html;
		$orderSelect.select2({
			width: '100%'
		});
	}

	function setMapStyle() {
		var style = 'blue_night';
		if (localStorage.getItem('rider-state')) {
			style = localStorage.getItem('rider-state');
		};
		var selector = '.map_style [value="' + style + '"]';
		var input = $(selector);
		if (input.length > 0) {
			input.prop('checked', 'checked');
		} else {
			style = 'blue_night';
			$('.map_style [value="blue_night"]').prop('checked', 'checked');
		}
		return style;
	}
	$('.map_style').on('click', 'input', function(event) {
		var style = $(this).attr('value');
		map.setMapStyle(style);
		localStorage.setItem('rider-state', style);
	});
	// 初始化地图对象，加载地图
	var mapStyle = setMapStyle();
	map = new AMap.Map("rider-track-map", {
		view: new AMap.View2D({
			center: new AMap.LngLat(116.498720, 40.008911),
			resizeEnable: true,
			zoom: 14
		}),
		mapStyle: mapStyle,
		resizeEnable: true,
		keyboardEnable: false
	});
	map.plugin(['AMap.ToolBar'], function() {
		toolBar = new AMap.ToolBar({
			direction: false,
		});
		toolBar.hideLocation();
		map.addControl(toolBar);
	});
	map.plugin(["AMap.Scale"], function() {
		scale = new AMap.Scale();
		map.addControl(scale);
	});

	function clearTrackCar() {
		if (trackMarker) {
			AMap.event.removeListener(trackMarker.endListener);
			trackMarker.stopMove();
			trackMarker.setMap(null);
		};
		trackMarker = null;
	}

	$('#track_pause').click(function(event) {
		if ($('#mapContainer').hasClass('pause')) {
			fnPauseFillPath(false);
			$('#track_play').addClass('active');
			if (trackMarker) {
				fnMoveCar();
			};
		} else {
			fnPauseFillPath(true);
			$('#track_play').removeClass('active');
			if (trackMarker) {
				trackMarker.stopMove();
			};
		}
		$('#mapContainer').toggleClass('pause');
	});

	var trackMarker;
	$('#track_play').click(function(event) {

		if (!steps || steps.length < 1) {
			return;
		};
		$('#mapContainer').removeClass('pause');
		$(this).addClass('active');

		clearTrackCar();
		temp = steps.concat();
		trackMarker = new AMap.Marker({
			map: map,
			position: temp.shift(),
			icon: "/static/imgs/track_car.png",
			offset: new AMap.Pixel(-13, -6),
			autoRotation: true
		});
		fnMoveCar();
		trackMarker.endListener = AMap.event.addListener(trackMarker, "moveend", function(e) {

			temp.shift();
			fnMoveCar();
		});
		// fnReplay(temp);
		// fnDrawFullPath(steps);
		// if (pointsArr.length) {
		// 	for (var i = 0, n = pointsArr.length; i < n; i++) {
		// 		fnMark(pointsArr[i][0], pointsArr[i][1])
		// 	}
		// }
	});

	$('.track-tip-title').hover(function() {

		$('.track-tip').removeClass('hidden');
	}, function() {
		$('.track-tip').addClass('hidden');
	});

	var rangingTool;
	map.plugin(["AMap.RangingTool"], function() {
		rangingTool = new AMap.RangingTool(map);
		AMap.event.addListener(rangingTool, "end", function(e) {
			rangingTool.turnOff();
			if (showPolygon) {
				showPolygon.show()
			};
			$('#RangingTool').removeClass('active');
		});
	});

	$('#RangingTool').click(function(event) {

		$(this).addClass('active');
		rangingTool.turnOn();
		if (showPolygon) {
			showPolygon.hide()
		};
	});
	$('#full_screen_btn').click(function(event) {

		if ($('#mapContainer').hasClass('full')) {
			$('span', $(this)).text('全屏');
		} else {
			$('span', $(this)).text('退出');
		}
		$('#mapContainer').toggleClass('full');
	});

	var ra = 1000000; // ratio for LngLat
	var steps, temp, interval, pointsArr, pauseMarker;

	function fnFillOrderPath(data) {


		$('#rider-track-map')[0].classList.add('order_path');
		$('#rider-track-map')[0].classList.remove('rider_path');
		document.querySelector('.rider-track-map').classList.remove('pause');
		var rp = data.recipientCoordinate, // recieve person
			sp = data.senderCoordinate; // send person
		// 将需要mark的点放在pointsArr里以便重播的时候调用
		pointsArr = [];
		pointsArr.push([rp, '送达']);
		pointsArr.push([sp, '取货']);
		fnMark(rp, '送达');
		fnMark(sp, '取货');

		if (!data.bmWaybillTraceList || data.bmWaybillTraceList.length < 1) {
			clearInterval(interval);
			steps = [];
			return false;
		}

		var startIndex = 0;
		var endIndex = data.bmWaybillTraceList.length - 1;
		for (var i = 0; i < data.bmWaybillTraceList.length; i++) {
			if (data.startTime < data.bmWaybillTraceList[i].ctime) {
				startIndex = i;
				break;
			};
		};
		for (var i = data.bmWaybillTraceList.length - 1; i >= 0; i--) {
			if (data.endTime > data.bmWaybillTraceList[i].ctime) {
				endIndex = i;
				break;
			};
		};
		var startPoint = data.bmWaybillTraceList[startIndex];
		var endPoint = data.bmWaybillTraceList[endIndex];
		pointsArr.push([startPoint, '起点']);
		pointsArr.push([endPoint, '终点']);
		fnMark(startPoint, '起点');
		fnMark(endPoint, '终点');

		var arr = fnPolyLine(data.bmWaybillTraceList, startIndex, endIndex);
		var pre = fnPolyLine(data.bmWaybillTraceList, 0, startIndex);
		var after = fnPolyLine(data.bmWaybillTraceList, endIndex);
		fnDrawFullPath(pre, 'dashed');
		fnDrawFullPath(after, 'dashed');
		steps = arr;
		fnDrawFullPath(steps);
		temp = arr.concat();
		clearInterval(interval);
	}

	function fnDrawArea(areaCoordinate, areaName) {

		if (areaCoordinate && areaCoordinate.length > 0) {

			var polygonArr = new Array();
			for (var i = 0; i < areaCoordinate.length; i++) {
				polygonArr.push([areaCoordinate[i].lng / ra, areaCoordinate[i].lat / ra]);
			};
			var lineArr = polygonArr.concat();
			lineArr.push(polygonArr[0]);
			drawAreaPolygon(lineArr);
			var polygon = new AMap.Polygon({
				path: polygonArr, //设置多边形边界路径
				strokeColor: "#6c553c", //线颜色
				strokeWeight: 4, //线宽
				fillColor: "#FFFFFF", //填充色
				fillOpacity: 0 //填充透明度
			});
			polygon.areaName = areaName;
			polygon.setMap(map);
			showPolygon = polygon;
			AMap.event.addListener(polygon, 'mouseover', function() {

				var infoWindow = new AMap.InfoWindow({
					isCustom: true,
					content: createAreaInfoWindow(polygon.areaName),
					offset: new AMap.Pixel(0, -5),
					autoMove:false
				});
				var pathArr = polygon.getPath();
				infoWindow.open(map, new AMap.LngLat(pathArr[0].lng, pathArr[0].lat));
				polygon.infoWindow = infoWindow;
			});

			AMap.event.addListener(polygon, 'mouseout', function() {

				if (polygon.infoWindow) {
					map.clearInfoWindow();
					polygon.infoWindow = null;
				}
			});
		};
	}

	function drawAreaPolygon(polygonArr) {

		var polyline = new AMap.Polyline({
			map: map,
			path: polygonArr,
			strokeColor: '#6c553c',
			strokeWeight: 4,
		})
	}

	function createAreaInfoWindow(title) {
		var info = document.createElement("div");
		info.className = "info";
		info.innerHTML = title;
		return info;
	}

	function fnFillRiderPath(data) {
		$('#rider-track-map')[0].classList.add('rider_path');
		$('#rider-track-map')[0].classList.remove('order_path');
		document.querySelector('.rider-track-map').classList.remove('pause');
		// marker
		var deliver = data.deliverCoordinate,
			fetch = data.fetchCoordinate;
		pointsArr = [];
		if (deliver.length) {
			for (var i = 0, n = deliver.length; i < n; i++) {
				pointsArr.push([deliver[i], '送达']);
				pointsArr.push([fetch[i], '取货']);
				fnMark(deliver[i], '送达');
				fnMark(fetch[i], '取货');
			}
		}
		// polyline
		if (!data.bmUserCoordinateHisViews.length) {
			clearInterval(interval);
			steps = [];
			temp = [];
			return false;
		} else {
			var startPoint = data.bmUserCoordinateHisViews[0];
			var endPoint = data.bmUserCoordinateHisViews[data.bmUserCoordinateHisViews.length - 1];
			pointsArr.push([startPoint, '起点']);
			pointsArr.push([endPoint, '终点']);
			fnMark(startPoint, '起点');
			fnMark(endPoint, '终点');
		}
		var arr = fnPolyLine(data.bmUserCoordinateHisViews);
		steps = arr;
		fnDrawFullPath(steps);
		temp = arr.concat();
		clearInterval(interval);
	}

	function fnDrawFullPath(arr, strokeStyle) {

		var style = 'solid';
		if (strokeStyle) {
			style = strokeStyle;
		};
		var polyline = new AMap.Polyline({
			map: map,
			path: arr,
			strokeColor: '#0d9bf2',
			strokeStyle: style,
			strokeWeight: 4,
			strokeOpacity: .7
		})
		map.setFitView();
		map.setCenter(arr[0]);
	}

	function fnDrawPath(arr) {
		if (arr.length <= 1) {
			clearInterval(interval);
			return false;
		}
		var points = [];
		points.push(arr.shift());
		points.push(arr[0]);
		var polyline = new AMap.Polyline({
				map: map,
				path: points,
				strokeColor: '#F51E1E',
				strokeWeight: 4,
				strokeOpacity: 1
			})
			// map.setFitView();
			// map.setCenter(arr[0]);
	}

	function fnMoveCar() {
		if (temp.length <= 1) {
			fnTrackPlayEnd();
		} else if (trackMarker) {
			trackMarker.moveTo(temp[0], 500);
		}
	}

	function fnTrackPlayEnd() {
		$('#track_play').removeClass('active');
		clearTrackCar();
	}

	function fnfnAddGrabPoint(pos) {
		if (!pos) {
			return;
		};
		if (pos.lng == 0 || pos.lat == 0) {
			return;
		};
		var m = document.createElement('div');
		m.className = 'marker_grab';
		var n = document.createElement('span');
		var time = fnTimeToTime(pos.ctime);
		n.innerHTML = '骑手' + time + '上报的抢单位置';
		m.appendChild(n);
		var marker = new AMap.Marker({
			map: map,
			position: new AMap.LngLat(pos.lng / ra, pos.lat / ra),
			offset: new AMap.Pixel(-5, -5),
			content: m
		})
	}

	function fnAddArrivedPoint(pos) {

		if (!pos) {
			return;
		};
		if (pos.lng == 0 || pos.lat == 0) {
			return;
		};
		var m = document.createElement('div');
		m.className = 'marker_arrive';
		var n = document.createElement('span');
		var time = fnTimeToTime(pos.ctime);
		n.innerHTML = '骑手' + time + '上报的到店位置';
		m.appendChild(n);
		var marker = new AMap.Marker({
			map: map,
			position: new AMap.LngLat(pos.lng / ra, pos.lat / ra),
			offset: new AMap.Pixel(-5, -5),
			content: m
		})
	}

	function fnMark(pos, type) {
		if (!pos) {
			return false;
		}
		var type = type || '';

		var offset = new AMap.Pixel(-5, -5);
		if ($('#rider-track-map').hasClass('order_path')) {
			offset = new AMap.Pixel(-14, -40);
		};
		var m = document.createElement('div');
		switch (type) {
			case '取货':
				m.className = 'marker_quhuo';
				var n = document.createElement('span');
				n.innerHTML = fnTimeToTime(pos.ctime)
				m.appendChild(n);
				break;
			case '送达':
				m.className = 'marker_songda';
				var n = document.createElement('span');
				n.innerHTML = fnTimeToTime(pos.ctime)
				m.appendChild(n);
				break;
			case '起点':
				m.className = 'marker_start';
				var n = document.createElement('span');
				n.innerHTML = fnTimeToTime(pos.ctime)
				m.appendChild(n);
				offset = new AMap.Pixel(-14, -40);
				break;
			case '终点':
				m.className = 'marker_end';
				var n = document.createElement('span');
				n.innerHTML = fnTimeToTime(pos.ctime)
				m.appendChild(n);
				offset = new AMap.Pixel(-14, -40);
				break;
			default:
				m.className = 'marker';
				break;
		}

		var marker = new AMap.Marker({
			map: map,
			position: new AMap.LngLat(pos.lng / ra, pos.lat / ra),
			offset: offset,
			content: m
		})
	}

	function fnPauseFillPath(isPause) {
		if (!interval) {
			return false;
		}
		if (isPause) {
			clearInterval(interval);
		} else {
			interval = setInterval(function() {
				fnDrawPath(temp)
			}, 200)
		}
	}

	function fnReplay(arr) {
		map.clearMap();
		// temp = arr.concat();
		clearInterval(interval);
		interval = setInterval(function() {
			fnDrawPath(arr);
		}, 200)
	}

	function fnPolyLine(data, startIndex, endIndex) {
		var start = 0;
		var end = data.length;
		if (startIndex) {
			start = startIndex;
		};
		if (endIndex != undefined) {
			end = endIndex + 1;
		};
		var arr = [];
		for (var i = start, n = end; i < n; i++) {
			var pos = data[i];
			arr.push(new AMap.LngLat(pos.lng / ra, pos.lat / ra));
		}
		return arr;
	}

	function fnPauseMarker(data) {
		var pos = data[0];
		var m = document.createElement('div');
		m.className = 'marker_pause'
		var n = document.createElement('span');
		n.innerHTML = fnTimeToTime(pos.ctime);
		m.appendChild(n);
		pauseMarker = new AMap.Marker({
			map: map,
			position: new AMap.LngLat(pos.lng, pos.lat),
			offset: new AMap.Pixel(-10, -34),
			content: m
		})
	}

	function fnRemovePauseMarker() {
		if (pauseMarker) {
			pauseMarker.setMap(null);
		}
	}

	function fnTimeToDate(time) {
		var stamp = time * 1000;
		var date = new Date();
		date.setTime(stamp);
		var m = date.getMonth() + 1,
			d = date.getDate(),
			h = date.getHours(),
			f = date.getMinutes(),
			s = date.getSeconds();
		var str = m + '/' + d + '&nbsp;' + (h > 9 ? h : '0' + h) + ':' + (m > 9 ? m : '0' + m) + ':' + (s > 9 ? s : '0' + s);
		return str;
	}

	function fnTimeToTime(time) {
		if (time == 0) {
			return '';
		}
		var date = new Date();
		date.setTime(time * 1000);
		var h = date.getHours(),
			m = date.getMinutes(),
			s = date.getSeconds();
		var str = (h > 9 ? h : '0' + h) + ':' + (m > 9 ? m : '0' + m) + ':' + (s > 9 ? s : '0' + s);
		return str;
	}

	function getStoredCity() {
		var cityId = 0;
		var argCity = r.utils.urlArg('cityId');
		if (argCity) {

			cityId = argCity;
		} else {
			var userId = cookie.getCookie("userId");
			var storedCity = localStorage.getItem(userId + 'riderTrack_selectedCity');
			if (storedCity && storedCity != 'undefined') {
				cityId = storedCity;
			};
		}

		return cityId;
	}

	function storeCityId(cityId) {
		var userId = cookie.getCookie("userId");
		localStorage.setItem(userId + 'riderTrack_selectedCity', cityId);
	}

	function getLoginInfo() {
		var cityId = getStoredCity();
		_getLoginInfo(cityId);
	}

	function _getLoginInfo(cityId) {
		if (!cityId) {
			cityId = 0;
		};
		if (cityListRequest) {
			return;
		};
		cityListRequest = $.ajax({
				url: '/partner/dispatch/homeLoginInfo',
				type: 'GET',
				dataType: 'json',
				data: {
					cityId: cityId
				},
			})
			.done(function(result) {
				if (result.code == 0) {
					$('.main-filters').removeClass('hidden');
					$('#loading-city').addClass('hidden');

					fillCityList(result.data.bmCityList, result.data.selectCityId);
					refreshOrgData(result.data.bmOrgList);

					fillAreaList(true);
				} else {
					$('#reload-base').removeClass('hidden');
					$('#base-info').text(result.msg);
				}
			})
			.fail(function() {
				$('#reload-base').removeClass('hidden');
				$('#base-info').text('加载失败，请重试');
			})
			.always(function() {
				cityListRequest = null
			});
	}

	function fillAreaList(useArg) {
		fillOrgList(useArg);
	}

	function fillOrgList(useArg) {
		$('#selectOrg').empty();
		var options = '';
		var count = 0;
		for (orgid in allOrgList) {

			var org = allOrgList[orgid];
			options = options + '<option' + ' value=' + org.orgid;
			options = options + '>' + org.name;
			options = options + '</option>';
			selected = '';
			count++;
		}

		options = '<option value="-1" selected="selected">未选择站点</option>' + options;

		selOrg.unbind('change');
		selOrg.append(options);
		$.fn.select2.amd.require(['select2/compat/matcher'], function(oldMatcher) {
			selOrg.select2({
				matcher: oldMatcher(matchPinyin),
				width: '100%'
			});
		});
		selOrg.on("change", function(e) {
			var orgId = parseInt($(this).val());
			var orgList = [];
			orgList.push(orgId);
			if (orgId > 0) {

				showRiderInput(false);
				// 请求指定站点下对应的骑手列表
				$.ajax({
					url: '/partner/dispatch/riderTracklist',
					type: 'POST',
					dataType: 'json',
					data: {
						orgList: orgList
					},
					traditional: true, // 必要参数
					beforeSend: function() {
						$ajaxbg.show();
					},
					success: function(res) {
						fnRiderSelectRender(res);
					},
					complete: function() {
						$ajaxbg.hide()
					},
					error: function() {
						alert('无法获取对应站点下骑手列表')
					}
				})
			} else {
				showRiderInput(true);
			}
		});
	}

	function showRiderInput(show) {
		// 先清空下级select
		$riderSelect[0].innerHTML = '';
		if (!show) {
			$riderInput.val('').parent().hide();
			$orderInput.val('').parent().hide();
			$riderSelect.parent().show();
			$orderSelect.parent().show();
		} else {
			$riderInput.parent().show();
			$orderInput.parent().show();
			$riderSelect.parent().hide();
			phone = null;
			orderId = null;
			$orderSelect.parent().hide();
		}
	}

	function refreshOrgData(data) {
		allOrgList = {};
		if (data) {
			for (var i = 0; i < data.length; i++) {
				var orgView = data[i];
				var org = {};
				org.orgid = orgView.orgId;
				org.name = orgView.orgName;
				org.dispatchStrategyDes = orgView.dispatchStrategyDes;
				org.areaId = orgView.areaId;

				allOrgList[orgView.orgId] = org;
			};
		}
	}

	function getAreaListWithCity(cityId) {
		$.ajax({
				url: '/partner/dispatch/homeLoginInfo',
				type: 'GET',
				dataType: 'json',
				data: {
					cityId: cityId
				},
			})
			.done(function(result) {
				if (result.code == 0) {

					refreshOrgData(result.data.bmOrgList);
					fillAreaList();
				} else {
					alert('获取组织信息失败，请重试');
				}
			})
			.fail(function() {
				refreshOrgData();
			})
			.always(function() {});
	}

	function fillCityList(data, selCityId) {
		cityListGot = true;
		var body = '';
		for (var i = 0; i < data.length; i++) {
			var city = data[i];
			if (selCityId == city.city_id) {
				body += '<option selected="selected" value="' + city.city_id + '">' + city.name + '</option>';
			} else {
				body += '<option value="' + city.city_id + '">' + city.name + '</option>';
			}
		};
		selCity.append(body);
		$.fn.select2.amd.require(['select2/compat/matcher'], function(oldMatcher) {
			selCity.select2({
				matcher: oldMatcher(matchPinyin),
				width: '100%'
			});
		});

		// 城市的下拉框
		selCity.on("change", function(e) {
			var cityId = getSelCityid();
			storeCityId(cityId);
			getAreaListWithCity(cityId);
			showRiderInput(true);
		});
		var cityId = getSelCityid();
		storeCityId(cityId);
	}

	function getSelCityid() {
		var cityid;
		$('#selectCity option:selected').each(function() {
			cityid = $(this).val();
		});
		return cityid;
	}
});