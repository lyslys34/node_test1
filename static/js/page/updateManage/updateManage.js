require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/root', 'module/validator','module/cookie'], function (t, validator,cookie) {
    var citys;
    var stations;
    $(document).ready(function() {

           var os_type=sessionStorage.getItem("os_type");
                  var app_type=sessionStorage.getItem("app_type");
                  var release_type=sessionStorage.getItem("release_type");



        gen_op_log();
        getCityList();
        getReleaseDeviceList();

        $("[name='release_type']").change(function () {

        })

        $('#btn-add-new-version').click(function() {
          console.log("sss");
            window.location.href=('/updateManage/add');
        });

        closeAlert = function () {
          $('#operation-alert').fadeOut();
          $('#operation-alert').empty();
        }
          saveDetailId=function(e){
          var id=e.attr("datail_href");
          console.log("c"+id);
          $("#modal"+id).modal();
        }
        undoRelease=function (e) {

          $("#undoRelease").modal();
          sessionStorage.setItem("undo_id",e.attr("app-id"));
          /*
          $('#operation-alert').fadeOut();
          $('#operation-alert span').empty();

          $.ajax({
            url: '/updateManage/undoRelease',
            type: 'POST',
            dataType: 'json',
            data: {id: e.parent(".panel-title-span").prevAll('.app-id').text()}
          })
          .done(function(data) {
            success_alert(data.msg);
            console.log("undo success");
          })
          .fail(function() {
            console.log("undo error");
          })
          .always(function() {
            console.log("undo complete");
          });
*/
        }


       $("#undo_button").on("click",function(){
          var reason=$("#undo_reason").val();
          if(reason==""){
            alert("请输入原因");
          }
          else{
             $('#operation-alert').fadeOut();
                 $('#operation-alert span').empty();

                 $.ajax({
                   url: '/updateManage/undoRelease',
                   type: 'POST',
                   dataType: 'json',
                   data: {id: sessionStorage.getItem("undo_id")}
                 })
                 .done(function(data) {
                   console.log("undo success");
                 })
                 .fail(function() {
                   console.log("undo error");
                 })
                 .always(function() {
                   console.log("undo complete");
                      setTimeout(function () {
                          var $my_form=$("#select_form");
                              $my_form.submit();
                         }, 500);
                 });
          }
        });

        edit_config = function (e) {
          modal_init(e);
          $('#update').modal('show');
          current_id  = e.attr("cityid");;
          $('#current-id').text(current_id);
          console.log(current_id);
        }

        updateRelease = function (e) {

          description = $('#description-update').val();
          force_update = $('#force_update').val();
          os = $('#current-os').text();

          release_station = new Object();
          citys = new Array();
          orgs = new Array();

          release_device = $('.device-select').val();
          if (os == 2) {
            release_device = $('.device-select-ios').val();
          }
          if (release_device == null) {
            release_device = new Array();
          }

          release_station.citys = $('.city-select').val();
          release_station.orgs = $('.station-select').val();
          if (!release_station.citys) {
            release_station.citys=new Array();
          }
          if (!release_station.orgs) {
            release_station.orgs=new Array();
          }
          console.log(JSON.stringify(release_station));
          $.ajax({
            url: '/updateManage/updateRelease',
            type: 'POST',
            dataType: 'json',
            data: { release_station: JSON.stringify(release_station),
                    description: description,
                    force_update: force_update,
                    id: $('#current-id').text(),
                    release_device: JSON.stringify(release_device),
                    mobiles: $('#mobiles-update').val()
                  }
          })
          .done(function(data) {
            $('#update').modal('hide');
            success_alert(data.msg);
          })
          .fail(function() {
          })
          .always(function() {
            $('#current-id').remove();
          });
        }

    });

    function getReleaseDeviceList() {
      $('.release-device').each(function () {
        if ($(this).text() != '' && $(this).text() != 'null') {
          devices = JSON.parse($(this).text());
          if (devices.length>0) {
            for (var x in devices) {
              $(this).next('.gray-device').append(devices[x]+'&nbsp;&nbsp;&nbsp;&nbsp;');
            }
          }
        }
      });
    }

    function idToName () {
      $('.release-station').each(function () {
        if ($(this).text() != '') {
          release_target = JSON.parse($(this).text());
          city_ids = release_target.citys;
          if (city_ids.length>0) {
            $(this).next('.gray-station').append('<strong>城市</strong>: &nbsp;');
            for (var x in city_ids) {
              id = city_ids[x];
              $(this).next('.gray-station').append($('.city'+id).text()+'&nbsp;&nbsp;');
            }
          }
        }
      });
    }

    function stationIdToName () {
      $('.release-station').each(function () {
        if ($(this).text() != '') {
          release_target = JSON.parse($(this).text());
          org_ids = release_target.orgs;
          if (org_ids.length>0) {
            $(this).next('.gray-station').append('<strong>站点</strong>: &nbsp;');
            for (var x in org_ids) {
              id = org_ids[x];
              $(this).next('.gray-station').append($('.org'+id).text()+'&nbsp;&nbsp;');
            }
          }
        }
      });
    }

    function getCityList () {
      //devices
      $.getJSON('/updateManage/getAllDevice', {}, function(json, textStatus) {
        device_list = JSON.parse(json.data);
        for(var i in device_list){
                device = device_list[i];
                if (device.match('iPhone') != null || device.match('iPod') != null) {
                  $('#device_option_ios').parent().append('<option value="'+device+'" >'+device+'</option>');
                  continue;
                }
                $('#device_option').parent().append('<option value="'+device+'" >'+device+'</option>');
        }
      });
      //citys
      $.ajax({
        url: '/org/citySearchList',
        type: 'POST',
        dataType: 'JSON',
        data: {}
      })
      .done(function(data) {
        console.log("request success");
        citys = data.data;
        for(var i in citys){
                city = citys[i].value;
                id = citys[i].id;
                $('.city_option').parent().append('<option class="city'+id+'" value="'+id+'" >'+city+'</option>');
        }
        idToName();
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("request complete");
      });
      //stations
      $.ajax({
        url: '/org/leafOrgSearchList',
        type: 'POST',
        dataType: 'JSON',
        data: {}
      })
      .done(function(data) {
        console.log("request success");
        stations = data.data;
        for(var i in stations){
                station = stations[i].value;
                id = stations[i].id;
                $('.station_option').parent().append('<option class="org'+id+'" value="'+id+'" >'+station+'</option>');
        }
        // idToName();
        stationIdToName();
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("request complete");
      });
    }

    function modal_init(e) {
     var id = e.attr("cityid");
     var release_type = e.attr("releasetype");
     var os_type = e.attr("ostype");
     var mobile=e.attr("mobile");
     var version=e.attr("app_version");
     var app_type=e.attr("app_type");
     var app_type_content="";
     var os_content=""
     sessionStorage.setItem("app_type_tmp",app_type);
     sessionStorage.setItem("os_type_tmp",os_type);
     if(app_type=="0")
       app_type_content="代理";
     else if(app_type=="1")
       app_type_content="众包";
     else if(app_type=="3")
        app_type_content="自建";
     else if(app_type=="5")
        app_type_content="角马";
        console.log("aaa"+os_type);
     if(os_type=="1"){
        os_content="安卓";
        }
     else if(os_type="2"){
       os_content="iOS"
        }
     $(".modal-title").html("更改发版配置: "+app_type_content+"&nbsp"+os_content+"&nbsp"+version);
      $('#current-os').text(os_type);
      $("#mobiles-update").text(mobile);

      coll = $('#collapse'+id);
      device_s.select2('val', '[]');
      device_s.select2('val', '[]');
      devices = '';
      if (coll.find('.release-device').text() != '' && $(this).text() != 'null') {
        test = coll.find('.release-device').text();
        devices = JSON.parse(test);
        device_s.select2('val', devices);
      }


      $('#release-range').show();

              if (release_type == '0') {
                $('.no-all').hide();
              }else{
                $('.no-all').show();
                    if (os_type == "2") {
                                $('#device').hide();
                                $('#device-ios').show();
                                device_s_ios.select2('val', devices);
                              }else{
                                   $('#device').show();
                                      $('#device-ios').hide();
                              }
              }
      description = coll.find('.description').text();
      force = coll.find('.force-up').text();
      $('#force_update').val(force);

      $('#description-update').empty;
      $('#description-update').text(description);

      releases = coll.find('.release-station').text();
      if (releases != '') {
        citys = JSON.parse(releases).citys;
        orgs = JSON.parse(releases).orgs;
        console.log(orgs);
        city_s.select2('val', citys);
        station_s.select2('val', orgs);
      }

    }

    function removeAllSpace(str) {
      return str.replace(/\s+/g, "");
    }

    function gen_op_log() {
      $('.operator').each(function() {
        operators = $(this).text().split('+');
        id = $(this).nextAll('.app-id').text();
        operator_area = $('#collapse'+id).find('.operation-log');
        for (var o in operators) {
          if (operators[o]) {
            operator = JSON.parse(operators[o]);
            operator_area.children('table').append('<tr> <td>'+operator.name+'</td> <td>'+operator.action+'</td> <td>'+operator.utime+'</td> </tr>');
          }
        }
      });
    }

    function success_alert(msg) {
     var os_type=sessionStorage.getItem("os_type");
              var app_type=sessionStorage.getItem("app_type");
              var release_type=sessionStorage.getItem("release_type");
              console.log(os_type+"-"+app_type+"-"+release_type);
              if( os_type == null){
              os_type=-1;
              }
              if( app_type==null){
              app_type=-1;
              }
              if( release_type == null ){
              release_type=-1;
              }

      setTimeout(function () {

        window.location.assign("/updateManage?app_type="+app_type+"&release_type="+release_type+"&os_type="+os_type);
      }, 500);

    }

});

$("#grayPercentBtn").on("click",function(){
           var release_station=new Object();
              citys=new Array();
              orgs=new Array();
       release_station.citys = $('.city-select').val();
       release_station.orgs = $('.station-select').val();
       var os_type=sessionStorage.getItem("os_type_tmp");
               var release_device="";
               if(os_type==1){
                 release_device=$('.device-select').val();
               }else{
                 release_device=$('.device-select-ios').val();
                }
           if(!release_station.citys){
                  release_station.citys=new Array();
              }
              if(!release_station.orgs){
                  release_station.orgs=new Array();
              }
                   $.ajax({

                          url: '/updateManage/getPercent',
                          type:'POST',
                          dataType:'json',
                          data:{
                                release_station:JSON.stringify(release_station),
                                 release_device: JSON.stringify(release_device),
                                  app_type:sessionStorage.getItem("app_type_tmp"),
                                    os_type:sessionStorage.getItem("os_type_tmp")
                          }
                     })
                     .done(function(data){
                         console.log("---success");
                        $("#gray_percent").html(data.data);
                     })
                     .fail(function(){
                   console.log("error");
               })
                .always(function(){
                    console.log("success");
                })
});

$("#search-versions").on('click', function(){
  //alert("hello");
  var $my_form=$("#select_form");
    $my_form.submit();
  sessionStorage.setItem("app_type",$("#application_type").val());
  sessionStorage.setItem("os_type",$("#system_type").val());
  sessionStorage.setItem("release_type",$("#release_type").val());

});

