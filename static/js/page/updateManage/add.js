
require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/root', 'module/validator','module/cookie'], function (t, validator,cookie) {

    $(document).ready(function() {
     
        getCityList();
        upload_ipa=function(){
          $('#upload-msg').remove();
          $('#upload-btn').attr('disabled','true')
          
        }
               upload_file = function () {
                       $('#upload-msg').remove();
                       $('#upload-btn').attr('disabled','true');
                       var fileObj = document.getElementById("apk-file").files[0]; // 获取文件对象
                       var FileController = "/file/apkupload";                    // 接收上传文件的后台地址
                       if($('#os_type').val()==2)
                         FileController="/file/ipaupload";
                       var form = new FormData();
                       form.append("apk_file", fileObj);                           // 文件对象

                       var app_type=$("#app_type").val();
                       if(app_type==-1){
                       alert("请选择众包或者自建");
                       return ;
                       }
                       form.append("app_type",$('#app_type').val());
                       if($('#os_type').val()==1){
                       $.ajax({
                         url: "/file/apkupload",
                         type: "POST",
                         data: form,
                         processData: false,
                         contentType: false
                       })
                       .done(function(data) {
                           console.log("success");
                           if (data.code==0) {
                               $('#apk-file').parent().append('<div class="alert alert-sm alert-info" id="upload-msg" style="display: inline;padding: 5px;">上传成功</div>');
                               apk = jQuery.parseJSON(data.data);
                               console.log(apk.appVersion);
                               $('#app_version').val(apk.appVersion);
                               $('#code').val(apk.appVersionCode);
                               $('#download_url').val(apk.downloadUrl);
                           } else {
                             $('#apk-file').parent().append('<div class="alert alert-sm alert-info" id="upload-msg" style="display: inline;padding: 5px;">上传失败</div>');
                           };
                           console.log(data.msg);
                       })
                       .fail(function() {
                         $('#upload-btn').removeAttr('disabled');
                           console.log("error");
                       }).always(function () {
                       });
                       }
                       else if($('#os_type').val()==2){
                          $.ajax({
                                url: "/file/ipaupload",
                                type: "POST",
                                data: form,
                                processData: false,
                                contentType: false
                              })
                              .done(function(data) {
                                $('#upload-btn').removeAttr('disabled');
                                  console.log("success");
                                  if (data.code==0) {
                                      $('#apk-file').parent().append('<div class="alert alert-sm alert-info" id="upload-msg" style="display: inline;padding: 5px;">上传成功</div>');
                                      ipa = jQuery.parseJSON(data.data);
                                      console.log(ipa.bundleShoartVersionString);
                                      $('#app_version').val(ipa.bundleShortVersionString);
                                      $('#code').val(ipa.bundleVersionCode);
                                      $('#download_url').val(ipa.downloadUrl);
                                  } else {
                                    $('#apk-file').parent().append('<div class="alert alert-sm alert-info" id="upload-msg" style="display: inline;padding: 5px;">上传失败</div>');
                                  };
                                  console.log(data.msg);
                              })
                              .fail(function() {
                                $('#upload-btn').removeAttr('disabled');
                                  console.log("error");
                              }).always(function () {
                              });
                       }
                   }
            $('#add-new-submit').click(function () {

                       if ($('#download_url').val()=="") {
                           alert("请上传文件");
                           return false;
                       }


                       is_empty=0;
                       $('input[type=text]:visible').each(function () {
                           if ($(this).val()=='') {
                               $(this).focus();
                               $(this).attr('placeholder','不能为空');
                               is_empty=1;
                               return false;
                               };
                       });
                       if (is_empty==1) {
                           return;
                       };
                       $('#msg .col-sm-10').empty();
                       $('#msg').hide();
                       release_station = new Object();
                       citys = new Array();
                       orgs = new Array();
                       release_device = new Array();
                       release_station.citys = $('.city-select').val();
                       release_station.orgs = $('.station-select').val();
                       release_device = $('.device-select').val();
                       all_null=1;
                       if ($('#os_type').val() == 2) {
                         release_device = $('.device-select-ios').val();
                       }
                       if (release_device == null) {
                         release_device = new Array();
                       }else{
                         all_null=0;
                       }
                       if (!release_station.citys) {
                         release_station.citys=new Array();
                       }else{
                         all_null=0;
                       }
                       if (!release_station.orgs) {
                         release_station.orgs=new Array();
                       }else{
                         all_null=0;
                       }
                       if(!(mobiles.length==0)){
                         all_null=0;
                       }
                       if($("input[name='release_type']:checked").val()==0){
                       all_null=0;
                       }
                       if(all_null==1){

                         alert("请输入灰度范围");
                         return ;
                       }

                       console.log(JSON.stringify(release_station));
                       console.log(JSON.stringify(release_device));
                       $.ajax({
                           url: '/updateManage/addNewVersion/',
                           type: 'POST',
                           dataType: 'json',
                           data: { app_type: $('#app_type').val(),
                                   os_type: $('#os_type').val(),
                                   force_update: $("input[name='force_update']:checked").val(),
                                   code: $('#code').val(),
                                   release_type: $("input[name='release_type']:checked").val(),
                                   app_version: $('#app_version').val(),
                                   download_url: $('#download_url').val(),
                                   description: $('#description').val(),
                                   release_station: JSON.stringify(release_station),
                                   release_device: JSON.stringify(release_device),
                                   mobiles:$('#mobiles').val()
                                   },
                       })
                       .done(function(data) {
                           console.log("success");
                           if (data.code==0) {
                               $("#success").fadeIn();
                               $("#success").fadeOut(1000);
                               setTimeout(function () {
                                 window.location.assign("/updateManage");
                               }, 2000);
                           } else {
                               alert(data.msg);

                           };
                           console.log(data.msg);
                       })
                       .fail(function() {
                           console.log("error");
                       })
                       .always(function() {
                           console.log("complete");
                       });

                   })
        $("#os_type").change(function () {
            if ($(this).val()==2) {
                $('#apk_upload').show();
          
                $('#device').hide();
                $('#device-ios').show();
                $('#plist_upload').show();
                $("#apk_file").show();
            } else {
                $('#plist_upload').hide();
                $('#device').show();
                $('#device-ios').hide();
                $('#apk_upload').show();
            };
        })

        $('#description').blur(function () {
            if ($(this).val()=='') {
                $(this).focus();
                $(this).attr('placeholder','不能为空');
            };
        })
        

        $("[name='release_type']").change(function () {
            if ($("input[name='release_type']:checked").val()==1) {
                $('#gray').slideDown();
            } else {
                $('#gray').slideUp();
            };

        })
    });

    function getCityList () {
            $.getJSON('/updateManage/getAllDevice', {}, function(json, textStatus) {
              device_list = JSON.parse(json.data);
              for(var i in device_list){
                      device = device_list[i];
                      if (device.match('iPhone') != null || device.match('iPod') != null) {
                        $('#device_option_ios').parent().append('<option value="'+device+'" >'+device+'</option>');
                        continue;
                      }
                      $('#device_option').parent().append('<option value="'+device+'" >'+device+'</option>');
              }
            });

            initPost('/org/citySearchList', {}, function (data) {
                    for(var i in data.data.data){
                            city = data.data.data[i].value;
                            id = data.data.data[i].id;
                            $('#city_option').parent().append('<option value="'+id+'" >'+city+'</option>');
                    }
            });
            initPost('/org/leafOrgSearchList', {}, function (data) {
                    for(var i in data.data.data){
                            station = data.data.data[i].value;
                            id = data.data.data[i].id;
                            $('#station_option').parent().append('<option value="'+id+'" >'+station+'</option>');
                    }
            });
    }

    function initPost(url, data, callback)
                {
                    if(typeof(data) == 'function') { callback = data; data = null; }
                    $.ajax({ type: 'POST', url: url, data:data, success:function(r) {

                        callback({ httpSuccess: true, data: r });

                    }, error: function(XmlHttpRequest, textStatus, errorThrown) {

                        callback({ httpSuccess: false, statusCode:XmlHttpRequest.status });

                    } });
                }

    function btn_click (address) {
        window.location.href=address;
    }

 $("#percent-submit").on("click",function(){
     var releaseStation=new Object();
       citys=new Array();
       orgs=new Array();
       releaseStation.citys=$('.city-select').val();
       releaseStation.orgs=$('.station-select').val();
       if(!releaseStation.citys){
           releaseStation.citys=new Array();
       }
       if(!releaseStation.orgs){
           releaseStation.orgs=new Array();
       }

       var os_type=$('#os_type').val();
       var release_device=null
       if(os_type==1){
         release_device = $('.device-select').val();
       }
       else if(os_type==2){
         release_device = $('.device-select-ios').val();
       }
       if(release_device==null)
       {
            release_device =new Array();
       }
         console.log("release_device"+release_device);
       $.ajax({

            url: '/updateManage/getPercent',
            type:'POST',
            dataType:'json',
            data:{
                  release_station:JSON.stringify(releaseStation),
                  release_device: JSON.stringify(release_device),
                  app_type: $('#app_type').val(),
                  os_type:$('#os_type').val()
            }
       })
       .done(function(data){
           console.log("---success");
          $("#gray_percent").html(data.data);
       })
       .fail(function(){
     console.log("error");
 })
  .always(function(){
      console.log("success");
  })
      })

});


