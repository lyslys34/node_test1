/**
 * 时间选择
 *@version 0.1
 *@author tzw
 *@Date 2015/12/20
 *@description 时间选择空间，0.1版本只是为了在业务中使用
 */
(function(factory) {
        if (typeof define === 'function' && define.amd) {
            // using AMD; register as anon module
            define(['jquery'], factory);
        } else {
            // no AMD; invoke directly
            factory((typeof(jQuery) != 'undefined') ? jQuery : window.Zepto);
        }
    }
    (function($) {
        var timePicker = function(option) {
        	var self = this;
        	this.init();
        }
        timePicker.init = function() {
            $('.hour-list').delegate('span', 'click', function(event) {
                var $self = $(this);
                $('.hour-list').hide().siblings('.minutes-list').data({
                    'minute': $self.text().trim().replace(':00', '')
                }).show();
            });
            $('.minutes-list').delegate('span', 'click', function(event) {
                var $self = $(this);
                $('.minutes-list').hide().data('target').val($('.minutes-list').data('minute') + ':' + $self.text().trim());
            });
        }
        $.fn.timePicker = timePicker;
    }));