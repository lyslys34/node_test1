require.config({
    baseUrl: MT.STATIC_ROOT + '/js',
    urlArgs: 'ver=' + pageVersion,
    shim: {
        'lib/underscore': {
            exports: '_'
        }
    }
});

require(['lib/underscore', 'page/staypoi/tpl', 'page/staypoi/addTimeModal', 'page/staypoi/dott', 'module/head-nav', 'module/cookie'], function(_, tpl, timeModal, dott) {
    window.G_poiTimeData = {};
    window.G_poiTimeDelList = [];

    var Main = {
        $poiId: $('input[name="poiId"]'),
        $loadWrap: $('.loading-wrap'),
        $listPoiBody: $('.search-list-poi tbody'),
        $listTimeBody: $('.search-list-time tbody'),
        $btnAddTime: $('.btn-add-time'), // 新增驻店商家
        $errorSearch: $('.error-info-search'), // 查询商家的错误消息
        $resultMsg: $('.operate-wrap .result-msg'),
        $btnSave: $('#main-container .btn-save'),
        poi_tmpl: null,
        time_tmpl: null,
        tmpl_no_data: null,
        del_confrim_tpl_simple: null,
        queryData: null,
        isModify: false,
        poiId: "",
        init: function() {
            Main.initData();
            Main.initEvent();
        },
        /**
         * 获取相关数据
         * @return 
         */
        initData: function() {
            Main.poi_tmpl = _.template(tpl.poi_addmodify_tr_tpl);
            Main.time_tmpl = _.template(tpl.time_tr_tpl);
            Main.tmpl_no_data = _.template(tpl.modal_no_data);
            Main.del_confrim_tpl_simple = _.template(tpl.del_confrim_tpl_simple);
            Main.queryData = dott.getQueryData();
            Main.isModify = ('poiId' in Main.queryData);
            Main.poiId = Main.queryData['poiId'] || "";
            // 编辑状态
            if (Main.isModify) {
                $("#main-container").addClass("modify");
                document.title = "修改驻店商家";
                Main.getPoiList();
                Main.getTimeList();
            } else {
                // 添加驻店时间按钮的状态，0：可点击，1：不可点击
                Main.changeBtnDisable(0);
                // 新增时保存按钮状态
                Main.$btnSave.attr('disabled','disabled');
            }
        },
        /**
         * 绑定事件
         * @return 
         */
        initEvent: function() {
            // 查询商家输入框
            Main.$poiId.on('keyup', function(event) {
                Main.$errorSearch.hide();
                // 回车
                if (event.keyCode == 13) {
                    $(this).siblings('button').click();
                }
            });
            // 查询按钮点击,新增
            $('.btn-search-poi').on('click', function(event) {
                if (!Main.$poiId.val().trim()) return;
                Main.getPoiList();
            });
            // 新增驻店时间
            Main.$btnAddTime.on('click', function() {
                $.when(timeModal.show()).done(Main.refreshTimeList);
            });
            // 编辑驻店时间
            Main.$listTimeBody.delegate('.fa-edit', 'click', function(event) { // 编辑
                var $tr = $(this).closest('tr');
                $.when(timeModal.show($tr.attr('data-timeId'))).done(Main.refreshTimeList);
            }).delegate('.fa-delete-t', 'click', function(event) { // 删除
                var $tr = $(this).closest('tr');
                $(Main.del_confrim_tpl_simple()).modal("show")
                    .delegate('.btn-save', 'click', function(event) {
                        $tr.remove();
                        G_poiTimeDelList.push($tr.attr('data-timeId'));
                        delete G_poiTimeData[$tr.attr('data-timeId')];
                    });
            });
            // 删除驻店商家
            // 保存,本质是删除
            Main.$btnSave.on('click', function(event) {
                Main.savePoiInfo();
            });
            // 取消
            $('#main-container .btn-cancel').on('click', function(event) {
                location.href = "/staypoi/index";
            });
        },
        /**
         * 更改添加驻店时间按钮的状态
         * @param  {number} status 1:enable 0:disable
         * @return 
         */
        changeBtnDisable: function(status) {
            status == 1 ? Main.$btnAddTime.removeAttr('disabled') : Main.$btnAddTime.attr('disabled', 'disabled');
        },
        /**
         * 根据下拉框返回响应的城市id和站点id
         * @return {object} {cityId:xx,stationId:xx}
         */
        getCityIdAreaId: function() {
            return {
                'cityId': Main.queryData['cityId'],
                'stationId': Main.queryData['stationId'],
            }
        },
        /**
         * 通过 城市、站点、骑手名称、poiId查询驻店poi
         * poiId:门店id
         * @return 
         */
        getPoiList: function() {
            Main.$loadWrap.show();
            var _url = Main.isModify ? "/staypoi/getStayPoi" : "/staypoi/getPoiToStay";
            $.ajax({
                    url: _url,
                    dataType: 'json',
                    data: $.extend({
                        'poiId': Main.$poiId.val().trim() || Main.queryData.poiId,
                        'pageSize': 20,
                        'pageNo': 1
                    }, Main.getCityIdAreaId())
                })
                .done(function(data) {
                    Main.$listPoiBody.empty();
                    if (data['resultCode'] == 0) {
                        var tmplData = data['data'];
                        if (!$.isArray(data['data'])) tmplData = [tmplData];
                        data['data'] && tmplData.length && Main.$listPoiBody.html(Main.poi_tmpl({
                            'data': tmplData
                        }));
                        !Main.isModify && (Main.poiId = Main.$poiId.val().trim()) && Main.changeBtnDisable(1);
                    } else {
                        Main.$errorSearch.text(data['resultMsg']).show();
                        Main.changeBtnDisable(0);
                    }
                })
                .fail(function() {
                    Main.$errorSearch.text("获取列表信息失败").show();
                })
                .always(function() {
                    Main.$loadWrap.hide();
                });
        },
        /**
         * 查询驻店时间信息
         * @return 
         */
        getTimeList: function() {
            $.ajax({
                    url: '/staypoi/getTimeAndRiderInfoByPoiId',
                    dataType: 'json',
                    data: {
                        'poiId': Main.poiId
                    }
                })
                .done(function(data) {
                    if (data['resultCode'] == 0) {
                        var j = data['data'].length;
                        var tmpData = [];
                        for (var i = 0; i < j; i++) {
                            // 删除列表中不存在才需要显示到页面上
                            if (window.G_poiTimeDelList.indexOf(data['data'][i]['timeId'] + "") < 0) {
                                tmpData.push(data['data'][i]);
                            }
                        }
                        Main.$listTimeBody.html(Main.time_tmpl($.extend({
                            'data': tmpData
                        }, {
                            'getWeekDayName': Main.getWeekDay
                        })));
                        // 保存到全局变量中
                        $.each(data['data'], function(index, item) {
                            var riders = [];
                            $.each(item['riders'], function(_index, _item) {
                                riders.push({
                                    'riderId': _item['riderId'],
                                    'riderName': _item['riderName'],
                                    'riderPhone': _item['riderPhone']
                                });
                            });
                            var m_timeId = (item['timeId']);
                            window.G_poiTimeData[m_timeId] = {
                                'timeId': m_timeId,
                                'timePeriod': item['timePeriod'],
                                'weekDay': item['weekDay'],
                                'riders': riders
                            };
                        });

                    } else {
                        alert(data['resultMsg']);
                    }
                })
                .fail(function() {
                    alert("获取驻店时间失败");
                })
                .always(function() {
                });
        },
        /**
         * 获取星期的名字
         * @param  {number} weekDay 星期 0-6 分别表示周一到周日
         * @return {string}         星期一到星期日
         */
        getWeekDay: function(weekDay) {
            var weekNames = ['一', '二', '三', '四', '五', '六', '日'];
            if (typeof weekDay != 'undefined' && typeof weekDay == 'number') {
                return "星期" + weekNames[weekDay];
            }
            if (typeof weekDay != 'undefined' && typeof weekDay == 'string') {
                for (var i = 0; i < weekNames.length; i++) {
                    if (weekDay == "星期" + weekNames[i]) {
                        return i;
                    }
                }
                return -1;
            }
            return "";
        },
        /**
         * 保存驻店商家信息
         * @return 
         */
        savePoiInfo: function() {
            var timeIds = [];
            var delTimeIds = window.G_poiTimeDelList.join(",");
            Main.$listTimeBody.find('tr').each(function() {
                timeIds.push($(this).attr('data-timeId'));
            });
            if (!delTimeIds.length) {
                Main.showResultMsg();
                setTimeout(function() {
                    location.href = "/staypoi/index";
                }, 1000);
                return;
            }
            if (!timeIds.length) {
                $(Main.tmpl_no_data({
                    'notime': 1
                })).modal("show").delegate('.btn-save', 'click', function(event) {
                    deleteTimes(delTimeIds);
                })
            } else {
                deleteTimes(delTimeIds);
            }

            function deleteTimes(timeIds) {
                $.ajax({
                        url: '/staypoi/deleteTime',
                        dataType: 'json',
                        data: {
                            'timeIds': timeIds,
                            'cityId': Main.queryData['cityId']
                        }
                    })
                    .done(function(data) {
                        if (data['resultCode'] == 0) {
                            Main.showResultMsg();
                            setTimeout(function() {
                                location.href = "/staypoi/index";
                            }, 1000);
                        } else {
                            Main.showResultMsg(data['resultMsg']);
                        }
                    })
                    .fail(function() {
                        alert("驻店商家信息保存失败");
                    })
                    .always(function() {
                    });

            }
        },
        /**
         * 显示保存
         * @param  {string} txt 内容
         * @return 
         */
        showResultMsg: function(txt) {
            !txt && (txt = "保存成功！");
            Main.$resultMsg.text(txt).show();
        },
        /**
         * 重新生成时间列表
         * @return 
         */
        refreshTimeList: function() {
            // 直接从全局变量中取，这个废弃掉，因为如果此时从页面删除，因为媒体有timeId，所以请求是不成功的，需要重新刷新
            // var data = [];
            // for (var i in window.G_poiTimeData) {
            //     data.push({
            //         'poiId': Main.$poiId.val().trim() || Main.queryData.poiId,
            //         'timeId': window.G_poiTimeData[i]['timeId'],
            //         'weekDay': Main.getWeekDay(window.G_poiTimeData[i]['weekDay']),
            //         'timePeriod': window.G_poiTimeData[i]['timePeriod'],
            //         'riders': window.G_poiTimeData[i]['riders']
            //     });
            // }
            // Main.$listTimeBody.html(Main.time_tmpl({
            //     'data': data
            // }));
            Main.getTimeList();
            // 新增时保存按钮状态
            Main.$btnSave.removeAttr('disabled');
        }
    }

    Main.init();
});