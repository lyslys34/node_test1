/*
 *增加驻店时间模态框
 */
define(['lib/underscore', 'page/staypoi/tpl', 'page/staypoi/dott'], function(_, tpl, dott) {
    var $modal = $('#timeModal');
    var tmpl = _.template(tpl.modal_tpl);
    var tmpl_tr = _.template(tpl.modal_tr_tpl);
    var tmpl_no_data = _.template(tpl.modal_no_data);
    var m_timeId = null;
    var $dtd;
    var isModify = false;
    var Main = {
        queryData: null,
        init: function() {
            Main.queryData = dott.getQueryData();
            Main.initEvent();
        },
        initEvent: function() {
            $modal.bind('click', function(event) { // 时间选择框消失
                var $tar = $(event.target || event.srcElement);
                if (!$tar.hasClass('timepicker') && !$tar.closest('.hour-list').length && !$tar.closest('.minutes-list').length) {
                    $(this).find('.hour-list').hide();
                }
            }).delegate('.modal .btn-cancel', 'click', function(event) { // 模态框的取消
                $(this).closest('.modal').slideUp();
                $('.modal-backdrop').slideUp();
                $dtd.reject();
            }).delegate('.timepicker', 'click', function(event) { // 时间显示
                var $self = $(this);
                if ($self.siblings('.time-list').is(':visible')) return;
                var pad = ($self.outerWidth() - $self.width()) / 2;
                var posi = $self.position();
                var left = posi.left + pad;
                var top = $self.outerHeight() + 2;
                var cssText = {
                    'left': left,
                    'top': top
                }
                $self.siblings('.hour-list,.minutes-list').css(cssText).data({
                    'target': $self
                });
                if ($self.attr('name') == 'endTime') {
                    Main.setTimeDisabled();
                } else {
                    Main.resetTimeDisabled();
                }
                $self.siblings('.hour-list').show();
            }).delegate('input[name="startTime"]', 'change', function(event) {
                var $self = $(this);
                var $endTime = $('[name="endTime"]');
                if ($self.val() >= '23:30') {
                    $endTime.val('24:00');
                }else{
                    $modal.find('input[name="endTime"]').trigger('change');
                }

            }).delegate('input[name="endTime"]', 'change', function(event) {
                var $self = $(this);
                var $startTime = $('[name="startTime"]');
                var startArr = $startTime.val().split(':');
                var endArr = ["", ""];
                if (startArr[1] >= 30) {
                    endArr[1] = Number(startArr[1]) - 30;
                    ((endArr[1] + "").length == 1) && (endArr[1] = "0" + endArr[1]);
                    endArr[0] = Number(startArr[0]) + 1;
                    ((endArr[0] + "").length == 1) && (endArr[0] = "0" + endArr[0]);
                } else {
                    endArr[0] = Number(startArr[0]);
                    endArr[1] = Number(startArr[1]) + 30;
                }
                if ($self.val() <= $startTime.val()) {
                    $self.val(endArr.join(':'));
                }
            }).delegate('.hour-list span', 'click', function(event) { // 小时选择
                var $self = $(this);
                if ($self.hasClass('disabled')) return;
                $('.hour-list').hide().siblings('.minutes-list').data({
                    'minute': $self.text().trim().replace(':00', '')
                }).show();
            }).delegate('.minutes-list span', 'click', function(event) { // 分钟选择
                var $self = $(this);
                if ($self.hasClass('disabled')) return;
                $('.minutes-list').hide().data('target').val($('.minutes-list').data('minute') + ':' + $self.text().trim()).trigger('change');
            }).delegate('.fa-delete-t', 'click', function(event) { // 删除
                $(this).closest('.row,tr').remove();
            }).delegate('.btn-search-rider', 'click', function(event) { //添加配送员
                var phone = $modal.find('[name="riderPhone"]').val().trim();
                if (!/^1\d{10}$/.test(phone)) {
                    $modal.find('.error-info-rider').text("请输入正确的手机号").show();
                    return;
                }
                if ((!isModify && !$('input[type="checkbox"]:checked').length) || !$('input[name="startTime"]').val() || !$('input[name="endTime"]').val()) {
                    $modal.find('.error-info-rider').text("请先选择日期和时间").show();
                    return;
                }
                if ($modal.find('.search-list td:contains("' + phone + '")').length) {
                    $modal.find('.error-info-rider').text("配送员已存在").show();
                    return;
                }
                Main.searchRider(phone);

            }).delegate('.btn-save', 'click', function(event) { // 保存
                if (!$modal.find(".search-list tbody tr").length) { //没有骑手
                    $(tmpl_no_data({
                        'norider': 1
                    })).modal("show").delegate('.btn-save', 'click', function(event) {
                        $modal.modal('hide');
                        isModify && Main.deleteTimes(m_timeId);
                    });
                } else {
                    Main.saveRiderInfo();
                }
            });
        },
        /**
         * 设置时间不可用
         */
        setTimeDisabled: function() {
            var $startTime = $('[name="startTime"]');
            var startArr = $startTime.val().split(':');
            var $hourList = $('.hour-list'),
                $minutesList = $('.minutes-list');
            var $hSpans = $hourList.find('span'),
                $mSpans = $minutesList.find('span');
            $hSpans.filter(':lt(' + startArr[0] + ')').addClass('disabled');
            if (startArr[1] >= 30) {
                $hSpans.eq(startArr[0]).addClass('disabled');
            } else {
                //$mSpans.eq(0).addClass('disabled');
            }
        },
        /**
         * 重置时间不可用
         */
        resetTimeDisabled: function() {
            var $hourList = $('.hour-list'),
                $minutesList = $('.minutes-list');
            $hourList.find('span.disabled').removeClass('disabled');
            $minutesList.find('span.disabled').removeClass('disabled');
        },
        /**
         * 删除modify的列表的某项
         * @param  {[type]} timeIds [description]
         * @return {[type]}         [description]
         */
        deleteTimes: function(m_timeId) {
            $.ajax({
                    url: '/staypoi/deleteTime',
                    dataType: 'json',
                    data: {
                        'timeIds': m_timeId,
                        'cityId': Main.queryData['cityId']
                    }
                })
                .done(function(data) {
                    if (data['resultCode'] == 0) {
                        delete window.G_poiTimeData[m_timeId];
                        m_timeId && (m_timeId.indexOf('t_') < 0) && window.G_poiTimeDelList.push(m_timeId);
                        $dtd.resolve();
                    } else {
                        $modal.find('.result-msg').addClass('error').text(data['resultMsg']).show();
                    }
                })
                .fail(function() {
                    $modal.find('.result-msg').addClass('error').text("删除驻店时间失败").show();
                })
                .always(function() {
                });
        },
        /**
         * 保存操作，直接保存数据库
         * @return 
         */
        saveRiderInfo: function() {
            var $chks = $modal.find('input[type="checkbox"]:checked');
            var $trs = $modal.find('.search-list tbody tr');
            var startTime = $modal.find('[name="startTime"]').val();
            var endTime = $modal.find('[name="endTime"]').val();
            var riders = [];
            var timeAndRider;
            var poiId = ($('input[name="poiId"]').val().trim() || Main.queryData['poiId']);
            // 列表
            $trs.each(function(index, el) {
                var $tds = $(el).find('td');
                riders.push({
                    "riderId": $(el).attr('data-riderId'),
                    "riderName": $tds.eq(0).text(),
                    "riderPhone": $tds.eq(1).text()
                });
            });
            timeAndRider = {
                'timeId': m_timeId,
                'timePeriod': startTime + "-" + endTime,
                'riders': riders
            };

            $.ajax({
                    url: '/staypoi/saveTimeAndRiderInfo',
                    dataType: 'json',
                    data: {
                        'poiId': poiId,
                        'timeAndRiderJson': JSON.stringify(timeAndRider),
                        'cityId': Main.queryData['cityId'],
                        'weekDays': Main.getWeekInfo().join(',')
                    }
                })
                .done(function(data) {
                    var resultMsg = isModify ? "驻店时间编辑成功！" : "驻店时间添加成功！";
                    if (data['resultCode'] != 0) {
                        if (data['resultMsg'] == '时间设置有冲突') {
                            $modal.find('.error-info-time').text("时间设置有冲突").show();
                        }
                        $modal.find('.result-msg').addClass('error').text(data['resultMsg']).show();
                    } else {
                        $modal.find('.result-msg').removeClass('error').text(resultMsg).show();
                        setModalStatus();
                        setTimeout(function() {
                            $modal.modal("hide");
                            $dtd.resolve();
                        }, 1000);
                    }
                })
                .fail(function() {
                    $modal.find('.result-msg').addClass('error').text('信息保存失败').show();
                })
                .always(function() {
                });
        },
        /**
         * 添加配送员
         * @param  {string} phone 配送员手机号
         * @return 
         */
        searchRider: function(phone) {
            $.ajax({
                    url: '/staypoi/getRiderInfoByPhone',
                    dataType: 'json',
                    data: {
                        'phone': phone,
                        'timePeriod': $modal.find('[name="startTime"]').val().trim() + "-" + $modal.find('[name="endTime"]').val().trim(),
                        'weekDays': Main.getWeekInfo().join(',')
                    }
                })
                .done(function(data) {
                    if (data['resultCode'] == 0) {
                        $modal.find('.error-info-rider').empty().hide();
                        data['data'] && $modal.find(".search-list tbody").append(tmpl_tr({
                            'riders': [data['data']]
                        }));
                    } else {
                        $modal.find('.error-info-rider').text(data['resultMsg']).show();
                    }
                })
                .fail(function() {
                    $modal.find('.error-info-rider').text('添加失败').show();
                })
                .always(function() {
                });
        },
        /**
         * 获取模态页面上的星期信息
         * @return {Array} 星期信息的数组
         */
        getWeekInfo: function() {
            var $chks = $modal.find('input[type="checkbox"]:checked');
            var weekDay = [];
            var weekDayName = $modal.find('.weekDayName').text();
            // 星期
            if ($modal.find('.weekDayName').length) { //编辑的时候，星期是不能修改的
                weekDay.push(getWeekDay("星期" + weekDayName.substr(weekDayName.length - 1, 1)));
            } else { //增加的时候
                $chks.each(function(index, el) {
                    weekDay.push($(el).val());
                });
            }
            return weekDay;
        },
        /**
         * 模态框保存驻店时间
         * @return 
         */
        saveInfo: function() {

        }
    }
    Main.init();
    /**
     * 参数是数字返回星期一-星期日,如果参数是字符串则返回0-6
     * @param  {number} weekDay 星期 0-6 分别表示周一到周日
     * @return {string}         星期一到星期日
     */
    function getWeekDay(weekDay) {
        var weekNames = ['一', '二', '三', '四', '五', '六', '日'];
        if (weekDay != null && typeof weekDay == 'number' && weekDay >= 0 && weekDay <= 6) {
            return "星期" + weekNames[weekDay];
        }
        if (weekDay && typeof weekDay == 'string') {
            for (var i = 0; i < weekNames.length; i++) {
                if (weekDay == "星期" + weekNames[i]) {
                    return i;
                }
            }
            return -1;
        }
        return "";
    }
    /**
     * 将时间区间截取为两个
     * @param  {string} timePeriod 时间区间，如 02:30-05:00
     * @return {Array}            
     */
    function splitTimePeriod(timePeriod) {
        if (timePeriod && typeof timePeriod == 'string') {
            return timePeriod.split("-");
        }
        return ["", ""]
    }
    /**
     * 显示模态框
     * @param  {string} timeId 时间Id,修改的时候才有
     * @return 
     */
    function show(timeId) {
        $dtd = $.Deferred();
        m_timeId = timeId || "";
        // 修改
        if (timeId) {
            // $.when(getData(timeId)).done(function(data) {
            //     var timePeriods = splitTimePeriod(data['timePeriod']);
            //     $modal.find('.modal-content').html(tmpl({
            //         'startTime': timePeriods[0],
            //         'endTime': timePeriods[1],
            //         'weekDayName': getWeekDay(data['weekDay']),
            //         'riders': data['riders'] || []
            //     }));
            //     $modal.modal("show");
            // })
            isModify = true;
            var data = window.G_poiTimeData[m_timeId];
            var timePeriods = splitTimePeriod(data['timePeriod']);
            $modal.find('.modal-content').html(tmpl({
                'startTime': timePeriods[0],
                'endTime': timePeriods[1],
                'weekDayName': getWeekDay(data['weekDay']),
                'riders': data['riders'] || []
            }));
            $modal.modal("show");
        } else { //新增
            $modal.find('.modal-content').html(tmpl({
                'startTime': '',
                'endTime': '',
                'weekDayName': '',
                'riders': []
            }));
            $modal.modal("show");
        }
        return $dtd;
    }
    /**
     * 编辑单个驻店时间调用，查询单个时间段信息
     * @param  {string} timeId 时间Id,修改的时候才有
     * @return 
     */
    function getData(timeId) {
        var dtd = $.Deferred();
        $.ajax({
                url: '/staypoi/getTimeAndRiderInfoById',
                dataType: 'json',
                data: {
                    'timeId': timeId
                }
            })
            .done(function(data) {
                if (data['resultCode'] == 0) {
                    dtd.resolve(data);
                } else {
                    alert(data['resultMsg']);
                }
            })
            .fail(function() {
                alert("驻店时间详细信息获取失败");
            })
            .always(function() {
            });
        return dtd;
    }
    /**
     * 通过modal获取到相应的数据对象
     * @return 
     */
    function setModalStatus() {
        var $chks = $modal.find('input[type="checkbox"]:checked');
        var $trs = $modal.find('.search-list tbody tr');
        var weekDay = [];
        var startTime = $modal.find('[name="startTime"]').val();
        var endTime = $modal.find('[name="endTime"]').val();
        var riders = [];
        var weekDayName = $modal.find('.weekDayName').text();
        // 列表
        $trs.each(function(index, el) {
            var $tds = $(el).find('td');
            riders.push({
                "riderId": $(el).attr('data-riderId'),
                "riderName": $tds.eq(0).text(),
                "riderPhone": $tds.eq(1).text()
            });
        });
        // 星期
        if ($modal.find('.weekDayName').length) {
            m_timeId = m_timeId || ("t_" + new Date().getTime());
            // 设置到全局变量
            window.G_poiTimeData[m_timeId] = {
                'timeId': m_timeId,
                'timePeriod': startTime + "-" + endTime,
                'weekDay': getWeekDay("星期" + weekDayName.substr(weekDayName.length - 1, 1)),
                'riders': riders
            };
        } else {
            $chks.each(function(index, el) {
                m_timeId = m_timeId || ("t_" + new Date().getTime());
                // 设置到全局变量
                window.G_poiTimeData[m_timeId] = {
                    'timeId': m_timeId,
                    'timePeriod': startTime + "-" + endTime,
                    'weekDay': $(el).val(),
                    'riders': riders
                };
            });
        }
    }

    return {
        show: show
    }

});