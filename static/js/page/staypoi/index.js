require.config({
    baseUrl: MT.STATIC_ROOT + '/js',
    urlArgs: 'ver=' + pageVersion,
    shim: {
        'lib/underscore': {
            exports: '_'
        }
    }
});

require(['lib/underscore', 'page/staypoi/tpl', 'module/head-nav', 'module/cookie'], function(_, tpl) {
    var Main = {
        cityList: {},
        cityHtml: "",
        // areaList: {},
        tmpl: null,
        delConfirmTmpl: null,
        $cityOrgWrap: $('.city-org-wrap'),
        $citySel: $('#cityId'),
        $areaSel: $('#areaId'),
        $listBody: $('.search-list tbody'),
        $poiId: $('input[name="poiId"]'),
        $riderName: $('input[name="riderName"]'),
        $loadWrap: $('.loading-wrap'),
        $searchResultInfo: $('.search-list .search-result-info'),
        searchCondition: null,
        txtSearchPoi: "没有在对应众包站点下查找到相关驻店商家",
        txtSearchRider: "没有查询到与该骑手相关的驻店信息",
        init: function() {
            Main.initData();
            Main.initEvent();
        },
        /**
         * 获取相关数据
         * @return 
         */
        initData: function() {
            Main.tmpl = _.template(tpl.poi_tr_tpl);
            Main.delConfirmTmpl = _.template(tpl.del_confrim_tpl);
            // 如果站点已经有选项了，说明只有某些站点可看，所以不需要查找城市和站点信息
            Main.$areaSel.find("option[value='-1']").length ? (Main.$citySel.select2("val", "-1"), Main.getCityAreaInfo(), Main.getOrgInfo("")) : setTimeout(function() {
                $('.btn-search-poi').trigger('click');
            }, 20);
        },
        /**
         * 绑定事件
         * @return 
         */
        initEvent: function() {
            // 隐藏popover
            $('body').on('click', function(e) {
                if ($(e.target).data('toggle') !== 'popover' && $(e.target).parents('[data-toggle="popover"]').length === 0 && $(e.target).parents('.popover.in').length === 0) {
                    $('[data-toggle="popover"]').popover('hide');
                }
            });
            // 删除商家
            Main.$listBody.delegate('.fa-delete-t', 'click', function(event) {
                var $tr = $(this).closest('tr');
                $(Main.delConfirmTmpl({
                        'poiName': $tr.find('td:eq(0)').text()
                    })).modal("show")
                    .delegate('.btn-save', 'click', function(event) {
                        $.when(
                            Main.deletePoi($tr.attr('data-stayPoiId'))
                        ).done(function() {
                            $tr.remove();
                        })
                    })
            });
            // 根据城市获取区域
            Main.$citySel.on("change", function() {
                var cityId = $(this).val();
                Main.getOrgInfo(cityId == '-1' ? '' : cityId);
            });
            // 搜索驻店列表
            $('input[name="poiId"],input[name="riderName"]').on('keyup', function(event) {
                // 回车
                if (event.keyCode == 13) {
                    $(this).siblings('button').click();
                }
            });
            // POI检索
            $('.btn-search-poi').on('click', function(event) {
                Main.searchCondition = {
                    'poiId': $(this).siblings('[name="poiId"]').val().trim()
                };
                Main.getList(null, Main.txtSearchPoi);
            });
            // 骑手检索
            $('.btn-search-rider').on('click', function(event) {
                Main.searchCondition = {
                    'bmUserName': $(this).siblings('[name="riderName"]').val().trim()
                };
                Main.getList(null, Main.txtSearchRider);
            });
            // 新增驻店商家
            $('.btn-add-poi').on('click', function(event) {
                location.href = "/staypoi/modify?cityId=" + Main.getCityIdAreaId()['cityId'] + "&stationId=" + Main.getCityIdAreaId()['stationId'];
            });
            // 编辑驻店商家
            Main.$listBody.delegate('.fa-edit', 'click', function(event) {
                var $tr = $(this).closest('tr');
                location.href = "/staypoi/modify?poiId=" + $tr.attr('data-poiId') + "&cityId=" + Main.getCityIdAreaId()['cityId'] + "&stationId=" + Main.getCityIdAreaId()['stationId'];
            });
        },
        /**
         * 获取城市和站点信息
         * @return 
         */
        getCityAreaInfo: function() {
            //获取全部城市列表
            $.ajax({
                url: "/poi/getCityAreaInfo",
                type: "get",
                dataType: "json",
                success: function(result) {
                    if (result.code == 0 && result.data != null) {
                        var htmlCity = "";
                        var htmlArea = "";
                        for (var key in result.data) {
                            htmlCity += '<option value="' + key + '">' + result.data[key].cityName + '</option>';
                            Main.cityList[key] = result.data[key].cityName;
                            // Main.areaList[key] = result.data[key].areaInfoVoList;
                        }
                        // 如果城市已经存在，则不需要添加城市，
                        // 但仍然需要全局的数据，因为切换城市的时候还得查询站点
                        if (Main.$citySel.find("option[value='-1']").length) {
                            Main.$citySel.append(htmlCity);
                            // setTimeout(function() {
                            //     Main.$citySel.select2("val", "-1");
                            // }, 0);
                        } else {
                            Main.$citySel.trigger('change');
                        }
                    } else {
                        alert("获取城市信息失败");
                    }
                },
                error: function() {
                    alert("获取城市信息失败");
                }
            });
        },
        /**
         * 获取站点信息
         * @param  {string} cityId 城市id
         * @return 
         */
        getOrgInfo: function(cityId) {
            $("#areaId option").remove();
            var _orgList = ['<option value="-1">全部站点</option>'];
            Main.$cityOrgWrap.append('<i class="loadding-icon" style="margin-top:4px;"></i>');
            $.ajax({
                    url: '/staypoi/zbStations',
                    dataType: 'json',
                    data: {
                        'cityId': cityId
                    }
                })
                .done(function(result) {
                    var data;
                    if (result['resultCode'] != 0) {
                        alert(result['resultMsg'])
                    } else {
                        data = result['data'];
                        if (data['data'] && data['data'].length) {
                            data = data['data'];
                            for (var i = 0; i < data.length; i++) {
                                _orgList.push('<option value="' + data[i]['orgId'] + '">' + data[i]['orgName'] + '</option>');
                            }
                            Main.$areaSel.append(_orgList.join('')).select2();
                        }
                        setTimeout(function() {
                            $('.btn-search-poi').trigger('click');
                        }, 10);
                    }
                })
                .fail(function(err) {
                    if (err['status'] == 0) return;
                    alert("获取站点信息失败");
                })
                .always(function() {
                    Main.$cityOrgWrap.find('.loadding-icon').remove();
                });
        },
        /**
         * 删除列表的某个驻店信息
         * @param  {string} poiId 驻店信息ID
         * @return 
         */
        deletePoi: function(poiId) {
            var dtd = $.Deferred();
            $.ajax({
                    url: '/staypoi/deleteStayPoi',
                    dataType: 'json',
                    data: $.extend({
                        'stayPoiId': poiId
                    }, Main.getCityIdAreaId())
                })
                .done(function(data) {
                    if (data['resultCode'] != 0) {
                        alert(data['resultMsg'])
                    } else {
                        dtd.resolve(data);
                    }
                })
                .fail(function() {
                    alert("删除失败");
                })
                .always(function() {});
            return dtd;
        },
        /**
         * 根据下拉框返回响应的城市id和站点id
         * @return {object} {cityId:xx,stationId:xx}
         */
        getCityIdAreaId: function() {
            return {
                'cityId': Main.$citySel.val() == -1 ? "" : Main.$citySel.val(),
                'stationId': Main.$areaSel.val() == -1 ? "" : (Main.$areaSel.val() || "")
            }
        },
        /**
         * 通过 城市、站点、骑手名称、poiId查询驻店poi
         * @param  {object} pageSize,pageNo
         * @return {string} emptyText  为空的文案
         */
        getList: function(option, emptyText) {
            Main.$loadWrap.show();
            var pageSize = 20,
                pageNo = 1;
            if (option) {
                pageSize = option['pageSize'];
                pageNo = option['pageNo'];
            }
            $.ajax({
                    url: '/staypoi/getStayPoi',
                    dataType: 'json',
                    data: $.extend({
                        'pageSize': pageSize,
                        'pageNo': pageNo
                    }, Main.getCityIdAreaId(), Main.searchCondition)
                })
                .done(function(data) {
                    if (data['resultCode'] == 0) {
                        if (data['data'] && data['data'].length) {
                            Main.$searchResultInfo.hide();
                            Main.$listBody.html(Main.tmpl(data));
                            $('[data-toggle="popover"]').popover();
                            Main.setPagination(data['totalCount'], data['pageSize'], data['pageNo']);
                        } else {
                            Main.$listBody.empty();
                            emptyText ? Main.$searchResultInfo.text(emptyText).show() : Main.$searchResultInfo.text('没有在对应众包站点下查找到相关驻店商家').show();
                        }
                    } else {
                        alert(data['resultMsg']);
                    }
                })
                .fail(function() {
                    alert("获取列表信息失败");
                })
                .always(function() {
                    Main.$loadWrap.hide();
                });

        },
        /**
         * 设置分页组件状态
         * @param {number} totalCount 总记录数
         * @param {number} pageSize 每页记录数
         */
        setPagination: function(totalCount, pageSize, pageNo) {
            var total = Math.ceil(totalCount / pageSize);
            total > 0 && $('.pagination').twbsPagination({
                totalPages: total,
                first: "第一页",
                last: "最后一页",
                prev: "上一页",
                next: "下一页",
                startPage: pageNo,
                visiblePages: 7,
                initiateStartPageClick: false,
                onPageClick: function(event, page) {
                    Main.getList({
                        'pageSize': 20,
                        'pageNo': page
                    });
                }
            });
        }
    }

    Main.init();
});