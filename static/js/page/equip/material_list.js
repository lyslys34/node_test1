require.config({
  baseUrl: MT.STATIC_ROOT + '/js',
  urlArgs: 'ver=' + pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});

require(['module/root', 'module/validator', 'module/cookie'], function(t, validator, cookie) {

	var pageSize = 10;
	var pageNo = 1; // 记录全局当前的pageNo

	var tableUrl = '/equip/material/list';

	
	$(document).ready(function () {
		// TODO 尽可能得有生命周期的概念
		init();
		
	});

	function init() {

	};

	var statusList = {
		'1' : '发放中',
		'2' : '草稿',
		'3' : '已下线'
	};

	var operateList = {
		'1' : '<input rel="{bmEquipApplyId}" type="button" value="确认领取" style="background-color: #58B7B1;color:#FFFFFF" class="fetch btn js_submitbtn" />',
		'2' : '操作人: {opUserName}<br/>领取时间:{utime}'
	};

	function updateList(pageNo, mobile, cityId, type) {
		$.post(tableUrl, {
			pageNo: pageNo || 1, 
			pageSize: pageSize,
			mobile : mobile || '',
			cityId : mobile || 0,
			type : type || 1
		}, function(data) {
		  setListAmount(data.data.total);
		  data = data.data.list;

		  var render = '';
		  var listTemplate = $("#listTemplate").html();

		  if (!data.length) {
		    $('#tbody-grant-equip-list').html('');
		    return;
		  }

		  data.forEach(function(item) {

		     var type = item.type;
		     var operateCnt = operateList[type];
		     var isShow = item.status == '3' ? 'none' : '';

		     var config = {
		       id : item.id,
		       name : item.bmUserName,
		       mobile : item.mobile,
		       cardNo : item.cardNo,
		       stationName : item.stationName,
		       equipName : item.equipName,
		       isShow : isShow
		     };
		     var tempTemplate = listTemplate;
		     for (key in config) {
		       var reg = new RegExp('{' + key + '}', 'g');
		       tempTemplate = tempTemplate.replace(reg, config[key]);
		     }
		     render += tempTemplate;
		  });
		  $('#tbody-grant-equip-list').html(render);
		})
	}

	updateList();

	function setListAmount(amount) {
		$('.amount').html('总计：' + amount + '条');
	}

	initPager();
  	// 全局初始化分页数据
  	// TODO 可以根据后台接口的情况进行
	function initPager() {
	$.post(tableUrl)
	  .done(function(data) {
	    var total = data.data.total;
	    setListAmount(total);
	    var totalPages;
	    if (total % pageSize === 0) {
	      totalPages = total / pageSize;
	    } else {
	      totalPages = (total / pageSize) + 1;
	    }
	    $('.pager-wrap .pagination').twbsPagination({
	      totalPages: totalPages,
	      visiblePages: 5,
	      first: '首页',
	      prev: '上一页',
	      next: '下一页',
	      last: '尾页',
	      onPageClick: function(event, page) {
	      	pageNo = page;
	        updateList(page);
	      }
	    });
	  });
	}

});
