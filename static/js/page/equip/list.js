require.config({
  baseUrl: MT.STATIC_ROOT + '/js',
  urlArgs: 'ver=' + pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});

require(['module/root', 'module/validator', 'module/cookie'], function(t, validator, cookie) {

	var pageSize = 10;
	var pageNo = 1; // 记录全局当前的pageNo

	var mockCity = [
	{
		'id' : 0,
		'name': '全部'
	},{
		'id' : 1,
		'name': '北京'
	},{
		'id' : 2,
		'name': '上海'
	}];

	$(document).ready(function () {
		// TODO 尽可能得有生命周期的概念
		init();
		
	});

	function init() {
		initCitys();
	}

	function initCitys() {
		var citysTpl = '';
		mockCity.forEach(function (item) {
			citysTpl += '<option value="' + item.id + '">' + item.name + '</option>';
		});
		$('#cityId').html(citysTpl);
		$('#cityId').select2();
	}

	var operateList = {
		'1' : '<input rel="{bmEquipApplyId}" type="button" value="确认领取" style="background-color: #58B7B1;color:#FFFFFF" class="fetch btn js_submitbtn" />',
		'2' : '操作人: {opUserName}<br/>领取时间:{utime}'
	};

	var form = $("#params-form");
	var in_mobile = $("#params-form input[name=mobile]");
	var in_type = $("#params-form input[name=type]");

	$('#search').click(function(){
		var s_mobile = $('#s_mobile').val();
		var city = $('#cityId').val();
		var type = $('#grantType').val();
		// in_mobile.val(s_mobile);
		// form.submit();

		console.log(s_mobile, city, type);
		updateExamList(pageNo, s_mobile, city, type);

	});


	$("#tbody-grant-equip-list").on('click','.fetch',function(){
		var id = $(this).attr('rel');
		if(!id){
			alert('无效的申请id');
			return false;
		}
		
		$("input#confirm").data("eaid",id);
		$("#showConfirm").modal();
	})

	$('input#confirm').click(function(){
		var id = $(this).data("eaid");
		if(!id){
			alert('无效的申请id');
			return false;
		}
		var postData = {'bmEquipApplyId':id};
		t.utils.post('/partner/equip/fetch',postData,function(r){
    		if (r.httpSuccess) {
    			if(r.data.code == 0){
    				//alert("领取成功");
    				// form.submit();
    				// TODO 重新刷新tableß
    			}else{
    				alert("提交失败:" + r.data.msg);
    			}
    		}else{
    			alert('提交失败');
    		}
        });
	});

	function updateList(pageNo, mobile, cityId, type) {
		$.post('/equip/grant/list', {
			pageNo: pageNo || 1, 
			pageSize: pageSize,
			mobile : mobile || '',
			cityId : mobile || 0,
			type : type || 1
		}, function(data) {
		  setListAmount(data.data.total);
		  data = data.data.list;

		  var render = '';
		  var listTemplate = $("#listTemplate").html();

		  

		  if (!data.length) {
		    $('#tbody-grant-equip-list').html('');
		    return;
		  }

		  data.forEach(function(item) {
		     var cTime = moment(item.ctime * 1000).format('YYYY-MM-DD HH:mm');
		     var uTime = moment(item.utime * 1000).format('YYYY-MM-DD HH:mm');

		     var type = item.type;
		     var operateCnt = operateList[type];
		     var operate = type == 1 ? operateCnt.replace(/{bmEquipApplyId}/g, item.bmEquipApplyId) :
		     	type == 2 ? operateCnt.replace(/{opUserName}/g, item.opUserName).replace(/{utime}/g, uTime) :
		     	'未知状态';

		     var config = {
		       name : item.bmUserName,
		       mobile : item.mobile,
		       cardNo : item.cardNo,
		       stationName : item.stationName,
		       equipName : item.equipName,
		       ctime : cTime,
		       operate : operate
		     };
		     var tempTemplate = listTemplate;
		     for (key in config) {
		       var reg = new RegExp('{' + key + '}', 'g');
		       tempTemplate = tempTemplate.replace(reg, config[key]);
		     }
		     render += tempTemplate;
		  });
		  $('#tbody-grant-equip-list').html(render);
		})
	}

	updateList();

	function setListAmount(amount) {
		$('.amount').html('总计：' + amount + '条');
	}

	initPager();
  	// 全局初始化分页数据
  	// TODO 可以根据后台接口的情况进行
	function initPager() {
	$.post('/equip/grant/list')
	  .done(function(data) {
	    var total = data.data.total;
	    setListAmount(total);
	    var totalPages;
	    if (total % pageSize === 0) {
	      totalPages = total / pageSize;
	    } else {
	      totalPages = (total / pageSize) + 1;
	    }
	    $('.pager-wrap .pagination').twbsPagination({
	      totalPages: totalPages,
	      visiblePages: 5,
	      first: '首页',
	      prev: '上一页',
	      next: '下一页',
	      last: '尾页',
	      onPageClick: function(event, page) {
	      	pageNo = page;
	        updateList(page);
	      }
	    });
	  });
	}

});
