(function() {
	$(document).ready(function() {
		$('#upload-img-btn').on('click', function() {
			$('.js_fileupload').click();
		});
	});

	var uploadOpts = {
		// TODO 提测时记得修改
		url : '/file-upload',
		// url : '/file/imguploadWithWatermark',
		add : function (e, data) {
			var acceptFileTypes = /(\.|\/)(jpe?g|png)$/i;
			var maxFileSize = 5242880; // 5M

			var fileType = data.originalFiles[0]['type'];
			if (!acceptFileTypes.test(fileType)) {
				alert('文件格式不正确');
				return false;
			}

			var fileSize = data.originalFiles[0]['size'];
			if (fileSize > maxFileSize) {
				alert('文件太大（5M）');
			}

			$('#upload-img-btn').html('上传中').prop('disabled', 'disabled');
			data.submit();
		},
		done : function (e, data) {
			$('#upload-img-btn').html('上传').prop('disabled', '');
			console.log(data);

			if (data.result) {
				if (data.result.code == 0) {
					if (data.result.data[0] && data.result.data[0].code == 0) {
						var res = data.result.data[0];
						var backUrl = res.url;
						var backUrlWithWatermark = res.waterMarkUrl;

						if (!backUrl) {
							alert('上传失败，获取图片链接失败');
							return false;
						}
						$('#upload-img-btn').html('上传').prop('disabled', '');

						$('#img-preview')
						.html('')
						.css('background-image', 'url(' + backUrl + ')');
						.css('background-size', '100% 100%');
					} else {
						showError("上传失败," + 
							(data.result.data[0] ? data.result.data[0].msg : "未知错误"));
					}

				} else {
					alert('上传失败，' + data.result.msg);
				}
			} else {
				alert('上传失败');
			}
		},
		fail : function (e, data) {
			$('#upload-img-btn').html('上传').prop('disabled', '');
			console.log(e);
			console.log(data);
		}
	}; // var opts;

	var fileupload = $(".js_fileupload").fileupload(uploadOpts);
})();