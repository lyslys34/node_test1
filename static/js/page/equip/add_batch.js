require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/autocomplete' : {
        deps : ['page/common']
    },
    'lib/bootstrap': {
        deps: ['lib/jquery']
    }
  }
});


require(['module/root', 'module/validator',  'module/cookie', 
		'page/common','lib/datepicker','lib/autocomplete'], 

		function (t, validator,cookie, common,datepicker,autocomplete) {

	var pageSize = 10;
	var pageNo = 1; // 记录全局当前的pageNo

	var mockCity = [
	{
		'id' : 0,
		'name': '全部'
	},{
		'id' : 1,
		'name': '北京'
	},{
		'id' : 2,
		'name': '上海'
	}];
	
	$(document).ready(function () {
		// TODO 尽可能得有生命周期的概念
		init();
		
	});

	function init() {
		initCitys();
	}

	function initCitys() {
		var citysTpl = '';
		mockCity.forEach(function (item) {
			citysTpl += '<option value="' + item.id + '">' + item.name + '</option>';
		});
		$('#cityId').html(citysTpl);
		$('#cityId').select2();
	}


});
