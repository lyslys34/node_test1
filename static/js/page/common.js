/**
 * Created with IntelliJ IDEA.
 * User: xuyc
 * Date: 14-12-22
 * Time: 下午4:47
 * To change this template use File | Settings | File Templates.
 */

define(['module/utils', 'lib/datepicker'], function(utils, datepicker) {

    datepicker.set();

    $('.query-select').each(function(k) {
        var p = $(this);
        p.val(p.data('value'));
    });

    function _empty(obj) { return obj === null || obj === undefined; }

    return {

        getDate: function(node) {
            var value = $(node).datepicker("option", "dateFormat", 'yy-mm-dd').val();
            var date = Date.parse(value);
            return date;
        },

        checkDateSpan: function(start, end, min, max, errMsg) {
            var span = end - start, f;
            if(min !== null && (f = 'min', span < min) || max != null && (f = 'max', span > max))
            {
                if(typeof errMsg == 'function') errMsg(f);
                else if(errMsg) alert(errMsg);
                return false;
            }

            return true;
        },

        checkDateSpan31days: function(start, end) {
            return this.checkDateSpan(start, end, 0, 31 * 24 * 3600 * 1000, function(f) {

                if(f == 'min')
                {
                    alert('开始时间不能大于结束时间');
                }
                else if(f == 'max')
                {
                    alert('所选时间范围不能大于31天');
                }

            });
        },

        getTimeSpan: function(startNode, endNode, partNode) {
            var startDate = this.getDate(startNode), endDate = this.getDate(endNode), part = $(partNode).val();
            if(!this.checkDateSpan31days(startDate, endDate)) return null;

            if(part != 0)
            {
                if(endDate != startDate)
                {
                    alert('查询必须限定在一天之内');
                    return null;
                }

                if(part == 1)
                {
                    endDate = startDate + 14.5 * 3600 * 1000;
                }
                else if(part == 2)
                {
                    startDate += 14.5 * 3600 * 1000;
                    endDate += 24 * 3600 * 1000 - 1;
                }
            }
            else
            {
                endDate += 24 * 3600 * 1000 - 1;
            }

            return { startDate: startDate, endDate: endDate };
        },

        reload: function() {
            var items = [], pairs = { 'poi_id': 'query-pois', 'dispatcher_id': 'query-dispatchers',
                'dispatch_type': 'query-dispatch-type', 'time_part': 'query-time-part' };

            var span = this.getTimeSpan('#query-start-date', '#query-end-date', '#query-time-part');
            if(!span) return;

            items.push('start_date=' + span.startDate);
            items.push('end_date=' + span.endDate);

            for(var name in pairs)
            {
                var value = $('#' + pairs[name]).val();
                if(_empty(value))
                {
                    value = utils.urlArg(name);
                }

                !_empty(value) && (items.push(encodeURI(name) + '=' + encodeURI(value)));
            }

            document.location.href = '?' + items.join('&');
        },

        load: function(url, args) {
            var names = [ 'start_date', 'end_date', 'time_part', 'poi_id', 'dispatcher_id', 'dispatch_type' ], items = [];
            $(names).each(function(k, name) {
                var value = args[name];
                if(_empty(value)) value = utils.urlArg(name);
                if(!_empty(value)) items.push(encodeURI(name) + '=' + encodeURI(value));
            });

            document.location.href = url + '?' + items.join('&');
        }
    };

});