require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});
require(['module/root', 'module/validator','module/cookie'], function (t, validator,cookie) {

    $(document).ready(function() {
        var request_url = '';
        addBreadcrumb();
        getCityList();
        del_city = function (e) {
            e.parent().parent().remove();
        }
        var dynamic_ids=new Array(
            "prop-id",
            "prop-name",
            "prop-description",
            "rule-pick-distance-reject-max",
            "rule-pick-distance-check-max",
            "rule-send-distance-reject-max",
            "rule-send-distance-check-max",
            "rule-delivery-time-reject-max",
            "rule-delivery-time-check-max",
            "rule-delivery-time-reject-min",
            "rule-delivery-time-check-min",
            "rule-book-time-reject-max",
            "rule-book-time-check-max"
            );
        $('#modal-prop-name').blur(function(){
            if($(this).val() == ''){
                $('#modal-prop-name').focus();
                $(this).addClass('border-red');
                $(this).nextAll(".error-msg").remove();
                $(this).parent().append('<span class="error-msg"><a data-toggle="tooltip" title="名称不能为空" data-placement="top" href="javascript:;" name="delete"><i class="fa fa-exclamation-circle fa-lg opration-icon"></i><span>');
            }else{
                $(this).removeClass("border-red");
                $(this).nextAll(".error-msg").remove();
            }
        });
        $('.rule-checkbox').change(function(){
            if($(this).prop('checked') == true){
                rule_input = $(this).nextAll('input');
                rule_input.removeAttr('disabled');
                data = rule_input.val();
                if(data == '' || parseInt(data) < 0){
                    rule_input.addClass("border-red");
                    rule_input.nextAll(".error-msg").remove();
                    rule_input.parent().append('<span class="error-msg"><a data-toggle="tooltip" title="不能为空且不能小于0" data-placement="top" href="javascript:;" name="delete"><i class="fa fa-exclamation-circle fa-lg opration-icon"></i><span>');
                }
            }else{
                rule_input = $(this).nextAll('input');
                rule_input.attr('disabled','disabled');
                rule_input.removeClass('border-red');
                $(this).nextAll(".error-msg").remove();
            }
        });
        dynamic_ids.forEach(function (id) {
            if(id.startsWith('rule')){
                $("#modal-"+id).blur(function(){
                    if($(this).prevAll('input').prop('checked')){
                        data = $(this).val();
                        data = data.replace(/\D/g,'');
                        $(this).val(data);
                        if(data == ''|| parseInt(data) < 0){
                            $(this).addClass("border-red");
                            $(this).nextAll(".error-msg").remove();
                            $(this).parent().append('<span class="error-msg"><a data-toggle="tooltip" title="不能为空且不能小于0" data-placement="top" href="javascript:;" name="delete"><i class="fa fa-exclamation-circle fa-lg opration-icon"></i><span>');
                        }else{
                            $(this).removeClass("border-red");
                            $(this).nextAll(".error-msg").remove();
                        }
                    }
                });
            }

        });

        $('.del-rule').click(function () {
            var rule_id = $(this).parent().parent().find('.prop-id').text();
            $.getJSON('/settleWaybillAudit/rule/delete', {id: rule_id}, function(json, textStatus) {
                    if(json.code == "0"){
                        window.location.href = '/settleWaybillAudit/rule/list?waybillType=1';
                    }else{
                        alert("删除规则失败");
                    }

            });
        });

        $('.edit-link').click(function() {
            $('.err-msg').remove();
            $('#city-panel').empty();
            $('.city-panel-all').css('display','block');
            var th=$(this);
            if (th.attr('id') == 'add-rule') {
                $('.modal-title').text("创建美团订单审核规则");
                $("input:checked").each(function () {
                    $(this).prop('checked',false);
                });
                dynamic_ids.forEach(function (id) {
                    $('#modal-'+id).val('');
                    if(id.length > 20){
                        $('#modal-'+id).attr("disabled","disabled");
                    }
                });
                request_url = '/settleWaybillAudit/rule/doCreate';
                return;
            };
            $('.modal-title').text("修改美团订单审核规则");
            request_url = '/settleWaybillAudit/rule/doUpdate';
            if(th.parent().parent().find('.default-rule').text() == 'true'){
                $('.city-panel-all').css('display','none');
            }
            dynamic_ids.forEach(function (id) {
                $('#modal-'+id).removeClass("border-red");
                $('#modal-'+id).val('');
                var value = th.parent().parent().find('.'+id).text();
                if (value == -1) {
                    value = '';
                };
                $('#modal-'+id).val(value);
                if ($('#modal-'+id).val() == '-1' || $('#modal-'+id).val() == '') {
                    $('#modal-'+id).prevAll('input').prop('checked', false);
                    if(id.startsWith('rule')){
                        $('#modal-'+id).attr('disabled','disabled');
                    }
                }else{
                    $('#modal-'+id).prevAll('input').prop('checked', true);
                    if(id.startsWith('rule')){
                        $('#modal-'+id).removeAttr('disabled');
                    }
                };
            });
            var citys = JSON.parse(th.parent().parent().find('.city-info').text().replace(',]',']'));
            citys.forEach(function (city) {
                $('#city-panel').append('<tr class="city-row"><td class="city-name">'+city[0]+'</td> <td class="city-id">'+city[1]+'</td> <td class="city-operation-log">'+city[2]+'&nbsp&nbsp'+city[3]+'</td> <td><a data-toggle="tooltip" class="del-city" title="删除" href="javascript:void(0)" onclick="del_city($(this));" data-placement="top"><i class="fa fa-trash-o fa-lg opration-icon"></i></a></td></tr>');
            });

        });

        $('#update-btn').click(function () {
            $('.err-msg').remove();
            if ($('#modal-prop-name').val() == '') {
                $('#modal-prop-name').focus();
                alert('名称不能为空');
                return;
            };
            //other-info
            var update_data = new Array();
            valid = true;
            dynamic_ids.forEach(function (id) {
                $('#modal-'+id).removeClass('border-red');
                data = $('#modal-'+id).val();
                if($('#modal-'+id).prevAll('input').prop('checked') == false){
                    data = -1;
                }else{
                    if(id.startsWith('rule') && (data == '' || parseInt(data)< 0)){
                        valid = false;
                        $('#modal-'+id).addClass("border-red");
                        $('#modal-'+id).parent().children(".error-msg").remove();
                        $('#modal-'+id).parent().append('<span class="error-msg"><a data-toggle="tooltip" title="不能为空且不能小于0" data-placement="top" href="javascript:;" name="delete"><i class="fa fa-exclamation-circle fa-lg opration-icon"></i><span>');
                    }
                };
                update_data.push(data);
            });
            if(!valid){
                alert("请检查输入数据!");
                return;
            }
            //city-info
            var city_names = new Array();
            var city_ids = new Array();
            $(".city-row").each(function () {
                city_names.push($(this).children('.city-name').text());
                city_ids.push($(this).children('.city-id').text());
            });

            $(this).attr('disabled', 'disabled');
            $.ajax({
                url: request_url,
                type: 'POST',
                dataType: 'json',
                data: { id: update_data[0],
                        name: update_data[1],
                        description: update_data[2],
                        pickDistanceRejectMax: update_data[3],
                        pickDistanceCheckMax: update_data[4],
                        sendDistanceRejectMax: update_data[5],
                        sendDistanceCheckMax: update_data[6],
                        deliveryTimeRejectMax: update_data[7],
                        deliveryTimeCheckMax: update_data[8],
                        deliveryTimeRejectMin: update_data[9],
                        deliveryTimeCheckMin: update_data[10],
                        bookTimeRejectMax: update_data[11],
                        bookTimeCheckMax: update_data[12],
                        waybillType:1,
                        city_ids: '['+city_ids+']',
                        city_names: JSON.stringify(city_names)
                      },
            })
            .done(function() {
                $('#update-btn').removeAttr('disabled');
            })
            .success(function(result){
                if(result.code == "0"){
                    alert(result.msg);
                    window.location.href = '/settleWaybillAudit/rule/list?waybillType=1';
                }else{
                    if(result.code == 2){
                        len = result.msg.length;
                        err_msgs = result.msg.substring(1,len-1).split(',');
                        err_msgs.forEach(function(err_msg){
                            err_msg_info = err_msg.trim().split('=');
                            if(err_msg_info[0].startsWith('GPS')){
                                $('.GPS').append('<span class="err-msg" style="color:red;">'+err_msg_info[1]+'<br/></span>');
                            }else if(err_msg_info[0].startsWith('time')){
                                $('.time').append('<span class="err-msg" style="color:red;">'+err_msg_info[1]+'<br/></span>')
                            }
                        });
                        result.data.forEach(function(param){
                            $('#modal-rule-'+param).addClass('border-red');
                        });
                        alert("参数检验不合格!");
                    }else{
                        alert(result.msg);
                    }

                }
            })
            .fail(function() {
                $('#update-btn').removeAttr('disabled');
                alert("请求错误，请稍后再试");
            })
            .always(function() {
                $('#update-btn').removeAttr('disabled');
            });
            
        });
        
        $('#add-city').click(function () {
            city_info = $('#citys').val().split(',');
            if(city_info.length < 2){
                return;
            }
            city_valid = true;
            $(".city-id").each(function () {
                if($(this).text() == city_info[1]){
                    alert(city_info[0]+"已存在!");
                    city_valid = false;
                    return;
                }
            });
            if(city_valid){
                $('#city-panel').append('<tr class="city-row"><td class="city-name">'+city_info[0]+'</td> <td class="city-id">'+city_info[1]+'</td> <td class="city-operation-log"></td> <td><a data-toggle="tooltip" class="del-city" title="删除" href="javascript:void(0)" onclick="del_city($(this));" data-placement="top"><i class="fa fa-trash-o fa-lg opration-icon"></i></a></td></tr>');
            }

        });
    
        function addBreadcrumb(){
            $('.breadcrumb').append('<li style="color:#757986;font-size:14px;">角马管理<span class="divider"></span></li><li style="color:#757986;font-size:14px;">美团订单审核规则管理<span class="active"></span></li>');
        }

        function getCityList () {
            initPost('/org/citySearchList', {}, function (data) {
                for(var i in data.data.data){
                    var city = data.data.data[i].value; 
                    var id = data.data.data[i].id;
                    $('#citys').append('<option value="'+city+','+id+'" >'+city+'</option>');
                }
            });
        }

        function initPost(url, data, callback){
            if(typeof(data) == 'function') { callback = data; data = null; }
            $.ajax({ type: 'POST', url: url, data:data, success:function(r) {

                callback({ httpSuccess: true, data: r });

            }, error: function(XmlHttpRequest, textStatus, errorThrown) {

                callback({ httpSuccess: false, statusCode:XmlHttpRequest.status });

            } });
        }

    });

});

