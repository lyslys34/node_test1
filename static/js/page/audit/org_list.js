require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu'
        ],
        'lib/autocomplete' : {
            deps : ['page/common']
        },
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/root', 'module/validator',  'module/cookie', 'lib/autocomplete'], function (t, validator, cookie, autocomplete){
	
	var orgNameMap = {};
	var orgIdMap = {};
	var cityNameMap = {};
	var cityIdMap = {};
	
	$(document).ready(function(){
		initSearchOrg();
		initSearchBtn();
	});
	
	function initSearchBtn() {
		$("#search").click(function() {
	        var orgId = $("#orgId").val().trim();
	        var orgName = $("#orgName").val().trim();
	        if(orgId == 0 && orgName != "") {
	          showError("请填写正确的组织名称");
	          return false;
	        }
	        if(orgId == "") {
	          showError("请填写组织名称");
	          return false;
	        }
	        
	        var cityId = $("#cityId").val().trim();
	        var cityName = $("#orgCity").val().trim();
	        if(cityId == 0 && cityName != "") {
	          showError("请填写正确的组织名称");
	          return false;
	        }
	        if(cityId == "") {
	          showError("请填写组织名称");
	          return false;
	        }

			if ($.trim($("#orgCity").val()) != "" &&  cityId == -1) {
				$("#cityId").val(-2);
			} else if ($.trim($("#orgCity").val()) == "") {
				$("#cityId").val(-1);
			}

		    $("#formValidate1").submit();
		});
	}
	
	function initSearchOrg(){
		myPOST('/auditOrg/searchList', {}, function (r) {
	         if (r.httpSuccess) {
	            if(r.data && r.data.code == 0) {
	                $(".js_org_name").autocomplete({
	                    delay:100,
	                    source : r.data.data,
	                    focus: function( event, ui ) {
	                        $( ".js_org_name" ).val( ui.item.value );
	                        $( ".js_org_id" ).val( ui.item.id );
	                        return false;
	                    },
	                    select: function(event, ui) {

	                        $(".js_org_name").val(ui.item.value );
	                        $(".js_org_id" ).val( ui.item.id );
	                    },
	                    change: function(event, ui) {
	                        var orgName = $(".js_org_name").val();
	                        if(validator.isBlank(orgName)) {
	                            $(".js_org_id").val(0);
	                        } 
	                    },
	                    open: function(event, ui) {
	                        $(".js_org_id").val('0');
	                    }

	                }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	                    return $( "<li>" )
	                        .append( "<a>" + item.value + "</a>" )
	                        .appendTo( ul );
	                };
	                if(r.data.data) {
	                    var orgId = $(".js_org_id").val();
	                    $.each(r.data.data, function(index, item) {
	                        orgNameMap[item.value] = item;
	                        orgIdMap[item.id] = item;
	                        if(orgId) {
	                          if(orgId == item.id) {
	                            $(".js_org_name").val(item.value);
	                          }
	                        }
	                    });
	                }

	                var riderId = $(".js_org_id").val();
	                $(".js_org_name").val(orgIdMap[riderId] != null && typeof orgIdMap[riderId] != 'undefined' ? orgIdMap[riderId].value : "");

	            } else {
	                alert("获取组织信息失败，" + r.data.msg);
	            }
	        }
	    });
	}

	function initSearchCity() {

		myPOST('/org/citySearchList', {}, function (r) {
			if (r.httpSuccess) {
				if (r.data && r.data.code == 0) {
					$(".js_city_name").autocomplete({
						delay: 100,
						source: r.data.data,
						focus: function (event, ui) {
							$(".js_city_name").val(ui.item.value);
							$(".js_city_id").val(ui.item.id);
							return false;
						},
						select: function (event, ui) {

							$(".js_city_name").val(ui.item.value);
							$(".js_city_id").val(ui.item.id);
						},
						change: function (event, ui) {
							var cityName = $(".js_city_name").val();
							if (validator.isBlank(cityName)) {
								$(".js_city_id").val(-1);
							}
						},
						open: function (event, ui) {
							$(".js_city_id").val('-1');
						}

					}).data("ui-autocomplete")._renderItem = function (ul, item) {
						return $("<li>")
							.append("<a>" + item.value + "</a>")
							.appendTo(ul);
					};
					if (r.data.data) {
						var cityId = $(".js_city_id").val();
						$.each(r.data.data, function (index, item) {
							cityNameMap[item.value] = item;
							cityIdMap[item.id] = item;
							if (cityId) {
								if (cityId == item.id) {
									$(".js_city_name").val(item.value);
								}
							}
						});

					}

					var riderId = $(".js_city_id").val();
					$(".js_city_name").val(cityIdMap[riderId] != null && typeof cityIdMap[riderId] != 'undefined' ? cityIdMap[riderId].value : "");
				}
			}
		});
	}
	function myPOST(url, data, callback)  // 发送POST请求
    {
        if(typeof(data) == 'function') { callback = data; data = null; }
        $.ajax({ type: 'POST', url: url, data:data, success:function(r) {

            callback({ httpSuccess: true, data: r });

        }, error: function(XmlHttpRequest, textStatus, errorThrown) {

            callback({ httpSuccess: false, statusCode:XmlHttpRequest.status });

        } });
    }


    function isBlank(value) {
	if(typeof String.prototype.trim !== 'function') {
	    String.prototype.trim = function() {
	        return this.replace(/^\s+|\s+$/g, '');
	    }
	}
	return !_valueExist(value) || value.trim() == "";
	}

	function showError(errMsg) {
	$("#alert_error").empty();
	$("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
	}

});