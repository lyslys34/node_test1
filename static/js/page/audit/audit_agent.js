require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root', 'module/validator',  'module/cookie', 'page/common','lib/datepicker'], function (t, validator,cookie, common,datepicker) {
    var radio=$("input[name='timeFlag']:checked").val();
    $(document).delegate(".dateType", "click", function() {
            radio=$("input[name='timeFlag']:checked").val();
            radioChange(radio);
        });
    $(document).delegate(".js_submitbtn", "click", function() {
            var url=$(this).attr("actionurl");
            submit(url);
        })

    var detailDia;
    $(document).delegate('.waybillid', 'click', function(event) {
        
        showWaybillDetail($(this).attr('value'));
    });

    $(function(){
        if(radio!='3'){
            $(".J-datepicker").datepicker("disable");
        }
    });
    function radioChange(radio) {
        if(radio == '3') {
            $(".J-datepicker").datepicker("enable");
        } else {
            $(".J-datepicker").datepicker("disable");
        }
    }

    function validate() {
        var startTimeStr = $(".js_date_start").val();
        var endTimeStr = $(".js_date_end").val();
        var startTime = new Date(Date.parse(startTimeStr.replace(/-/g,   "/")));
        var endTime = new Date(Date.parse(endTimeStr.replace(/-/g,   "/")));

        if(startTime > endTime) {
            alert("开始时间不能晚于结束时间");
            return false;
        }

        return true;
    }
    function submit(url){
        radio=$("input[name='timeFlag']:checked").val();
        if( radio!='3' || (radio == '3' && validate()) ){   //时间区间自定义的时候才进行时间的验证
            $("#formValidate1").attr("action",url);
            $("#formValidate1").submit();
        }
    }
    function showWaybillDetail(waybillId)
    {
        if (detailDia) {
            detailDia.remove();
            detailDia = null;
        }; 
        var body = '<div id="detailCon">这是ID＝' + waybillId+ '的订单详情</div>';
        $.ajax({
             url: "/partner/waybill/detailV2.ajax?waybillId="+ waybillId, 
             success: function(result){

                body = '<div>';
                body = body + result + '</div>';
                detailDia = t.ui.showModalDialog({
                    title: '订单详情', body: body, buttons: [],
                    style:{
                        ".modal-footer":{"display":"none"},
                        ".modal-title":{"display":"none"},
                        ".modal-dialog":{"width":"900px"},
                        ".modal-body":{"padding-top":"0"},
                        ".modal-header .close":{"margin-right": "-8px","margin-top": "-10px"},
                        ".modal-header":{"border-bottom": "none"}
                        }
                });
                detailDia.on("hidden.bs.modal", function(){
                    this.remove();
                    detailDia = null;
                });
            },
            error:function(){
                alert('访问出错');
            }});
    }
});