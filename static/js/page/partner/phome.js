require.config({
    baseUrl: MT.STATIC_ROOT + '/js',
    urlArgs: 'ver=' + pageVersion,
});
require(['module/root', 'lib/moment-with-locales.min', 'lib/weather'], function(r, moment) {


    var WeatherMoodule = {
        loadData: function(orgId) {
            if (!orgId) {
                return;
            };
            WeatherMoodule.orgId = orgId;
            if (!WeatherMoodule.inited) {
                WeatherMoodule.inited = true;
                WeatherMoodule.startLoop();
            } else {
                WeatherMoodule.loadFromServer();
            }
        },
        startLoop: function() {
            WeatherMoodule.loadFromServer()
            setInterval(WeatherMoodule.loadFromServer, 10 * 60 * 1000);
        },
        loadFromServer: function() {
            $("#weather").Weather(WeatherMoodule.orgId);
        }
    }

    var DispatchModule = {

        loadData: function(orgId) {
            if (!orgId) {
                return;
            };

            DispatchModule.orgId = orgId;
            if (!DispatchModule.inited) {
                DispatchModule.inited = true;
                DispatchModule.bindEvents();
                DispatchModule.startLoop();
            } else {
                DispatchModule.loadFromServer();
            }
        },
        bindEvents: function() {
            $('.home_count_span').click(function(event) {

                var url = 'http://dispatch.peisong.meituan.com/partner/dispatch/home';
                switch (this.id) {
                    case 'dis_manul_count':
                        url += '#-10';
                        break;
                    case 'dis_new_count':
                        url += '#10';
                        break;
                    case 'dis_notcom_count':
                        url += '#100';
                        break;
                    case 'dis_rider_counts':
                        url += '#10';
                        break;
                }
                window.open(url, '_top');
            });
        },
        startLoop: function() {
            DispatchModule.loadFromServer()
            setInterval(DispatchModule.loadFromServer, 60 * 1000);
        },

        loadFromServer: function() {
            $.ajax({
                    url: '/partner/getHomeData',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        curOrgId: DispatchModule.orgId,
                        modules: "realtimeMonitor"
                    },
                })
                .done(function(result) {
                    if (result['code'] == undefined) {
                        console.log('result format error');
                        return;
                    };
                    if (result.code == 0) {
                        DispatchModule.fillFeilds(result.data.realtimeMonitor);
                    } else {
                        //todo
                    }
                })
                .fail(function() {})
                .always(function() {});
        },
        fillFeilds: function(data) {
            if (!data.data) {
                return;
            };
            data = data.data;
            $('#dis_manul_count').text(data.manualCount);
            $('#dis_manul_count').removeClass('home_orange_back');
            if (data.manualCount != 0) {
                $('#dis_manul_count').addClass('home_orange_back');
            };
            $('#dis_new_count').text(data.newCount);
            $('#dis_notcom_count').text(data.notCompleted);
            $('#dis_rider_counts').text(data.idle + ' / ' + data.rest + ' / ' + data.busy);
            if (data.redirect == 1) {
                $('#dispatch_module .home_panel_link').removeClass('hidden');
            } else {
                $('#dispatch_module .home_panel_link').addClass('hidden');
            }
        },
    }

    var DataModule = {
        loadData: function(orgId) {
            if (!orgId) {
                return;
            };
            DataModule.orgId = orgId;
            if (!DataModule.inited) {

                DataModule.inited = true;
            }
            DataModule.retry = 0;
            DataModule.loadFromServer();
        },
        loadFromServer: function() {
            $.ajax({
                    url: '/partner/getHomeData',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        curOrgId: DataModule.orgId,
                        modules: "bizProfile"
                    },
                })
                .done(function(result) {
                    if (result['code'] == undefined) {
                        console.log('bizProfile result format error');
                        return;
                    };
                    if (result.code == 0) {
                        // $("#home_rider_count").text(result.riderCount);
                        // $("#home_poi_count").text(result.poiCount);
                        DataModule.fillFeilds(result.data.bizProfile);
                    } else {
                        //todo
                    }
                })
                .fail(function() {
                    if (DataModule.retry < 1) {
                        DataModule.loadFromServer();
                        DataModule.retry = 1;
                    };
                })
                .always(function() {});
        },
        fillFeilds: function(data) {
            if (!data.data) {
                return;
            };
            data = data.data;
            DataModule.fillDataRates('riderEfficiency', data);
            DataModule.fillDataRates('completeWaybill', data);
            DataModule.fillDataRates('incidentRatio', data);
            DataModule.fillDataRates('completeRatio', data);

            if (data.redirect == 1) {
                $('#data_module .home_panel_link').removeClass('hidden');
                $('#data_module .home_panel_link').prop('href', '/data/monitor/key1');
            } else if (data.redirect == 2) {
                $('#data_module .home_panel_link').removeClass('hidden');
                $('#data_module .home_panel_link').prop('href', '/data/monitor/key2');
            } else {
                $('#data_module .home_panel_link').addClass('hidden');
            }
        },
        fillDataRates: function(name, data) {
            var ob = data[name];
            if (ob == undefined) {
                return;
            };
            if (name == 'incidentRatio' || name == 'completeRatio') {
                $('#' + name + ' .home_data_count').text(ob.yesterday + '%');
            } else {
                $('#' + name + ' .home_data_count').text(ob.yesterday);
            }
            $('#' + name + ' .home_rate_num span').text(Math.abs(ob.compare) + '%');
            if (ob.compare > 0) {
                if (name == 'incidentRatio') {
                    $('#' + name + ' .home_rate_num img').prop('src', '/static/imgs/orange-up.png');
                } else {
                    $('#' + name + ' .home_rate_num img').prop('src', '/static/imgs/green-up.png');
                }
                $('#' + name + ' .home_rate_num img').removeClass('hidden');
            } else if (ob.compare < 0) {
                if (name == 'incidentRatio') {
                    $('#' + name + ' .home_rate_num img').prop('src', '/static/imgs/green-down.png');
                } else {
                    $('#' + name + ' .home_rate_num img').prop('src', '/static/imgs/orange-down.png');
                }
                $('#' + name + ' .home_rate_num img').removeClass('hidden');
            } else {
                $('#' + name + ' .home_rate_num span').text('-');
                $('#' + name + ' .home_rate_num img').addClass('hidden');
            }
        },
    }

    var AbnormalModule = {

        loadData: function(orgId) {
            if (!orgId) {
                return;
            };

            AbnormalModule.orgId = orgId;
            if (!AbnormalModule.inited) {
                AbnormalModule.inited = true;
                AbnormalModule.bindEvents();
                AbnormalModule.startLoop();
            } else {
                AbnormalModule.loadFromServer();
            }
        },
        bindEvents: function() {
            var now = moment();
            dateNow = now.format('YYYY-MM-DD');

            var pre = now.subtract(30, 'days');
            datePre = pre.format('YYYY-MM-DD');

            $('#abnormal_waybill a').prop('href', '/partner/pWaybillAudit/list?status=10&timeFlag=3&beginTimeStr=' + datePre + '&endTimeStr=' + dateNow);
        },
        startLoop: function() {
            AbnormalModule.loadFromServer()
            setInterval(AbnormalModule.loadFromServer, 30 * 60 * 1000);

        },
        loadFromServer: function() {
            $.ajax({
                    url: '/partner/getHomeData',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        curOrgId: AbnormalModule.orgId,
                        modules: "exceptionalWaybill,exam"
                    },
                })
                .done(function(result) {
                    if (result['code'] == undefined) {
                        console.log('result format error');
                        return;
                    };
                    if (result.code == 0) {
                        AbnormalModule.fillFeilds(result.data.exceptionalWaybill);
                        if (result.data.exceptionalWaybill.code == -1) {
                            $('#abnormal_waybill').addClass('hidden');
                        } else {
                            $('#abnormal_waybill').removeClass('hidden');
                        }
                        AbnormalModule.fillExam(result.data.exam);
                    }
                })
                .fail(function() {})
                .always(function() {});
        },
        fillFeilds: function(data) {
            if (!data || !data.data) {
                return;
            };
            data = data.data;
            $('#abnormal_waybill .abnormal_count').text(data.count);
            if (data.count > 0) {
                $('#abnormal_waybill .abnormal_count').addClass('red_color');
            } else {
                $('#abnormal_waybill .abnormal_count').removeClass('red_color');
            }
        },
        fillExam: function(data){

            var margintop = 'style="margin-top:10px;"';
            if ($('#abnormal_waybill').hasClass('hidden')) {
                margintop = '';
            };
            $('#exam').remove();
            if (data && data.code == 0 && data.data && data.data.count > 0) {
                var edata = data.data;
                var body = '<div id="exam"' + margintop + '>'
                + '<span>有<span class="abnormal_count red_color">' + edata.count + '</span>个考试</span>'
                + '<a class="pull-right" href="/train/examinee">考试&nbsp;&gt;</a>'
                + '</div>'
                $('#abnormal_waybill').after(body);
            }
        },
    }

    var NotificationModule = {

        loadData: function(orgId) {
            if (!orgId) {
                return;
            };

            NotificationModule.orgId = orgId;
            if (!NotificationModule.inited) {
                NotificationModule.inited = true;
                NotificationModule.bindEvents();
                NotificationModule.startLoop();
            } else {
                NotificationModule.loadFromServer();
            }
        },
        startLoop: function() {
            NotificationModule.loadFromServer()
            setInterval(NotificationModule.loadFromServer, 30 * 60 * 1000);
        },
        bindEvents: function() {
            $('#notification').on('click', '.noti_row', function(event) {
                event.preventDefault();
                var url = $(this).attr('value');
                window.open(url, '_blank');

                var not_read = $('.noti_notread', $(this));

                if (not_read.length > 0 && !not_read.hasClass('hidden')) {

                    $('.noti_notread', $(this)).addClass('hidden');
                    var count = $('#notification .abnormal_count').text();
                    count = count - 1;
                    count = count >= 0 ? count : 0;
                    $('#notification .abnormal_count').text(count);
                    if (count > 0) {
                        $('#notification .abnormal_count').addClass('red_color');
                    } else {
                        $('#notification .abnormal_count').removeClass('red_color');
                    }
                };
            });
        },
        loadFromServer: function() {
            $.ajax({
                    url: '/partner/getHomeData',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        curOrgId: NotificationModule.orgId,
                        modules: "notification"
                    },
                })
                .done(function(result) {
                    if (result['code'] == undefined) {
                        console.log('result format error');
                        return;
                    };
                    if (result.code == 0) {
                        NotificationModule.fillFeilds(result.data.notification);
                    } else {
                        //todo
                    }
                })
                .fail(function() {})
                .always(function() {});
        },
        fillFeilds: function(data) {
            if (!data || !data.data) {
                return;
            };
            data = data.data;

            $('#notification .abnormal_count').text(data.unreadCount);
            if (data.unreadCount > 0) {
                $('#notification .abnormal_count').addClass('red_color');
            } else {
                $('#notification .abnormal_count').removeClass('red_color');
            }
            var rows = '';
            $('#notification .noti_row').remove();
            if (data['notices']) {
                for (var i = 0; i < data.notices.length && i < 2; i++) {
                    var noti = data.notices[i];
                    rows += '<div class="noti_row" value="' + noti.url + '">';
                    rows += '<p>' + noti.title + '</p>';
                    rows += '<span>' + noti.time + '</span>';
                    if (noti.read != 1) {
                        rows += '<span class="pull-right noti_notread" style="color:red">' + '未读</span>';
                    }

                    rows += '</div>';
                };
            };
            if (rows != '') {
                $('#notification').append(rows);
            };
        },
    }

    var AuditMsgModule = {
        loadData: function(orgId) {
            if (!orgId) {
                return;
            };

            AuditMsgModule.orgId = orgId;
            if (!AuditMsgModule.inited) {
                AuditMsgModule.inited = true;
                AuditMsgModule.bindEvents();
                AuditMsgModule.startLoop();
            } else {
                AuditMsgModule.loadFromServer();
            }
        },
        startLoop: function() {
            AuditMsgModule.loadFromServer()
            setInterval(AuditMsgModule.loadFromServer, 30 * 60 * 1000);
        },

        loadFromServer: function() {
            $.ajax({
                    url: '/partner/getHomeData',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        curOrgId: AuditMsgModule.orgId,
                        modules: "auditMsg"
                    },
                })
                .done(function(result) {
                    if (result['code'] == undefined) {
                        console.log('result format error');
                        return;
                    };
                    if (result.code == 0) {
                        AuditMsgModule.fillFeilds(result.data.auditMsg);
                    } else {
                        //todo
                    }
                })
                .fail(function() {})
                .always(function() {});
        },
        fillFeilds: function(data) {
            if (data.code == 0) {
                $('#auditMsg').removeClass('hidden');
                $('.message-container').remove();
                AuditMsgModule.auditMsg = {};
                AuditMsgModule.fillMsg(data.data, 'agentPayment');
                AuditMsgModule.fillMsg(data.data, 'riderPayment');
                AuditMsgModule.fillMsg(data.data, 'exceptionalWaybill');
                AuditMsgModule.fillMsg(data.data, 'poiBindOrOnline');
                AuditMsgModule.fillMsg(data.data, 'poiUnbindOrOffline');
                AuditMsgModule.fillMsg(data.data, 'headPortraitAudit');
                AuditMsgModule.fillMsg(data.data, 'userAudit');
            }else if(data.code == -2){
                $('#auditMsg').addClass('hidden');
            }
        },
        fillMsg: function(dataPara, name) {
            data = dataPara[name];
            if (data.code == 0) {
                AuditMsgModule.auditMsg[name] = data.notices;

                var body = '<div class="message-container" name="' + name + '">';

                body = body + '<div class="message-header">'
                body = body + '<span class="msg-title">' + data.title + '</span>';
                if (data.unreadCount > 0) {
                    body = body + '<span class="msg-count">未读' + '<span class="msg-unread red_color">' + data.unreadCount + '</span>' + '/' + data.count + '&nbsp;&nbsp;<i class="fa fa-angle-right"></i></span>';
                } else {
                    body = body + '<span class="msg-count normal-color">未读' + '<span class="msg-unread">' + data.unreadCount + '</span>' + '/' + data.count + '&nbsp;&nbsp;<i class="fa fa-angle-right"></i></span>';
                }
                body = body + '</div>';
                body = body + '</div>';
                $('#auditMsg').append(body);
            };
        },
        setMsgRead: function(msgId) {
            $.ajax({
                    url: '/partner/setMsgRead',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        idList: msgId
                    },
                })
                .done(function() {})
                .fail(function() {})
                .always(function() {});
        },
        setLocalRead: function(name, msgId) {

            if (AuditMsgModule.auditMsg[name]) {

                var notices = AuditMsgModule.auditMsg[name];
                for (var i = notices.length - 1; i >= 0; i--) {
                    if (notices[i].msgId == msgId) {
                        notices[i].read = 1;
                        break;
                    };
                };
            };
        },
        showMsgList: function(container) {

            $('.message-list', container).remove();
            var name = container.attr('name');
            var notices = AuditMsgModule.auditMsg[name];

            var sortedNotices = [];
            var readNotices = [];
            for (var i = 0; i < notices.length; i++) {
                if (notices[i].read != 1) {
                    sortedNotices.push(notices[i]);
                }else{
                    readNotices.push(notices[i]);
                }
            };
            notices = sortedNotices.concat(readNotices);

            var body = '<div class="message-list">';
            body += '<div class="message-list-inner">';
            for (var i = 0; i < notices.length; i++) {
                var noti = notices[i];
                body += '<div class="noti_row" value="' + noti.url + '" ' + 'msgId="' + noti.msgId + '">';
                body += '<p>' + noti.title + '</p>';
                body += '<span>' + noti.time + '</span>';
                if (noti.read != 1) {
                    body += '<span class="pull-right noti_notread" style="color:red">' + '未读</span>';
                }
                body += '</div>';
            };
            body = body + '</div>';
            body = body + '</div>';
            // if (notices.length > 7) {
                body = body + '<div class="message-footer hidden">';
                body = body + '<span class="msg-back hidden">后退&nbsp;<i class="fa fa-angle-up"></i></span>' + '<span class="msg-next">更多&nbsp;<i class="fa fa-angle-down"></i></span>';
                body = body + '</div>';
            // };
            container.append(body);
            AuditMsgModule.showFooterIfneeded(container);
        },
        showFooterIfneeded: function(container){
            var maxHeight = $('.message-list', container).innerHeight();
            var listHeight = $('.message-list-inner', container).innerHeight();
            if (listHeight > maxHeight + 2) {
                $('.message-footer', container).removeClass('hidden');
            };
        },
        bindEvents: function() {

            $('#auditMsg').on('click', '.message-header', function(event) {
                event.preventDefault();
                var container = $(this).parents('.message-container');
                var name = container.attr('name');
                var notices = AuditMsgModule.auditMsg[name];
                if (container.hasClass('msg-open')) {
                    container.removeClass('msg-open');
                    $('.message-list', container).remove();
                    $('.message-footer', container).remove();
                } else if (notices && notices.length > 0) {
                    $('.message-container').removeClass('msg-open');
                    $('.message-list').remove();
                    $('.message-footer').remove();
                    container.addClass('msg-open');
                    AuditMsgModule.showMsgList(container);
                }
            });
            $('#auditMsg').on('click', '.noti_row', function(event) {
                event.preventDefault();
                var url = $(this).attr('value');
                var msgId = $(this).attr('msgId');
                var name = $(this).parents('.message-container').attr('name');
                AuditMsgModule.setLocalRead(name, msgId);
                AuditMsgModule.setMsgRead(msgId);

                var not_read = $('.noti_notread', $(this));

                if (not_read.length > 0 && !not_read.hasClass('hidden')) {

                    $('.noti_notread', $(this)).addClass('hidden');
                    var container = $(this).parents('.message-container');

                    var count = $('.msg-unread', container).text();
                    count = count - 1;
                    count = count >= 0 ? count : 0;
                    $('.msg-unread', container).text(count);
                    if (count > 0) {
                        $('.msg-unread', container).addClass('red_color');
                    } else {
                        $('.msg-unread', container).removeClass('red_color');
                        $('.msg-count', container).addClass('normal-color');
                    }
                };
                window.open(url, '_blank');
            });
            $('#auditMsg').on('click', '.msg-next', function(event) {
                event.preventDefault();
                var container = $(this).parents('.message-container');
                var inner = $('.message-list-inner', container);
                var maxHeight = $('.message-list', container).innerHeight();
                if (inner.innerHeight() + inner.position().top > maxHeight) {
                    var newtop = Math.abs(inner.position().top - maxHeight);
                    var style = 'translateY(-' + newtop + 'px)';
                    inner.css('transform', style);
                    if (inner.innerHeight() - newtop <= maxHeight) {
                        $(this).addClass('hidden');
                    };
                    $(this).siblings('.msg-back').removeClass('hidden');
                };
            });
            $('#auditMsg').on('click', '.msg-back', function(event) {
                event.preventDefault();
                var container = $(this).parents('.message-container');
                var inner = $('.message-list-inner', container);
                var maxHeight = $('.message-list', container).innerHeight();

                var newtop = inner.position().top + maxHeight;
                if (newtop > 0) {newtop = 0};
                var style = 'translateY(' + newtop + 'px)';
                inner.css('transform', style);
                if (newtop >= 0) {
                    $(this).addClass('hidden');
                };
                $(this).siblings('.msg-next').removeClass('hidden');
            });
        }
    }
    init();


    function init() {
        // $("#home_rider_count").text(home_riderCount);
        // $("#home_poi_count").text(home_poiCount);
        initOrgSelect();
        loadModules();

        $('#main-container').on('click', '.problem_main_li', function(event) {
            event.preventDefault();
            var link = $(this).find('a');
            var url = link.prop('href');
            window.open(url, '_blank');
        });
    }

    function initOrgSelect() {
        var options = '';
        var sel = '-';
        for (orgid in home_orgList) {
            if (sel == '-') {
                sel = ' slected="selected"';
            }
            var org = home_orgList[orgid];
            options = options + '<option' + sel + ' value=' + org.orgId;
            options = options + '>' + org.name + '</option>';
            sel = '';
        }
        orgSelect.append(options);
        mySelect2.amd.require(['select2/compat/matcher'], function(oldMatcher) {
            orgSelect.select2({
                matcher: oldMatcher(matchPinyin)
            });
        });
        orgSelect.select2();
    }
    orgSelect.change(function() {
        onSelOrgHidden();
    });

    function onSelOrgHidden() {
        loadModules();
    }

    function getSelOrgs() {
        var selAreas = new Array();
        $('#selectOrg option:selected').each(function() {
            selAreas.push($(this).val());
        });
        return selAreas;
    }

    function loadModules() {
        var orgs = getSelOrgs();
        if (orgs.length > 0) {
            var orgId = orgs[0];
            var selOrg = home_orgList[orgId];
            $("#home_name").text(selOrg.name);
            WeatherMoodule.loadData(orgId);
            DispatchModule.loadData(orgId);
            DataModule.loadData(orgId);
            AbnormalModule.loadData(orgId);
            NotificationModule.loadData(orgId);
            AuditMsgModule.loadData(orgId);
        };
    }
});