require.config({
    baseUrl: MT.STATIC_ROOT + '/js',
    urlArgs: 'ver=' + pageVersion
});

require(['page/offline/module/dott', 'module/validator', 'module/cookie', 'module/head-nav'], function(dott, validator, cookie, headnav) {
    var Main = {
        chart: null,
        option: null,
        $dom: $('#chart'),
        /**
         * 入口
         * @return 
         */
        init: function() {
            Main.initMenu();
            Main.initSelect();
            Main.initChart();
            Main.initEvent();
            Main.getAjaxData();
        },
        /**
         * 左侧菜单的展开和选中，因为headerjs要等请求的菜单数据返回后才加载，
         * 而此时headnav-js执行的代码找不到菜单，导致没有展开
         * @return 
         */
        initMenu: function() {
            var inter = setInterval(setActiveMenu, 100);
            function setActiveMenu() {
                $('.scrollable-sidebar').length && $('.scrollable-sidebar').slimScroll({
                    height: '100%',
                    size: '0px'
                });
                if ($(".submenu").length) {
                    var pathname = location.pathname;
                    $(".submenu").find("a").each(function() {
                        var $self = $(this);
                        if ($self.attr('href').indexOf(pathname) > -1) {
                            $self.parents("li").addClass("active")
                            $self.parent().parent().addClass("in");
                            clearInterval(inter);
                            return false;
                        }
                    });
                }
            }
        },
        /**
         * 初始化下拉框
         * @return 
         */
        initSelect: function() {
            $('#system').select2({});
            $('#app').select2({});
            $('#city').select2({});
            $('#date').select2({});
        },
        /**
         * 初始化图标
         * @return 
         */
        initChart: function() {
            Main.option = {
                chart: {
                    borderWidth: 0,
                    borderRadius: 5,
                    spacingTop: 20, //dom对图的padding-top
                    zoomType: 'xy' //缩放的坐标
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: '掉线率监控',
                    style: {
                        color: '#777',
                        fontSize: '16px',
                        fontWeight: 'bolder'
                    },
                    x: -20 //center
                },
                lang: {
                    loading: "加载中 ...",
                    noData: "暂无数据 ...",
                    months: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
                    weekdays: ['星期天', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六']
                },
                noData: {
                    style: {
                        fontWeight: 'bold',
                        fontSize: '15px',
                        color: '#303030'
                    }
                },
                xAxis: {
                    title: {
                        text: '时间'
                    },
                    tickInterval: 300 * 1000,
                    type: "datetime",
                    labels: {
                        rotation: -45,
                        step: 1,
                        format: "{value:%m-%d %H:%M}"
                    },
                    minPadding: 0.2,
                    categories: []
                },
                yAxis: {
                    title: {
                        text: '掉线率（%）'
                    },
                    min: 0, // 定义最小值
                    max: 100,
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                legend: {
                    verticalAlign: 'bottom',
                    maxHeight: 98,
                    navigation: {
                        activeColor: '#3E576F',
                        animation: true,
                        arrowSize: 12,
                        inactiveColor: '#CCC',
                        style: {
                            fontWeight: 'bold',
                            color: '#333',
                            fontSize: '12px'
                        }
                    }
                },
                plotOptions: {
                    series: {
                        marker: {
                            // enabled: false, //有没有标注点
                        },
                        events: {
                            legendItemClick: function() {
                                var visibility = this.visible ? 'visible' : 'hidden';
                                return true;
                            }
                        }
                    },
                    line: {
                        turboThreshold: 10000 //最大的点数
                    }
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: '#FCFFC5',
                    crosshairs: {
                        width: 2,
                        color: '#48B'
                    },
                    pointFormat: '<span style="color:{point.color}">\u25CF</span>{series.name}: <b>{point.y}%</b>，<b>{point.count}人</b><br/>',
                    shared: true // 共享数据给别的series，因此在formatter中能遍历访问
                },
                series: []
            };
            // Main.$dom.highcharts(Main.option);
            // Main.chart = Main.$dom.highcharts();
        },
        /**
         * 绑定事件
         * @return 
         */
        initEvent: function() {
            $('#searchBtn').on('click', Main.getAjaxData);
        },
        /**
         * 为请求准备参数
         * @return {object} 返回参数对象
         */
        prepareDate: function() {
            var dateobj = (function() {
                var date = new Date();
                var startdate,
                    enddate = dott.dateJoin(date);
                switch ($("#date").val()) {
                    // 今天
                    case "1":
                        startdate = dott.dateJoin(date.setDate(date.getDate()));
                        break;
                        // 昨天
                    case "2":
                        startdate = dott.dateJoin(date.setDate(date.getDate() - 1));
                        break;
                        // 最近三天
                    case "3":
                        startdate = dott.dateJoin(date.setDate(date.getDate() - 2));
                        break;
                        // 最近一周
                    case "4":
                        startdate = dott.dateJoin(date.setDate(date.getDate() - 6));
                        break;
                        // 最近两周
                    case "5":
                        startdate = dott.dateJoin(date.setDate(date.getDate() - 13));
                        break;
                }
                return {
                    startdate: startdate,
                    enddate: enddate
                };
            })();
            return {
                osType: $('#system').val(),
                appType: $('#app').val(),
                cityId: $('#city').val(),
                startDate: dateobj.startdate,
                endDate: dateobj.enddate
            }
        },
        /**
         * 发请求
         * @return {}
         */
        getAjaxData: function() {
            Main.chart && Main.chart.showLoading();
            $.post('/monitor/offline/data', Main.prepareDate(), 'json')
                .done(Main.renderChart)
                .fail(function() {
                    alert("数据获取失败！")
                })
                .always(function() {
                    Main.chart && Main.chart.hideLoading();
                });
        },
        /**
         * 描绘图
         * @param  {object} ajax获取到的数据
         * @return 
         */
        renderChart: function(data) {
            var seriesData = [],
                xAxisDatas = [];
            Main.chart && Main.chart.hideLoading();
            if (data.code != 0) return;
            // 准备series的data
            seriesData = (function() {
                var series = [],
                    seriesData = data['data'];
                var startdate = Main.prepareDate()['startDate'];
                $.each(seriesData, function(i, item) {
                    $.each(item['offlineRiderData'], function(j, item1) {
                        series.push({
                            'pointInterval': 300 * 1000,
                            'pointStart': Date.UTC(startdate.substring(0, 4), Number(startdate.substring(4, 6)) - 1, startdate.substring(6, 8), 0, 0),
                            'name': item['appGroup'].replace('QsApp','').replace('ZbApp','') + " " + item1['appVersion'],
                            'appGroup': item['appGroup'],
                            'visible': item1['display'],
                            'data': (function(percents, counts) {
                                var arr = [];
                                for (var i = 0; i < percents.length; i++) {
                                    arr.push({
                                        y: Number((percents[i] + "").split('.')[0]),
                                        count: counts[i]
                                    })
                                }
                                return arr;
                            })(item1['offlineRiderPercents'], item1['offlineRiderCounts'])
                        });
                    });
                });
                return series;
            })();
            Main.option.series = seriesData;
            Main.option.xAxis.labels.step = (Number($("#date").val()) > 2 ? 2 : Number($("#date").val()));
            Main.$dom.highcharts(Main.option);
            Main.chart = Main.$dom.highcharts();
        }
    };
    Main.init();
});