require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});
require(['module/root', 'module/validator','module/cookie','module/utils'], function (t, validator,cookie,utils) {
	(function() {
		function _resize() {
			var $wrapper = $('#container_wrapper');
			var wh = $(window).height();
			var t = $wrapper.offset().top;
			var h = wh - t - 20;
			$wrapper.height(h);

			$('.faq_group').each(function() {
				var top = $(this).position().top;
				$(this).data('top', top);
			})
		}
		_resize();
		$(window).resize(_resize);
	})()

	$('.faq_link').on('click', function() {
		$('#rbox').off('scroll');
		var $a = $(this);
		var id = $a.data('link');
		$a.addClass('on').parent().siblings('li').find('a').removeClass('on');

		var $h = $(id);
		var top = $h.data('top')
		$('#rbox').scrollTop(top);

		setTimeout(function() {
			$('#rbox').on('scroll', _scroll)
		}, 500)
	})

	$('#rbox').on('scroll', _scroll)

	function _scroll() {
		var sTop = $(this).scrollTop();
		$('.faq_group').each(function() {
			var top = $(this).data('top');
			if (sTop + 100 > top) {
				var id = $(this).attr('id');
				$('#' + id + '-a').addClass('on').parent().siblings('li').find('a').removeClass('on');
			}
		})
	}

	$('.faqTitleSpan').on('click',function(){
		var $currentObject = $(this).find('span');
		var $pobj = $(this).parents("li");
		if($currentObject.hasClass("glyphicon glyphicon-chevron-down")){
			$currentObject.removeClass("glyphicon glyphicon-chevron-down");
			$currentObject.addClass("glyphicon glyphicon-chevron-up");
			$pobj.addClass("openFaq");
		}else if($currentObject.hasClass("glyphicon glyphicon-chevron-up")){
			$currentObject.removeClass("glyphicon glyphicon-chevron-up");
			$currentObject.addClass("glyphicon glyphicon-chevron-down");
			$pobj.removeClass("openFaq");
		}

	})



	$("document").ready(function(){
		$(".richtext").each(function(index, obj){
			var text = decodeURIComponent($(obj).text());
			$(obj).html(text);
		});

		var requestFaqTypeId = utils.urlArg('faqType');
		if(requestFaqTypeId){
			var triggerItem = "#faq-"+requestFaqTypeId+"-a";
			$(triggerItem).click();
		}


	});
})