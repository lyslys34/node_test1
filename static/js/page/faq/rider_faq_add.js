require.config({
    baseUrl: MT.STATIC_ROOT + '/js', urlArgs: 'ver=' + pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});
require(['module/root', 'module/validator', 'module/cookie'], function (t, validator, cookie) {

    /** 添加问题弹出框 **/
    $(document).ready(function () {

        /** 取消添加或编辑 **/
        $("#saveCancel").click(function () {
            clearSaveFaqInfoBoxData();
            window.history.go(-1);
            window.close();
        })

        var question;
        var answer;
        var faqTypeId;
        var orgTypesStr = "";
        var osTypesStr = "";
        var status;
        var displayOrder;
        var id = 0; //faqInfo 的id


        /** 确认保存发布 **/
        $("#publicConfirm").click(function () {
            $("input[name='status'][value='1']").attr("checked", 'checked');
            saveConfirm();
        });

        /** 确认保存为草稿 **/
        $("#draftConfirm").click(function () {
            $("input[name='status'][value='0']").attr("checked", 'checked');
            saveConfirm();
        });

        /** 编辑弹出框 **/
        $(".js_update_faq_info").click(function () {

            id = $(this).parent().attr('faqInfoId');

            var channelType = $("#channelType").val();

            getFaqInfo(id, channelType);

        })

        function saveConfirm() {
            faqTypeId = $("#originFaqTypeId").val();
            var text = encodeURIComponent(um.getContent());
            $('#answer').val(text);

            if (validateParam()) {

                var data = getSaveParam();
                var url = "/faq/doSaveFaqInfo.ajax";

                t.utils.post(url, data, function (r) {
                    if (r.httpSuccess) {
                        if (r.data.code == 0) {
                            location.href = "/faq/faqInfoList?faqTypeId=" + faqTypeId;
                        } else {
                            showMsg(r.data.msg);
                        }
                    } else {
                        showMsg("系统错误，请重试");
                    }
                });

            }
        }

        function getFaqInfo(id, channelType) {
            var url = "updateFaqInfo/" + id + "/" + channelType;
            t.utils.post(url, "", function (r) {
                if (r.httpSuccess) {
                    if (r.data.code == 0) {
                        //填充编辑弹出框
                        fillUpdateFaqInfoBox(r.data.data);
                    } else {
                        showMsg(r.data.msg);
                    }
                } else {
                    showMsg("系统错误，请重试");
                }
            });

        }


        function fillUpdateFaqInfoBox(data) {
            $("#faqInfoTitle").html("<h4>编辑子问题<h4>");

            var faqInfoVo = data.faqInfoVo;

            $("#faqInfoId").val(faqInfoVo.id);

            buildFaqTypeSelect(data.faqTypeList, 0);


            //填充其他字段的值到弹出框
            $("#question").val(faqInfoVo.question);
            $("#answer").val(faqInfoVo.answer);
            $("#answer").val(faqInfoVo.answerExt);
            $("#resetOrder").attr("curDisplayOrder", faqInfoVo.displayOrder);
            $("#diaplayOrder").val(faqInfoVo.displayOrder);

//        $(".resetDisplayOrder").html(resetDisplayOrderButton);

            $(".resetDisplayOrder").html(resetDisplayOrderDiv);

            $("#keepDisplayOrder").val(faqInfoVo.displayOrder);
            $("#showKeepDisplayOrderRadio").css('visibility', 'visible');

            $("#lastDisplayOrder").prop("checked", false);

            $("#keepDisplayOrder").prop("checked", true);

            //TODO 如果没有其他下拉内容或者下拉内容的id就等于自己的id，则让“其他”按钮设置为不可点击
            if (data.faqInfoViewList.length == 0 || (data.faqInfoViewList.length == 1)) {
                $("#showFaqInfoOrder").attr("disabled", "true");
//            $("#keepDisplayOrder").click();
            } else {
                $("#showFaqInfoOrder").attr("disabled", false);
                appendFaqInfoSelect(data.faqInfoViewList, faqInfoVo.id);
            }

            var orgTypes = faqInfoVo.orgTypeList;
            var osTypes = faqInfoVo.osTypeList;

            for (var i = 0; i < orgTypes.length; i++) {
                $("input[name='orgTypeCheckbox']").each(function () {
                    if ($(this).val() == orgTypes[i]) {
                        $(this).prop("checked", true);
                    }
                })
            }

            for (var i = 0; i < osTypes.length; i++) {
                $("input[name='osTypeCheckbox']").each(function () {
                    if ($(this).val() == osTypes[i]) {
                        $(this).prop("checked", true);
                    }
                })
            }

            var status0 = faqInfoVo.status;
            $("input[name='status']").each(function () {
                if ($(this).val() == status0) {
                    $(this).prop("checked", true);
                } else {
                    $(this).prop("checked", false);
                }
            })

        }

        /** 排序位置后的单选按钮点击变化 **/
        $(".resetDisplayOrder").on('click', '#lastDisplayOrder', function () {

            $("#diaplayOrder").val($(this).val());
            $("#showOrderList").css('visibility', 'hidden');
            $("#orderText").css('visibility', 'hidden');
        })

        $(".resetDisplayOrder").on('click', '#keepDisplayOrder', function () {

            $("#diaplayOrder").val($(this).val());
            $("#showOrderList").css('visibility', 'hidden');
            $("#orderText").css('visibility', 'hidden');
        })

        $(".resetDisplayOrder").on('click', '#showFaqInfoOrder', function () {
            var newDisplayOrder = $("#faqInfoDisplayOrder option:selected").attr("displayOrder");
            $("#diaplayOrder").val(newDisplayOrder);

            $("#showOrderList").css('visibility', 'visible');
            $("#orderText").css('visibility', 'visible');
        })

        function getFaqInfoList(faqTypeId) {

            var url = "/faq/faqInfoListOfOrder";

            t.utils.post(url, {faqTypeId: faqTypeId}, function (r) {
                if (r.httpSuccess) {
                    if (r.data.code == 0) {
                        if (r.data.data.length == 0) {
                            $("#showFaqInfoOrder").attr("disabled", "true");
                            $("#lastDisplayOrder").click();
                            return false;
                        } else {

                            $("#showFaqInfoOrder").attr("disabled", false);
                            appendFaqInfoSelect(r.data.data, 0);

                        }

                    } else {
                        showMsg(r.data.msg);
                    }
                } else {
                    showMsg("系统错误，请重试");
                }
            })
        }

        var resetDisplayOrderDiv = '<div id="OrderRadionButtonDiv">' +
            '<label class="radio-inline w150" style="margin-top:4px; float:left">' +
            '<input type="radio" name="diaplayOrder" id="lastDisplayOrder"' +
            'value="-1" checked> 最后' +
            '</label>' +
            '<div class="radio" style="margin-top:4px; float:left; margin-left:10px;padding-left:4px">' +
            '<label style="float:left">' +
            '<input type="radio" name="diaplayOrder" id="showFaqInfoOrder"' +
            'value="-2"> 其他' +
            '</label>' +
            '<div id="showOrderList" style="visibility:hidden;float: left;margin-left:10px;margin-top:4px;">' +
            '<select class="form-control input-sm" id="faqInfoDisplayOrder" style="width:292px;">' +

            '</select>' +
            '</div>' +
            '<label style="font-size:12px;visibility:hidden;float: left" id="orderText">之前</label>' +

            '</div>' +

            '</div>';


        var resetDisplayOrderButton = '<a href="#" class="btn btn-success" id="resetOrder" flag="0">重新排序</a>';

        $(".resetDisplayOrder").on('click', '#resetOrder', function () {
            if ($(this).attr("flag") == 0) {
                $(this).attr("flag", 1);
                $(this).html("恢复原有位置");
                $("#diaplayOrder").val(-1);
                $(".resetDisplayOrder").append(resetDisplayOrderDiv);
            } else {
                $(this).attr("flag", 0);
                $(this).html("重新排序");
                $("#diaplayOrder").val($("#resetOrder").attr("curDisplayOrder"));
                $("#OrderRadionButtonDiv").remove();
            }

        })

        //获取问题分类的下拉框
        var selectDiv = '<select class="form-control input-sm" id="faqTypeId" name="faqTypeId" style="width:400px"></select>'

        function buildFaqTypeSelect(faqTypeList, excludeId) {
            $("#faqTypeDetail").append(selectDiv);
            var originFaqTypeId = $("#originFaqTypeId").val();
            appendFaqTypeSelect(faqTypeList, excludeId, originFaqTypeId);
            //分类变动，带动下面的子问题排序列表变化
            var faqTypeId = $("#faqTypeId option:selected").val();
            var faqInfoId = $("#faqInfoId").val();
            changeFaqInfoList(faqTypeId, faqInfoId);
        }

        $("#faqTypeDetail").on('change', '#faqTypeId', function () {
            $("#tempFaqTypeId").val($(this).val());

            //改变子问题的下拉框
            var faqTypeId = $("#faqTypeId option:selected").val();
            var faqInfoId = $("#faqInfoId").val();
            changeFaqInfoList(faqTypeId, faqInfoId);

            //修改排序位置的按钮选择默认为最后,将保持原位置单选按钮去掉
            var originFaqTypeId = $("#originFaqTypeId").val();
            if (faqTypeId != originFaqTypeId) {
                $("#lastDisplayOrder").click();
                $("#showKeepDisplayOrderRadio").css('visibility', 'hidden');
            } else {
                $("#showKeepDisplayOrderRadio").css('visibility', 'visible');
                $("#keepDisplayOrder").click();
            }
        })

        function changeFaqInfoList(faqTypeId, faqInfoId) {
            var url = '/faq/faqInfoListOfOrder';
            data = {faqTypeId: faqTypeId};
            t.utils.post(url, data, function (r) {
                if (r.httpSuccess) {
                    if (r.data.code == 0) {
                        appendFaqInfoSelect(r.data.data, faqInfoId);
                    } else {
                        showMsg(r.data.msg);
                    }
                } else {
                    showMsg("系统错误，请重试");
                }
            })
        }

        function appendFaqInfoSelect(faqInfoViewList, excludeId) {
            var targetSelect = $("#faqInfoDisplayOrder");
            if (faqInfoViewList) {
                targetSelect.empty();
                $.each(faqInfoViewList, function (index, item) {
                    var op = $(document.createElement("option")).val(item.id).text(item.question).attr('displayOrder', item.displayOrder);

                    //如果需要排除自己的位置的话则传入id
                    if (item.id != excludeId) {
                        targetSelect.append(op);
                    }

                });
            }

            if ($("input[name='diaplayOrder']:checked").val() == -2) {
                var newDisplayOrder = $("#faqInfoDisplayOrder option:selected").attr("displayOrder");
                $("#diaplayOrder").val(newDisplayOrder);
            }

        }

        $(".resetDisplayOrder").on('change', '#faqInfoDisplayOrder', function () {
            if ($("input[name='diaplayOrder']:checked").val() == -2) {
                var newDisplayOrder = $("#faqInfoDisplayOrder option:selected").attr("displayOrder");
                $("#diaplayOrder").val(newDisplayOrder);
            }

        })

        function appendFaqTypeSelect(faqTypeList, excludeId, selectedFaqType) {
            var targetSelect = $("#faqTypeId");
            if (faqTypeList) {
                targetSelect.empty();
                $.each(faqTypeList, function (index, item) {
                    var op = $(document.createElement("option")).val(item.id).text(item.typeDesc).attr('displayOrder', item.displayOrder);

                    //指定选中的问题分类
                    if (item.id == selectedFaqType) {
                        op.attr("selected", "selected");
                    }

                    //如果需要排除自己的位置的话则传入id
                    if (item.id != excludeId) {
                        targetSelect.append(op);
                    }


                });
            }
            //targetSelect.select2();
            //$("#faqTypeId").select2();
        }

        function validateParam() {
            if (validator.isBlank($("input[name='question']").val())) {
                alert("问题内容不能为空");
                return false;
            }

            if (($("input[name='question']").val()).length > 30) {
                alert("问题内容不能大于30个字符");
                return false;
            }

            if (validator.isBlank($("#answer").val())&&validator.isBlank($("#answerExt").val())) {
                alert("问题解答内容不能为空");
                return false;
            }

            var orgTypes = $("input[name='orgTypeCheckbox']:checked");
            if (myChannleType != 3 && (orgTypes == null || orgTypes.length == 0)) {
                alert("至少选择一个可见组织");
                return false;
            }

            var osTypes = $("input[name='osTypeCheckbox']:checked");
            if (myChannleType != 2 && (osTypes == null || osTypes.length == 0)) {
                alert("至少选择一个操作系统");
                return false;
            }

            return true;
        }

        function getSaveParam() {

            question = $("input[name='question']").val();
            answer = $("#answer").val();
            displayOrder = $("#diaplayOrder").val();

            $("input[name='orgTypeCheckbox']:checked").each(function () {
                orgTypesStr = orgTypesStr + "," + $(this).val();
            })
            orgTypesStr = orgTypesStr.substring(1);

            $("input[name='osTypeCheckbox']:checked").each(function () {
                osTypesStr = osTypesStr + "," + $(this).val();
            })
            osTypesStr = osTypesStr.substring(1);

            status = $("input[name='status']:checked").val();

            var data = {
                question: question,
                displayOrder: displayOrder,
                orgTypesStr: orgTypesStr,
                osTypesStr: osTypesStr,
                status: status,
                id: id,
                faqTypeId: faqTypeId,
                channelType:myChannleType
            }

            if($('input[name=contentType]')[0].checked) {
              data.answer = $("#answer").val();
            } else {
              data.answerExt = JSON.stringify({linkUrl: $("#answerExt").val()});
            }

            return data;

        }

        /** 清空保存&编辑的弹出框 **/
        function clearSaveFaqInfoBoxData() {

            $("#faqInfoTitle").html("");
            $("#faqTypeDetail").html("");
            $(".resetDisplayOrder").html("");
            $("#faqInfoId").val(0);

            $("input[name='question']").val("");
            $("#answer").val("");
            $("#answerExt").val("");
            $("#diaplayOrder").val("-1");
            $("#showKeepDisplayOrderRadio").css('visibility', 'hidden');
//        $("input[name='diaplayOrder']").each(function(){
//            if($(this).val()==-1){
//                $(this).attr("checked",true);
//            }else{
//                $(this).attr("checked",false);
//            }
//        })

            $("input[name='orgTypeCheckbox']:checked").each(function () {
                $(this).prop("checked", false);
            })

            $("input[name='osTypeCheckbox']:checked").each(function () {
                $(this).prop("checked", false);
            })

            $("input[name='status']").each(function () {
                if ($(this).val() == 0) {
                    $(this).prop("checked", true);
                } else {
                    $(this).prop("checked", false);
                }
            })


        }

        /** 错误提示弹出框 **/
        function showMsg(msg, reload) {
            var body = $('<div style="height: 50px;padding: 15px;text-align: center;">' + msg + '</div>');
            t.ui.showModalDialog({
                body: body, buttons: [
                    t.ui.createDialogButton('close', '关闭', function () {     // 取消
                        if (reload) {
                            location.reload(true);
                        }
                    })
                ],
                style: {
                    ".modal-dialog": {"padding": "100px 110px"},
                    ".modal-footer": {"padding": "10px"},
                    ".btn-default": {"font-size": "12px"}
                }
            });
        }

        /** 删除操作 **/
        var delId = 0;

        $(".js_del_faq_info").click(function () {
            delId = $(this).parent().attr("faqInfoId");
            $("#delFaqInfoBox").modal();
        })

        $("#cancelDel").click(function () {
            $("#delFaqInfoBox").modal('hide');
        })

        $("#confirmDel").click(function () {
            url = "/faq/delFaqInfo/" + delId;
            t.utils.post(url, "", function (r) {
                if (r.httpSuccess) {
                    if (r.data.code == 0) {
                        location.reload(true);
                    } else {
                        showMsg(r.data.msg);
                    }
                } else {
                    showMsg("系统错误，请重试");
                }
            })
        })

        //填充排序位置的下拉框
        var faqTypeId = $("#originFaqTypeId").val();
        var myChannleType = $("#channelType").val();
        getFaqInfoList(faqTypeId);

        // umeditor
        var um = UM.getEditor('umeditor', {
            initialFrameHeight: '300',
            initialFrameWidth: '600',
            // zIndex: 300
        });
        if ($('#answer').val().length > 0) {
            var html = decodeURIComponent($('#answer').val());
            um.setContent(html);
        }

        $(".resetDisplayOrder").html(resetDisplayOrderDiv);


    });

    var category = $('#faqTypeDetail').html().split('&gt;');
    if (category[0] === '美团骑手' && category[1] === '系统操作指南') {
      updateContentChoice();
    }

    function updateContentChoice() {
      $('.choice-type').show();

    }

    $('#saveFaqInfoBox').on('click', 'input[name=contentType]', function() {
      if ($(this).hasClass('active')) {
        return;
      }
      $('input[name=contentType]').toggleClass('active');
      $('#editor').toggle();
      $('#answerExt').toggle();
    });

})