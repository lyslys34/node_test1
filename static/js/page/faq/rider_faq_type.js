require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});
require(['module/root', 'module/validator','module/cookie'], function (t, validator,cookie) {
    //新建的弹出框中排序位置显示的div
    var resetDisplayOrderDiv='<div id="OrderRadionButtonDiv">'+
                                  '<label class="radio-inline" style="margin-top:4px;">'+
                                      '<input type="radio" name="diaplayOrder" id="lastDisplayOrder"'+
                                           'value="-1" checked> 最后'+
                                  '</label>'+
                                  '<label class="radio-inline" style="margin-top:4px;visibility:hidden" id="showKeepDisplayOrderRadio">'+
                                      '<input type="radio" name="diaplayOrder" id="keepDisplayOrder"'+
                                           'value=""> 位置不变'+
                                  '</label>'+
                                  '<div class="radio" style="margin:0px;">'+
                                      '<label style="float:left">'+
                                          '<input type="radio" name="diaplayOrder" id="showFaqTypeOrder"'+
                                             'value="-2"> 其他'+
                                      '</label>'+
                                      '<div id="showOrderList" style="visibility:hidden;float: left;margin-left:10px;margin-top:4px;">'+
                                          '<select class="form-control input-sm" id="faqTypeDisplayOrder" style="width:292px;">'+

                                          '</select>'+
                                      '</div>'+
                                      '<label style="font-size:12px;visibility:hidden;float: left" id="orderText">之前</label>'+
                                  '</div>'+
                              '</div>';

    //编辑时弹出框中排序位置显示的div
    var resetDisplayOrderButton='<a href="javascript:;" class="btn btn-success" id="resetOrder" flag="0">重新排序</a>';

    //用于点击保存时上传的参数
    var saveId;
    var typeDesc;
    var channelType;
    var status;
    var displayOrder;

    /** 保存问题分类（新建或者编辑） **/
    $("#confirmSave").click(function(){
        if(validateParam()){
            var data=getSaveParam();
            var url="/faq/doSaveFaqType.ajax";

            t.utils.post(url, data, function (r){
                if (r.httpSuccess){
                    if(r.data.code == 0){
                        location.reload(true);
                    }else{
                        showMsg(r.data.msg);
                    }
                }else{
                    showMsg("系统错误，请重试");
                }
            });
        }
    })

    function validateParam(){

        if(validator.isBlank($("#typeDescOfBox").val())){
            alert("分类内容不能为空");
            return false;
        }

        if(($("#typeDescOfBox").val()).length>30){
            alert("分类内容不能大于30个字符");
            return false;
        }

        return true;

    }

    function getSaveParam(){
        typeDesc=$("#typeDescOfBox").val();
        channelType=$("#channelTypeOfBox").val();
        typeCode=$("#typeCodeOfBox").val();
        status=$("input[name='status']:checked").val();
        displayOrder=$("#diaplayOrder").val();

        data={id:saveId,
        	  typeCode:typeCode,	
              typeDesc:typeDesc,
              channelType:channelType,
              status:status,
              displayOrder:displayOrder}

        return data;
    }

    /** 重新排序按钮 **/
    $("#sortOrder").on('click','#resetOrder',function(){
        if($(this).attr("flag")==0){
            $(this).attr("flag",1);
            $(this).html("恢复原有位置");
            $("#sortOrder").append(resetDisplayOrderDiv);
            $("#diaplayOrder").val(-1);
        }else{
            $(this).attr("flag",0);
            $(this).html("重新排序");
            $("#diaplayOrder").val($("#resetOrder").attr("curDisplayOrder"));
            $("#OrderRadionButtonDiv").remove();
        }

    })

    /** 新建弹出框弹出 **/
    $(".addFaqType").click(function(){
        saveId=0;

        fillSaveFaqTypeBoxOfAdd();

        $("#saveFaqTypeBox").modal();
    })

    function fillSaveFaqTypeBoxOfAdd(){

        $("#faqTypeTitle").html("<h4>添加新问题分类<h4>");
        $("#sortOrder").html(resetDisplayOrderDiv);

        //填充分类问题下拉框
        changeFaqTypeSelect($("#channelTypeOfBox").val(), $("#typeCodeOfBox").val());

    }
    /** 详情  **/
//    $(".js_info_faq_type").click(function(){
//        var faqTypeId=$(this).parent().attr("typeId");
//        var url="/faq/faqInfoList";
//        window.location.href=url+"?faqTypeId="+faqTypeId;
////        t.utils.post(url,{faqTypeId:faqTypeId},function(){
////        });
//    })

    /** 编辑弹出框弹出  **/
    $(".js_update_faq_type").click(function(){
        saveId=$(this).parent().attr("typeId");

        fillSaveFaqTypeBoxOfUpdate();

        getFaqTypeView(saveId);

        $("#saveFaqTypeBox").modal();
    })

    function fillSaveFaqTypeBoxOfUpdate(){

        $("#faqTypeTitle").html("<h4>编辑问题分类</h4>");
        //        $("#sortOrder").html(resetDisplayOrderButton);
        $("#sortOrder").html(resetDisplayOrderDiv);
        $("#showKeepDisplayOrderRadio").css('visibility','visible');

        $("#lastDisplayOrder").prop("checked",false);
        $("#keepDisplayOrder").prop("checked",true);
    }

    function getFaqTypeView(faqTypeId){
         url="/faq/updateFaqType/"+faqTypeId;
         var res;
         t.utils.post(url, "", function (r){
             if (r.httpSuccess){
                 if(r.data.code == 0){
                     res=r.data.data;
                     fillSaveFaqTypeBoxOfUpdateData(res);
                 }else{
                     showMsg(r.data.msg);
                 }
             }else{
                 showMsg("系统错误，请重试");
             }
         });

         return res;

    }


    function fillSaveFaqTypeBoxOfUpdateData(data){

        var bmFaqTypeView=data.faqTypeView;
        var faqTypeViewList=data.faqTypeViewList;

        $("#channelTypeOfBox").attr("value",bmFaqTypeView.channelType);
        $("#typeCodeOfBox").attr("value",bmFaqTypeView.typeCode);
        $("#typeDescOfBox").val(bmFaqTypeView.typeDesc);

        var status0=bmFaqTypeView.status;
        $("input[name='status']").each(function(){
            if($(this).val()==status0){
                $(this).prop("checked",true);
            }else{
                $(this).prop("checked",false);
            }
        })

        $("#diaplayOrder").val(bmFaqTypeView.displayOrder);
        $("#keepDisplayOrder").val(bmFaqTypeView.displayOrder);


//        $("#resetOrder").attr("curDisplayOrder",bmFaqTypeView.displayOrder);
        //如果没有其他下拉内容或者下拉内容的id就等于自己的id，则让“其他”按钮设置为不可点击
        if(faqTypeViewList.length==0||(faqTypeViewList.length==1)){
            $("#showFaqInfoOrder").attr("disabled","true");
//            $("#keepDisplayOrder").click();
        }else{
            $("#showFaqInfoOrder").attr("disabled",false);
            //填充分类问题的下拉框
            appendFaqTypeSelect(faqTypeViewList,saveId,0);
        }


    }

    function appendFaqTypeSelect(faqTypeList,excludeId,selectedFaqType){
        var targetSelect = $("#faqTypeDisplayOrder");
        if(faqTypeList) {
            targetSelect.empty();
            $.each(faqTypeList, function(index, item){
                var op = $(document.createElement("option")).val(item.id).text(item.typeDesc).attr('displayOrder', item.displayOrder);

                //指定选中的问题分类
                if(item.id==selectedFaqType){
                    op.attr("selected","selected");
                }

                //如果需要排除自己的位置的话则传入id
                if(item.id!=excludeId){
                    targetSelect.append(op);
                }


            });

            //判断当前选择框是否选的是其他，则变化disPlayOrder
            if($("input[name='diaplayOrder']:checked").val()==-2){
                var newDisplayOrder=$("#faqTypeDisplayOrder option:selected").attr("displayOrder");
                $("#diaplayOrder").val(newDisplayOrder);
            }

        }

    }

    /** 排序位置 点击单选按钮效果 **/
    $("#sortOrder").on('click','#lastDisplayOrder',function(){

        $("#diaplayOrder").val($(this).val());
        $("#showOrderList").css('visibility','hidden');
        $("#orderText").css('visibility','hidden');
    })

    $("#sortOrder").on('click','#keepDisplayOrder',function(){

        $("#diaplayOrder").val($(this).val());
        $("#showOrderList").css('visibility','hidden');
        $("#orderText").css('visibility','hidden');
    })

    $("#sortOrder").on('click','#showFaqTypeOrder',function(){
        var newDisplayOrder=$("#faqTypeDisplayOrder option:selected").attr("displayOrder");
        $("#diaplayOrder").val(newDisplayOrder);

        $("#showOrderList").css('visibility','visible');
        $("#orderText").css('visibility','visible');
    })

    /** 展示平台下拉框选择变化时处理 **/
    $("#channelTypeOfBox").change(function(){
        changeFaqTypeSelect($(this).val(), $("#typeCodeOfBox").val());
    })
    $("#typeCodeOfBox").change(function() {
        changeFaqTypeSelect($("#channelTypeOfBox").val(), $(this).val());
    })

    function changeFaqTypeSelect(channelType, typeCode){
        getFaqTypeViewList(channelType, typeCode);
    }

    function getFaqTypeViewList(channelType, typeCode){
        var url="/faq/faqTypeListOfOrder";
        data={channelType:channelType, typeCode:typeCode};
        t.utils.post(url,data, function (r){
            if (r.httpSuccess){
                if(r.data.code == 0){
                    var faqTypeViewList=r.data.data;
                    appendFaqTypeSelect(faqTypeViewList,saveId,0);
                }else{
                    showMsg(r.data.msg);
                }
            }else{
                showMsg("系统错误，请重试");
            }
        });
    }

    /** 排序位置下拉框变化的效果 **/
    $("#sortOrder").on("change","#faqTypeDisplayOrder",function(){
        //判断当前选择框是否选的是其他，则变化disPlayOrder
        if($("input[name='diaplayOrder']:checked").val()==-2){
            var newDisplayOrder=$("#faqTypeDisplayOrder option:selected").attr("displayOrder");
            $("#diaplayOrder").val(newDisplayOrder);
        }
    })

    /** 新建或编辑弹出框的取消操作 **/
    $("#cancel").click(function(){
        clearSaveFaqTypeBox();
        $("#saveFaqTypeBox").modal('hide');
    })

    function clearSaveFaqTypeBox(){
        $("#faqTypeTitle").html("");
        $("#typeDescOfBox").html("");
        $("#diaplayOrder").val("-1");
        $("#showKeepDisplayOrderRadio").css('visibility','hidden');
        saveId=0;

        $("#sortOrder").html("");
        $("input[name='status']").each(function(){
            if($(this).val()==0){
                $(this).prop("checked",true);
            }else{
                $(this).prop("checked",false);
            }
        })
    }

    /** 删除操作 **/
    var delId=0;

    $(".js_del_faq_type").click(function(){
        delId=$(this).parent().attr("typeId");
        delDesc=$(this).parent().attr("typeDesc");
        $("#showTitle").html("确认删除\" "+delDesc+" \"");
        $("#delFaqTypeBox").modal();
    })

    $("#cancelDel").click(function(){
        $("#delFaqTypeBox").modal('hide');
    })

    $("#confirmDel").click(function(){
        url="/faq/delFaqType/"+delId;
        t.utils.post(url,"",function(r){
            if (r.httpSuccess){
                if(r.data.code == 0){
                    location.reload(true);
                }else{
                    showMsg(r.data.msg);
                }
            }else{
                showMsg("系统错误，请重试");
            }
        });
    })


//    function appendChannelSelect(data,channelType){
//        var targetSelect = $("#channelTypeOfBox");
//        if(data) {
//            targetSelect.empty();
//            $.each(data, function(index, item){
//                var op = $(document.createElement("option")).val(item.id).text(item.channelTypeDesc);
//
//                if(item.id==channelType){
//                    op.attr("selected","selected");
//                }
//
//                targetSelect.append(op);
//
//            });
//        }
//    }

    /** 错误提示弹出框 **/
    function showMsg(msg,reload){
        var body = $('<div style="height: 50px;padding: 15px;text-align: center;">'+msg+'</div>');
        t.ui.showModalDialog({
            body: body, buttons: [
                t.ui.createDialogButton('close','关闭', function () {     // 取消
                    if(reload){
                        location.reload(true);
                    }
                })
            ],
            style:{".modal-dialog":{"padding":"100px 110px"},
                   ".modal-footer":{"padding":"10px"},
                   ".btn-default":{"font-size":"12px"}
            }
        });
    }

})