require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});


require(['module/root', 'module/validator', 'module/cookie'], function (t, validator, cookie, common) {

	$(document).ready(function(){
	    $("#approve").click(function(){
	        var paramStr = $(this).attr("data");
	        var param = eval('('+paramStr+')');
	        approve(param);
	    });
	    $("#disagree").click(function(){
        	var paramStr = $(this).attr("data");
            var param = eval('('+paramStr+')');
        	_showPanel(param);

         });
	     function approve(param){
	        var status = param.status;
	        if(status==1){
	           $.ajax({
                   url: "/rider/check/",
                   type: "POST",
                   data: param,
                   dataType: "json",
                   success: function(result){
                       if(result.code==0){
                          if(result.data==-1){
                           alert("没有需要审核的记录了！");
                           window.location.href="/rider/checklist";
                          }else{
                           alert("审核成功！请审核下一条");
                           window.location.href="/rider/checkDetail/"+result.data;
                          }
                       }
                   },
                   error:function(){
                       alert('访问出错');
                   }});
	        }

	     }
	     // 显示审核不通过原因
         function _showPanel(param) {
             var body = $(

                     '<div class="panel-body">'+
                     '       <div>'+
                     '           <label class="checkbox"><input type="checkbox" name="remark" class="fl strong" value="公安部认证不通过">公安部认证不通过</label>'+
                     '       </div>'+
                     '       <div>'+
                     '           <label class="checkbox"><input type="checkbox" name="remark" class="fl strong" value="上传的照片不清晰">上传的照片不清晰</label>'+
                     '       </div>'+
                     '       <div>'+
                     '           <label class="checkbox"><input type="checkbox" name="remark" class="fl strong" value="非本人证件">非本人证件</label>'+
                     '       </div>'+
                     '       <div>'+
                     '           <label class="checkbox"><input type="checkbox" name="remark" class="fl strong" value="其他">其他<input type="text" id="otherRemark" /></label>'+
                     '       </div>'+

                     '<div>'
             );

             function _setErrMsg(msg, clear) {
                 if (clear) {
                     e_name.val('');
                 }
                 e_errMsg.text(msg);
             }


             var dlg = t.ui.showModalDialog({
                 title: "请选择审核不通过原因", body: body, buttons: [

                     t.ui.createDialogButton('ok', function () {        // 确定

                         var $errorMsgTd = $(".err-msg", body);

                         var remark="";
                         var temp="";
                         var a = document.getElementsByName("remark");
                         var len=a.length;
                         for ( var i = 0; i < len; i++) {
                             if (a[i].checked) {
                                 temp = a[i].value;
                                 remark = remark + "," +temp;
                             }
                         }

                         var b = document.getElementById("otherRemark").value;
                         if(b){

                              remark = remark + "," +b;


                         }

                         var postData = param;

                         if (remark) {
                             postData['remark']=remark.substring(1);
                         }
                         if(remark.length==0){
                             alert("请选择原因!");
                         }else{
                             t.utils.post("/rider/check/",postData,function(result){
                             if(result.data.code==0){
                                 if(result.data.data==-1){
                                     alert("没有需要审核的记录了！");

                                     window.location.href="/rider/checklist";
                                 }else{
                                     alert("审核成功！请审核下一条");

                                     window.location.href="/rider/checkDetail/"+result.data.data;
                                 }
                             }
                             else{
                                 alert("审核失败");
                                 return false;
                                 }
                             });
                             return true;
                         }

                     }),

                     t.ui.createDialogButton('close', function () {     // 取消


                     })

                 ]
             });
         }
	});

});


