
require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});

require(['module/root', 'module/validator', 'module/cookie'], function (t, validator, cookie, common) {


    var detailDia;
    $(document).delegate('.waybillid', 'click', function(event) {
        
        showWaybillDetail($(this).attr('value'));
    });

    function showWaybillDetail(waybillId)
    {
        if (detailDia) {
            detailDia.remove();
            detailDia = null;
        }; 
        var body = '<div id="detailCon">这是ID＝' + waybillId+ '的订单详情</div>';
        $.ajax({
             url: "/partner/waybill/detailV2.ajax?waybillId="+ waybillId, 
             success: function(result){

                body = '<div>';
                body = body + result + '</div>';
                detailDia = t.ui.showModalDialog({
                    title: '订单详情', body: body, buttons: [],
                    style:{
                        ".modal-footer":{"display":"none"},
                        ".modal-title":{"display":"none"},
                        ".modal-dialog":{"width":"900px"},
                        ".modal-body":{"padding-top":"0"},
                        ".modal-header .close":{"margin-right": "-8px","margin-top": "-10px"},
                        ".modal-header":{"border-bottom": "none"}
                        }
                });
                detailDia.on("hidden.bs.modal", function(){
                    this.remove();
                    detailDia = null;
                });
            },
            error:function(){
                alert('访问出错');
            }});
    }
});