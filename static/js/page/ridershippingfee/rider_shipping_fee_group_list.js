require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});

require(['module/root', 'module/validator',  'module/cookie'], function (t, validator, cookie) {

$(document).ready(function(){
    initAddGroupBtn();
    initLimits();
    initAddGroupSubmitBtn();
    initManageShippingFee();
});

function initAddGroupBtn() {
    $("#addGroup").click(function() {
        $("#addGroupModal").modal();
    });
}

function initSearchBtn() {
    $("#search").click(function() {
        var groupName = $("#groupName");
        if (!validator.isBlank(groupName)) {
            if(validator.checkInput(groupName)) {
                showError("组名称中不能含有特殊字符");
                return false;
            }
        }

        $("#search").submit();
        
    });
}

function initLimits() {
    // $('input').keyup(function(){
    //     var value = $(this).val();
    //     $(this).val(validator.replaceInput(value));
    // });
    $('input#shippingFee').keyup(function(){  
            var c=$(this).val();  
            $(this).val(validator.replaceNum(c));
    }); 
}

function initManageShippingFee() {
    $(".manageShippingFee").click(function() {
        var id = $(this).attr('rel');
        location.href="/ridershippingfee/groupdetail?groupId="+id;
    });
}

function initAddGroupSubmitBtn() {
    $('#addGroupSubmit').on('click', function(){
        var groupName = $("#group_name").val().trim();
        var shippingFee = $("#shippingFee").val().trim();
        var paymentType = $(".payment:checked").val();
        if(groupName == "") {
          showAddGroupError("请填写组名");
          return;
        }
        if(shippingFee == "") {
          showAddGroupError("请填写配送费用金额");
          return;
        }

        if(isNaN(shippingFee)) {
          showAddGroupError("配送费用金额必须为数字");
          return;
        }

        if(paymentType == "" || (paymentType != 1 && paymentType != 2)) {
          showAddGroupError("请选择支付方式");
          return;
        }
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/ridershippingfee/addGroup",
            data: {
                groupName : groupName,
                shippingFee : shippingFee,
                paymentType : paymentType
            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4 class='text-center text-success'>添加组成功</h4>");
                    $("#addGroupModal").modal('hide');
                    $("#showAddGroupRes").modal();
                    setTimeout('$("#search").click()',2000);
                }else {
                    showAddGroupError(data.errMsg);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });
      });  
}

function showError(errMsg) {
    $("#alert_error").empty();
    $("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

function showAddGroupError(errMsg) {
    $("#alert_add_group_error").empty();
    $("#alert_add_group_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

});
