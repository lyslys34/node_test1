require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});

require(['module/root', 'module/validator',  'module/cookie'], function (t, validator, cookie) {

$(document).ready(function(){
    initEditBtn();
    initLimits();
    initEditGroupSubmitBtn();
    initAddPoiBtn();
    initDeletePoiBtn();

});

function initEditBtn() {
    $("#editGroup").click(function() {
        $("#editGroupModal").modal();
    });

    $("#addPoi").click(function() {
        $("#addPoiModal").modal();
    });
}

function initLimits() {
    // $('input').keyup(function(){
    //     var value = $(this).val();
    //     $(this).val(validator.replaceInput(value));
    // });
    $('textarea').keyup(function(){
        var value = $(this).val();
        $(this).val(value.replace(/[^\d,]/g,''));
    });
    $('input#subsidy').keyup(function(){  
            var c=$(this).val();  
            $(this).val(validator.replaceNum(c));
    }); 

    $(".selectAll").click(function() {
        var b = this.checked;
        $(".selectItem").each(function() {
            this.checked = b;
        });
    });
}

function initAddPoiBtn() {
    $("#addPoiSubmit").click(function() {
        var ids = $("#poi_ids").val();
        var id = $("#id").val();
        var idList = ids.split(",");
        var b = true;

        if(validator.isBlank(id)) {
            showAddPoiGroupError("无法获取配送费用组");
            return false;
        }

        $.each(idList, function(n,value) {
            if(value !='' && isNaN(value)) {
                showAddPoiGroupError("每一个id都必须为数字");
                b = false;
                return false;
            }
        });

        if(!b) {
            return false;
        }
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/ridershippingfee/bindPoiGroup",
            data: {
                groupId : id,
                poiIds : ids
            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4  class='text-center text-success'>添加有效商家成功</h4>");
                    $("#addPoiModal").modal('hide');
                    $("#showSuccessMsg").modal();
                    setTimeout('location.reload()',2000);
                }else {
                    showAddPoiGroupError(data.errMsg);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });   
    });
}

function initDeletePoiBtn() {
    $("#deletePoi").click(function() {
        var id = $("#id").val();
        var ids = [];
        $(".selectItem").each(function() {
            if(this.checked) {
                ids.push($(this).val());
            }
        });
        if(validator.isBlank(id)) {
            showError("无法获取补贴组");
            return false;
        }
        var b = true;
        $.each(ids, function(n,value) {
            if(value !='' && isNaN(value)) {
                showError("每一个id都必须为数字");
                b = false;
                return false;
            }
        });

        if(!b) {
            return false;
        }
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/ridershippingfee/unbindPoiGroup",
            data: {
                groupId : id,
                poiIds : ids.join(',')
            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4  class='text-center text-success'>删除商家成功</h4>");
                    $("#addPoiModal").modal('hide');
                    $("#showSuccessMsg").modal();
                    setTimeout('location.reload()',2000);
                }else {
                    showError(data.errMsg);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });   
    });
}

function initEditGroupSubmitBtn() {
    $('#editGroupSubmit').on('click', function(){
        var groupId = $("#id").val().trim();
        var groupName = $("#groupname").val().trim();
        var shippingFee = $("#shippingFee").val().trim();
        var paymentType = $(".payment:checked").val();

        if(groupName == "") {
          showAddGroupError("请填写组名");
          return;
        }
        if(shippingFee == "") {
          showAddGroupError("请填写配送费用金额");
          return;
        }

        if(isNaN(shippingFee)) {
            showAddGroupError("配送费用金额额必须为数字");
          return;
        }

        if(paymentType == "" || (paymentType != 1 && paymentType != 2)) {
          showAddGroupError("请选择支付方式");
          return;
        }

        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/ridershippingfee/updateGroup",
            data: {
                groupId : groupId,
                groupName : groupName,
                shippingFee : shippingFee,
                paymentType : paymentType
            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4  class='text-center text-success'>修改组成功</h4>");
                    $("#editGroupModal").modal('hide');
                    $("#showSuccessMsg").modal();
                    setTimeout('location.reload()',2000);
                }else {
                    showAddGroupError(data.errMsg);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });
      });  
}

function showError(errMsg) {
    $("#alert_error").empty();
    $("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

function showAddPoiGroupError(errMsg) {
    $("#alert_add_poi_group_error").empty();
    $("#alert_add_poi_group_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

function showAddGroupError(errMsg) {
    $("#alert_edit_group_error").empty();
    $("#alert_edit_group_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

});
