require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
                'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
                'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
                'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
                'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu'
            ],
            'lib/autocomplete' : {
                deps : ['page/common']
            },
            'lib/bootstrap': {
                deps: ['lib/jquery']
            }
  }
});
require(['module/root', 'module/validator',  'module/cookie', 'page/common','lib/datepicker','lib/autocomplete'], function (t, validator,cookie, common,datepicker,autocomplete) {

    var bmUserId;
    var wmPoiId;
    var orgId;
    var roleCode;
    var orgType;

    $("#addPoi").click(function(){
        bmUserId=$("input[name='userId']").val();
        orgType=$("input[name='orgType']").val();
        if(orgType!=5){
            alert("选择的组织不是角马类型的，不能绑定商家");
            return false;
        }
        orgId=$("input[name='orgId']").val();
        roleCode=$("input[name='roleCode']").val();
        $("#bindOrgModal").modal();
        return false;
    })

    $("#bindPoiSubmit").click(function(){
        wmPoiId=$("#wmPoiId").val();
        if(isBlank(wmPoiId)){
            alert("没有输入绑定的商家号，请输入");
            return false;
        }
        $("#bindOrgModal").modal('hide');
        var data={bmUserId:bmUserId,
                wmPoiId:wmPoiId,
                roleCode:roleCode,
                orgId:orgId,
                orgType:orgType}
        myPOST("/rider/bindJiaoMaPoi.ajax",data,function(r){
            if (r.httpSuccess) {
                if(r.data.success) {
                    var data=r.data.resultMsg.split(",");
                    var showPoiTr='<tr id="'+data[0]+'">'+
                                    '<td>'+data[1]+'</td>'+
                                    '<td>'+wmPoiId+'</td>'+
                                    '<td><input value="解除绑定" class="btn btn-danger btn-sm unBindPoiSubmit" poiId="'+wmPoiId+'" roleCode="'+roleCode+'" style="width:80px;"/></td>'+
                                '</tr>';
                    $("#showJiaoMaPoi").append(showPoiTr);
                } else {
                   alert(r.data.resultMsg);
                }
            } else {
                 //alert(JSON.stringify(r));
                alert('绑定失败，系统错误');
            }
        });

    })

    $(document).delegate(".unBindPoiSubmit","click",$(this),function(){
        bmUserId=$("input[name='userId']").val();
        wmPoiId=$(this).attr("poiId");
        roleCode=$(this).attr("roleCode");
        var delPoi=$(this).parent().parent();
        var data={bmUserId:bmUserId,
                  wmPoiId:wmPoiId,
                  roleCode:roleCode,
                 }
        myPOST("/rider/unBindJiaoMaPoi.ajax",data,function(r){
            if (r.httpSuccess) {
                if(r.data.success) {
                    delPoi.remove();
                } else {
                    alert(r.data.resultMsg);
                }
            } else {
              //alert(JSON.stringify(r));
               alert('解绑定失败，系统错误');
            }
        });
        return false;
    });

    function myPOST(url, data, callback)  // 发送POST请求
    {
        if(typeof(data) == 'function') { callback = data; data = null; }
        $.ajax({ type: 'POST', url: url, data:data, success:function(r) {

             callback({ httpSuccess: true, data: r });

        }, error: function(XmlHttpRequest, textStatus, errorThrown) {

            callback({ httpSuccess: false, statusCode:XmlHttpRequest.status });

        } });
    }

    function isBlank(value) {
        if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
               return this.replace(/^\s+|\s+$/g, '');
            }
        }
        return value==null || value.trim() == "";
    }
})