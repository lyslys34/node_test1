require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/root', 'module/validator','module/cookie'], function (t, validator,cookie) {
  $(document).ready(function () {
    var pageNum = 1;
    var postageMapList = null;
    var globalCityObject;
    var globalCityIdArray = [];
    var globalCityNameArray = [];
    var globalCityLevelArray = [];
    var globalCityNightPostageArray = [];
    var globalCityNewPostageArray = [];
    var globalCityPostageCodeArray  = [];

    //新增的众包驻店
    var globalCityHourlyWaybillNumArray = [];
    var globalCityHourlyInstoreArray = [];
    var globalCityCommissionInstoreArray = [];

    var startTimeSelectHtml = genTimeSelectHtml("start-time",null);
    var endTimeSelectHtml = genTimeEndSelectHtml("end-time",null);

    $('#btn-add-city').click(function () {
      window.location.href='/city/update/';
    })

  $('.modify-city').click(function () {
    window.open($(this).val(),'_blank');
  })

  $('#query-city').click(function(){
    pageNum = 1;
    getCityList();
  })

  $("#choose-all").click(function(){
    var obj = $(this);
    var checkboxList = $("input[name=need-update]");
    for( i in checkboxList ){
        var checkbox = checkboxList[i];
        checkbox.checked = obj.prop("checked") ;
    }
    calChooseNum();
  })

  $("#batch-set-postage").click(function(){
    var chosedCheckboxList = $("input[name=need-update]:checked");
    if( chosedCheckboxList==null || chosedCheckboxList.length==0){
        alert("请选择要更新的城市");
    }else{
        updatePostageList();
        $("#batch-set-postage-modal").modal();
    }
  })

  $("#batch-set-night-postage").click(function(){
    var chosedCheckboxList = $("input[name=need-update]:checked");
    if( chosedCheckboxList==null || chosedCheckboxList.length==0){
        alert("请选择要更新的城市");
    }else{
        $("#batch-set-night-postage-modal").modal();
    }
  })
  $("#batch-set-public-instore").click(function(){
      var chosedCheckboxList = $("input[name=need-update]:checked");
      if( chosedCheckboxList==null || chosedCheckboxList.length==0){
          alert("请选择要更新的城市");
      }else{
          $("#batch-set-public-instore-modal").modal();
      }

  })

      $("#batch-set-winter-postage").click(function(){
          var chosedCheckboxList = $("input[name=need-update]:checked");
          if( chosedCheckboxList==null || chosedCheckboxList.length==0){
              alert("请选择要更新的城市");
          }else{
              $("#batch-set-winter-postage-modal").modal();
          }
      })

      $("#batch-set-winter-postage-btn").click(function(){

          var winterPostageArray = [];
          var winterPostageHtmlList = $(".winter-postage");
          var winterPostageSubsidyHtmlList = $(".winter-postageSubsidy");


          var winterPostageLength = winterPostageHtmlList.length;
          var winterPostageSubsidyLength = winterPostageSubsidyHtmlList.length;

          if( winterPostageLength!=winterPostageSubsidyLength ){
              alert("页面有误，设置的数据对应不上，请联系RD");
          }else{
              for( var i=0; i<winterPostageHtmlList.length; i++ ){
                  var winterPostageObj = new Object();
                  winterPostageObj['postage']   = winterPostageHtmlList[i].value;
                  winterPostageObj['postageSubsidy']   = winterPostageSubsidyHtmlList[i].value;
                  if( (winterPostageObj['postage']=="" || isNaN(winterPostageObj['postage']))&& (winterPostageObj['postageSubsidy']=="" || isNaN(winterPostageObj['postageSubsidy'])) ){
                      alert("城市冬季配送费和夜间补贴必须是数字型");
                      clearLastUpdateFields();
                      return ;
                  }else{
                      winterPostageObj.type = "01"
                      winterPostageArray.push(winterPostageObj);
                  }

              }
          }
          //更新数据
          initBatchUpdateParamters();
          //调用发送请求的接口
          var data = new Object();
          data['cityIds'] = JSON.stringify( globalCityIdArray );
          data['cityNames'] = JSON.stringify( globalCityNameArray );
          data['cityNewPostages'] = JSON.stringify( globalCityNewPostageArray );
          if( winterPostageArray.length == 0 ){
              data['newPostage'] = "";
          }else{

              data['newPostage'] = JSON.stringify( winterPostageArray );
          }

          sendBatchUpdatePost( "/city/batchUpdateNewPostage", data );
          clearLastUpdateFields();
          $("#batch-set-winter-postage-cancel-btn").click();


      })
      $("#batch-set-winter-postage-cancel-btn").click(function(){
          $(".winter-postage").val("");
          $(".winter-postageSubsidy").val("");
      })


      //批量设置众包驻店
      $("#batch-set-public-instore-btn").click(function(){

          var publicInstoreArray = [];
          var hourlyWaybillNum = $("#hourlyWaybillNum").val();//达标单量(单/时)
          var hourlyInstore = $("#hourlyInstore").val();//驻店时薪
          var commissionInstore = $("#commissionInstore").val();//驻店提成(元/单)
          if( (hourlyWaybillNum =="" || isNaN(hourlyWaybillNum))&& (hourlyInstore=="" || isNaN(hourlyInstore)) &&(commissionInstore=="" || isNaN(commissionInstore)) ){
              alert("达标单量、驻店时薪和驻店提成必须是数字型");
              clearLastUpdateFields();
              return ;
          }
          //更新数据
          initBatchUpdateParamters();
          //调用发送请求的接口
          var data = new Object();
          data['cityIds'] = JSON.stringify( globalCityIdArray );
        //  data['cityNames'] = JSON.stringify( globalCityNameArray );
          //data['cityNewPostages'] = JSON.stringify( globalCityNewPostageArray );
        //  data['cityHourlyWaybillNums'] = JSON.stringify( globalCityHourlyWaybillNumArray );
        //  data['cityHourlyInstores'] = JSON.stringify( globalCityHourlyInstoreArray );
         // data['cityCommissionInstores'] = JSON.stringify( globalCityCommissionInstoreArray );


          data['hourlyWaybillNum'] = hourlyWaybillNum;
          data['hourlyInstore'] = hourlyInstore;
          data['commissionInstore'] = commissionInstore;

          sendBatchUpdatePost( "/city/batchUpdatePublicInstore", data );
          clearLastUpdateFields();
          $("#batch-set-public-instore-cancel-btn").click();


      })
      $("#batch-set-public-instore-cancel-btn").click(function(){
          $("#hourlyWaybillNum").val("");
          $("#hourlyInstore").val("");
          $("#commissionInstore").val("");
      })

  $("#add-more-night-postage").on("click",function(){
      var nightPostageHtml = $("#night-postage");
      var htmlNode = '<div class="row night" style="margin-top: 15px;margin-right:5px;width:700px;">'
          +'<div class="col-md-2" style="width:90px;">'
              //+'<input type="text" class="form-control input-sm start-time">'
          +startTimeSelectHtml
          +'</div>'
          +'<div class="col-md-1" style="text-align: center; margin-top: 5px;width:30px;">至'
          +'</div>'
          +'<div class="col-md-2" style="width:90px;">'
              //+'<input type="text" class="form-control input-sm end-time">'
          +endTimeSelectHtml
          +'</div>'
          +'<div class="col-md-2" style="text-align: center; margin-top: 5px;width:90px;">夜间配送费'
          +'</div>'
          +'<div class="col-md-2">'
          +'<input type="text" class="form-control input-sm night-postage" style="width:75px;">'
          +'</div>'
          +'<div class="col-md-2" style="text-align: center; margin-top: 5px;width:90px;">夜间补贴'
          +'</div>'
          +'<div class="col-md-2">'
          +'<input type="text" class="form-control input-sm night-postageSubsidy" style="width:75px;">'
          +'</div>'
          +'<div class="col-md-1" style="text-align: center; margin-top: 5px";width:50px;>'
          +'<a class="del-time" style="padding-top:8px;" onclick="removeNightHtml(this)">'
          +'<i class="fa fa-minus fa-lg opration-icon">'
          +'</i>'
          +'</a>'
          +'</div>'
          +'</div>'
          +'</div>';
      nightPostageHtml.append(htmlNode);
  })

  $("#city-level-select").on("change", function(){
    updatePostageList();
  })
  getCityList();

  $("#batch-set-night-postage-btn").click(function(){

    var nightPostageArray = [];
    //获取用户设置的城市夜间邮资方案
    var startTimeHtmlList = $(".start-time");
    var endTimeHtmlList = $(".end-time");
    var nightPostageHtmlList = $(".night-postage");
    var nightPostageSubsidyHtmlList = $(".night-postageSubsidy");

    var startTimeLength = startTimeHtmlList.length;
    var endTimeLength   = endTimeHtmlList.length;
    var nightPostageLength = nightPostageHtmlList.length;
    var nightPostageSubsidyLength = nightPostageSubsidyHtmlList.length;
    if( startTimeLength!=endTimeLength || endTimeLength!=nightPostageLength){
        alert("页面有误，设置的数据对应不上，请联系RD");
    }else{
        var sortedNightArray = [];
        for( var i=0; i<startTimeLength; i++ ){
            var nightPostageObj = new Object();
            nightPostageObj['startTime'] = startTimeHtmlList[i].value;
            nightPostageObj['endTime']   = endTimeHtmlList[i].value;
            nightPostageObj['postage']   = nightPostageHtmlList[i].value;
            nightPostageObj['postageSubsidy'] = nightPostageSubsidyHtmlList[i].value;
            if( nightPostageObj['startTime'] < nightPostageObj['endTime'] ){
                if( nightPostageObj['postage']=="" || isNaN(nightPostageObj['postage']) ){
                    alert("城市夜间配送费必须是数字型");
                    clearlastupdatefields();
                    return ;
                }else{
                    nightPostageArray.push(nightPostageObj);
                }
            }else{
                alert("起始时间不能大于结束时间");
                clearLastUpdateFields();
                return;
            }

        }

        if(startTimeLength!=0){
            sortedNightArray.push( nightPostageArray[0]['startTime'] );
            sortedNightArray.push( nightPostageArray[0]['endTime'] );
            for(var i=1; i < nightPostageArray.length; i++){
                j=0;
                var temp = nightPostageArray[i];
                while( j<sortedNightArray.length && sortedNightArray[j] < temp['startTime']){
                    j+=2;
                }
                sortedNightArray.splice(j,0, temp['endTime']);
                sortedNightArray.splice(j,0, temp['startTime']);
            }

            for(var j=1; j+2<sortedNightArray.length; j+=2){
                if( sortedNightArray[j] > sortedNightArray[j+1] ){
                    alert("时间片设置有冲突");
                    clearLastUpdateFields();
                    return;
                }
            }
        }
    }
    //更新数据
    initBatchUpdateParamters();
    //调用发送请求的接口
    var data = new Object();
    data['cityIds'] = JSON.stringify( globalCityIdArray );
    data['cityNames'] = JSON.stringify( globalCityNameArray );
    data['cityNightPostages'] = JSON.stringify( globalCityNightPostageArray );
    if( nightPostageArray.length == 0 ){
        data['nightPostage'] = "";
    }else{
        data['nightPostage'] = JSON.stringify( nightPostageArray );
    }

    sendBatchUpdatePost( "/city/batchUpdateNightPostage", data );
    clearLastUpdateFields();
    $("#batch-set-night-postage-cancel-btn").click();
  })



      $("#batch-set-postage-btn").click(function(){
    //获取城市的级别参数
    var cityLevel = $("#city-level-select").val();
    //获取城市的邮资方案code
    var postageCode = $("input[name=postage-code]:checked").val();

    //更新数据
    initBatchUpdateParamters();
    //调用发送请求的接口
    var data = new Object();
    data['cityIds'] = JSON.stringify( globalCityIdArray );
    data['cityNames'] = JSON.stringify( globalCityNameArray );
    data['cityLevels'] = JSON.stringify( globalCityLevelArray );
    data['cityPostages'] = JSON.stringify( globalCityPostageCodeArray );
    data['postage'] = postageCode;
    data['level']   = cityLevel;

    sendBatchUpdatePost( "/city/bacthUpdateLevelPostage", data );
    clearLastUpdateFields();
    $("#batch-set-postage-cancel-btn").click();
  })

  //计算选择的条目数
  calChooseNum = function(){
    var chooseInput = $("input[name=need-update]:checked");
    var itemNum = chooseInput.length;
    $("#chose-number-text").text("选中"+itemNum+"条记录");
  }

  //移除夜间邮资条目
  removeNightHtml = function(obj){
    $(obj).parent().parent().remove();
  }

  //将城市列表信息转换成Object，便于检索城市信息
  function convertListToObject(dataArray){
    globalCityObject = new Object();
    for( i in dataArray ){
        var data = dataArray[i];
        var cityId = data['cityId'];
        globalCityObject[cityId] = data;
    }
  }
  //批量设置邮资方案弹窗中根据城市级别显示城市邮资列表
  function updatePostageList(){
    var tempCityLevel = $("#city-level-select").val();
    if( postageMapList == null ){
        alert("未获取到系统设置的邮资方案列表");
    }else{
        var currentPostageList = postageMapList[ tempCityLevel ];
        var postageStrategyListHtml = $("#postage-strategy-list");
        postageStrategyListHtml.empty();
        for( i in currentPostageList ){
            var postageItem = currentPostageList[i];
            var postageItemHtml = null;
            if( i==0 ){
                postageItemHtml='<div class="radio">'
                                +'<label>'
                                +'<input type="radio" name="postage-code" id="postage-strategy" checked="checked" value="'+postageItem['value']+'"/>'+postageItem['comment']
                                +'</label>'
                                +'</div>'
            }else{
                postageItemHtml='<div class="radio">'
                                +'<label>'
                                +'<input type="radio" name="postage-code" id="postage-strategy" value="'+postageItem['value']+'"/>'+postageItem['comment']
                                +'</label>'
                                +'</div>'
            }
            postageStrategyListHtml.append(postageItemHtml);
        }
    }

  }

  //批量更新城市信息前调用该函数获取原始数据属性值
  function initBatchUpdateParamters(){
    getChosedCityIdList();
    getOriginCityFiledArray();
  }

  //获取选中更新的城市id列表和城市名称列表
  function getChosedCityIdList(){
    var chosedCheckboxList = $("input[name=need-update]:checked");
    var cityIdArray = [];

    for( var i=0; i< chosedCheckboxList.length; i++ ){
        var checkbox = chosedCheckboxList[i];

        var cityId = $(checkbox).parent("td").siblings(".cityId").text();
        cityIdArray.push(cityId);
    }

    globalCityIdArray = cityIdArray;
  }

  //根据globalCityIdArray 和globalCityObject获取要参数的参数原始值
  function getOriginCityFiledArray(){
    if( globalCityIdArray==null ){
        return ;
    }
    for(i in globalCityIdArray){
        var cityId = globalCityIdArray[i];
        var city = globalCityObject[cityId];
        var cityName = city['name'];
        var cityPostageCode = city['postageStrategyId'];
        var cityNightPostage = city['nightPostage'];
        var cityNewPostage = city['newPostage'];
        var cityLevel = city['level'];
        //新增的众包驻店
        var cityHourlyWaybillNum = city['hourlyWaybillNum'];
        var cityHourlyInstore = city['hourlyInstore'];
        var cityCommissionInstore = city['commissionInstore'];

        globalCityNameArray.push(cityName);
        globalCityPostageCodeArray.push(cityPostageCode);
        globalCityNightPostageArray.push(cityNightPostage);
        globalCityNewPostageArray.push(cityNewPostage);
        globalCityLevelArray.push(cityLevel);
        //新增的众包驻店
        globalCityHourlyInstoreArray.push(cityHourlyInstore);
        globalCityHourlyWaybillNumArray.push(cityHourlyWaybillNum);
        globalCityCommissionInstoreArray.push(cityCommissionInstore);
    }
  }

  //清除要改动的原始参数列表
  function clearLastUpdateFields(){
    globalCityIdArray = [];
    globalCityLevelArray = [];
    globalCityNameArray = [];
    globalCityNightPostageArray = [];
    globalCityPostageCodeArray = [];
    //新增的众包驻店
    globalCityHourlyInstoreArray = [];
    globalCityCommissionInstoreArray = [];
    globalCityHourlyWaybillNumArray = [];
  }

  //根据查询参数，获取城市信息列表
  function getCityList(){
    $("#chose-number-text").text("");
    clearLastUpdateFields();
    $("#choose-all")[0].checked="";
    var cityId = $("#cityId").val();
    var cityLevel = $('#city-level').val();
    requestCityList(cityId, cityLevel, pageNum, 20);

  }

  //发送获取城市信息的请求
  function requestCityList( cityId, cityLevel, pageNo, pageSize ){
    $.get("/city/search",{ cityId:cityId, cityLevel:cityLevel,pageNo:pageNo,pageSize:pageSize})
    .done( function( json ){
      var code = json.code;
      if( code == 0 ){
        fillCityList(json.data);
      }else{
        alert(json.message);
      }
    })
    .fail( function() {
      alert("搜索城市信息请求失败");
    });
  }

  //根据城市信息数据，填充表格
  function fillCityList(data){
    postageMapList = data['postageMapList'];
    var cityList = data['vos'];
    var cityInfoHtmlBody = $("#city-info");
    cityInfoHtmlBody.empty();
    for(var i in cityList ){
      var city = cityList[i];
      var cityRecord = "<tr>"+'<td ><input class="cityCheck" type="checkbox" name="need-update" style="opacity: 1;position:relative" onclick="calChooseNum()"></td>'
              + '<td class="cityId">'+city['cityId']+"</td>"
              + '<td class="name">'+city["cityName"]+"</td>"
              + '<td class="level">'+city['level']+"</td>"
              + '<td class="caiwuPinyin">'+city['caiwuPinyin']+"</td>"
              + '<td class="postageStartegyId" style="width: 200px">'+city['postageStrategyName']+"</td>"
              + '<td class="nightPostage" style="width: 200px;">';
      var nightPostageList = city['nightPostageStrategys'];
      var newPostageStrategyList = city['newPostageStrategys'] ;
      for(var j in nightPostageList ){
        var nightPostage = nightPostageList[j];
        cityRecord += nightPostage['startTime']+"-"+nightPostage['endTime']+"      配送费"+nightPostage['postage']+"元"+" 补贴"+nightPostage['postageSubsidy']+"元<br/>";
      }
        cityRecord +="</td>";
      for(var j in newPostageStrategyList){
          var newPostage = newPostageStrategyList[j];
          cityRecord += '<td class="newPostage" style="width: 200px;">'
                        +"冬季配送费"+newPostage['postage']+"元 补贴"+newPostage['postageSubsidy']
                        +'元</td>';
      }
      cityRecord +='<td style="width: 200px;">'
                +"达标单量:"+city['hourlyWaybillNum']+"单/时<br/>"
                +"驻店时薪:"+city['hourlyInstore']+"元<br/>"
                +"驻店提成:"+city['commissionInstore']+"元/单";
      cityRecord+='<td><a href="/city/update/'+city['cityId']+'" target="_blank">修改</a>   '
            +'<a href="/city/oplog/'+city['cityId']+'" target="_blank">操作记录</a>   </td>'+"</tr>";

      cityInfoHtmlBody.append(cityRecord);
    }

    var pageNo = data['pageNo'];
    var totalPageNo = data['totalPageNo'];
    var totalCount  = data['totalCount'];

    $("#record-count").text("共有"+totalCount+"记录");
    gen_pager(totalPageNo, pageNo, totalCount);

    convertListToObject(cityList);
  }

  //发送调用批量更新的请求
  function sendBatchUpdatePost(uri, data){
    $.post(uri,data)
    .done(function(json){
        var code = json.code;
        if( code == 0 ){
            alert("数据更新成功");
            getCityList();
        }else{
            alert("数据更新失败");
        }
    })
    .fail(function(){
        alert("数据请求失败");
    });
  }

      //产生时间下拉列表
      function genTimeSelectHtml( classTag, value ){
          var timeSelectHtml = '<select class="form-control input-sm '+classTag+'">'
          var timeValue = '';
          var optionHtml = '';
          j=0;
          while( j < 24){

              if(j < 10){
                  timeValue = "0" + j+":00";
              } else {
                  timeValue = j+":00";
              }

              selected="";
              if( timeValue == value ){
                  selected = "selected";
              }
              optionHtml = '<option value="'+timeValue+'" '+selected+'>'+timeValue+'</option>';
              timeSelectHtml += optionHtml;

              if(j < 10){
                  timeValue = "0" + j + ":30";
              } else {
                  timeValue = j + ":30";
              }

              selected="";
              if( timeValue == value ){
                  selected = "selected";
              }
              optionHtml = '<option value="'+timeValue+'" '+selected+'>'+timeValue+'</option>';
              timeSelectHtml += optionHtml;
              j++;
          }

          timeValue = "23:59";
          selected="";
          if( timeValue == value ){
              selected = "selected";
          }
          optionHtml = '<option value="'+timeValue+'" '+selected+'>'+timeValue+'</option>';
          timeSelectHtml += optionHtml;

          return timeSelectHtml+"</select>";
      }

  pager_click = function(link_num) {
      pageNum = parseInt(link_num);
      getCityList();
  }
  pager_pre   = function(){
      pageNum = pageNum -1;
      getCityList();
  }
  pager_next  = function(){
      pageNum = pageNum + 1;
      getCityList();
  }

      //产生结束时间下拉列表
      function genTimeEndSelectHtml( classTag, value ){
          var timeSelectHtml = '<select class="form-control input-sm '+classTag+'">'
          var timeValue = '';
          var optionHtml = '';
          j=0;
          while( j < 24){
              if(j!=0) {
                  if (j < 10) {
                      timeValue = "0" + j + ":59";
                  } else {
                      timeValue = j + ":59";
                  }

                  selected = "";
                  if (timeValue == value) {
                      selected = "selected";
                  }
                  optionHtml = '<option value="' + timeValue + '" ' + selected + '>' + timeValue + '</option>';
                  timeSelectHtml += optionHtml;
              }
              if(j < 10){
                  timeValue = "0" + j + ":29";
              } else {
                  timeValue = j + ":29";
              }

              selected="";
              if( timeValue == value ){
                  selected = "selected";
              }
              optionHtml = '<option value="'+timeValue+'" '+selected+'>'+timeValue+'</option>';
              timeSelectHtml += optionHtml;
              j++;
          }

          timeValue = "23:59";
          selected="";
          if( timeValue == value ){
              selected = "selected";
          }
          optionHtml = '<option value="'+timeValue+'" '+selected+'>'+timeValue+'</option>';
          timeSelectHtml += optionHtml;

          return timeSelectHtml+"</select>";
      }

  function gen_pager(pageTotal, pageNum, total) {
      $('#pager').empty();
      if (total==0) {
        $('#pager').append('<li class="disabled"><a href="javascript:;">上一页</a></li>');
        $('#pager').append('<li class="disabled"><a href="javascript:;">下一页</a></li>');
        return false;
      }
      if (pageNum == 1) {
        $('#pager').append('<li class="disabled"><a href="javascript:;">上一页</a></li>');
      } else {
        $('#pager').append('<li><a href="javascript:;" onclick="pager_pre();">上一页</a></li>');
      };
      if (pageTotal <= 7) {
        for (var i = 1; i <= pageTotal; i++) {
          if (pageNum == i) {
            $('#pager').append('<li class="active page-n"><a>' + i + '</a></li>');
            continue;
          };
          $('#pager').append('<li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">' + i + '</a></li>');
        }
      } else {
        pageNum = parseInt(pageNum);
        diff_t = pageTotal - pageNum;
        diff_1 = pageNum - 1;
        if (diff_1 > 2) {
          pageNum = parseInt(pageNum);
          if (diff_t < 3) {
            $('#pager').append('<li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">1</a></li><li><a class="page-n" href="javascript:;">...</a></li>');
            for (var i = parseInt(pageNum) - 1; i <= pageTotal; i++) {
              if (pageNum == i) {
                $('#pager').append('<li class="active page-n"><a>' + i + '</a></li>');
                continue;
              };
              $('#pager').append('<li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">' + i + '</a></li>');
            }
          } else {
            $('#pager').append('<li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">1</a></li><li><a class="page-n" href="javascript:;">...</a></li>');
            for (var i = parseInt(pageNum) - 1; i <= pageNum + 1; i++) {
              if (pageNum == i) {
                $('#pager').append('<li class="active page-n"><a>' + i + '</a></li>');
                continue;
              };
              $('#pager').append('<li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">' + i + '</a></li>');
            }
            $('#pager').append('<li><a class="page-n" href="javascript:;">...</a></li><li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">' + pageTotal + '</a></li>');
          };
        } else {
          for (var i = 1; i <= parseInt(pageNum) + 1; i++) {
            if (pageNum == i) {
              $('#pager').append('<li class="active page-n"><a>' + i + '</a></li>');
              continue;
            };
            $('#pager').append('<li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">' + i + '</a></li>');
          }
          $('#pager').append('<li><a class="page-n" href="javascript:;">...</a></li>');
          $('#pager').append('<li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">' + pageTotal + '</a></li>');
        };
      };
      if (pageNum == pageTotal) {
        $('#pager').append('<li class="disabled"><a href="javascript:;">下一页</a></li>');
      } else {
        $('#pager').append('<li><a href="javascript:;" onclick="pager_next();">下一页</a></li>');
      };
    }


  });




});
