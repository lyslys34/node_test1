
$(document).ready(function () {
	var startTimeSelectHtml = genTimeSelectHtml("start-time", null);
	var endTimeSelectHtml   = genTimeSelectHtml("end-time", null);
	var postageData = $('#night-postage-hide').val();
	var newPostageData = $('#new-postage-hide').val();
	if( (postageData!=undefined) && (postageData!=null) && (postageData!="")){
		var postageData = JSON.parse(postageData);
		var nightPostageHtml = $("#night-postage-list");
		for( i in postageData ){

			if((postageData[i]['postageSubsidy']==undefined) || (postageData[i]['postageSubsidy']==null) || (postageData[i]['postageSubsidy']=="")){
				postageData[i]['postageSubsidy']="0";
			}
			var itemHtml = '<div class="row night" style="margin-top: 15px">' +
                                   		'<div class="col-md-1" style="width:100px;">' +
                                   		genTimeSelectHtml("start-time", postageData[i]['startTime']) +
                                   		'</div>' +
                                   		'<div class="col-md-1" style="text-align: center; margin-top: 8px; width:20px;">至</div>' +
                                   		'<div class="col-md-1" style="width:100px;">' +
										genTimeEndSelectHtml("end-time", postageData[i]['endTime']) +
                                   		'</div>' +
                                   		'<div class="col-md-1" style="text-align: center; margin-top: 8px;  width:120px;">夜间配送费：</div>' +
                                   		'<div class="col-md-1">' +
                                   		'<input type="text" class="form-control input-sm night-postage" style="width:80px;" value="'+postageData[i]['postage']+'">' +
                                   		'</div>' +
										'<div class="col-md-1" style="text-align: center; margin-top: 8px;  width:100px; margin-left:50px;">夜间补贴：</div>' +
										'<div class="col-md-1">' +
										'<input type="text" class="form-control input-sm night-postage-subsidy" style="width:80px;" value="'+postageData[i]['postageSubsidy']+'">' +
										'</div>' +
                                   		'<div class="col-md-1" style="text-align: center; margin-top: 8px;">' +
                                   		'<a class="del-night-postage-item pull-right" onClick="removeNightHtml(this)">' +
                                   		'<i class="fa fa-minus fa-lg opration-icon"></i>' +
                                   		'</a>' +
                                   		'</div>' +
                                   		'</div>';
            nightPostageHtml.append(itemHtml);
		}

	}

	var newPostageHtml = $("#new-postage-list");
	if( (newPostageData!=undefined) && (newPostageData!=null) && (newPostageData!="")){
		var newPostageData = JSON.parse(newPostageData);
		for( i in newPostageData ){
			var itemHtml = '<div class="row newPos" style="margin-top: 15px">' +
				'<div class="col-md-1" style="text-align: center; margin-top: 8px;  width:120px;">冬季配送费：</div>' +
				'<div class="col-md-1">' +
				'<input type="text" class="form-control input-sm new-postage" style="width:80px;" value="'+newPostageData[i]['postage']+'">' +
				'</div>' +
				'<div class="col-md-1" style="text-align: center; margin-top: 8px;  width:100px;margin-left:50px">冬季补贴：</div>' +
				'<div class="col-md-1">' +
				'<input type="text" class="form-control input-sm new-postage-subsidy" style="width:80px;" value="'+newPostageData[i]['postageSubsidy']+'">' +
				'</div>' +
				'</div>';
			newPostageHtml.append(itemHtml);
		}


	}
	else{
		var itemHtml = '<div class="row newPos" style="margin-top: 15px">' +
			'<div class="col-md-1" style="text-align: center; margin-top: 8px;  width:120px;">冬季配送费：</div>' +
			'<div class="col-md-1">' +
			'<input type="text" class="form-control input-sm new-postage" style="width:80px;" >' +
			'</div>' +
			'<div class="col-md-1" style="text-align: center; margin-top: 8px;  width:100px;margin-left:50px;">冬季补贴：</div>' +
			'<div class="col-md-1">' +
			'<input type="text" class="form-control input-sm new-postage-subsidy" style="width:80px;" ' +
			'</div>' +
			'</div>';
		newPostageHtml.append(itemHtml);
	}


	$('#add-more-night-postage').click(function(){
		var nightPostageHtml = $("#night-postage-list");
		nightPostageHtml.append('<div class="row night" style="margin-top: 15px">' +
        		'<div class="col-md-1" style="width:100px;">' +
        		startTimeSelectHtml +
        		'</div>' +
        		'<div class="col-md-1" style="text-align: center; margin-top: 8px; width:20px;">至</div>' +
        		'<div class="col-md-1" style="width:100px;">' +
        		endTimeSelectHtml +
        		'</div>' +
        		'<div class="col-md-1" style="text-align: center; margin-top: 8px;  width:120px;">夜间配送费：</div>' +
        		'<div class="col-md-1">' +
        		'<input type="text" class="form-control input-sm night-postage" style="width:80px;">' +
        		'</div>' +
				'<div class="col-md-1" style="text-align: center; margin-top: 8px; width:100px; margin-left:50px;">夜间补贴：</div>' +
				'<div class="col-md-1">' +
				'<input type="text" class="form-control input-sm night-postage-subsidy" style="width:80px;">' +
				'</div>' +
        		'<div class="col-md-1" style="text-align: center; margin-top: 8px;">' +
        		'<a class="del-night-postage-item pull-right" onClick="removeNightHtml(this)">' +
        		'<i class="fa fa-minus fa-lg opration-icon"></i>' +
        		'</a>' +
        		'</div>' +
        		'</div>');
	})

	$('#city_update').click(function () {
		$('#logmsg').removeClass().slideUp().empty();
		$('#city_update').attr('disabled', 'disabled');
		var name = $('#name').val();
		var cityId = $('#cityId').val();
		var postage = $('#postage').val();
		var regionId = $('#regionId').val();
		var level = $('#level').val();
		var caiwuPinyin = $('#caiwuPinyin').val();
		var postageStrategyId = $("input[name='postageStrategyRadio']:checked").val();
		var data = new Array();
		var dataNewPostage =  new Array();
		var i = 0;
		var j = 0;
		var flag = true;
		$('.night').each(function(){
			var start = $(this).find('.start-time').val();
			var end = $(this).find('.end-time').val();
			var postage = $(this).find('.night-postage').val();
			var postageSubsidy = $(this).find('.night-postage-subsidy').val();
			if( postage=='' || isNaN(postage) ){
				alert("请填写夜间配送费");
				flag = false;
			}
			if( postageSubsidy=='' || isNaN(postageSubsidy) ){
				alert("请填写夜间补贴");
				flag = false;
			}
			if( start>=end ){
				alert("开始时间不能大于等于结束时间");
				flag = false;
			}
			data[i++] = {"startTime":start,"endTime":end, "postage":postage, "postageSubsidy":postageSubsidy};
		})
		$('.newPos').each(function(){
			var postage = $(this).find('.new-postage').val();
			var postageSubsidy = $(this).find('.new-postage-subsidy').val();
			if( postage=='' || isNaN(postage) ){
				alert("请填写冬季配送费");
				flag = false;
			}
			if( postageSubsidy=='' || isNaN(postageSubsidy) ){
				alert("请填写冬季补贴");
				flag = false;
			}
			dataNewPostage[j++] = {"postage":postage, "postageSubsidy":postageSubsidy,type:"01"};
		})
		if( flag == false ){
			$('#city_update').removeAttr('disabled');
			return ;
		}
		var nightPostageStrategy = JSON.stringify(data);
        var newPostageStrategy = JSON.stringify(dataNewPostage);
		$.ajax({
		  url: '/city/doUpdate',
		  type: "POST",
		  data: {name: name, cityId: cityId, postage: postage, regionId: regionId, level: level, caiwuPinyin: caiwuPinyin, postageStrategyId: postageStrategyId, nightPostageStrategy:nightPostageStrategy,newPostageStrategy:newPostageStrategy},
		  timeout: 3000,
		  dataType:"json",
		  success:function(data){
		  	$('#city_update').removeAttr('disabled');
		    if (data.code == "0") {
		    	$('#logmsg').addClass('alert alert-success').append(data.msg).slideDown();
		    } else {
		        $('#logmsg').addClass('alert alert-danger').append(data.msg).slideDown();
		    }
		  },
		  error:function(){alert("请求错误，请稍后再试");$('#city_update').removeAttr('disabled');}
		});
		$('body,html').animate({scrollTop:0},1000);
	})

	var dynamic_city_leve = $('#level').val();

        $('#level').change(function () {
        	level = $(this).val();
                $('.radio').hide();
                $('.radio-'+level).show();
                value = level + '00';
                $('input[name=postageStrategyRadio][value='+value+']').attr("checked",true);
        })
      //移除夜间邮资条目
      removeNightHtml = function(obj){
          $(obj).parent().parent().remove();
      }
      //产生时间下拉列表
      function genTimeSelectHtml( classTag, value ){
        var timeSelectHtml = '<select class="form-control input-sm '+classTag+'">'
        var timeValue = '';
        var optionHtml = '';
        j=0;
        while( j < 24){

			if(j < 10){
				timeValue = "0" + j+":00";
			} else {
				timeValue = j+":00";
			}

            selected="";
            if( timeValue == value ){
            	selected = "selected";
            }
            optionHtml = '<option value="'+timeValue+'" '+selected+'>'+timeValue+'</option>';
            timeSelectHtml += optionHtml;

			if(j < 10){
				timeValue = "0" + j + ":30";
			} else {
				timeValue = j + ":30";
			}

			selected="";
            if( timeValue == value ){
            	selected = "selected";
            }
            optionHtml = '<option value="'+timeValue+'" '+selected+'>'+timeValue+'</option>';
            timeSelectHtml += optionHtml;
            j++;
        }

        timeValue = "23:59";
		selected="";
        if( timeValue == value ){
            selected = "selected";
        }
        optionHtml = '<option value="'+timeValue+'" '+selected+'>'+timeValue+'</option>';
        timeSelectHtml += optionHtml;

        return timeSelectHtml+"</select>";
      }
	//产生结束时间下拉列表
	function genTimeEndSelectHtml( classTag, value ){
		var timeSelectHtml = '<select class="form-control input-sm '+classTag+'">'
		var timeValue = '';
		var optionHtml = '';
		j=0;
		while( j < 24){
			if(j!=0) {
				if (j < 10) {
					timeValue = "0" + j + ":59";
				} else {
					timeValue = j + ":59";
				}

				selected = "";
				if (timeValue == value) {
					selected = "selected";
				}
				optionHtml = '<option value="' + timeValue + '" ' + selected + '>' + timeValue + '</option>';
				timeSelectHtml += optionHtml;
			}
			if(j < 10){
				timeValue = "0" + j + ":29";
			} else {
				timeValue = j + ":29";
			}

			selected="";
			if( timeValue == value ){
				selected = "selected";
			}
			optionHtml = '<option value="'+timeValue+'" '+selected+'>'+timeValue+'</option>';
			timeSelectHtml += optionHtml;
			j++;
		}

		timeValue = "23:59";
		selected="";
		if( timeValue == value ){
			selected = "selected";
		}
		optionHtml = '<option value="'+timeValue+'" '+selected+'>'+timeValue+'</option>';
		timeSelectHtml += optionHtml;

		return timeSelectHtml+"</select>";
	}

})