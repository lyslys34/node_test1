require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root', 'module/validator','module/cookie','lib/datepicker'], function (t, validator,cookie,datepicker) {
  datepicker.set({
    maxDate:new Date()
  });
  $(".ui-datepicker-trigger").hide();


  $("#export").click(function(){
    var searchAction = $("#fm").attr("action");
    $("#fm").attr("action","/feedback/export");
    $("#fm").attr("target","_blank");
    $("#fm").submit();
    $("#fm").attr("action",searchAction);
    $("#fm").attr("target","_self");
  });

  $("#fm").submit(function(){
    var beginTime = $("[name=beginTime]").val();
    var endTime = $("[name=endTime]").val();
    if(beginTime > endTime){
      alert("开始时间不能大于结束时间！")
      return false;
    }
  });

  questionTypeChange();

  roleChange();

  $("#questionType").change(function(){
    questionTypeChange();
  });

  $("#role").change(function(){
    roleChange();
  });
  $("[name='reply']").click(function(){
    var id = $(this).attr("id");
    var feedbackerId = $(this).attr("feedbackerId");
    showReplyPanel(id,feedbackerId);
  });

  function osTypeClass(){
    if($("#complainPoi").hasClass("hide")){
      $("#osType").addClass("col-md-offset-6");
    }else{
      $("#osType").removeClass("col-md-offset-6");
    }
  }

  function questionTypeChange(){
    var questionType = $("#questionType").val();
    if(questionType == "8000"){
      $("#complainPoi").removeClass("hide");
    }else{
      $("#complainPoi").addClass("hide");
    }
    osTypeClass();
  }

  function roleChange(){
    var role = $("#role :selected").attr("code");
    if(role == 1001){
      $("#osType").removeClass("hide");
    }else{
      $("#osType").addClass("hide");
    }
    osTypeClass();
  }


  function showReplyPanel(id,feedbackerId){
    var body = $("#replyPanel");
    $("#feedbackId").val(id);
    $("#feedbackerId").val(feedbackerId);
    
    body.removeClass("hide");
    var dlg = t.ui.showModalDialog({
    body: body, buttons: [
      t.ui.createDialogButton('ok','提交', function () {
          $("#status").val("1");
          save();
          return false;
          }),
      t.ui.createDialogButton('close','取消', function(){
          closePanel();
          })
    ],
    style:{".modal-content":{"width":"530px"}}
    });
  }
  function save(){
    var content = $.trim($("#content").val());
    var remark = $.trim($("#remark").val());
    if(content.length == 0){
      alert("回复内容不能为空");
      return false;
    }
    if(content.length > 500){

      alert("长度不能超过500字");
      return false;
    }
    alert
    $("#returnUrl").val(window.location.href);
    $("#replyPanel").find("form").submit();
  }

  function closePanel(){
    $("#replyPanel").find("textarea").val("");
    $("#replyPanel").find("input").val("");
  }
});