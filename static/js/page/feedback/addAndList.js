require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root'], function (t) {
  var complaintsOption = $(".complaints");
  complaintsOption.remove();
  $("[required=true]").each(function(){
    $(this).prepend("<span style='color:red;padding-right:2px;'>*</span>");
  });

  $("[name=type]").click(function(){
    if($(this).val() == 1){
      $("#questionType option:eq("+($("#questionType").length-3)+")").after(complaintsOption);
    }else{
      complaintsOption.remove();
    }
  });

  $("#fm").submit(function(e){
    var type = $("#type :checked").val();
    if(type < 0 || type=='undefined' || type == undefined){
      alert("请选择反馈类型");
      return false;
    }
    var questionType = $("#questionType").val();
    if(questionType < 0){
      alert("请选择问题分类");
      return false;
    }
    var content = $.trim($("#content").val());
    if(content.length == 0){
      alert("请输入内容");
      return false;
    }
    if(content.length > 500){
      alert("反馈内容大于500个字符");
      return false;
    }
  });
});