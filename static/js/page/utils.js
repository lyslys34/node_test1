/**
 * Created by lixiangyang on 15/6/29.
 */

var IDDWeight =[7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1];
var IDCheckDigit = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];

var isIdentityCardValid = function(number) {
    if (number != null && (number.length == 15 || number.length == 18)) {
        if (/^\d*.?$/.test(number)) {
            if (number.length == 18) {
                var last = number.charAt(17);
                var c = getIDCardCheckDigit(number);
                return last == c || (last == 'x' && c == 'X');
            } else {
                return true;
            }
        }
    }
    return false;
}

var getIDCardCheckDigit = function (eighteenCardID) {
    //assert (eighteenCardID.length() == 18);
    var sum = 0;
    for (var i = 0; i < 17; i++) {
        var k = eighteenCardID.charAt(i);
        var a = (k - '0');
        sum += IDDWeight[i] * a;
    }
    return IDCheckDigit[sum % 11];
}


var isMobile = function(mobile) {
    if (typeof mobile != 'undefined') {
        return /^1\d{10}$/.test(mobile);
    }
    return false;
}


var isBlank = function(value) {
    if(typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function() {
            return this.replace(/^\s+|\s+$/g, '');
        }
    }
    return !_valueExist(value) || value.trim() == "";
}


var isInteger = function(s) {
    var re = /^[1-9]+[0-9]*]*$/;

    return re.test(s) || s == 0;
}


function _valueExist(value) {
    if (typeof value == 'undefined') {
        return false;
    }
    if (value == null) {
        return false;
    }
    return true;
}
