require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/head-nav'], function (nav) {

	var getRequest = function(name){
		var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); 
		var r = window.location.search.substr(1).match(reg);  
		if (r!=null) return r[2]; return ""; 
	};

	function getLocalTime(nS) {     
		var now = new Date(parseInt(nS) * 1000);  
		var year=now.getFullYear();     
		var month=now.getMonth()+1;     
		var date=now.getDate();     
		var hour=now.getHours();     
		var minute=now.getMinutes();     
		var second=now.getSeconds();     
      	return   year+"-"+month+"-"+date+"  "+hour+":"+minute+":"+second;   
    } 

	var id = getRequest("id");


	if(id!=""){

		$.ajax({
			url : "/settleAdjustment/list/op/search",
			type :"post",
			data : {adjustmentId:getRequest("id")},
			dataType :"json",
			success : function(data){
				if(data.code==1){
					console.log(data);
					var html = [];
					for(var i=0;i<data.data.length;i++){
						var d = data.data[i];
						html.push('<tr>');
						html.push('<td>'+d.operatorName+'</td>');
						html.push('<td>'+d.operatorComment+'</td>');
						html.push('<td>'+getLocalTime(d.ctime)+'</td>');
						html.push('<td>'+(d.type==1?"通过":"驳回")+'</td>');
						html.push('</tr>');
					}
					$("#list").html(html.join(""));
				}else{
					alert(data.msg);	
				}
			},
			error:function(){
				alert(sysErrorMsg);
			}
		});	

	}


});