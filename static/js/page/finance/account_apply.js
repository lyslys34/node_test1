require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/head-nav'], function (nav) {

	var sysErrorMsg = "网络请求遇到问题，请稍后再试";
	//初始化月份
	var getMonth = function(){
		var dt = new Date();
		var year = dt.getFullYear();
		var month = dt.getMonth()+1;
		if(month<10)
			month = "0"+""+month;
		return year+"-"+month;
	}
	
	var myAlert = function(msg){
   		$("#alertDialog .msg").html(msg);
   		$("#alertDialog").modal("show");
   	}

   	var myConfirm = function(msg,callback){
   		$("#confirmDialog .msg").html(msg);
   		$("#confirmDialog").modal("show");
   		$("#confirmDialog .submit").unbind("click").bind("click",function(){
   			if(callback)
   				callback();
   			$("#confirmDialog").modal("hide");
   		});
   	}

	var getShortName = function(fullName){
		return fullName.substr(fullName.lastIndexOf("/")+1,(fullName.length-fullName.lastIndexOf("/")-1));
		
	};

	var configBiz = {

		data : {
			id : 0,
			upId : "",
			upList :{},
			list:[]
		},

		init :function(){


			$(".cancel").click(function(){
				$(this).parents(".modal").modal("hide");
			});

			$("body").on("click",".file",function(){
				window.open($(this).data("file"));
			});


			this.initRow();
			this.initUpload();

			$("#btnSaveAll").click(function(){

				var checked = true;
				$("#list tr").each(function(e){

					var trid = $(this).attr("id");
					var d = {};
					d.openAccountId = $(this).find(".stationId").val();
					d.rewardAmount = $(this).find(".rewardMoney").val();
					d.rewardComment = $(this).find(".rewardReason").val();
					d.punishAmount = $(this).find(".punishMoney").val();
					d.punishComment = $(this).find(".punishReason").val();
					if(d.rewardAmount=="")
						d.rewardAmount = 0;
					if(d.punishAmount =="")
						d.punishAmount = 0;
					if(configBiz.data.upList[$(this).attr("id")]==undefined)
						configBiz.data.upList[$(this).attr("id")] = [];
					d.uploadFileString = JSON.stringify(configBiz.data.upList[$(this).attr("id")]);
					console.log(d);
					if(configBiz.data.upList[$(this).attr("id")].length==0){
						checked = "没有上传附件";
					}else if(configBiz.data.upList[$(this).attr("id")].length>5){
						checked = "最多只能上传5个附件";
					}
					if(d.rewardAmount==0&&d.punishAmount==0){
						checked = "奖励和惩罚金额必须填写一项";
					}
					if(d.punishAmount!=0&&d.punishComment==""){
						checked = "惩罚原因不能为空";
					}
					if(d.rewardAmount!=0&&d.rewardComment==""){
						checked = "奖励原因不能为空";
					}
					if(d.openAccountId==""){
						checked = "站点ID不能为空";
					}

					if(checked!==true){
						checked = "第"+(e+1)+"行"+checked;
						myAlert(checked);
						return false;
					}else{
						configBiz.data.list.push(d);	
					}

				});

				if(checked===true){

					$.ajax({
						url : "/settleAdjustment/save",
						type :"post",
						data : JSON.stringify(configBiz.data.list),
						contentType: "application/json; charset=utf-8",
						dataType :"json",
						success:function(data){
							configBiz.data.list = [];
							if(data.code==1){
								if(data.success==1){
									myAlert("保存成功");
									setTimeout(function(){
										window.location.reload();
									},2000);
								}else
									myAlert(data.msg);
							}else{
								myAlert(data.msg);
							}
						},
						error:function(){
							configBiz.data.list = [];
							myAlert(sysErrorMsg);
						}
					});

				}

			});
			
			$("#btnAddRow").trigger("click");
		},
		
		initRow:function(){

			$("#btnAddRow5").click(function(){
				$("#btnAddRow").trigger("click");
				$("#btnAddRow").trigger("click");
				$("#btnAddRow").trigger("click");
			});
			$("#btnAddRow").click(function(){

				if($("#list tr").length==20){
					$(".btnAddRow").hide();
				}else{
					configBiz.data.id++;
					var tmpl = '\
					<tr id ="col'+configBiz.data.id+'">\
	                <td>'+getMonth()+'</td>\
	                <td><input type="number" min="0" class="input-sm stationId inputNarrow" placeholder="输入ID"/></td>\
	                <td class="stationName">先输入ID</td>\
	                <td><input type="number" min="0" class="input-sm rewardMoney inputNarrow"/></td>\
	                <td><textarea rows="3" class="input-sm rewardReason"></textarea></td>\
	                <td><input type="number" min="0" class="input-sm punishMoney inputNarrow"/></td>\
	                <td><textarea rows="3" class="input-sm punishReason"></textarea></td>\
	                <td class="fileTd">\
	                    <p><button type="button" class="btn btn-default btn-sm upload">添加附件</button></p>\
	                </td>\
	                <td>\
	                    <button type="button" class="btn btn-info btn-sm delete">删除一行</button>\
	                </td>\
	            </tr>\
					';
					$("#list").append(tmpl);
					$("#btnSaveAll").show();
				}
			});


			$("#list").on("click",".delete",function(){
				var trid = $(this).parents("tr").attr("id");
				myConfirm("确定要删除这一行吗？",function(){
					$("#"+trid).remove();
					if($("#list tr").length==0){
						$("#btnSaveAll").hide();
					}
					if($("#list tr").length<20)
						$(".btnAddRow").show();

				});
				
			});


			$("#list").on("blur",".stationId",function(){
				var id = $(this).val();
				var trid = $(this).parents("tr").attr("id");
				if(id!="")
					configBiz.getStationName(id,trid);
			});

			$("#list").on("keydown",".stationId",function(e){
				if(e.keyCode==13){
					var id = $(this).val();
					var trid = $(this).parents("tr").attr("id");
					if(id!="")
						configBiz.getStationName(id,trid);
				}
			});

			$("#list").on("click","i",function(){
				var file = $(this).data("file");
				var trid = $(this).parents("tr").attr("id");
				if(configBiz.data.upList[trid]!=undefined){
					for(var i=0;i<configBiz.data.upList[trid].length;i++){
						if(configBiz.data.upList[trid][i].path==file){
							configBiz.data.upList[trid].splice(i,1);
							break;
						}
					}
					console.log(configBiz.data.upList);
					if(configBiz.data.upList[trid].length<5){
						$("#"+trid+" .upload").show();
					}
				}
				$(this).parents("p").remove();

				

			});

		},
		initUpload:function(){
			$("#upCtrl").change(function(){
				var upUrl = "/settleAdjustment/uploadFile";
				var formData = new FormData($("form")[0]);
				$.ajax({
					url : upUrl,
					type :"post",
					data : formData,
					processData: false,
    				contentType: false,
					dataType :"json",
					success : function(data){
						if(data.code==1){
							if(configBiz.data.upList[configBiz.data.upId]==undefined)
								configBiz.data.upList[configBiz.data.upId] = [];
							configBiz.data.upList[configBiz.data.upId].push(data.msg);
							$("#"+configBiz.data.upId+" .fileTd").prepend('<p><span class="file" data-file="'+data.msg.path+'">'+data.msg.name+'</span>&nbsp;<i data-file="'+data.msg.path+'" class="fa fa-trash-o fa-lg opration-icon"></i></p>');
						}else{
							myAlert(data.msg);
						}
						if(configBiz.data.upList[configBiz.data.upId].length==5){
							$("#"+configBiz.data.upId+" .upload").hide();
						}
						$("form")[0].reset();
					},
					error:function(){
						myAlert(sysErrorMsg);
						$("form")[0].reset();
					}
				});

			});

			$("#list").on("click",".upload",function(){
				configBiz.data.upId = $(this).parents("tr").attr("id");
				return $("#upCtrl").click();
			});


		},
		getStationName:function(id,trid){
			var url = "/settleAdjustment/getOrgName";
			$.ajax({
					url : url,
					type :"get",
					data : {orgId:id},
					dataType :"json",
					success : function(data){
						if(data.code==1){

							$.ajax({
								url:"/settleAdjustment/check",
								type:"get",
								data:{orgId:id,settleDate:getMonth().replace("-","")},
								dataType:"json",
								success:function(result){
									if(result.code==1)
										if(result.result===true){
											$("#"+trid+" .stationName").text(data.data);
										}else{
											myAlert("该加盟站当月调账申请已存在");
											$("#"+trid+" .stationName").text("先输入ID");
											$("#"+trid+" .stationId").val("");
										}
									else{
										myAlert(data.msg);
										$("#"+trid+" .stationName").text("先输入ID");
										$("#"+trid+" .stationId").val("");
									}
								},
								error:function(){
									myAlert(sysErrorMsg);
								}
							});

							
						}else{
							myAlert(data.msg);
							$("#"+trid+" .stationName").text("先输入ID");
							$("#"+trid+" .stationId").val("");
						}
					},
					error:function(){
						myAlert(sysErrorMsg);
					}
				});
		}

	};

	configBiz.init();

});