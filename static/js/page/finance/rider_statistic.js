  

  require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['page/finance/common'], function (common) {
    $(document).delegate(".js_submitbtn", "click", function() {
        var url = $(this).attr("actionurl");
        submit(url);
    })

    $(document).keypress(function(e) {
        if(e.which == 13) {
            submit();
        }
    });

    function validate() {
        var startTimeStr = $(".js_date_start").val();
        var endTimeStr = $(".js_date_end").val();
        var startTime = new Date(Date.parse(startTimeStr.replace(/-/g,   "/")));
        var endTime = new Date(Date.parse(endTimeStr.replace(/-/g,   "/")));

        if(endTime - startTime > 2*30*24*60*60*1000) {
            alert("日期间隔不能大于60天");
            return false;
        }
        if(startTime > endTime) {
            alert("开始时间不能晚于结束时间");
            return false;
        }

        return true;
    }


    function submit(url) {
        if(validate()) {
            if(url){
                $("#fm").attr("action",url);
            }
            $("#fm").submit();
        }
    }

});