require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/head-nav','page/grade/datepicker'], function (nav,datepicker) {

	var sysErrorMsg = "网络请求错误，请稍后重试";
	datepicker.set({ dateFmt: 'yyyy-MM', isShowToday: false, isShowClear: false});

	var myAlert = function(msg){
   		$("#alertDialog .msg").html(msg);
   		$("#alertDialog").modal("show");
   	}

   	var myConfirm = function(msg,callback){
   		$("#confirmDialog .msg").html(msg);
   		$("#confirmDialog").modal("show");
   		$("#confirmDialog .submit").unbind("click").bind("click",function(){
   			if(callback)
   				callback();
   			$("#confirmDialog").modal("hide");
   		});
   	}

	var getShortName = function(fullName){
		return fullName.substr(fullName.lastIndexOf("/")+1,(fullName.length-fullName.lastIndexOf("/")-1));
		
	};

	var listBiz = {

		data : {
			editId : 0,
			editData : {},
			list:{},
			page : 1
		},
		init : function(){
			//初始化月份
			var dt = new Date();
			var year = dt.getFullYear();
			var month = dt.getMonth()+1;
			if(month<10)
				month = "0"+""+month;
			$("#timeStr").val(year+"-"+month);

			if(roleType==0)
				$("#btnApply").show();

			this.getList(1);
			this.initDropDown();
			this.buildStation("-1");


			$("#btnSearch").click(function(){
				listBiz.getList(1);
			});

			$(".cancel").click(function(){
				$(this).parents(".modal").modal("hide");
			});

			$("#btnApply").click(function(){
				window.location.href = "/settleAdjustment/apply/";
			});

			$("body").on("click",".file",function(){
				window.open($(this).data("file"));
			});


			$("#list").on("click",".iconEdit",function(){
				var id = $(this).parents("tr").data("id");
				listBiz.data.editId = id;

				var d = listBiz.data.list[id];
				
				listBiz.data.editData = jQuery.extend(true, {}, d);
				$("#editDialog .month").text(d.settleDate);
				$("#editDialog .stationID").text(d.openAccountId);
				$("#editDialog .stationName").text(d.openAccountName);
				$("#editDialog .rewardMoney").val(d.rewardAmount);
				$("#editDialog .rewardReason").val(d.rewardComment);
				$("#editDialog .punishMoney").val(d.punishAmount);
				$("#editDialog .punishReason").val(d.punishComment);
				var html="";
				
				for(var i=0;i<d.uploadFileList.length;i++){
					html+='<div class="fileDiv"><span class="file" data-file="'+d.uploadFileList[i].path+'">'+d.uploadFileList[i].name+'</span>&nbsp;<i class="fa fa-trash-o fa-lg opration-icon" data-file="'+d.uploadFileList[i].path+'"></i></div>';
				}
				if(listBiz.data.editData.uploadFileList.length<5){
					$("#editDialog .upload").show();
				}else
					$("#editDialog .upload").hide();
				$("#editDialog .fileCon").html(html);
				$("#editDialog").modal();
			});

			$(".fileCon").on("click","i",function(){
				
				$(this).parents(".fileDiv").remove();
				
				var dFile = $(this).attr("data-file");
				//console.log($(this));
				for(var i=0;i<listBiz.data.editData.uploadFileList.length;i++){
					//console.log(dFile,listBiz.data.editData.uploadFileList[i].path);
					if(dFile == listBiz.data.editData.uploadFileList[i].path){
						listBiz.data.editData.uploadFileList.splice(i,1);
						break;
					}
				}
				//console.log(listBiz.data.editData);
				if(listBiz.data.editData.uploadFileList.length<5){
					$("#editDialog .upload").show();
				}
				//console.log(listBiz.data.editData);
			});


			$("#editDialog .submit").click(function(){

				listBiz.data.editData.rewardAmount = $("#editDialog .rewardMoney").val();
				listBiz.data.editData.rewardComment = $("#editDialog .rewardReason").val();
				listBiz.data.editData.punishAmount = $("#editDialog .punishMoney").val();
				listBiz.data.editData.punishComment = $("#editDialog .punishReason").val();
				if(listBiz.data.editData.rewardAmount=="")
					listBiz.data.editData.rewardAmount = 0;
				if(listBiz.data.editData.punishAmount=="")
					listBiz.data.editData.punishAmount = 0;

				var checked = true;
				console.log(listBiz.data.editData);
				if(listBiz.data.editData.uploadFileList.length==0){
					checked = "必须上传附件";
				}else if(listBiz.data.editData.uploadFileList.length>5){
					checked = "最多只能上传5个附件";
				}
				if(listBiz.data.editData.rewardAmount==0&&listBiz.data.editData.punishAmount==0){
					checked = "奖励和惩罚金额必须填写一项";
				}
				if(listBiz.data.editData.punishAmount!=0&&listBiz.data.editData.punishComment==""){
					checked = "惩罚原因必须填写";
				}
				if(listBiz.data.editData.rewardAmount!=0&&listBiz.data.editData.rewardComment==""){
					checked = "奖励原因必须填写";
				}
				if(checked!==true){
					myAlert(checked);
				}else{
					var SettleAdjustmentParam = {};
					SettleAdjustmentParam.id = listBiz.data.editId;
					SettleAdjustmentParam = listBiz.data.editData;
					SettleAdjustmentParam.uploadFileString = JSON.stringify(SettleAdjustmentParam.uploadFileList);
					//console.log(SettleAdjustmentParam);

					$.ajax({
						url : "/settleAdjustment/update",
						type :"post",
						data : SettleAdjustmentParam,
						dataType :"json",
						success : function(data){
							if(data.code==1){
								//console.log(data);
								$("#editDialog").modal("hide");
								myAlert(data.msg);
								listBiz.getList(listBiz.data.page);
							}else{
								myAlert(data.msg);	
							}
						},
						error:function(){
							myAlert(sysErrorMsg);
						}
					});	
				}

				

			});

			this.initUpload();
			this.initAudit();

			$("#pager").on("click","a",function(){
				listBiz.getList($(this).data("page"));
			});

			$("#confirmDialog .submit").click(function(){
				$("#confirmDialog").modal("hide");
			});

		},
		initAudit:function(){

			$("#list").on("click",".iconDetail",function(){
				window.location.href = "/settleAdjustment/list/opLog?id="+$(this).parents("tr").data("id");
			});

			$("#list").on("click",".iconOkay",function(){
				listBiz.data.editId = $(this).parents("tr").data("id");
				myConfirm("确认要通过审核吗？",function(){
					listBiz.doAudit(1);
				});
			});

			$("#list").on("click",".iconRefuse",function(){
				listBiz.data.editId = $(this).parents("tr").data("id");
				$("#refuseDialog").modal();

			});

			$("#refuseDialog .submit").click(function(){
				var comment = $("#refuseDialog textarea").val();
				if(comment!=""){
					listBiz.doAudit(0,comment);
				}
				else
					myAlert("驳回原因不能为空");
			});
		},
		doAudit:function(isOkay,comment){
			var postData = {
				adjustmentId: listBiz.data.editId,
				auditLevel: roleType,
				auditType: isOkay,
				comment: comment
			};
			$.ajax({
				url : "/settleAdjustment/audit",
				type :"post",
				data : postData,
				dataType :"json",
				success : function(data){
					if(data.code==1){
						$("#refuseDialog").modal("hide");
					}
					var msg = "驳回成功";
					if(isOkay)
						msg = "审核成功";
					myAlert(msg);
					listBiz.getList(listBiz.data.page);
				},
				error:function(){
					myAlert(sysErrorMsg);
				}
			});
		},
		initUpload:function(){

			$(".upload").click(function(){
				return $("#upCtrl").click();
			});
			$("#upCtrl").change(function(){
				
				var upUrl = "/settleAdjustment/uploadFile";
				var formData = new FormData($("form")[0]);
				$.ajax({
					url : upUrl,
					type :"post",
					data : formData,
					processData: false,
    				contentType: false,
					dataType :"json",
					success : function(data){
						if(data.code==1){
							var fileData = data.msg;
							listBiz.data.editData.uploadFileList.push(fileData);
							$("#editDialog .fileCon").append('<div class="fileDiv"><span class="file" data-file="'+fileData.path+'">'+fileData.name+'</span>&nbsp;<i class="fa fa-trash-o fa-lg opration-icon" data-file="'+fileData.path+'"></i></div>');
						}else{
							myAlert(data.msg);
						}
						if(listBiz.data.editData.uploadFileList.length==5){
							$("#editDialog .upload").hide();
						}
						$("form")[0].reset();
					},	
					error:function(){
						myAlert(sysErrorMsg);
						$("form")[0].reset();
					}
				});

			});
		},
		initDropDown:function(){
			$.ajax({
				url : "/settleAdjustment/selector/agent",
				type :"get",
				data : {},
				dataType :"json",
				success : function(data){
					if(data.code==1){
						var tmpl = ['<option value="-1">全部</option>'];
						for(var i=0;i<data.data.length;i++){
							var d = data.data[i];
							tmpl.push("<option value='"+d.orgId+"'>"+d.orgName+"("+d.orgId+")</option>");
						}
						$("#agentId").append(tmpl.join("")).select2();

						$("#agentId").change(function(){
							listBiz.buildStation($("#agentId").val());
						});

					}else{
						
					}
				},
				error:function(){
					myAlert(sysErrorMsg);
				}
			});
		},
		buildStation:function(agentId){
			$.ajax({
				url : "/settleAdjustment/selector/station",
				type :"get",
				data : {agentId:agentId},
				dataType :"json",
				success : function(data){
					if(data.code==1){
						var tmpl = ["<option value='-1'>全部</option>"];
						for(var i=0;i<data.data.length;i++){
							var d = data.data[i];
							tmpl.push("<option value='"+d.orgId+"'>"+d.orgName+"("+d.orgId+")</option>");
						}
						$("#stationId").html(tmpl.join("")).select2();

					}else{
						
					}
				},
				error:function(){
					myAlert(sysErrorMsg);
				}
			});
		},
		getList:function(page){
			listBiz.data.page = page;
			$("#list").html('<tr><td colspan="10">加载中…… </td></tr>');
			var buildPage = function(currentPage,pageTotal){
				var pageHtml = '<li><a data-page="'+(currentPage-1==0?currentPage:(currentPage-1))+'">上一页</a></li>';
				var getPageHtml = function(i){
					return '<li '+(currentPage==i?'class = "active"':'')+'><a data-page="'+i+'">'+i+'</a></li>';
				}
				var preHtml = "";
				for(var i = currentPage; i >= (currentPage-5>1?(currentPage-5):1); i--){
					preHtml = getPageHtml(i)+preHtml;
				}
				pageHtml = pageHtml + preHtml;
				for(var i=currentPage+1; i<= (currentPage+5>pageTotal?pageTotal:(currentPage+5)); i++){
					pageHtml+= getPageHtml(i);
				}
				pageHtml += '<li><a data-page="'+(currentPage+1>pageTotal?pageTotal:(currentPage+1))+'">下一页</a></li>';
				$("#pager").html(pageHtml);

			};

			var buildUploadList = function(arr){
				var html = "";
				for(var i=0;i<arr.length;i++){
					html+="<span class='file' data-file='"+arr[i].path+"'>"+arr[i].name+"</span><br/>";
				}
				return html;
			};

			var url = "/settleAdjustment/apply/search";
			if(roleType!=0)
				url = "/settleAdjustment/audit/search";
			var filterData = {
				pageNum:page,
				pageSize:20,
				agentId : $("#agentId").val(),
				stationId : $("#stationId").val(),
				settleDate : $("#timeStr").val().replace("-",""),
				status: $("#status").val()
			};
			var checkRole = function(status){
				if(status==10&&roleType==1){
					return true;
				}
				if(status==20&&roleType==2){
					return true;
				}
				if(status==30&&roleType==3){
					return true;
				}
				return false;
			};
			
			$.ajax({
				url : url,
				type :"get",
				data : filterData,
				dataType :"json",
				success : function(data){
					if(data.code==1){
						var tmpl = [];
						for(var i=0;i<data.data.length;i++){
							var d = data.data[i];
							try{
								d.uploadFileList = JSON.parse(d.uploadFileString);
							}catch(e){
								d.uploadFileList = [];
							}

							listBiz.data.list[d.id] = d;
							tmpl.push("<tr data-id='"+d.id+"'>");
							tmpl.push('<td>'+d.settleDate+'</td>');
							tmpl.push('<td>'+d.openAccountName+'</td>');
							tmpl.push('<td>'+d.openAccountId+'</td>');
							tmpl.push('<td>'+d.rewardAmount+'</td>');
							tmpl.push('<td>'+d.rewardComment+'</td>');
							tmpl.push('<td>'+d.punishAmount+'</td>');
							tmpl.push('<td>'+d.punishComment+'</td>');
							tmpl.push('<td>'+buildUploadList(d.uploadFileList)+'</td>');
							if(roleType!=0){
								tmpl.push('<td>'+d.applicantName+'</td>')
							}else{
								$(".applictionTh").remove();
							}
							tmpl.push('<td>'+d.statusShowName+'</td>');
							tmpl.push('<td>');
							tmpl.push('<a class="iconDetail" data-toggle="tooltip" title="审核记录" data-placement="top"><i class="fa fa-info-circle fa-lg opration-icon"></i></a>');
							if(roleType>=0){
								if(roleType==0&&(d.status==10||d.status==50))
									tmpl.push('<a class="iconEdit" data-toggle="tooltip" title="编辑" data-placement="top"><i class="fa fa-edit fa-lg opration-icon"></i></a>');
								
								if(checkRole(d.status)&&d.status<40){
				            		tmpl.push('<a class="iconOkay" data-toggle="tooltip" title="通过" data-placement="top"><i class="fa fa-thumbs-up fa-lg opration-icon"></i></a>');
					        		tmpl.push('<a class="iconRefuse" data-toggle="tooltip" title="驳回" data-placement="top"><i class="fa fa-thumbs-down fa-lg opration-icon"></i></a>');
					        	}
					        }
			            	tmpl.push("</td></tr>");
						}
						$("#list").html(tmpl.join(""));
						$("[data-toggle='tooltip']").tooltip();
						
						$("#countLable").text("共"+data.totalCount+"项");
						var pageCount = Math.ceil(data.totalCount/data.pageSize);
						buildPage(page,pageCount);

					}else{
						myAlert(data.msg);
					}
				},
				error:function(){
					myAlert(sysErrorMsg);
				}
			});
		}

	};
	
	listBiz.init();


});