/*
 *	@liupeidong
 */
require.config({
  	baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
});
require(['module/root', 'lib/artTemplate'], function(root, template) {
	console.log('test');
	var bm = {
		_init: function() {
			var me = this;
			$('#orgSelect').select2({
				width: 150
			});
			me._bind();
			me._ajax();
		},
		_bind: function() {
			var me = this;
			$('#orgSelect').on('change', function() {
				var orgId = $(this).val();
				var orgName = $(this).find('option:selected').text();
				$('#orgName').text(orgName);
				me._ajax();
			});
			$('body').on('click', '.msg_list .unread', function() {
				var count = parseInt($('#unreadCount').text());
				$('#unreadCount').text(count - 1);
				$(this).removeClass('unread');
				$(this).find('.msg_icon').remove();
			})
		},
		_ajax: function() {
			var me = this;
			var orgId = $('#orgSelect').find('option:selected').val();
			$.ajax({
				url: '/admin/getHomeData?curOrgId=' + orgId + '&modules=' + window.bm_modules,
				type: 'GET',
				data: {},
				success: function(res) {
					console.log(res);
					if (res.code == 0) {
						if (/message/.test(window.bm_modules)) {
							mMsg._init(res.data.message);
						}else {
							$('#mMsg').remove()
						}
						if (/auditmsg/.test(window.bm_modules)) {
							mAudit._init(res.data.auditmsg);
						}
						if (/bizprofile/.test(window.bm_modules)) {
							mBiz._init(res.data.bizprofile);
						}else {
							$('#mBiz').remove();
						}
						if (/weather/.test(window.bm_modules)){
							mWea._init(res.data.weather);
						}else {
							$('#mWeather').remove();
						}
						if (/exam/.test(window.bm_modules)) {

							EXAM._init(res.data.exam);
						}else{
							$('#exam').remove();
						}
						if (/monitor/.test(window.bm_modules)){
							mMon._init(res.data.monitor);
							var interval = setInterval(function() {
								var orgId = $('#orgSelect').find('option:selected').val();
								$.ajax({
									url: '/admin/getHomeData?curOrgId=' + orgId + '&modules=monitor',
									type: 'GET',
									data: {},
									success: function(res) {
										if (res.code == 0) {
											mMon._init(res.data.monitor);
										}else {
											alert(res.msg);
										}
									}
								})
							}, 60000)
						}else {
							$('#mMonitor').remove();
						}
					}else {
						alert(res.msg);
					}
				}
			})
		}
	}

	var examTpl = '<div id="exam">'
				+ '<span>有<span class="abnormal_count red_color">' + '{{data.count}}' + '</span>个考试</span>'
				+ '<a class="pull-right" href="/train/examinee">去考试&nbsp;&gt;</a>';
				+'</div>';

	var EXAM = {
		_init: function(exam){

			$('#exam').remove();
			var data = exam;
			if (data && data.code == 0 && data.data && data.data.count > 0) {
				var render = template.compile(examTpl);
				var html = render(data);
				$('#mMsg .module_title').after(html);
			};

		}
	}

	var mMsg = {					// module message
		_init: function(message) {
			if (message.code == 0) {
				var html = template('J_msg_tpl', message.data);
				$('#msgBox').html(html)
			}else {
				var html = '<p class="green" style="margin-bottom: 5px;">有<span id="unreadCount" class="red big">0</span>条未读通知 <a class="module_link green" href="/msg/r/list?type=1"><small>去阅读<i class="fa fa-chevron-right"></i></small></a></p>';
				$('#msgBox').html(html);
			}
		}
	}

	var tpl = '<div id="auditMsg">'
				+ '<p class="section-title">审核消息</p>'
				+ '{{each msgArr as item}}'
				+ '<div class="message-container">'
					+'<div class="message-header">'
						+'<span class="msg-title">{{item.title}}</span>'
						+'<span class="msg-count">未读<span class="msg-unread red_color">{{item.unreadCount}}</span>/{{item.count}}<i class="fa fa-angle-right"></i></span>'
					+'</div>'
					+'<div class="message-list">'
					+'<ul class="message-list-wrapper">'
					+'{{each item.notices as notice}}'
					+'<li class="msg_item">'
						+'<a class="msg_link {{notice.read == 0 ? \'unread\' : \'\'}}" href="{{notice.url}}" target="_blank" data-id="{{notice.msgId}}">'
							+'{{if notice.read == 0}}'
								+'<span class="msg_icon">未读</span>'
							+'{{/if}}'
							+'<p class="msg_title">{{notice.title}}</p>'
							+'<small class="msg_time">{{notice.time}}</small>'
						+'</a>'
					+'</li>'
					+'{{/each}}'
					+'</ul>'
					+'</div>'
					+'{{if item.notices.length > 6}}'
					+'<div class="message-footer">'
					+'<span class="show_pre hidden">后退<i class="fa fa-angle-up"></i></span><span class="show_more">更多<i class="fa fa-angle-down"></i></span>'
					+'</div>'
					+'{{/if}}'
				+ '</div>'
				+ '{{/each}}'
				+ '</div>'

	var mAudit = {
		msg: {},
		index1: 0,
		index2: 0,
		_init: function(msg) {
			var me = this;
			if (msg.code != 0) {
				return false;
			}
			msg.msgArr = [];
			for (var k in msg.data) {
				if (msg.data[k].code == 0 && msg.data[k].count > 0) {
					msg.msgArr.push(msg.data[k])
				}
			}
			me.msg = msg;

			var render = template.compile(tpl);
			var html = render(msg);
			$('#auditMsg').remove();
			$('#mMsg').append(html);

			me._bind();
		},
		_sort: function() {
			var me = this;
			var notices = me.msg.msgArr[me.index1].notices;
			var tempUnread = [],
				tempRead = [];
			for (var i = 0, n = notices.length; i < n; i ++) {
				var notice = notices[i];
				if (notice.read == 1) {
					tempRead.push(notice);
				}else {
					tempUnread.push(notice);
				}
			}
			me.msg.msgArr[me.index1].notices = tempUnread.concat(tempRead);

			var render = template.compile(tpl);
			var html = render(me.msg);
			$('#auditMsg').remove();
			$('#mMsg').append(html);
		},
		_bind: function() {
			var me = this;
			$('body').on('click', '.message-header', function() {
				var $container = $(this).parent('.message-container');
				$container.siblings('.message-container').removeClass('message-open');
				$(this).siblings('.message-list').find('.message-list-wrapper').css('top', '0');
				if ($container.hasClass('message-open')) {
					$container.removeClass('message-open');
					me._sort();
				}else {
					$container.addClass('message-open');
				}
			});
			$('body').on('click', '.message-list .unread', function() {
				var id = $(this).data('id');
				$(this).find('.msg_icon').remove();
				var $unread = $(this).parents('.message-container').find('.msg-unread');
				var unread = parseInt($unread.text());
				$unread.text(--unread);
				$.get('/admin/msgRead?idList=' + id, function(res) {
					console.info('消息' + id + '已读');
				});
				me.index2 = $(this).parent().index();
				me.index1 = $(this).parents('.message-container').index() - 1;
				me.msg.msgArr[me.index1].unreadCount --;
				me.msg.msgArr[me.index1].notices[me.index2].read = 1;
				// me._sort();
			})
			$('body').on('click', '.show_more', function() {
				var $list = $(this).parent().siblings('.message-list').find('.message-list-wrapper');
				var length = $list.children('li').length;
				var h = $list.height();
				var t = Math.abs(parseInt($list.css('top')));
				var x = h - (t + 240) > 240 ? 240 : h - (t + 240);
				$list.css('top', -1 * (x + t) + 'px');

				$(this).siblings('.show_pre').removeClass('hidden');
				if (x <= 240) {
					$(this).addClass('hidden');
				}else {
					$(this).removeClass('hidden')
				}
			})
			$('body').on('click', '.show_pre', function() {
				var $list = $(this).parent().siblings('.message-list').find('.message-list-wrapper');
				var length = $list.children('li').length;
				var h = $list.height();
				var t = Math.abs(parseInt($list.css('top')));
				var x = t > 240 ? 240 : t;
				$list.css('top', -1 * (t - x) + 'px');

				$(this).siblings('.show_more').removeClass('hidden');
				if (x <= 240) {
					$(this).addClass('hidden');
				}else {
					$(this).removeClass('hidden');
				}
			})
		}
	}

	var mBiz = {
		_init: function(bizprofile) {
			if (bizprofile.code == 0) {
				var profile = bizprofile.data;
				for (var i = 0, n = profile.list.length; i < n; i ++) {
					var k = profile.list[i];
					// 比较值的格式化
					if (k.compare == 0) {
						k.compareIcon = 'hide';
					}else{
						// 超时单率特殊判断
						k.compareIcon = k.compare > 0 ? 'fa-long-arrow-up' : 'fa-long-arrow-down';
						k.compare = Math.abs(k.compare) + '%';
						if (k.name == '超时单率')  {
							k.compareIcon += ' color';
						}
					}
					// 昨日数据的格式化
					if ((k.name == '订单完成率' || k.name == '超时单率')) {
						k.yesterday = k.yesterday + '%';
					}
					if (k.yesterday > 10000) {
						k.yesterday = (k.yesterday / 10000).toFixed(2) + '<small style="font-size: 14px;">万</small>';
					}

				}
				var html = template('J_biz_tpl', bizprofile.data);
				$('#mBiz').html(html);
			}else {
				var html = '<p style="text-align: center;font-size: 16px;margin-top: 20px;">暂无数据</p>'
				$('#mBiz').html(html);
			}
		}
	}

	var mWea = {
		_init: function(weather) {
			if (weather.code == 0) {
				if (weather.data.length == 1) {
					var html = template('J_we_single_tpl', weather.data[0]);
					$('#weBox').html(html)
				}else if (weather.data.length > 1) {
					var data = {
						date: fnFormatDate(weather.data[0].weatherDate),
						week: weather.data[0].dayOfWeek,
						list: weather.data
					}
					var html = template('J_we_multi_tpl', data);
					$('#weBox').html(html)
				}
			}else {
				var html = template('J_we_none_tpl', {});
				$('#weBox').html(html);
			}
			$('.wi').each(function() {
				var weather = $(this).data('we');
				$(this).addClass(fnWeatherIcon(weather));
			})
			var now = new Date();
			if (now.getHours() > 18 && now.getHours() < 6) {
				$('.wi_day').parent('td').hide();
			}else {
				$('.wi_night').parent('td').hide();
			}
		}
	}

	var chartDefault = {
		colors: ['#00abe4'],
		title: {									// 图表名称
			text: null
		},
		credits: {									// 版权信息
			enabled: false,
		},
		chart: {
			backgroundColor: '#f5f7f9',
			spacing: [5,10,0,5],
		},
		legend: {
			enabled: false,
		},
		xAxis: {
			tickmarkPlacement: 'on',
			labels: {
				enabled: false,
			},
			tickLength: 0,
			categories: []
		},
	}
	var mMon = {
		_init: function(monitor) {
			var me = this;
			me._format(monitor.data);
		},
		_format: function(charts) {
			var chart1 = {
				yAxis: {
					title: null,
					floor: 0,
					tickAmount: 2,
					opposite: true,
					labels: {
						enabled: false
					}
				},
				series: [{
					name: '人均负载',
					data: []
				}]
			}
			var chart2 = {
				yAxis: {
					title: null,
					floor: 0,
					tickAmount: 2,
					opposite: true,
					labels: {
						enabled: false
					}
				},
				series: [{
					name: '手工派单数量',
					data: []
				}]
			}
			var chart3 = {
				yAxis: {
					title: null,
					floor: 0,
					ceiling: 100,
					tickAmount: 2,
					opposite: true,
					labels: {
						enabled: false,
						format: '{value}%'
					}
				},
				series: [{
					name: '超时单率',
					data: [],
					tooltip: {
						valueSuffix: '%'
					}
				}]
			}
			var chart4 = {
				yAxis: {
					title: null,
					floor: 0,
					ceiling: 100,
					tickAmount: 2,
					opposite: true,
					labels: {
						enabled: false,
						format: '{value}%'
					}
				},
				series: [{
					name: '开工骑手比例',
					data: [],
					tooltip: {
						valueSuffix: '%'
					}
				}]
			}

			for (var i = 0, n = charts.length; i < n; i ++) {
				chartDefault.xAxis.categories.push(charts[i].name);
				chart1.series[0].data.push(charts[i].avgRiderLoad);
				chart2.series[0].data.push(charts[i].needAssignedWaybillCount);
				chart3.series[0].data.push(charts[i].timeoutWaybillPercent);
				chart4.series[0].data.push(charts[i].workingRiderPercent);
			};
			var oChart1 = $.extend({}, chartDefault, chart1);
			$('#chart1').highcharts(oChart1);
			var oChart2 = $.extend({}, chartDefault, chart2);
			$('#chart2').highcharts(oChart2);
			var oChart3 = $.extend({}, chartDefault, chart3);
			$('#chart3').highcharts(oChart3);
			var oChart4 = $.extend({}, chartDefault, chart4);
			$('#chart4').highcharts(oChart4);

		}
	}

	bm._init();

	$('body').on('mouseenter', '.tool_tip_hover', function(e) {
		var text = $(this).data('title');
		var tooltip = document.createElement('span');
		tooltip.className = 'custom_tooltip';
		tooltip.innerHTML = text;
		var x = e.clientX,
			y = e.clientY;
		tooltip.style.left = x + 'px';
		tooltip.style.top = y + 'px';
		$(this).append($(tooltip))
	});
	$('body').on('mouseleave', '.tool_tip_hover', function(e) {
		$(this).find('.custom_tooltip').remove()
	});

	function fnFormatDate(str) {
		var arr = str.split('-');
		return arr[1] + '月' + arr[2] + '日';
	}

	function fnWeatherIcon(weather) {
		var weatherIcons = {
			"晴": "wi-day-sunny",
			// 雾和云
			"薄雾": "wi-cloudy", "雾": "wi-cloudy", "晴间多云": "wi-cloudy",
			"多云": "wi-cloudy", "少云": "wi-cloudy",
			// 阴天有白天和晚上的区别
			"阴": "wi-day-haze",
			// 小风
			"有风": "wi-windy", "平静": "wi-windy",
			"微风": "wi-windy", "和风": "wi-windy", "清风": "wi-windy",
			// 大风
			"强风/劲风": "wi-strong-wind", "疾风": "wi-strong-wind", "大风": "wi-strong-wind", "烈风": "wi-strong-wind",
			"风暴": "wi-strong-wind",
			// 飓风
			"狂爆风": "wi-hurricane", "飓风": "wi-hurricane", "热带风暴": "wi-hurricane",
			// 龙卷风
			"龙卷风": "wi-tornado",
			// 小雨
			"阵雨": "wi-raindrops", "雷阵雨": "wi-raindrops",
			"小雨": "wi-raindrops", "毛毛雨/细雨": "wi-raindrops","中雨": "wi-raindrops",
			// 大雨
			"强阵雨": "wi-raindrop","大雨": "wi-raindrop",
			"强雷阵雨": "wi-raindrop",
			"极端降雨": "wi-raindrop", "特大暴雨": "wi-raindrop",
			// 冷雨
			"冻雨": "wi-snow-wind",
			"雷阵雨伴有冰雹": "wi-snow-wind",
			"雨夹雪": "wi-snow-wind", "雨雪天气": "wi-snow-wind", "阵雨夹雪": "wi-snow-wind",
			// 雪
			"小雪": "wi-snowflake-cold", "中雪": "wi-snowflake-cold", "大雪": "wi-snowflake-cold",
			"阵雪": "wi-snowflake-cold",
			// 灰
			"霾": "wi-dust", "扬沙": "wi-dust", "浮尘": "wi-dust",
			"火山灰": "wi-dust", "沙尘暴": "wi-dust", "强沙尘暴": "wi-dust",
			// 具体的
			"热": "wi-hot",
			"冷": "wi-thermometer-exterior",
			"na": "wi-na"
		}
		return weatherIcons[weather];
	}
})