define(function() {
	var json = {
	    "data": {
	        "weather": {//天气
	            "data": [
					// 如果是多个城市
	                {
	                    "cityId": 120100,
	                    "cityName": "天津",
	                    "dayOfweek": "星期三",
	                    "weatherDate": "2015-11-07",
	                    "weatherDays": [
							{
	                            maxTemperature: 12, //最高温度
	                            minTemperature: 7, //最低温度
	                            weatherDaytime: "中雨",//白天天气
	                            weatherNight: "中雨"//晚上天气
	                        },
	                        {
	                            maxTemperature: 10,
	                            minTemperature: 7,
	                            weatherDaytime: "小雨",
	                            weatherNight: "小雨"
	                        }
						],
	                },
	                {
	                    "cityId": 120100,
	                    "cityName": "天津",
	                    "dayOfweek": "星期三",
	                    "weatherDate": "2015-11-07",
	                    "weatherDays": [
							{
	                            maxTemperature: 12, //最高温度
	                            minTemperature: 7, //最低温度
	                            weatherDaytime: "中雨",//白天天气
	                            weatherNight: "中雨"//晚上天气
	                        },
	                        {
	                            maxTemperature: 10,
	                            minTemperature: 7,
	                            weatherDaytime: "小雨",
	                            weatherNight: "小雨"
	                        }
						],
	                },
	                {
	                    "cityId": 120100,
	                    "cityName": "天津",
	                    "dayOfweek": "星期三",
	                    "weatherDate": "2015-11-07",
	                    "weatherDays": [
							{
	                            maxTemperature: 12, //最高温度
	                            minTemperature: 7, //最低温度
	                            weatherDaytime: "中雨",//白天天气
	                            weatherNight: "中雨"//晚上天气
	                        },
	                        {
	                            maxTemperature: 10,
	                            minTemperature: 7,
	                            weatherDaytime: "小雨",
	                            weatherNight: "小雨"
	                        }
						],
	                },

	            ],
	            "code": 0,
	            "msg": "OK"
	        },
	 
	        "monitor": {//监控模块
	            "data": [
	                {
	                    name: "外卖线下测试站点",//站点名称
	                    avgRiderLoad: 5,//骑手人效
	                    needAssignedWaybillCount: 50,//手工派单数量
	                    timeoutWaybillPercent: 1.5,//超时单率
	                    workingRiderPercent: 80//开工骑手比例
	                },
	                {
	                    name: "中关村配送站12",
	                    avgRiderLoad: 8,
	                    needAssignedWaybillCount: 60,
	                    timeoutWaybillPercent: 1.8,
	                    workingRiderPercent: 60
	                },
	                {
	                    name: "中关村配",
	                    avgRiderLoad: 12,
	                    needAssignedWaybillCount: 70,
	                    timeoutWaybillPercent: 1.3,
	                    workingRiderPercent: 90
	                },
	                {
	                    name: "中关村配送站",
	                    avgRiderLoad: 6,
	                    needAssignedWaybillCount: 40,
	                    timeoutWaybillPercent: 2.8,
	                    workingRiderPercent: 75
	                }
	            ],
	            "code": 0,  // 参考下面的返回码
	            "msg": "OK"
	        },
	        "bizprofile": {  //运营数据
	            data:{
					list: [{
						name: '骑手人效',
	                    yesterday:23,//昨天人效
	                    lastWeek:34.33,//上周均值.  前端目前用不到
	                    compare: -3//比较结果. 比上个月低时为负值
	                },
	                {
						name: '订单完成量',
	                    yesterday:999,
	                    lastWeek:449,
	                    compare:50,
	                },
	                {
						name: '超时单率',
	                    yesterday:9,
	                    lastWeek:44,
	                    compare:50,
	                },
	                {
						name: '完成单率',
	                    yesterday:9,
	                    lastWeek:49,
	                    compare:50,
	                }],
	                redirect:1//控制跳转  1可以 0:不能跳转
	            },
	            "code": 0,  // 参考下面的返回码
	            "msg": "OK"
	        },
	        "message": {
	            "data": {
	                "notices": [
	                    {
	                        "time": "2015/12/09", //时间
	                        "title": "title111", //标题
	                        "msgId": 231, //消息ID
	                        "read": 0, //是否阅读
	                        "url": "/msg/r/detail?id=231"//消息连接地址
	                    },
	                    {
	                        "time": "2015/12/08",
	                        "title": "title222",
	                        "msgId": 225,
	                        "read": 1,
	                        "url": "/msg/r/detail?id=225"
	                    }
	                ],
	                "unreadCount": 12 //未读
	            },
	            "code": 0,
	            "msg": "OK"
	        }
	    },
	    "code": 0,
	    "msg": "OK"
	}
	return json;
})