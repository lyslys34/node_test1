require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/root', 'module/validator','module/cookie','lib/moment-with-locales.min'], function (t, validator,cookie,moment) {
  var pageNum=1;
  var pageSize=10;
  var replyed=0;
  var  detailDia;

     var session_replyed= sessionStorage.getItem("replyed");
      if(session_replyed!=null){
       replyed=session_replyed;

 }

 $("#mobile").val(sessionStorage.getItem("mobile"));
 console.log(replyed);
 if(replyed==0){
   $("#not_reply_tab").parent("li").css("border","1px solid #ddd");
   $("#not_reply_tab").parent("li").css("border-top-right-radius","5px ");
   $("#not_reply_tab").parent("li").css("border-top-left-radius","5px ");
   $(".add_reply").removeClass("disabled");
   $(".add_reply").addClass("active");
   $(".not_reply").show();
   $(".replyed").hide();
   $("#no_wrapper").css("cursor","no-drop");
   $("#yes_wrapper").css("cursor","pointer");
   $("#replyed").val("0");
 }
  else{
      $("#reply_tab").parent("li").css("border","1px solid #ddd");
      $("#reply_tab").parent("li").css("border-top-right-radius","5px ");
      $("#reply_tab").parent("li").css("border-top-left-radius","5px ");
      $(".add_reply").removeClass("active");
      $(".add_reply").addClass("disabled");
      $(".not_reply").hide();
      $(".replyed").show();
      $("#yes_wrapper").css("cursor","no-drop");
      $("#no_wrapper").css("cursor","pointer");
      $("#replyed").val("1");
  }



  $(document).on("keydown", function(e)
  {
  console.log(e, 123, e.keyCode);
      if(e.keyCode==13)
    {
        console.log("key");
      if($("#mobile").is(":focus")){
               $("#mobile-button").click();
           }
    }
  }
);


  $("#not_reply_tab").on("click",function(){
       if(replyed==0){
       return ;
       }
       else{
       sessionStorage.setItem("mobile","");
       reply=0;
       pageNum=1;
       pageSize=10;
       sessionStorage.setItem("replyed","0");
       $("#pageNo").val("1");
       $("#pageSize").val("10");
       $("#replyed").val("0");
       $("#mobileContent").val("");
       var $my_form=$("#complain_form");
       $my_form.submit();
       }
  });

  $("#reply_tab").on("click",function(){
           if(replyed==1){return ;}
           else{
               sessionStorage.setItem("mobile","");
               replyed=1;
               pageNum=1;
               pageSize=10;
               sessionStorage.setItem("replyed","1");
                $("#pageNo").val("1");
                $("#pageSize").val("10");
                $("#replyed").val("1");
                $("#mobileContent").val("");
              var $my_form=$("#complain_form");
              $my_form.submit();
           }

  });

  $(".add_reply").on("click",function(e){
        $("#reply_modal").modal();
        $("#add_reply_btn").data('waybillId', $(e.target).attr("waybillId"));
        $("#add_reply_btn").data("complain_id",$(e.target).attr("complain_id"));
  });
  var detailRequest;
    function showWaybillDetail(waybillId)
    {

        if (detailDia) {
            detailDia.remove();
            detailDia = null;
        };
        if (detailRequest) {
          detailRequest.abort();
        };
        var body = '<div id="detailCon">这是ID＝' + waybillId+ '的订单详情</div>';
        detailRequest = $.ajax({
             url: "/partner/waybill/detailV2.ajax?waybillId="+ waybillId,
             success: function(result){

                body = '<div>';
                body = body + result + '</div>';
                detailDia = t.ui.showModalDialog({
                    title: '订单详情', body: body, buttons: [],
                    style:{
                        ".modal-footer":{"display":"none"},
                        ".modal-title":{"display":"none"},
                        ".modal-dialog":{"width":"900px"},
                        ".modal-body":{"padding-top":"0"},
                        ".modal-header .close":{"margin-right": "-8px","margin-top": "-10px"},
                        ".modal-header":{"border-bottom": "none"}
                        }
                });
                detailDia.on("hidden.bs.modal", function(){
                    this.remove();
                    detailDia = null;
                });
            },
            error:function(){
            }});
    }
    //手机号查询事件
  $("#mobile-button").on("click",function(e){
    sessionStorage.setItem("mobile","");
    var mobile=$("#mobile").val();
    if(mobile.length>0){
    var telReg = !!mobile.match(/^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/);
    //如果手机号码不能通过验证
    if(telReg == false){
     alert("请输入正确的手机号码");
     return ;
    }
    }
     var replyed=sessionStorage.getItem("replyed");
     $("#pageNo").val("1");
     $("#pageSize").val("10");
     $("#replyed").val(replyed);
     $("#mobileContent").val(mobile);
     sessionStorage.setItem("mobile",$("#mobileContent").val());
     var $my_form=$("#complain_form");
     $my_form.submit();
  })

  $(".waybill_detail").on("click",function(e){
   var waybill_id=$(e.target).attr("waybill_id");
   if(waybill_id==null){
   return ;
   }
   else{
   showWaybillDetail(waybill_id);
   }

  });
  $("#add_reply_btn").on("click",function(e){
        console.log("sdfasd");
       var content=$("#reply_content").val();
       if(content.length==0){
        alert("请输入回复内容");
        return ;
       }

       var remark=$("#reply_remark").val();
       var waybillId=$("#add_reply_btn").data("waybillId");
       var complainId=$("#add_reply_btn").data("complain_id");
      console.log(complainId+"----");
       $.ajax({
          url:"/complain/addReply",
          type:"POST",
          dataType:'json',
          data:{
              waybill_id:waybillId,
              content:content,
              remark:remark,
              complain_id:complainId
          },
          success: function() {
            console.log('success')
          }

       })
         .always(function(){
              var pageNum=$(".clearfix .active a").html();
              var pageSize=10;
              var mobile=sessionStorage.getItem("mobile");
              if(mobile==null||mobile==undefined){
                mobile="";
              }
               setTimeout(function () {
                 window.location.assign("/complain/getComplainListByParam?replyed=0&pageNo="+pageNum+"&pageSize=10&mobile="+mobile);
                  }, 500);
              })
         });

});