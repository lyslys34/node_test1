/**
 * Created by lixiangyang on 15/11/30.
 */

var citySelect = $("#orgCity");

$(document).ready(function () {

    $(".js_uploadbtn").click(function () {
        $(this).siblings(".js_fileupload").click();
    });

    $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
        citySelect.select2({
            matcher: oldMatcher(matchPinyin)
        });
    });

    $(document).keypress(function(e) {
        if(e.which == 13) {
            submit();
        }
    });

    var uploadOption = {
        url: "/file/imgupload",
        add: function (e, data) {
            var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
            var maxFileSize = 5242880;
            if (data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                showTip("文件格式不正确(gif,jpg,png)", 2);
                return false;
            }
            if (data.originalFiles[0]['size'] && data.originalFiles[0]['size'] > maxFileSize) {
                showTip("文件太大(5M)", 2);
                return false;
            }
            data.context = $(this).siblings(".js_uploadbtn").addClass("disabled").text("正在上传。。。");
            data.submit();
        },
        done: function (e, data) {
            $(this).siblings(".js_uploadbtn").removeClass("disabled").text("重新上传");

            if (data.result) {
                if (data.result.code == 0) {
                    if (data.result.data[0] && data.result.data[0].code == 0) {
                        var backUrl = data.result.data[0].url;
                        if (typeof backUrl == undefined || backUrl == null || backUrl == '') {
                            showTip("上传失败，获取图片链接失败", 2);
                            return false;
                        }
                        $(this).siblings("img").css("display", "block").attr("src", backUrl);
                        $(this).siblings(".js_upload_back_url").val(backUrl);
                    } else {
                        showTip("上传失败," + (data.result.data[0] ? data.result.data[0].msg : "未知错误"), 2);
                    }
                } else {
                    showTip("上传失败，" + data.result.msg, 2);
                }
            } else {
                showTip("上传失败", 2);
            }
        }
    };

    function submit() {
        var orgName = $("#orgName").val();
        var orgCity = $("#orgCity").val();
        var isProxy = $("#isProxy").val();
        var emergencyContact = $("#emergencyContact").val();
        var emergencyPhone = $("#emergencyPhone").val();
        var teamSize = $("#teamSize").val();
        var workingTime = $("#workingTime").val();
        var experienceType = $("input[name='experienceType']:checked").val();
        var hasInsurance = $("input[name='hasInsurance']:checked").val();
        var foodLicenseNumber = $("#foodLicenseNumber").val();
        var foodLicenseFile = $("#foodLicenseFile").val();


        if(isBlank(orgName)) {
            showTip("请输入正确的总账号名称", 2);
            return false;
        }
        if(isBlank(emergencyContact)) {
            showTip("请输入正确的紧急联系人", 2);
            return false;
        }
        if(isBlank(emergencyPhone) || !isMobile(emergencyPhone)) {
            showTip("请输入正确的紧急联系人电话", 2);
            return false;
        }
        if(isBlank(teamSize) || !isInteger(teamSize)) {
            showTip("请输入正确的团队规模", 2);
            return false;
        }
        if(isBlank(workingTime) || !isInteger(workingTime)) {
            showTip("请输入正确的从业时长", 2);
            return false;
        }
        if(isBlank(hasInsurance)) {
            showTip("请选择是否有骑手商业意外险", 2);
            return false;
        }


        var data = {}
        data.orgName = orgName;
        data.cityId = orgCity;
        data.isProxy = isProxy;
        data.emergencyContact = emergencyContact;
        data.emergencyPhone = emergencyPhone;
        data.teamSize = teamSize;
        data.workingTime = workingTime;
        data.experienceType = experienceType;
        data.hasInsurance = hasInsurance;
        data.foodLicenseNumber = foodLicenseNumber;
        data.foodLicenseFile = foodLicenseFile;

        _showPop({
            type:1,
            content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + "确认创建总账号：“" + orgName + "“？</div>",
            ok: {
                display: true,
                name: "确定",
                callback: function(){
                    unbindSubmit();
                    $.ajax({
                        dataType: 'json',
                        type : 'post',
                        url : "/headfranchisee/create",
                        data : data,
                        success : function(r){
                            if (r) {
                                if(r.code == 0) {
                                    if(r.data && r.data.baseData && r.data.baseData.id) {
                                        alert("创建成功！点击确定去创建总加盟商负责人");
                                        window.location.href="/rider/goCreateHeadFranchiseeCharge?orgId=" + r.data.baseData.id;
                                    } else {
                                        alert("创建总账号成功,请在人员管理中添加总账号负责人");
                                        window.location.href="/";
                                    }
                                } else {
                                    alert(r.msg);
                                    bindSubmit();
                                }
                            } else {
                                alert("添加总账号失败!");
                                bindSubmit();
                            }
                        },
                        error: function() {
                            alert("添加总账号失败!网络异常！");
                            bindSubmit();
                        }
                    });
                }
            }
        });
    }

    function showTip(content, type) {
        _showPop({
            type:type,
            content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + content + "</div>"
        });
    }

    function bindSubmit() {
        $("#js_save_and_next").on("click", function() {
            submit();
        }).removeAttr("disabled");
    }

    function unbindSubmit() {
        $("#js_save_and_next").off("click").attr("disabled", "disabled");
    }

    $("#js_save_and_next").on("click", function() {
        submit();
    });

    $(".js_fileupload").fileupload(uploadOption);

    $(".js_img_click_open").click(function(){
        window.open($(this).attr("src"));
    });
});



