require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});

require(['module/root', 'module/validator',  'module/cookie'], function (t, validator, cookie) {

$(document).ready(function(){
    initBtn();
    initLimits();
    initAddFranchiseeBtn();

});

function initBtn() {
    $("#showSuccessMsg").on('hide.bs.modal', function () {
        location.reload(); 
    });

    $("#successSubmit").click(function() {
        $("#showSuccessMsg").modal('hide');
    });

    $("#addFranchiseeBtn").click(function() {
        $("#addFranchiseeModal").modal();
    });

    $("#cancelAddFranchiseeBtn").click(function() {
    	$("#franchisee_ids").val("");
        $("#addFranchiseeModal").modal('hide');
    });

    $(".unbindfranchisee_link").click(function() {  //解绑入口
        var headFranchiseeId = $("#parentId").val();
        if(headFranchiseeId == "") {
          showError("无法获取当前总加盟商信息");
          return false;
        }

        var franchiseeId = $(this).attr('rel');
        if(franchiseeId == "") {
          showError("无法获取子加盟商信息");
          return false;
        }

        $("#unBindHeadFranchiseeId").val(headFranchiseeId);
        $("#unBindFranchiseeId").val(franchiseeId);
        $("#unBindTipsModal").modal();
    });

    $('#cancelunBind').on('click', function() {
        $("#unBindHeadFranchiseeId").val('');
        $("#unBindFranchiseeId").val('');
        $("#unBindTipsModal").modal('hide');
    });

    $('#unbindCloseButton').on('click', function() {
        $("#unBindHeadFranchiseeId").val('');
        $("#unBindFranchiseeId").val('');
        $("#unBindTipsModal").modal('hide');
    });

    $('#unBindResubmit').on('click', function() {
        var unBindHeadFranchiseeId = $("#unBindHeadFranchiseeId").val();
        var unBindFranchiseeId = $("#unBindFranchiseeId").val();
        $("#unBindFranchiseeId").val(0);
        $("#unBindFranchiseeId").val(0);
        $("#unBindTipsModal").modal('hide');
        $.ajax({
             dataType: 'json',
             type : 'post',
             url : "/headfranchisee/unbindHeadFranchisee",
             data: {
                   headFranchiseeId: unBindHeadFranchiseeId,
                 franchiseeId: unBindFranchiseeId
             },
             success : function(data){
                 if(data.success){
                     $("#unBindResMsg").html("<h4>解绑成功</h4>");
                     $("#showUnbindRes").modal();
                     setTimeout(function(){
                        $("#showUnbindRes").modal("hide");
                        window.location.href = "/headfranchisee/franchiseelist?parentId="+unBindHeadFranchiseeId+"&f=join_org";
                      },2000);
                 }else {
                     $("#unBindResMsg").html("<h4>解绑失败</h4>");
                     setTimeout(function(){
                        $("#showUnbindRes").modal();
                    }, 2000);
                 }
             },
             error:function(XMLHttpRequest ,errMsg){
                 alert("网络连接失败");
             }
        });
    });
}

function initLimits() {
    $('textarea').keyup(function(){
        var value = $(this).val();
        $(this).val(value.replace(/[^\d\n]/g,''));
    });

    $(".check_num").keyup(function(){
        var value = $(this).val();
        $(this).val(value.replace(/[^\d]/g,''));
    });
}

function initAddFranchiseeBtn() {
    $("#addFranchiseeSubmit").click(function() {
        var ids = $("#franchisee_ids").val();
        var id = $("#parentId").val();
        var idList = ids.split("\n");
        var b = true;

        if(validator.isBlank(id)) {
            showAddFranchiseeError("无法获取当前总加盟商信息");
            return false;
        }

        $.each(idList, function(n,value) {
            if(value !='' && isNaN(value)) {
                showAddFranchiseeError("每一个id都必须为数字");
                b = false;
                return false;
            }
        });

        if(!b) {
            return false;
        }
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/headfranchisee/bindHeadFranchisee",
            data: {
                headFranchiseeId : id,
                franchiseeIds : ids
            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4  class='text-center text-danger'>"+data.result.resultMsg+"</h4>");
                    $("#addFranchiseeModal").modal('hide');
                    $("#showSuccessMsg").modal();
                }else {
                    showAddFranchiseeError(data.errMsg);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });   
    });
}

function showError(errMsg) {
    $("#alert_error").empty();
    $("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

function showAddFranchiseeError(errMsg) {
    $("#alert_add_franchisee_error").empty();
    $("#alert_add_franchisee_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

function showBindAreaError(errMsg) {
    $("#alert_bind_area_error").empty();
    $("#alert_bind_area_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

});
