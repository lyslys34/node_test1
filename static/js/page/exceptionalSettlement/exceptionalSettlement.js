require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});


require(['page/finance/common'], function (common) {

    //点击页码
    $(document).delegate('a[name=pageCode]', 'click', function () {
        var page = $.trim($(this).text());
        window.location.href = "/partner/exceptionalSettlement?currentPage=" + page + "&" + getQueryStr();
    });

    //点击上一页
    $(document).delegate('#prev', 'click', function () {
        if ($(this).parent().hasClass('disabled')) {
            return false;
        }
        var page = parseInt($.trim($(this).attr('data-page'))) - 1;
        window.location.href = "/partner/exceptionalSettlement?currentPage=" + page + "&" + getQueryStr();
    });

    //点击下一页
    $(document).delegate('#next', 'click', function () {
        if ($(this).parent().hasClass('disabled')) {
            return false;
        }
        var page = parseInt($.trim($(this).attr('data-page'))) + 1;
        window.location.href = "/partner/exceptionalSettlement?currentPage=" + page + "&" + getQueryStr();
    });

    ////点击查看详情
    //$(document).delegate('a[name=poiDetail]', 'click', function () {
    //    var poiId = $.trim($(this).attr('data-poiId'));
    //    window.open("/exceptionalSettlement?poiId=" + poiId + "&startDate=" + $('#query-start-date').val() + "&endDate=" + $('#query-end-date').val() + "&type=" + $('#poiType option:selected').val());
    //});

    $(document).delegate('#query-commit', 'click', function () {
        var startDate = common.str2Date($('#query-start-date').val());
        var endDate = common.str2Date($('#query-end-date').val());
        if (startDate > endDate) {
            alert("开始时间必须大于结束时间");
            return false;
        }
        if ((endDate - startDate) / 1000 / 24 / 60 / 60 > 59) {
            alert("时间间隔不能超过60天");
            return false;
        }
        var date = common.str2Date("2015-05-15");
        if (startDate < date || endDate < date) {
            alert("只能查询5月15日之后的数据");
            return false;
        }

        window.location.href = "/partner/exceptionalSettlement?" + getQueryStr();
    });

    function getQueryStr() {
        var startDate = $('#query-start-date').val();
        var endDate = $('#query-end-date').val();
        var type = $('#exceptionalType option:selected').val();
        var str = "startDate=" + startDate + "&endDate=" + endDate + "&exceptionalType=" + type;;
        return str;
    }

    //导出表格
    $(document).delegate('a[name=excelReport]', 'click', function () {
        window.location.href = "/partner/exceptionalSettlement/excelReport?" + getQueryStr();
    });

});
