require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/root','module/ui'], function (t,ui) {
	initSkipButton();
	function initSkipButton(){
    	$("#skip").click(function(){
    		showConfirm("后期可使用“结算设置”功能填写信息",function(){
    			window.location.href="/org/stations";
    		});
    	});
    }

    function showConfirm(msg,okFunc){
        var body = $('<div style="height: 100px;padding: 15px;text-align: center;">'+msg+'</div>');
        ui.showModalDialog({
            body: body, buttons: [
                ui.createDialogButton('ok','知道了',function(){
                    okFunc();
                })
            ],
            style:{".modal-dialog":{"padding":"100px 110px"},
                   ".modal-footer":{"padding":"10px"},
                   ".btn-default":{"font-size":"12px"}
            }
        });
    }
});