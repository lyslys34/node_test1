var $province =  $("#province");
var $riderName = $("#riderName");
var $city = $("#city");
var $bank = $("#bank");
var $branchId = $("#branchId");
$(document).ready(function(){
	initProvinceOption();
	provinceChange();
	cityChange();
	initBankOption();
	bankChange();
	initSaveButton();
	initInput();
});

function initSaveButton() {
	$("#save_button").click(function() {
        var branchSub = $("#branchId").children('option:selected').text();
        $("#bankSub").val(branchSub);
        if (verifyData()) {
            //$("#data_form").submit();
			$('#save_button').attr('disabled', 'disabled');
			$('#data_form').ajaxSubmit({
				url:$("#data_form").attr('action'),
				dataType:'json',
				complete:function(data){
					if(data.statusText=="OK"){
						$('#save_button').removeAttr('disabled');
						if(data.responseJSON.success == true) {
							if (from == 'sc') {
								window.location.href = "/org/stations";
								return false;
							}
							if ('join_org_charge' == from) {
								window.location.href = "/org/join/orgList";
								return false;
							}
							$('#failBtn').css('display','none');
							$('#reloadBtn').css('display','inline-block');
							$('#myModal p').text(data.responseJSON.message.substring(data.responseJSON.message.indexOf('|')+1));
							$('#myModal').modal('show');
							//code:-1 0 1 2 3
						}else if(data.responseJSON.code == 403){
							$('#failBtn').css('display','inline-block');
							$('#reloadBtn').css('display','none');
							$('#myModal p').text('银行卡正在校验中，无法再次修改，请校验完成后再修改保存');
							$('#myModal').modal('show');
						}else{
							$('#myModal p').text(data.responseJSON.message);
							$('#reloadBtn').css('display','none');
							$('#failBtn').css('display','inline-block');
							$('#myModal').modal('show');
						}
					}
					else{
						var msg = '保存修改失败，请稍后重试！';
						if(data.responseJSON.message!=undefined && data.responseJSON.message!=null) {
							msg = msg + data.responseJSON.message;
						}
						$('#myModal p').text(msg);
						$('#reloadBtn').css('display','none');
						$('#failBtn').css('display','inline-block');
						$('#myModal').modal('show');
					}
				},
				error:function(){
					$('#myModal p').text('网络连接失败，请稍后重试！');
					$('#reloadBtn').css('display','none');
					$('#failBtn').css('display','inline-block');
					$('#myModal').modal('show');
				}
			});
        }
	});
}

function verifyData() {
	clearWarning();
	var type = $("#type").val();
	if (type == 1){
		var contractFiles = $("#contractFiles").val();
		var originContractPic = $("#originContractPic").val();
		if ((originContractPic == null || originContractPic =="") && (contractFiles == null || contractFiles == "")) {
			displayWarningMsg("contractFiles", "请上传合同照片");
			$('#contractFiles').focus();
			return false;
		}

		var contractCode = $('#contractCode').val();
		if (contractCode==null || contractCode=="") {
			displayWarningMsg("contractCode", "请填写合同编号");
			$('#contractCode').focus();
			return false;
		} else if (contractCode.length > 50) {
			displayWarningMsg("contractCode", "合同编号太长，请重新填写!");
			$('#contractCode').focus();
			return false;
		}

		var licenceCode = $('#licenceCode').val();
		if (licenceCode==null || licenceCode=="") {
			displayWarningMsg("licenceCode", "请填写营业执照编号");
			$('#licenceCode').focus();
			return false;
		}else if (licenceCode.length > 50) {
			displayWarningMsg("contractCode", "营业执照编号太长，请重新填写!");
			$('#licenceCode').focus();
			return false;
		}

		var contractCode = $('#contractCode').val();
		if (contractCode==null || contractCode=="") {
			alert("请填写合同编号");
			return false;
		}

		var licenceCode = $('#licenceCode').val();
		if (licenceCode==null || licenceCode=="") {
			alert("请填写营业执照编号");
			return false;
		}
	}
	var file = $("#file").val();
	var originPic = $("#originPic").val();
	if ((originPic == null || originPic =="") && (file == null || file == "")) {
		if (type == 1){
			displayWarningMsg("originPic", "请上传营业执照照片");
			$('#file').focus();
			displayWarning("originPic");
			return false;
		}else{
			//displayWarningMsg("originPic", "请上传身份证照片");
		}
	}
	var minMoney = $.trim($("#minMoney").val());
	if (minMoney == null || minMoney == "") {
		displayWarningMsg("minMoney", "请填写最低结算金额");
		$('#minMoney').focus();
		return false;
	}
	if ($("input:radio[name='cardType']:checked").length == 0) {
		alert("请选择公私");
		return false;
	}
	var province = $("#province").children('option:selected').val();
	if (province == null || province == "" || province == 0) {
		displayWarningMsg("location", "请选择银行卡所在省");
		return false;
	}
	var city = $("#city").children('option:selected').val();
	if (city == null || city == "" || city == 0) {
		displayWarningMsg("location", "请选择银行卡所在市");
		return false;
	}
	var bank = $("#bank").children('option:selected').val();
	if (bank == null || bank == "" || bank == 0) {
		displayWarningMsg("bank", "请选择开户银行");
		return false;
	}
	var branchId = $("#branchId").children('option:selected').val();
	if (branchId == null || branchId == "" || branchId == 0) {
		displayWarningMsg("bank", "请选择开户支行");
		return false;
	}
	var cardName = $.trim($("#cardName").val());
	if (cardName == null || cardName == "") {
		displayWarningMsg("cardName", "请填写开户名");
		return false;
	}else if (cardName.length > 30) {
		displayWarningMsg("cardName", "开户名太长，请重新填写!");
		$('#cardName').focus();
		return false;
	}
	var cardNo = $.trim($("#cardNo").val());
	if (cardNo == null || cardNo == "") {
		displayWarningMsg("cardNo", "请填写银行卡号");
		return false;
	}else if (cardNo.length > 30) {
		displayWarningMsg("cardNo", "银行卡号太长，请重新填写!");
		$('#cardNo').focus();
		return false;
	}
	return true;
}

function initProvinceOption() {
    $.ajax({
        dataType: 'json',
        type : 'post',
        url : "/payment/province",
        success : function(data){
            if(data.success){
            	var provinces = data.provinces;
            	for (var i in provinces) {
                	$("#province").append("<option value =" + provinces[i].id +">" + provinces[i].name + "</option>");
            	}
                var provinceInit = $("#provinceInit").val();
            	$("#province").find("option[value='" + provinceInit + "']").attr("selected",true);
            	$province.select2();
            	initCityOption();
            }else {
                alert(data.success);
            }
        },
        error:function(XMLHttpRequest ,errMsg){
            alert("网络连接失败");
        }
    });
    
}

function initRiderOption() {
    var orgId = $("#orgId").val();
    alert("111");
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/payment/rider",
            data: {
                orgId: orgId
            },
            success : function(data){
                if(data.success){
                    var riders = data.riders;
                    for (var i in riders) {
                        $("#riderName").append("<option value =" + riders[i].id +">" + riders[i].name + "</option>");
                    }
                    var riderNameInit = $("#riderNameInit").val();
                    $("#riderName").find("option[value='" + riderNameInit + "']").attr("selected",true);
                    $riderName.select2();
                    alert("111");
                }else {
                    alert(data.success);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
                alert("2222");
            }
        });
}

function provinceChange() {
    $('#province').change(function(){
    	initCityOption();
    });
}

function initCityOption() {
    var provinceId = $("#province").children('option:selected').val();
    $("#city").empty();
    $.ajax({
        dataType: 'json',
        type : 'post',
        url : "/payment/city",
        data: {
        	provinceId: provinceId
        },
        success : function(data){
            if(data.success){
            	var citys = data.citys;
            	$("#city").append("<option value = '0'></option>");
            	for (var i in citys) {
                	$("#city").append("<option value =" + citys[i].cityId +">" + citys[i].cityName + "</option>");
            	}
            	var cityInit = $("#cityInit").val();
            	$("#city").find("option[value='" + cityInit + "']").attr("selected",true);
				if ( cityInit > 0 && $("#city").children('option:selected').val()==cityInit ) {
					$('#locationWarning').css('display','none');
				}
            	$city.select2();
	            initBankBranchOption();
            }else {
                alert(data.success);
            }
        },
        error:function(XMLHttpRequest ,errMsg){
            alert("网络连接失败");
        }
    });
}

function initBankOption() {
	$.ajax({
        dataType: 'json',
        type : 'post',
        url : "/payment/bank",
        success : function(data){
            if(data.success){
            	var banks = data.banks;
            	for (var i in banks) {
                	$("#bank").append("<option value =" + banks[i].id +">" + banks[i].name + "</option>");
            	}
            	var bankInit = $("#bankInit").val();
            	$("#bank").find("option[value='" + bankInit + "']").attr("selected",true);
            	$bank.select2();
            }else {
                alert(data.success);
            }
        },
        error:function(XMLHttpRequest ,errMsg){
            alert("网络连接失败");
        }
    });
}

function cityChange() {
    $('#city').change(function(){
	    initBankBranchOption();
    });
}

function bankChange() {
    $('#bank').change(function(){
	    initBankBranchOption();
    });
}

function initBankBranchOption() {
    var bankId = $("#bank").children('option:selected').val();
    var cityId = $('#city').children('option:selected').val();
    $("#branchId").empty();
    $.ajax({
        dataType: 'json',
        type : 'post',
        url : "/payment/bankbranch",
        data: {
        	cityId: cityId,
        	bankId: bankId
        },
        success : function(data){
            if(data.success){
            	var branchs = data.branchs;
            	$("#branchId").append("<option value = '0'></option>");
            	for (var i in branchs) {
                	$("#branchId").append("<option value =" + branchs[i].id +">" + branchs[i].name + "</option>");
            	}
            	var bankbranchInit = $("#bankbranchInit").val();
            	//兼容久数据branchId为code的情况
            	if (bankbranchInit.length == 12) {
            		for (var i in branchs) {
                    	if (branchs[i].code == bankbranchInit) {
                    		bankbranchInit = branchs[i].id;
                    		break;
                    	}
                	}
            	}
            	$("#branchId").find("option[value='" + bankbranchInit + "']").attr("selected",true);
				if ( bankbranchInit > 0 && $("#branchId").children('option:selected').val()==bankbranchInit ) {
					$('#bankWarning').css('display','none');
				}
            	$branchId.select2();
            }else {
                alert(data.success);
            }
        },
        error:function(XMLHttpRequest ,errMsg){
            alert("网络连接失败");
        }
    });
}

function initInput(){
	/*所有input框的事件绑定*/
	$('.warning').prev().children('input').change(function(){
		if( $(this).val() == null || $(this).val() == ''){
			var msg = $(this).parent().prev('label').html();
			msg = '请填写' + msg.substring(0, msg.length-1);
			displayWarningMsg($(this).attr('id'), msg);
		}else if( ($(this).attr('id')=='contractCode' || $(this).attr('id')=='licenceCode') && $(this).val().length > 50){
			var msg = $(this).parent().prev('label').html();
			msg = msg.substring(0, msg.length-1) + '太长，请重新填写！';
			displayWarningMsg($(this).attr('id'), msg);
		}else if( ($(this).attr('id')=='cardName' || $(this).attr('id')=='cardNo') && $(this).val().length > 30){
			var msg = $(this).parent().prev('label').html();
			msg = msg.substring(0, msg.length-1) + '太长，请重新填写！';
			displayWarningMsg($(this).attr('id'), msg);
		}else{
			hideWarning($(this).attr('id'));
		}
	});

	$('#minMoney').change(function(){
		if( $(this).val() == null || $(this).val() == ''){
			displayWarningMsg('minMoney', '请填写最低结算金额');
		}else{
			hideWarning('minMoney');
		}
	});

	/*省市select的事件绑定*/
	$('#province, #city').change(function(){
		if ($('#city').val() == null || $('#city').val() == "" || $('#city').val() == 0) {
			displayWarningMsg("location", "请选择银行卡所在省市");
		}else{
			$('#locationWarning').css('display','none');
		}
	});

	/*银行开户行的select的事件绑定*/
	$('#bank, #branchId').change(function(){
		if ($('#branchId').val() == null || $('#branchId').val() == "" || $('#branchId').val() == 0) {
			displayWarningMsg("bank", "请选择银行卡开户银行和支行");
		}else{
			$('#bankWarning').css('display','none');
		}
	});

	$('#failBtn').click(function(){
		$('#myModal').modal('hide');
		$('#save_button').removeAttr('disabled');
	});
}

/*
 * 使id对应的错误提示可见并显示对应文案
 * @param id
 * @param msg
 */
function displayWarningMsg(id, msg){
	$('#' + id + 'Warning').html(msg);
	$('#' + id + 'Warning').css('display','block');
	$('#' + id).focus();
}

/*
 * 使id对应的错误提示元素可见
 */
function displayWarning(id){
	$('#' + id + 'Warning').css('display','block');
	$('#' + id).focus();
}

function hideWarning(id){
	$('#' + id + 'Warning').css('display','none');
}

/*
 * 清空所有的错误提示
 */
function clearWarning(){
	$('.warning').css('display','none');
	$('.warningPic').css('display','none');
	$('.warningSelect').css('display','none');
}