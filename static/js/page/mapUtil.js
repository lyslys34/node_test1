define([], function() {

    var order_line_colors = [];
    order_line_colors[0] = "#ff0000";
    order_line_colors[1] = "#0000ff";
    order_line_colors[2] = "#ff00ff";
    order_line_colors[3] = "#009f3c";
    order_line_colors[4] = "#6c000a";
    order_line_colors[5] = "#e3007b";
    order_line_colors[6] = "#ffff00";
    order_line_colors[7] = "#df0024";
    order_line_colors[8] = "#3d107b";
    order_line_colors[9] = "#990050";

    return {

        getArcLineColor: function(index) {

            var _index = Math.abs(index) % 10;
            return order_line_colors[_index];
        },
        getControlPoint: function(startXY, endXY) {

            var x = (startXY.getX() + endXY.getX()) / 2;
            var y = (startXY.getY() + endXY.getY()) / 2;
            var xdis = endXY.getX() - startXY.getX();
            var ydis = endXY.getY() - startXY.getY();

            var random = Math.floor(Math.random() * (10 - 0 + 1) + 0);
            if (random > 5) {
                x = x + (ydis) * 0.3;
                y = y - (xdis) * 0.3;
            } else {
                x = x - (ydis) * 0.3;
                y = y + (xdis) * 0.3;
            }

            return {
                x: x,
                y: y
            };
        },
        drawCurve: function(sPos, ePos, lineColor) {

            var controlPos = this.getControlPoint(sPos, ePos);
            var canvas = document.getElementById('bm_canvas_forarc');
            if (canvas && canvas.getContext) {

                var ctx = canvas.getContext('2d');

                ctx.beginPath();
                ctx.lineWidth = 2;
                ctx.strokeStyle = lineColor;

                ctx.moveTo(sPos.getX(), sPos.getY());
                ctx.quadraticCurveTo(controlPos.x, controlPos.y, ePos.getX(), ePos.getY());
                ctx.stroke();
            }
        },
        getSendMarkerContent: function(color, text) {
            var markerContent = document.createElement("div");
            markerContent.className = "bm_marker_container";

            //点标记中的图标
            var markerImg = document.createElement("img");
            markerImg.src = '/static/imgs/map-point-shadow.png';
            markerContent.appendChild(markerImg);

            //点标记中的文本
            var markerSpan = document.createElement("div");
            markerSpan.className = "bm_css_marker";
            if (color) {
                markerSpan.style.backgroundColor = color;
            };
            markerContent.appendChild(markerSpan);

            var markerText = document.createElement("div");
            markerText.className = 'bm_marker_text';
            markerText.innerHTML = '送';
            if (text) {
                markerText.innerHTML = text;
            };
            markerContent.appendChild(markerText);

            return markerContent;
        },
        clearCanvas: function(){

            var canvas = $('#bm_canvas_forarc');
            if (canvas.length > 0) {
                var width = canvas.innerWidth();
                var height = canvas.innerHeight();
                canvas = canvas[0];
                if (canvas.getContext) {
                    var ctx = canvas.getContext('2d');
                    ctx.clearRect(0, 0, width, height);
                }
            };
        },
        createCanvasIfNeeded: function(){
            var amapLayer = $('.amap-layers');
            if (amapLayer.length < 1) {
                return;
            };
            var canvas = $('#bm_canvas_forarc');
            if (canvas.length > 0) {
                return;
            };
            var width = amapLayer.innerWidth();
            var height = amapLayer.innerHeight();
            var body = '<canvas id="bm_canvas_back" width="' + width + '" height="' + height + '"' + 'style="position:absolute;top:0;left:0; z-index:99;background-color:black;opacity:0.3;"></canvas>';
            body += '<canvas id="bm_canvas_forarc" width="' + width + '" height="' + height + '"' + 'style="position:absolute;top:0;left:0; z-index:100;"></canvas>';
            amapLayer.append(body);
        },
        createCanvasForArc: function() {
            var amapLayer = $('.amap-layers');
            if (amapLayer.length < 1) {
                return;
            };
            var canvas = $('#bm_canvas_forarc');
            if (canvas.length > 0) {
                canvas.remove();
            };
            canvas = $('#bm_canvas_back');
            if (canvas.length > 0) {
                canvas.remove();
            };
            var width = amapLayer.innerWidth();
            var height = amapLayer.innerHeight();
            var body = '<canvas id="bm_canvas_back" width="' + width + '" height="' + height + '"' + 'style="position:absolute;top:0;left:0; z-index:99;background-color:black;opacity:0.3;"></canvas>';
            body += '<canvas id="bm_canvas_forarc" width="' + width + '" height="' + height + '"' + 'style="position:absolute;top:0;left:0; z-index:100;"></canvas>';
            amapLayer.append(body);
        },
        removeCanvas:function() {
            var canvas = $('#bm_canvas_forarc');
            if (canvas.length > 0) {
                canvas.remove();
            };
            canvas = $('#bm_canvas_back');
            if (canvas.length > 0) {
                canvas.remove();
            };
        }
    }
});