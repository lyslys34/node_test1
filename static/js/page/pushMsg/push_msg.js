require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/root'], function(t){

    function validate() {
        var orgId = $('#orgId').val();
        if ($.trim(orgId) === "") {
            alert("组织ID不能为空");
            return false;
        }
        var title = $('#title').val();
        if ($.trim(title) === "") {
            alert("消息标题不能为空");
            return false;
        }
        return true;
    }

    $('#orgId').keyup(function(){
        $(this).val($(this).val().replace(/[^0-9]/g,''));
    });


    $('#orgId').bind('input',function(){
        $(this).val($(this).val().replace(/[^0-9]/g,''));
    });


    $(document).delegate('#send', 'click', function(){
        if ($(this).hasClass('disabled')) {
            return false;
        }
        var bool = validate();
        if (bool == false) {
            return false;
        }
        $('#send').addClass('disabled');
        var orgId = $('#orgId').val();
        var title = $('#title').val();
        var postData = {
            orgId: orgId,
            title: title
        };
        var url = "/pushMsg/push";
        t.utils.post(url, postData, function (r) {
            if (r.httpSuccess && r.data.code == 0) {
                $('#send').removeClass('disabled');
                alert('发送消息成功');
                $(this).removeClass('disabled');
            } else {
                $('#send').removeClass('disabled');
                alert('发送消息出现错误');
                $(this).removeClass('disabled');
            }
        });
    });
});