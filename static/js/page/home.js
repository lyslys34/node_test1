/**
 * 斑马配送项目A端后台的的样式调整
 *
 * @date 2015-04-08 星期三
 * @author XU Kai(xukai@meituan.com)
 *
 */

require.config({
  baseUrl: MT.STATIC_ROOT + '/js',
  urlArgs: 'ver=' + pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});

define(['module/utils', 'module/cookie', 'module/ui', 'module/head-nav'], function(utils, cookie, ui) {
  'use strict';

  $('[data-toggle="tooltip"]').tooltip();

  //要求IE用户下载Chrome浏览器
  ieNotification();

  //初次进入显示通知信息
  showNotic();
  var msgPull = self.setInterval(showNotic,1000*60);
  showReplyCount();

  //增加桌面通知的显示
  var msgUrl = (url_config.admin?url_config.admin:'http://peisong.meituan.com') + '/msg/r/msgForPromptBox.ajax';
  showMsgAtDesktop();
  setInterval(showMsgAtDesktop,1000*10*60);
  function showMsgAtDesktop(){
    $.ajax({
      url:msgUrl,
      type:'get',
      dataType:'jsonp',
      jsonp:'callback',
      success:function(data){
        callback(data);
      },
      fail:function(err){
        callback(void 0);
      }
    })
  }

  function callback(data){
    if(!!data){
      data.forEach(function(item,index){
        var title = '【您有1条未读通知】\n'+ item.msgTitle+' 查看详情>>';
        var op = {
            tag: item.msgId,
        };
        var noti = utils.showDesktopNotification(title,op);
        noti.onclick = function(){
          window.open(url_config.admin+'/msg/r/detail?id='+this.tag,'_blank')
        }
      })
    }
  }

  function showNotic() {
    var read = cookie.getCookie("read");
    if (read == 1) {
      return false;
    }

    var url = "/msg/r/latest.ajax";

    utils.post(url, {}, function(r) {
      if (r.httpSuccess) {
        var data = r.data;
        if (data.code == 0) {
          $("#notice-bar").remove();
          var page = data.data;
          if (page != null && page.bmUserMsgViewList != null && page.bmUserMsgViewList.length > 0) {
            var msgList = page.bmUserMsgViewList;
            var bodyHtml = '<div id="notice-bar"><table class="table" style="background-color:#fdf8dd;margin-bottom:0"><tr><td style="width: 87%;text-align: left;padding-top:10px;padding-bottom:9px;"><img src="/static/imgs/icon.png" style="width: 18px;margin-right:10px;">';
            var detailUrl = "/msg/r/detail?id=";
            $.each(msgList, function(i, msg) {
              var title = msg.msgTitle;
              var publishTime = msg.createTime * 1000;
              var publishDate = new Date(publishTime);
              var month = publishDate.getMonth() + 1;
              var day = publishDate.getDate();
              bodyHtml = bodyHtml + '<span style="padding-right:30px;"><a href="' + detailUrl + msg.msgId + '" target="_blank" title="' + title + '">' + (title.length > 20 ? (title.substring(0, 20) + "...") : title) + '(' + month + '月' + day + '日)</a></span>';
            });
            bodyHtml = bodyHtml + '</td><td style="padding-top:10px;padding-bottom:9px;"><span style="padding-right:30px;"><a href="/msg/r/list">查看全部>></a></span><button  style="margin-top:-5px;" type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button></td></tr></table></div>';
            var body = $(bodyHtml);
            $("#topholder").after(body);
            $("#wrapper").addClass('with-notice');
            $("button", body).click(function() {
              body.remove();
              $("#wrapper").removeClass('with-notice');
              cookie.setSessionCookie("read", 1);
            });
          }
        }
      }
    });
  }

  function ieNotification() {
    if (!utils.ieDetection()) {
      return ;
    }

    if ($(window).width() > 1024) {
      $('.ie-notification').removeClass('hide');
    }

    $(window).resize(function() {
      if ($(window).width() > 1024 && $('.ie-notification').hasClass('hide')) {
        return $('.ie-notification').removeClass('hide');
      }
      if ($(window).width() < 1024 && !$('.ie-notification').hasClass('hide')) {
        $('.ie-notification').addClass('hide');
      }
    });


    if (window.localStorage.getItem('ieNotification')) {
      return ;
    }

    var ieNotificationTime = window.localStorage.getItem('ieNotificationTime');
    if (ieNotificationTime) {
      // 点击以后再说以后,24小时内不会有弹窗提醒
      if (ieNotificationTime < +new Date()) {
        return window.localStorage.setItem('ieNotification_time', '');;
      }
      return ;
    }
    window.localStorage.setItem('ieNotification', 'true');
    var dialog = ui.showModalDialog({
      body: '<p style="font-size: 16px;color: #333;line-height: 35px;">建议您使用<span style="color: #00abe4;">chrome</span>浏览器</p>' +
      '<p style="font-size: 14px;color: #666;">避免烽火台部分功能无法使用</p>' +
      '<i class="close-dialog"></i>',
      buttons: [
        ui.createDialogButton('close', '以后再说', function() {
          //一天内不再提醒
          window.localStorage.setItem('ieNotificationTime', (+new Date()) + 24 * 60 * 60 * 1000);
        }),
        ui.createDialogButton('ok', '立即下载', function() {
          window.open('http://xiazai.sogou.com/detail/34/8/6262355089742005676.html');
        }),
      ],
      style: {
        '.modal-dialog': {
          'position': 'relative',
          'width': '450px',
          'margin': '200px auto 0 auto'
        },
        '.modal-body': {
          'padding': '45px 0 0 130px'
        },
        '.modal-footer': {
          'margin': '22px 0 30px 0',
          'border-top-width': '0px'
        },
        '.btn-default': {
          'width': '140px',
          'height': '33px',
          'font-size': '14px',
          'color': '#00abe4',
          'border': '1px solid #00abe4',
          'float': 'left',
          'margin-left': '56px'
        },
        '.btn-success': {
          'width': '140px',
          'height': '33px',
          'font-size': '14px',
          'background-color': '#00abe4',
          'border-width': '0',
          'float': 'left',
          'margin-left': '28px'
        },
        '.close-dialog': {
          'display': 'block',
          'position': 'absolute',
          'top': '12px',
          'right': '12px',
          'width': '14px',
          'height': '14px',
          'cursor': 'pointer',
          'background': 'url("static/imgs/close.png")'
        }
      }
     });
     $(document).on('click', '.modal-dialog .close-dialog,.modal-dialog .btn-success', function() {
       dialog.modal('hide');
     });
  }

  function showReplyCount() {
    var url = "/feedback/getReplyCount"
    utils.post(url, {}, function(r) {
      if (r.httpSuccess) {
        var data = r.data;
        if (data.code == 0) {
          var count = data.data;
          if (count > 0) {
            $("#brand-feedback").text(data.data);
            $("#brand-feedback").removeClass("hide");
          }
        }
      }
    });
  }
});