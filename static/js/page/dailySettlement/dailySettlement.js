require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['page/finance/common'], function (common) {
    $(document).delegate('#query-commit', 'click', function () {
        var startDate = common.str2Date($('#query-start-date').val());
        var endDate = common.str2Date($('#query-end-date').val());
        if (startDate > endDate) {
            alert("开始时间必须大于结束时间");
            return false;
        }
        if ((endDate - startDate) / 1000 / 24 / 60 / 60 > 6) {
            alert("时间间隔不能超过7天");
            return false;
        }
        window.location.href = "/partner/dailySettlement?" + getQueryStr();
    });

    //点击查看商家详情
    $(document).delegate('a[name=poiDetail]', 'click', function () {
         window.open("/partner/poiSettlement?type=1&" + getQueryStr());
    });

    //点击查看配送员详情
    $(document).delegate('a[name=dispatcherDetail]', 'click', function () {
        window.open("/partner/finance/riderStatistics?ts="+ $('#query-start-date').val() + "&te=" + $('#query-end-date').val());
    });

    function getQueryStr() {
        var startDate = $('#query-start-date').val();
        var endDate = $('#query-end-date').val();
        var str =  "startDate=" + startDate + "&endDate=" + endDate;
        return str;
    }
});