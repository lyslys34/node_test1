/**
 * 头部以及菜单生成
 *
 * @date 2015-09-22
 * @author tzw
 *
 */

// 模板引擎
(function() {
    var root = this,
        Temp = {};
    var escapes = {
            "'": "'",
            '\\': '\\',
            '\r': 'r',
            '\n': 'n',
            '\t': 't',
            '\u2028': 'u2028',
            '\u2029': 'u2029'
        },
        escaper = /\\|'|\r|\n|\t|\u2028|\u2029/g;
    // Save bytes in the minified (but not gzipped) version:
    var ArrayProto = Array.prototype,
        ObjProto = Object.prototype,
        FuncProto = Function.prototype;
    var nativeKeys = Object.keys,
        nativeForEach = ArrayProto.forEach;
    // Create quick reference variables for speed access to core prototypes.
    var push = ArrayProto.push,
        slice = ArrayProto.slice,
        concat = ArrayProto.concat,
        toString = ObjProto.toString,
        hasOwnProperty = ObjProto.hasOwnProperty;
    var Temp = {};
    Temp.has = function(obj, key) {
        return hasOwnProperty.call(obj, key);
    };
    Temp.each = Temp.forEach = function(obj, iterator, context) {
        if (obj == null) return;
        if (nativeForEach && obj.forEach === nativeForEach) {
            obj.forEach(iterator, context);
        } else if (obj.length === +obj.length) {
            for (var i = 0, l = obj.length; i < l; i++) {
                if (iterator.call(context, obj[i], i, obj) === breaker) return;
            }
        } else {
            for (var key in obj) {
                if (Temp.has(obj, key)) {
                    if (iterator.call(context, obj[key], key, obj) === breaker) return;
                }
            }
        }
    };
    // Fill in a given object with default properties.
    Temp.defaults = function(obj) {
        Temp.each(slice.call(arguments, 1), function(source) {
            if (source) {
                for (var prop in source) {
                    if (obj[prop] == null) obj[prop] = source[prop];
                }
            }
        });
        return obj;
    };
    // Invert the keys and values of an object. The values must be serializable.
    Temp.invert = function(obj) {
        var result = {};
        for (var key in obj)
            if (Temp.has(obj, key)) result[obj[key]] = key;
        return result;
    };
    Temp.keys = nativeKeys || function(obj) {
        if (obj !== Object(obj)) throw new TypeError('Invalid object');
        var keys = [];
        for (var key in obj)
            if (Temp.has(obj, key)) keys[keys.length] = key;
        return keys;
    };
    // List of HTML entities for escaping.
    var entityMap = {
        escape: {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#x27;',
            '/': '&#x2F;'
        }
    };
    entityMap.unescape = Temp.invert(entityMap.escape);
    // Regexes containing the keys and values listed immediately above.
    var entityRegexes = {
        escape: new RegExp('[' + Temp.keys(entityMap.escape).join('') + ']', 'g'),
        unescape: new RegExp('(' + Temp.keys(entityMap.unescape).join('|') + ')', 'g')
    };
    // Functions for escaping and unescaping strings to/from HTML interpolation.
    Temp.each(['escape', 'unescape'], function(method) {
        Temp[method] = function(string) {
            if (string == null) return '';
            return ('' + string).replace(entityRegexes[method], function(match) {
                return entityMap[method][match];
            });
        };
    });
    Temp.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
    };
    // JavaScript micro-templating, similar to John Resig's implementation.
    // Underscore templating handles arbitrary delimiters, preserves whitespace,
    // and correctly escapes quotes within interpolated code.
    Temp.template = function(text, data, settings) {
        var render;
        settings = Temp.defaults({}, settings, Temp.templateSettings);

        // Combine delimiters into one regular expression via alternation.
        var matcher = new RegExp([
            (settings.escape || noMatch).source, (settings.interpolate || noMatch).source, (settings.evaluate || noMatch).source
        ].join('|') + '|$', 'g');

        // Compile the template source, escaping string literals appropriately.
        var index = 0;
        var source = "TmpTmpp+='";
        text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
            source += text.slice(index, offset)
                .replace(escaper, function(match) {
                    return '\\' + escapes[match];
                });

            if (escape) {
                source += "'+\n((TmpTmpt=(" + escape + "))==null?'':Temp.escape(TmpTmpt))+\n'";
            }
            if (interpolate) {
                source += "'+\n((TmpTmpt=(" + interpolate + "))==null?'':TmpTmpt)+\n'";
            }
            if (evaluate) {
                source += "';\n" + evaluate + "\nTmpTmpp+='";
            }
            index = offset + match.length;
            return match;
        });
        source += "';\n";

        // If a variable is not specified, place data values in local scope.
        if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';

        source = "var TmpTmpt,TmpTmpp='',TmpTmpj=Array.prototype.join," +
            "print=function(){TmpTmpp+=TmpTmpj.call(arguments,'');};\n" +
            source + "return TmpTmpp;\n";

        try {
            render = new Function(settings.variable || 'obj', 'Temp', source);
        } catch (e) {
            e.source = source;
            throw e;
        }

        if (data) return render(data, Temp);
        var template = function(data) {
            return render.call(this, data, Temp);
        };

        // Provide the compiled function source as a convenience for precompilation.
        template.source = 'function(' + (settings.variable || 'obj') + '){\n' + source + '}';

        return template;
    };

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' && module.exports) {
            exports = module.exports = Tem;
        }
        exports.Temp = Temp;
    }
    root.Temp = Temp;
}).call(this);
// 头部以及菜单生成
(function() {
    var topNavStatic =
        [
        '      <div class="home-wrapper">',
        '          <a href="'+ url_config.admin +'" class="brand home-link">',
        '            <span class="project-name">美团配送烽火台</span>',
        '            <div class="title-content"></div>',
        '          </a>',
        '          <a class="btn toggle-btn j-toggle-menu" href="javascript:;">',
        '              <i class="fa fa-bars"></i>',
        '          </a>',
        '       </div>'].join('');
    // top-nav
    var topNavTpl =
        [
        '        <div class="pull-right header-info">',
        '          <a href="'+ url_config.admin +'/partner/download" class="app-download-wrapper pull-left">',
        '            <span class="vertical-line"></span>',
        '            <i class="fa fa-custom-download"></i>',
        '            <span class="info-text">客户端</span>',
        '          </a>',
        '          <a id="feedback" href="'+ url_config.admin +'/feedback/addAndList" class="goto-feedback-wrapper pull-left">',
        '            <span class="vertical-line"></span><i class="fa fa-custom-chat"></i><span class="info-text">反馈</span>',
        '            <span id="brand-feedback" class="badge hide" style="background-color: red;color: #fff;">0</span>',
        '          </a>',
        '          <div class="user-info-wrapper pull-left">',
        '            <a href="javascript:;" class="user-info-content pull-left">',
        '              <span class="vertical-line"></span>',
        '              <span class="info-content">',
        '                <i class="fa fa-custom-user"></i>',
        '                   <% if(typeof userName != "undefined"){ %>',
        '                   <span class="info-text"><%= userName %></span>',
        '                   <% }else{ %>',
        '                   <span class="info-text user-name"></span>',
        '                   <% } %>',
        '              </span>',
        '              <span class="custom-triangle"><i></i><em></em></span>',
        '            </a>',
        '            <ul>',
        '              <li>',
        '                  <a href="<%= logoutUrl %>" class="user-info-list logout logoutConfirm_open" data-popup-ordinal="1" id="adminLogout">',
        '                    <i class="fa fa-power-off"></i>',
        '                    <span class="info-text">退出</span>',
        '                  </a>',
        '              </li>',
        '              <% if(typeof __mock__  != "undefined" && __mock__){ %>',
        '                   <li>',
        '                    <a href="'+ url_config.admin +'/mock/stop" id="stopMock" class="user-info-list">',
        '                      <i class="fa fa-pause"></i>',
        '                      <span class="info-text">停止模拟</span>',
        '                    </a>',
        '                  </li>',
        '              <% } %>',
        '            </ul>',
        '          </div>',
        '        </div>']
        .join("");
    // 面包屑
    var breadTpl = ['<ul class="breadcrumb">',
        '          <% for (var i = 0,len = data.length;i<len;i++) { %>',
        '              <li style="color:#757986;font-size:14px;"><%= data[i] %>',
        '                  <% if(i == len-1){ %>',
        '                  <span class="active"></span>',
        '                  <% }else{ %>',
        '                  <span class="divider"></span>',
        '                  <% } %>  ',
        '              </li>',
        '          <% } %>',
        '        </ul>'
    ].join("");
    // sidebar
    var sidebarTpl = ['    <div class="sidebar-inner scrollable-sidebar">',
        '        <div class="main-menu">',
        '            <ul>',
        '            <% for (var i = 0;i<menus.length;i++) { %>',
        '                <% if(menus[i]["menus"] && menus[i]["menus"].length){ %>',
        '                <li class="openable">',
        '                    <a href="#<%= menus[i].title %>" class="nav-header collapsed" data-toggle="collapse">',
        '                        <% var title = menus[i]["title"];if(title){ %>',
        '                           <div class="fa-wrapper">',
        '                           <% if(title == "包裹中心"){ %>',
        '                               <i class="fa fa-lg fa-custom-package"></i>',
        '                           <% }else if(title == "配送员数据中心"){ %>',
        '                               <i class="fa fa-lg fa-custom-delivery-clerk"></i>',
        '                           <% }else if(title == "组织架构"){ %>',
        '                               <i class="fa fa-lg fa-custom-architecture"></i>',
        '                           <% }else if(title == "结算中心"){ %>',
        '                               <i class="fa fa-lg fa-custom-clearing-center"></i>',
        '                           <% }else if(title == "数据中心"){ %>',
        '                               <i class="fa fa-lg fa-custom-data-center"></i>',
        '                           <% }else if(title == "众包管理"){ %>',
        '                               <i class="fa fa-lg fa-custom-crowdsourcing"></i>',
        '                           <% }else if(title == "角马管理"){ %>',
        '                               <i class="fa fa-lg fa-custom-wildebeest"></i>',
        '                           <% }else if(title == "系统管理"){ %>',
        '                               <i class="fa fa-lg fa-custom-system-manage"></i>',
        '                           <% }else if(title == "业务管理"){ %>',
        '                               <i class="fa fa-lg fa-custom-business-manage"></i>',
        '                           <% }else if(title == "调度中心"){ %>',
        '                               <i class="fa fa-lg fa-custom-dispatching-center"></i>',
        '                           <% }else if(title == "报表"){ %>',
        '                               <i class="fa fa-lg fa-custom-fa-custom-report-form"></i>',
        '                           <% }else{ %>',
        '                               <i class="fa fa-lg fa-custom-package"></i>',
        '                           <% } %>',
        '                           </div>',
        '                        <% } %>',
        '                        <span class="text"><%= menus[i].title %></span>',
        '                        <span class="menu-hover"></span>',
        '                    </a>',
        '                    <ul id="<%= menus[i].title %>" class="nav nav-list submenu collapse">',
        '                      <% if(menus[i]["menus"]){ %>',
        '                      <% for (var m = 0,n= menus[i]["menus"];m<n.length;m++) { %>',
        '                          <li>',
        '                              <a href="<%= n[m]["url"] %>">',
        '                                  <span class="submenu-label"><%= n[m]["title"] %></span>',
        '                              </a>',
        '                          </li>',
        '                      <% } %>',
        '                      <% } %>',
        '                    </ul>',
        '                </li>',
        '                <% } %>',
        '            <% } %>',
        '            </ul>',
        '        </div>',
        '    </div>',
    ].join("");

    var Main = {
        init: function() {
            this.renderStatic();
            this.getData(Main.render);
        },
        renderStatic: function() {
            //初次加载top-nav静态部分
            $('#top-nav').append(topNavStatic);
        },
        getData: function(callback) {
            $.ajax({
                url: '/frame/info',
                async:false,
                success: function(data) {
                    //数据格式{"userName": "","isPartner": 1,"logoutUrl": "","menus": [menu]}
                    //menu：id，title，url，code，sort，type，createTime，menus: [menu]
                    callback(data);
                },
                error: function(err) {
                    console.info(err);
                }
            })
        },
        render: function(data) {
            var $wrapper = $('#wrapper'),
                topnav, bread, sidebar;
            var prepareBread = function(data) {
                var pathname = location.pathname;
                var dataBread = {
                    'data': []
                };
                if (data && data['menus']) {
                    for (var i = 0, j = data['menus']; i < j.length; i++) {
                        if (j[i]['menus']) {
                            for (var m = 0, n = j[i]['menus']; m < n.length; m++) {
                                if (n[m]['url'].indexOf(pathname) > -1) {
                                    dataBread['data'].unshift(n[m]['title']);
                                    dataBread['data'].unshift(j[i]['title']);
                                    break;
                                }
                            }
                        }
                    }
                }
                return dataBread;
            };

            // 头部
            topnav = Temp.template(topNavTpl, data);
            $('#top-nav').append(topnav);
            // 面包屑
            var breadData = prepareBread(data);
            bread = Temp.template(breadTpl, breadData);
            $wrapper.find('#breadcrumb-bar').append(bread);
            // sidebar
            sidebar = Temp.template(sidebarTpl, data);
            $('aside.fixed').append(sidebar);
            // 选中
            breadData && breadData['data'] && $('#' + breadData['data'][0]).closest('li').addClass("active");
        }
    };
    Main.init();
})();

// 追加其他功能的文件
(function() {
    //监控中心
    dyncLoad([url_config.monitor + "/static/js/page/monitor/controller/monitor_com.js", url_config.monitor + "/static/css/page/monitor/monitor_com.css"]);
})();