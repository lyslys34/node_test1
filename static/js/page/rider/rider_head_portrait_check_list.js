require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root', 'module/validator','module/cookie'], function (t, validator,cookie) {
    var userId;
    var id;
    var disagreeType=2; //驳回的check字段
    var aggreType=1;    //审核通过的check字段
    var separator='#' //驳回原因的分隔符

    $("#check-All").click(function(){
        if(this.checked){
        	$(".user_check_box").each(function(){
        		if(!this.checked){
        			$(this).click();
        		}
        	})
        }else{
        	$(".user_check_box").each(function(){
        		if(this.checked){
        			$(this).click();
        		}
        	})
        }
    });

    $("#multi_pass").click(function(){
        if($("input[name='check-user']:checked").length > 0){
             $("#multiAggresUserConfirm").modal();
        } else {
        	alert("请先选择需要审核的记录");
        }
    });

    $('#checkTable input[name="check-user"]').click(function(){
        var num = $("input[name='check-user']:checked").length;
        if(!!num){
            $('#se_num').text(num);
        }else{
            $('#se_num').text(0);
        }
    });

    $('#multiCancel').click(function(){
        $("#multiAggresUserConfirm").modal('hide');
    });

    $('#multiAggreUserBtn').click(function  () {
        var recordIds = [];
        $("input[name='check-user']:checked").each(function(index, el) {
            var record_id = $(this).attr("recordId");
            recordIds.push(record_id);

        });
         $.ajax({
                url: '/rider/headPortraitMultiCheck',
                type: 'POST',
                dataType: 'json',
                data: {ids: recordIds.join(','),checkStatus:aggreType},
            })
            .done(function(data) {
                if(data.code == "0"){
                    location.reload();
                }else{
                    alert(data.msg);
                }
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
    });

    //驳回按钮，显示驳回框
    $(".disagree").click(function(){
        userId=$(this).parent().attr("userId");
        id=$(this).parent().attr("recordId");
        $("#disaggresUser").modal();
    })

    $(".approve").click(function(){
        id=$(this).parent().attr("recordId");
        $("#aggresUserConfirm").modal();
    })

    //驳回页面确认驳回
    $("#disaggreUserBtn").click(function(){

        var postData=getCheckPostData(disagreeType);

        if(postData==null){
            return false;
        }

        $("#disaggresUser").modal('hide');

        var url="/rider/headPortraitMultiCheck";

        t.utils.post(url, postData, function (r) {
            if (r.httpSuccess) {
                if(r.data.code == 0 ) {
//                    window.location.href="/rider/noChecklist";
                	location.reload();
                    return false;
                } else {
                    showMsg(r.data.msg);
                    return false;
                }
            } else {
                showMsg("提交失败,系统错误!");
                return false;
            }
        });

    })

    $("#cancelCheck").click(function(){
        $("#disaggresUser").modal('hide');
    })

    //审核通过
    $("#aggreUserBtn").click(function(){

        $("#aggresUserConfirm").modal('hide');

        var postData=getCheckPostData(aggreType);
        var url="/rider/headPortraitMultiCheck";

        t.utils.post(url, postData, function (r) {
            if (r.httpSuccess) {
                if(r.data.code == 0 ) {
//                     window.location.href="/rider/noChecklist";
                	location.reload();
                    return false;
                } else {
                    showMsg(r.data.msg);
                    return false;
                }
            } else {
                showMsg("提交失败,系统错误!");
                return false;
            }
        });

    })

    //点击图片，显示原图
    $(".smallImg").click(function(){
        var src = $(this).attr("src");
        $("#imgOrigin").find(".modal-dialog").html("<img src='"+src+"' />");
        $("#imgOrigin").modal();
        if ($(this).hasClass('riderHeadImg')) {
          $(".modal-dialog img").css("border-radius", "50%");
        }
    });

    function getCheckPostData(checkType){
        var comment = "";
        if(checkType==disagreeType ){
              comment = $("#comment").val();

              if(isBlank(comment)){
                  alert("请选择驳回原因");
                  return null;
              }

        }

        postData={ids:id,
                  checkStatus:checkType,
                  comment:comment}

        return postData;
    }

    function showMsg(msg,reload){
        var body = $('<div style="height: 100px;padding: 15px;text-align: center;">'+msg+'</div>');
        t.ui.showModalDialog({
            body: body, buttons: [
                t.ui.createDialogButton('close','关闭', function () {     // 取消
                    if(reload){
                        location.reload(true);
                    }
                })
            ],
            style:{".modal-dialog":{"padding":"100px 110px"},
                   ".modal-footer":{"padding":"10px"},
                   ".btn-default":{"font-size":"12px"}
            }
        });
    }

    $("#cancel").click(function(){
        $("#aggresUserConfirm").modal('hide');
    })

    function isBlank(value) {

        if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
                return this.replace(/^\s+|\s+$/g, '');
            }
        }
        return value==null || value.trim() == "" || value==undefined;
    }
})