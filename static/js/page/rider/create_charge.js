/**
 * Created by lixiangyang on 15/8/18.
 */

$(document).ready(function () {

    $(".js_uploadbtn").click(function () {
        $(this).siblings(".js_fileupload").click();
    });

    $(".js_img_click_open").click(function(){
        window.open($(this).attr("src"));
    });

    $("#js_save").click(function () {
        var orgId = $("#orgId").val();
        var name = $("#userName").val();
        var idcardNo = $("#idcardNo").val();
        var gender = $("input[name='gender']:checked").val();
        var jobType = $("input[name='jobType']:checked").val();
        var phone = $("#userPhone").val();
        var faceUrl = $("#js_faceUrl").val();
        var idcardUrl = $("#js_cardUrl").val();
        var faceUrlWithWatermark=$("#faceUrlWithWatermark").val();
        var idCardUrlWithWatermark=$("#idCardUrlWithWatermark").val();

        if(isBlank(name)) {
            showTip("请输入正确的姓名", 2);
            return false;
        }
        if(!isMobile(phone)) {
            showTip("请输入正确的手机号", 2);
            return false;
        }
        if(!isIdentityCardValid(idcardNo)) {
            showTip("请输入正确的身份证号", 2);
            return false;
        }
        if(isBlank(faceUrl)) {
            showTip("请上传手持身份证照片", 2);
            return false;
        }
        if(isBlank(idcardUrl)) {
            showTip("请上传身份证正面照", 2);
            return false;
        }

        var data = {}
        data.orgId = orgId;
        data.name = name;
        data.idcardNo = idcardNo;
        data.gender = gender;
        data.jobType = jobType;
        data.phone = phone;
        data.faceUrl = faceUrl;
        data.idcardUrl = idcardUrl;
        data.idCardUrlWithWatermark=idCardUrlWithWatermark;
        data.faceUrlWithWatermark=faceUrlWithWatermark;

        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/rider/doCreateOrgCharge",
            data : data,
            success : function(r){
                if (r) {
                    if(r.code == 0) {
                        window.location.href="/settle/orgsetting?orgId=" + orgId +"&f=join_org_charge";
                    } else {
                        showTip(r.msg, 3);
                    }
                } else {
                    showTip("添加加盟商负责人失败!", 3);
                }
            }
        });

    });

    var uploadOption = {
        url: "/file/imguploadWithWatermark",
        add: function (e, data) {
            var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
            var maxFileSize = 5242880;
            if (data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                showTip("文件格式不正确(gif,jpg,png)", 2);
                return false;
            }
            if (data.originalFiles[0]['size'] && data.originalFiles[0]['size'] > maxFileSize) {
                showTip("文件太大(5M)", 2);
                return false;
            }
            data.context = $(this).siblings(".js_uploadbtn").addClass("disabled").val("正在上传");
            data.submit();
        },
        done: function (e, data) {
            $(this).siblings(".js_uploadbtn").val("点击上传").removeClass("disabled")

            if (data.result) {
                if (data.result.code == 0) {
                    if (data.result.data[0] && data.result.data[0].code == 0) {
                        var backUrl = data.result.data[0].url;
                        var backUrlWithWatermark=data.result.data[0].waterMarkUrl;

                        if (typeof backUrl == undefined || backUrl == null || backUrl == '') {
                            showTip("上传失败，获取图片链接失败", 2);
                            return false;
                        }

                        if(!isBlank(backUrlWithWatermark)){
                            $(this).siblings(".js_upload_back_url_with_watermark").val(backUrlWithWatermark);
                        }

                        $(this).siblings("img").css("display", "block").attr("src", backUrl);
                        $(this).siblings(".js_upload_back_url").val(backUrl);
                    } else {
                        showTip("上传失败," + (data.result.data[0] ? data.result.data[0].msg : "未知错误"), 2);
                    }
                } else {
                    showTip("上传失败，" + data.result.msg, 2);
                }
            } else {
                showTip("上传失败", 2);
            }
        }
    };

    function showTip(content, type) {
        _showPop({
            type:type,
            content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + content + "</div>"
        });
    }

    $("#js_passed").click(function() {
        var content = "如不填写负责人信息，则加盟商无法登陆烽火台，后期可使用人员管理中\"添加人员\"功能来添加负责人信息";
        _showPop({
            type:3,
            content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + content + "</div>",
            ok: {
                display: true,
                name: "确定",
                callback: function() {
                    window.location.href="/settle/orgsetting?orgId=" + $("#orgId").val() +"&f=join_org_charge";
                }
            }
        });
    })

    $(".js_fileupload").fileupload(uploadOption);



});



