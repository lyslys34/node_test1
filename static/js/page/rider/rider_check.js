require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root', 'module/validator','module/cookie'], function (t, validator,cookie) {
    var userId;
    var id;
    var desc;   //驳回原因
    var disagreeType=2; //驳回的check字段
    var aggreType=1;    //审核通过的check字段
    var separator='#' //驳回原因的分隔符

    $("#check-All").click(function(){
        if(this.checked){
        	$(".user_check_box").each(function(){
        		if(!this.checked){
        			$(this).click();
        		}
        	})
        }else{
        	$(".user_check_box").each(function(){
        		if(this.checked){
        			$(this).click();
        		}
        	})
        }
    });

    $("#multi_pass").click(function(){
        if($("input[name='check-user']:checked").length > 0){
             $("#multiAggresUserConfirm").modal();
        } else {
        	alert("请先选择需要审核的记录");
        }
    });

    $('#checkTable input[name="check-user"]').click(function(){
        var num = $("input[name='check-user']:checked").length;
        if(!!num){
            $('#se_num').text(num);
        }else{
            $('#se_num').text(0);
        }
    });

    $('#multiCancel').click(function(){
        $("#multiAggresUserConfirm").modal('hide');
    });

    $('#multiAggreUserBtn').click(function  () {
        var userids = [];
        var recordIds = [];
        $("input[name='check-user']:checked").each(function(index, el) {
            var item = $(this).parent().parent().children("td[userId]");
            var user_id = item.attr("userId");
            var record_id = item.attr("recordId");
            userids.push(user_id);
            recordIds.push(record_id);

        });
        // $('input[name='check-user']:checked').each(function() {

        // });
         $.ajax({
                url: '/rider/multiCheck',
                type: 'POST',
                dataType: 'json',
                data: {ids: recordIds.join(','),userIds:userids.join(',')},
            })
            .done(function(data) {
                if(data.resultCode == "0"){
                    location.reload();
                }else{
                    alert(data.resultMsg);
                }
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
    });
    $(function() {
		$.fn.picTransform = function(options) {
			var ibox = {
				wrap: this,
				isMoving: false,
				startX: 0,
				startY: 0,
				startL: 0,
				startT: 0,
				// boxRect: this.find('.ibox_wrapper')[0].getBoundingClientRect(),
				limitX: [0, 0],
				limitY: [0, 0],
				events: [
					['click', '.tool_btn', '_tool']
					,['mousedown', '.photo', '_moveStart']
				],
				_init: function() {
					var me = this;
					me._bind();
					me._otherBind();
				},
				_tool: function(e) {
					var me = this;
					var $btn = $(e.target);
					var type = $btn.data('type');
					var $img = $btn.parent().siblings('.ibox_wrapper').find('img');

					me._transform($img, type);
				},
				_transform: function(img, type) {
					var $img = img;
					var scale = parseFloat( Number($img.data('scale')).toFixed(1) );
					var rotate = parseInt($img.data('rotate'));
                    var horiz = parseInt($img.data('horiz'));
					var prop = '-webkit-transform',
						rule = '';
					switch (type) {
						case 'plus':
							if (scale >= 2) {break};
							scale += 0.2;
							break;
						case 'minus':
							if (scale <= 1) {break};
							scale -= 0.2;
							break;
						case 'right':
							if (rotate >= 360) {break};
							rotate += 90;
							break;
						case 'left':
							if (rotate <= -360) {break};
							rotate -= 90;
							break;
                        case 'horiz':
                            horiz = horiz == 0 ? 180 : 0;
                            break;
					}
					rule += ' scale(' + scale + ')';
					rule += ' rotateZ(' + rotate + 'deg)';
                    rule += ' rotateY(' + horiz + 'deg)';
					$img.data('scale', scale);
					$img.data('rotate', rotate);
                    $img.data('horiz', horiz);
					$img.css(prop, rule);
                    if (scale == 1) {
                        $img.css({
                            left: '0',
                            top: '0'
                        });
                    }
				},
				_moveStart: function(e) {
					var me = this;
					var dom = e.target;

                    if ($(dom).data('scale') != '1') {
                        me.isMoving = true;
                    }else if ($(dom).data('rotate') != '0' && $(dom).data('rotate') != '360' && $(dom).data('rotate') != '-360') {
                        me.isMoving = true;
                    }
					me.startX 	= e.pageX;
					me.startY 	= e.pageY;
					me.startL 	= parseFloat(document.defaultView.getComputedStyle(dom, null).left);
					me.startT 	= parseFloat(document.defaultView.getComputedStyle(dom, null).top);
					var rect 	= dom.getBoundingClientRect(),
						boxRect = dom.parentNode.getBoundingClientRect();

					var left 	= boxRect.left - rect.left,
						right 	= boxRect.right - rect.right,
						top 	= boxRect.top - rect.top,
						bottom 	= boxRect.bottom - rect.bottom;
					me.limitX = [right, left];
					me.limitY = [bottom, top];

					$(document).on('mousemove.transform', function(e) {
						me._moving(e, dom);
					})
				},
				_moving: function(e, img) {
					var me = this;
					if (!me.isMoving) return false;
					var x = e.pageX - me.startX,
						y = e.pageY - me.startY;
					img.style.left = me.startL + x + 'px';
					img.style.top = me.startT + y + 'px';
				},
				_moveEnd: function(e) {
					var me = this;
					me.isMoving = false;
				},
				_bind: function() {
					var me = this;
					var	context = me,
						events  = me.events,
						$wrap    = me.wrap;

					events && $.each(events, function(k, v) {
						var handler = me[v[2]] ? me[v[2]] : null;
						var ev = v[0] + '.transform',
							el = v[1];

						$wrap.off(ev).on(ev, el, function(e) {
							handler.call(context, e)
						})
					})
				},
				_otherBind: function() {
                    var me = this;
					$(document).on('mouseup.transform', function() {
						$(this).off('mousemove.transform');
                        me._moveEnd();
					})
				}
			}
			ibox._init();
		}

		$('.ibox').picTransform();
	})


    //驳回按钮，显示驳回框
    $("#disagree").click(function(){
        userId=$("input[name='userId']").val();
        id=$("input[name='id']").val();
        $("#disaggresUser").modal();
    })

    $(".disagree").click(function(){
        userId=$(this).parent().attr("userId");
        id=$(this).parent().attr("recordId");
        $("#disaggresUser").modal();
    })

    $(".approve").click(function(){
        userId=$(this).parent().attr("userId");
        id=$(this).parent().attr("recordId");
        $("#aggresUserConfirm").modal();
    })

    //驳回页面确认驳回
    $("#disaggreUserBtn").click(function(){

        var postData=getCheckPostData(disagreeType);

        if(postData==null){
            return false;
        }

        $("#disaggresUser").modal('hide');

        var url="/rider/otherCheck";

        t.utils.post(url, postData, function (r) {
            if (r.httpSuccess) {
                if(r.data.success ) {
//                    window.location.href="/rider/noChecklist";
                	location.reload();
                    return false;
                } else {
                    showMsg(r.data.resultMsg);
                    return false;
                }
            } else {
                showMsg("提交失败,系统错误!");
                return false;
            }
        });

    })

    //驳回页面确认驳回
    $("#disaggreUserBtn1").click(function(){

        var postData=getCheckPostData(disagreeType);

        if(postData==null){
            return false;
        }

        $("#disaggresUser").modal('hide');

        var url="/rider/otherCheck";

        t.utils.post(url, postData, function (r) {
            if (r.httpSuccess) {
                if(r.data.success ) {
                	var pageNo = $("#pageNo").val();
                    window.location.href="/rider/noChecklist?pageNo="+pageNo;
//                	location.reload();
                    return false;
                } else {
                    showMsg(r.data.resultMsg);
                    return false;
                }
            } else {
                showMsg("提交失败,系统错误!");
                return false;
            }
        });

    })

    $("#cancelCheck").click(function(){
        $("#disaggresUser").modal('hide');
    })


    var textarea='<textarea rows="3" id="otherDesc" name="otherDisaggreDesc" placeholder="Enter text" style="width:350px;" class="form-control"></textarea>';

//    $("input[name='remark']").click(function(){
//        desc=$(this).val();
//        if(desc=="0"){
//            $("#showOtherDesc").append(textarea);
//        }else{
//            $("#otherDesc").remove();
//        }
//
//    })
      $("#optionsRadios5").click(function(){
          if($(this).is(":checked")==true){
              $("#showOtherDesc").append(textarea);
          }else{
              $("#otherDesc").remove();
          }
      })

    //审核通过确认框
    $("#approve").click(function(){
        userId=$("input[name='userId']").val();
        id=$("input[name='id']").val();
        $("#aggresUserConfirm").modal();
    })

    //审核通过
    $("#aggreUserBtn").click(function(){

        $("#aggresUserConfirm").modal('hide');

        var postData=getCheckPostData(aggreType);
        var url="/rider/otherCheck";

        t.utils.post(url, postData, function (r) {
            if (r.httpSuccess) {
                if(r.data.success ) {
//                     window.location.href="/rider/noChecklist";
                	location.reload();
                    return false;
                } else {
                    showMsg(r.data.resultMsg);
                    return false;
                }
            } else {
                showMsg("提交失败,系统错误!");
                return false;
            }
        });

    })

    //审核通过
    $("#aggreUserBtn1").click(function(){

        $("#aggresUserConfirm").modal('hide');

        var postData=getCheckPostData(aggreType);
        var url="/rider/otherCheck";

        t.utils.post(url, postData, function (r) {
            if (r.httpSuccess) {
                if(r.data.success ) {
                	var pageNo = $("#pageNo").val();
                     window.location.href="/rider/noChecklist?pageNo=" + pageNo;
//                	location.reload();
                    return false;
                } else {
                    showMsg(r.data.resultMsg);
                    return false;
                }
            } else {
                showMsg("提交失败,系统错误!");
                return false;
            }
        });

    })

    var needAdditionalDesc=false;  //是否需要输入文本框的附加原因（选择了其他就必须输入）

    function getCheckPostData(checkType){

        var addDesc=$("#otherDesc").val();

        if(checkType==disagreeType ){

              desc=getCheckboxVal();

              if(needAdditionalDesc){
                  if(isBlank(addDesc)){
                      alert("请输入其他原因");
                      return null;
                  }

                  //验证输入驳回原因长度，不能超过150字符
                  if(addDesc.length>150){
                      alert("输入的原因长度不能大于150个字符！");
                      return null;
                  }

                  if(isBlank(desc)){
                      desc=addDesc;
                  }else{
                      desc=desc+ separator +addDesc;
                  }
              }

              if(isBlank(desc)){
                  alert("请选择驳回原因");
                  return null;
              }

        }

        postData={
                  userId:userId,
                  id:id,
                  checkedType:checkType,
                  remark:desc}

        return postData;
    }

    function getCheckboxVal(){

        var remark="";
        var temp="";
        var a = document.getElementsByName("remark");
        var len=a.length;
        for ( var i = 0; i < len; i++) {
            if (a[i].checked) {
                temp = a[i].value;
                //选择了其他，判断文本框中是否输入了内容
                if(isBlank(temp)){
                    needAdditionalDesc=true;
                    continue;
                }
                remark = remark + separator +temp;
            }
        }

        if(!isBlank(remark)){
            remark=remark.substring(1);
        }

        return remark;

    }

    function showMsg(msg,reload){
        var body = $('<div style="height: 100px;padding: 15px;text-align: center;">'+msg+'</div>');
        t.ui.showModalDialog({
            body: body, buttons: [
                t.ui.createDialogButton('close','关闭', function () {     // 取消
                    if(reload){
                        location.reload(true);
                    }
                })
            ],
            style:{".modal-dialog":{"padding":"100px 110px"},
                   ".modal-footer":{"padding":"10px"},
                   ".btn-default":{"font-size":"12px"}
            }
        });
    }

    $("#cancel").click(function(){
        $("#aggresUserConfirm").modal('hide');
    })

    function isBlank(value) {

        if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
                return this.replace(/^\s+|\s+$/g, '');
            }
        }
        return value==null || value.trim() == "" || value==undefined;
    }

    $(".js_btn_jump").click(function () {
        pageJump();
    });

    function pageJump() {
        var pageNum = $("#pageNum").val();
        var userName = $("#userName").val();
        var userPhone = $("#userPhone").val();
        var orgId = $("#orgId").val();
        var cityId = $("#cityId").val();

        if (!pageNum || isNaN(pageNum)) {
            alert("请输入数字");

            return;
        }

        if (pageNum <= 0) {
            alert("请输入大于零的数");

            return;
        }

        if (String(pageNum).indexOf(".") > -1) {
            alert("请输入整数");

            return;
        }

        auditUrl = "/rider/noChecklist?pageNo=" + pageNum + "&userName=" + userName + "&userPhone=" + userPhone + "&orgId=" + orgId + "&cityId=" + cityId;

        window.open(auditUrl, '_top');
    }
})