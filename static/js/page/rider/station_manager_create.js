/**
 * Created by lixiangyang on 15/6/24.
 */

var orgSelect = $('#orgId');
$(document).ready(function () {

    $("#skip").click(function(){
        _showPop({
            type:1,
            content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>如果不填写负责人信息，则站长无法登录烽火台。后期可使用人员管理中“添加人员”功能来添加负责人信息</div>",
            ok: {
                display: true,
                name: "知道了",
                callback: function(){
                    var returnUrl = $("[name=returnUrl]").val();
                    if(returnUrl != 'undefined' && returnUrl != null && returnUrl.length > 0 ){
                        window.location.href = returnUrl;
                    }else{
                        window.location.href="/org/stations";
                    }
                }
            }
        });
    });

    $(".js_uploadbtn").click(function () {
        $(this).siblings(".js_fileupload").click();
    });



    $("#js_submit").click(function () {
        var name = $("#userName").val();
        var cardNo = $("#idcardNo").val();
        var phone = $("#userPhone").val();
        var faceUrl = $("#js_faceUrl").val();
        var cardUrl = $("#js_cardUrl").val();
        var employeeId = $("#mis-id").val();
        if(isBlank(name)) {
            alert("请输入正确的姓名");
            return false;
        }

        if(!isMobile(phone)) {
            alert("请输入正确的手机号");
            return false;
        }
        
        if(!isIdentityCardValid(cardNo)) {
            alert("请输入正确的身份证号");
            return false;
        }
        if(isBlank(faceUrl)) {
            alert("请上传手持身份证照片");
            return false;
        }
        if(isBlank(cardUrl)) {
            alert("请上传身份证正面照");
            return false;
        }
        if(isBlank(employeeId)){
            $("#mis-id").val(-1);
        }

        $("#formValidate1").submit();
    });

    var uploadOption = {
        url: "/file/imguploadWithWatermark",
        add: function (e, data) {
            var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
            var maxFileSize = 5242880;
            if (data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                alert("文件格式不正确(gif,jpg,png)");
                return false;
            }
            if (data.originalFiles[0]['size'] && data.originalFiles[0]['size'] > maxFileSize) {
                alert("文件太大(5M)");
                return false;
            }
            data.context = $(this).siblings(".js_uploadbtn").addClass("disabled").val("正在上传").unbind();
            data.submit();
        },
        done: function (e, data) {
            $(this).siblings(".js_uploadbtn").val("点击上传").removeClass("disabled").bind("click", {}, function () {
                $(this).siblings(".js_fileupload").click();
            });

            if (data.result) {
                if (data.result.code == 0) {
                    if (data.result.data[0] && data.result.data[0].code == 0) {
                        var backUrl = data.result.data[0].url;
                        var backUrlWithWatermark=data.result.data[0].waterMarkUrl;

                        if (typeof backUrl == undefined || backUrl == null || backUrl == '') {
                            alert("上传失败，获取图片链接失败");
                            return false;
                        }

                        if(!isBlank(backUrlWithWatermark)){
                            $(this).siblings(".js_upload_back_url_with_watermark").val(backUrlWithWatermark);
                        }

                        $(this).siblings("img").css("display", "block").attr("src", backUrl);
                        $(this).siblings(".js_upload_back_url").val(backUrl);
                    } else {
                        alert("上传失败," + (data.result.data[0] ? data.result.data[0].msg : "未知错误"));
                    }
                } else {
                    alert("上传失败，" + data.result.msg);
                }
            } else {
                alert("上传失败");
            }
        }
    };

    var fileupload = $(".js_fileupload").fileupload(uploadOption);

    $("#employeeId").keydown(function(event) {
        if (event.keyCode == 8) {
            $('#mis-id').val("");
            $('#employeeId').val("");
            $('.mis-search').hide();
        }
    });

    var lastTime;
    $("#employeeId").keyup(function(event) {
        lastTime = event.timeStamp;
        setTimeout(function() {
            if (lastTime - event.timeStamp == 0) {
                var date = new Date();
                $.getJSON('/employee/query?q=' + $('#employeeId').val()).success(function(data) {
                    var city_list = new Array();
                    if (data.data.length == 0) {
                        return;
                    }
                    $('.mis-search').show();
                    $('.mis-search-ul').empty();
                    for (var i in data.data) {
                        var name = data.data[i].name;
                        var id = data.data[i].id;
                        $('.mis-search-ul').append('<li onclick="add_to_input($(this));" data="'+id+'">' + name + '</li>');
                    }

                }).error(function(data) {
                });
            }
        }, 300);
    });

    add_to_input = function (e) {
        $('.mis-search').prev().val(e.text());
        $('#mis-id').val(e.attr('data'));
        $('.mis-search').hide();
    };

});



