
var orgSelect = $('#orgId');
$(document).ready(function () {


    $(".js_uploadbtn").click(function () {
        $(this).siblings(".js_fileupload").click();
    });

    $("#orgId").change(function(){
        $("#orgType").val($("#orgId option:selected").attr("orgType"));
        changeEntryDayDiv();
    })

    $("#js_submit").click(function () {
        var name = $("#userName").val();
        var cardNo = $("#idcardNo").val();
        var phone = $("#userPhone").val();
        var faceUrl = $("#js_faceUrl").val();
        var cardUrl = $("#js_cardUrl").val();
        if(isBlank(name)) {
            alert("请输入正确的姓名");
            return false;
        }
        if(!isMobile(phone)) {
            alert("请输入正确的手机号");
            return false;
        }
        if(!isIdentityCardValid(cardNo)) {
            alert("请输入正确的身份证号");
            return false;
        }
        if(isBlank(faceUrl)) {
            alert("请上传手持身份证照片");
            return false;
        }
        if(isBlank(cardUrl)) {
            alert("请上传身份证正面照");
            return false;
        }


        $("#formValidate1").submit();
    });

    var uploadOption = {
        //url: "/file/imgupload",
        url:"file/imguploadWithWatermark",
        add: function (e, data) {
            var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
            var maxFileSize = 5242880;
            if (data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                alert("文件格式不正确(gif,jpg,png)");
                return false;
            }
            if (data.originalFiles[0]['size'] && data.originalFiles[0]['size'] > maxFileSize) {
                alert("文件太大(5M)");
                return false;
            }
            data.context = $(this).siblings(".js_uploadbtn").addClass("disabled").val("正在上传").unbind();
            data.submit();
        },
        done: function (e, data) {
            $(this).siblings(".js_uploadbtn").val("点击上传").removeClass("disabled").bind("click", {}, function () {
                $(this).siblings(".js_fileupload").click();
            });

            if (data.result) {
                if (data.result.code == 0) {
                    if (data.result.data[0] && data.result.data[0].code == 0) {
                        var backUrl = data.result.data[0].url;
                        if (typeof backUrl == undefined || backUrl == null || backUrl == '') {
                            alert("上传失败，获取图片链接失败");
                            return false;
                        }
                        $(this).siblings("img").css("display", "block").attr("src", backUrl);
                        $(this).siblings(".js_upload_back_url").val(backUrl);
                    } else {
                        alert("上传失败," + (data.result.data[0] ? data.result.data[0].msg : "未知错误"));
                    }
                } else {
                    alert("上传失败，" + data.result.msg);
                }
            } else {
                alert("上传失败");
            }
        }
    };

    function getStationOrgList(code) {
        var url = '/org/orgSearchList';
        if(code == 1001) {
            url = '/org/stationOrgList';
        }

        var targetSelect = $("#orgId");

        $.ajax({
            dataType: 'json',
            type : 'post',
            url : url,
            success : function(r){
                if (r && r.code == 0) {
                    appendOrg(r.data);
                    orgSelect.select2();
                    $("#orgType").val($("#orgId option:selected").attr("orgType"));
                    changeEntryDayDiv();
                }
            }
        });
    }

    function appendOrg(data) {
        var targetSelect = $("#orgId");
        if(data) {
            targetSelect.empty();
            $.each(data, function(index, item){
                var op = $(document.createElement("option")).val(item.id).text(item.value).attr('orgType', item.type);
                targetSelect.append(op);
            });
        }
    }

    function changeEntryDayDiv() {
        if($("#orgType").val() == '1' && $("#roleType").val() == '1001') {
            $('#entryDateDiv').show();
            //$('#entryDate').attr("disabled","");
            $('#entryDate').removeAttr("disabled");
        } else {
            $('#entryDateDiv').hide();
            $('#entryDate').attr("disabled","disabled");
        }
    }

    var fileupload = $(".js_fileupload").fileupload(uploadOption);


    $("#roleType").on("change", function(e) {
        getStationOrgList($(this).val());
    });

    $("#roleType").change();


});



