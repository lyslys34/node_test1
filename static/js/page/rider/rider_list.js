require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});
require(['module/root', 'module/validator','module/cookie',  'lib/autocomplete'], function (t, validator,cookie, autocomplete) {

    $(document).delegate('.js_rider_delete', 'click', function () {
        _showDeletePanel($(this).parents('tr'));
    });

    $(document).delegate('#cancelDimission', 'click', function(){
        $('#dimissionNameHidden').val("");
        $('#dimissionName').text("");
        $('#dimissionUserId').val("");
        $('#dimissionNameInput').val("");

        $('#dimissionModal').modal('hide');
    });

    $(document).delegate('#confirmDimission', 'click', function(){
        if ($.trim($('#dimissionNameHidden').val()) != $.trim($('#dimissionNameInput').val())) {
            alert("请正确填写待离职骑手姓名");
            return false;
        } else {
            t.utils.post('turnover', 
            {bmUserId: $('#dimissionUserId').val(),
             termDate: $('#termDate').val()
            }, 
            function (r) {
                if (r.httpSuccess) {
                    if(r.data && r.data.code == 0) {
                        window.location.reload();
                    } else {
                        alert("配送员离职失败，" + r.data.msg);
                    }
                } else {
                    alert('配送人员离职出现错误错误');
                }
            });
        }
    });

    function _showDeletePanel(d) {

        if(d.data('orgtype') == '1' && d.data('rolecode') == '1001') {
            $('#termDateDiv').show();
            //$('#termDate').attr("disabled","");
        } else {
            $('#termDateDiv').hide();
            //$('#termDate').attr("disabled","disabled");
        }
        $('#dimissionNameHidden').val(d.data('name'));
        $('#dimissionName').text(d.data('name'));
        $('#dimissionUserId').val(d.data('id'));
        $('#dimissionNameInput').val("");
        $('#dimissionModal').modal();

    }

    $.datepicker.setDefaults({
        maxDate: 0
    })
});