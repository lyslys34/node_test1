require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
		deps: ['lib/jquery']
    },
    'lib/artTemplate': {
    	exports: 'template'
    }
  }
});
require(['module/root', 'lib/artTemplate'], function (root, template) {
	var noop = function() {}
	window.aop = /partner/.test(location.href) ? 'p' : 'a';
	var bm = {
		wrap: $('body'),
		appealId: '',
		approvalTodo: '',
		riderId: '',
		attendanceDate: '',
		appealToken: '',
		events: [
			['click', '.approve_btns > .btn-success', '_approve']
			,['click', '.approve_btns > .btn-danger', '_reject']
			,['click', '#confirmBox .sure', '_sureApprove']
			,['click', '#confirmBox .cancel', '_cancelApprove']
			,['click', '#rejectBox .sure', '_sureReject']
			,['click', '#rejectBox .cancel', '_cancelReject']
		],
		_init: function() {
			var me = this;

			me._getApealDetail();
			me._bind();
		},
		_getApealDetail: function(){
			var me = this;
			var href = location.href;
			var params = href.split('?')[1];
			var appealToken = params.match(/(^|&)appealtoken=([^&]*)(&|$)/)[2];
			me.appealToken = appealToken;
			var callback = function(res) {
				// res.attendanceDate = me._formatDate(res.attendanceDate);
				res.appealTime = me._formatDate(res.appealTime);
				res.modifyTime = me._formatDate(res.modifyTime);

				for (var i = 0, n = res.approvalRecords.length; i < n; i ++) {
					res.approvalRecords[i].approvalTime = me._formatDate(res.approvalRecords[i].approvalTime);
				}

				var data = res;
				var html = template('J_detail_tpl', data);
				$('#appealDetail').html(html);

				me.approvalTodo = res.approvalTodo;
				me.appealId = res.appealId;
			}
			me._ajax('/rider/attendance/appealDetail', {appealToken: appealToken, aop: window.aop}, callback)
		},
		_approve: function() {
			var me = this;
			var html = '<p>确认通过此骑手的申诉请求后将进入下一流程</p>';
			$('#confirmBox').show().find('.content').html(html);
		},
		_sureApprove: function() {
			var me = this;
			var callback = function() {
				me._historyBack();
			}
			me._ajax('/rider/attendance/appealApprove', {appealToken: me.appealToken, aop: window.aop, approve: true}, callback);
		},
		_cancelApprove: function(e) {
			$('#confirmBox').hide();
		},
		_reject: function() {
			$('#rejectBox').show();
		},
		_sureReject: function() {
			var me = this;
			var callback = function() {
				me._historyBack();
			}
			var reason = $('#reason').val();
			if (reason.trim()) {
				me._ajax('/rider/attendance/appealApprove', {appealToken: me.appealToken, aop: window.aop, approve: false, reason: reason}, callback);
			}else {
				alert('驳回理由不能为空');
			}
		},
		_historyBack: function() {
			location.href = '/rider/attendance/' + (window.aop == 'p' ? 'partner/' : '') + '#appealback';
		},
		_cancelReject: function() {
			$('#rejectBox').hide();
		},
		_formatDate: function(time) {
			if (!time) {
				return '';
			}
			var date = new Date(time);
			var y = date.getFullYear(),
				m = date.getMonth() + 1,
				d = date.getDate(),
				h = date.getHours(),
				f = date.getMinutes(),
				s = date.getSeconds();
			return y + '-' + (m > 9 ? m : '0' + m) + '-' + (d > 9 ? d : '0' + d) + ' ' + (h > 9 ? h : '0' + h) + ':' + (f > 9 ? f : '0' + f) + ':' + (s > 9 ? s : '0' + s);
		},
		_bind: function() {									// 绑定事件
			var me = this;
			var	context = me,
				events  = me.events,
				$wrap   = me.wrap;

			events && $.each(events, function(k, v) {
				var handler;
				me[v[2]] && (handler = me[v[2]]);

				var ev = v[0] + '.' + k,
					el = v[1];
				$wrap.off(ev).on(ev, el, function(e) {
					handler.apply(context, [e, this]);
				})
			})
		},
		_ajax: function(url, data, callback, type) {		// ajax
			var me = this;
			var type 	 = type || 'POST',
				callback = callback || noop;
			$.ajax({
				url: url,
				type: type,
				dataType: 'json',
				data: data,
				success: function(res) {
					console.log(res);
					if (res.code == 0) {
						callback(res.data, me);
					}else {
						alert('操作失败');
						console.warn(res.msg);
					}
				},
				error: function() {
					alert('请求{' + url + '}出错!');
				}
			})
		}
	}
	bm._init();
})