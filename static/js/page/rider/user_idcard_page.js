require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});
require(['module/root', 'module/validator','module/cookie'], function (t, validator,cookie) {

	$(".btn-success").click(function() {
	    var cardNo=$("#idCard").val();
        if(!isBlank(cardNo)){
            if(!isIdentityCardValid(cardNo)) {
                showError("请输入正确身份证号");
                return false;
            }else{
                $("#alert_error").empty();
                return true;
            }
        } else {
        	showError("请输入身份证号");
                return false;
        }
    })
    
	function showError(errMsg) {
    	$("#alert_error").empty();
    	$("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
    	$('body,html').animate({scrollTop:0},200);
    }
});