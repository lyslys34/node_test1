/**
 * Created by lixiangyang on 15/6/25.
 */

var orgSelect = $('#orgId');
var levelSelect=$('#levelType');
var levelType=-1;
var zijianOrgType=1;
var partnerOrgType = 2;
var cityProxyOrgType = 7;//城市代理站
var riderRoleCode=1001;
var managerRoleCode = 2001;

$(document).ready(function(){

    var riderAvatarData = {};

    $("#employeeId").keydown(function(event) {
        if (event.keyCode == 8) {
            $('#mis-id').val("");
            $('#employeeId').val("");
            $('.mis-search').hide();
        }
    });

    var lastTime;
    $("#employeeId").keyup(function(event) {
        lastTime = event.timeStamp;
        setTimeout(function() {
            if (lastTime - event.timeStamp == 0) {
                var date = new Date();
                $.getJSON('/employee/query?q=' + $('#employeeId').val()).success(function(data) {
                    var city_list = new Array();
                    if (data.data.length == 0) {
                        return;
                    }
                    $('.mis-search').show();
                    $('.mis-search-ul').empty();
                    for (var i in data.data) {
                        var name = data.data[i].name;
                        var id = data.data[i].id;
                        $('.mis-search-ul').append('<li onclick="add_to_input($(this));" data="'+id+'">' + name + '</li>');
                    }

                }).error(function(data) {
                });
            }
        }, 300);
    });

    add_to_input = function (e) {
        $('.mis-search').prev().val(e.text());
        $('#mis-id').val(e.attr('data'));
        $('.mis-search').hide();
    };


    $(".js_uploadbtn").click(function(){
        $(this).siblings(".js_fileupload").click();
    });

    $("#J_upload_avatar_click").click(function (){
      $("#J_upload_avatar").click();
    });

    $("#J_upload_avatar").change(function() {
        var file = this.files[0];
        if (file.size > 5 * 1000 * 1000) {
          return showError('您的图片太大了，请传输5M以下的图片');
        }
        var fc = new Widget.ScreenShot('result', {
            file: file,
            width: 400,
            height: 443,
            selectMaxWidth: 400,
            selectMaxHeight: 300,
            callback: cutout
          });
        fc.init();
        setTimeout(cutout, 300);
        function cutout() {
          var clipData = fc.clip();
          riderAvatarData = clipData;
          document.getElementById('J_avatar_preview').src = clipData.src;
        }
    });

    $("#orgId").change(function(){
        $("#orgType").val($("#orgId option:selected").attr("orgType"));
        $("#orgIdTemp").val($("#orgId option:selected").val());

        var levelType=$("#levelType").val();
        if(isBlank(levelType)){
            $("#levelType").val($("#orgId option:selected").attr("levelType"));
        }
    })

    $("#levelType").on("change",function(e){

        $("#originLevelType").val($(this).val());
        $("#orgTypeTemp").val($("#levelType option:selected").attr("orgType"));

        showOtherInfo(); //展示自建配送员的三个必填信息

        changeOrg($(this).val());


    });

    $("#roleType").on("change", function(e) {
        changeLevelType($(this).val());
    });

    $("#idcardNo").blur(function(){
        var cardNo=$("#idcardNo").val();
        if(!isBlank(cardNo)){
            if(!isIdentityCardValid(cardNo)) {
                showError("身份证号无法通过公安部数据库校验");
                $("#idcardNo").focus();
            }else{
                $("#alert_error").empty();
            }
        }
    })

    $("#createUserSubmit").click(function(){
        var data=validAndBuildData();
        if(data==null){
            return false;
        }
        var $this = $(this);
        $this.addClass("disabled");
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/rider/doCreate.ajax",
            data : data,
            contentType: false,
            processData: false,
            success : function(r){
                if (r) {
                    if(r.code == 0) {
                        if(isBlank(r.msg)){
                            window.location.href="/rider/list";
                        }else{
                            window.location.href=r.msg;
                        }
                    }else {
                        showError(r.msg);
                        $this.removeClass("disabled");
                    }
                } else {
                    showError("添加人员失败!");
                    $this.removeClass("disabled");
                }
            },
            error:function(r){
                showError("添加人员失败!请稍后重试！");
                $this.removeClass("disabled");
            }
        });

    })

    $("#updateUserSubmit").click(function(){
        var originOrgId=$("#originOrgId").val();
        var id=$("#id").val();

        var data=validAndBuildData();
        if(data==null){
            return false;
        }
        data.append('originOrgId', originOrgId);
        data.append('id', id);

        var $this = $(this);
        $this.addClass("disabled");
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/rider/doUpdate",
            data : data,
            contentType: false,
            processData: false,
            success : function(r){
                if (r) {
                    if(r.code == 0) {
                        if(isBlank(r.msg)){
                            window.location.href="/rider/list";
                        }else{
                            if (r.msg == "/partner/login/logout") {
                                alert("手机号修改成功。点击重新登录");
                            }
                            window.location.href=r.msg;
                        }
                    }else {
                        showError(r.msg);
                        $this.removeClass("disabled");
                    }
                } else {
                    showError("添加人员失败!");
                    $this.removeClass("disabled");
                }
            },
            error:function(r){
                showError("添加人员失败!请稍后重试！");
                $this.removeClass("disabled");
            }
        });

    })

    function validAndBuildData(){
        var orgId = $("#orgId").val();
        var gender = $("#gender").val();
        var jobType = $("#jobType").val();
        var faceUrlWithWatermark=$("#faceUrlWithWatermark").val();
        var idCardUrlWithWatermark=$("#idCardUrlWithWatermark").val();
        var orgType=$("#orgType").val();
        var roleType=$("#roleType").val();

        var name = $("#userName").val();
        var cardNo = $("#idcardNo").val();
        var phone = $("#userPhone").val();
        var faceUrl = $("#js_faceUrl").val();
        var cardUrl = $("#js_cardUrl").val();

        var houseHoldRegisterType=$("#houseHoldRegisterType").val();
        var directRecruit=$("#directRecruit").val();
        var laborDispatchingCompany=$("#laborDispatchingCompany").val();

        var employeeId = $.trim($('#mis-id').val());
        if(isBlank(employeeId)){
            employeeId = -1;
        }

        var riderAvatar = $('#rider-avatar').val();

        var entryDate = $.trim($('#entryDate').val());

        if(isBlank(name)) {
            showError("请填写姓名");
            return null;
        }

        if(isBlank(cardNo)) {
            showError("请填写身份证号码");
            return null;
        }
        if(isBlank(faceUrl)) {
            showError("请上传手持身份证照片");
            return null;
        }
        if(isBlank(cardUrl)) {
            showError("请上传身份证正面照");
            return null;
        }

        if(isBlank(phone)){
            showError("请填写手机号");
            return null;
        }

        if(!isMobile(phone)) {
            showError("请输入正确的手机号");
            return null;
        }
        if(!$(".headPortrait").hasClass("hide") && $("#J_avatar_preview").attr("src").indexOf("http://") == -1){
            //暂时注释，现在头像不是必填。
//            if(isBlank(riderAvatarData.src)) {
//                showError("请上传骑手头像");
//                return null;
//            }

            if (!isBlank(riderAvatarData.src) && riderAvatarData.width < 450) {
              return showError('您的图片不够清楚，请重新传输或者选择更大的区域');
            }
        }

        if(orgType==zijianOrgType&&roleType==riderRoleCode){
            if(isBlank(entryDate)) {
                showError("请填写入职日期");
                return null;
            }
        
            if(isBlank(houseHoldRegisterType)||houseHoldRegisterType==0){
                showError("请选择户口类型");
                return null;
            }
            if(isBlank(directRecruit)||directRecruit==0){
                showError("请选择是否直招");
                return null;
            }
            if(isBlank(laborDispatchingCompany)||laborDispatchingCompany==0){
                showError("请选择所属劳务派遣公司");
                return null;
            }

        }else{

            houseHoldRegisterType=0;
            directRecruit=0;
            laborDispatchingCompany=0;
            entryDate=null;
        }


        var formData = new FormData();
        formData.append('orgId', orgId);
        formData.append('name', name);
        formData.append('idcardNo', cardNo);
        formData.append('gender', gender);
        formData.append('jobType', jobType);
        formData.append('phone', phone);
        formData.append('faceUrl', faceUrl);
        formData.append('idcardUrl', cardUrl);
        formData.append('roleType', roleType);
        formData.append('orgType', orgType);
        formData.append('idCardUrlWithWatermark', idCardUrlWithWatermark);
        formData.append('faceUrlWithWatermark', faceUrlWithWatermark);
        formData.append('houseHoldRegisterType', houseHoldRegisterType);
        formData.append('directRecruit', directRecruit);
        formData.append('laborDispatchingCompany', laborDispatchingCompany);
        formData.append('employeeId', employeeId);
        if(!isBlank(entryDate)) {
            formData.append('entryDate',  entryDate);
        }

        if(!isBlank(riderAvatarData.src)){
            // base64图片转换成正常可接受形式
            var avatarData = riderAvatarData.src;
            avatarData = avatarData.split(',')[1];
            avatarData = window.atob(avatarData);
            var ia = new Uint8Array(avatarData.length);

            for (var i = 0; i < avatarData.length; i++) {
                ia[i] = avatarData.charCodeAt(i);
            };

            // canvas.toDataURL 返回的默认格式就是 image/png
            var blob = new Blob([ia], {type:"image/png"});

            formData.append('riderAvatar',blob);
        }


        return formData;
    }

    var uploadOption = {
        url:"/file/imguploadWithWatermark",
        add: function(e, data) {
            var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
            var maxFileSize = 5242880;
            if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                showError("文件格式不正确(gif,jpg,png)");
                return false;
            }
            if(data.originalFiles[0]['size'] && data.originalFiles[0]['size'] > maxFileSize) {
                showError("文件太大(5M)");
                return false;
            }
            data.context = $(this).siblings(".js_uploadbtn").addClass("disabled").val("正在上传").unbind();
            data.submit();
        },
        done: function (e, data) {
            $(this).siblings(".js_uploadbtn").val("点击上传").removeClass("disabled").bind("click", {}, function(){
                $(this).siblings(".js_fileupload").click();
            });

            if(data.result){
                if(data.result.code==0) {
                    if(data.result.data[0] && data.result.data[0].code == 0) {
                        var backUrl = data.result.data[0].url;
                        var backUrlWithWatermark=data.result.data[0].waterMarkUrl;

                        if (typeof backUrl == undefined || backUrl == null || backUrl == '') {
                            showError("上传失败，获取图片链接失败");
                            return false;
                        }

                        if(!isBlank(backUrlWithWatermark)){
                            $(this).siblings(".js_upload_back_url_with_watermark").val(backUrlWithWatermark);
                        }

                        $(this).siblings("img").css("display", "block").attr("src", backUrl);
                        $(this).siblings(".js_upload_back_url").val(backUrl);
                    } else {
                        showError("上传失败," + (data.result.data[0] ? data.result.data[0].msg : "未知错误"));
                    }
                } else {
                    showError("上传失败，" + data.result.msg);
                }
            } else {
                showError("上传失败");
            }
        }
    };


    function appendOrg(data) {
        var targetSelect = $("#orgId");
        var originOrgId = $("#orgIdTemp").val();
        if(data) {
            targetSelect.empty();
            $.each(data, function(index, item){
                var op = $(document.createElement("option")).val(item.id).text(item.value).attr('orgType', item.type);
                if(item.id==originOrgId) {
                    op.attr('selected', 'selected');
                }
                targetSelect.append(op);
            });
        }
    }

    function changeOrg(levelType){

//        var levelType=$("#levelType option:selected").val();
        var roleCode=$("#roleType option:selected").val();
        var orgType=$("#orgTypeTemp").val();
        orgSelect.empty();
        changeOrgByLevelTypeAndRoleCode(levelType,roleCode,orgType);
    }

    function changeOrgByLevelTypeAndRoleCode(levelType,roleCode,orgType){

        var url='/org/getByRoleAndLevelType';

        var data={
            'roleCode':0
        };

        if(!isBlank(levelType)){

            //如果没有组织层级则根据组织类型拉取组织
            if(levelType==0){
                data.orgType=orgType;
                url='/org/getByOrgType';
            }else{
                data.levelType=levelType;
            }
        }
        if(!isBlank(roleCode)){
            data.roleCode=roleCode;
        }

        $.ajax({
            dataType: 'json',
            data:data,
            type : 'post',
            url : url,
            success : function(r){
                if (r && r.code == 0) {
                    appendOrg(r.data);
                    orgSelect.select2();
                    $("#orgType").val($("#orgId option:selected").attr("orgType"));
                    $("#orgIdTemp").val($("#orgId option:selected").val());
                }
            }
        });
    }

    function changeLevelType(roleCode){

       levelSelect.empty();
       var url='/org/getLevelTypeByRoleCode';
       var originOrgType=$("#originOrgType").val();
       var data={'roleCode':roleCode}       

       if(!isBlank(originOrgType)){
           data.originOrgType=originOrgType;
       }

       $.ajax({
           dataType: 'json',
           data:data,
           type : 'post',
           url : url,
           success : function(r){
               if (r && r.code == 0) {
                   appendLevelType(r.data);
                   levelSelect.select2();
                   levelType=$("#levelType option:selected").val();
                   $("#originLevelType").val(levelType);
                   $("#orgTypeTemp").val($("#levelType option:selected").attr("orgType"));
                   showOtherInfo(); //展示自建配送员的三个必填信息
                   changeOrg(levelType);

               }
           }
       });
    }

    //如果是自建配送员则显示户口类型，所属派遣公司，是否直招的信息
    function showOtherInfo(){
        var roleCode=$("#roleType option:selected").val();
        var orgType=$("#orgTypeTemp").val();


        if(!isBlank(roleCode) && !isBlank(orgType) && (orgType==zijianOrgType&&roleCode==riderRoleCode)){
            $(".otherThree").css("display", "block");
            $('#entryDateDiv').show();
            //$('#entryDate').attr("disabled","");
            $('#entryDate').removeAttr("disabled");
        }else{

            $(".otherThree").css("display", "none");

            $("#houseHoldRegisterType").val("0");
            $("#directRecruit").val("0");
            $("#laborDispatchingCompany").val("0");
            $('#entryDateDiv').hide();
            $('#entryDate').attr("disabled","disabled");
        }

        if (!isBlank(roleCode) && !isBlank(orgType) && (orgType==zijianOrgType&&roleCode==managerRoleCode)) {
            $('#misDiv').css("display", "block");
        } else {
            $('#misDiv').css("display", "none");
            $('#mis-id').val("");
            $('#employeeId').val("");
            $('.mis-search').hide();
        }

        //如果是自建和加盟和城市代理站配送员，显示上传头像
        if(!isBlank(roleCode) && !isBlank(orgType) && ((orgType==zijianOrgType || orgType == partnerOrgType || orgType == cityProxyOrgType)&&roleCode==riderRoleCode)){
            $(".headPortrait").removeClass("hide");
        }else{
            $(".headPortrait").addClass("hide");
        }
    }

    var crowOrgType=4;
    var jiaomaOrgType=5;
    function appendLevelType(data){
        var targetSelect = $("#levelType");
        var originLevelTypeId = $("#originLevelType").val();
        var orgTypeTemp=$("#orgTypeTemp").val();
        if(data) {
            targetSelect.empty();
            $.each(data, function(index, item){
                var op = $(document.createElement("option")).val(item.code).text(item.name).attr('orgType', item.orgType);
                if(orgTypeTemp==jiaomaOrgType){
                    if(item.orgType==orgTypeTemp){
                        op.attr('selected', 'selected');
                    }
                }else{
                    if((item.code==originLevelTypeId) && (item.orgType==orgTypeTemp)) {
                        op.attr('selected', 'selected');
                    }
                }
                targetSelect.append(op);
            });
        }
    }
    
    function changeEntryDayDiv() {
        if($("#orgType").val() == '1' && $("#roleType").val() == '1001') {
            $('#entryDateDiv').show();
            $('#entryDate').attr("disabled","");
        } else {
            $('#entryDateDiv').hide();
            $('#entryDate').attr("disabled","disabled");
        }
    }

    var fileupload = $(".js_fileupload").fileupload(uploadOption);


    $("#roleType").change();

    function showError(errMsg) {
        $("#alert_error").empty();
        $("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
        $('body,html').animate({scrollTop:0},200);
    }
})
