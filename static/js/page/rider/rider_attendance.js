require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
		deps: ['lib/jquery']
    },
    'lib/artTemplate': {
    	exports: 'template'
    }
  }
});
require(['module/root', 'lib/artTemplate'], function (root, template) {
	var noop = function() {};
	// 一些相关的事件绑定
	(function() {
		$('body').delegate('.calendar-table tbody td', 'mouseover', function(e) {
			$(this).addClass('hover').siblings('td').addClass('hover');
		})
		$('body').delegate('.calendar-table tbody td', 'mouseout', function(e) {
			$(this).removeClass('hover').siblings('td').removeClass('hover');
		})
	})()

	var bm = {
		wrap: $('body'),
		week: {
			sdate: '',
			edate: ''
		},
		page: {
			pageNum: 1,
			totalPage: null
		},
		appealPage: {
			pageNum: 1,
			totalPage: null
		},
		rid: null,				// 骑手id, 查询骑手月考勤需要
		isInit: false,
		events: [
			['click', '.mtab_item', '_tab']
			,['click', '#changeShouldDays', '_changeShouldDays']
			,['click', '#submitShouldDays', '_submitShouldDays']
			,['click', '#cancelEditShouldDays', '_cancelShouldDays']
			,['click', '.week_arrow', '_siblingWeek']
			,['click', '.page_btn', '_pageSwitch']					// 分页
			,['click', '.appeal_page_btn', '_appealPageSwitch']					// 分页
			,['click', '#searchBtn', '_searchWeekList']
			,['click', '.rider_item', '_popup']
			,['click', '#closeMonthList', '_closeMonthList']
			,['click', '#monthPickerBtn', '_pickMonth']				// 选择年月
			,['click', '.month_arrow', '_siblingMonth']
			,['click', '#submitMonthPick', '_submitMonthPick']
			,['click', '#cancelMonthPick', '_cancelMonthPick']
			,['click', '#appealSearchBtn', '_searchAppeal']
		],
		_init: function() {
			var me = this;

			me._aop();
			me._bind();
			me._weekPicker();
			me._getStations();

			if (/appealback/ig.test(location.href)) {
				$('.mtab_item').eq(1).click();
			}
		},
		_tab: function(e) {
			var me = this;
			var $tab = $(e.target);
			$tab.addClass('on').siblings().removeClass('on');
			var page = $tab.data('page');
			$('#' + page).removeClass('hide').siblings('.page').addClass('hide');

			if (page == 'page2' && !me.isInit) {
				me._initPage2();
			}
		},
		_aop: function() {
			if (window.aop == 'p') {
				$('.aop').hide();
			}
		},
		// 查询用户权限下所有站点
		_getStations: function() {
			var me = this;
			// todo 判断A端P端
			var callback = function(res) {
				var html = '';
				html += '<option value="0" selected>全部站点</option>';
				for (var i in res) {
					html += '<option value="' + i + '">' + res[i] + '</option>';
				}
				$stationSelect.html(html);
				$stationSelect.select2({width: 100});
			}
			me._ajax('/rider/attendance/stations', {}, callback, 'GET');
		},
		_searchWeekList: function(page) {
			var me = this;
			var page = isNaN(page) ? 1 : page;
			var data = {
				stationId: $stationSelect.val(),
				riderName: $('#rname').val(),
				riderMobile: $('#rphone').val(),
				sdate: $('#weekString').data('sdate'),
				edate: $('#weekString').data('edate'),
				pageNum: page,
				pageSize: 20
			}
			var callback = function(res) {
				var table = {
					thead: res.tableHead,
					tbody: res.tableBody,
					pageNum: res.pageNum,
					totalPage: res.pageCount
				}
				var html = template('J_week_tpl', table);
				$('#weekList').html(html);

				me.page.pageNum = res.pageNum;
				me.page.totalPage = res.pageCount;
			}
			me._ajax('/rider/attendance/weekList', data, callback);
		},
		// 切换页面
		_pageSwitch: function(e) {
			var me = this;
			var $btn = $(e.target);
			var n = $btn.data('page') == 'n' ? $('#pageInput').val() : $btn.data('page');

			if (n == me.page.pageNum) return false;			// 是当前页面
			if (n > 0 && n <= me.page.totalPage) {
				me._searchWeekList(n);
			}
		},
		// 周选择功能 begin
		// 通过daterangepicker选择周
		_weekPicker: function() {
			var me = this;
			// 初始化日历选择器
			$('#weekPicker').daterangepicker({
				singleDatePicker: true,
				startOfWeek: 'monday',
				locale: {
					firstDay: 1,
			        daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
			        monthNames: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
			    }
			}, function(value) {
				var $td = $(event.target);
				var date = value.format('YYYY-MM-DD');
				var date = {
					year: date.split('-')[0],
					month: date.split('-')[1],
					first: $td.parent().find('td').eq(0).text(),
					last: $td.parent().find('td').eq(6).text()
				}
				me._calculateWeek(date);
			});

			me._initWeek();
		},
		// 页面初始化时选择当前周
		_initWeek: function() {
			var me = this;
			$('#weekPicker').click();
			var $table = $('.calendar-table').eq(0).find('.active');
			var $month = $table.parents('.calendar-table').find('thead .month');
			var yearMonth = $month.text();
			var date = {
				year: yearMonth.split(/\s/)[1],
				month: yearMonth.split(/\s/)[0],
				first: $('.calendar-table .active').parent().find('td').eq(0).text(),
				last: $('.calendar-table .active').parent().find('td').eq(6).text()
			}
			me._calculateWeek(date);
			$('.daterangepicker .active').click();
		},
		// 选择上or下一周
		_siblingWeek: function(e) {
			var me = this;
			var $arrow = $(e.target);
			var $week = $('#weekString');

			var sdate = me.week.sdate.split('-');
			var edate = me.week.edate.split('-');
			var sdate = {
				year: Number(sdate[0]),
				month: Number(sdate[1]),
				day: Number(sdate[2])
			}
			var edate = {
				year: Number(edate[0]),
				month: Number(edate[1]),
				day: Number(edate[2])
			}

			if ($arrow.data('type') == 'next') {
				var newSDate = me._nextWeek(sdate);
				var newEDate = me._nextWeek(edate);
			}else {
				var newSDate = me._prevWeek(sdate);
				var newEDate = me._prevWeek(edate);
			}
			me._changeWeek(newSDate, newEDate);
		},
		// 计算选择的周
		_calculateWeek: function(date) {
			var me = this;
			var year  = parseInt(date.year),
				month = parseInt(date.month),
				first = parseInt(date.first),
				last  = parseInt(date.last);
			if (first > last) {				// 跨月
				var temp = new Date(year, month - 1, last);
				var day  = temp.getDay();
				if (day == 0) {				// 说明选择日期所属的月份是后一个月
					var edate = year + '-' + (month < 10 ? '0' + month : month) + '-' + (last < 10 ? '0' + last : last);		// end date
					if (month == 1) {
						--year;
						month = 12;
					}else {
						--month;
					}
					var sdate = year + '-' + (month < 10 ? '0' + month : month) + '-' + (first < 10 ? '0' + first : first); 	// start date
				}else {
					var sdate = year + '-' + (month < 10 ? '0' + month : month) + '-' + (first < 10 ? '0' + first : first);
					if (month == 12) {
						++year;
						month = 1;
					}else {
						++month;
					}
					var edate = year + '-' + (month < 10 ? '0' + month : month) + '-' + (last < 10 ? '0' + last : last);
				}
			}else {
				var sdate = year + '-' + (month < 10 ? '0' + month : month) + '-' + (first < 10 ? '0' + first : first);
				var edate = year + '-' + (month < 10 ? '0' + month : month) + '-' + (last < 10 ? '0' + last : last);
			}
			me._changeWeek(sdate, edate);
		},
		// 下一周
		_nextWeek: function(date) {
			var me = this;
			var year  = date.year,
				month = date.month,
				day   = date.day;
			var days  = me._calculateMonthDays(year, month);

			if ((days - day) < 7) {							// 下一周在下一个月
				var newDay = day + 7 - days;
				var newYear = month == 12 ? ++year : year;	// 下一个月在下一年？
				var newMonth = month == 12 ? 1 : ++month;
			}else {
				var newDay = day + 7;
				var newMonth = month;
				var newYear = year;
			}
			return newYear + '-' + (newMonth < 10 ? '0' + newMonth : newMonth) + '-' + (newDay < 10 ? '0' + newDay : newDay);
		},
		// 上一周
		_prevWeek: function(date) {
			var me = this;
			var year  = date.year,
				month = date.month,
				day   = date.day;
			if (day > 7) {			// 上周在本月
				var newDay = day - 7,
					newMonth = month,
					newYear = year;
			}else {					// 上周在上月
				var newYear = month == 1 ? --year : year;
				var newMonth = month == 1 ? 12 : --month;
				var days = me._calculateMonthDays(newYear, newMonth);
				var newDay = days - (7 - day);
			}
			return newYear + '-' + (newMonth < 10 ? '0' + newMonth : newMonth) + '-' + (newDay < 10 ? '0' + newDay : newDay);
		},
		// 计算该月有多少天
		_calculateMonthDays: function(year, month) {
			var days;
			var isBigYear = !(year % 4) && !!(year % 100) || !(year % 400);
			if ( (month > 7 && !(month % 2)) || (month < 8 && month % 2) ) {
				days = 31;
			}else {
				if (month == 2) {
					days = isBigYear ? 29 : 28;
				}else {
					days = 30;
				}
			}
			return days;
		},
		// 改变周选择条件
		_changeWeek: function(sdate, edate) {
			var me = this;
			me.week.sdate = sdate;
			me.week.edate = edate;
			var $week = $('#weekString');
			$week.html(sdate + '<span>至</span>' + edate);
			$week.data('sdate', sdate.split('-').join(''));
			$week.data('edate', edate.split('-').join(''));

			var year = $week.data('edate').substr(0, 4);
			var month = $week.data('edate').substr(4, 2);
			var oldMonth = $('#thisMonth').text();
			if (month != oldMonth) {			// 更新每月应当出勤数
				$('#thisMonth').text(month);
				$('#thisYear').val(year);
				var callback = function(res) {
					$('#shouldDays').text(res)
				}
				me._ajax('/rider/attendance/expectDays', {year: year, month: month}, callback);
			}
			me._searchWeekList(1);
		},
		// 改变每月应出勤天数
		_changeShouldDays: function() {
			var now = new Date();
			var year = now.getFullYear(),
				month = now.getMonth() + 1;
			var select1 = '<label>年份：<select id="shouldDaysYearSelect">';
			for (var i = 0; i < 4; i ++) {
				var str1 = '<option value=\"' + (year - 1 + i) + '\" ' + (i == 1 ? 'selected' : '') + '>' + (year - 1 + i) + '</option>';
				select1 += str1;
			}
			select1 += '</select></label>';
			var select2 = '<label>月份：<select id="shouldDaysMonthSelect">';
			for (var i = 1; i < 13; i ++) {
				var str2 = '<option value=\"' + i + '\" ' + (i == month ? 'selected' : '') + '>' + i + '</option>';
				select2 += str2;
			}
			select2 += '</select></label>';
			var html = select1 + select2;

			$('.shouldday_select_box').html(html);
			$('#editShouldDays').addClass('show');
			$('#bgWrapper').addClass('show');
		},
		_submitShouldDays: function() {
			var me = this;
			var data = {
				year: $('#shouldDaysYearSelect').val(),
				month: $('#shouldDaysMonthSelect').val(),
				days: parseInt($('#shouldDaysInput').val())
			}
			if (isNaN(data.days)) {
				alert('请输入具体天数');
			}
			var callback = function() {
				me._cancelShouldDays();
			}
			me._ajax('/rider/attendance/expectDaysUpdate', data, callback)
		},
		_cancelShouldDays: function() {
			$('#editShouldDays').removeClass('show');
			$('#bgWrapper').removeClass('show');
		},
		// 弹出考勤月历
		_popup: function(e) {
			var me = this;
			var $td = $(e.currentTarget);
			var rid = $td.data('rid');
			var year = $('#thisYear').val();
			var month = $('#thisMonth').text();
			me.rid = rid;
			me._getMonthList(year, month, rid)
		},
		// 获取考勤月数据
		_getMonthList: function(year, month, rid) {
			var me = this;
			var data = {
				riderId: rid,
				date: year + '' + month
			}
			var callback = function(res) {
				var data = me._renderMonth(res);
				var html = template('J_month_tpl', data);
				$('#monthList').html(html).addClass('show');
				$('#bgWrapper').addClass('show');
			}
			me._ajax('/rider/attendance/monthList', data, callback);
		},
		// 改变月查询条件
		_siblingMonth: function(e) {
			var me = this;
			var $month = $('#monthString');
			var $arrow = $(e.target);
			var type = $arrow.data('type');
			var year = parseInt($month.data('year')),
				month = parseInt($month.data('month'));
			if (type == 'next') {
				var year = month == 12 ? ++year : year;
				var month = month == 12 ? 1 : ++month;
			}else {
				var year = month == 1 ? --year : year;
				var month = month == 1 ? 12 : --month;
			}
			me._changeMonth(year, month);
		},
		_pickMonth: function() {
			var now = new Date();
			var year = now.getFullYear(),
				month = now.getMonth() + 1;
			var select1 = '<label>年份：<select id="monthYearSelect">';
			for (var i = 0; i < 4; i ++) {
				var str1 = '<option value=\"' + (year - 1 + i) + '\" ' + (i == 1 ? 'selected' : '') + '>' + (year - 1 + i) + '</option>';
				select1 += str1;
			}
			select1 += '</select></label>';
			var select2 = '<label>月份：<select id="monthMonthSelect">';
			for (var i = 1; i < 13; i ++) {
				var str2 = '<option value=\"' + i + '\" ' + (i == month ? 'selected' : '') + '>' + i + '</option>';
				select2 += str2;
			}
			select2 += '</select></label>';
			var html = select1 + select2;
			$('#monthPickerSelectBox').html(html);
			$('#monthPickerBox').addClass('show');
		},
		_submitMonthPick: function() {
			var me = this;
			var year = $('#monthYearSelect').val(),
				month = $('#monthMonthSelect').val();
			$('#monthPickerBox').removeClass('show');
			me._changeMonth(year, month);
		},
		_cancelMonthPick: function() {
			$('#monthPickerBox').removeClass('show');
		},
		_changeMonth: function(year, month) {
			var me = this;
			var $month = $('#monthString');
			$month.text(year + '年' + month + '月');
			$month.data('year', year);
			$month.data('month', month);

			me._getMonthList(year, month, me.rid);
		},
		// 渲染月日历数据
		_renderMonth: function(data) {
			var year = data.year,
				month = data.month;
			var firstDay = (new Date(year, month - 1, 1)).getDay();
			var emptyDays = firstDay > 0 ? firstDay - 1 : 6;
			while (emptyDays --) {
				var emptyDay = {};
				data.dataList.unshift(emptyDay);
			}
			data.monthList = [];
			var arr = [];
			if (!data.dataList) {return false;}
			for (var i = 0, n = data.dataList.length; i < n; i++) {
				if (i > 0 && !(i % 7)) {
					data.monthList.push(arr);
					arr = [];
					arr.push(data.dataList[i])
				}else {
					arr.push(data.dataList[i])
				}
			}
			data.monthList.push(arr);
			return data;
		},
		// 关闭月历
		_closeMonthList: function() {
			$('#monthList').removeClass('show');
			$('#bgWrapper').removeClass('show');
		},
		// 周选择功能 end
		// page 2
		_initPage2: function() {
			var me = this;
			$('#statusSelect').select2({width: 100});
			me._searchAppeal(1);
			me.isInit = true;
		},
		_searchAppeal: function(page) {
			var me = this;
			var page = isNaN(page) ? 1 : page;
			var data = {
				aop: window.aop,
				appealStatus: $('#statusSelect').val(),
				appealToken: $('#appealToken').val(),
				riderName: $('#appealName').val(),
				riderMobile: $('#appealPhone').val(),
				pageNum: page,
				pageSize: 20
			}
			var callback = function(res) {
				if (!res.tableBody) {
					return false;
				}
				for (var i = 0, n = res.tableBody.length; i < n; i ++) {
					res.tableBody[i].appealTime = me._formatDate(res.tableBody[i].appealTime);
				}
				var table = {
					thead: res.tableHead,
					tbody: res.tableBody,
					pageNum: res.pageNum,
					totalPage: res.pageCount
				}
				var html = template('J_appeal_tpl', table);
				$('#appealList').html(html);

				me.appealPage.pageNum = res.pageNum;
				me.appealPage.totalPage = res.pageCount;
			}
			me._ajax('/rider/attendance/appealList', data, callback)
		},
		// 切换页面
		_appealPageSwitch: function(e) {
			var me = this;
			var $btn = $(e.target);
			var n = $btn.data('page') == 'n' ? $('#appealPageInput').val() : $btn.data('page');

			if (n == me.appealPage.pageNum) return false;			// 是当前页面
			if (n > 0 && n <= me.appealPage.totalPage) {
				me._searchAppeal(n);
			}
		},
		_formatDate: function(time) {
			var date = new Date(time);
			var y = date.getFullYear(),
				m = date.getMonth() + 1,
				d = date.getDate(),
				h = date.getHours(),
				f = date.getMinutes(),
				s = date.getSeconds();
			return y + '-' + (m > 9 ? m : '0' + m) + '-' + (d > 9 ? d : '0' + d) + ' ' + (h > 9 ? h : '0' + h) + ':' + (f > 9 ? f : '0' + f) + ':' + (s > 9 ? s : '0' + s);
		},
		_bind: function() {									// 绑定事件
			var me = this;
			var	context = me,
				events  = me.events,
				$wrap   = me.wrap;

			events && $.each(events, function(k, v) {
				var handler;
				me[v[2]] && (handler = me[v[2]]);

				var ev = v[0] + '.' + k,
					el = v[1];
				$wrap.off(ev).on(ev, el, function(e) {
					handler.apply(context, [e, this]);
				})
			})
		},
		_ajax: function(url, data, callback, type) {		// ajax
			var me = this;
			var type 	 = type || 'POST',
				callback = callback || noop;
			$.ajax({
				url: url,
				type: type,
				dataType: 'json',
				data: data,
				success: function(res) {
					callback(res.data, me);
				},
				error: function() {
					alert('请求{' + url + '}出错!');
				}
			})
		}
	}
	bm._init();
})