require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/root'], function(t){

    function validate() {
        var misId = $('#misId').val();
        if ($.trim(misId) === "") {
            alert("misId不能为空");
            return false;
        }
        var name = $('#name').val();
        if ($.trim(name) === "") {
            alert("姓名不能为空");
            return false;
        }
        var orgId = $('#orgId').val();
        if ($.trim(orgId) === "") {
            alert("组织ID不能为空");
            return false;
        }
        return true;
    }

    $(document).delegate('#send', 'click', function(){
        if ($(this).hasClass('disabled')) {
            return false;
        }
        var bool = validate();
        if (bool == false) {
            return false;
        }
        $('#send').addClass('disabled');
        var misId = $.trim($('#misId').val());
        var name = $.trim($('#name').val());
        var orgId = parseInt($.trim($('#orgId').val()));
        var mobile = $.trim($('#mobile').val());
        var postData = {
            misId: misId,
            name: name,
            orgId:orgId,
            mobile:mobile
        };
        var url = "/headquarter/add";
        t.utils.post(url, postData, function (r) {
            if (r.httpSuccess && r.data.code == 0) {
                $('#send').removeClass('disabled');
                alert('添加成功');
                $(this).removeClass('disabled');
            } else {
                $('#send').removeClass('disabled');
                alert(r.data.msg);
                $(this).removeClass('disabled');
            }
        });
    });
});