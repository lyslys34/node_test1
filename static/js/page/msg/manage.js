require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root', 'module/validator','module/cookie','lib/datepicker'], function (t, validator,cookie,datepicker) {
    //未读
    $("[name=unread]").click(function(){
        var id = $(this).attr("data");
        getMsgUsers(id,true,1,true);
    });

    //已读
    $("[name=send]").click(function(){
        var id = $(this).attr("data");
        //弹窗展示
        getMsgUsers(id,false,1,true);
    });
    datepicker.set();
    //下线
    $("[name=offline]").click(function(){
        if (!confirm('确定要将该条通知下线吗?')) return;
        var id = $(this).parent().attr("id");
        var postData = {id:id};
        var url = "/msg/offline.ajax";
        post(url,postData);

    });

    //删除
    $("[name=delete]").click(function(){
        if (!confirm('确定要删除该条通知吗?')) return;
        var id = $(this).parent().attr("id");
        var postData={id:id};
        var url="/msg/delete.ajax";
        post(url,postData);
    });

    //置顶
    $("[name=top]").click(function(){
        if (!confirm('确定要将该条通知置顶吗?')) return;
        var id = $(this).parent().attr("id");
        var postData={id:id};
        var url="/msg/top.ajax";
        post(url,postData);
    });

    //取消置顶
    $("[name=cancelTop]").click(function(){
        if (!confirm('确定要取消该条通知置顶吗?')) return;
        var id = $(this).parent().attr("id");
        var postData={id:id};
        var url="/msg/cancelTop.ajax";
        post(url,postData);
    });

    //未读人员翻页
    $(document).delegate('.turn-over', 'click', function() {
        var id = $(this).attr("id");
        var read = $("#userPage").attr("read");
        var pageNo = $(this).attr("pageNo");
        getMsgUsers(id,read,pageNo,false);
    });

//    //定时下线
//    $("#setOfflineTime").click(function(){
//        if($("#setOfflineTime")[0].checked){
//           $("#offlineTime").attr("disabled",false).removeClass("readonly");
//        }else{
//            $("#offlineTime").attr("disabled","disabled").addClass("readonly");
//        }
//    });

    //显示未读人数弹窗
    function _showUserPanel(title){
        var body = $("#unread-container");
        body.removeClass("hide");
        var dlg = t.ui.showModalDialog({
            title:"【"+title+"】",
            body: body,
            style:{".modal-content":{"width":"600px"},".modal-footer":{"display":"none"}}
        });
    }

    //获取未读人员名单
    function getMsgUsers(id,read,pageNo,showUserPanel){
        $('#userHeader').empty();
        $("#userList").empty();
        $("#userPage").empty();
        var postData = {id:id,pageNum:pageNo};
        var url = "/msg/sendUser.ajax";
        var title = "应发人员";
        if(read == true || read == "true"){
            url = "/msg/unreadUser.ajax";
            title = "未读人员";
        }
        t.utils.post(url, postData, function (r) {
          if (r.httpSuccess) {
              if(r.data.code == 0) {
                  var page = r.data.data;
                  if(page != null && page.data != null){
                     if(page.data[0].id == void 0){
                        $('#userHeader').html($('#shopHeaderTemplate').html());
                        if(read == true || read == "true"){
                            title = "未读商家";
                        }else{
                            title = "应发商家";
                        }
                        var str= ''
                        $.each(page.data,function(i,shop){
                            str+='<tr><td>'+shop.poiId+'</td><td>'+shop.poiName+'</td><td>'+shop.cityName+'</td><td>'+shop.deliveryAreaName+'</td><td>'+shop.poiPhone+'</td></tr>'
                        })
                        $("#userList").html(str);
                     }else{
                        $('#userHeader').html($('#userHeaderTemplate').html());
                        $.each(page.data,function(i,user){
                            var tr = $("<tr></tr>");
                            var nameTd = $("<td></td>");
                            nameTd.text(user.name);
                            tr.append(nameTd);
                            tr.append('<td>'+user.mobile+'</td>');
                            var genderStr = '未知';
                            if(user.gender == 1){
                                genderStr = '男';
                            }
                            if(user.gender == 2){
                                genderStr = '女';
                            }
                            tr.append('<td>'+genderStr+'</td>');

                            var cityName = "";
                            var orgName = "";
                            var type = "";
                            var userOrg = user.userOrg;
                            if(userOrg != null){
                                var baseData = userOrg.baseData;
                                if(baseData != null){
                                    var cityView = baseData.cityView;
                                    if(cityView != null){
                                        cityName = cityView.name;
                                    }
                                    orgName = baseData.name;
                                    type = getOrgTypeName(baseData.type);
                                }

                            }

                            tr.append('<td>'+cityName+'</td>');
                            tr.append('<td>'+orgName+'</td>');
                            tr.append('<td>'+type+'</td>');

                            var userRole = user.userRole;
                            var role = "";
                            if(userRole != null){
                                role = userRole.name;
                            }
                            tr.append('<td>'+role+'</td>');
                            $("#userList").append(tr);
                         });
                     }
                     pager(id,read,page.pageNo,page.pageSize,page.totalCount);
                     if(showUserPanel){
                        //弹窗展示
                        _showUserPanel(title);
                     }
                  }
              } else {
                  showMsg("获取数据失败！" + r.data.msg);
                  return false;
              }

          } else {
              showMsg("获取数据失败,系统错误!");
              return false;
          }
        });
    }

    function pager(id,read,pageNo,pageSize,recordCount){
        var pageCount = Math.ceil(recordCount/pageSize);
        //上一页处理
        var userPageUl = $("#userPage");
        userPageUl.attr("read",read);
        userPageUl.empty();
        if(pageNo == 1){
            userPageUl.append('<li class="disabled"><a href="javascript:;">上一页</a></li>');
        }else{
            userPageUl.append('<li><a href="javascript:;" class="turn-over" id='+id+' pageNo='+(pageNo-1)+'>上一页</a></li>');
        }
        //如果前面页数过多,显示...
        var start = 1;
        if(pageNo > 4){
            start = pageNo - 1;
            userPageUl.append('<li><a href="javascript:;" class="turn-over" id='+id+' pageNo=1>1</a></li>');
            userPageUl.append('<li><a href="javascript:;" class="turn-over" id='+id+' pageNo=2>2</a></li>');
            userPageUl.append('<li><a href="javascript:;">...</a></li>');
        }
        //显示当前页号和它附近的页号
        var end = pageNo+1;
        if(end > pageCount){
            end = pageCount;
        }
        for(var i = start; i <= end;i++){
            if(pageNo == i){
                userPageUl.append('<li class="active" ><a href="javascript:;">'+i+'</a></li>');
            }else{
                userPageUl.append('<li><a href="javascript:;" class="turn-over" id='+id+' pageNo='+i+'>'+i+'</a></li>');
            }
        }
        //如果后面页数过多,显示...
        if(end < pageCount -2){
            userPageUl.append('<li><a href="javascript:;">...</a></li>');
        }
        if(end < pageCount -1){
            userPageUl.append('<li><a href="javascript:;" class="turn-over" id='+id+' pageNo='+(pageCount - 1)+'>'+(pageCount-1)+'</a></li>');
        }
        if(end < pageCount){
            userPageUl.append('<li><a href="javascript:;" class="turn-over" id='+id+' pageNo='+pageCount+'>'+pageCount+'</a></li>');
        }
        //下一页处理
        if(pageNo == pageCount){
            userPageUl.append('<li class="disabled"><a href="javascript:;">下一页</a></li>');
        }else{
            userPageUl.append('<li><a href="javascript:;" class="turn-over" id='+id+' pageNo='+(pageNo+1)+'>下一页</a></li>');
        }
    }

    function showMsg(msg,reload){
        var body = $('<div style="height: 100px;padding: 15px;text-align: center;">'+msg+'</div>');
        t.ui.showModalDialog({
            body: body, buttons: [
                t.ui.createDialogButton('close','关闭', function () {     // 取消
                    if(reload){
                        location.reload(true);
                    }
                })
            ],
            style:{".modal-dialog":{"padding":"100px 110px"},
                   ".modal-footer":{"padding":"10px"},
                   ".btn-default":{"font-size":"12px"}
            }
        });
    }

    function post(url,postData){
        t.utils.post(url, postData, function (r) {
          if (r.httpSuccess) {
              if(r.data.code == 0) {
                  location.reload(true);
                  return false;
              } else {
                  showMsg("提交失败！" + r.data.msg);
                  return false;
              }

          } else {
              showMsg("提交失败,系统错误!");
              return false;
          }
        });
    }

    function getOrgTypeName(type){
        //SELF(1,"自建"), PROXY(2,"加盟"), ZHUDIAN(3,"驻店三方"), ZHONGBAO(4,"众包"), JIAOMA(5, "角马");
        var typeMap = {1:"自建",2:"加盟",3:"驻店三方",4:"众包",5:"角马"};
        var typeName =  typeMap[type];
        if(typeName == "undefined" || typeName == undefined){
            return '未知';
        }else{
            return typeName;
        }
    }
});