require.config({
    baseUrl: MT.STATIC_ROOT + '/js',
    urlArgs: 'ver=' + pageVersion,
});
require(['module/root'], function(t) {
    function getParameter(key){
        var value = window.location.search.match(new RegExp("[?&]"+key+"=([^&]*)(&?)"),"i");
        return value?decodeURIComponent(value[1]):'';
    }
    $(document).ready(function() {
        if(!getParameter('id')){
            selectArea();
            selectPOIArea();
        }
        $('#expiry_time').bind('focus',function(){
            if(!$(this).val()){
                var today = new Date(),
                date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                $(this).val(date+' 23:59:59');
                WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'%y-%M-%d %H:%m:%s',onpicked:function(){$('#expiry_time').trigger('blur')}});
                $(this).val('');
            }else{
                WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'%y-%M-%d %H:%m:%s',onpicked:function(){$('#expiry_time').trigger('blur')}})
            }
        })
        // umeditor
        var um = UM.getEditor('umeditor', {
            initialFrameHeight: '300',
            initialFrameWidth: '600',
            // zIndex: 300
        });
        if ($('#edit_text').val().length > 0) {
            var html = decodeURIComponent($('#edit_text').val());
            um.setContent(html);
        }

        um.addListener('fullscreenchanged',function(){
            if($('.edui-container').width()<600){
                setTimeout(function(){
                    $('.edui-container').css('z-index',300);
                },0)
            }else{
                setTimeout(function(){
                    $('.edui-container').css('z-index',1000);
                },0)
            }
        })

        initPage();

        $("input[name='big_type']").click(function() {
            showInputByBigType();
            if($(this).val()==102){
                $('input[name="channel_type"]').val(2)
            }else{
                $('input[name="channel_type"]').val(1)
            }
            if($(this).val()==103){
                $('#areaWrap').addClass('hide');
                $('#poiAreaWrap').removeClass('hide');
                $('input[name=ios]').attr('disabled','disabled').get(0).checked = false;
            }else{
                $('#areaWrap').removeClass('hide');
                $('#poiAreaWrap').addClass('hide');
                $('input[name=ios]').removeAttr('disabled');
            }
            $('input[name=area]').get(0).checked =true;
            $('input[name=poiArea]').get(0).checked =true;
            $('input[name=area]').trigger('change');
            $('input[name=poiArea]').trigger('change');
        });

        //选择区域 全国or部分区域
        $("input[name='area'],input[name='big_type']").change(function() {
            selectArea();
        });

        //选择区域 全国or部分区域
        $("input[name='poiArea'],input[name='big_type']").change(function() {
            selectPOIArea();
        });

        //切换组织类型
        $("#orgType").change(function() {
            getTopOrgByOrgType();
        });

        //选择烽火台用户
         $('#users').bind('change',function(){
            g_user_ids = $('#users').val();
         })

        //添加组织
        $("#addOrg").click(function(){
            var topOrg = $("#orgType").next(".orgSelect").val();
            var selectedOrgs = $(this).next().find("textarea").val();
            if(topOrg == -1){
                var orgType = $("#orgType").val();
                for(var i = 0; i < g_org_types.length; i++){
                    if(orgType == g_org_types[i]){
                        return false;
                    }
                }
                g_org_types.push(orgType);
                var orgTypeName = $("#orgType :selected").text();
                $("#selectedOrgs").append('<li class="selected"><a name="deleteOrgType" href="javascript:;" data='+orgType+' class="select2-selection__choice__remove">x</a>'+orgTypeName+'</li>');
                return false;
            }

            var currentOrgSelect = $(".orgSelect:last");
            var currentOrgId = currentOrgSelect.val();
            if(currentOrgId == -1){
                currentOrgSelect = currentOrgSelect.prev().prev();
            }
            var currentOrgId = currentOrgSelect.find(":selected").attr("value");
            for(var i = 0; i < g_org_ids.length; i++){
                if(currentOrgId == g_org_ids[i]){
                    return false;
                }
            }

            g_org_ids.push(currentOrgId);
            var orgName = currentOrgSelect.find(":selected").text();
            $("#selectedOrgs").append('<li class="selected"><a name="deleteOrg" href="javascript:;" data='+currentOrgId+' class="select2-selection__choice__remove">x</a>'+orgName+'</li>');
            $("#selectedOrgs").parent().css("height",$("#selectedOrgs").height()+10);
        });

        //删除组织类型
        $(document).delegate("[name=deleteOrgType]","click",function(){
            $(this).parent().remove();
            var orgType = $(this).attr("data");
            for (var i = 0; i < g_org_types.length; i++) {
                if (orgType == g_org_types[i]) {
                    g_org_types.splice(i,1);
                    $("#selectedOrgs").parent().css("height",$("#selectedOrgs").height()+10);
                    return false;
                }
            }
        });

        //删除组织
        $(document).delegate("[name=deleteOrg]","click",function(){
            $(this).parent().remove();
            var orgId = $(this).attr("data");
            for (var i = 0; i < g_org_ids.length; i++) {
                if (orgId == g_org_ids[i]) {
                    g_org_ids.splice(i,1);
                    $("#selectedOrgs").parent().css("height",$("#selectedOrgs").height()+10);
                    return false;
                }
            }
        });

        $('input[name="notice_flag"]').bind('change',function(){
            setTimeout(function(){
                setSendHeadquartersStaff($(this)[0].checked)
            }.bind(this),0)
        })

    //点击保存并发布
    // todo
    $("#send").click(function(){
        var text = encodeURIComponent(um.getContent());
        console.log(text);
        $('#edit_text').val(text);
        if(!validateSubmit()){
            return false;
        }
        $("#status").val("2");
        $("[name=orgTypes]").val(g_org_types.join(","));
        $("[name=orgIds]").val(g_org_ids.join(","));
        $("#fm").submit();
        $(this).addClass("disabled");
    });

    $("#save").click(function(){
        if(!validateSubmit()){
            return false;
        }
        var text = encodeURIComponent(um.getContent());
        $('#edit_text').val(text);
        $("#status").val("1");
        $("[name=orgTypes]").val(g_org_types.join(","));
        $("[name=orgIds]").val(g_org_ids.join(","));
        $("#fm").submit();
        $(this).addClass("disabled");
    });
    //初始化页面
    function initPage(){
        showInputByBigType();
        $('#user_phones').keyup(function(){
            var c=$(this).val();
            $(this).val(c.replace(/[^\d,]/g,''));
        });
        $("#user").select2();
        if(g_get_sub_area){
            selectSubArea();
        }
        if(g_get_sub_poi_area){
            selectSubPOIArea();
        }
        $("#selectedOrgs").parent().css("height",$("#selectedOrgs").height()+10);

        $('#userPhones').keyup(function(){
            var c=$(this).val();
            $(this).val(c.replace(/[^\d,]/g,''));
        });
        $("#city").select2();
        $("#city").val(g_city_ids).trigger("change");

        $("#body_type :radio").click(function(){
            var body_type = $(this).val();
            if(body_type == 0){
                $("#body_url").attr("type","hidden");
                $("#editor").removeClass("hide");
            }else{
                $("#body_url").attr("type","text");
                $("#editor").addClass("hide");
            }
        });
        getUsers();
        getPOICitys();
        getPOIAreas();
        getPOIShops();
    }

    function getUsers(){
        $.ajax({
            url:'/msg/beaconUsers',
            type:'post',
            dataType:'json'
        }).done(function(data){
            var data = data.data;
            var options = '';
            for(var i=0;i<data.length;i++){
                options+='<option value="'+data[i].id+'">'+data[i].value+'</option>';
            }
            document.querySelector('#users').innerHTML = options;
            $('#users').select2();
            $('#users').val(g_user_ids).trigger('change');
        }).fail(function(err){
            console.log(err);
        })
    }

    function getPOICitys(){
        $.ajax({
            url:'/org/citySearchList',
            type:'post',
            dataType:'json'
        }).done(function(data){
            var data = data.data;
            var options = '';
            for(var i=0;i<data.length;i++){
                options+='<option value="'+data[i].id+'">'+data[i].value+'</option>';
            }
            document.querySelector('#poiCitySelect').innerHTML = options;
            $('#poiCitySelect').select2();
            $('#poiCitySelect').val(g_poi_city_ids).trigger('change');
        }).fail(function(err){
            console.log(err);
        })
    }

    function getPOIAreas(){
        $.ajax({
            url:'/org/crowdAreaSearchList',
            type:'post',
            dataType:'json'
        }).done(function(data){
            var data = data.data;
            var options = '';
            for(var i=0;i<data.length;i++){
                options+='<option value="'+data[i].id+'">'+data[i].value+'</option>';
            }
            document.querySelector('#poiAreaSelect').innerHTML = options;
            $('#poiAreaSelect').select2();
            $('#poiAreaSelect').val(g_poi_area_ids).trigger('change');
        }).fail(function(err){
            console.log(err);
        })
    }

    function getPOIShops(){
        $('#poiShopSelect').select2({
            ajax:{
                url:'/poi/getAllCrowdPoi',
                type:'post',
                dataType:'json',
                delay:300,
                cache:'true',
                data:function(params){
                    return {
                        name:params.term||''
                    }
                },
                processResults:function(data,params){
                    return {
                        results:data.data
                    }
                }
            },
            templateResult:function(list) {
              if (!list.id) { return list.value; }
              var $list = $(
                '<span data-id="'+list.id+'">'+list.value+'</span>'
              );
              return $list;
            },
            templateSelection:function(list){
              if(list.text){
                  var $list = $(
                    '<span data-id="'+list.id+'">'+list.text+'</span>'
                  );
                  return $list;
              }else{
                return list.value;
              }
                  
            }
        });
    }

    //提交前校验
    function validateSubmit(){
        var title = $.trim($("#title").val());
        if(title.length == 0){
            alert("标题不能为空！");
            return false;
        }
        if(title.length > 30){
            alert("标题不能超过30个字！");
            return false;
        }

        if($("#body_type :checked").val() == 1){
            var body_url = $.trim($("#body_url").val());
            if(body_url.length == 0){
                alert("内容不能为空!");
                return false;
            }
        }/*else{
            var body = $.trim($("#body").val());
            if(body.length == 0){
                alert("内容不能为空！");
                return false;
            }
        }*/
        var bigType = $("#big_type :checked").val();
        var area = $("#area :checked").val();
        var poiArea = $('input[name=poiArea]:checked').val();
        var userPhones = $("#userPhones").val();
        if(area == 1 && $("#city").val() == null && g_org_ids == 0 && g_org_types == 0 && userPhones.length == 0&&(bigType==100||bigType==101)) {
            alert("未选择发送范围！");
            return false;
        }   
        if(area == 1 &&  g_org_ids == 0 && g_org_types == 0 && $('#users').val() == null &&bigType==102) {
            alert("未选择发送范围！");
            return false;
        }  
        if(bigType==103 && poiArea == 1 && ! $('#poiCitySelect').val() && ! $('#poiAreaSelect').val() && ! $('#poiShopSelect').val()){
            alert("未选择发送范围！");
            return false;
        }
        if(bigType != 102){
            if($("#os_type :checked").length == 0){
                alert("请选择发送的操作系统！");
                return false;
            }
        }

        return true;
    }

    //选择区域
    function selectArea(){
        var area = $("#area :checked").val();
        if(area == 0){
            $("#sub-area").addClass("hide");
        }else{
            selectSubArea();
        }
        //清空之前的选择
        g_city_ids = [];
        g_org_types = [];
        g_org_ids = [];
        g_user_ids = [];
        sendHeadquartersStaff = false;
        setSendHeadquartersStaff(false);
        $('input[name="notice_flag"]')[0].checked = false;
        $("#selectedOrgs").empty();
        $("textarea").val("");
    }

    function selectPOIArea(){
        var poiArea = $('input[name=poiArea]:checked').val();
        if(poiArea == 0){
            $('#subPOIArea').addClass('hide');
        }else{
            selectSubPOIArea();
        }
        g_poi_city_ids = [];
        g_poi_area_ids = [];
        g_poi_shop_ids = [];
    }

    function selectSubPOIArea(){
        $('#subPOIArea').removeClass('hide');
        $('#poiCitySelect').val(null).trigger('change');
        $('#poiAreaSelect').val(null).trigger('change');
        $('#poiShopSelect').val(null).trigger('change');
    }

    function selectSubArea(){
        //选择部分区域
        $("#sub-area").removeClass("hide");
        //加载城市列表
        searchCity();
        $('#users').val(null).trigger('change');
        var big_type = $("#big_type :checked").val();
        if(big_type == 101){
            //美团骑手
            initOrgTypeList(1);
        }else if(big_type == 100){
            initOrgTypeList(2);
        }else{
            initOrgTypeList(-1)
        }
    }

    //根据选择平台显示输入框
    function showInputByBigType(){
        var big_type = $("#big_type :checked").val();
        if(big_type == 102){
            //烽火台，隐藏客户端内容
            $(".for-app").addClass("hide");
            $('#cityArea').hide();
            $('#userArea').hide();
            $('#userSelect2Area').show();
            $('#showType').removeClass("hide");
        }else if(big_type == 103){
            $(".for-app").removeClass("hide");
            $('#showType').addClass("hide");
        }else{
            $(".for-app").removeClass("hide");
            $('#cityArea').show();
            $('#userArea').show();
            $('#userSelect2Area').hide();
            $('#showType').addClass("hide");
        }
    }

        function searchCity() {
            myPOST('/org/citySearchList', {}, function(r) {
                if (r.httpSuccess) {
                    if (r.data && r.data.code == 0) {
                        $("#city").empty();
                        $.each(r.data.data, function(i, city) {
                            $("#city").append('<option value="' + city.id + '">' + city.value + '</option>');
                        });
                        $("#city").select2();
                        $("#city").val(g_city_ids).trigger("change");
                    } else {
                        alert("获取城市信息失败，" + r.data.msg);
                    }
                } else {
                    //alert(JSON.stringify(r));
                    // alert('获取组织信息失败，系统错误');
                }
            });
        }

        function initOrgTypeList(appType) {
            var orgTypes = [];
            var zjType = {
                "id": 1,
                "name": "自建"
            };
            var jmType = {
                "id": 2,
                "name": "加盟"
            };
            var zbType = {
                "id": 4,
                "name": "众包"
            };
            if (appType == -1) {
                orgTypes[0] = zjType;
                orgTypes[1] = jmType;
                orgTypes[2] = zbType;
            }
            if (appType == 1) {
                orgTypes[0] = zjType;
                orgTypes[1] = jmType;
            }
            if (appType == 2) {
                orgTypes[0] = zbType;
            }
            $("#orgType").empty();
            $.each(orgTypes, function(i, orgType) {
                $("#orgType").append('<option value="' + orgType.id + '">' + orgType.name + '</option>')
            });
            getTopOrgByOrgType();
            if(!edit){
                $("#selectedOrgs").empty();
            }
        }

        function getTopOrgByOrgType() {
            var orgTypeId = $("#orgType :selected").val();
            //根据选中的那一个类型获取对应的一级组织
            myPOST("/org/topOrg", {
                orgType: orgTypeId
            }, function(r) {
                if (r.httpSuccess) {
                    if (r.data && r.data.code == 0) {
                        appendOrgList(r.data.data, $("#orgType"));
                    } else {
                        alert("获取组织信息失败，" + r.data.msg);
                    }
                } else {
                    //alert(JSON.stringify(r));
                    // alert('获取组织信息失败，系统错误');
                }
            });
        }

        function appendOrgList(orgs, $parentDom) {
            $parentDom.nextAll().remove();
            if (orgs != null && orgs.length > 0) {
                var orgSelect = $('<select class="orgSelect"></select>');
                $parentDom.after(orgSelect);
                orgSelect.append('<option value="-1">全部</option>')
                $.each(orgs, function(i, org) {
                    orgSelect.append('<option value="' + org.baseData.id + '">' + org.baseData.name + '</option>');
                });
                orgSelect.on("change", function() {
                    var orgId = orgSelect.val();
                    getChildOrgById(orgId, orgSelect);
                });
            }
            $(".orgSelect").select2();
         }

        function getChildOrgById(orgId, $parentDom) {
            myPOST("/org/childOrgs", {
                orgId: orgId
            }, function(r) {
                if (r.httpSuccess) {
                    if (r.data && r.data.code == 0) {
                        appendOrgList(r.data.data, $parentDom);
                    } else {
                        alert("获取组织信息失败，" + r.data.msg);
                    }
                } else {
                    //alert(JSON.stringify(r));
                    // alert('获取组织信息失败，系统错误');
                }
            });
        }

        function myPOST(url, data, callback) // 发送POST请求
        {
            if (typeof(data) == 'function') {
                callback = data;
                data = null;
            }
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                success: function(r) {

                    callback({
                        httpSuccess: true,
                        data: r
                    });

                },
                error: function(XmlHttpRequest, textStatus, errorThrown) {

                    callback({
                        httpSuccess: false,
                        statusCode: XmlHttpRequest.status
                    });

                }
            });
        }

        function setSendHeadquartersStaff(val){
            sendHeadquartersStaff = val;
            $('input[name="sendHeadquartersStaff"]').val(val)
        }

    });

});