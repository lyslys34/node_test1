
require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu'
        ],
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});
require(['module/root','module/validator','lib/datepicker'], function (t,validator,datepicker) {
  datepicker.set();

  $("#fm").submit(function(){
    var beginTime = $("#beginTime").val();
    var endTime = $("#endTime").val();
    if(beginTime > endTime){
        $("#alert-error").text("起始时间不能晚于结束时间").removeClass("hide");
        return false;
    }

    var opMisId = $("#opMisId").val();
    var hideOpMisId = $("#op-mis-id").val();
    if(validator.isBlank(hideOpMisId) || opMisId.indexOf(hideOpMisId) < 0){
        $("#op-mis-id").val(opMisId);
    }

    var mockMisId = $("#mockMisId").val();
    var hideMockMisId = $("#mock-mis-id").val();
    if(validator.isBlank(hideMockMisId) || mockMisId.indexOf(hideMockMisId) < 0){
        $("#mock-mis-id").val(mockMisId);
    }

  });

//  $(".j-mis-id").keydown(function(event) {
//      if (event.keyCode == 8) {
//        $(this).next('.mis-search').find("input").val("");
//        $(this).val("");
//        $(this).next('.mis-search').hide();
//      }
//    });

    var lastTime;
    $(".j-mis-id").keyup(function(event) {
      var $this = $(this);
      lastTime = event.timeStamp;
      setTimeout(function() {
        if (lastTime - event.timeStamp == 0) {
          var date = new Date();
          $.getJSON('/employee/query?q=' + $this.val()).success(function(data) {
            var city_list = new Array();
            if (data.data.length == 0) {
              $this.next('.mis-search').hide();
              return;
            }
            $this.next('.mis-search').show();
            $this.next('.mis-search').find('.mis-search-ul').empty();
            for (var i in data.data) {
              var name = data.data[i].name;
              var id = data.data[i].id;
              var login = data.data[i].login;
              $this.next('.mis-search').find('.mis-search-ul').append('<li onclick="add_to_input($(this));" data="'+login+'">' + name + '</li>');
            }

          }).error(function(data) {
            console.log('网络错误');
          });
        }
      }, 300);
    });

  add_to_input = function (e) {
      e.parent().parent().prev().val(e.text());
      e.parent().parent().find("input").val(e.attr('data'));
      // $('.mis-search-ul').append('<input style="display: none;" id="mis-id" name="employeeId" value="'+e.attr('data')+'" >');
      e.parent().parent().hide();
  }

//  $( "#opMisId" ).autocomplete({
//    source:function( request, response ) {
//       var misId = $.trim(request.term);
//       $.ajax({
//         url: "/mock/users",
//         dataType: "json",
//         data: {
//           misId:misId
//         },
//         success: function( data ) {
//           if(data.code == 0){
//              response( data.data );
//           }
//         }
//       });
//    },
//    minLength:1,
//    select: function( event, ui ) {
//       g_opMisId = ui.item.label;
//    },
//    change: function(event, ui){
//       if(ui.item){
//          g_opMisId = ui.item.label;
//       }else{
//          g_opMisId = $("#opMisId").val();
//       }
//    }
//  }).focus(function(event) {
//      $(this).autocomplete("search");
//  }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
//          return $( "<li>" )
//              .append( "<a>" + item.value + "</a>" )
//              .appendTo( ul );
//      };
//
//  $( "#mockMisId" ).autocomplete({
//      source:function( request, response ) {
//         var misId = $.trim(request.term);
//         $.ajax({
//           url: "/mock/users",
//           dataType: "json",
//           data: {
//             misId:misId
//           },
//           success: function( data ) {
//             if(data.code == 0){
//                response( data.data );
//             }
//           }
//         });
//      },
//      minLength:1,
//      select: function( event, ui ) {
//         g_mockMisId = ui.item.label;
//      },
//      change: function(event, ui){
//         if(ui.item){
//            g_mockMisId = ui.item.label;
//         }else{
//            g_mockMisId = $("#mockMisId").val();
//         }
//      }
//    }).focus(function(event) {
//        $(this).autocomplete("search");
//    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
//            return $( "<li>" )
//                .append( "<a>" + item.value + "</a>" )
//                .appendTo( ul );
//        };
});