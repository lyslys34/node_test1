
require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu'
        ],
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});
require(['module/root','module/validator'], function (t,validator) {
  $("#fm").submit(function(){
    var mockMisId = $('#mockMisId').val();
    if(validator.isBlank(mockMisId)){
      $("#alert-error").text("请填写员工mis账号或手机号").removeClass("hide");
      return false;
    }
    var mock = $("#mis-id").val();
    if(validator.isBlank(mock) || mockMisId.indexOf(mock) < 0){
        $("#mis-id").val(mockMisId);
    }
  });

//  $("#mockMisId").keydown(function(event) {
//    if (event.keyCode == 8) {
//      $('#mis-id').val("");
//      $('#mockMisId').val("");
//      $('.mis-search').hide();
//    }
//  });

  var lastTime;
  $("#mockMisId").keyup(function(event) {
    lastTime = event.timeStamp;
    setTimeout(function() {
      if (lastTime - event.timeStamp == 0) {
        var date = new Date();
        $.getJSON('/employee/query?q=' + $('#mockMisId').val()).success(function(data) {
          var city_list = new Array();
          if (data.data.length == 0) {
            $('.mis-search-ul').empty();
            return;
          }
          $('.mis-search').show();
          $('.mis-search-ul').empty();
          for (var i in data.data) {
            var name = data.data[i].name;
            var id = data.data[i].id;
            var login = data.data[i].login;
            $('.mis-search-ul').append('<li onclick="add_to_input($(this));" data="'+login+'">' + name + '</li>');
          }

        }).error(function(data) {
          console.log('网络错误');
        });
      }
    }, 300);
  });

add_to_input = function (e) {
    $('.mis-search').prev().val(e.text());
    $('#mis-id').val(e.attr('data'));
    // $('.mis-search-ul').append('<input style="display: none;" id="mis-id" name="employeeId" value="'+e.attr('data')+'" >');
    $('.mis-search').hide();
}

//  $( "#mockMisId" ).autocomplete({
//    source:function( request, response ) {
//       var misId = $.trim(request.term);
//       $.ajax({
//         url: "/mock/users",
//         dataType: "json",
//         data: {
//           misId:misId
//         },
//         success: function( data ) {
//           if(data.code == 0){
//              response( data.data );
//           }
//         }
//       });
//    },
//    minLength:1,
//    select: function( event, ui ) {
//       g_misId = ui.item.label;
//    },
//    change: function(event, ui){
//       if(ui.item){
//          g_misId = ui.item.label;
//       }else{
//          g_misId = $("#mockMisId").val();
//       }
//    }
//  }).focus(function(event) {
//      $(this).autocomplete("search");
//  }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
//          return $( "<li>" )
//              .append( "<a>" + item.value + "</a>" )
//              .appendTo( ul );
//      };
});