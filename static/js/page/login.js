
require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root', 'module/validator'], function (r, validator) {


    $("#send-captcha").click(function () {
        $(".error").addClass("hide");
        var mobile = $("#mobile").val();
        if (!validator.isMobile(mobile)) {
            alert("请输入手机号");
            return false;
        }

        disableSendCaptcha();
        $.post("/partner/login/sendValidCodeToSms", {
            mobile: mobile
        }, function (res) {

            try {
                var code = res.code;
                if (code != 0) {
                    $(".error").removeClass("hide").find(".err-msg").text(res.msg);
                    if(code == 101089){//操作过于频繁，请1分钟后重试
                        countDown(60);
                        return false;
                    }
                    if(code == 101090){//动态码获取次数过多，请24小时后重试
                        return false;
                    }
                    enableSendCaptcha();
                } else {
                    countDown(60);
                }
            } catch (e) {
                //just go on
            }

        });
    });

    function countDown(time) {
        var id = window.setInterval(function () {
            if (--time <= 0) {
                $("#send-captcha").val('获取验证码');
                window.clearInterval(id);
                enableSendCaptcha();
            } else {
                $("#send-captcha").val('重新获取(' + time + ')');
            }
        }, 1000);
    }

    function disableSendCaptcha() {
        $("#send-captcha").attr("disabled",true);
    }

    function enableSendCaptcha() {
        $("#send-captcha").attr("disabled",false);
    }
});