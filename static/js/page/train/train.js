/**
 * Author: fengtianran@meituan.com
 */

require.config({
  baseUrl: MT.STATIC_ROOT + '/js',
  urlArgs: 'ver=' + pageVersion
});

require(['module/root'], function(t) {
  var cityNameMap = {};
  var cityIdMap = {};
  var bmUserId;
  var bmStaffBlacklistId;
  var opType;
  var opDescription;

  var pageSize = 10;
  var appScoreDialogAjaxParams = {};
  var scorePageSize = 5;
  var scoreDialogAjaxParams = {};

  var orgTypeList = {
    '1': '众包APP',
    '2': '自建',
    '3': '加盟',
    '4': '骑手APP'
  };

  var roleList = {
    '1001': '众包骑手',
    '1-1001': '自建骑手',
    '2-1001': '加盟骑手',
    '5001': '配送经理',
    '2001': '组织负责人',
    '2100': '调度员',
    '3001': '渠道经理',
    '2001': '组织负责人',
    '2100': '调度员'
  };

  var examStatus = {
    0: '未考试',
    1: '未通过',
    2: '通过'
  };

  var buttonList = {
    '99': '<input type="button" value="&nbsp;已&nbsp;上&nbsp;线&nbsp;" nextStage="-1" trainId="{trainId}" class="stage btn btn-success btn-primary js_submitbtn active" disabled=true/>',
    '10': '<input type="button" value="部署线上" nextStage="99" trainId="{trainId}" class="stage btn btn-success btn-primary js_submitbtn active" />',
    '0': '<input type="button" value="备机测试" nextStage="10" trainId="{trainId}" class="stage btn btn-primary btn-primary js_submitbtn active" />'
  };

  // 考试管理的渲染逻辑
  var examManageGenerator = {
    init: function() {
      this.updateExamList(1);
      this.bind();
    },

    bind: function() {
      //修改按钮点击事件
      $(".exam-manage").on("click", ".modify", function() {
        var trainId = $(this).attr("trainId");
        var nextStage = $(this).parent().find('.stage').attr('nextstage');
        if (trainId) {
          if (nextStage == -1) {
            $("#params-form").attr("action", "/train/detail?trainId=" + trainId);
            $("#trainId").val(trainId);
            $("#params-form").submit();
            return;
          }
          $("#params-form").attr("action", "/train/edit");
          $("#trainId").val(trainId);
          $("#params-form").submit();
        } else {
          showMsg('培训Id获取失败');
          return false;
        }
      });
      //删除按钮点击事件
      $(".exam-manage").on("click", ".del", function() {
        var trainId = $(this).attr("trainId");
        var desc = "确认移除培训【" + $(this).parent().parent().find(".t_name").text() + "】?";
        $("#showConfirmTitle").html(desc);
        $("#params-form").attr("action", "/train/deleteTrain");
        $("#trainId").val(trainId);
        $("#showConfirm").modal();
      });

      //删除
      $("#confirm").click(function(){
        var form = $("#params-form");
        var trainId = $("#trainId").val();
        var url = form.attr("action");
        var postData = {"trainId":trainId};
        t.utils.post(url,postData,function(r){
          if (r.httpSuccess) {
            if(r.data.code == 0){
              form.attr("action","/train/list");
              form.submit();
            }else{
              showMsg(r.data.msg);
            }
          }else{
            showMsg("提交失败,系统错误!");
                  return false;
          }
        });
          $("#showConfirm").modal('hide');
      });

      /**
       * ajax改变上线按钮的样式和属性
       */
      $(".exam-manage").on('click', '.stage', function() {
        var $el = $(this);
        var nextStage = $el.attr('nextStage');
        var trainId = $el.attr("trainId");
        if (nextStage) {
          var postData = {
            trainId: trainId,
            stage: nextStage
          };
          $el.attr('disabled', true);
          $el.val('提交中..');
          t.utils.post('/train/changeStage', postData, function(r) {
            if (r.httpSuccess) {
              if (r.data.code == 0) {
                var parent = $el.parent();
                if (nextStage == 99) {
                  // 原本使用&nbsp;来进行模板中空格的处理，但是js插入value是无法正确解析成空格的，所以需要用unicode字符代替
                  parent.find('.modify').val('\u00a0详\u00a0情\u00a0');
                }
                parent.find('.stage').remove();
                parent.append(buttonList[nextStage].replace('{trainId}', trainId));
              } else {
                $(this).attr('disabled', false);
                showMsg(r.data.msg);
              }
            } else {
              showMsg("提交失败,系统错误!");
              $(this).attr('disabled', false);
            }
          });
        } else {
          showMsg('未知的操作类型');
        }
      });
    },

    /**
     * 更新考试管理页面的table
     * @method function
     * @param  {String} pageNo [需要改变到的页码]
     * @param  {object} params [请求的查询参数]
     */
    updateExamList: function(pageNo, params) {
      params = params ? params : {};
      params.pageNo = pageNo;
      params.pageSize = pageSize;
      t.utils.post('/train/list.ajax', params, function(data) {
        data = data.data;
        if (typeof data === 'string') {
          data = JSON.parse(data);
        }
        data = data.data;

        var render = '';
        var examListTemplate = $("#examListTemplate").html();
        data.forEach(function(item) {
          var button = buttonList[item.stage].replace('{trainId}', item.train_id);
          var modifyText = item.stage == 99 ? '&nbsp;详&nbsp;情&nbsp;' : '&nbsp;修&nbsp;改&nbsp;';
          var config = {
            title: item.title,
            desc: item.description ? item.description : '无',
            content: item.content ? item.content : '无',
            time: (new Date(item.ctime * 1000)).toLocaleString(),
            trainId: item.train_id,
            button: button,
            operator: item.operator,
            modifyText: modifyText
          };
          var org = orgTypeList[item.orgType];
          if (item.force) {
            org += '<span>(限制抢单)</span>'
          }
          config.org = org;
          var tempTemplate = examListTemplate;
          for (key in config) {
            var reg = new RegExp('{' + key + '}', 'g');
            tempTemplate = tempTemplate.replace(reg, config[key]);
          }
          render += tempTemplate;
        });
        $('.exam-manage .tbody-dispatcher-list').html(render);
      });
      if ($('.t_org').eq(0).text().indexOf('众包') > -1) {
        $('.exam-nav li').eq(1).hide();
      }
    }
  }

  // 初始化考试管理
  examManageGenerator.init();

  // 成绩管理的生成器
  var scoreManageGenerator = {

    // 用来储存成绩管理页面table的搜索参数的存储
    scoreParams: {},

    // 用来储存错题top5的搜索参数的存储
    wrongNumAjaxParams: {},

    init: function() {
      var me = this;
      $.post('/train/score/getCount', me.scoreParams)
      .done(function(data) {
        data = data.data;
        if (typeof data == 'number') {
          $('.exam-pager-manage .train-count').text(data);
        }
        me.resetPager(data);
        me.updateExamScore(1);
        me.bind();
      });
    },

    bind: function() {
      var me = this;

      // 用来触发显示考生成绩页面，根据orgType的不同，分为APP考生成绩和普通考生成绩
      $('.tbody-dispatcher-list').on('click', '.t_score a', function() {
        var trainId = $(this).attr('trainId');
        if ($(this).attr('t_org') == 4) {
          me.showAppScoreDialog(trainId);
        } else {
          me.showScoreDialog(trainId);
        }
      });

      // 用来触发显示错题top5页面
      $('.tbody-dispatcher-list').on('click', '.t_wrong a', function() {
        var trainId = $(this).attr('trainId');
        me.showWrongDialog(trainId);
      });

      // 绑定成绩管理的搜索按钮
      $('.exam-pager-manage-search').on('click', '.search-button', function() {
        me.scoreParams.content = $('.exam-pager-manage-search input[name=content]').val();
        $.post('/train/score/getCount', me.scoreParams)
        .done(function(data) {
          data = data.data;
          if (typeof data == 'number') {
            $('.exam-pager-manage .train-count').text(data);
          }
          me.resetPager(data);
          me.updateExamScore(1);
        });
      });
    },

    /**
     * 重置成绩管理页面的分页组件
     * @method function
     * @param  {Number} count [成绩管理的试卷总数]
     */
    resetPager: function(count) {
      var totalPages;
      var me = this;
      if (count <= pageSize) {
        $('.exam-pager-manage-pager').html('<div class="pager"></div>');
        return;
      }
      if (count % pageSize === 0) {
        totalPages = count / pageSize;
      } else {
        totalPages = (count / pageSize) + 1;
      }
      $('.exam-pager-manage-pager').html('<div class="pager"></div>');
      $('.exam-pager-manage-pager .pager').twbsPagination({
        totalPages: totalPages,
        visiblePages: 10,
        first: '首页',
        prev: '上一页',
        next: '下一页',
        last: '尾页',
        onPageClick: function(event, page) {
          me.updateExamScore(page);
        }
      });
    },

    /**
     * 更新成绩管理页面的table
     * @method function
     * @param  {Number} pageNo [需要更新到的页码]
     */
    updateExamScore: function(pageNo) {
      params = this.scoreParams;
      params.pageNo = pageNo;
      params.pageSize = pageSize;
      t.utils.post('/train/score/list', params, function(data) {
        data = data.data;
        if (typeof data === 'string') {
          data = JSON.parse(data);
        }
        data = data.data;

        var render = '';
        var examScoreTemplate = $("#examScoreTemplate").html();
        data.forEach(function(item) {
          var role = item.applyRoles ? item.applyRoles.split(',').map(function(key) {
            return '<p>' + roleList[key] + '</p>'
          }).join('') : '';
          var avgScore = item.avgScore;
          if (item.orgType == 4 || item.avgScore == -1) {
            avgScore = '/';
          }
          var score;
          if (item.eTime * 1000 > +new Date()) {
            score = '考试中';
          } else {
            score = '<a trainId="' + item.train_id + '" t_org="' + item.orgType + '" href="#">查看</a>';
          }
          var config = {
            title: item.title,
            desc: item.description ? item.description : '无',
            content: item.content ? item.content : '无',
            role: role,
            org: orgTypeList[item.orgType],
            avg: avgScore,
            score: score,
            wrong: score
          };
          var tempTemplate = examScoreTemplate;
          for (key in config) {
            var reg = new RegExp('{' + key + '}', 'g');
            tempTemplate = tempTemplate.replace(reg, config[key]);
          }
          render += tempTemplate;
        });
        $('.exam-pager-manage .tbody-dispatcher-list').html(render);
      })
    },

    /**
     * 展示普通考生的成绩dialog
     * @method function
     * @param  {Number} trainId [考试的试题id]
     */
    showScoreDialog: function(trainId) {
      var me = this;
      var scoreListUrl = '/train/examinee/list';
      if (!trainId) {
        return;
      }
      scoreDialogAjaxParams = {
        trainId: trainId,
        pageSize: scorePageSize
      };
      // 取消之前对dialog的事件绑定
      $('#scoreDialog').off('click');
      // 先进行考生数目的查询
      $.post('/train/examinee/getCount', {
        trainId: trainId
      })
      .done(function(data) {
        var count = data.data;
        me.initScorePager('#scoreDialog', count, function(pageNo) {
          me.renderDialog({
            url: scoreListUrl,
            pageNo: pageNo,
            params: scoreDialogAjaxParams,
            cb: me.updateScoreList
          });
        });

        me.renderDialog({
          url: scoreListUrl,
          pageNo: 1,
          params: scoreDialogAjaxParams,
          cb: function(data) {
            me.updateScoreList(data);
            $('#scoreDialog').modal('show');
            $('#scoreDialog').on('click', '.export-button', function() {
              var params = {
                trainId: trainId,
                userName: $('#scoreDialog input[name=userName]').val(),
                cityName: $('#scoreDialog input[name=cityName]').val(),
                roleName: $('#scoreDialog input[name=roleName]').val()
              };
              var downloadUrl = '/train/examinee/score/export?' + $.param(params);
              var link = document.createElement('a');
              link.href = downloadUrl;
              link.download = '考生成绩.xls';
              link.click();
            });
            $('#scoreDialog').on('click', '.search-button', function() {
              var trainId = scoreDialogAjaxParams.trainId;
              scoreDialogAjaxParams = {
                trainId: trainId,
                pageSize: scorePageSize,
                userName: $('#scoreDialog input[name=userName]').val(),
                cityName: $('#scoreDialog input[name=cityName]').val(),
                roleName: $('#scoreDialog input[name=roleName]').val()
              };
              $.post('/train/examinee/getCount', scoreDialogAjaxParams)
              .done(function(data) {
                data = data.data;
                me.initScorePager('#scoreDialog', data, function(pageNo) {
                  me.renderDialog({
                    url: scoreListUrl,
                    pageNo: pageNo,
                    params: scoreDialogAjaxParams,
                    cb: me.updateScoreList
                  });
                });
                me.renderDialog({
                  url: scoreListUrl,
                  pageNo: 1,
                  params: scoreDialogAjaxParams,
                  cb: me.updateScoreList
                });
              });
            });
          }
        });
      });
    },

    /*
      用来获取渲染dialog的表格内容
      @params {object} args
      args 的参数如下
      @params {Number} pageNo 渲染页面的页码
      @params {object} params 发送请求的数据参数
      @params {String} url 请求的url地址
      @params {String} msg 请求错误时的消息显示
      @params {Function} cb 请求结束的回调函数
     */
    renderDialog: function(args) {
      var pageNo = args.pageNo;
      var cb = args.cb;
      var params = args.params;
      var url = args.url;
      var msg = args.msg ? args.msg : '获取考生成绩失败';
      var formData = Object.assign({
        pageNo: pageNo
      }, params);
      $.post(url, formData)
        .done(function(data) {
          data = data.data;
          if (!data) {
            showMsg(msg);
          }
          cb(data);
        });
    },

    /**
     * 展示骑手APP考生的成绩dialog
     * @method function
     * @param  {Number} trainId [考试的试题id]
     */
    showAppScoreDialog: function(trainId) {
      var me = this;
      if (!trainId) {
        return;
      }
      var appScoreListUrl = '/train/examinee/list';
      appScoreDialogAjaxParams = {
        trainId: trainId,
        pageSize: scorePageSize
      };
      $('#appScoreDialog').off('click');
      $.post('/train/examinee/getCount', {
        trainId: trainId
      })
      .done(function(data) {
        var count = data.data;
        me.initScorePager('#appScoreDialog', count, function(pageNo) {
          me.renderDialog({
            url: appScoreListUrl,
            pageNo: pageNo,
            params: appScoreDialogAjaxParams,
            cb: me.updateAppScoreList
          });
        });

        me.renderDialog({
          url: appScoreListUrl,
          pageNo: 1,
          params: appScoreDialogAjaxParams,
          cb: function(data) {
            me.updateAppScoreList(data);
            $('#appScoreDialog').modal('show');
            $('#appScoreDialog').on('click', '.export-button', function() {
              var params = {
                trainId: trainId,
                cityName: $('#appScoreDialog input[name=cityName]').val(),
                orgType: $('#appScoreDialog select[name=orgType]').val(),
                status: $('#appScoreDialog select[name=status]').val()
              }
              var downloadUrl = '/train/examinee/score/export?' + $.param(params);
              var link = document.createElement('a');
              link.href = downloadUrl;
              link.download = '考生成绩.xls';
              link.click();
            });
            $('#appScoreDialog').on('click', '.search-button', function() {
              var trainId = appScoreDialogAjaxParams.trainId;
              appScoreDialogAjaxParams = {
                trainId: trainId,
                pageSize: scorePageSize,
                exportType: 1,
                cityName: $('#appScoreDialog input[name=cityName]').val(),
                orgType: $('#appScoreDialog select[name=orgType]').val(),
                status: $('#appScoreDialog select[name=status]').val()
              }
              $.post('/train/examinee/getCount', appScoreDialogAjaxParams)
              .done(function(data) {
                data = data.data;
                me.initScorePager('#appScoreDialog', data, function(pageNo) {
                  me.renderDialog({
                    url: appScoreListUrl,
                    pageNo: pageNo,
                    params: appScoreDialogAjaxParams,
                    cb: me.updateAppScoreList
                  });
                });
                me.renderDialog({
                  url: appScoreListUrl,
                  pageNo: 1,
                  params: appScoreDialogAjaxParams,
                  cb: me.updateAppScoreList
                });
              });
            });
          }
        });

      });
    },

    /**
     * 更新普通考生的成绩dialog中的table
     * @method function
     * @param  {Array} data [考生的成绩数据]
     */
    updateScoreList: function(data) {
      var render = '';
      var wrongTemplate = $("#studentScoreTemplate").html();
      data.forEach(function(item) {
        var config = {
          name: item.userName,
          city: item.cityName,
          org: item.orgName,
          role: item.roleName,
          score: item.score,
          wrong: item.wrongCount
        };
        var tempTemplate = wrongTemplate;
        for (key in config) {
          var reg = new RegExp('{' + key + '}', 'g');
          tempTemplate = tempTemplate.replace(reg, config[key]);
        }
        render += tempTemplate;
      });
      $('#scoreDialog tbody').html(render);
    },

    /**
     * 更新骑手APP考生的成绩dialog中的table
     * @method function
     * @param  {Array} data [考生的成绩数据]
     */
    updateAppScoreList: function(data) {
      var render = '';
      var wrongTemplate = $("#appScoreTemplate").html();
      data.forEach(function(item) {
        var config = {
          name: item.userName,
          phone: item.account,
          city: item.cityName,
          org: item.orgName,
          role: item.roleName,
          score: examStatus[item.status],
        };
        var tempTemplate = wrongTemplate;
        for (key in config) {
          var reg = new RegExp('{' + key + '}', 'g');
          tempTemplate = tempTemplate.replace(reg, config[key]);
        }
        render += tempTemplate;
      });
      $('#appScoreDialog tbody').html(render);
    },

    /**
     * 显示错题top5dialog
     * @method function
     * @param  {Number} trainId [错题对应的试题id]
     */
    showWrongDialog: function(trainId) {
      var me = this;
      if (!trainId) {
        return;
      }
      $('#wrongDialog').off('click');
      $.post('/train/getWrongTop5', {
          trainId: trainId
        })
        .done(function(data) {
          data = data.data;
          if (!data) {
            showMsg('获取错题失败');
            return;
          }
          me.updateWrongList(data);
          $('#wrongDialog').modal('show');

          $('#wrongDialog').on('click', '.search-button', function() {
            $.post('/train/getWrongTop5', {
                trainId: trainId,
                cityName: $('#wrongDialog input').val()
              })
              .done(function(data) {
                data = data.data;
                if (!data) {
                  showMsg('获取错题失败');
                  return;
                }
                me.updateWrongList(data);
              });
          })
        });
    },

    /**
     * 更新错题top5dialog中的table
     * @method function
     * @param  {Array} data [考生的成绩数据]
     */
    updateWrongList: function(data) {
      var render = '';
      var wrongTemplate = $("#wrongTemplate").html();
      data.forEach(function(item) {
        var config = {
          city: item.cityName,
          subject: item.examSubject,
          wrong: item.wrongCount
        };
        var tempTemplate = wrongTemplate;
        for (key in config) {
          var reg = new RegExp('{' + key + '}', 'g');
          tempTemplate = tempTemplate.replace(reg, config[key]);
        }
        render += tempTemplate;
      });
      $('#wrongDialog tbody').html(render);
    },

    /**
     * 初始化dialog中的页码
     * @method function
     * @param  {String}   parent    [css选择器的字符串，标注当前页码所在的dialog]
     * @param  {Number}   pageCount [需要分页的数据的总数量]
     * @param  {Function} callback  [在点击页码的时候，触发的callback方法，当点击已选中的页码的时候不会触发该函数]
     */
    initScorePager: function(parent, pageCount, callback) {
      var totalPages;
      if (pageCount <= scorePageSize) {
        $(parent + ' .pager-wrap').html('<div class="pager"></div>');
        return;
      }
      if (pageCount % scorePageSize === 0) {
        totalPages = pageCount / scorePageSize;
      } else {
        totalPages = (pageCount / scorePageSize) + 1;
      }
      $(parent + ' .pager-wrap').html('<div class="pager"></div>');
      $(parent + ' .pager').twbsPagination({
        totalPages: totalPages,
        visiblePages: 5,
        first: '首页',
        prev: '上一页',
        next: '下一页',
        last: '尾页',
        onPageClick: function(event, page) {
          callback && callback(page);
        }
      });
    }
  };

  scoreManageGenerator.init();

  // 绑定新建考试按钮的click事件
  $("#create").click(function() {
    var trainId = 0;
    $("#params-form").attr("action", "/train/edit");
    $("#trainId").val(0);
    $("#params-form").submit();
  });

  /**
   * 显示消息提示框
   * @method showMsg
   * @param  {String} msg [需要提示的信息]
   */
  function showMsg(msg) {
    var body = $('<div style="height: 100px;line-height: 100px;text-align: center;">' + msg + '</div>');
    t.ui.showModalDialog({
      body: body,
      buttons: [
        t.ui.createDialogButton('close', '关闭', function() {})
      ],
      style: {
        ".modal-dialog": {
          "padding": "100px 110px"
        },
        ".modal-footer": {
          "padding": "10px"
        },
        ".btn-default": {
          "font-size": "12px"
        }
      }
    });
  }

  // 绑定试卷管理和成绩管理的切换事件
  $('.exam-nav').on('click', 'li', function() {
    if ($(this).hasClass('active')) {
      return;
    }
    $('.exam-nav li').toggleClass('active');
    $('.page-container').toggle();
  });

  initPager();
  // 全局初始化分页数据
  function initPager() {
    $.get('/train/getTrainCount')
      .done(function(data) {
        var trainCount = data.data;
        var totalPages;
        if (trainCount % pageSize === 0) {
          totalPages = trainCount / pageSize;
        } else {
          totalPages = (trainCount / pageSize) + 1;
        }
        $('.train-count').text(trainCount);
        $('.exam-manage .pager').twbsPagination({
          totalPages: totalPages,
          visiblePages: 10,
          first: '首页',
          prev: '上一页',
          next: '下一页',
          last: '尾页',
          onPageClick: function(event, page) {
            examManageGenerator.updateExamList(page);
          }
        });
      });
  }

  // 初始化查询条件中的选择框
  function initSelect2() {
    $('select[name=orgType]').select2({
      height: '33px'
    });
    $('select[name=status]').select2();
  }

  initSelect2();
});
