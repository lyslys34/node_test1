require.config({
  baseUrl: MT.STATIC_ROOT + '/js',
  urlArgs: 'ver=' + pageVersion,
  shim: {
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core': ['page/common'],
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget': ['page/common'],
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position': ['page/common'],
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu': ['page/common'],
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':
    [
      'page/common',
      'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
      'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
      'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
      'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu'
    ]
  },
});

require(['module/root', 'lib/datepicker', 'lib/hupload-2.1', 'lib/htemplate-1.2'], function(t, datepicker) {

  $('#loading').show();
  var trainId //培训课程id
  var isUpdate; //是否是修改培训内容

  trainId = $('#trainId').val();
  isUpdate = trainId != 0 && !!trainId;

  function showMsg(msg, reload, cb, text) {
    var body = $('<div style="height: 100px;line-height: 100px;text-align: center;">' + msg + '</div>');

    function reloadFunc() { // 取消
      if (reload) {
        location.reload(true);
      }
    }
    cb = cb ? cb : reloadFunc;
    t.ui.showModalDialog({
      body: body,
      buttons: [
        t.ui.createDialogButton('close', '关闭', cb)
      ],
      style: {
        ".modal-dialog": {
          "padding": "100px 110px"
        },
        ".modal-footer": {
          "padding": "10px"
        },
        ".btn-default": {
          "font-size": "12px"
        }
      }
    });
  }

  //上传
  var hupload = {
    open: function($elem) {
      $elem.each(function() {
        var $self = $(this);
        //var li = $(this).closest("li").siblings(".upload-list");
        $self.hupload({
          url: "/activity/uploadImage",
          fileName: 'image',
          noTypes: 'exe,sh',
          startUpLoad: function() {
            var cc = $('#tbody-pic tr').length;
            if (!!cc && cc > 9) {
              showMsg("培训图片不能多于10张");
              return false;
            }

            $("#upload-button").text('图片上传中..');
          },
          checkCb: function(re, name) {
            $("#upload-button").text('上传图片');
            if (!!re) {
              if (re.code != 0) {
                showMsg("上传失败，请重试<br/>原因:" + re.msg);
                return false;
              }
            } else {
              showMsg("上传失败，请重试");
              return false;
            }
            return true;
          },
          cb: function(re, name) {
            var count = $('#tbody-pic').find("tr").length + 1;
            var templateStr = '<tr id=""><td width="5%" ><input type="checkbox" />' + '</td><td width="5%" class="t_no">' + count + '</td><td width="60%" class="t_pic"><a target="_blank" class="t_url" href="{data}">' + name + '</a></td>' + '<td width="30%" class="t_op"><input type="button" value="修改" class="btn btn-warning js_submitbtn modify" />&nbsp' + '<input type="button" value="删除" class="btn btn-danger js_submitbtn del" />&nbsp' + '<input type="button" value="上移" class="btn btn-info js_submitbtn up" />&nbsp' + '<input type="button" value="下移" class="btn btn-info js_submitbtn down" /></td>';
            $(".tbody-pic").append(htemplate(templateStr, re));
            re_sort_no($(".tbody-pic")[0]);
          },
          errorCb: function() {
            $("#upload-button").text('上传图片');
          }
        });
      });
    },
    close: function($elem) {
      $elem.each(function() {
        $(this).off();
        var frameId = $(this).attr("frameId");
        $("#" + frameId).remove();
      });
    }
  };
  hupload.open($(".hupload"));

  var ANS_TAGS = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
  var BASH_ANSWER = $("#showCreate .answer:first").clone();

  $("#createSubject").click(function() {
    var cc = $('#subject_table tr').length;
    if (!!cc && cc > 19) {
      showMsg('培训考试题不能超过20道');
      return false;
    }
    clear_create();
    $("#subject-contain").append(BASH_ANSWER.clone());
    $("#confirmCreate").data('line_no', -1);
    $("#subject_title").text('添加新题目');
    $("#showCreate").modal();
  });

  function clear_create() {
    var c = $("#showCreate");
    c.find('.subject_type').removeAttr('checked');
    c.find('input').val("");
    c.find('.answer').remove();
  };

  $("#subject-contain").on("click", ".add-answer", function() {
    if ($(".answer").length > ANS_TAGS.length - 1) {
      showMsg("T_T, 7个还不够？");
      return false;
    }
    var newAns = $(".answer:first").clone();
    newAns.find(".answer-content").val("");
    newAns.find(".answer_c").removeAttr("checked");
    newAns.find(".answerId").val("");
    $("#subject-contain").append(newAns);
    refresh_ans_tag();
  });
  $("#subject-contain").on("click", ".rm-answer", function() {
    if ($(".answer").length == 1) {
      showMsg("兄台，到底儿了");
      return false;
    }
    $(this).parent().parent().remove();
    refresh_ans_tag();
  });

  var refresh_ans_tag = function() {
    $.each($("#subject-contain .ans_tag"), function(index, ele) {
      var tag = ANS_TAGS[index];
      $(this).text(tag);
    })
  };

  $("#pic-table").on('click', 'th input:checkbox', function() {
    var subList = $(this).parents('table').find('tbody input:checkbox'); //
    var checkAll = $(this)[0].checked == true;
    if (!subList) {
      return false;
    }
    $.each(subList, function() {
      if (checkAll) {
        if (!this.checked || this.checked != true) {
          $(this).click();
        }
      } else {
        if (this.checked == true) {
          $(this).click();
        }
      }
    })
  })

  $(".table").on('click', '.del', function() {
    var table = $(this).parents('table')[0];
    $(this).parents('tr').remove();
    re_sort_no(table);
  })
  $("#tbody-pic").on('click', '.modify', function() {
    var table = $(this).parents('table')[0];
    $(this).parents('tr').remove();
    re_sort_no(table);
    $('#upload-button').trigger('click');
  })
  $(".table").on('click', '.up', function() {
    if ($(this).parents('tr').find('td.t_no').text().trim() == 1) {
      showMsg("顶出天际了");
      return false;
    }
    var table = $(this).parents('table')[0]
    var data = $(this).parents('tr').data('subject');
    var c = $(this).parents('tr').clone();
    if (!!data) {
      c.data('subject', data);
    }
    $(this).parents('tr').prev().before(c)
    $(this).parents('tr').remove();
    re_sort_no(table);
  })
  $(".table").on('click', '.down', function() {
    var table = $(this).parents('table')[0];
    if ($(this).parents('tr').find('td.t_no').text().trim() == $(table).find('tbody tr').length) {
      showMsg("兄台，到底儿了");
      return false;
    }

    var c = $(this).parents('tr').clone();
    $(this).parents('tr').next().after(c)
    $(this).parents('tr').remove();
    re_sort_no(table);
  })

  var re_sort_no = function(table) {
    var list = $(table).find('td.t_no');
    $.each(list, function(i, item) {
      $(this).text(i + 1);
    })
  };

  $("#showCreate").on('click', 'input.subject_type', function() {
    var type = 'radio';
    if ($(this).hasClass('multi')) {
      type = 'checkbox';
    }
    var inputs = $("#showCreate div.answer input.answer_c");
    if (!!inputs && inputs.length > 0) {
      var oldType = inputs[0].type;
      if (oldType != type) {
        inputs.attr('type', type);
      }
    }
  })

  $('#main-container').on('click', '#confirmCreate', function() {
    var c = $("#showCreate");

    var subject = {};
    subject.trainId = trainId;
    subject.subjectId = c.find('#subject_id').val() || 0;
    subject.content = c.find('#desc').val() || '';
    var subjectType = c.find('input.subject_type:checked').attr("id");
    if (subjectType == 'type_single') {
      subject.type = 0;
    } else if (subjectType == 'type_multi') {
      subject.type = 1;
    } else {
      showMsg('请选择答案类型');
      return false;
    }
    var answers = [];
    var selected = 0;
    $.each(c.find(".answer"), function() {
      var answer = {};
      answer.answerId = $(this).find('.answerId').val();
      answer.content = $(this).find('.answer-content').val();
      answer.subjectId = subject.id;
      if ($(this).find('.answer_c')[0].checked) {
        answer.type = 1;
        selected += 1;
      } else {
        answer.type = 2;
      }
      answers.push(answer);
    })
    if (selected == 0) {
      showMsg('请选择正确答案');
      return false;
    } else if (subject.type == 1 && selected < 2) {
      showMsg('多选题正确答案数需大于2');
      return false;
    }

    subject.examSubjectAnswers = answers;

    if (!checkSubject(subject)) {
      return false;
    }

    $("#showCreate").modal('hide');

    var line_no = $(this).data('line_no');
    if (line_no < 0) {
      add_subject(subject);
    } else {
      update_subject(subject, line_no);
    }

    updateGrade();
  })

  $('#tbody-dispatcher-list').on('click', '.modify', function() {
    clear_create();
    var subject = $(this).parents('tr').data('subject');
    fill_subject_form(subject);
    var line_no = $(this).parents('tr').find('td.t_no').text();
    $("#confirmCreate").data('line_no', line_no);
    $("#showCreate").modal();
    $("#subject_title").text('修改题目');
  })

  $('#batch-del').click(function() {
    var checked = $('#tbody-pic input:checked');
    $.each(checked, function() {
      $(this).parents('tr').remove();
    })
    re_sort_no($(".tbody-pic")[0]);
    $("#pic-table thead input:checked").click();
  });

  var fill_subject_form = function(subject) {
    if (!!subject) {
      var s = $("#subject-contain");
      s.find("input#subject_id").val(subject.subjectId);
      if (subject.type == 0) {
        s.find('#type_single').click();
      } else {
        s.find('#type_multi').click();
      }
      s.find('#desc').val(subject.content);
      s.find('.answer').remove();
      $.each(subject.examSubjectAnswers, function() {
        add_answer_item(this, subject.type);
      })
    } else {
      showMsg('读取题目信息失败');
    }
  }

  var add_answer_item = function(answer, subject_type) {
    var index = $("#showCreate .answer").length;
    if (index > ANS_TAGS.length - 1) {
      showMsg('答案过多了亲');
      return false;
    }
    var type = subject_type == 0 ? 'radio' : 'checkbox';
    var checked = answer.type == 1;
    var answerItem = '<div class="row clearfix answer text-center center-block">' + '<input class="answerId" name="answerId" type="hidden" value="' + answer.answerId + '"/>' + '<div class="col-md-2 column answer_d text-right">' + '答案<span class="ans_tag">' + ANS_TAGS[index] + '</span>' + '</div>' + '<div class="col-md-4 column" style="height:34px">' + '<input type="text" class="center-block answer-content" style="width:100%;margin-top:5px" value="' + answer.content + '"/>' + '</div>' + '<div class="col-md-3 column">' + '<button  class="add-answer btn btn-success">+</button>&nbsp;' + '<button  class="rm-answer btn btn-danger">-</button>' + '</div>' + '<div class="col-md-3 column text-left answer_d">' + '<input name="answer_c" class="answer_c" type="' + type + '" ';
    if (checked) {
      answerItem += ' checked=true ';
    }
    answerItem += ' style="margin-top:8px" />' + '<label class="check_lab">正确答案</label>' + '</div>' + '</div>';
    $("#subject-contain").append(answerItem);
    refresh_ans_tag();
  }

  function add_subject(subject) {
    var t = $("#tbody-dispatcher-list");
    var index = t.find('tr').length + 1;
    var type = subject.type == 0 ? '单选题' : '多选题';
    var line = $(tableAddSubject(index, subject.content, type, subject.examSubjectAnswers));
    t.append(line);
    t.find('tr:last').data('subject', subject)
  };

  function tableAddSubject(index, content, type, answers) {
    var answerBrs = '';
    if (!!answers) {
      for (var i in answers) {
        answerBrs += '<br/>' + ANS_TAGS[i] + '.&nbsp&nbsp;' + answers[i].content;
        if (answers[i].type == 1) {
          answerBrs += '&nbsp;(答案)';
        }
      }
    }
    return '<tr><td width="10%" class="t_no">' + index + '</td><td width="45%" class="t_content">' + content + answerBrs + '</td>' + '<td width="15%" class="t_type">' + type + '</td>' + '<td width="30%" class="t_op">' + '<input type="button" value="修改" class="btn btn-primary btn-warning js_submitbtn modify" />&nbsp;' + '<input type="button" value="删除" class="btn btn-primary btn-danger js_submitbtn del" />&nbsp;' + '<input type="button" value="上移" class="btn btn-primary btn-info js_submitbtn up" />&nbsp;' + '<input type="button" value="下移" class="btn btn-primary btn-info js_submitbtn down" />' + '</td></tr>';
  };

  function update_subject(subject, index) {
    var t = $("#tbody-dispatcher-list");
    var row = t.find('tr').eq(index - 1);
    var type = subject.type == 0 ? '单选题' : '多选题';
    if (!!row) {
      var content = subject.content;
      var answers = subject.examSubjectAnswers;
      if (!!answers) {
        for (var i in answers) {
          content += '<br/>' + ANS_TAGS[i] + '.&nbsp&nbsp;' + answers[i].content;
          if (answers[i].type == 1) {
            content += '&nbsp;(答案)';
          }
        }
      }

      row.find('td.t_content').html(content);
      row.find('td.t_type').text(type);
      row.data('subject', subject);
    } else {
      showMsg('没有找到待更新条目');
    }
  }

  function checkSubject(subject) {
    if (!subject) {
      showMsg('题目信息为空，请检查');
      return false;
    }
    if (isUpdate && !subject.trainId) {
      showMsg('题目所属培训为空');
      return false;
    }
    if (!subject.content) {
      showMsg('题目内容不能为空');
      return false;
    }
    if (subject.type != 0 && subject.type != 1) {
      showMsg('题目类型有错');
      return false;
    }
    if (!subject.examSubjectAnswers) {
      showMsg('请设置题目答案');
      return false;
    }
    return checkAnswers(subject.examSubjectAnswers);

  }

  function checkAnswers(answers) {
    if (answers.length <= 0) {
      showMsg('答案不能为空，请设置');
    }
    for (var i in answers) {
      var a = answers[i];
      if (!a.content) {
        showMsg('答案不能为空，请检查');
        return false;
      }
      if (!a.type || (a.type != 1 && a.type != 2)) {
        showMsg('答案类型错误');
        return false;
      }
    }
    return true;
  }

  $("input[name=org_type]").click(function() {
    var id = this.id;
    if (id === org_type_id) {
      return;
    }
    org_type_id = id;
    eventList[id]();
    applyRoleGen(roleList[id]);
  });

  function removeOtherOrgType(data) {
    var totalOrgType = 4;
    while (totalOrgType) {
      if (data != totalOrgType) {
          $('input[name=org_type][orgtype=' + totalOrgType + ']').parent().remove();
      }
      totalOrgType--;
    }
  }

  function initUserAuthType() {
    $.post('/train/getUserAuthType', function(data) {
      data = data.data;
      if (!data) {
        showMsg('获取用户权限失败', null, function() {
            location.replace('/train/list');
        });
        return ;
      }
      $('input[name=org_type][orgtype=' + data + ']').click();
      removeOtherOrgType(data);
      $('#main-container').css('visibility', 'visible');
      $('#loading').hide();
    });
  }

  //加载培训详情
  loadTrainDetail();

  function loadTrainDetail() {
    initUserAuthType();
    if (!trainId || trainId == 0) {
      initDatePicker();
      return false;
    }
    var postData = {};
    postData.trainId = trainId;

    t.utils.post('/train/getTrainDetail', postData, function(r) {
      if (r.httpSuccess) {
        if (r.data.code == 0) {
          fillTrain(r.data.data);
        } else {
          showMsg(r.data.msg);
        }

      } else {
        showMsg('数据加载出错，请重试');
      }
      $('#loading').hide();
    });
  }

  $('#commit_train').click(function() {
    var train = collectTrain();
    if (checkTrain(train)) {
      if (train.orgType == 2 || train.orgType == 3) {
        train.sTime = new Date(train.sTime + ' 00:00:00') / 1000;
        train.eTime = new Date(train.eTime + ' 23:59:59') / 1000;
      }

      var url = (!train.id || train.id == 0) ? '/train/addNewTrain' : '/train/updateTrain';
      var postData = {
        trainEntry: JSON.stringify(train)
      }
      $('#loading').show();
      t.utils.post(url, postData, function(r) {
        if (r.httpSuccess) {
          if (r.data.code == 0) {
            showMsg(isUpdate ? "修改考试成功" : "创建考试成功", null, function() {
              location.href = '/train/list';
            }, '确定');
          } else {
            showMsg(r.data.msg);
          }
        } else {
          showMsg('数据提交出错，请重试');
        }
        $('#loading').hide();
      });
    }
  });

  $('#cancel_train').click(function() {
    location.href = '/train/list';
  });

  //收集考试信息
  function collectTrain() {
    var train = {};

    train.id = trainId;
    train.title = $('#train_title').val().trim();
    train.description = $('#train_desc').val().trim();
    train.orgType = $('input[name=org_type]:checked').attr("orgtype");
    train.duration = $('input[name=exam-time]').val().trim();
    if (train.orgType == 2 || train.orgType == 3) {
      train.sTime = $('.reportrange input')[0].value.trim();
      train.eTime = $('.reportrange input')[1].value.trim();
    } else {
      train.force = $('#force_true').is(':checked') == true ? 1 : 0;
      train.content = $('#train_content').val().trim();
    }

    var contents = [];
    var content_trs = $('#tbody-pic tr');
    $.each(content_trs, function() {
      var content = {};
      content.trainId = trainId || 0;
      content.id = $(this).attr('id') || 0;
      content.content = $(this).find('a.t_url')[0].href;
      content.viewSeq = $(this).find('td.t_no').text();
      contents.push(content);
    })
    train.trainSubjectParams = contents;

    var subjects = [];
    var subject_trs = $('#tbody-dispatcher-list tr');
    $.each(subject_trs, function() {
      var subject = $(this).data('subject') || {};
      subjects.push(subject);
    })
    train.examSubjectView = subjects;

    var roleCheckedList = $('.checkbox-wrapper input:checked');
    var applyRoles = [];
    roleCheckedList.each(function() {
      applyRoles.push($(this).val());
    });
    train.applyRoles = applyRoles.join(',');
    return train;
  }

  function fillTrain(train) {
    trainId = train.id;
    var startDate = moment(train.sTime * 1000).format('YYYY-MM-DD');
    var endDate = moment(train.eTime * 1000).format('YYYY-MM-DD');
    initDatePicker(startDate, endDate);
    $('input[name=exam-time]').val(train.duration);
    $('#train_title').val(train.title);
    $('#train_content').val(train.content);
    $('#train_desc').val(train.description);
    $('input[name=org_type][orgtype=' + train.orgType + ']').click();
    if (train.force == 1) {
      $('#force_true').click();
      $('#force_true').attr('checked', 'true');
    } else {
      $('#force_false').click();
    }
    if (train.applyRoles) {
      var applyRoles = train.applyRoles.split(',');
      applyRoles.forEach(function(key) {
        $('input[value=' + key + ']').click();
      });
    }
    if (!!train.trainSubjectParams) {
      for (var i in train.trainSubjectParams) {
        var content = train.trainSubjectParams[i];
        var index = new Number(i) + 1;
        var tr = '<tr id="' + content.id + '" ><td width="5%"><input type="checkbox" />' + '</td><td width="5%" class="t_no">' + index + '</td><td width="60%" class="t_pic"><a target="_blank" class="t_url" href="' + content.content + '">' + content.content + '</a></td>' + '<td width="30%" class="t_op"><input type="button" value="修改" class="btn btn-warning js_submitbtn modify" />&nbsp' + '<input type="button" value="删除" class="btn btn-danger js_submitbtn del" />&nbsp' + '<input type="button" value="上移" class="btn btn-info js_submitbtn up" />&nbsp' + '<input type="button" value="下移" class="btn btn-info js_submitbtn down" /></td>';
        $(".tbody-pic").append(tr);
      }
    }
    if (!!train.examSubjectView) {
      var subject_c = $("#tbody-dispatcher-list");
      for (var i in train.examSubjectView) {
        var subject = train.examSubjectView[i];
        var type = subject.type == 0 ? '单选题' : '多选题';
        var tr = tableAddSubject(Number(i) + 1, subject.content, type, subject.examSubjectAnswers);
        subject_c.append(tr);
        subject_c.find('tr:last').data('subject', subject);
      }
    }
    updateGrade();
    $('#loading').hide();
  }

  function checkTrain(train, type) {
    if (!train) {
      showMsg('考试对象为空');
      return false;
    }
    if (!train.title) {
      showMsg('考试名称不能为空');
      return false;
    }

    if (!train.description) {
      showMsg('备注不能为空');
      return false;
    }
    if (!train.applyRoles) {
      showMsg('考生角色不能为空');
      return false;
    }
    if (train.orgType == 2 || train.orgType == 3) {
      if (!train.sTime || !train.eTime) {
        showMsg('考试周期不能为空');
        return false;
      }
      if (!train.duration) {
        showMsg('考试时间不能为空');
        return false;
      }
    } else {
      if (!train.content) {
        showMsg('APP展示文案不能为空');
        return false;
      }
    }
    if (!train.examSubjectView.length) {
      showMsg('考试试题不能为空');
      return false;
    }
    if (train.orgType == 1) {
      return checkContent(train.trainSubjectParams);
    }
    return true;
  };

  function checkContent(contents) {
    if (contents.length == 0) {
      showMsg('培训内容不能为空');
      return false;
    }
    for (var i in contents) {
      var c = contents[i];
      if (!c.content) {
        showMsg('培训图片地址为空');
        return false;
      }
      if (!c.viewSeq) {
        showMsg('培训内容序号为空');
        return false;
      }
    }
    return true;
  };

  //增加垂直居中的方法
  jQuery.fn.center = function() {
    this.css('position', 'absolute');
    this.css('top', ($(window).height() - this.height()) / 2 + $(window).scrollTop() + 'px');
    this.css('left', ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + 'px');
    return this;
  };
  // 使用方法
  $("#loadingPic").center();

  var eventList = {
    'org_type_crew': showCrew,
    'org_type_rider': showRider,
    'org_type_self_construct': showSelfConstruct,
    'org_type_affiliate': showAffiliate
  };

  var roleList = {
    'org_type_crew': {
      '1001': '众包骑手'
    },
    'org_type_rider': {
      '1-1001': '自建骑手',
      '2-1001': '加盟骑手'
    },
    'org_type_self_construct': {
      '5001': '配送经理',
      '2001': '组织负责人',
      '2100': '调度员'
    },
    'org_type_affiliate': {
      '3001': '渠道经理',
      '2001': '组织负责人',
      '2100': '调度员'
    }
  };

  var org_type_id = $('input[name=org_type]:checked')[0].id;

  function initOrgType() {
    $.post('/train/getUserOrgType', function(ret) {
      var orgType = ret.data;
      if (orgType === 1) {
        $('#org_type_crew').parent().remove();
        $('#org_type_rider').click();
      } else if (orgType === 4) {
        $('#org_type_rider').parent().remove();
        $('#org_type_self_construct').parent().remove();
        $('#org_type_affiliate').parent().remove();
        $('#org_type_crew').click();
      } else {
        location.replace('train/list');
      }
    })
  }

  function showCrew() {
    commonShow();
    $('.limit').show();
    $('.app-item').hide();
    $('.train-content').parent().show();
  }

  function showRider() {
    commonHide();
    $('.limit').show();
    $('.app-item').hide();
    $('.train-content').parent().show();
  }

  function showSelfConstruct() {
    commonHide();
    $('.limit').hide();
    $('.app-item').show();
    $('.train-content').parent().hide();
  }

  function showAffiliate() {
    commonHide();
    $('.limit').hide();
    $('.app-item').show();
    $('.train-content').parent().hide();
  }

  function commonShow() {
    $(".crew-item").show();
    $(".other-item").hide();
  }

  function commonHide() {
    $(".crew-item").hide();
    $(".other-item").show();
  }

  function updateGrade() {
    var length = $('#tbody-dispatcher-list tr').length;
    var average;
    if (length) {
      average = (100 / length).toFixed(2);
    } else {
      average = 0;
    }
    $('.exam-count').text(length);
    $('.average-grade').text(average);
  }

  function applyRoleGen(list) {
    var dom = '';
    Object.keys(list).forEach(function(key) {
      dom += '<input value="' + key + '" type="checkbox"/><label for="role">' + list[key] + '</label>';
    })
    $('.checkbox-wrapper').html(dom);
  }

  function initDatePicker(startDate, endDate) {
    var startDate = startDate ? startDate : moment().add(1, 'd').format('YYYY-MM-DD');
    var endDate = endDate ? endDate : moment().add(7, 'd').format('YYYY-MM-DD');
    var optionSetting = {
      startDate: startDate,
      endDate: endDate,
      minDate: '2012-01-01',
      maxDate: moment().add(1, 'y').format('YYYY-MM-DD'),
      dateLimit: {
        days: 60
      },
      showDropdowns: true,
      showWeekNumbers: true,
      timePicker: false,
      timePickerIncrement: 1,
      timePicker12Hour: true,
      opens: 'right',
      buttonClasses: ['btn btn-default'],
      applyClass: 'btn-sm btn-success',
      cancelClass: 'btn-sm',
      format: 'YYYYMMDD',
      separator: ' to ',
      locale: {
        applyLabel: '应用',
        cancelLabel: '取消',
        fromLabel: '从',
        toLabel: '到',
        customRangeLabel: '定制',
        daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
        monthNames: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        firstDay: 1
      }
    };

    function datepickerCallback(start, end, label) {
      $('.reportrange input')[0].value = start.format('YYYY-MM-DD');
      $('.reportrange input')[1].value = end.format('YYYY-MM-DD');
    };

    $('.reportrange input')[0].value = startDate;
    $('.reportrange input')[1].value = endDate;
    $('.reportrange').daterangepicker(optionSetting, datepickerCallback);
  }
});
