require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
});

require(['module/root'], function (t) {

  var examData;
  var trainId;
  var endTime;
  var params = location.search.slice(1).split('&').reduce(function(prev, item) {
    item = item.split('=');
    prev[item[0]] = item[1];
    return prev;
  }, {});

  if (params.result) {
    getResult();
  } else {
    getExam();
  }

  function showMsg(msg, commit){
      var body = $('<div style="height: 100px;line-height: 100px;text-align: center;">'+msg+'</div>');
      var buttons = [t.ui.createDialogButton('close', '确定', function() {})];
      if (commit) {
        buttons = [
            t.ui.createDialogButton('close', '确定', function() {
              $.post('/train/examinee/commitExam', getExamAnswer())
              .done(function() {
                location.replace(location.pathname + location.search + '&result=ture');
              })
              .fail(function() {
                showMsg('交卷失败');
              });
            }),
            t.ui.createDialogButton('close', '取消', function() {})
        ];
      }
      t.ui.showModalDialog({
          body: body, buttons: buttons,
          style:{".modal-dialog":{"padding":"100px 110px"},
                 ".modal-footer":{"padding":"10px"},
                 ".btn-default":{"font-size":"12px"}
          }
      });
  }

  trainId = params.train;

  function getExam() {
    $('#main-container').removeClass('hide');
    $.post('/train/examinee/getExam', {trainId: params.train}, function(data) {
      data = data.data;
      if (!data) {
        location.replace('/train/examinee');
        return;
      }
      var trainEndTime = data.trainEndTime;
      var dateString = moment(trainEndTime*1000).format('MM月DD日');
      if (!data.stime) {
        $.post('/train/examinee/uploadExamTime', {
          stime: Math.floor((new Date())/1000),
          trainId: params.train
        })
      }
      examData = data;
      var now = moment(new Date());
      endTime = moment(new Date(data.duration*60*1000 + data.stime*1000));
      function changeTime() {
        var now = moment(new Date());
        var time = moment.duration(endTime.diff(now));
        if (time <= 0) {
          clearInterval(intervalTaskId);
          $.post('/train/examinee/commitExam', getExamAnswer())
          .done(function() {
            showMsg('时间到, 自动交卷');
            location.replace('/train/examinee');
          });
        }
        $('.hour').html(time.hours());
        $('.minutes').html(time.minutes());
        $('.second').html(time.seconds());
      }
      var intervalTaskId = setInterval(changeTime, 1000);
      changeTime();
      $('.exam-title').html(data.bmTrainTitle);
      var subjectCount = 0;
      for (subjectViewIndex in data.examSubjectViews) {
        var subjectView = data.examSubjectViews[subjectViewIndex];
        subjectCount+=1;
        var subjectType = "radio";
        if(!!subjectView.type && subjectView.type == 1){
          subjectType = "checkbox";
        }
        var item = "<div class='row item subject-wrap' rel='"+subjectView.type+"'><div class='col-xs-12 big-f subject'>"
            + subjectCount
            + "."
            + subjectView.content
            + "</div><div class='col-xs-12 big-f answer-c'>";

        subjectView.examineeAnswers?subjectView.examineeAnswers.forEach(function(key) {
          setTimeout(function(){$('input[answerid=' + key + ']').click()});
        }):'';
        var count = 1;
        subjectView.examSubjectAnswers.forEach(function(answer) {
          item += "<div class='answer'><label>" + numberConvert(count) +". </label><input name='" + answer.subjectId
          + "' type='"+subjectType+"' subjectId='"
          + answer.subjectId + "' answerId='"
          + answer.answerId + "' "
          +" /><span>"
          + answer.content + "</span></div>";
          count++;
        });
        item += '</div></div>';
        $('#exam-container').append(item);

      }
    });
  }

  function getResult() {
    $.post('/train/examinee/getExamResult', {trainId: params.train}, function(data) {
      data = data.data;
      $('.save').addClass('hide');
      $('.commit').addClass('hide');
      $('.close-button').removeClass('hide');
      $('.score-analysis').removeClass('hide');
      $('h3').hide();
      $('#main-container').removeClass('hide');
      if (!data) {
        return;
      }
      $('.close-button').click(function() {
        location.replace('/train/examinee');
      });
      examData = data;
      $('.score-analysis .score').html(data.score);
      $('.score-analysis .true-answer').html(data.correctNum);
      $('.score-analysis .question-count').html(data.totalNum);
      $('.exam-title').html(data.bmTrainTitle);
      var subjectCount = 0;
      for (subjectViewIndex in data.examSubjectViews) {
        var subjectView = data.examSubjectViews[subjectViewIndex];
        subjectCount+=1;
        var subjectType = "radio";
        if(subjectView.type && subjectView.type == 1){
          subjectType = "checkbox";
        }
        var item = "<div class='row item subject-wrap' rel='"+subjectView.type+"'><div class='col-xs-12 big-f subject'>"
            + subjectCount
            + "."
            + subjectView.content
            + "<label class='noti'></label></div><div class='col-xs-12 big-f answer-c'>";

        var hasAnswer = {};
        subjectView.examineeAnswers?subjectView.examineeAnswers.forEach(function(key) {
          hasAnswer[key] = true;
        }):'';
        var count = 1;
        subjectView.examSubjectAnswers.forEach(function(answer) {
          var trueAnswer = '';
          if (answer.type === 1) {
            trueAnswer = 'trueAnswer="' + answer.subjectId + '"';
          }
          var checkedAnswer = '';
          if (hasAnswer[answer.answerId]) {
            checkedAnswer = 'checked';
          }
          item += "<div class='answer'><label>" + numberConvert(count) +". </label><input name='" + answer.subjectId
          + "' type='"+subjectType+"' subjectId='"
          + answer.subjectId + "' answerId='"
          + answer.answerId + "' "
          + trueAnswer
          + checkedAnswer
          +" /><span>"
          + answer.content + "</span></div>";
          count++;
        });
        item += '</div></div>';
        $('#exam-container').append(item);
        $('input').attr('disabled', true);
        $('.subject-wrap').each(function() {
          var answers = [];
          var trues = [];
          $(this).find('.answer input').each(function(key) {
            var me = $(this);
            key = key + 1;
            if (me.prop('checked')) {
              answers.push(numberConvert(key));
            }
            if (me.attr('trueAnswer')) {
              trues.push(numberConvert(key));
            }
          });
          var labelLeft = '';
          if (answers.length) {
            labelLeft = '(您选择的答案是' + answers.join(',');
          } else {
            labelLeft = '(您未选择答案';
          }
          var label = labelLeft  + '，正确答案是' + trues.join(',') + ')';
          $(this).find('.noti').html(label);
        });
      }
    });
  }

  $('.save').click(function() {
    $.post('/train/examinee/saveAnswer', getExamAnswer())
    .done(function() {
      location.replace('/train/examinee');
    })
    .fail(function() {
      showMsg('保存失败');
    });
  });

  $('.commit').click(function() {
    showMsg('是否交卷?', true);
  });

  function getExamAnswer() {
    var data = {
      bmTrainId: trainId,
      bmPaperId: examData.bmPaperId,
      bmExamSingleAnswerDetails: [],
      bmExamMultiAnswerDetails: []
    };
    var bmExamSingleAnswer = {};
    var bmExamMultiAnswer = {};
    $('input:checked').each(function(index, el){
      var el = $(el);
      if (el.attr('type') === 'radio') {
        bmExamSingleAnswer[el.attr('subjectId')] = Number(el.attr('answerid'));
      } else {
        if (!bmExamMultiAnswer[el.attr('subjectId')]) {
          bmExamMultiAnswer[el.attr('subjectId')] = [];
        }
        bmExamMultiAnswer[el.attr('subjectId')].push(Number(el.attr('answerid')));
      }
    });
    for(key in bmExamSingleAnswer) {
      data.bmExamSingleAnswerDetails.push({
        bmSubjectId: Number(key),
        bmAnswerId: bmExamSingleAnswer[key]
      });
    }
    for(key in bmExamMultiAnswer) {
      data.bmExamMultiAnswerDetails.push({
        bmSubjectId: Number(key),
        bmAnswerIds: bmExamMultiAnswer[key]
      });
    }
    return {answerEntry: JSON.stringify(data)};
  }

  function numberConvert(data){
    return (Number(data) + 9).toString(16).toUpperCase();
  }

});
