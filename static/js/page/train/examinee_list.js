require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
});

require(['module/root'], function (t) {

  var pageSize = 10;

  var statusList = {
    '-1': '未开始',
    '0': '未开始',
    '1': '考试中',
    '2': '已结束'
  };

  var operateList = {
    '-1': '未开始',
    '0': '<a class="start-exam" data-url="/train/examinee/exam?train={trainId}">开始考试</a>',
    '1': '<a href="/train/examinee/exam?train={trainId}">继续答题</a>',
    '2': '<a href="/train/examinee/exam?train={trainId}&result=ture">查看</a>',
  };

  $('.tbody-dispatcher-list').on('click', '.start-exam', function() {
    var me = $(this);
    var duration = me.parents('tr').find('.t_time').text().split('/')[1];
    showMsg(duration, me.data('url'));
  });

  function showMsg(duration, url) {
    var durationString;
    if (duration) {
      durationString = '<p>本次考试时长：'
      + duration
      + '分钟<p/>';
    } else {
      durationString = '';
    }
    var msg = '<p>亲~您可以选择在考试周期内任意时间进行答题！~但是确认开始考试后，那么需要在规定时间内完成哦~否则到时系统会自动提交滴~<p/>'
      + durationString
      + '<p>确认现在开始考试嘛？</p>';
    var body = $('<div style="height: 130px;line-height: 20px;">' + msg + '</div>');
    t.ui.showModalDialog({
      body: body,
      buttons: [
        t.ui.createDialogButton('close', '确认', function() {
          location.replace(url);
        }),
        t.ui.createDialogButton('close', '取消', function() {})
      ],
      style: {
        ".modal-dialog": {
          "padding": "100px 110px"
        },
        ".modal-footer": {
          "padding": "10px"
        },
        ".btn-default": {
          "font-size": "12px"
        }
      }
    });
  }

  function updateExamList(pageNo) {
    $.post('/train/examinee/exam/list', {pageNo: pageNo||1, pageSize: pageSize}, function(data) {
      data = data.data;

      var render = '';
      var examListTemplate = $("#examListTemplate").html();

      if (!data.length) {
        $('.exam-manage').html('');
      }

      data.forEach(function(item) {
         var startDate = moment(item.sTime*1000).format('YYYY.MM.DD');
         var endDate = moment(item.eTime*1000).format('YYYY.MM.DD');
         var config = {
           title: item.title,
           cycle: startDate + ' - ' + endDate,
           time: item.duration ? item.answerDuration + '/' +item.duration : '无',
           score: item.score,
           wrong: item.wrongCount,
           status: statusList[item.status],
           operate: operateList[item.status].replace(/{trainId}/g, item.trainId)
         };
         var tempTemplate = examListTemplate;
         for (key in config) {
           var reg = new RegExp('{' + key + '}', 'g');
           tempTemplate = tempTemplate.replace(reg, config[key]);
         }
         render += tempTemplate;
      });
      $('.tbody-dispatcher-list').html(render);
    })
  }

  updateExamList();

  initPager();
  // 全局初始化分页数据
  function initPager() {
    $.get('/train/examinee/exam/getCount')
      .done(function(data) {
        var trainCount = data.data;
        var totalPages;
        if (trainCount % pageSize === 0) {
          totalPages = trainCount / pageSize;
        } else {
          totalPages = (trainCount / pageSize) + 1;
        }
        $('.pager-wrap .pager').twbsPagination({
          totalPages: totalPages,
          visiblePages: 10,
          first: '首页',
          prev: '上一页',
          next: '下一页',
          last: '尾页',
          onPageClick: function(event, page) {
            updateExamList(page);
          }
        });
      });
  }
});
