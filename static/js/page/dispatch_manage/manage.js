require.config({
	baseUrl: MT.STATIC_ROOT + '/js',
	urlArgs: 'ver=' + pageVersion,
	shim: {}
});


require(['module/root', 'module/cookie', 'lib/datepicker', 'lib/vue', 'page/dispatch_manage/moment'], function(r, cookie, datepicker, Vue, moment) {
	var noop = function() {};
	// 一些初始化
	(function() {
		datepicker.set({maxDate: new Date()})

		$('body').on('mouseenter', '.tooltip_icon', function(e) {
			$(this).siblings('.version_desc').addClass('show');
		})
		$('body').on('mouseleave', '.tooltip_icon', function(e) {
			$(this).siblings('.version_desc').removeClass('show');
		})
	})()


	var basePage = new Vue({

		el: "#basePage",
		data: {
			currentComp: null,
			compShow: false,
			isCityInit: false,
			conditions: {
				orgType: -1,
				cityId: -1,
				areaId: -1,
				dispatchStrategy: -1,
				endTime: '',
				pageNum: 1,
				pageSize: 20,
			},
			page: {
				current: 1,
				target: 1,
				totalPage: 1,
				totalData: 0
			},
			cityList: [],			// 城市列表
			areaList: [],			// 区域列表
			results: [],			// 搜索结果列表
			targetArea: {},			// 修改的区域
			targetAreaId: 0,
			history: [],
		},
		created: function() {
			var today = new Date();
			today.setHours(23);
			today.setMinutes(59);
			today.setSeconds(59);
			this.conditions.endTime = Math.floor(today.getTime() / 1000);

			this._getCityArea(-1);		// 获取所有城市、区域列表
		},
		ready: function() {
			this._search();				// 初始化查询
		},
		methods: {
			_export: function() {
				var data = {
					orgType: this.conditions.orgType,
					cityId: this.conditions.cityId,
					areaId: this.conditions.areaId,
					dispatchStrategy: this.conditions.dispatchStrategy,
					endTime: this.conditions.endTime
				}
				var arr = [];
				for (k in data) {
					var str = k + '=' + data[k];
					arr.push(str);
				}
				var url = '/areaDispatch/export?' + arr.join('&');
				window.open(url);
			},
			_getCityArea: function(cityId) {
				var me = this;
				var callback = function(res) {
					if (!me.isCityInit) {
						me.cityList = res.bmCityList;
						me.isCityInit = true;

						$('#cityId').select2({width: 150});
					}

					me.areaList = cityId > 0 ? res.bmDeliveryAreaList : [];			// 全部城市只能对应选择全部区域，而不是将所有的区域罗列，没有获取全部区域的接口
					$('#areaId').select2({width: 150});
				}
				_ajax('/partner/dispatch/homeLoginInfo', {cityId: cityId}, callback)
			},
			_searchBtn: function() {
				this.conditions.pageNum = 1;
				this._search();
			},
			_search: function() {
				var vm = this;
				var callback = function(res) {
					vm.results = res.areaDispatchStrategyList;
					vm.page.current = res.currentPage;
					vm.page.totalPage = res.totalPage;
					vm.page.totalData = res.total;
					vm.page.target 	  = 1;
				}
				_ajax('/areaDispatch/getAreaStrategyList', vm.conditions, callback)
			},
			_pageSwitch: function(n) {
				var n = parseInt(n);
				if (n < 1 || n > this.page.totalPage) {
					return false;
				}
				this.conditions.pageNum = n;
				this._search()
			},
			_edit: function(areaId) {
				var vm = this;
				var callback = function(res) {
					vm.targetAreaId = areaId;
					vm.targetArea = res;
					vm.compShow   = true;
					vm.currentComp = 'editComp';
				}
				_ajax('/areaDispatch/getAreaCurrentDispatchStrategyInfo', {areaId: areaId}, callback)
			},
			_editAll: function() {
				this.compShow = true;
				this.currentComp = 'editAllComp';
			},
			_history: function(areaId) {
				var vm = this;
				console.info('查询历史的区域ID是' + areaId);
				var callback = function(res) {
					vm.history = res;
					vm.compShow = true;
					vm.currentComp = 'historyComp'
				}
				_ajax('/areaDispatch/getAreaHistoryDispatchStrategyInfo', {areaId: areaId}, callback)
			},
			_newVersion: function() {
				this.compShow = true;
				this.currentComp = 'newComp';
			},
			_defaultSetting: function() {
				settingPage._init();
				settingPage.show = true;
			}
		},
		watch: {
			'conditions.cityId': function(val) {
				this._getCityArea(val)
			}
		},
		components: {			// 弹出层作为一个小组件
			newComp: {
				template: document.getElementById('J_new_tpl').innerHTML,
				data: function() {
					return {
						dispatchStrategy: '1',
						strategyVersion: '',
						versionCode: '',
						versionDesc: ''
					}
				},
				methods: {
					_save: function() {
						var vm = this;
						var data = {
							dispatchStrategy: vm.dispatchStrategy,
							strategyVersion: vm.strategyVersion,
							versionCode: vm.versionCode,
							versionDesc: vm.versionDesc
						}
						if (!/^\d+$/.test(data.strategyVersion)) {
							alert('版本号只能为正整数');
							return false;
						}
						var callback = function() {
							alert('添加成功');
							vm._cancel();
						}
						_ajax('/areaDispatch/createStrategyVersion', data, callback, 'POST')
					},
					_cancel: function() {
						this.$parent.compShow = false;
					}
				}
			},
			editComp: {
				props: ['target', 'targetid'],
				template: document.getElementById('J_edit_tpl').innerHTML,
				data: function() {
					return {
						areaIds: [],				// 区域ID
						dispatchStrategy: '',		// 调度模式
						strategyVersion: '',		// 版本号
						isForceAssign: '0',			// 是否强制
						specialSet: '',				// 特殊设置
						versionList: [],			// 对应调度模式可选的版本列表
					}
				},
				created: function() {
					var vm = this;
					vm.areaIds.push(vm.targetid);
					vm.specialSet = vm.target.specialSet || '';

					var list = vm.target.strategyList;
					if (!list) {return false;}			// 可能存在大量数据异常情况
					vm.dispatchStrategy = vm.target.strategyList[0].id;
					for (var i = 0, n = list.length; i < n; i ++) {
						if (list[i].name == vm.target.dispatchStrategyName) {
							vm.dispatchStrategy = list[i].id;
						}
					}
					vm.isForceAssign = vm.target.isForceReassign == '是' ? '1' : '0';

					vm._getVersion(vm.dispatchStrategy);
				},
				methods: {
					_getVersion: function(id) {
						var vm = this;
						var callback = function(res) {
							vm.versionList = res;
							vm.strategyVersion = vm.versionList[0].strategyVersion;
							for (var i = 0, n = vm.versionList.length; i < n; i ++) {
								if (vm.versionList[i].versionCode == vm.target.versionCode) {
									vm.strategyVersion = vm.versionList[i].strategyVersion;
								}
							}
						}
						_ajax('/areaDispatch/getAllStrategyVersionOfStrategyType', {dispatchStrategy: id}, callback)
					},
					_save: function() {
						var vm = this;
						if (!confirm('是否确认修改？')) {
							return false;
						}
						var data = {
							dispatchStrategy: vm.dispatchStrategy,
							areaIds: vm.areaIds,
							strategyVersion: vm.strategyVersion,
							isForceAssign: vm.isForceAssign,
							specialSet: vm.specialSet
						}
						var callback = function() {
							alert('修改成功');
							vm._cancel();
						}
						_ajax('/areaDispatch/updateAreaDispatchStrategyInfo', data, callback, 'POST')
					},
					_cancel: function() {
						this.$parent.compShow = false;
					}
				},
				filters: {
					_unixDate: function(val) {
						return moment.unix(val).format('YYYY-MM-DD')
					}
				}
			},
			editAllComp: {
				template: document.getElementById('J_id_tpl').innerHTML,
				data: function() {
					return {
						areaIds: [],				// 区域ID
						dispatchStrategy: '',		// 调度模式
						strategyVersion: '',		// 版本号
						isForceAssign: '0',			// 是否强制
						specialSet: '',				// 特殊设置
						strategyList: [],
						versionList: [],			// 对应调度模式可选的版本列表
						checked: false,
					}
				},
				methods: {
					_checking: function() {
						this.checked = false;
					},
					_check: function() {
						var vm = this;
						var areaIds = vm.areaIds;
						var n = areaIds.length;
						if (!n) {
							alert('区域ID不能为空');
							return false;
						}
						for (var i = 0; i < n; i ++) {
							if (/[^\d,]/.test(areaIds[i])) {
								alert('区域ID含有非法字符，请重新输入');
								return false;
							}
						}
						var callback = function(res) {
							if (!res.length) {
								alert('区域ID对应调度模式不统一，请重新输入');
								return false;
							}else {
								vm.strategyList = res;
								vm.dispatchStrategy = vm.strategyList[0].id;
								vm._getVersion(vm.dispatchStrategy);
								vm.checked = true;
							}

						}
						_ajax('/areaDispatch/getDispatchStrategyListByAreaIds', {areaIds: vm.areaIds}, callback);
					},
					_getVersion: function(id) {
						var vm = this;
						var callback = function(res) {
							vm.versionList = res;
							vm.strategyVersion = vm.versionList[0].strategyVersion;
						}
						_ajax('/areaDispatch/getAllStrategyVersionOfStrategyType', {dispatchStrategy: id}, callback)
					},
					_save: function() {
						var vm = this;
						if (!confirm('是否确认修改？')) {
							return false;
						}
						var data = {
							dispatchStrategy: vm.dispatchStrategy,
							areaIds: vm.areaIds,
							strategyVersion: vm.strategyVersion,
							isForceAssign: vm.isForceAssign,
							specialSet: vm.specialSet
						}
						var callback = function() {
							alert('修改成功');
							vm._cancel();
						}
						_ajax('/areaDispatch/updateAreaDispatchStrategyInfo', data, callback, 'POST')
					},
					_cancel: function() {
						this.$parent.compShow = false;
					}
				},
				filters: {
					_strToArr: {
						write: function(val) {
							return val.split(',');
						}
					}
				}
			},
			historyComp: {
				props: ['history'],
				data: function() {
					return {
						param: '',
						vshow: false,
					}
				},
				template: document.getElementById('J_history_tpl').innerHTML,
				methods: {
					_cancel: function() {
						this.$parent.compShow = false;
					},
					_viewParam: function(str) {
						this.param = str;
						this.vshow = true;
					},
					_closeView: function() {
						this.vshow = false;
					}
				},
				filters: {
					_unixDate: function(val) {
						return moment.unix(val).format('YYYY-MM-DD HH:mm:ss')
					}
				}
			}
		},
		filters: {
			_dateToStamp: {					// 将日期输入的格式与时间戳格式相互转换
				read: function(val) {
					var date  = new Date(val * 1000);
					var year  = date.getFullYear(),
						month = date.getMonth() + 1,
						day   = date.getDate();
					return year + '-' + _db(month) + '-' + _db(day)
				},
				write: function(val) {
					var dates = val.split(/-/);
					var year  = dates[0],
						month = dates[1] - 1,
						day   = dates[2];
					var date  = new Date(year, month, day, 23, 59, 59);
					return Math.floor(date.getTime() / 1000);
				}
			},
			_unixDate: function(val) {
				return moment.unix(val).format('YYYY-MM-DD')
			}
		}

	})


	var settingPage = new Vue({
		el: '#settingPage',
		data: {
			show: false,
			typeList: [{name: '纯抢', val: 1}, {name: '人工', val: 5}, {name: '众包', val: 8}, {name: '城市代理', val: 10}, {name: '派单', val: 12}],
			zijianLimit: [],
			jiamengLimit: [],
			zhongbaoLimit: [],
			cityproxyLimit: [],
			// 请求数据
			zijian: [],
			jiameng: [],
			zhongbao: [],
			cityproxy: [],
			//
			tuiqiangList: [],
			paidanList: [],
			chunqiangList: [],
			zhongbaoList: [],
			rengongList: [],
			//
			tuiqiangDefault: '',
			paidanDefault: '',
			chunqiangDefault: '',
			zhongbaoDefault: '',
			rengongDefault: '',
			// 根据调度模式修改
			dispatchStrategy: '1',			// 默认要选择一种调度模式
			versionList: [],
			strategyVersion: '',
			versionCode: '',
			versionDesc: ''
		},
		methods: {
			_init: function() {
				var vm = this;

				var cb1 = function(res) {
					vm.zijianLimit = res.zijian.limit;
					vm.jiamengLimit = res.jiameng.limit;
					vm.zhongbaoLimit = res.zhongbao.limit;
					vm.cityproxyLimit = res.cityproxy.limit;

					vm.zijian = res.zijian.defaultVal;
					vm.jiameng = res.jiameng.defaultVal;
					vm.zhongbao = res.zhongbao.defaultVal;
					vm.cityproxy = res.cityproxy.defaultVal;
				}
				_ajax('/areaDispatch/getAllStrategySetting', {}, cb1)

				var cb2 = function(res) {
					vm.tuiqiangList  = res.tuiqiang;
					vm.paidanList 	 = res.paidan;
					// todo 返回值这里是 chuqiang
					vm.chunqiangList = res.chunqiang;
					vm.zhongbaoList  = res.zhongbao;
					vm.rengongList   = res.rengong;

					vm.tuiqiangDefault 	= vm._fd(vm.tuiqiangList);
					vm.paidanDefault 	= vm._fd(vm.paidanList);
					vm.chunqiangDefault = vm._fd(vm.chunqiangList);
					vm.zhongbaoDefault 	= vm._fd(vm.zhongbaoList);
					vm.rengongDefault 	= vm._fd(vm.rengongList)
				}
				_ajax('/areaDispatch/getAllStrategyVersion', {}, cb2)

				vm._getVersionInfo();
			},
			_fd: function(list) {		// 获取默认的设置
				var res;
				for (var i = 0, n = list.length; i < n; i ++) {
					if (list[i].isDefault == 1) {
						res = list[i].strategyVersion;
					}
				}
				return res;
			},
			_getVersionInfo: function() {
				var vm = this;
				var callback = function(res) {
					vm.versionList = res;
					vm.strategyVersion = vm._fd(vm.versionList);
				}
				_ajax('/areaDispatch/getAllStrategyVersionOfStrategyType', {dispatchStrategy: vm.dispatchStrategy}, callback)
			},
			_selectVersion: function() {
				var vm = this;
				var id = this.strategyVersion;
				for (var i = 0, n = this.versionList.length; i < n; i ++) {
					if (id == this.versionList[i].strategyVersion) {
						this.versionCode = this.versionList[i].versionCode;
						this.versionDesc = this.versionList[i].versionDesc;
					}
				}
			},
			_saveOrgTypeDefault: function() {			// 保存业务类型支持默认修改
				var callback = function() {
					alert('保存成功');
				}
				var data = {
					zijian: this.zijian,
					jiameng: this.jiameng,
					zhongbao: this.zhongbao,
					cityproxy: this.cityproxy
				};
				for (var k in data) {
					if (data[k].length == 0) {
						alert('默认选择不可为空');
						return false;
					}
				}
				_ajax('/areaDispatch/saveAllStrategySetting', data, callback, 'POST')
			},
			_saveVersionDefault: function() {			// 保存版本默认修改
				var callback = function() {
					alert('保存成功');
				}
				var data = {
					tuiqiangDefaultVersion: this.tuiqiangDefault,
					paidanDefaultVersion: this.paidanDefault,
					chunqiangDefaultVersion: this.chunqiangDefault,
					zhongbaoDefaultVersion: this.zhongbaoDefault,
					rengongDefaultVersion: this.rengongDefault
				}
				_ajax('/areaDispatch/saveStrategyDefaultVersion', data, callback, 'POST')
			},
			_saveVersionInfo: function() {				// 保存版本信息
				var vm = this;
				var callback = function() {
					alert('保存成功');
					vm._init()
				}
				var data = {
					strategyVersion: vm.strategyVersion,
					versionCode: vm.versionCode,
					versionDesc: vm.versionDesc
				}
				_ajax('/areaDispatch/updateStrategyVersion', data, callback, 'POST');
			},
			_cancel: function() {
				this.show = false;
			}
		},
		watch: {
			'strategyVersion': function() {
				this._selectVersion()
			}
		}
	})

	// select2与Vue组合使用时，虽然触发了select的change事件，却无法触发数据双向绑定的change。暂时采用手动修改vue模型中属性的方法hack处理。
	$('.select2_element').on('change', function(e) {
		var prop = $(this).attr('id');
		basePage.conditions[prop] = $(this).val();
	})

	function _db(n) {					// 强行两位数
		return n < 10 ? '0' + n : n;
	}
	function _timeToDate(time) {
		var date = new Date(time);
		return date.getFullYear() + '-' + _db(date.getMonth() + 1) + '-' + _db(date.getDate());
	}

	// utils
	function _ajax(url, data, callback, type) {
		var callback = callback || noop;
		var	type = type || 'POST';
		// ajax here
		$.ajax({
			url: url,
			type: type,
			data: data,
			traditional: true,
			success: function(res) {
				if (res.code == 0) {
					callback(res.data)
				}else {
					alert(res.msg)
				}
			}
		})
	}

});