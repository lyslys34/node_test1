require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root', 'module/validator','module/cookie'], function (t, validator,cookie) {

    $(function(){
       // initProxyPoi();
    });
    function initProxyPoi(){
        var url="/proxyPoi/getPoiNameMap.ajax";
        t.utils.post(url, null, function (r) {
            if(r.httpSuccess){
                if(r.data.success){
                    //给页面添加一个代购商城市Id和name的隐藏域
                    $("#proxyPoiMap").val(r.data.data);
                    return true;
                }
            }else{
                showMsg("获取失败,系统错误!");
                return false;
            }
        });
    }

    //新建
    $("#create").click(function(){
        $("#showTitle").html("新建商家");
        _showEditPanel(false);
    });

    //新建时给poi添加失去焦点事件
    $("#wmPoiId").bind('blur',function(){
        var poiId=$("#wmPoiId").val();
        if(isNull(poiId)){
            showMsg("poiId不能为空");
            return false;
        }
//        var map=$("#proxyPoiMap").val();
//        if(map!=null){
//            if(map.hasOwnProperty(poiId)){
//                var poiName=map[poiId];
//                $("#poiName").html(poiName);
//            }else{
//                $("#poiName").html("没取出来");
//            }
//            return true;
//        }
        var postData={wmPoiId:poiId};
        var url="/proxyPoi/getPoiName.ajax";
        t.utils.post(url, postData, function (r) {
            if (r.httpSuccess) {
                if(r.data.success) {
                    $("#poiName").html(r.data.resultMsg);
                        return false;
                } else {
                        $("#poiName").html("没取出来");
                        return false;
                }

            } else {
                    showMsg("提交失败,系统错误!");
                    return false;
            }
        });
     })
    function isNull( str ){
        if(str==null){
            return true;
        }
        if ( str == "" ) return true;
        var regu = "^[ ]+$";
        var re = new RegExp(regu);
        return re.test(str);
    }
    //编辑
    $(".edit").click(function(){
        var id = $(this).parent().attr("bmPoiId");
        var wmPoiId=$(this).parent().attr("wmPoiId");
        var wmPoiName=$(this).attr("wmPoiName");
        $("#id").prop("value",id);
        $("#wmPoiId").prop("value",wmPoiId);
        $("#poiName").html(wmPoiName);
        $("#showTitle").html("编辑商家");
        _showEditPanel(true);
    });

    //删除
    $(".del").click(function(){
        if (!confirm('确定要删除该商家信息吗?')) return;
        var id = $(this).parent().attr("bmPoiId");
        var wmPoiId=$(this).parent().attr("wmPoiId");
        var postData={id:id,wmPoiId:wmPoiId};
        var url="/proxyPoi/delete.ajax";
        post(url,postData);
    });

    function _showEditPanel(edit) {
         var body = $("#create-container");
         body.removeClass("hide");
         var buttons = [];
         buttons =  [
                   t.ui.createDialogButton('ok','保存', function () {
                       var poiId=$("#wmPoiId").val();
                       var discount=$("#discount").val();
                       if(isNull(poiId)){
                           showMsg("代购商Id不能为空");
                           return false;
                       }
                       if(discount>100){
                           showMsg("折扣不能大于100");
                           return false;
                       }
                       save(edit);
                       return false;
                   }),

                   t.ui.createDialogButton('close','取消', function(){
                    closePanel();
                    return true;
                   })
         ];
         if(edit){
             disablePanel();
         }
         var dlg = t.ui.showModalDialog({
             body: body, buttons: buttons,
             style:{".modal-content":{"width":"530px"}}
         });
     }


    function closePanel(){
        $("#create-container :text").val("").attr("disabled",false);
        $("#poiName").html("");
        $("#create-container :hidden").val("0");
    }
    function disablePanel(){
        $("#wmPoiId").prop("disabled","disabled");
    }

    //提交表单
    function save(edit){
        if(edit){
            $("#wmPoiId").prop("disabled",false);
        }
        var url = $("#proxyPoiForm").attr("action");
        var form = $("#proxyPoiForm").serializeArray();
        var postData = {};
        var valid = true;

        $.each(form,function(i,field){
            var $this = $("[name="+field.name+"]");
            if($this.attr("required") && field.value == ""){
                if(field.name=='discount'){
                    showMsg("折扣不能为空");
                }else{
                    showMsg($this.attr("placeholder"));
                }
                valid = false;
                return false;
            }
            postData[field.name] = field.value;
        });
        if(!valid){
            return false;
        }

        t.utils.post(url, postData, function (r) {
          if (r.httpSuccess) {
              if(r.data.success ) {
                  location.reload(true);
                  return false;
              } else {
                  showMsg(r.data.resultMsg);
                  return false;
              }

          } else {
              showMsg("提交失败,系统错误!");
              return false;
          }
        });
    }
    function showMsg(msg,reload){
        var body = $('<div style="height: 100px;padding: 15px;text-align: center;">'+msg+'</div>');
        t.ui.showModalDialog({
            body: body, buttons: [
                t.ui.createDialogButton('close','关闭', function () {     // 取消
                    if(reload){
                        location.reload(true);
                    }
                })
            ],
            style:{".modal-dialog":{"padding":"100px 110px"},
                   ".modal-footer":{"padding":"10px"},
                   ".btn-default":{"font-size":"12px"}
            }
        });
    }

    function post(url,postData){
        t.utils.post(url, postData, function (r) {
          if (r.httpSuccess) {
              if(r.data.success) {
                  location.reload(true);
                  return false;
              } else {
                  showMsg(r.data.resultMsg);
                  return false;
              }

          } else {
              showMsg("提交失败,系统错误!");
              return false;
          }
        });
    }
});