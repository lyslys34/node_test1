require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/root','module/popmsg','module/ajaxManager','module/validator','lib/jquery.tmpl'],function(t,popmsg,ajaxManager,validator){
	var averageList = {},distanceList = {},aveSubsidyList = {},disSubsidyList = {},peakSubsidyList = {},packageSubsidyList = {},weatherSubsidyList = {},tempSubsidyList = {};
	var shopId = void 0,cityIds = [],cityNames = [],shopName = void 0;
	var comfirmFlag = true;
//	var initAverageVal = void 0, initDistanceVal = void 0;
	var changeFlag = false;
	var currentLink = void 0;
	var AJAXURL = {
		GET_AVERAGE_LIST:'/settlePlan/list?subtype=11&type=10',
		GET_DISTANCE_LIST:'/settlePlan/list?subtype=12&type=10',
		GET_AVE_SUBSIDY_LIST:'/settlePlan/list?subtype=31&type=30',
		GET_DIS_SUBSIDY_LIST:'/settlePlan/list?subtype=32&type=30',
		GET_PEAK_SUBSIDY_LIST:'/settlePlan/list?subtype=41&type=40',
		GET_PACKAGE_SUBSIDY_LIST:'/settlePlan/list?subtype=42&type=40',
		GET_WEATHER_SUBSIDY_LIST:'/settlePlan/list?subtype=43&type=40',
		GET_TEMP_SUBSIDY_LIST:'/settlePlan/list?subtype=44&type=40',
		GET_CITY_AND_AREA:'/baseData/cityArea/list',
		SAVE_PLAN:'/settlePlan/city/bind'
	}

	var dataMap = {
		'/settlePlan/list?subtype=11&type=10':['#shopAverage','#cityAverage','#areaAverage',averageList],
		'/settlePlan/list?subtype=12&type=10':['#shopDistance','#cityDistance','#areaDistance',distanceList],
		'/settlePlan/list?subtype=31&type=30':['#cityAverageSubsidy','#areaAverageSubsidy','#shopAverageSubsidy',aveSubsidyList],
		'/settlePlan/list?subtype=32&type=30':['#cityDistanceSubsidy','#areaDistanceSubsidy','#shopDistanceSubsidy',disSubsidyList],
		'/settlePlan/list?subtype=41&type=40':['#cityPeakSubsidy','#areaPeakSubsidy','#peakSubsidy',peakSubsidyList],
		'/settlePlan/list?subtype=42&type=40':['#areaPackageSubsidy','#cityPackageSubsidy','#packageSubsidy',packageSubsidyList],
		'/settlePlan/list?subtype=43&type=40':['#areaWeatherSubsidy','#cityWeatherSubsidy','#weatherSubsidy',weatherSubsidyList],
		'/settlePlan/list?subtype=44&type=40':['#areaTempSubsidy','#cityTempSubsidy','#tempSubsidy',tempSubsidyList]
	}

/*	var p1 = new Promise(function(resolve,rejected){
		ajaxManager.getServerData(AJAXURL.GET_AVERAGE_LIST,{},function(data){
			var str = JSON.stringify(data).split('\\"').join('"').split('"{').join('{').split('}"').join('}');
			initAverageList(JSON.parse(str).settlePlanVos);
			resolve();
		},false)
	})

	var p2 = new Promise(function(resolve,rejected){
		ajaxManager.getServerData(AJAXURL.GET_DISTANCE_LIST,{},function(data){
			var str = JSON.stringify(data).split('\\"').join('"').split('"{').join('{').split('}"').join('}');
			initDistanceList(JSON.parse(str).settlePlanVos);
			resolve();
		},false)
	})

	Promise.all([p1,p2]).then(function(){
		$('.modal').find('.l-block').find('input[type="radio"]').trigger('change');
	}) */

	for(var url in dataMap){
		(function(url){
			ajaxManager.getServerData(url,{},function(data){
				var str = JSON.stringify(data).split('\\"').join('"').split('"{').join('{').split('}"').join('}');
				var list = JSON.parse(str).settlePlanVos;
				var str = '';
				list.map(function(content){
					dataMap[url][dataMap[url].length-1][content.id] = content;
					str+='<option value="'+content.id+'">'+content.name+'</option>';
				})
				if(url==AJAXURL.GET_PEAK_SUBSIDY_LIST){
					for(var r in peakSubsidyList){
						var maxIndex = 0,maxfee = 0;
						for(var i=0;i<peakSubsidyList[r].rule.rule.length;i++){
							if(parseFloat(peakSubsidyList[r].rule.rule[i].fee)>maxfee){
								maxfee = parseFloat(peakSubsidyList[r].rule.rule[i].fee);
								maxIndex = i;
							}
						}
						peakSubsidyList[r].rule.rule[maxIndex].max = true;
					}
				}
				for(var i=0;i<dataMap[url].length-1;i++){
					$(dataMap[url][i]).html(str);
				}
			},false)
		})(url)
	}

	initCityAndArea();

	$('.city-wrap').on('click','span',function(){
		var id = $(this).attr('data-id');
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$('#cityBlock').find('span[data-id="'+id+'"]').remove();
			var index = cityIds.indexOf(id);
			cityIds.splice(index,1);
			cityNames.splice(index,1)
		}else{
			$(this).addClass('active');
			$('#cityBlock').append('<span data-id="'+id+'">'+$(this).html()+'ID'+id+'  </span>');
			$('#cityBlock').scrollTop(10000);
			cityIds.push($(this).attr('data-id'));
			cityNames.push($(this).html());
		}
	})


	$('.modal').on('hidden.bs.modal',function(){
		$(this).find('input[type="checkbox"],input[type="radio"]').removeAttr('checked');
		$(this).find('select').each(function(){
			$(this).val($(this).children().eq(0).val());
		});
		$(this).find('.t-block').find('.r-block').html('');
		changeFlag = false;
	})

	$('#citySettle').on('hidden.bs.modal',function(){
		$('#cityBlock').html('');
		$('.all-city').find('.active').removeClass('active');
		cityIds = [];
		cityNames = [];
	})


	function initCityAndArea(){
		ajaxManager.getServerData(AJAXURL.GET_CITY_AND_AREA,{},function(data){
			var cityList = data.cityAreaVos;
			var cityStr = '',areaStr = '';
			cityList.forEach(function(r,i){
				cityStr+='<span data-id="'+r.id+'">'+r.name+'</span>';
			});
			$('.city-wrap').html(cityStr);
		},false)
	}

	$('#setCity').bind('click',function(){
		changeFlag = false;
		$('#citySettle').modal('show');
		$('#citySettle').find('.b-title-wrap').find('button').eq(0).trigger('click');
	});


	$('#citySettle,#shopSettle').find('.b-title-left').find('button').bind('click',function(){
		if($(this).parent().hasClass('active')){
			return;
		}
		var $dom = $(this).parents('.b-title-wrap').next();
		var $domWrap = $dom.parents('.modal');
		var $detailWrap = $dom.parents('.modal').find('.t-block').find('.r-block');
		var names = [];
		$detailWrap.find('.table-title').each(function(){
			names.push($(this).html());
		})
		if(changeFlag||cityIds.length>0){
			if(confirm('您当前选择的方案为'+ (names.join(",")||'无方案') +'，是否确定要保存？')){
				comfirmFlag = false;
				$domWrap.find('.modal-footer').find('.btn-success').trigger('click');
				return;
			}else{
				$('#cityBlock').html('');
				$('.all-city').find('.active').removeClass('active');
				cityIds = [];
				cityNames = [];
				$domWrap.find('input[type="checkbox"],input[type="radio"]').removeAttr('checked');
				$domWrap.find('select').each(function(){
					$(this).val($(this).children().eq(0).val());
				});
				if($domWrap.attr('id')=='shopSettle'){
					popModal(currentLink);
				}
			}
		}
		$(this).parent().siblings().removeClass('active');
		$(this).parent().addClass('active');
		var index = $(this).attr('data-index');
		$dom.find('.l-block').children().hide();
		$dom.find('.l-block').children().eq(index).show();
		$dom.find('.l-block').children().eq(index).find('input').eq(0).trigger('change');
		if(index==2&&$domWrap.find('input[type="checkbox"]:checked').length<=0){
			changeFlag = true;
		}else{
			changeFlag = false;
		}
		
	})

	$('#shopList').find('.setlink').bind('click',function(){
		currentLink = $(this);
//		initShopModal();
		popModal($(this));
		$('input[name="shop-price"]').trigger('change');
		shopName = $(this).attr('data-name');
		shopId = $(this).attr('data-id'); 
		$('#shopSettle').find('.shop').html(shopName+'ID'+shopId);
		changeFlag = false; 
		$('#shopSettle').modal('show');
		$('#shopSettle').find('.b-title-wrap').find('button').eq(0).trigger('click');
	})	

	$('#shopList').find('.setSubsidylink').bind('click',function(){
		currentLink = $(this);
//		initShopModal();
		popModal($(this));
		$('input[name="shop-price"]').trigger('change');
		shopName = $(this).attr('data-name');
		shopId = $(this).attr('data-id'); 
		$('#shopSettle').find('.shop').html(shopName+'ID'+shopId);
		changeFlag = false; 
		$('#shopSettle').modal('show');
//		if($(this).attr('data-subtype-subsidy')||!$(this).attr('data-subtype-add-subsidy')){
		$('#shopSettle').find('.b-title-wrap').find('button').eq(1).trigger('click');
//		}else{
//			$('#shopSettle').find('.b-title-wrap').find('button').eq(2).trigger('click');
//		}
	})	

	function popModal($el){
		var val = $el.attr('data-subtype'); 
		var subsidyVal = $el.attr('data-subtype-subsidy');
		var subsidypeakVal = $el.attr('data-subtype-peak-subsidy');
		var subsidypackageVal = $el.attr('data-subtype-package-subsidy');
		var subsidyweatherVal = $el.attr('data-subtype-weather-subsidy');
		var subsidytempVal = $el.attr('data-subtype-temp-subsidy');
		if(val==11){
			$('input[name="shop-price"]')[0].checked = true;
			$('#shopAverage').val($el.attr('data-planId'));
		}else if(val==12){
			$('input[name="shop-price"]')[1].checked = true;
			$('#shopDistance').val($el.attr('data-planId'));
		}
		if(subsidyVal==31){
			$('input[name="shop-subsidy"]')[0].checked = true;
			$('#shopAverageSubsidy').val($el.attr('data-planId-subsidy'));
		}else if(subsidyVal==32){
			$('input[name="shop-subsidy"]')[1].checked = true;
			$('#shopDistanceSubsidy').val($el.attr('data-planId-subsidy'));
		}
		if(subsidypeakVal){
			$('input[name="shop-add-subsidy"]')[0].checked = true;
			$('#peakSubsidy').val($el.attr('data-planId-peak-subsidy'));
		}
		if(subsidypackageVal){
			$('input[name="shop-add-subsidy"]')[1].checked = true;
			$('#packageSubsidy').val($el.attr('data-planId-package-subsidy'));
		}
		if(subsidyweatherVal){
			$('input[name="shop-add-subsidy"]')[2].checked = true;
			$('#weatherSubsidy').val($el.attr('data-planId-weather-subsidy'));
		}
		if(subsidytempVal){
			$('input[name="shop-add-subsidy"]')[3].checked = true;
			$('#tempSubsidy').val($el.attr('data-planId-temp-subsidy'));
		}
	}

	$('.modal').find('.l-block').find('.radio').find('input[type="radio"],select').bind('change',function(){
		var $dom = $(this).parents('.radio').find('input[type="radio"]:checked');
		var subtype = $dom.val();
		var id = $dom.parent().next().val();
		var $wrap = $(this).parents('.l-block').next();
		if(!subtype){
			changeFlag = true;
			$wrap.html('');
			return;
		}
		showPro($wrap,subtype,id);
	});

	$('.modal').find('.l-block').find('.checkbox').find('input[type="checkbox"],select').bind('change',function(){
		var $dom = $(this).parents('.l-block').find('input[type="checkbox"]:checked');
		var subtype = [];
		var id = [];
		$dom.each(function(){
			subtype.push($(this).val());
			id.push($(this).parent().next().val());
		});
		var $wrap = $(this).parents('.l-block').next();
		if(!subtype.length){
			changeFlag = true;
			$wrap.html('');
			return;
		}
		showPro($wrap,subtype,id);
	}); 


	$('#shopSave,#citySave,#areaSave').bind('click',function(){
		if(ajaxManager[AJAXURL.SAVE_PLAN]){
			return;
		}
		var $detailWrap = $(this).parents('.modal-content').find('.t-block').find('.r-block');
		var names = [];
		$detailWrap.find('.table-title').each(function(){
			names.push($(this).html());
		})
		if(comfirmFlag){
			comfirmFlag = true;
			if(!confirm('您当前选择的方案为'+ (names.join(",")||'无方案') +'，是否确定要保存？')){
				return false;
			}
		}
		var param = {};
		var index = $(this).parents('.modal').find('.b-title-left').find('.active').children().attr('data-index');
		switch($(this).attr('id')){
			case 'shopSave':
				param.cityNames = shopName;
				param.cityIds = shopId;
				break;
			case 'citySave':
				if(cityIds.length<=0){
					alert('保存失败，请选择城市！');
					return;
				}
				param.cityIds = cityIds.join(',');
				param.cityNames = cityNames.join(',');
				break;
		}
		switch(index){
			case '0':
				param.planType = 10;
				break;
			case '1':
				param.planType = 30;
				break;
			case '2':
				param.planType = 40;
				break;
		}
		var planId = [];
		var fee = 0;
		if($.trim($detailWrap.html())==''&&index!=2){
			alert('保存失败，请先选择方案！')
			return;
		}
		$detailWrap.find('.pro-price').each(function(){
			fee+=parseFloat($(this).val())
		})
		if(fee>10){
			alert('附加补贴的总额不能超过10元！');
			return;
		}
		$detailWrap.find('.table-title').each(function(){
			planId.push($(this).attr('data-id'));
		})
//		var $dom = $(this).parents('.modal-content').find('input[type="radio"]:checked');
//		param.planId = $dom.parent().next().val();
		param.planId = planId.join(',');
		ajaxManager.getServerData(AJAXURL.SAVE_PLAN,param,function(data){
			if(data.success){
				$('#shopSave,#citySave,#areaSave').unbind();
				location.reload();
			}else{
				alert(data.message||'保存失败！');
			}
		})
	})


	function showPro($wrap,subtype,id){
		changeFlag = true;
		if(Object.prototype.toString.call(subtype)=='[object Array]'){
			$wrap.html('');
			for(var i=0;i<subtype.length;i++){
				var matchobj = matchPro(subtype[i]);
				if(matchobj.dataObj[id[i]]){
					$wrap.append($('#'+matchobj.tmplId).tmpl(matchobj.dataObj[id[i]]));
				}
			}
			
		}else{
			var matchobj = matchPro(subtype);
			if(matchobj.dataObj[id]){
				$wrap.html($('#'+matchobj.tmplId).tmpl(matchobj.dataObj[id]));
			}
		}
	}

	function matchPro(subtype){
		var tmplId,dataObj;
		switch(subtype){
			case '11':
				tmplId = 'averageTmpl';
				dataObj = averageList;
				break;
			case '12':
				tmplId = 'distanceTmpl';
				dataObj = distanceList;
				break;
			case '31':
				tmplId = 'aveSubsidyTmpl';
				dataObj = aveSubsidyList;
				break;
			case '32':
				tmplId = 'disSubsidyTmpl';
				dataObj = disSubsidyList;
				break;
			case '41':
				tmplId = 'peakSubsidyTmpl';
				dataObj = peakSubsidyList;
				break;
			case '42':
				tmplId = 'packageSubsidyTmpl';
				dataObj = packageSubsidyList;
				break;
			case '43':
				tmplId = 'weatherOrTempSubsidyTmpl';
				dataObj = weatherSubsidyList;
				break;
			case '44':
				tmplId = 'weatherOrTempSubsidyTmpl';
				dataObj = tempSubsidyList;
				break;
		}
		return {tmplId:tmplId,dataObj:dataObj}
	}

})