require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
});

require(['module/root', 'module/validator',  'module/cookie', 'lib/bootstrap-datetimepicker.min'], function (t, validator, cookie, datetimepicker) {

$(document).ready(function(){
    initPage();
    initAddDimensionBtn();
    initLimits();
    initChangeGroupStatusBtn();
    initAddInfoBtn();
    changeChecked();
    initDeleteDimensionBtn();
});

function initPage() {
    var nowDate = dateFormat(new Date(), 'yyyy-MM-dd hh:mm');
    var endDate = '2015-12-31 23:59:59';
//    $(".form_datetime").datetimepicker({
//        format: 'yyyy-mm-dd hh:ii',
//        minuteStep: 60,
//        startDate: nowDate,
//        endDate:endDate
//    });

    // $("#group_other_order_dimension").hide();
    $(".group_order_type").each(function() {
        var b = this.checked;
        var thisId = $(this).attr("id");

        if(thisId =="group_st_order_type") {
            if(b) {
                $("#group_st_order_dimension").show();
            } else {
                $("#group_st_order_dimension").hide();
            }
        }

        if(thisId =="group_other_order_type") {
            if(b) {
                $("#group_other_order_dimension").show();
            } else {
                $("#group_other_order_dimension").hide();
            }
        } else {
            $("#group_other_order_dimension").hide();
        }
    });

    showBtn();
}

function showBtn() {
    var id = $("#id").val();
    var revokeStatus = 3;
    var pauseStatus = 2; 
    var normalStatus = 1;
    var currentDate = dateFormat(new Date(), 'yyyy-MM-dd hh:mm');
    var startDate = $("#group_start_time").val();
    var endDate = $("#group_end_time").val();
    var status = $("#status").val();
    if(typeof id == 'undefined' || validator.isBlank(id) || id < 1) {
        $("#pauseGroup").hide();
        $("#revokeGroup").hide();
        $("#restartGroup").hide();
    } else if (status == revokeStatus) {
        $("#pauseGroup").hide();
        $("#revokeGroup").hide();
        $("#restartGroup").hide();
    } else if (status == pauseStatus) {
        $("#pauseGroup").hide();
        $("#revokeGroup").show();
        $("#restartGroup").show();
    } else if (status == normalStatus) {
        if (currentDate < startDate) {
            $("#pauseGroup").hide();
            $("#revokeGroup").show();
            $("#restartGroup").hide();
        } else if (currentDate > startDate && currentDate < endDate) {
            $("#pauseGroup").show();
            $("#revokeGroup").show();
            $("#restartGroup").hide();
        } else if (currentDate > endDate) {
            $("#pauseGroup").hide();
            $("#revokeGroup").hide();
            $("#restartGroup").hide();
        }
    }
}

function changeChecked() {
    $(".group_order_type").click(function() {
        var b = this.checked;
        var thisId = $(this).attr("id");

        if(thisId =="group_st_order_type") {
            if(b) {
                $("#group_st_order_dimension").show();
            } else {
                $("#group_st_order_dimension").hide();
            }
        }

        if(thisId =="group_other_order_type") {
            if(b) {
                $("#group_other_order_dimension").show();
            } else {
                $("#group_other_order_dimension").hide();
            }
        }
    });

    $("input[name='group_type']").click(function() {
        var groupDimension = $("input[name='group_type']:checked").val();
        var cityDimension = 1;
        var poiDimension = 3;
        if(groupDimension == cityDimension) {
            $("#addModalTitle").text("添加参与活动运单所属城市");
            $("#addDimension").text("添加城市");
            $("#deleteDimension").text("删除城市");
        }
        if(groupDimension == poiDimension) {
            $("#addModalTitle").text("添加参与活动运单所属商家");
            $("#addDimension").text("添加商家");
            $("#deleteDimension").text("删除商家");
        }
    })
}

function initAddDimensionBtn() {
	$("#addDimension").click(function() {
	    $("#addDimensionModal").modal();
	});

     $("#addDimensionSubmit").click(function() {
        var ids = $("#dimension_ids").val();
        var notIdsPatten = /[^\d,]/g;

        if(notIdsPatten.test(ids)) {
            showAddDimensionError("输入数据格式错误");
            return false;
        } 

        $("#type_ids").val(ids);
        if(typeof $("#type_ids").val() !='undefined' && $("#type_ids").val() !='') {
            $("#show_add_ids").show();
        } else {
            $("#show_add_ids").hide();
        }
        $("#addDimensionModal").modal("hide");
    });
}

function initLimits() {
    $('.input_number').keyup(function(){
        var value = $(this).val();
        $(this).val(validator.replaceNum(value));
    });
    $('#dimension_ids').keyup(function(){  
        var c=$(this).val();  
        $(this).val(c.replace(/[^\d,]/g,''));
    }); 

    $(".selectAll").click(function() {
        var b = this.checked;
        $(".selectItem").each(function() {
            this.checked = b;
        });
    });

}

function initDeleteDimensionBtn() {
    $("#deleteDimension").click(function() {
        var id = $("#id").val();
        var ids = [];
        $(".selectItem").each(function() {
            if(this.checked) {
                ids.push($(this).val());
            }
        });
        if(validator.isBlank(id)) {
            showError("无法获取补贴组");
            return false;
        }
        var b = true;
        $.each(ids, function(n,value) {
            if(value !='' && isNaN(value)) {
                showError("每一个id都必须为数字");
                b = false;
                return false;
            }
        });

        if(!b) {
            return false;
        }
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/ridersubsidy/unbindPoiGroup",
            data: {
                groupId : id,
                poiIds : ids.join(',')
            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4  class='text-center text-success'>删除成功</h4>");
                    $("#addPoiModal").modal('hide');
                    $("#showSuccessMsg").modal();
                    setTimeout('location.reload()',2000);
                }else {
                    showError(data.errMsg);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });   
    });
}

function initChangeGroupStatusBtn() {
    $("#pauseGroup").click(function() {
        var id = $("#id").val();
        if(!validator.isExist(id)) {
            showError("无法获取补贴组");
            return false;
        }

        var status = $("#status").val();
        if(typeof status == 'undefined' || status == '') {
            showError("无法获取补贴组状态");
            return false;
        }

        var revokeStatus = 3;
        var pauseStatus = 2;

        if(pauseStatus == status) {
            showError("当前状态为暂停状态， 无法再次暂停");
            return false;
        }

        if(revokeStatus == status) {
            showError("当前状态为撤销状态， 无法暂停");
            return false;
        }

        var statusMsg = $("#status_desc").val();

        $("#alert_status_massage").empty();
        $("#alert_status_massage").append("<div class='alert alert-danger' role='alert'>当前状态为 "+statusMsg+" ，确认要暂停吗？</div>");

        $("#old_status").val(status);
        $("#new_status").val(pauseStatus);
        $("#changeGroupStatusModal").modal(); 
    });

    $("#revokeGroup").click(function() {
        var id = $("#id").val();
        if(!validator.isExist(id)) {
            showError("无法获取补贴组");
            return false;
        }

        var status = $("#status").val();
        if(typeof status == 'undefined' || status == '') {
            showError("无法获取补贴组状态");
            return false;
        }

        var revokeStatus = 3;

        if(revokeStatus == status) {
            showError("当前状态为撤销状态， 无法再次撤销");
            return false;
        }

        var statusMsg = $("#status_desc").val();

        $("#alert_status_massage").empty();
        $("#alert_status_massage").append("<div class='alert alert-danger' role='alert'>当前状态为"+statusMsg+"，确认要撤销吗？</div>");

        $("#old_status").val(status);
        $("#new_status").val(revokeStatus);
        $("#changeGroupStatusModal").modal(); 
    });

    $("#restartGroup").click(function() {
        var id = $("#id").val();
        if(!validator.isExist(id)) {
            showError("无法获取补贴组");
            return false;
        }

        var status = $("#status").val();
        if(typeof status == 'undefined' || status == '') {
            showError("无法获取补贴组状态");
            return false;
        }

        var pauseStatus = 2;
        var normalStatus = 1;

        if(pauseStatus != status) {
            showError("当前状态为不是撤销状态， 无法重新开始");
            return false;
        }

        var currentDate = dateFormat(new Date(), 'yyyy-MM-dd hh:mm');
        var endDate = $("#group_end_time").val();

        if(currentDate > endDate) {
            showError("当前活动已结束， 无法重新开始");
            return false;
        }

        var statusMsg = $("#status_desc").val();

        $("#alert_status_massage").empty();
        $("#alert_status_massage").append("<div class='alert alert-danger' role='alert'>当前状态为"+statusMsg+"，确认要重新开始吗？</div>");

        $("#old_status").val(status);
        $("#new_status").val(normalStatus);
        $("#changeGroupStatusModal").modal(); 
    });

    $("#B_continue_change").click(function() {
        var status = $("#new_status").val();
        var id = $("#id").val();
        if(!validator.isExist(id)) {
            showError("无法获取补贴组");
            return false;
        }
        if(!validator.isExist(status)) {
            showError("无法获取补贴状态");
            return false;
        }
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/ridersubsidy/changeGroupStatus",
            data: {
                groupId : id,
                status : status
            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4  class='text-center text-success'>"+"修改补贴状态成功"+"</h4>");
                    $("#changeGroupStatusModal").modal('hide'); 
                    $("#showSuccessMsg").modal();
                    setTimeout("location.reload()",2000);
                }else {
                    showError(data.errMsg);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });   
    });

    $("#B_cancel_change").click(function() {
        $("#changeGroupStatusModal").modal("hide");
        return false;
    });
}

function initSearchBtn() {
    $("#search").click(function() {
        var groupName = $("#groupName");
        if (!validator.isBlank(groupName)) {
            if(validator.checkInput(groupName)) {
                showError("组名称中不能含有特殊字符");
                return false;
            }
        }

        $("#search").submit();
        
    });
}
function initAddInfoBtn() {
    $("#addInfoBtn").click(function() {
        $("#saveGroupModal").modal();
    });

    $("#B_cancel_save").click(function() {
        $("#saveGroupModal").modal("hide");
        return false;
    });

    $('#B_continue_save').on('click', function(){
        $("#saveGroupModal").modal("hide");
        var id = $("#id").val();
        if(typeof id == 'undefined' ||id == '') {
            id = 0;
        }
        var orgType = $("#org_type").val();
        if(typeof orgType == 'undefined') {
            showError("无法获取组织类型");
            return false;
        }
        var groupName = $("#group_name").val().trim();
        var groupDesc = $("#group_desc").val().trim();
        var startTime = $("#group_start_time").val().trim();
        var endTime = $("#group_end_time").val().trim();
        var groupOrderType = [];
        $(".group_order_type").each(function() {
            if(this.checked) {
                groupOrderType.push($(this).val());
            }
        }); 

        var groupDimension = $("input[name='group_type']:checked").val();
        var typeIds = $("#type_ids").val();

        var mtMoney = $("#mt_money").val();
        var orderPackageMoney = $("#orderPackageMoney").val();
        var mtOrderCeil = $("#mt_order_ceil").val();
        var otherMoney = $("#other_money").val();
        var otherOrderCeil = $("#other_order_ceil").val();

        if(validator.isBlank(groupName)) {
          showError("请填写组名");
          return false;
        }

        if(validator.isBlank(groupDesc)) {
          showError("请填写APP显示文案");
          return false;
        }

        var timePatten = /^(\d{4})\-(\d{2})\-(\d{2}) (\d{2}):(\d{2})$/;
        if(validator.isBlank(startTime)) {
          showError("请选择开始时间");
          return false;
        }
        if(!timePatten.test(startTime)) {
            showError("开始时间格式不正确");
            return false;
        }
        if(validator.isBlank(endTime)) {
          showError("请选择结束时间");
          return false;
        }
        if(!timePatten.test(endTime)) {
            showError("结束时间格式不正确");
            return false;
        }
        if(startTime >= endTime) {
            showError("开始时间不能大于结束时间");
            return false;
        }

        var b = true;
        $.each(groupOrderType, function(n,value) {
            if(value !='' && isNaN(value)) {
                showError("运单对应订单类型错误");
                b = false;
                return false;
            }
        });

        if(!b) {
            return false;
        }
        if(isNaN(groupDimension)) {
            showError("请选择活动运单范围");
            return false;
        }
        var notIdsPatten = /[^\d,]/g;
        if(notIdsPatten.test(typeIds)) {
            showError("填写的参与活动的id格式不正确");
            return false;
        }
        var dimensionIds = [];
        if(typeof typeIds != 'undefined' && typeIds != '') {
            dimensionIds = typeIds.split(",");
        }
        var mtCheck = $("#group_st_order_type").is(":checked");
        if(mtCheck) {
            if(mtMoney.length == 0 || isNaN(mtMoney)) {
                showError("美团平台补贴金额格式错误");
                return false;
            } 
            if(mtOrderCeil.length == 0 || isNaN(mtOrderCeil)) {
                showError("美团平台补贴上限格式错误");
                return false;
            }
            if(orderPackageMoney.length == 0 || isNaN(orderPackageMoney)){
                showError("美团订单包补贴金额格式错误");
                return false;
            }
            if(parseFloat(mtMoney) < parseFloat(orderPackageMoney) ){
                showError("订单包补贴金额不能大于非订单包补贴金额");
                return false;
            }
        } else {
            mtMoney = 0;
            orderPackageMoney = 0;
            mtOrderCeil = 0;
        }
        
        var otherCheck = $("#group_other_order_type").is(":checked");
        if (otherCheck) {
            if(otherMoney.length == 0 || isNaN(otherMoney)) {
                showError("非美团平台补贴金额格式错误");
                return false;
            }
            if(otherOrderCeil.length == 0 || isNaN(otherOrderCeil)) {
                showError("非美团平台补贴上限格式错误");
                return false;
            }
        } else {
            otherMoney = 0;
            otherOrderCeil = 0;
        }
        
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/ridersubsidy/saveGroup",
            data: {
                groupId : parseInt(id),
                orgType : parseInt(orgType),
                groupName : groupName,
                groupDesc : groupDesc,
                beginTime : startTime,
                endTime : endTime,
                groupOrderType : groupOrderType.join(','),
                groupDimension : parseInt(groupDimension),
                typeIds : dimensionIds.join(','),
                mtMoney : parseFloat(mtMoney),
                orderPackageMoney : parseFloat(orderPackageMoney),
                mtOrderCeil : parseInt(mtOrderCeil),
                otherMoney : parseFloat(otherMoney),
                otherOrderCeil : parseInt(otherOrderCeil)                    
            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4 class='text-center text-success'>提交成功</h4>");
                    $("#showSuccessMsg").modal();
                    setTimeout("location.href='/ridersubsidy/grouplist'",2000);
                }else {
                    showError(data.errMsg);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });
      });  
}

function gotoTop() {
    $('html,body').animate({ scrollTop: 0 }, 300);
}

function showError(errMsg) {
    gotoTop();
    $("#alert_error").empty();
    $("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

function showAddDimensionError(errMsg) {
    $("#alert_add_dimension_error").empty();
    $("#alert_add_dimension_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

function dateFormat(date, fmt) {
    var o = {   
        "M+" : date.getMonth()+1,                 //月份   
        "d+" : date.getDate(),                    //日   
        "h+" : date.getHours(),                   //小时   
        "m+" : date.getMinutes(),                 //分   
        "s+" : date.getSeconds(),                 //秒   
        "q+" : Math.floor((date.getMonth()+3)/3), //季度   
        "S"  : date.getMilliseconds()             //毫秒   
    };   
    if(/(y+)/.test(fmt)) {
        fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length));   
    }
        
    for(var k in o) {
        if(new RegExp("("+ k +")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
        }         
    }         
    return fmt;   
}

});
