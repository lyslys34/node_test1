require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/root','module/popmsg','module/ajaxManager','module/validator','lib/jquery.tmpl'],function(t,popmsg,ajaxManager,validator){
	var TIPS = {
			NAME_NOT_EXIST:'请输入方案的名称！',
			APP_NAME_NOT_EXIST:'请输入app的文案！',
			COMFIRM_DELETE:'确定要删除这个方案吗？',
			PRICE_NOT_EXIST:'金额不能为空！',
			PRICE_IS_ZERO:'金额不能大于等于0！',
			NOT_NUMBER:'金额必须是数字！',
			BEFORE_LEAVE:'你有未保存的方案，离开方案将丢失。',
			PRICE_GT_TEN:'金额不能大于10元',
			PRICE_LT_TEN:'金额不能小于-10元',
			FIXED_TWO:'金额小数点后不能多于两位',
			NEW_PRO_FAILED:'新建失败，请先保存编辑中的方案！'
	}

	var AJAXURL = {
		GET_LIST:'/settlePlan/list?subtype=42&type=40',
		DELETE_PRO:'/settlePlan/delete',
		SAVE_OR_EDIT_PRO:'/settlePlan/saveOrUpdate?subtype=42&type=40',
		SWITCH_STATUS:'/settlePlan/switch'
	}

	initPage();

	$('#add').bind('click',function(){
		if($('.save').length>0){
			alert(TIPS.NEW_PRO_FAILED);
			return;
		}
		var str = $('#proTmpl').html();
		$('#proWrap').html(str+$('#proWrap').html());
	})

	$('body').on('click','.save',function(){
		var $dom =  $(this).parents('.pro');
			$priceDom = $dom.find('.pro-price'),
			$nameDom = $dom.find('.pro-name'),
			$appDom = $dom.find('.pro-app-name');
		var price = $.trim($priceDom.val()),name = $.trim($nameDom.val()),appname = $.trim($appDom.val());
		if(validateName(name)){
			if(validateAppName(appname)){
				if(validatePrice(price)){
					var data = {planId:$dom.attr('data-id')||void 0,name:name,rule:JSON.stringify({fee:price}),appName:appname};
					ajaxManager.getServerData(AJAXURL.SAVE_OR_EDIT_PRO,data,function(data){
						if(data.success){
							initPage(true);
						}else{
							alert(data.message||'保存失败！');
						}
					})
					//saveCallback($(this),$priceDom,$nameDom);
				}
			}
		}
	})
	$('body').on('click','.edit',function(){
		var $priceDom = $(this).parents('.pro').find('.pro-price'),
			$nameDom = $(this).parents('.pro').find('.pro-name'),
			$appDom = $(this).parents('.pro').find('.pro-app-name');
		$priceDom.removeAttr('readonly');
		$nameDom.removeAttr('readonly');
		$appDom.removeAttr('readonly');
		$(this).parent().prev().hide();
		$(this).removeClass('edit').addClass('save').html('保存');
	})

	$('body').on('click','.del',function(){
		if(confirm(TIPS.COMFIRM_DELETE)){
			var $dom = $(this).parents('.pro');
			var id;
			if(id=$dom.attr('data-id')){
				ajaxManager.getServerData(AJAXURL.DELETE_PRO,{planId:id},function(data){
					if(data.success){
						initPage(true);
					}else{
						alert(data.message||'删除失败！');
					}
				})
			}else{
				$dom.remove();
			}
			
		}
	})

	$('body').on('click','.pro-open',function(){
		if($(this).hasClass('actiev')){
			return;
		}
		var planId = $(this).parents('.pro').attr('data-id');
		ajaxManager.getServerData(AJAXURL.SWITCH_STATUS,{planId:planId,status:1},function(data){
			if(data.success){
				$(this).addClass('active');
				$(this).next().removeClass('active');
			}else{
				alert(data.message||'开启方案失败！')
			}
		}.bind(this))
	})

	$('body').on('click','.pro-close',function(){
		if($(this).hasClass('actiev')){
			return;
		}
		var planId = $(this).parents('.pro').attr('data-id');
		ajaxManager.getServerData(AJAXURL.SWITCH_STATUS,{planId:planId,status:0},function(data){
			if(data.success){
				$(this).addClass('active');
				$(this).prev().removeClass('active');
			}else{
				alert(data.message||'关闭方案失败！')
			}
		}.bind(this))
	})

	window.onbeforeunload = function(){
		if($('.save').length>0){
			return TIPS.BEFORE_LEAVE;
		}
	}

	function validateName(name){
		if(validator.isBlank(name)){
			alert(TIPS.NAME_NOT_EXIST);
			return false;
		}
		return true;
	}

	function validateAppName(name){
		if(validator.isBlank(name)){
			alert(TIPS.APP_NAME_NOT_EXIST);
			return false;
		}
		return true;
	}


	function validatePrice(price){
		if(validator.isBlank(price)){
			alert(TIPS.PRICE_NOT_EXIST);
			return false;
		}
		if(isNaN(price)){
			alert(TIPS.NOT_NUMBER);
			return false;
		}
		if(price>=0){
			alert(TIPS.PRICE_IS_ZERO);
			return false;
		}
		if(price<-10){
			alert(TIPS.PRICE_LT_TEN);
			return false;
		}
		if(!validateFixedTwo(price)){
			alert(TIPS.FIXED_TWO);
			return false;
		}
		return true;
	}

	function saveCallback($saveBtn,$priceInput,$nameInput){
		$saveBtn.removeClass('save').addClass('edit').html('编辑');
		$priceInput.attr('readonly','readonly').val(toFixed($priceInput.val()));
		$nameInput.attr('readonly','readonly')
	}

	function toFixed(val){
		return Math.floor(parseFloat(val)*100)/100;
	}

	function validateFixedTwo(val){
		var pattern =/^-[0-9]+([.]\d{1,2})?$/;
		if(!pattern.test(val)){
		    return false;
		}
		return true;
	}

	function initPage(){
		ajaxManager.getServerData(AJAXURL.GET_LIST,{},function(data){
			if(data.success){
				var str = JSON.stringify(data).split('\\"').join('"').split('"{').join('{').split('}"').join('}');
				$('#proWrap').html($('#proListTmpl').tmpl(JSON.parse(str)));
			}
		},false);
	}

})