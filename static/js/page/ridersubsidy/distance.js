require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/root','module/popmsg','module/ajaxManager','module/validator','lib/jquery.tmpl'],function(t,popmsg,ajaxManager,validator){
	var TIPS = {
			NAME_NOT_EXIST:'请输入方案的名称！',
			COMFIRM_DELETE:'确定要删除这个方案吗？',
			PRICE_NOT_EXIST:'金额不能为空！',
			PRICE_IS_ZERO:'金额不能小于等于0！',
			NOT_NUMBER:'金额必须是数字！',
			BEFORE_LEAVE:'你有未保存的方案或设置，离开方案将丢失。',
			LAST_LINE:'已经是最后一行了！',
			INVALIAD:'保存不成功！输入格式有误。',
			BIGGER_THAN_NEXT:'保存不成功！ 终止点需要大于起始点。',
			PRICE_GT_TEN:'金额不能大于20元',
			NOT_EMPTY:'方案不能为空',
			FIXED_TWO:'金额小数点后不能多于两位',
			NEW_PRO_FAILED:'新建失败，请先保存编辑中的方案！'
	}

	var AJAXURL = {
		GET_LIST:'/settlePlan/list?subtype=12&type=10',
		SAVE_OR_EDIT_PRO:'/settlePlan/saveOrUpdate?subtype=12&type=10',
		DELETE_PRO:'/settlePlan/delete',
		SAVE_PRICE:'/settlePlan/abnormalDis?subtype=12'
	}

	initPage();

	$('#add').bind('click',function(){
		if($('.save').length>0){
			alert(TIPS.NEW_PRO_FAILED);
			return;
		}
		var str = $('#proTmpl').html();
		$('#proWrap').html(str+$('#proWrap').html());
	})

	$('#priceSave').bind('click',function(){
		if($(this).hasClass('price-edit')){
			$(this).removeClass('price-edit').addClass('price-save').html('保存');
			$('.price-input').eq(0).removeAttr('readonly')
		}else{
			var val = $.trim($('.price-input').eq(0).val());
			if(validatePrice(val)){
				ajaxManager.getServerData(AJAXURL.SAVE_PRICE,{money:val},function(data){
					if(data.success){
						popmsg('保存成功！');
						$(this).removeClass('price-save').addClass('price-edit').html('编辑');
						$('.price-input').eq(0).attr('readonly','readonly')/*.val(toFixed($('.price-input').val()))*/;
					}else{
						alert(data.message||'保存失败！')
					}
				}.bind(this));
			}
		}	
	})

	$('body').on('click','.addline',function(){
		var $dom = $(this).parents('.pro');
		var $tbody = $dom.find('tbody');
		var val = $tbody.find('tr').last().find('input').eq(1).val();
		var str = $('#lineTmpl').html();
		$tbody.append(str);
		$tbody.find('tr').last().find('input').eq(0).val(val);
		$dom.find('.edit').removeClass('edit').addClass('save').html('保存');
	})

	$('body').on('click','.delline',function(){
		var $dom = $(this).parents('.pro');
		var $tr = $dom.find('tbody').find('tr');
		if($tr.length<=1){
			alert(TIPS.LAST_LINE);
			return;
		}else{
			$tr.last().remove();
		}
		if($dom.find('input[readonly]').length==$dom.find('input[type="text"]').length){
			$dom.find('.save').removeClass('save').addClass('edit').html('编辑');
		}
	})

	$('body').on('input propertychange','.end-input',function(){
		var $dom = $(this).parents('tr').next();
		if($dom.length){
			$dom.find('input').eq(0).val($(this).val());
		}
	})

	$('body').on('click','.del',function(){
		if(confirm(TIPS.COMFIRM_DELETE)){
			var $dom = $(this).parents('.pro');
			var id;
			if(id=$dom.attr('data-id')){
				ajaxManager.getServerData(AJAXURL.DELETE_PRO,{planId:id},function(data){
					if(data.success){
						initPage(true);
					}else{
						alert(data.message||'删除失败！');
					}
				})
			}else{
				$dom.remove();
			}
			
		}
	})

	$('body').on('click','.save',function(){
		var $dom = $(this).parents('.pro');
		var $nameDom = $dom.find('.pro-name');
		var name = $nameDom.val();
		var $endDomlist = $dom.find('.end-input');
		var $priceDomlist = $dom.find('.price-input');
		if(validateName(name)){
			if(validateEndInput($endDomlist)){
				if(validatePriceInput($priceDomlist)){
					var props = {};
					props.planId = $dom.attr('data-id')||void 0;
					props.name = $.trim(name);
					props.rule = {};
					props.rule.rule = [];
					$dom.find('tbody').children('tr').each(function(){
						var $inputs = $(this).find('input');
						var rule = {};
						rule.start = $.trim($inputs.eq(0).val());
						rule.end = $.trim($inputs.eq(1).val());
						rule.fee = $.trim($inputs.eq(2).val());
						props.rule.rule.push(rule);
					})
					props.rule = JSON.stringify(props.rule)
					ajaxManager.getServerData(AJAXURL.SAVE_OR_EDIT_PRO,props,function(data){
						if(data.success){
						//	saveCallback($(this),$endDomlist,$priceDomlist,$nameDom);
							initPage(true);
						}else{
							alert(data.message||'保存失败！');
						}
					})
					
				}
			}
		}
	})

	$('body').on('click','.edit',function(){
		var $dom = $(this).parents('.pro');
		var $nameDom = $dom.find('.pro-name');
		var $endDomlist = $dom.find('.end-input');
		var $priceDomlist = $dom.find('.price-input');
		$endDomlist.removeAttr('readonly');
		$priceDomlist.removeAttr('readonly');
		$nameDom.removeAttr('readonly');
		$(this).removeClass('edit').addClass('save').html('保存');
		$dom.find('.right').show();
	})

	window.onbeforeunload = function(){
		if($('.save').length>0||$('.price-save').length>0){
			return TIPS.BEFORE_LEAVE;
		}
	}

	function validateName(name){
		if(validator.isBlank(name)){
			alert(TIPS.NAME_NOT_EXIST);
			return false;
		}
		return true;
	}

	function validatePrice(price){
		if(validator.isBlank(price)){
			alert(TIPS.PRICE_NOT_EXIST);
			return false;
		}
		if(isNaN(price)){
			alert(TIPS.NOT_NUMBER);
			return false;
		}
		if(price<=0){
			alert(TIPS.PRICE_IS_ZERO);
			return false;
		}
		if(price>20){
			alert(TIPS.PRICE_GT_TEN);
			return false;
		}
		if(!validateFixedTwo(price)){
			alert(TIPS.FIXED_TWO);
			return false;
		}
		return true;
	}

	function validateEndInput($domList){
		for(var i=0;i<$domList.length;i++){
			var val = $domList.eq(i).val();
			if(validator.isBlank(val)){
				alert(TIPS.NOT_EMPTY);
				return false;
			}
			if(isNaN(val)||val<=0||!isNumber(val)){
				alert(TIPS.INVALIAD);
				return false;
			}
			if(i>0){
				if(parseFloat($domList.eq(i-1).val())>=parseFloat($domList.eq(i).val())){
					alert(TIPS.BIGGER_THAN_NEXT);
					return false;
				}	
			}
		}
		return true;
	}

	function validatePriceInput($domList){
		for(var i=0;i<$domList.length;i++){
			var val = $domList.eq(i).val();
			if(validator.isBlank(val)){
				alert(TIPS.NOT_EMPTY);
				return false;
			}
			if(isNaN(val)||val<=0||!validateFixedTwo(val)){
				alert(TIPS.INVALIAD);
				return false;
			}
			if(val>20){
				alert(TIPS.PRICE_GT_TEN);
				return false;
			}
		}
		return true;
	}



	function saveCallback($saveBtn,$endDomlist,$priceDomlist,$nameDom){
		$saveBtn.removeClass('save').addClass('edit').html('编辑');
		$priceDomlist.each(function(){
			$(this).attr('readonly','readonly').val(toFixed($(this).val()));
		})
		$endDomlist.each(function(){
			$(this).attr('readonly','readonly')
		})
		$nameDom.attr('readonly','readonly');
	}

	function toFixed(val){
		return Math.floor(parseFloat(val)*100)/100;
	}

	function validateFixedTwo(val){
		var pattern =/^[0-9]+([.]\d{1,2})?$/;
		if(!pattern.test(val)){
		    return false;
		}
		return true;
	}

	function isNumber(val){
		return /^[0-9]+$/.test(val);
	}

	function initPage(showLoading){
		ajaxManager.getServerData(AJAXURL.GET_LIST,{},function(data){
			if(data.success){
				var str = JSON.stringify(data).split('\\"').join('"').split('"{').join('{').split('}"').join('}');
				$('#proWrap').html($('#proListTmpl').tmpl(JSON.parse(str)));
			}
		},showLoading?true:false);
	}
})