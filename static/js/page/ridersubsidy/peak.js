require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/root','module/popmsg','module/ajaxManager','module/validator','lib/jquery.tmpl'],function(t,popmsg,ajaxManager,validator){
	var TIPS = {
			NAME_NOT_EXIST:'请输入方案的名称！',
			APP_NAME_NOT_EXIST:'请输入app的文案！',
			COMFIRM_DELETE:'确定要删除这个方案吗？',
			PRICE_NOT_EXIST:'金额不能为空！',
			PRICE_IS_ZERO:'金额不能小于等于0！',
			NOT_NUMBER:'金额必须是数字！',
			BEFORE_LEAVE:'你有未保存的方案，离开方案将丢失。',
			PRICE_GT_TEN:'金额不能大于20元',
			FIXED_TWO:'金额小数点后不能多于两位',
			NEW_PRO_FAILED:'新建失败，请先保存编辑中的方案！',
			CAN_NOT_DELETE:'删除失败，方案的时间不能为空！',
			TIME_INVALID:'保存失败，结束时间必须大于开始时间！',
			TIME_REPEAT:'保存失败，设置的时间有重叠的时间段！'
	}

	var AJAXURL = {
		GET_LIST:'/settlePlan/list?subtype=41&type=40',
		DELETE_PRO:'/settlePlan/delete',
		SAVE_OR_EDIT_PRO:'/settlePlan/saveOrUpdate?subtype=41&type=40',
		SWITCH_STATUS:'/settlePlan/switch'
	}

	initPage();

	$('#add').bind('click',function(){
		if($('.save').length>0){
			alert(TIPS.NEW_PRO_FAILED);
			return;
		}
		var str = $('#proTmpl').html();
		$('#proWrap').html(str+$('#proWrap').html());
	})

	$('body').on('click','.save',function(){
		var $dom =  $(this).parents('.pro');
			$priceDom = $dom.find('.pro-price'),
			$nameDom = $dom.find('.pro-name'),
			$appDom = $dom.find('.pro-app-name'),
			$timeDom = $dom.find('.p-time');
		var name = $.trim($nameDom.val()),appname = $.trim($appDom.val()),price = [];
		$priceDom.each(function(){
			price.push($(this).val());
		})
		var timeObj = {
			startTime:[],
			endTime:[]
		}
		$timeDom.each(function(){
			timeObj.startTime.push($(this).find('select').eq(0).val());
			timeObj.endTime.push($(this).find('select').eq(1).val());
		})

		if(validateName(name)){
			if(validateAppName(appname)){
				if(validateTime(timeObj)){
					if(validatePrice(price)){
						var data = {planId:$dom.attr('data-id')||void 0,name:name,appName:appname};
						data.rule = {};
						data.rule.rule = [];
						for(var i=0;i<price.length;i++){
							data.rule.rule.push({startTime:timeObj.startTime[i],endTime:timeObj.endTime[i],fee:price[i]})
						};
						data.rule = JSON.stringify(data.rule);
						ajaxManager.getServerData(AJAXURL.SAVE_OR_EDIT_PRO,data,function(data){
							if(data.success){
								initPage(true);
							}else{
								alert(data.message||'保存失败！');
							}
						})
						//saveCallback($(this),$priceDom,$nameDom);
					}
				}
			}
		}
	})
	$('body').on('click','.edit',function(){
		var $priceDom = $(this).parents('.pro').find('.pro-price'),
			$nameDom = $(this).parents('.pro').find('.pro-name'),
			$appDom = $(this).parents('.pro').find('.pro-app-name'),
			$select = $(this).parents('.pro').find('select');
		$priceDom.removeAttr('readonly');
		$nameDom.removeAttr('readonly');
		$appDom.removeAttr('readonly');
		$select.removeAttr('disabled')
		$(this).parents('.pro').find('.add-time').show();
		$(this).parents('.pro').find('.del-time').show();
		$(this).parent().prev().hide();
		$(this).removeClass('edit').addClass('save').html('保存');
	})

	$('body').on('click','.del',function(){
		if(confirm(TIPS.COMFIRM_DELETE)){
			var $dom = $(this).parents('.pro');
			var id;
			if(id=$dom.attr('data-id')){
				ajaxManager.getServerData(AJAXURL.DELETE_PRO,{planId:id},function(data){
					if(data.success){
						initPage(true);
					}else{
						alert(data.message||'删除失败！');
					}
				})
			}else{
				$dom.remove();
			}
			
		}
	})

	$('body').on('click','.pro-open',function(){
		if($(this).hasClass('actiev')){
			return;
		}
		var planId = $(this).parents('.pro').attr('data-id');
		ajaxManager.getServerData(AJAXURL.SWITCH_STATUS,{planId:planId,status:1},function(data){
			if(data.success){
				$(this).addClass('active');
				$(this).next().removeClass('active');
			}else{
				alert(data.message||'开启方案失败！')
			}
		}.bind(this))
	})

	$('body').on('click','.pro-close',function(){
		if($(this).hasClass('actiev')){
			return;
		}
		var planId = $(this).parents('.pro').attr('data-id');
		ajaxManager.getServerData(AJAXURL.SWITCH_STATUS,{planId:planId,status:0},function(data){
			if(data.success){
				$(this).addClass('active');
				$(this).prev().removeClass('active');
			}else{
				alert(data.message||'关闭方案失败！')
			}
		}.bind(this))
	})

	$('body').on('click','.add-time',function(){
		var html = $('#pTimeTemplate').html();
		$(this).parent().next().after(html);
	})

	$('body').on('click','.del-time',function(){
		if($(this).parents('.pro').find('.p-time').length<=1){
			alert(TIPS.CAN_NOT_DELETE);
			return;
		}
		$(this).parent().next().remove();
		$(this).parent().remove();
	})

	window.onbeforeunload = function(){
		if($('.save').length>0){
			return TIPS.BEFORE_LEAVE;
		}
	}

	function validateName(name){
		if(validator.isBlank(name)){
			alert(TIPS.NAME_NOT_EXIST);
			return false;
		}
		return true;
	}

	function validateAppName(name){
		if(validator.isBlank(name)){
			alert(TIPS.APP_NAME_NOT_EXIST);
			return false;
		}
		return true;
	}

	function validateTime(timeObj){
		var tObj = cloneObj(timeObj)
		for(var i=0;i<tObj['startTime'].length;i++){
			tObj['startTime'][i] = parseInt(tObj['startTime'][i].split(':').join(''));
			tObj['endTime'][i] = parseInt(tObj['endTime'][i].split(':').join(''));
		}
		for(var i=0;i<tObj['startTime'].length;i++){
			if(tObj['startTime'][i]>=tObj['endTime'][i]){
				alert(TIPS.TIME_INVALID);
				return false;
			}
			for(var j=0;j<tObj['startTime'].length;j++){
				if(i!=j){
					if((tObj['startTime'][i]>tObj['startTime'][j]&&tObj['startTime'][i]<tObj['endTime'][j])||(tObj['endTime'][i]>tObj['startTime'][j]&&tObj['endTime'][i]<tObj['endTime'][j])||(tObj['endTime'][i]==tObj['endTime'][j]&&tObj['startTime'][i]==tObj['startTime'][j])){
						alert(TIPS.TIME_REPEAT);
						return false;
					}
				}
			}
		}
		return true;
	}

	function cloneObj(obj){
		var clone = {};
		for(var i in obj){
			if(Object.prototype.toString.call(obj[i])=='[object Array]'){
				clone[i] = [];
				for(var j=0;j<obj[i].length;j++){
					clone[i].push(obj[i][j]);
				}
			}else{
				clone[i] = obj[i];
			}
		}
		return clone;
	}


	function validatePrice(prices){
		for(var i=0;i<prices.length;i++){
			var price = prices[i];
			if(validator.isBlank(price)){
				alert(TIPS.PRICE_NOT_EXIST);
				return false;
			}
			if(isNaN(price)){
				alert(TIPS.NOT_NUMBER);
				return false;
			}
			if(price<=0){
				alert(TIPS.PRICE_IS_ZERO);
				return false;
			}
			if(price>20){
				alert(TIPS.PRICE_GT_TEN);
				return false;
			}
			if(!validateFixedTwo(price)){
				alert(TIPS.FIXED_TWO);
				return false;
			}
		}
		return true;
	}

	function saveCallback($saveBtn,$priceInput,$nameInput){
		$saveBtn.removeClass('save').addClass('edit').html('编辑');
		$priceInput.attr('readonly','readonly').val(toFixed($priceInput.val()));
		$nameInput.attr('readonly','readonly')
	}

	function toFixed(val){
		return Math.floor(parseFloat(val)*100)/100;
	}

	function validateFixedTwo(val){
		var pattern =/^[0-9]+([.]\d{1,2})?$/;
		if(!pattern.test(val)){
		    return false;
		}
		return true;
	}

	function initPage(){
		ajaxManager.getServerData(AJAXURL.GET_LIST,{},function(data){
			if(data.success){
				var str = JSON.stringify(data).split('\\"').join('"').split('"{').join('{').split('}"').join('}');
				$('#proWrap').html($('#proListTmpl').tmpl(JSON.parse(str)));
				$('#proWrap').find('select').each(function(){
					$(this).find('option[value="'+$(this).attr('data-value')+'"]').attr('selected','true');
				})
			}
		},false);
	}

})