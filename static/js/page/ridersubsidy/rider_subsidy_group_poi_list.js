require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});

require(['module/root', 'module/validator',  'module/cookie'], function (t, validator, cookie) {

$(document).ready(function(){
	initEditBtn();
    initLimits();
    initEditGroupSubmitBtn();
    initAddPoiBtn();
    initDeletePoiBtn();
    initChangeGroupStatusBtn();
});

function initEditBtn() {
	$("#editGroup").click(function() {
	    $("#editGroupModal").modal();
	});

    $("#addPoi").click(function() {
        $("#addPoiModal").modal();
    });
}

function initLimits() {
    // $('input').blur(function(){
    //     var value = $(this).val();
    //     $(this).val(validator.replaceInput(value));
    // });
    $('textarea').keyup(function(){
        var value = $(this).val();
        $(this).val(value.replace(/[^\d\n]/g,''));
    });
    $('input#subsidy').keyup(function(){  
            var c=$(this).val();  
            $(this).val(validator.replaceNum(c));
    }); 

    $(".selectAll").click(function() {
        var b = this.checked;
        $(".selectItem").each(function() {
            this.checked = b;
        });
    });
}

function initAddPoiBtn() {
    $("#addPoiSubmit").click(function() {
        var ids = $("#poi_ids").val();
        var id = $("#id").val();
        var idList = ids.split("\n");
        var b = true;

        if(validator.isBlank(id)) {
            showAddPoiGroupError("无法获取补贴组");
            return false;
        }

        $.each(idList, function(n,value) {
            if(value !='' && isNaN(value)) {
                showAddPoiGroupError("每一个id都必须为数字");
                b = false;
                return false;
            }
        });

        if(!b) {
            return false;
        }
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/ridersubsidy/bindPoiGroup",
            data: {
                groupId : id,
                poiIds : ids
            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4  class='text-center text-success'>添加有效商家成功</h4>");
                    $("#addPoiModal").modal('hide');
                    $("#showSuccessMsg").modal();
                    setTimeout('location.reload()',2000);
                }else {
                    showAddPoiGroupError(data.errMsg);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });   
    });
}

function initDeletePoiBtn() {
    $("#deletePoi").click(function() {
        var id = $("#id").val();
        var ids = [];
        $(".selectItem").each(function() {
            if(this.checked) {
                ids.push($(this).val());
            }
        });
        if(validator.isBlank(id)) {
            showError("无法获取补贴组");
            return false;
        }
        var b = true;
        $.each(ids, function(n,value) {
            if(value !='' && isNaN(value)) {
                showError("每一个id都必须为数字");
                b = false;
                return false;
            }
        });

        if(!b) {
            return false;
        }
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/ridersubsidy/unbindPoiGroup",
            data: {
                groupId : id,
                poiIds : ids.join(',')
            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4  class='text-center text-success'>删除商家成功</h4>");
                    $("#addPoiModal").modal('hide');
                    $("#showSuccessMsg").modal();
                    setTimeout('location.reload()',2000);
                }else {
                    showError(data.errMsg);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });   
    });
}

function initChangeGroupStatusBtn() {
    $("#changeGroupStatus").click(function() {
        var id = $("#id").val();
        if(validator.isBlank(id)) {
            showError("无法获取补贴组");
            return false;
        }

        var status = $(this).attr('rel');
        var successMsg = '';
        if(status > 1) {
            status = 1;
            successMsg = "恢复补贴成功";
        } else {
            status = 2;
            successMsg = "暂停补贴成功";
        }

        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/ridersubsidy/changeGroupStatus",
            data: {
                groupId : id,
                status : status
            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4  class='text-center text-success'>"+successMsg+"</h4>");
                    $("#addPoiModal").modal('hide');
                    $("#showSuccessMsg").modal();
                    setTimeout("location.reload()",2000);
                }else {
                    showError(data.errMsg);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });   
    });
}

function initEditGroupSubmitBtn() {
    $('#editGroupSubmit').on('click', function(){
        var groupId = $("#id").val().trim();
        var groupName = $("#groupname").val().trim();
        var subsidy = $("#subsidy").val().trim();
        var subsidyDesc = $("#subsidyDesc").val().trim();

        if(groupName == "") {
          showAddGroupError("请填写组名");
          return;
        }
        if(subsidy == "") {
          showAddGroupError("请填写配送补贴额");
          return;
        }

        if(isNaN(subsidy)) {
            showAddGroupError("配送补贴额必须为数字");
          return;
        }

        if(subsidyDesc == "") {
          showAddGroupError("请填写APP显示文案");
          return;
        }
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/ridersubsidy/updateGroup",
            data: {
                groupId : groupId,
                groupName : groupName,
                subsidy : subsidy,
                subsidyDesc : subsidyDesc
            },
            success : function(data){
                if(data.success){
                    $("#resMsg").html("<h4  class='text-center text-success'>修改组成功</h4>");
                    $("#editGroupModal").modal('hide');
                    $("#showSuccessMsg").modal();
                    setTimeout('location.reload()',2000);
                }else {
                    showAddGroupError(data.errMsg);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });
      });  
}

function showError(errMsg) {
    $("#alert_error").empty();
    $("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

function showAddPoiGroupError(errMsg) {
    $("#alert_add_poi_group_error").empty();
    $("#alert_add_poi_group_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

function showAddGroupError(errMsg) {
    $("#alert_edit_group_error").empty();
    $("#alert_edit_group_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
}

});
