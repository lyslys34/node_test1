
$(document).ready(function() {
	var statMap={};
    var ajaxbg = $("#loadingWrapper");
    var localStorage = window.localStorage;
    var basicInfo = ['配送方类型', '外卖区域', '物理城市', '站点'];
    var regionLegendArr = [],
        cityLegendArr   = [],
        orgLegendArr    = [];
    //选择的选项，from url
    var statSelsect = function(statId, menuId){
        // eg statId = $("#areaStatus");选项的状态
        //    menuId = dim_bm_region_chain;传到controller的id
        var statId = "#" + statId;
        if(statMap[statId]){
        	return true;
        }
        $.ajax({// ajax获取controller的数据
            dataType: "json",
            url: "/data/monitor/querydim",
            type: "POST",
            data: {
                dimId: menuId,
                start: "1",
                end: "2"
            },
            beforeSend: function(xhr){
                ajaxbg.addClass("show");
            },
            success: function(res){
                if ( res.stat == "ok" ) {
                    var arr = res.result,
                        body = "<option value=''>全部</option>";
                    for (var i = 0, len = arr.length; i < len; i++) {
                        body += "<option value='" + arr[i].id + "'>"+ arr[i].caption + "</option>";
                    };
                    $(statId).html(body);
                    if(menuId=="dim_bm_city_chain"){
                        $("#realityCityStatus").select2();
                    }
                    statMap[statId]=true;
                }else{
                    alert("数据获取失败！");
                }
            },
            complete:function(xhr){
                ajaxbg.removeClass("show");
            },
            error: function(error) {
                alert("网络连接失败！");
            }
        });
        return false;
    };
    statSelsect("areaStatus", "dim_bm_region_chain");// 区域

    var getTabsActive = function(){
        var active = $(".tabs-pane .active-item");
        return active.attr("id");
    }
	//图表
	var chartData = [];// 折线图y
    var areaOption = {
    	chart: {
            type: 'spline'
        },
    	title: {
    	    text: '区域数据总览',
    	    x: -20 //center
    	},
    	xAxis: { 
    		categories: []
    	},
    	yAxis: [
            {// 0 ~ 100
                title: {
                    text: '主维度'
                },
                opposite: true,//左右浮动
                labels: {
                    formatter: function() {
                        return this.value
                    }
                }
            },
            {// 100~10000
                title: {
                    text: '超数维度'
                },
                plotLines: [{
                    value: 1,
                    width: 3
                }],
                labels: {
                    formatter: function() {
                       return this.value + "个";
                    }
                },
                opposite: true  //是否左右浮动的
            },
            {// yAxis:0  1000->1K 1000,0000->1M  1,000 ~ 1,0000,0000
                title: {
                    text: '大数维度'
                },
                plotLines: [{
                    value: 0,
                    width: 2
                }],
                labels: {
                    formatter: function() {
                        if( this.value >= 0 && this.value <= 10000 ){
                            return this.value;
                        }else if ( this.value <= 100000 && this.value > 10000 ) {
                            return this.value/1000 + "K";
                        }else{
                            return this.value/1000000 +'M';
                        }
                    }
                }
            },
            {// yAxis:1 %0 ~ 100%
                title: {
                    text: '小数维度'
                },
                plotLines: [{
                    value: 1,
                    width: 3
                }],
                labels: {
                    formatter: function() {
                       return this.value + "%";
                    }
                }
            }
        ],
        legend: {
            verticalAlign: 'bottom'
        },
        plotOptions: {
            line: {
                enableMouseTracking: true
            },
            spline: {
                marker: {
                    radius: 4,
                }
            },
            series: {
                events: {
                //控制图标的图例legend不允许切换
                    legendItemClick: function (event) {  
                        var clickLegend = $(this);//clickLegend[0]._i获取到当前的位置
                        var activeItem = getTabsActive();

                        if ( activeItem==="area" ) {
                            if ( !clickLegend[0].visible ) {
                                regionLegendArr.push( clickLegend[0]._i );
                            }else{
                                for (var i = 0; i < regionLegendArr.length; i++) {
                                    if (regionLegendArr[i] == clickLegend[0]._i) {
                                        regionLegendArr.splice(i, 1);
                                        break;
                                    };
                                };
                            }
                        }else if(activeItem==="realityCity"){
                            if ( !clickLegend[0].visible ) {
                                cityLegendArr.push( clickLegend[0]._i );
                            }else{
                                for (var i = 0; i < cityLegendArr.length; i++) {
                                    if (cityLegendArr[i] == clickLegend[0]._i) {
                                        cityLegendArr.splice(i, 1);
                                        break;
                                    };
                                };
                            }
                        }else if(activeItem==="map"){
                            if ( !clickLegend[0].visible ) {
                                orgLegendArr.push( clickLegend[0]._i );
                            }else{
                                for (var i = 0; i < orgLegendArr.length; i++) {
                                    if (orgLegendArr[i] == clickLegend[0]._i) {
                                        orgLegendArr.splice(i, 1);
                                        break;
                                    };
                                };
                            }
                        }
                        return true; //return  true 则表示允许切换
                    }
                }
            }
                        
        },
        credits:{ enabled:false },
        tooltip: {
            crosshairs: {
                width: 1,
                color: '#ff6900',
                dashStyle: 'line'
            },
            shared: true
        },
  		series: []
    };

    var arr = {},locNode;
    var areaCharts = function(arr, faterNode){
    	if(!arr['level']){
    		areaOption.title.text='区域数据总览';
    	}else{
    		switch (arr['level']){
	    		case 'region':{
	    			areaOption.title.text='区域数据总览';
	    			break;
	    			}
	    		case 'city':{
	    			areaOption.title.text='城市数据总览';
	    			break;
	    		}
	    		case 'org':{
	    			areaOption.title.text='站点数据总览';
	    			break;
	    		}	
	    		
    		}
    	}
    	
        var needDay = $(".reportrange span").html();

        if ( !arr["start"] && !arr["end"] ) {
        //window.NeedDay是一个超全局变量，从dateInput过继过来使用
        // 有最近一周，最近一月，最近三天，昨天
            arr["start"] = needDay.substr(0, 8);
            arr["end"] = needDay.substr(-8, 8);
        }
        //重绘图表时，伴随着分页信息的重绘，所以，页面重新置为-1,取全部数据
        arr["toPage"] = "-1";

        if ( !arr["type"] && !arr["value"] ) {
            arr["type"] = "region";
        };
       //delete by panql 默认查全部
        if ( arr["start"].substr(4,2) == arr["end"].substr(4,2) ) {
            // 同一个月的
            areaOption.xAxis.categories.length = 0;//

            for (var i = parseInt(arr["start"]); i <= parseInt(arr["end"]); i++) {
                i = i + "";
                var temp = i.substr(0, 4);
                temp += "-"+i.substr(4, 2);
                temp += "-"+i.substr(-2, 2);

                areaOption.xAxis.categories.push(temp);
            };

        }else{//不同月份的
            // 重新写的
            areaOption.xAxis.categories.length = 0;
            var start_m = parseInt( arr["start"].substr(4, 2) ),
                end_m   = parseInt( arr["end"].substr(4, 2) ),//开始&结束 月份
                start_d = parseInt( arr["start"].substr(-2, 2) ),
                end_d   = parseInt( arr["end"].substr(-2, 2) ),//开始&结束 日
                start_y = parseInt( arr["start"].substr(0, 4) ),
                end_y   = parseInt( arr["end"].substr(0, 4) ),//开始&结束 年
                oneDay  = 1000 * 60 * 60 * 24;//一天的毫秒

            if ( start_y == end_y ) {//年份相同的

                var iDate = new Date(start_y, start_m-1, 1),// 当前月
                    iNext = new Date(start_y, start_m, 1);// 下月
                var days = (iNext.getTime() - iDate.getTime())/oneDay;//得到这个月天数
                for (var j = start_d; j <= days; j++) {
                    areaOption.xAxis.categories.push( start_y+"-"+start_m+"-"+j );
                };

                for (var i = start_m+1; i < end_m; i++) {
                    var locDate = new Date(start_y, i-1, 1),// 当前月
                        locNext = new Date(start_y, i, 1);// 下月
                    var days = (locNext.getTime() - locDate.getTime())/oneDay;//得到这个月天数
                    for (var k = 1; k <= days; k++) {
                        areaOption.xAxis.categories.push( start_y+"-"+i+"-"+k );
                    };
                };

                for (var j = 1; j <= end_d; j++) {
                    areaOption.xAxis.categories.push( start_y+"-"+end_m+"-"+j );
                };
            }else{//跨年的
                // 开始的那个月
                var iDate = new Date(start_y, start_m-1, 1),// 当年当月
                    iNext = new Date(start_y, start_m, 1);// 下月
                var days = (iNext.getTime() - iDate.getTime())/oneDay;//得到这个月天数
                for (var j = start_d; j <= days; j++) {
                    areaOption.xAxis.categories.push( start_y+"-"+start_m+"-"+j );
                };

                for (var i = start_m+1; i <= 12; i++) {// 上一年
                    var locDate = new Date(start_y, i-1, 1),// 当前月
                        locNext = new Date(start_y, i, 1);// 下月
                    var days = (locNext.getTime() - locDate.getTime())/oneDay;//得到这个月天数
                    for (var k = 1; k <= days; k++) {
                        areaOption.xAxis.categories.push( start_y+"-"+i+"-"+k );
                    };
                };
                for (var i = 1; i < end_m; i++) {// 下一年
                    var locDate = new Date(start_y, i-1, 1),// 当前月
                        locNext = new Date(start_y, i, 1);// 下月
                    var days = (locNext.getTime() - locDate.getTime())/oneDay;//得到这个月天数
                    for (var k = 1; k <= days; k++) {
                        areaOption.xAxis.categories.push( end_y+"-"+i+"-"+k );
                    };
                };

                // 结束的 那年那个月
                for (var j = 1; j <= end_d; j++) {
                    areaOption.xAxis.categories.push( end_y+"-"+end_m+"-"+j );
                };
            }

        }

        arr["org"] = '';
        arr["typename"] = '';
        arr["city"] = '';
        arr["region"] = '';
        $(faterNode).find('.j-btn-success').data('typename','').data('org','').data('region','').data('city','');
        getJsonAndChart(arr, faterNode);
    }

    var fnRenderChartList = function(ParentNode,chartArr) {
        var listTpl = '<ul class="list-group">';
        for (var i = 0, j = chartArr.length; i < j; i++) {
            listTpl += '<li class="list-group-item clearfix inbox-item"><label class="label-checkbox inline">';
            listTpl += '<input type="checkbox" class="chk-item" data-txt="'+ chartArr[i].name +'">';
            listTpl += '<span class="custom-checkbox"></span></label><span class="from">'+ chartArr[i].name +'</span></li>';                     
        }
            listTpl += '</ul>';
        ParentNode.find('.chart-list').html(listTpl);
        var id = ParentNode.attr('id');
        var chartList; 
        if (id == 'area') {
            chartList = (localStorage['area_chartArr'])? (localStorage['area_chartArr']).split(',') : '';
        } else if ( id == 'realityCity') {
            chartList = (localStorage['real_chartArr'])? (localStorage['real_chartArr']).split(',') : '';
        } else if ( id == 'map') {
            chartList = (localStorage['map_chartArr'])? (localStorage['map_chartArr']).split(',') : '';
        }
        var jQchartList = ParentNode.find('.chart-list');
        if (!!chartList) {
            for (var x = 0, y = chartList.length; x < y; x++) {
            jQchartList.find('span:contains("'+ chartList[x] +'")').siblings('label').children('input').prop('checked', 'checked').addClass('selected');
            }
        } else {
            jQchartList.find('input.chk-item').addClass('selected').prop('checked','checked');
        }
    };

    var getJsonAndChart = function(urlArr, faterNode, noTable){
        if ( !faterNode ) {
            faterNode = $("#area");
        }

        
        var dim ='';

        if ( urlArr["level"] ) {
            urlArr["level"] = urlArr["level"];
        }else{
            urlArr["level"] = "region";
        }
        
        if ( !!urlArr["value1"] && !!urlArr["value"]) {// value 和 value1都存在,物理城市页面
            dim += 'type:' + urlArr["type"] + ",";
            dim += "value:" + urlArr["value"] + ";";

            dim += 'type:' + urlArr["type1"] + ",";
            dim += "value:" + urlArr["value1"];
        }else if ( !!urlArr["value"] && !urlArr["value1"]){// 
            dim += 'type:' + urlArr["type"] + ",";
            dim += "value:" + urlArr["value"];
        }else if( !!urlArr["value1"] && !urlArr["value"]){
            dim += 'type:' + urlArr["type1"] + ",";
            dim += "value:" + urlArr["value1"];
        }
        //edit by panql 判断有木有

        $.ajax({// 折线图渲染
            dataType: "json",
            url: "/data/monitor/querygroupdata",
            type: "POST",
            data: {
                start: ""+urlArr["start"], 
                end: ""+urlArr["end"],
                dim: dim,
                level: urlArr["level"],
                typename: urlArr["typename"]? urlArr["typename"] : '',
                region: urlArr["region"]? urlArr["region"] : '',
                city: urlArr["city"]? urlArr["city"] : '',
                org: urlArr["org"]? urlArr["org"] : ''
            },
            traditional:true,
            beforeSend: function(xhr){
                ajaxbg.addClass("show");
            },
            success: function(json){
                var json = json,
                    stat = json.stat,
                    data = json.result;
                if ( stat === "ok" ) {
                    
                    if ( data.length==0 ) {
                        alert("当前日期没有数据，请重新选择！");
                        areaOption.xAxis.categories = [];
                        areaOption.series = [];
                        faterNode.find('.chartsRend').highcharts(areaOption);
                    }else{
                        areaOption.series = []; //每次重新渲染的时候，先清空再写入
                        fnRenderChartList(faterNode, data);
                        var hasDefaultFlag = false;
                        var customLevel = faterNode.find('.j-custom-btn').data('level'); 
                        var chartArr = localStorage[customLevel + "_chartArr"] ? localStorage[customLevel + "_chartArr"].split(",") : '';
                        
                        for (var i = 0, k = data.length; i<k;i++) {// 遍历返回的json数据 对象
                            if (!!chartArr) {
                                if ($.isEmptyObject(data[i])) {
                                break;
                                }
                                if (!chartArr.some(function(value) {
                                    return value == data[i].name;
                                })) {
                                    data.splice(i, 1);
                                    i--;
                                    continue;
                                }
                            }
                            for( var j in data[i].data ){// 遍历返回的json数据的 数组

                                var temp = parseFloat(data[i].data[j]);

                                if( !temp ){
                                    temp = 0;
                                }else if( data[i].data[j].indexOf("%") > 0 ) {// 判断百分号
                                    data[i].yAxis = 3;
                                }else if( temp > 0 && temp <= 1000 ){// 3 0~100
                                    data[i].yAxis = 0;
                                }else if( temp > 1000 && temp <= 10000 ){//2 100~1000
                                    data[i].yAxis = 1;
                                }else{// 0 > 100000
                                    data[i].yAxis = 2;
                                }
                                
                                var activeItem = getTabsActive();
                                if ( activeItem==="area" ) {
                                    for (var legend = 0; legend < regionLegendArr.length; legend++) {
                                        var tempInt = regionLegendArr[legend];
                                        data[tempInt].selected = true;
                                        data[tempInt].visible = true;
                                    };
                                }else if(activeItem==="realityCity"){
                                    for (var legend = 0; legend < cityLegendArr.length; legend++) {
                                        var tempInt = cityLegendArr[legend];
                                        data[tempInt].selected = true;
                                        data[tempInt].visible = true;
                                    };
                                }else if(activeItem==="map"){
                                    for (var legend = 0; legend < orgLegendArr.length; legend++) {
                                        var tempInt = orgLegendArr[legend];
                                        data[tempInt].selected = true;
                                        data[tempInt].visible = true;
                                    };
                                }
                                                        
                                if ( data[i].name == "完成订单数" || data[i].name == "上岗骑手数") {
                                    data[i].selected = true;
                                    hasDefaultFlag = true;
                                }else{
                                    data[i].visible = false;
                                }

                                data[i].data.splice(j, 1, temp);
                            }
                            areaOption.series.splice(i, 1, data[i]);
                        }; 
                    }
                    if (!hasDefaultFlag && !!areaOption.series[0]) {
                        areaOption.series[0].selected = true;
                        areaOption.series[0].visible = true;
                    }
                    faterNode.find('.chartsRend').highcharts(areaOption);
                    if (!noTable) {
                        fnGetTableBody(urlArr, dim, faterNode);
                    }
                }else{
                    alert("error");
                }
            },
            complete:function(xhr){
                ajaxbg.removeClass('show');
            },
            error: function(error) {
                alert("网络连接失败！");
            }
        });// 折线图渲染
    }

    var fnRenderMenuList = function(ParentNode,headerArr) {
        var listTpl = '<ul class="list-group">';
        for (var i = 0, j = headerArr.length; i < j; i++) {
            var headerObj = headerArr[i].caption;
            if (basicInfo.some(function(info) {
                return info === headerObj;
            })) {
                continue;
            }
            listTpl += '<li class="list-group-item clearfix inbox-item"><label class="label-checkbox inline">';
            listTpl += '<input type="checkbox" class="chk-item" data-txt="'+ headerObj +'">';
            listTpl += '<span class="custom-checkbox"></span></label><span class="from">'+ headerObj +'</span></li>';                     
        }
            listTpl += '</ul>';
        ParentNode.find('.menu-list').html(listTpl);
        var id = ParentNode.attr('id');
        var menuList; 
        if (id == 'area') {
            menuList = (localStorage['area_menuArr'])? (localStorage['area_menuArr']).split(',') : '';
        } else if ( id == 'realityCity') {
            menuList = (localStorage['real_menuArr'])? (localStorage['real_menuArr']).split(',') : '';
        } else if ( id == 'map') {
            menuList = (localStorage['map_menuArr'])? (localStorage['map_menuArr']).split(',') : '';
        }
        var jQmenuList = ParentNode.find('.menu-list');
        if (!!menuList) {
            for (var x = 0, y = menuList.length; x < y; x++) {
                jQmenuList.find('span:contains("'+ menuList[x] +'")').siblings('label').children('input').prop('checked', 'checked').addClass('selected');
            }
        } else {
            jQmenuList.find('input.chk-item').addClass('selected').prop('checked','checked');
        }
        
    };
    var fnGetTableBody = function(urlArr, urlDim, faterNode){// 渲染表格
        $.ajax({// 表头...要从表头获取id
            url: '/data/monitor/queryheader',
            type: 'POST',
            dataType: 'json',
            data: { level: urlArr["level"] },
            beforeSend: function(xhr){
                ajaxbg.addClass("show");
            },
            success: function(data) {
                var temp = "", temp = [];
                temp = urlArr;
                temp["dim"] = urlDim;
                if ( data.stat === "ok" ) {
                    fnRenderMenuList(faterNode, data.result);
                    rendTable(urlArr, data.result, faterNode);//获取到数据直接发送给该函数处理
                }else{
                    alert("error");
                }
            },
            error: function(){
                alert("网络连接失败！")
            }
        });// 表头...要从表头获取id
    }

    var rendTable = function(urlArr, headRes, faterNode) {//渲染表格
        $.ajax({ // 表体
            url: '/data/monitor/querydata',
            type: 'POST',
            dataType: 'json',
            data: { 
                start: ""+urlArr["start"], 
                end: ""+urlArr["end"],
                dim: urlArr["dim"],
                level: urlArr["level"],
                toPage: -1,
                pagesize: 15
            },
            beforeSend: function(xhr){
                ajaxbg.addClass("show");
            },
            success: function(data) {
                var temp = "";
                if ( data.stat === "ok" ) {
                    var bodyRes = data.result;
                    if (  bodyRes.totalcount <= 0 ) {
                        temp += "数据暂时没有！";
                    }else{
                        temp += "<table class=\"table table-waimai display\" cellspacing=\"0\"><thead ><tr>";
                        for ( var y in headRes) {// 头部
                            temp += "<th>"+headRes[y].caption+"</th>";
                        };
                        var tbodyStr = "</tr></thead>"
                        // temp += "</tr>";
                        for (var i = 0,len = bodyRes.data.length; i < len; i++) {//20 tr
                            // tbodyStr += "<tr class='table-body'>";
                            var tdStr='';
                            var isTotalTr = false;
                            var bodyDataTemp = bodyRes.data[i];
                            for ( var x in headRes) {// 8 td
                                for ( var k in bodyDataTemp ) {

                                    if ( k == headRes[x].id ) {
                                    // 比较数据id是否相等，一个对象返回只有一个与之对应的
                                        tdStr += "<td title=\""+ bodyDataTemp[k] +"\">"+countTxt(bodyDataTemp[k], 16)+"</td>";
                                        if(bodyDataTemp[k]=='合计'){
                                          isTotalTr = true;  
                                        }
                                        break;
                                    }
                                    // console.log( k );//indicator_1822393291:key
                                    // console.log( temp[k] );//value
                                }
                            };
                            if( isTotalTr ){
                                //style setting
                                tbodyStr += "<tr  class=\"selected\" '>";
                                tbodyStr+=tdStr;
                            }else{
                                tbodyStr += "<tr>";
                                tbodyStr += tdStr;
                            }
                            tbodyStr += "</tr>";
                        };
                        temp += tbodyStr ;
                        temp += "</table>";
                    }
                    faterNode.find('.charts-table').html(temp);
                    faterNode.find('.charts-table table').dataTable({
                        "scrollX": true,
                        "scrollY": 550,
                        "dom": "frtiS",
                        "deferRender": true,
                        "oLanguage": { 
                            "sSearch" : "搜索"
                        },
                        "aaSorting":[]
                    });
                    var customLevel = faterNode.find('.j-custom-btn').data('level'); 
                    var chartArr = localStorage[customLevel + "_menuArr"] ? localStorage[customLevel + "_menuArr"].split(",") : '';
                    if (!!chartArr) {
                        var indexArr = [];
                        for (var x in headRes) {
                            indexArr.push(x);
                            for (var y in chartArr) {
                                if (headRes[x].caption === chartArr[y] || ($.inArray(headRes[x].caption, basicInfo)) >= 0 ) {
                                    indexArr.pop();
                                    break;
                                }
                            }
                        }
                        faterNode.find('.charts-table table').DataTable().columns(indexArr).visible(false, false);
                        faterNode.find('.charts-table table').DataTable().columns.adjust().draw(false);
                    }
                }else{
                    alert("error");
                }
            },
            complete:function(xhr){
                ajaxbg.removeClass("show");
            },
            error: function(){
                alert("网络连接失败！")
            }
        });
    }//渲染表格结束
    function btnSearchBg() {
        var thisNode = $(this);

        siblingsLocNode = thisNode.parents(".tabs-pane-item");

        getQueryParams(siblingsLocNode,arr);
        areaCharts(arr, siblingsLocNode);
    }

    $(".btn-search-bg").on('click', btnSearchBg);

    $("#areaStatus").on("change", function(){
        arr["type"] = "region";//区域 外卖区域 编号为1
        arr["value"] = this.value;
    });
    $("#peisongStatus").on("change", function(){
        arr["type"] = "type";//物理城市的配送方类型 编号为2
        arr["value"] = this.value;
    });
    $("#realityCityStatus").on("change", function(){
        arr["type1"] = "city";//物理城市的物理城市 编号为3
        arr["value1"] = this.value;
    });
    $("#mapStatus").on("change", function(){
        arr["type"] = "type";//站点的配送方式按 编号为4
        arr["value"] = this.value;
    });


    $(".tabs-menu a").on("click", function(ev) {// tab切换
        var thisNode = $(this),
            href     = thisNode.attr("href"),//找到链接
            ev       = ev || window.event,
            tabsPane = $(href);
        thisNode = thisNode.parent(".menu-item");
        arr["level"] = thisNode.children('a').attr("level");
        getQueryParams(tabsPane,arr);

        if ( ev.preventDefault() ) {// 阻止默认事件
            ev.preventDefault();
        }else{
            ev.returnValue = false;
        }
        if ( thisNode.attr("class").indexOf("active-tab") <= 0 ) {
            thisNode.siblings().removeClass("active-tab");
            tabsPane.siblings().removeClass("active-item");

            thisNode.addClass("active-tab");//上下一起切换
            tabsPane.addClass("active-item");
            //edit by panql 默认值处理
            if( href === "#area" ){
                // arr["type1"] = null; arr["value1"] = null;
                // TODO 以下几个都被我修改了 因为现在导出之后会使图变形 不知道以后是否要修改 所以留一个注释的
                // if ( thisNode.children('a').attr("data-chooced")==="none" ) {
                getJsonAndChart(arr, tabsPane, true);
                    // thisNode.children('a').attr("data-chooced", "chooced");
                // }else{
                //     return;
                // }

                if(statSelsect("areaStatus", "dim_bm_region_chain")){
                    getJsonAndChart(arr, tabsPane, true);
                    return;
                }// 区域
                arr["type"] = "region"; arr["value"] = "";
                areaCharts(arr, tabsPane);
            }else if( href === "#realityCity" ){
                // arr["type"] = "type"; arr["value"] = null;
                // arr["type1"] = "city"; 

                // 物理城市
                if(statSelsect("realityCityStatus", "dim_bm_city_chain")){
                    getJsonAndChart(arr, tabsPane, true);
                    return;
                }else{
                    getJsonAndChart(arr, tabsPane, true);
                }
                areaCharts(arr, tabsPane);
            }else if( href==="#map" ){
                arr["type"] = "type";arr["value"] = null;
                // arr["type1"] = null; arr["value1"] = null;
               // statSelsect("realityCityStatus", "dim_bm_city_chain");// 物理城市


               if(statMap["#orgsite"]){
                getJsonAndChart(arr, tabsPane, true);
                return;
               }else{
                   getJsonAndChart(arr, tabsPane, true);
               }
                areaCharts(arr, tabsPane);
               statMap["#orgsite"] =true;
            }
        }
        
    });

    $(".pagination").on('click', 'li', function(event) {
        var thisNode = $(this),liTemp = "",
            locValue = parseInt(arr["toPage"]),//当前选择的页码
            liSiblings = thisNode.siblings('li');
            liLength = liSiblings.length,
            maxValue = liSiblings[liLength-2].innerHTML;
            locNode = thisNode.parent().parent().parent(".tabs-pane-item");


        thisNode.siblings('li').removeClass('active');
        thisNode.addClass('active');

        liTemp = thisNode.html();
        if ( liTemp=="..." ) {
            alert("对不起，您好像点错了，请重新选择！");
            locValue = locValue;
        }else if( liTemp=="»"){//«   »
            if ( locValue <= maxValue) {
                locValue = locValue + 1;
            }else{
                alert("亲，没有更多的了！");
            }
        }else if( liTemp=="«" ){
            if ( locValue > 1) {
                locValue = locValue - 1;
            }else{
                alert("亲，没有上一页了！");
            }
        }else{
            locValue = thisNode.html();
        }

        arr["toPage"] = locValue;
        var needDay = $(".reportrange span").html();

        if ( !arr["start"] && !arr["end"] ) {
        //window.NeedDay是一个超全局变量，从dateInput过继过来使用
        // 有最近一周，最近一月，最近三天，昨天
            arr["start"] = needDay.substr(0, 8);
            arr["end"] = needDay.substr(-8, 8);
            arr["toPage"] = "1";
        }
        //分页时获取条件 分页按钮点击时，没有必要重新绘制折线图，因为条件并没有变化
        getQueryParams(locNode,arr);
        var dim ='';
        
        if ( arr["value"] ) {
            dim += 'type:' + arr["type"] + ",";
            dim += "value:" + arr["value"] + ";";
        };
        if ( arr["value1"] ) {
            dim += ';type:' + arr["type1"] + ",";
            dim += "value:" + arr["value1"] + ";";
        };
        fnGetTableBody(arr, dim, locNode);
    });// 分页点击结束
    /**
     * 
     */
    var getQueryParams = function(locNode,arr){
    	arr["type"]=null;
    	arr["value"]=null;
    	arr["value1"]=null;
    	arr["value1"] = null;
        //find 的返回值可能是一个jquery对象，并不能保证element一定存在
    	var area = locNode.find('#areaStatus');
    	//如果存在外卖区域
    	if( area.val() &&area.length>0 ){
            arr["value1"] = null;
    		arr["type"]="region";
            arr["value"]=area.val();
    	}
    	var kind = locNode.find("#peisongStatus");
        // 物理城市的配送类型
        if( kind.val() && kind.length>0 ){
            arr["value1"] = null;
            arr["type"]="type";
            arr["value"]=kind.val();
        }

        kind = locNode.find("#mapStatus");
        if( kind.val() &&kind.length>0 ){
            arr["value1"] = null;
            arr["type"]="type";
            arr["value"]=kind.val();
        }

        var city=locNode.find("#realityCityStatus");
        if( city.val() && city.length>0 ){
            arr["type1"] = "city";
            arr["value1"] = city.val();
        }

        if ( arr["level"] ) {
            arr["level"] = arr["level"];
        }else{
            arr["level"] = "region";
        }

        var date = locNode.find('.reportrange span');
        date = date.html();
        arr["start"] = date.substr(0, 8);
        arr["end"]  =date.substr(-8, 8);

    };
    /**
     * 当前表搜索
     */
    var searchCurrentPage=function(searchKey,searchNode){
        if (!searchNode) {
            return;
        };
        var tableNode=searchNode.parent().parent(".tabs-pane-item").find(".table-waimai");
        if(!tableNode){
            return;
        }
        //
        var trs=tableNode.children().children();
        if (trs.length<=1) {
            return;
        };
        //略过表头
        for (var i = 1; i < trs.length; i++) {
            var trObj=$(trs[i]);
            var  finded=false;
            if(searchKey&&searchKey!=''){
                var tds=trObj.children();
                for (var j = 0; j < tds.length; j++) {
                    if($(tds[j]).text().indexOf(searchKey)!=-1){
                        finded=true; 
                        break;
                   }
                };
            }else{
                finded=true;
            }
            trObj.css('display',finded?'':'none');
        };

    };

    //单表搜索 修正监听事件
    $(".tabs-pane").on('click', '.j-btn-daochu', function(event) {// 数据导出
        thisNode = $(this);
        var href = "/data/monitor/export?";
        var root = thisNode.parent().parent();
        var level = thisNode.attr("level");

        if ( level ) {
            level = thisNode.attr("level");
        }else{
            level = "region";
        }
        arr["level"] = level;
        getQueryParams(root, arr);
        href += "start="+arr["start"]+"&end="+arr["end"]+"&dim=";
        if ( arr["value"] ) {
            href += "type:"+arr['type']+",value:"+arr['value'];
        };

        if ( arr["value1"] ) {
            href += ";type:"+arr['type1']+",value:"+arr['value1'];
        };

        href += "&level="+arr["level"];
        thisNode.attr({
            "href": href,
            "target": '_blank'
        });
    });
    $(".tabs-pane").on('click', '.j-btn-success', function(event) {// 数据导出 按日期
        var thisNode = $(this);
        var root = thisNode.parent().parent();//从点击节点获取父节点
        var level = thisNode.attr("level");
        var href = "/data/monitor/exportgroupdata?";

        if ( level ) {
            level = thisNode.attr("level");
        }else{
            level = "region";
        }
        arr["level"] = level;
        getQueryParams(root, arr);
        href += "start="+arr["start"]+"&end="+arr["end"]+"&dim=";
        
        if ( arr["value"] ) {
            href += "type:"+arr['type']+",value:"+arr['value'];
        };

        if ( arr["value1"] ) {
            href += ";type:"+arr['type1']+",value:"+arr['value1'];
        };
        
        href += "&level="+level;
        href += "&typename="+ (thisNode.data("typename")? thisNode.data("typename"):'') +"&region="+ (thisNode.data("region")?thisNode.data("region") : '') +"&city="+ (thisNode.data("city")?thisNode.data("city"):'') +"&org="+ (thisNode.data("org")?thisNode.data("org"):'') ;
        thisNode.attr({
            "href": href,
            "target": '_blank'
        });
    });
    $(".tabs-pane").on('click', 'tbody tr', function() {
        var jQself = $(this),
            jQparents = jQself.parents('.tabs-pane-item'),
            pId = jQparents.attr('id');
        jQself.addClass('selected').siblings('.selected').removeClass('selected');
        if (pId == 'area') {
            arr["typename"] = jQself.children().eq(0).attr('title');
            arr["region"] = jQself.children().eq(1).attr('title');
        } else if (pId == 'realityCity') {
            arr["typename"] = jQself.children().eq(0).attr('title');
            arr["city"] = jQself.children().eq(1).attr('title');
            arr["region"] = jQself.children().eq(2).attr('title');
        } else if (pId == 'map') {
            arr["org"] = jQself.children().eq(0).attr('title');
            arr["typename"] = jQself.children().eq(1).attr('title');
            arr["city"] = jQself.children().eq(2).attr('title');
            arr["region"] = jQself.children().eq(3).attr('title');
        }
        jQparents.find('.j-btn-success')
            .data('typename',arr["typename"]? arr["typename"] : '')
            .data('region',arr["region"]? arr["region"] : '')
            .data('city',arr["city"]? arr["city"] : '')
            .data('org',arr["org"]? arr["org"] : '');
        getJsonAndChart(arr, jQparents, true);
    });
    $(".tabs-pane").on('click', '.chk-item', function() {
        var jQself = $(this);
        $(this).toggleClass('selected');
    });

    $(".tabs-pane").on('click', '.j-reset-list', function() {
        var jQself = $(this),
            jQparents = jQself.parents('.modal-content'),
            jQpane = jQparents.find('.tab-pane.in'),
            jQselected = jQpane.find('input.selected');
        if (jQselected.size() > 0) {
            jQpane.find('input.chk-item').prop('checked','').removeClass('selected');
        }   else {
            jQpane.find('input.chk-item').prop('checked','checked').addClass('selected');
        }
    });

    $(".tabs-pane").on('click', '.j-custom-btn', function() {
        var jQself = $(this),
            jQmodalCon = jQself.parents('.modal-content'),
            jQmenuInput = jQmodalCon.find('.menu-list input.selected'),
            jQchartInput = jQmodalCon.find('.chart-list input.selected'),
            menuArr = [],
            chartArr = [],
            menuLen = jQmenuInput.length,
            chartLen = jQchartInput.length,
            level = jQself.data('level');
        if (!chartLen) {
            alert('请在图标项中至少选择一个指标.');
            return;
        }
        if (!menuLen) {
            alert('请在列表项中至少选择一个指标.');
            return;
        }
        for (var i = 0, j = menuLen; i < j; i++) {
            menuArr.push($(jQmenuInput[i]).data('txt'));
        }
        for (var m = 0, n = chartLen; m < n; m++) {
            chartArr.push($(jQchartInput[m]).data('txt'));
        }
        localStorage[level + "_menuArr"] = menuArr;
        localStorage[level + "_chartArr"] = chartArr;
        var seletedcolchart, seletedcoltable;
        if ($.cookie('seletedcolchart') && $.cookie('seletedcoltable')) {
            seletedcolchart = JSON.parse($.cookie('seletedcolchart'));
            seletedcoltable = JSON.parse($.cookie('seletedcoltable'));
        } else {
            seletedcolchart = {};
            seletedcoltable = {};
        }
        if (level == 'area') {
            seletedcolchart.region = chartArr;
            seletedcoltable.region = menuArr;
        } else if (level == 'real') {
            seletedcolchart.city = chartArr;
            seletedcoltable.city = menuArr;
        } else if (level == 'map') {
            seletedcolchart.org = chartArr;
            seletedcoltable.org = menuArr;
        }
        $.cookie('seletedcolchart', JSON.stringify(seletedcolchart), { expires: 360, path: '/' });
        $.cookie('seletedcoltable', JSON.stringify(seletedcoltable), { expires: 360, path: '/' });
        jQself.parents('.modal').modal('hide');
        btnSearchBg.call(this);  // 设置的时候重新渲染
    });

    function countTxt(str, len) {
      if (str) {
        var strLen = str.replace(/[\u4e00-\u9fa5\s]/g, '**').length, newStr = [], totalCount = 0;

        if (strLen <= len) {
          return str;
        } else {
          for (var i = 0; i < strLen; i++) {
            var nowValue = str.charAt(i);
            if (/[^\x00-\xff]/.test(nowValue)) {
              totalCount += 2;
            } else {
              totalCount += 1;
            }
            newStr.push(nowValue);
            if (totalCount >= len) {
              break;
            }
          }
            return newStr.join('') + '...';
        }
      } else {
        return '';
      }
    };

    areaCharts(arr, locNode);
})