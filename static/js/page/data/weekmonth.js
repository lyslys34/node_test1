$(document).ready(function(){
    var ajaxbg = $("#background,#progressBar");
    ajaxbg.hide();

	// tab标签的切换
	$("#tabsNav li").on('click', function() {//tab切换
		var _this = $(this),
			url   = _this.attr("data-href"),//通过这个来获取对应id
			chooced = _this.attr("data-chooced");
		// 后面可以通过这个ID来判断在哪个tab

		var tabsPane = "#" + url;//当前对应的tab标签

		_this.siblings('li').removeClass('active');
		_this.addClass('active');
		$(tabsPane).siblings('.tabs-pane-item').removeClass('active');
		$(tabsPane).addClass('active');

		// 判断当前的输入框里面有木有选择的毫秒数(存入毫秒数是为了方便获取时间和显示最近月&周)
		// 如没有，则将现在的毫秒数存到输入框的data-date  
		// 默认的日期变化
		var currTab     = fnJudgeTab(),
			weekOrMonth = $(currTab).children('.chooce-days').children('.btn-active').attr("data-chooce"),
			inputBoolen = $(currTab).children('.chooce-days').children('input').attr("data-date-"+weekOrMonth);

		if ( !inputBoolen ) {
			var myDate  = new Date();//选择的那天
			var month = myDate.getMonth(),//不减1是因为默认就是－1
	  			years = myDate.getFullYear();
	  		var monthDate = new Date(years, month-1, 1);


		  	// 把毫秒数都放到输入框里面 之后直接显示出来即可
		  	$(currTab).children('.chooce-days').children('input').attr("data-date-week", myDate.getTime() );
		  	$(currTab).children('.chooce-days').children('input').attr("data-date-month", monthDate.getTime() );

		  	fnShowInput();
		};
		fnClickDateChooce();

		// if ( chooced==="none" ) {//是否点击过这个nav li
		// TODO 不用判断是否点击过了啊
			// _this.attr("data-chooced","chooced");

			if ( currTab == "#view" ) {// 是否是视图tab标签 如果是自动加载表格
				fnGetTableData();
			}else{// 是否是直营tab标签 如果是自动加载图表
				fnShowSelect();

				fnGetChartsData(2);//左边的图标
				fnGetChartsData(3);//右边的图表
			}
		// }
	});

	// 周和月的button切换
	$(".chooce-days .btn").on('click', function() {// 周－月 点击效果
		var _this   = $(this),
			forNode = _this.parent(); // 返回给所谓的父级元素

		if ( _this.attr("data-chooce")=="week" ) {
			_this.siblings('button').removeClass('btn-active');
			_this.addClass('btn-active');
		}else{
			_this.siblings('button').removeClass('btn-active');
			_this.addClass('btn-active');
		}

		fnShowInput();// 输入框的显示
		fnClickDateChooce();


		var currTab = fnJudgeTab();
		if ( currTab === "#view" ) {//获取之后会自动渲染表格
			fnGetTableData();
		}
		// 直营tab标签 且每点击过才会触发请求获取列表
		if ( currTab === "#direct" || currTab === "#join" ) {
			fnShowSelect();

			fnGetChartsData(2);
			fnGetChartsData(3);
		}
	});
	// 日期组件点击 之后选择日期之后的
	$('.click-chooce-date').daterangepicker({//日期组件的
	    opens: "left",
	    singleDatePicker: true,
	    showDropdowns: true,
	    timePickerIncrement: 1,
	    startDate: moment().subtract(0, 'days').format('MM/DD/YYYY'),
	    endDate: moment().subtract(0, 'days').format('MM/DD/YYYY'),
	    minDate: '01/01/2012',
	    maxDate: moment().subtract(0, 'days').format('MM/DD/YYYY'),
	    locale: {
	        daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
	        monthNames: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
	    }
	}, function(start, end, label) {
		var dateStr = start.format("MM/DD/YYYY");// 选择的那天(这是调用的接口的今天)
		var years = parseInt( dateStr.substr(-4, 4) ),
			month = parseInt( dateStr.substr(0, 2) ),
			day   = parseInt( dateStr.substr(3, 2) );

	  	var myDate = new Date(years,month-1,day);
		var currTab = fnJudgeTab(),
		 weekOrMonth = $(currTab).children('.chooce-days').children('.btn-active').attr("data-chooce");

	  	if ( weekOrMonth == "week" ) {
	  		$(currTab).children('.chooce-days').children('input').attr("data-date-week", myDate.getTime() );
	  	}

	  	fnShowInput();//点击之后触发的现实输入框的日期
		if ( currTab === "#view" ) {// 得到日期后自动加载
			fnGetTableData();
		}else{
			fnGetChartsData(2);
			fnGetChartsData(3);
		}
	});
	// 日期组件的一些选择判断 
	// 月的时候选择的是月的下拉菜单
	function fnClickDateChooce(){
		var currTab0 = fnJudgeTab(),
		weekOrMonth0 = $(currTab0).children('.chooce-days').children('.btn-active').attr("data-chooce"),
		    nowaDays = new Date(),
		    nowMonth = (nowaDays.getMonth()+1)>=10 ? (nowaDays.getMonth()+1) : "0" + (nowaDays.getMonth()+1),
		    nowYears = nowaDays.getFullYear(),
		    nowDay   = nowaDays.getDate() >= 10 ? nowaDays.getDate() : "0" + nowaDays.getDate();

		var startDay = $(currTab0).children('.chooce-days').children('input').attr("data-date-week");
		if ( weekOrMonth0 != "week" ) {
			startDay = $(currTab0).children('.chooce-days').children('input').attr("data-date-month");
		};

		if ( !!startDay ) {
			startDay = Number( startDay );
			var monthChooce = new Date(startDay);
			nowMonth = (monthChooce.getMonth()+1)>=10 ? (monthChooce.getMonth()+1) : "0" + (monthChooce.getMonth()+1);
			nowYears = monthChooce.getFullYear();
			nowDay   = monthChooce.getDate() >= 10 ? monthChooce.getDate() : "0" + monthChooce.getDate();
		}

		var	dayStr   = nowMonth + "/" + nowDay + "/" + nowYears;
		if ( currTab0=="#view" ) {
			$('.click-chooce-date:eq(0)').data('daterangepicker').setStartDate(dayStr);
			$('.click-chooce-date:eq(0)').data('daterangepicker').setEndDate(dayStr);
		}else if(currTab0=="#direct"){
			$('.click-chooce-date:eq(1)').data('daterangepicker').setStartDate(dayStr);
			$('.click-chooce-date:eq(1)').data('daterangepicker').setEndDate(dayStr);
		}else{
			$('.click-chooce-date:eq(2)').data('daterangepicker').setStartDate(dayStr);
			$('.click-chooce-date:eq(2)').data('daterangepicker').setEndDate(dayStr);
		}
			
	};
		
	// TODO 对日期组件的月份的修改
	// 是否是月 不是月的话回复原来的日期样子 否则显示月和日的修改而已
	$(document).on('click', ".click-chooce-date", function(event) {
		var currTab = fnJudgeTab();
		var weekOrMonth = $(currTab).children('.chooce-days').children('.btn-active').attr("data-chooce");
		if ( weekOrMonth === "month" ) {
			fnClickDateChooce();
			$(".table-condensed").css("width", "224px");
			$(".daterangepicker .right .table-condensed thead").children('tr:eq(1)').css("display", "none");
			$(".daterangepicker .right .table-condensed thead").children('tr:eq(3)').css("display", "none");
			$(".daterangepicker .right .table-condensed thead").children('tr:eq(5)').css("display", "none");
			$(".table-condensed tbody").hide();
		}else{
			fnClickDateChooce();
			$(".table-condensed thead").children('tr').show();
			$(".table-condensed tbody").show();
		}
	});


	// 当是月份的时候可以直接选择月
	$(document).on("change", ".monthselect", function() {
		var currTab = fnJudgeTab();
		var weekOrMonth = $(currTab).children('.chooce-days').children('.btn-active').attr("data-chooce");
		if ( weekOrMonth==="month" ) {
			var monthChangeYears = Number( $(this).siblings('.yearselect').val() ),
				monthChangeMonth = Number( $(this).val() );
			var monthChangeDate = new Date(monthChangeYears, monthChangeMonth, 1);

	  		$(currTab).children('.chooce-days').children('input').attr("data-date-month", monthChangeDate.getTime() );

	  		fnShowInput();
			$(".show-calendar").hide();

			if ( currTab === "#view" ) {// 得到日期后自动加载
				fnGetTableData();
			}else{
				fnGetChartsData(2);
				fnGetChartsData(3);
			}
		}
	});

	// 选择年份的时候 判断前面有没有事月 如果是月就不显示下面的日期那些
	$(document).on("change", ".yearselect", function() {
		var currTab = fnJudgeTab();
		var weekOrMonth = $(currTab).children('.chooce-days').children('.btn-active').attr("data-chooce");
		if ( weekOrMonth==="month" ) {
			$(".table-condensed").css("width", "224px");
			$(".daterangepicker .right .table-condensed thead").children('tr:eq(1)').hide();
			$(".daterangepicker .right .table-condensed thead").children('tr:eq(3)').hide();
			$(".daterangepicker .right .table-condensed thead").children('tr:eq(5)').hide();
			$(".table-condensed tbody").hide();
		};
	});
	// TODO 在月的情况下，对于点击上下月的时候，对日期组建的修改，不让他默认显示下面的东西
	$(document).on("click", ".available", function() {
		var currTab = fnJudgeTab();
		var weekOrMonth = $(currTab).children('.chooce-days').children('.btn-active').attr("data-chooce");
		if ( weekOrMonth==="month" ) {
			var monthChangeYears = Number( $(this).siblings("th").children('.yearselect').val() ),
				monthChangeMonth = Number( $(this).siblings("th").children('.monthselect').val() );

			// TODO 向前的 判断是否是上个月的
			if ( $(this).attr("class").indexOf("prev") >= 0 ) {
				monthChangeMonth = monthChangeMonth - 1;
			}else{
				monthChangeMonth = monthChangeMonth + 1;
			}
			var monthChangeDate = new Date(monthChangeYears, monthChangeMonth, 1);
	  		$(currTab).children('.chooce-days').children('input').attr("data-date-month", monthChangeDate.getTime() );

			// 将改变的显示在表单中
	  		fnShowInput();

			$(".table-condensed").css("width", "224px");
			$(".daterangepicker .right .table-condensed thead").children('tr:eq(1)').hide();
			$(".daterangepicker .right .table-condensed thead").children('tr:eq(3)').hide();
			$(".daterangepicker .right .table-condensed thead").children('tr:eq(5)').hide();
			$(".table-condensed tbody").hide();

			if ( currTab === "#view" ) {// 得到日期后自动加载
				fnGetTableData();
			}else{
				fnGetChartsData(2);
				fnGetChartsData(3);
			}
		};
	});

	// 点击 导出的
	$(document).on("click", ".btn-submit", function(event) {
		var currTab = fnJudgeTab();
		var msDate = $(currTab).children('.chooce-days').children('input').attr("data-start");

		var weekOrMonth = $(currTab).children('.chooce-days').children('.btn-active').attr("data-chooce");
	  	var sendUrl = "/data/monitor/weekmonth/";

	  	// 通过这几个参数来使页面重新请求那些数据 
	  	// TODO 找一下hightcharts里面的问题
	  	$("#tabsNav li").attr("data-chooced", "none");
	  	if ( currTab == "#view" ) {
	  		sendUrl += "exportmaindata?";
	  		if ( weekOrMonth == "month" ) {
		  		sendUrl += "date="+msDate+"&&timelevel=month";
		  	}else{
		  		sendUrl += "date="+msDate;
		  	}
		}else{
	  		sendUrl += "exportchartdata?";

			var input = $(currTab).children('.chooce-days').children('input'),
		    startDate = input.attr("data-start");

			// 下拉框的值由这个直接获取val
			var selectstr = ".chooce-select-" + weekOrMonth;
			var select    = $(currTab).find(selectstr);
	        if( select.val() && select.length > 0 ){
		  		sendUrl += "id="+select.val();
	        }
			
			sendUrl += "&timelevel=" + weekOrMonth;
			// 开始时间由当前的值给
			var dateStart = $(currTab).children('.chooce-days').children('input').attr("data-start");
		  	sendUrl += "&date=" + dateStart;
			
		  	if ( currTab=="#direct" ) {//在哪个tab 标签
				sendUrl += "&typeid=" + 101;
			};
			if ( currTab=="#join" ) {
				sendUrl += "&typeid=" + 201;
			};
		}
		$(this).attr("href", sendUrl);
	});

	$(".chooce-select").on('change', function() {
		fnGetChartsData(2);
		fnGetChartsData(3);
	});

	// 判断在哪个tab页 最终返回当前tab页的id 形式为 eg "#view" 其他用的时候只要$(res)即可
	function fnJudgeTab(){
		var liActive  = $("#tabsNav li").siblings(".active"), //判断一下在哪个tab页面
	  	 	activeTab = liActive.attr("data-href");//获取到对应的tab id值
	  	var idStr     = "#" + activeTab;
	  	return idStr;
	}

	// 下拉框的获取和显示
	function fnGetSelectList(){
		var currTab = fnJudgeTab();
		var  currId = $(currTab);
		weekOrMonth = currId.children('.chooce-days').children('.btn-active').attr("data-chooce");
		
		$.ajax({
			url: "/data/monitor/weekmonth/querylist",
			type: 'GET',
			dataType: 'json',
			data: {timelevel: "month"},
			success: function(result){
				if ( result.stat == "ok" ) {
					var resData    = result.result,
						selectHtml = "";
					for (var i = 0; i < resData.length; i++) {
						selectHtml += "<option value=" + resData[i].id + ">" +
											resData[i].caption +
									   "</option>";
					};

					$('.chooce-select-month').html(selectHtml);
				};
			},
			error: function(){
				console.log("get data for select error");
			}
		});

		$.ajax({
			url: "/data/monitor/weekmonth/querylist",
			type: 'GET',
			dataType: 'json',
			data: {timelevel: "week"},
			success: function(result){
				if ( result.stat == "ok" ) {
					var resData    = result.result,
						selectHtml = "";
					for (var i = 0; i < resData.length; i++) {
						selectHtml += "<option value=" + resData[i].id + ">" +
											resData[i].caption +
									   "</option>";
					};

					$('.chooce-select-week').html(selectHtml);
				};
			},
			error: function(){
				console.log("get data for select error");
			}
		});
	};

	// 显示那个下拉菜单
	function fnShowSelect(){
		var currTab = fnJudgeTab(),
		weekOrMonth = $(currTab).children('.chooce-days').children('.btn-active').attr("data-chooce");

		var activeNode = ".chooce-select-" + weekOrMonth;

		$(currTab).find(".chooce-select").css("display", "none");
		$(currTab).find(activeNode).css("display", "block");
	}

	// 日期在输入框里面的显示形式 直接从自身这获取他的毫秒数
	// TODO 毫秒数没有变 但是月的时候会变成本月而不是上月
	function fnShowInput(){//已经判断在哪个tab
		var currTab = fnJudgeTab();//直接知道在哪个tab页

		var id      = $(currTab),
	  		weekOrM = id.children('.chooce-days').children('.btn-active').attr("data-chooce"),
	  		input   = id.children('.chooce-days').children('input'),
	  		inputVal= "";

	  	var nowaDays = new Date();
	    var oneDay = 1000*60*60*24;//一天的毫秒数

	  	if ( weekOrM == "week" ) {
	  		var msDate  = Number( input.attr("data-date-week") );
	  		var myDate = new Date( msDate );//毫秒数

			var	week     = myDate.getDay(),
				minusDay = week!=0 ? week-1 : -1;
				// 上周是按自然周的上周 但这个不是自然周
				// 获取今天是本周的第几天 这里按1234567算 加3是因为找到上周五
				//  -1 0 1 2 3 4 5

			var	preWeekFri = new Date( myDate.getTime()-(oneDay*(minusDay+3)) ),// 上周星期五的毫秒
				nowtWeekThu = new Date( myDate.getTime()-(oneDay*(minusDay-3)) );// 本周四
			var nowtWeekFri = new Date( myDate.getTime()-(oneDay*(minusDay-4)) );//本周五

			// 如果是本周四 || 本周五，就显示(自然周的)下一周
			if ( minusDay >= 4 ) {
				preWeekFri = new Date( preWeekFri.getTime()+oneDay * 7 );
				nowtWeekThu = new Date( nowtWeekThu.getTime()+oneDay * 7 );
			};

			// 判断本周五毫秒数是否大于当前 如果大于就显示本周 否则显示上上周五到上周四
			nowaDays = new Date(nowaDays.getFullYear(),nowaDays.getMonth(),nowaDays.getDate());
			nowtWeekFri = new Date(nowtWeekFri.getFullYear(),nowtWeekFri.getMonth(),nowtWeekFri.getDate());
			if ( nowaDays.getTime() < nowtWeekFri.getTime() ) {
				preWeekFri = new Date( preWeekFri.getTime()-oneDay * 7 );
				nowtWeekThu = new Date( nowtWeekThu.getTime()-oneDay * 7 );
			};

			inputVal = (preWeekFri.getMonth()+1) + "/" + (preWeekFri.getDate()) + " - " + 
				   (nowtWeekThu.getMonth()+1) + "/" + (nowtWeekThu.getDate());

			input.attr("data-start", fnDateFormat(preWeekFri) );
			input.val( inputVal );
	  	}else{
	  		var msDate  = Number( input.attr("data-date-month") );
	  		var myDate = new Date( msDate );//毫秒数
			var	nowYears = myDate.getFullYear(),// 当年
				nowMonth = myDate.getMonth();

			if ( nowMonth == 0 ) {
				nowMonth = 12;
				nowYears = nowYears - 1;
			};
			// TODO 现在是直接返回本月的
			inputVal = nowYears + "年" + (nowMonth+1) + "月";

			// TODO 不知道怎么判断是不是第一次点进来，所以我要怎么办

			input.attr("data-start", fnDateFormat( (new Date(nowYears,nowMonth,1) ).getTime()) );
			input.val( inputVal );
	  	}
	}

	// 传进去一个毫秒数resDate 返回当前的日期 YYYYMMDD
	function fnDateFormat(resDate){
		var msDate = Number(resDate);
		var date  = new Date( msDate ),
			years = date.getFullYear(),
			month = date.getMonth() + 1,
			day   = date.getDate();
		if ( month < 10 ) {
			month = "0" + month;
		};
		if ( day < 10 ) {
			day = "0" + day;
		};
		return years+""+month+day;
	}

	// 视图tab的表格 resData表格数据来自ajax请求
	function fnViewTable(resData){
		var html = "";
		var trData = resData;// 避免全局扰乱当前函数

		// 使用index标记当前第几个合并行
		// 第一行的时候因为前面没有对应的比较，所以比较其与后面不一样的，之后标记位置。为下一次判断提供位置
		// 判断是否是最后一个，因为最后一个和倒数第二个是否一样
		var firRowIndex = 0,// 第一行第一列的默认为1
			secRowIndex = 0;// 第一行第二列的默认为1
		for (var i = 0; i < trData.length; i++) {//行
			html += "<tr>";
			var tdData = trData[i].data;

			if ( i==0 ) {//第一个特殊处理
				for (var y = 0; y < trData.length; y++) {// 一行一列
					if ( trData[i].data[0] != trData[i+y].data[0] ) {
						firRowIndex = y;
						html += "<th rowspan="+y+">"+trData[i].data[0]+"</th>";
						break;
					};
				};

			}else if( i==(trData.length-1) ){
			// 最后一个且前面都相等 只有最后一个不一样
				if ( trData[i].data[0] != trData[i-1].data[0] ) {
					html += "<th>"+trData[i].data[0]+"</th>";
				};

			}else if ( i==firRowIndex ) {// // 不是第一行的话
				// 从上个位置遍历一次，如果发现(全相等)就直接跑最后一条
				for (var z = 0; z < (trData.length-firRowIndex); z++) {
					if( trData[i].data[0] != trData[i+z].data[0] ) {
						firRowIndex = firRowIndex + z;
						html += "<th rowspan="+(z)+">"+trData[i].data[0]+"</th>";
						break;
					}
					//如果是最后一个 且前面都不相等
					if ( z==(trData.length-firRowIndex-1) && trData[i].data[0] != trData[i-z].data[0]) {
						firRowIndex = firRowIndex + z;
						html += "<th rowspan="+(z+1)+">"+trData[i].data[0]+"</th>";
						break;
					}
				};

			}

			if ( i==0 ) {//第一个特殊处理
				for (var y = 0; y < trData.length; y++) {// 一行一列
					if ( trData[i].data[1] != trData[i+y].data[1] ) {
						secRowIndex = y;
						html += "<th rowspan="+y+">"+trData[i].data[1]+"</th>";
						break;
					};
				};

			}else if( i==(trData.length-1) ){
			// 最后一个且前面都相等 只有最后一个不一样
				if ( trData[i].data[1] != trData[i-1].data[1] ) {
					html += "<th>"+trData[i].data[1]+"</th>";
				};

			}else if ( i==secRowIndex ) {// // 不是第一行的话
				// 从上个位置遍历一次，如果发现(全相等)就直接跑最后一条
				for (var z = 0; z < (trData.length-secRowIndex); z++) {
					if( trData[i].data[1] != trData[i+z].data[1] ) {
						secRowIndex = secRowIndex + z;
						html += "<th rowspan="+(z)+">"+trData[i].data[1]+"</th>";
						break;
					}
					if ( z==(trData.length-secRowIndex-1) && trData[i].data[1] != trData[i-z].data[1]) {
						secRowIndex = secRowIndex + z;
						html += "<th rowspan="+(z+1)+">"+trData[i].data[1]+"</th>";
						break;
					}
				};
			}

			html += "<th>"+trData[i].name+"</th>";//标题列
			for (var j = 2; j < tdData.length; j++) {// 数据列
				var temp = trData[i].data[j];
				html += fnNumberFormat(temp);//处理数的函数 将这些数格式化
			};

			html += "</tr>";
		};
		$(".view-table tbody").html( html );
	}

	// ajax请求的视图表格的数据
	function fnGetTableData(){
		var currTab = fnJudgeTab();//直接知道在哪个tab页
		var id      = $(currTab),
	  		input   = id.children('.chooce-days').children('input'),
		 startDate  = input.attr("data-start");

	  	var weekOrMonth = id.children('.chooce-days').children('.btn-active').attr("data-chooce");
	  	var sendData = new Object();
	  	if ( weekOrMonth == "month" ) {
	  		id.find(".view-table tr:eq(0)").children("th:eq(1)").html("本月");
	  		id.find(".view-table tr:eq(0)").children("th:eq(2)").html("上月");
	  		sendData = { date: startDate, timelevel: "month" };
	  	}else{
	  		id.find(".view-table tr:eq(0)").children("th:eq(1)").html("本周");
	  		id.find(".view-table tr:eq(0)").children("th:eq(2)").html("上周");
	  		sendData = { date: startDate };
	  	}

		$.ajax({
			url: '/data/monitor/weekmonth/querymaindata',
			type: 'POST',
			dataType: 'json',
			data: sendData,
			beforeSend: function(){
                ajaxbg.show();
            },
			success: function(res){
				if ( res.stat=="ok" ) {
					var resData = res.result.result;
					// 判断是否空 以保证传过去的对象必须有数据
					if ( resData.length===0 ) {
						$(".view-table tbody").html( "暂无数据！" );
					}else{
						fnViewTable(resData);
					}
				}else{
					$(".view-table tbody").html( "数据加载失败！" );
				}
			},
			error: function(){
				console.log("this view-table is error");
			},
			complete:function(){
                ajaxbg.hide();
            }
		});
	}

	var chartsData = {
        chart: { type: 'column' },
        title: { 
        	text: '有订单不配送数量'
        	// align: 'left'
    	},
    	subtitle: {
    		text: " "
    	},
        xAxis: { 
        	categories: [],
		    style: { font:"4px" },
		    gridLineWidth: 1
        },
        yAxis: [
            {// %
                title: {
                    text: ''
                },
                max: 100,
                // opposite: true,//左右浮动
                labels: {
                    formatter: function() {
                        return this.value + "%";
                    }
                }
            },
            {// !%
                title: {
                    text: ''
                },
                labels: {
                    formatter: function() {
                       return this.value;
                    }
                },
            }
        ],
        plotOptions: {
            column: {
                // pointPadding: 0.2,
                borderWidth: 0.1,
				stacking: ''//重叠否
            }
        },
        tooltip: { 
        	shared: true,
        	useHTML: true,
			borderWidth: 0,
			borderRadius: 5,
			borderColor: '#909090',
			style: {
				width: "100%",
				padding: "5px",
		    	background: 'white'
			},
			positioner: function(boxWidth, boxHeight, point) {
				var chart = this.chart,
			        plotLeft = chart.plotLeft,
			        plotTop = chart.plotTop,
			        plotWidth = chart.plotWidth,
			        plotHeight = chart.plotHeight,
			        pointX = point.plotX,
			        pointY = point.plotY,
			        x = pointX + plotLeft,
			        y = pointY - boxHeight + plotTop,
			        alignedRight;

			    if ((x + boxWidth) > (plotLeft + plotWidth)) {
			        x -= (x + boxWidth) - (plotLeft + plotWidth);
			        y = pointY - boxHeight + plotTop;
			        alignedRight = true;
			    }

			    if (y < plotTop + 5) {
			        y = plotTop + 5;

			        if (alignedRight && pointY >= y && pointY <= (y + boxHeight)) {
			            y = pointY + plotTop;
			        }
			    }

	        	return {
	        		x: x,
	        		y: y
	        	};
	        	
    		},
        	formatter: function() {
        		var temp = "", xStr = this.x;
        		temp += "<div style='display:block;width:100%;'>" + "<i>"+xStr + "</i><br/>";
	        	if ( chartsData.plotOptions.column.stacking == "normal" ) {
	        		for (var i = 0; i < this.points.length; i++) {
		        		var name = this.points[i].series.name;
		        		if ( name.indexOf("<") >= 0 ) {
							name = "&lt;" + name.substr(1);
		        		};
		        		if ( name.length > 2 ) {
		        			temp += '<div style="display:inline-block;width:95px;">' +
		        			 			'<i style="display:inline-block;width: 70%;text-align:right;">' + name + '&nbsp;:&nbsp;</i>' +
		        						'<b style="display: inline-block;width: 20%;text-align:left;">' + this.points[i].y + '</b>' +
		        					'</div><br/>';
		        		}else{
		        			temp += '<div style="display:inline-block;width:100%;padding-right:5px;padding-left5px;">' +
		        			 			'<i style="display:inline-block;width: 50%;text-align:right;">' + name + '&nbsp;:&nbsp;</i>' +
		        						'<b style="display: inline-block;width: 50%;text-align:left;">' + this.points[i].y + '</b>' +
		        					'</div><br/>';
		        		}	
		        	};
	        	}else{
	        		for (var i = 0; i < this.points.length; i++) {
	        			
		        		var name = this.points[i].series.name;
		        		if ( name.indexOf("<") >= 0 ) {
							name = "&lt;" + name.substr(1);
		        		};
		        		temp += '<span style="display:inline-block;width:100%;padding-left:5px;padding-right:5px;">'
		        					+ "<i style='display:inline-block;'>" + name + '&nbsp;:&nbsp;</i>'
		        					+ "<b style='display:inline-block;'>" + this.points[i].y + '</b>'
		        				+ '</span><br/>';
		        	};
	        	}
		        temp += "</div>";

	        	return temp;
	        }
        },
        legend: {
		    y: 30,
		    // layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
		    backgroundColor: 'white',
			borderWidth: 1,
			borderRadius: 5,
			borderColor: '#909090'
        },//图标
        series: [ ],
        credits: {//去水印  
  			enabled: false  
		}
    };

    // 通过传过来的发送的参数 发送请求 return resDAta；返回的是一个json，这个就是根据参数得到的数据
	function fnAjaxRes(sendData){
		$.ajax({
			url: '/data/monitor/weekmonth/querychartdata',
			type: 'GET',
			dataType: 'json',
			data: sendData,
			beforeSend: function(){
                ajaxbg.show();
            },
			success: function(result){
				if ( result.length != 0 ) {
					// 分段配送时长 && 站点按单量分布
					if ( sendData.id=="export_weekmonth_698342540" || sendData.id=="export_weekmonth_-2055201568" ) {
						chartsData.plotOptions.column.stacking = "normal";
					}else{
						chartsData.plotOptions.column.stacking = "";
					}

					var currTab = fnJudgeTab();
					var selectStr = ".chooce-select-" + sendData.timelevel;
					var selectText = $(currTab).find(selectStr).find("option:selected").text();
					if ( sendData.dataflag == 2 ) {
						chartsData.title.text = selectText + "(全国)";
						fnDrawLeftCharts(result.chartData );// 直营图 & 表
						fnLeftTable(result);
					};

					if ( sendData.dataflag == 3 ) {
						chartsData.title.text = selectText + "(详情)";
						fnDrawRightCharts(result.chartData);
						fnRightTable(result);
					}
				}else{
					return;
				}
			},
			error: function(){
				console.log("post Stacking Charts data error");
			},
			complete:function(){
                ajaxbg.hide();
            }
		});
	}

	// 画图表的 调用他之后将参数传给fnAjaxRes()请求数据 
	// 请求之后调用fnDrawProfitCharts画图 fnNoStackingProfitTable画表
	function fnGetChartsData(haveCity){
		var currTab = fnJudgeTab();//直接知道在哪个tab页
		var id      = $(currTab),
		    input   = id.children('.chooce-days').children('input'),
		startDate   = input.attr("data-start"),
		weekOrMonth = id.children('.chooce-days').children('.btn-active').attr("data-chooce");

		var sendData = new Object();//最终传给ajax的参数
		
		// 下拉框的值由这个直接获取val
		var selectstr = ".chooce-select-" + weekOrMonth;
		var select    = id.find(selectstr);
        if( select.val() && select.length > 0 ){
            sendData.id = select.val();
        }
		
		//周月之间的切换
	  	if ( weekOrMonth == "month" ) {
	  		sendData.timelevel = "month";
	  	}else{
	  		sendData.timelevel = "week";
	  	}

	  	// 开始时间由当前的值给
		var dateStart = id.children('.chooce-days').children('input').attr("data-start");
	  	sendData.date = dateStart;

	  	if ( currTab=="#direct" ) {//在哪个tab 标签
			sendData.typeid = 101;
		};
		if ( currTab=="#join" ) {
			sendData.typeid = 201;
		};

		// 堆形图 由函数调用时给 2代表左边的 3代表右边的
		sendData.dataflag = haveCity;
		fnAjaxRes(sendData);
	}

	// 左边画堆型柱状图  ＝》配送时长
	// @data 图表的数据
	function fnDrawLeftCharts(data){
		// 堆图的x轴
		chartsData.xAxis.categories.length = 0;
		for (var i = 0; i < data.xNames.length; i++) {
			chartsData.xAxis.categories.push( data.xNames[i] );
		};

		// y轴数据的push
		fnYdataIfstacking(data.data);
		var currTab = fnJudgeTab();
		$(currTab).find(".charts-stacking").highcharts(chartsData);
	}

	// 右边画无堆集的柱形图  ＝》利润
	// @data 图表的数据
	function fnDrawRightCharts(data){
		// x
		chartsData.xAxis.categories.length = 0;
		for (var i = 0; i < data.xNames.length; i++) {
			// 判断是否有","有的话换行
			// TODO 使用一个临时变量 str
			var temp = data.xNames[i];
			if( data.xNames[i].indexOf(",") > 0 ) {
				var str = data.xNames[i];
				var arr = str.split(",");
				temp = arr[0] + "<br/>" + arr[1];
			}
			chartsData.xAxis.categories.push( temp );
		};
		// y轴数据的push
		fnYdataIfstacking(data.data);
		var currTab = fnJudgeTab();
		$(currTab).find(".charts-noStacking").highcharts(chartsData);
	}

	// 判断是否是堆积状态 然后将数据push进y轴
	// @yData 传进来的y轴的数据 只有 fnDrawLeftCharts & fnDrawRightCharts调用
	function fnYdataIfstacking(yData){
		// y
		chartsData.series.length = 0;
		var resLen = yData.length;

		var r = 165, g = 229, b = 255;
		var ri = parseInt( (r-63)/resLen ),
			gi = parseInt( (g-124)/resLen ),
			bi = parseInt( (b-195)/resLen );

		// 是否堆积图
		// var colorArr = ["#96cbfd", '#73b4f0', '#579be3', '#4083c6', '#4277ad', '#456b93'];
		if ( yData.length!=0 ) {
			if ( yData[0].data.length == 0 ) {
	            chartsData.series.yAxis = 1;
	            chartsData.series.length = 0;
			}else{
				for (var i = 0; i < resLen; i++) {// y轴的数据一样
					var temp = new Object();

					temp.name = yData[i].yName;//TODO
					temp.data = new Array();

					for (var j = 0; j < yData[i].data.length; j++) {
						if( yData[i].data[j].indexOf("%") >= 0 ) {// 判断百分号
			            	temp.yAxis = 0;
			        	}else{
			            	temp.yAxis = 1;
			        	}
						temp.data.push( parseFloat( yData[i].data[j] ) );
					};
					// 颜色
					r -= ri; g -= gi; b -= bi;
					temp.color = "rgb("+r+","+g+","+b +")";
					// temp.color = colorArr[i];

					chartsData.series.push(temp);
				};
			}
		};
	}
	
	// 有堆形柱形图下面的表
	function fnLeftTable(data){
		var currTab = fnJudgeTab();
		var tableHtml = "";
		if (data.data.length != 1 ) {
			var currTab = fnJudgeTab();
			tableHtml += "<table>" +
						"<thead><tr>";
			for (var i = 0; i < data.captions.length; i++) {
				tableHtml += "<th>" + data.captions[i] + "</th>";
			};
			tableHtml += "</tr></thead>";
			tableHtml += "<tbody>"

			for (var i = 1; i < data.data.length; i++) {
				tableHtml += "<tr>";
				for (var j = 0; j < data.data[i].length; j++) {
					if ( j != 1 ) {
						tableHtml += fnNumberFormat(data.data[i][j]);
					}else{
						tableHtml += "<td>" + data.data[i][j] + "</td>";
					}
				};

				tableHtml += "<tr>";
			};

			tableHtml += "</tbody>" +
						"</table>";
		}else{
			tableHtml += "暂无数据！";
		}
		$(currTab).find('.stacking-table').html(tableHtml);
	}
	// 没有堆型图下面的表 就是柱形图下面的表 ＝》利润
	function fnRightTable(data){
		var currTab = fnJudgeTab();
		var tableHtml = "";

		// TODO 
		// 这个不等于1的原因是第一条数据是表头，但是captions没有数据
		if (data.data.length != 1 ) {
			tableHtml += "<table>" +
						"<thead><tr>";
			for (var i = 0; i < data.captions.length; i++) {
				tableHtml += "<th>" + data.captions[i] + "</th>";
			};
			tableHtml += "</tr></thead>";
			tableHtml += "<tbody>"

			for (var i = 1; i < data.data.length; i++) {
				tableHtml += "<tr>";
				for (var j = 0; j < data.data[i].length; j++) {
					if ( j != 2 ) {
						tableHtml += fnNumberFormat(data.data[i][j]);
					}else{
						tableHtml += "<td>" + data.data[i][j] + "</td>";
					}
				};

				tableHtml += "<tr>";
			};

			tableHtml += "</tbody>" +
						"</table>";
		}else{
			tableHtml = "暂无数据！";
		}
		$(currTab).find('.noStacking-table').html(tableHtml);
	}
	// 写了一个处理小数或者 大于1000的格式化 可以将其变为xxx,xxx.xx
	// 传进来一个string 通过判断是否分数，小数，等等
	// 返回一个字符串 <td>XXX</td>
	function fnNumberFormat(str){
		var temp = str;
		var html = "";
		if(temp.indexOf("%") >= 0){//不是百分数
			html += "<td style='text-align: right;'>" + temp + "</td>";
		}else if( isNaN(temp) ){//不是数字
			if ( temp == "-" ) {
				html += "<td style='text-align: center;'>"+ temp +"</td>";
			}else{
				html += "<td>"+ temp +"</td>";
			}
		}else{//是数字
			html += "<td style='text-align: right;'>";
				// TODO 小数的
				var xiaoshuIndex = temp.indexOf(".");
				var xiaoshu = "";//保存小数之后的几位
				xiaoshu = temp.substr(xiaoshuIndex);//对于小数的处理

				temp = parseInt(temp);
				// 整数的时候才可以这样
				if ( temp > 1000 || temp < -1000 ) {
					temp = temp + "";
					var tempArr = temp.split("");
					var tempStr = "";//保存加了，的那个字符串
					for (var tempI = temp.length-1; tempI >= 0; tempI=tempI-3){
						//负数的符号位 不增加,
						if(tempI==0&&temp<0){
							continue;
						}
						 tempArr[tempI] += ",";
					}
					tempStr += tempArr.join("");

					tempStr = tempStr.substr(0,tempStr.length-1);//去掉最后一个逗号
					if ( xiaoshuIndex>=0 ) {
						html += tempStr + xiaoshu;
					}else{
						html += tempStr;
					}
				}else{
					//小数此处需要处理
					if ( xiaoshuIndex>=0 ) {
						html += temp + xiaoshu;
					}else{
						html += temp;
					}
				}
		}
		return html;
	}

	// 默认的初始化函数 直接调用啦
	(function init(){
	  	// 默认就是今天的毫秒数咯
	  	var myDate  = new Date();//选择的那天
	  	var currTab = fnJudgeTab();
	  	// 把毫秒数都放到输入框里面 之后直接显示出来即可

	  	var month = myDate.getMonth(),//不减1是因为默认就是－1
	  		years = myDate.getFullYear();
	  	var monthDate = new Date(years, month-1, 1);

		var weekOrMonth = $(currTab).children('.chooce-days').children('.btn-active').attr("data-chooce");
	  	$(currTab).children('.chooce-days').children('input').attr("data-date-week", myDate.getTime());
	  	$(currTab).children('.chooce-days').children('input').attr("data-date-month", monthDate.getTime());
		
	  	fnShowInput();
		fnClickDateChooce();//时间组件的一些选择
	  	fnGetTableData();//进来默认加载表格
		fnGetSelectList();//加载那个下拉框
	}());

});