require.config({baseUrl:MT.STATIC_ROOT+"/js",urlArgs:"ver="+pageVersion,shim:{"lib/bootstrap":{deps:["lib/jquery"]}}}),define(["module/utils","module/cookie","module/head-nav"],function(utils,cookie){"use strict";$('[data-toggle="tooltip"]').tooltip()});


  var jQmenuToggle = $('#menuToggle'), jQwrapper = $('#wrapper');

  var menuToggle = function () {
    (jQwrapper.hasClass('sidebar-hide')) ? jQwrapper.removeClass('sidebar-hide'): jQwrapper.addClass('sidebar-hide');
  };

  var bindEvt = function () {
    jQmenuToggle.click(menuToggle);
  };
  bindEvt();