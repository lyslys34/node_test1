;$(function() {

	(function() {
		var week = moment().subtract(6, 'days').format('YYYYMMDD') + ' - ' + moment().subtract(0, 'days').format('YYYYMMDD');
		var $datepicker = $('#datepicker');
		$datepicker.val(week);
		$datepicker.daterangepicker({
		    startDate: moment().subtract(6, 'days').format('YYYYMMDD'),
		    endDate: moment().subtract(0, 'days').format('YYYYMMDD'),
		    minDate: moment().subtract(40, 'days'),			// 最早不能超过30天前
		    maxDate: moment().subtract(0, 'days'),			// 未来日期不可选
		    dateLimit: {									// 时间间隔不超过6天（一周）
		    	'days': 6
		    },
			format: 'YYYYMMDD',
			locale: {
		        applyLabel: '确定',
		        cancelLabel: '取消',
		        fromLabel: '从',
		        toLabel: '到',
		        customRangeLabel: '定制',
		        daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
		        monthNames: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
		        firstDay: 1
		    }
		}, function(start, end, label) {
			$datepicker.val(start.format('YYYYMMDD') + ' - ' + end.format('YYYYMMDD'));
		})
	})()

	var bm = {
		wrap: $('body'),
		sortIndex: null,				// 用来记录排序的列
		sortOrder: null,				// 用来记录排序的方式
		orgType: -1,
		pageNum: 1,
		pageSize: 20,
		currPage: 1,
		totalPage: 1,
		events: [
			['click', '#searchBtn', '_searchEvent']
			,['click', '#exportBtn', '_export']
			,['click', '#fixedHead .fa', '_sort']
			,['click', '.page_tab', '_pageTab']
			,['keyup', '#targetPage', '_jumpToPage']
			,['click', '#jumpBtn', '_jumpToPage']
			,['change', '#pageSet', '_changePageSize']
		],
		_init: function() {
			var me = this;

			me._getOrgLevel();
			// me._search();
			me._bind();
		},
		_getOrgLevel: function() {				// 获取组织层级
			var me = this;
			var callback = function(res) {
				sel.orgList = res;

				sel._init();
				me._search();
			}
			me._ajax('/data/monitor/abnormalorder/orgList', {}, 'GET', callback)
		},
		_searchEvent: function(e) {
			var me = this;
			if ($(e.target).hasClass('disabled')) {
				return false;
			}

			me.pageNum = 1;
			me.sortOrder = null;
			me.sortIndex = null;

			me._search();
		},
		_search: function() {
			var me = this;
			var data = {
				type: me.orgType,
				orgId: me._getSelectedOrg(),
				city: $('#cityName').val(),
				rider: $('#riderName').val(),
				stime: $('#datepicker').val().substr(0, 8),
				etime: $('#datepicker').val().substr(-8),
				sortIndex: me.sortIndex,
				sortOrder: me.sortOrder,
				pageNum: me.pageNum,
				pageSize: me.pageSize
			}

			var callback = function(res) {
				me._renderTable(res)
			}
			me._ajax('/data/monitor/abnormalorder/tableData', data, 'GET', callback)
		},
		_getSelectedOrg: function() {
			var id = -1;
			var $sel = $('.s2-element');
			var n = $sel.length;
			for (var i = n; i > 0; i --) {
				var v = $sel.eq(i - 1).val();
				if (v > 0) {
					id = v;
					break;
				}
			}
			return id;
		},
		_renderTable: function(data) {
			var me = this;
			var html = template('J_table_tpl', data);
			$('#tableBox').html(html);

			// pagination
			me.totalPage = data.totalPage;
			me.currPage  = data.currPage;
			$('#totalPage').html(me.totalPage);
			$('#currPage').html(me.currPage);
			$('#targetPage').val('');
			// fix table head
			var $thead = $('#tableBox').find('thead');
			var arr = [];
			$thead.find('th').each(function() {
				var w = $(this).width() + 30;
				arr.push(w);
			})
			var $theadCopy = $thead.clone();
			$theadCopy.find('th').each(function(index) {
				$(this).css({
					minWidth: arr[index] + 'px',
					maxWidth: arr[index] + 'px'
				})
			})
			$('#fixedHead').empty().prepend($theadCopy);
			$('#tableBox').width($('#fixedHead').width() + 2)

			if (me.sortIndex !== null && me.sortIndex >= 0) {
				$('#fixedHead').find('th').eq(me.sortIndex).find('.fa').eq(me.sortOrder).addClass('on');
			}
		},
		_pageTab: function(e) {
			var me = this;
			var $tab = $(e.target);
			var type = $tab.data('type');
			var n;
			switch(type) {
				case 'first':
					n = 1;
					break;
				case 'prev':
					n = me.currPage == 1 ? 1 : me.currPage - 1;
					break;
				case 'next':
					n = me.currPage == me.totalPage ? me.totalPage : me.currPage + 1;
					break;
				case 'last':
					n = me.totalPage;
					break;
			}
			if (n == me.currPage) {
				return false;
			}else {
				me.pageNum = n;
				me._search();
			}
		},
		_jumpToPage: function(e) {
			var me = this;
			if ((e.type == 'keyup' && e.keyCode == 13) || (e.type = 'click' && e.target.tagName.toLowerCase() == 'button')) {
				var n = $('#targetPage').val();
				if (!/\d+/.test(n) || n < 1 || n > me.totalPage) {
					alert('请输入正确页码');
					return false;
				}
				me.pageNum = n;
				me._search();
			}
		},
		_changePageSize: function(e) {
			var me = this;
			var n = $(e.target).val();
			me.pageSize = n;
			me.pageNum = 1;
			me._search();
		},
		_sort: function(e) {
			var me = this;
			var $fa = $(e.target);
			var $td = $fa.parents('th');

			me.sortOrder = $fa.hasClass('fa-caret-up') ? 0 : 1;
			me.sortIndex = $td.index();

			me._search()
		},
		_export: function() {
			var me = this;
			var data = {
				type: me.orgType,
				orgId: me._getSelectedOrg(),
				city: $('#cityName').val(),
				rider: $('#riderName').val(),
				stime: $('#datepicker').val().substr(0, 8),
				etime: $('#datepicker').val().substr(-8),
			}
			var arr = [];
			for (var k in data) {
				var str = k + '=' + data[k];
				arr.push(str);
			}
			var href = arr.join('&');
			window.open('/data/monitor/abnormalorder/tableDataExport?' + href)
		},
		_bind: function() {									// 绑定事件
			var me = this;
			var	context = me,
				events  = me.events,
				$wrap   = me.wrap;

			events && $.each(events, function(k, v) {
				var handler;
				me[v[2]] && (handler = me[v[2]]);

				var ev = v[0] + '.' + k,
					el = v[1];
				$wrap.off(ev).on(ev, el, function(e) {
					handler.apply(context, [e, this]);
				})
			})
		},
		_ajax: function(url, data, type, callback) {
			var me = this;
			var callback = callback || function() {};
			$.ajax({
				url: url,
				type: type,
				dataType: 'json',
				data: data,
				success: function(res) {
					$('#searchBtn').removeClass('disabled');
					if (res.code == 0) {
						callback(res.data);
					}else {
						alert(res.msg);
					}
				},
				beforeSend: function() {
					$('#searchBtn').addClass('disabled')
				}
			})
		}
	}

	// 组织层级select
	var sel = {
		orgList: [],
		level: 0,					// 修改选择项对应的level
		levelCount: 0,				// 总的level数量
		orgIndex: 0,				// 总部人员有三种组织权限，保证返回数据的顺序是『自建-加盟-众包』。其他权限org只有一种，orgIndex总为0.
		_init: function() {
			var me = this;

			me.orgIndex = 0;
			(me.orgList.length != 3) && $('.tab').eq(me.orgIndex).siblings('.tab').remove();	// 非总部人员没有查看其它tab页的权限

			me._renderSelectDom(me.orgList[me.orgIndex]);

			me._bind();
		},
		_renderSelectDom: function(data) {
			var me = this;

			$('.tab').eq(me.orgIndex).addClass('on').siblings('.tab').removeClass('on');
			bm.orgType = $('.tab').eq(me.orgIndex).data('type');

			var n = data.levels.length;
			me.levelCount = n;

			var html = '';
			for (var i = 0; i < n; i ++) {
				var level = data.levels[i];
				html += '<select class="s2-element" data-type=\"' + level.levelType + '\" data-desc=\"' + level.levelDesc + '\">'
						+ '<option value=\"-1\">全部' + level.levelDesc + '</option>'
						+ '</select>';
			}
			$('#selectBox').html(html);

			me.level = 0;	// 从 level 0 开始重新渲染所有select
			me._initRenderSelect(data.subLevel);
		},
		_initRenderSelect: function(sub) {			// 第一次渲染select列表
			var me = this;
			if (!sub) {return false;}

			var list = sub.orgNodes;
			if (!list) {
				alert('该组织层级没有数据，请重新选择');
				return false;
			}

			var n = list.length;
			var $sel = $('.s2-element').eq(me.level);

			var html = '';
			if (n > 1) {
				html += '<option value=\"-1\">全部' + $sel.data('desc') + '</option>';
				for (var i = 0; i < n; i ++) {
					html += '<option value=\"' + list[i].id + '\">' + list[i].name + '</option>';
				}
				$sel.html(html);
			}else {
				html += '<option value=\"' + list[0].id + '\">' + list[0].name + '</option>';
				$sel.html(html);
				$sel.prop('disabled', true);

				me.level ++;

				me._initRenderSelect(list[0].subLevel);
			}
			$('.s2-element').select2({width: 160})
		},
		_renderSelect: function(sub) {
			var me = this;

			if (!sub) {return false;}			// subLevel不存在的异常情况

			var list = sub.orgNodes;
			var $sel = $('.s2-element').eq(me.level);

			var n = list.length;


			var html = '';
			if (n == 0) {
				alert('该组织层级没有数据，请重新选择');
				for (var i = me.level; i < $('.s2-element').length; i ++) {
					var $oSel = $('.s2-element').eq(i);
					var oHtml = '<select class="s2-element" data-type=\"' + $oSel.data('type') + '\" data-desc=\"' + $oSel.data('desc') + '\">'
							+ '<option value=\"-1\">全部' + $oSel.data('desc') + '</option>'
							+ '</select>';
					$oSel.html(oHtml)
				}
			}

			if (n >= 1) {
				// 渲染当前level的select
				html += '<option value=\"-1\">全部' + $sel.data('desc') + '</option>';
				for (var i = 0; i < n; i ++) {
					html += '<option value=\"' + list[i].id + '\">' + list[i].name + '</option>'
				}
				$sel.html(html);

				for (var i = me.level + 1; i < me.levelCount; i ++) {
					var $oSel = $('.s2-element').eq(i);
					var oHtml = '<option value=\"-1\">全部' + $oSel.data('desc') + '</option>'
					$oSel.html(oHtml);
				}
			}

			$('.s2-element').select2({width: 160})
		},
		_bind: function() {
			var me = this;
			$('.tab').on('click', function() {
				var index = $(this).index();
				me.orgIndex = index;
				me.level = 0;
				me._renderSelectDom(me.orgList[me.orgIndex]);
			})

			$('body').off('change.s2').on('change.s2', '.s2-element', function() {
				var $sel = $(this);
				var level = $sel.index('.s2-element');
				var index = $sel.find('option:selected').index();

				me.level = level + 1;
				var target = me.orgList[me.orgIndex];
				for (var i = 0; i < me.level; i ++) {
					var index = $('.s2-element').eq(i).find('option:selected').index();
					if (index == 0) {			// 选择了全部
						target = target;
						me.level -= 1;			// 重新渲染当前level
					}else {						// 渲染下一级level
						target = target.subLevel.orgNodes[index - 1]
					}
				}
				me._renderSelect(target.subLevel);
			})
		}
	}

	bm._init()
})