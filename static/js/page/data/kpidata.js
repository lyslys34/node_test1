;$(function() {
	var noop = function() {};
	// 默认显示的数据列
	var defaultSeries = ['完成订单数','完成率'];
	// highcharts
	var oChart;
	var chartDefaults = {
		title: {text: '数据监控'},
		credits:{ enabled:false },
		chart: {
			renderTo: 'highcharts',
			type: 'spline',
			events: {}
		},
		xAxis: {
			title: {text: '时间'},
			categories: []
		},
		yAxis: [
			{		// yAxis: 0, 百分比维度
				title: {text: '', style: {color: '#333'}},
				labels: {
					style: {
						color: '#333'
					},
					formatter: function() {
						return this.value + '%';
					}
				}
			},
			{		// yAxis: 1, 小数维度, 1~100
				title: {text: '', style: {color: '#E0850D'}},
				labels: {
					style: {
						color: '#E0850D'
					}
				}
			},
			{		// yAxis: 2, 大数量级, 1~1k
				title: {text: '', style: {color: '#0D08CC'}},
				labels: {
					style: {
						color: '#0D08CC'
					}
				},
				opposite: true
			},
			{		// yAxis: 3, 超大数量级, 1~1000K
				title: {text: '', style: {color: '#059C1A'}},
				labels: {
					style: {
						color: '#059C1A'
					}
				},
				opposite: true
			}
		],
		series: [],
		tooltip: {
			shared: true,
			crosshairs: {
				width: 1,
				color: 'orange'
			},
			headerFormat: '<b>{point.x}</b><br/>'
		},
		plotOptions: {
			series: {
				events: {
					legendItemClick: function() {
						var name = this.name;
						var visible = this.visible;
						if (visible) {		// 当前该数据列是可见的，即点击是为了隐藏数据列。故将对应name从默认显示中移除
							var index = $.inArray(name, defaultSeries);
							defaultSeries.splice(index, 1);
						}else {
							defaultSeries.push(name);
						}
					}
				}
			}
		}
	};
	// dataTable
	$.extend($.fn.dataTableExt.oSort, {						// datatables中文排序
	    "chinese-string-asc" : function (s1, s2) {
	        return s1.localeCompare(s2);
	    },
	    "chinese-string-desc" : function (s1, s2) {
	        return s2.localeCompare(s1);
	    }
	});
	// extend dataTable default settings
	$.extend($.fn.dataTable.defaults, {
		scrollX: true,
		scrollY: 500,
		paging: false,										// 分页
		orderClasses: true,
		deferRender: true,
		order: [],											// 默认不排序
		ordering: false,
		info: false,
		iDisplayLength: 200,									// 默认显示20条
		lengthMenu: [ [20, 50, -1], [20, 50, "全部"] ],		// 规定表格每页显示多少条
		language: {
			search: '搜索：',
			searchPlaceholder: '输入关键词',
			lengthMenu: '每页显示 _MENU_ 条',
			infoFiltered: "",
			paginate: {
				next: '下一页',
				previous: '上一页'
			}
		},
		columnDefs: [										// 按照中文字符排序
	       { type: 'chinese-string', targets: 0 }
	    ]
	});
	// banma
	var banma = {
		wrap: $('#main-container'),
		events: [
			['change', '#searchInput', '_searchInput']				// 从select2中选择
			,['click', '#mainTable tr', '_chooseTableItem']			// 点击表格单元行将对应行数据绘制为highcharts
			,['click', '.drill_btn', '_drill']						// 点击drill_btn下钻查询
			,['click', '.item_rider, .item_poi', '_chooseDrill']	// 选择下钻到骑手or商家
			,['click', '.close_icon', '_closeDrillTip']				// 关闭下钻菜单
			,['mouseover', '#exportBtn', '_showExportMenu1']		// 导出
			,['mouseout', '#exportBtn', '_showExportMenu2']			// 导出
			,['click', '.export_table', '_exportTable']				// 导出表格
			,['click', '.export_chart', '_exportChart']				// 导出图表
			,['click', '.tab_item', '_searchAll']					// 点击Tab栏查询全部
			,['click', '#searchBtn', '_search']						// 查询
			,['click', '.td_jump', '_jump']
			,['click', '.nav_item', '_nav']
			,['mouseover', '.has_full_name', '_showFullName']
			,['mouseout', '.has_full_name', '_removeFullName']
			,['click', '#pageSwitch button', '_pageSwitch']
			,['click', '.arr_box > i', '_sort']
		],
		$dataTable: null,
		orgType: window.orgType,
		trailIndex: 0,
		parentId: window.orgType == 1 ? -330 : 0,
		level: null,
		isInit: false,
		_aop: null,
		nowPage: 1,
		totalPage: 1,
		_init: function() {
			var me = this;
			me._aop = me._getCookie('banma_data_aop');
			me.tabList = (me.orgType == 2 ? [{name: '加盟大区', level: 200}, {name: '加盟城市', level: 210}, {name: '加盟商', level: 220}, {name: '站点', level: 230}, {name: '骑手', level: 140}, {name: '商家', level: 150}] : [{name: '', level: 300}, {name: '配送城市', level: 310}, {name: '', level: 320}, {name: '站点', level: 330}, {name: '骑手', level: 140}, {name: '商家', level: 150}]);
			me._initForm();
			me._bind();
		},
		_initForm: function() {
			var me = this;
			var post = {
				// todo 临时改为直营默认层级为站点。
				parentId: window.orgType == 1 ? -330 : 0
			}
			me._ajax('/data/monitor/orgTableData', post, me._paintTable);
		},
		_paintTableV2: function(res, me) {

			if(res.data.tableRows == 0){
				alert('没有更下一层数据');
				return false;
			}

			me._paintTable(res,me);

        },
		_paintTable: function(res, me) {
			var data = res.data;
			// 要绘制的table数据
			var table = {
				title: data.tableHead,
				body:  data.tableBody
			}
			// 利用js模板引擎渲染数据同时实例化tableData
			var html = template('J_table_tpl', table);
			var $table = $('#tableArea');
			$table.html(html);
			var ordering = table.title[0].toLowerCase() == 'bmorgid' ? true : false;
			me.$dataTable = $table.find('#mainTable').dataTable({ordering: ordering});
			me.$dataTable.api().columns.adjust().draw(false);
			me._tdStyle();
			// 分页
			if (data.pageCount && data.pageCount > 1) {
				$('#pageSwitch').removeClass('hide');
				me.nowPage = data.page;
				me.totalPage = data.pageCount;
				$('#nowpage').text(me.nowPage);
				$('#totalpage').text(me.totalPage);
				$('#totallines').text(data.total);
				$('#mainTable_info').hide();
			}else {
				$('#pageSwitch').addClass('hide');
				$('#mainTable_info').show();
			}
			// 绘制面包屑
			me._trial(data.parents);
			// 将table中的第一行数据绘制为highcharts
			me._paintIndexLine(0);
			// 将层级结果select2
			me._select2();
		},
		_pageSwitch: function(e) {
			var me = this;
			var $btn = $(e.target);
			var type = $('#mainTable').children('tbody').data('type').toLowerCase();
			var dir  = $btn.data('page');

			var now = me.nowPage;
			var	total = me.totalPage;
			var page;

			switch(dir) {
				case 'next':
					now < total && (page = ++now);
					break;
				case 'prev':
					now > 1 && (page = --now);
					break;
				case 'first':
					now != 1 && (page = 1);
					break;
				case 'last':
					now != total && (page = total);
					break;
				case 'jump':
					var input = $('#pageinput').val();
					(input > 0 && input <= total && input != now) && (page = input);
					break;
			}
			if (!page) {
				return false;
			}
			var post = {
				parentId: me.parentId,
				page: page,
				pageSize: 200
			}
			var url = '/data/monitor/' + (type == 'riderid' ? 'riderTableData' : 'poiTableData');
			me._ajax(url, post, me._paintTable);
		},
		_tdStyle: function() {
			var $a = $('.td_a_btn');
			$a.each(function() {
				var text = $(this).text();
				if (text.length > 10) {
					text = text.substr(0, 10) + '...'
					$(this).text(text)
					$(this).addClass('has_full_name')
				}
			});
		},
		_showFullName: function(e) {
			var $dom = $(e.target);
			if (!$dom.hasClass('td_a_btn')) {
				return false;
			}
			var name = $dom.data('text');
			$dom.append('<b class="full_name">' + name + '</b>');
		},
		_removeFullName: function(e) {
			var $dom = $(e.target);
			if (!$dom.hasClass('td_a_btn')) {
				return false;
			}
			$dom.find('b').remove();
		},
		_trial: function(arr) {
			var n = arr.length;
			var oNav = document.getElementById('nav');
			var html = '';
			html += '<a class="nav_item" href="javascript:;" data-id="' + (window.orgType == 1 ? -330 : 0) + '">全部</a>';
			if (n > 0) {
				for (var i = 0; i < n; i ++) {
					html += '<a class="nav_item" href="javascript:;" data-id="' + arr[i].id + '">' + arr[i].name + '</a>';
				}
			}
			oNav.innerHTML = html;
		},
		_nav: function(e) {
			var me = this;
			var $dom = $(e.target);
			var length = $dom.parent().children().length;
			var index = $dom.index();
			if (index == length - 1) {
				return false;
			}
			var post = {
				parentId: $dom.data('id')
			}
			me._ajax('/data/monitor/orgTableData', post, me._paintTable);
			me.parentId = $dom.data('id');
		},
		_paintIndexLine: function(index) {				//绘制第index行的数据
			var me = this;
			var $tr = $('#mainTable').find('tbody').find('tr').eq(index);
			var $td = $tr.find('td').eq(0);
			$tr.addClass('active').siblings('tr').removeClass('active');

			if (!$td.hasClass('drill_td')) {
				// 请求图表数据
				var orgId = $td.data('org');
				var level = $td.data('leveltype')
				// 标记当前level
				me.level = level;
				var post = {
					orgId: orgId,
					levelType: level
				}
				me._ajax('/data/monitor/orgChartData', post, me._paintChart);
			}else {
				var orgId = $td.data('org');
				var id    = $td.data('id');
				var type  = $tr.parent().data('type').toLowerCase();
				var post  = {
					orgId: orgId
				}
				if (type == 'riderid') {
					post.riderId = id;
					me.level = 140;				// 骑手level人为规定为140
					var url = '/data/monitor/riderChartData';
				}else {
					post.poiId = id;
					me.level = 150;				// 商家level人为规定为150
					var url = '/data/monitor/poiChartData';
				}
				me._ajax(url, post, me._paintChart);
			}

			me._tab();
		},
		_paintChart: function(res, me) {
			var data = res.data;
			var series = me._formatSeries(data.chartData);
			var xTime  = me._formatDate(data.chartAxis);
			var chartSettings = {
				xAxis: {
					title: {text: '时间'},
					categories: xTime
				},
				series: series
			}
			var settings = $.extend({}, chartDefaults, chartSettings);
			oChart = new Highcharts.Chart(settings);
		},
		_chooseTableItem: function(e) {
			var me = this;
			var $dom = $(e.target);
			if ($dom[0].tagName.toLowerCase() == 'td') {		// 确保点击的不是下钻按钮
				var $tr = $dom.parent();
				var index = $tr.index();
				me._paintIndexLine(index);
			}
		},
		_drill: function(e) {
			var me = this;
			var dom = e.target,
				td  = dom.parentNode,
				id  = td.getAttribute('data-org');
//				childSize = Number(td.getAttribute('data-child'));

//			if (childSize == 0 && me.level != 230 && me.level != 330) {
//				alert('没有更下一层数据');
//				return false;
//			}

			// 如果是站点级, 则选择下钻到骑手or商家
			if (me.level == 230 || me.level == 330) {
				me._showDrillMenu(dom, e);
				return false;
			}

			var post = {
				parentId: id
			}
			me._ajax('/data/monitor/orgTableData', post, me._paintTableV2);
			me.parentId = id;
		},
		_showDrillMenu: function(dom, e) {
			$('.choose_drill').removeClass('show');
			var x = e.pageX,
				y = e.pageY;
			var dom = dom.parentNode;
			if (dom.getElementsByTagName('span').length) {
				dom.getElementsByTagName('span')[0].classList.add('show');
				return false;
			}
			var span = document.createElement('span');
			span.className = 'choose_drill show';
			span.innerHTML = '<i class="item item_rider">骑手</i><i class="item item_poi">商家</i><i class="close_icon">&times;</i>';
			dom.appendChild(span);
		},
		_chooseDrill: function(e) {
			var me = this;
			var $dom = $(e.target);
			var id = $dom.parent().parent().data('org');

			var post = {
				parentId: id,
				page: 1,
				pageSize: 200
			}
			var url = '/data/monitor/' + ($dom.hasClass('item_rider') ? 'riderTableData' : 'poiTableData');
			me._ajax(url, post, me._paintTable);
			me.parentId = id;
		},
		_closeDrillTip: function(e) {
			var dom = e.target;
			dom.parentNode.classList.remove('show');
		},
		_tab: function() {											// 初始化Tab栏, 根据第一次请求返回的数据确定最高层级
			var me    = this;
			var reg   = /(\d)\d$/;
			var level = me.level;
			var	index = level.toString().match(reg)[1];
			var $tab  = $('#tab');
			if (!me.isInit) {
				var oFrag = document.createDocumentFragment();
				for (var i = index, n = me.tabList.length; i < n; i ++) {
					if (me.tabList[i].name) {
						var span = document.createElement('span');
						span.className = 'tab_item';
						span.setAttribute('data-level', me.tabList[i].level);
						(i == index) && (span.classList.add('on'));
						span.innerHTML = me.tabList[i].name;
						oFrag.appendChild(span);
					}
				}
				$tab[0].appendChild(oFrag)
				me.isInit = true;
			}else {
				var $item = $tab.find('.tab_item[data-level=' + level + ']');
				$item.addClass('on').siblings().removeClass('on');
			}
		},
		_search: function() {
			var me = this;

			var type = $('#mainTable').children('tbody').data('type').toLowerCase();
			var post = {
				parentId: me.parentId
			}
			if (type == 'org') {
				var url = '/data/monitor/orgTableData';
			}else if (type == 'riderid') {
				var url = '/data/monitor/riderTableData';
				post.page = 1;
				post.pageSize = 200;
			}else {
				var url = '/data/monitor/poiTableData';
				post.page = 1;
				post.pageSize = 200;
			}

			me._ajax(url, post, me._paintTable);
		},
		_searchAll: function(e) {									// 点击Tab查询对应层级全部信息
			var me = this;
			var $dom = $(e.target);

			var level = $dom.data('level');
			me.level = level;

			if (level != 140 && level != 150) {
				me.parentId = (-1 * level);								// parentId为负数查询全部
				var post = {
					parentId: me.parentId
				}
				me._ajax('/data/monitor/orgTableData', post, me._paintTable)
			}else {
				me.parentId = 0;
				var post = {
					parentId: me.parentId,
					page: 1,
					pageSize: 200
				}
				var url = '/data/monitor/' + (level == 140 ? 'riderTableData' : 'poiTableData');
				me._ajax(url, post, me._paintTable);
			}
			$dom.addClass('on').siblings().removeClass('on');
		},
		_select2: function() {
			var me = this;
			var list = me.$dataTable.api().column(0).nodes();
			var html = '';
			html += '<option value="">全部</option>';
			for (var i = 0, n = list.length; i < n; i ++) {
				var td = list[i];
				// var name = td.children[0].innerHTML;
				var name = td.children[0].getAttribute('data-text');
				html += '<option value="' + name + '">' + name + '</option>';
			}
			$('#searchInput')[0].innerHTML = html;
			$('#searchInput').select2({width: '150px'});
		},
		_searchInput: function(e) {
			var me = this;
			var value = e.target.value;
			me.$dataTable.api().column(0).search(value).draw();
			me._paintIndexLine(0);
		},
		_sort: function(e) {
			var me = this;
			var $dom = $(e.target);

			var sort = $dom.parents('td').data('sort');
			var order = $dom.data('sort');

			var type = $('#mainTable').children('tbody').data('type').toLowerCase();
			var url = type == 'riderid' ? '/data/monitor/riderTableData' : '/data/monitor/poiTableData'
			var post = {
				parentId: me.parentId
			}
			post.page = 1;
			post.pageSize = 200;
			post.orderBy = sort + '.' + order;
			me._ajax(url, post, me._paintTable);

		},
		_jump: function(e) {
			var me  = this;
			var $dom = $(e.target);
			var id   = $dom.parent().data('id');
			var orgId = $dom.parent().data('org');
			var name = $dom.text();
			var type = $dom.parents('tbody').data('type').toLowerCase();
			var time = me._getTime();
			var reg = /(^\d{4})(\d{2})(\d{2}$)/
			var stime = time.stime.match(reg).slice(1, 4).join('-');
			var etime = time.etime.match(reg).slice(1, 4).join('-');
			var url = 'http://peisong.meituan.com/partner/statistics/dailyData?orgId=' + orgId + '&';
			url += (type == 'riderid' ? 'riderId=' : 'platformPoiId=');
			url += id + '&';
			url += (type == 'riderid' ? 'riderName=' : 'poiName=');
			url += (name + '&ts=' + stime + '&te=' + etime);
			window.open(url);
		},
		_showExportMenu: function() {
			$('#exportMenu').toggleClass('show');
		},
		_showExportMenu1: function() {
			$('#exportMenu').addClass('show');
		},
		_showExportMenu2: function() {
			$('#exportMenu').removeClass('show');
		},
		_exportTable: function() {
			var me = this;
			var time = me._getTime();
			var type = $('#mainTable').children('tbody').data('type').toLowerCase();
			if (type == 'org') {
				var url = '/data/monitor/orgTableDataExport';
			}else if (type == 'riderid') {
				var url = '/data/monitor/riderTableDataExport';
			}else {
				var url = '/data/monitor/poiTableDataExport';
			}
			url += '?orgType=' + me.orgType + '&parentId=' + me.parentId + '&stime=' + time.stime + '&etime=' + time.etime;
			window.open(url);
		},
		_exportChart: function() {
			var me = this;
			var time = me._getTime();
			var type = $('#mainTable').children('tbody').data('type').toLowerCase();
			var $td = $('#mainTable').find('.active').find('td').eq(0);
			var orgId = $td.data('org');
			var level = $td.data('leveltype');
			var id = $td.data('id');
			if (type == 'org') {
				var url = '/data/monitor/orgChartDataExport?levelType=' + level;
			}else if (type == 'riderid') {
				var url = '/data/monitor/riderChartDataExport?riderId=' + id;
			}else {
				var url = '/data/monitor/poiChartDataExport?poiId=' + id;
			}
			url += '&orgType=' + me.orgType + '&orgId=' + orgId + '&stime=' + time.stime + '&etime=' + time.etime;
			window.open(url);
		},
		_getCookie: function(str) {
			var cookie = document.cookie.replace(/\s/g, '');
			var arr = cookie.split(';');
			for (var i = 0, n = arr.length; i < n; i ++) {
				var key = arr[i].split('=')[0];
				if (key == str) {
					return arr[i].split('=')[1];
				}
			}
		},
		_formatSeries: function(obj) {					//将数据格式化为hightcharts中的数据列格式
			var me = this;
			var res = [];
			for (k in obj) {
				var temp = {};
				temp.name = k;
				temp.data = obj[k];
				temp.yAxis = me._yAxis(obj[k]);

				temp.visible = ($.inArray(k, defaultSeries) < 0) ? false : true;
				res.push(temp);
			}
			return res;
		},
		_yAxis: function(data) {						// 判断数据列对应的Y轴
			if (/%/.test(data[0])) {
				return 0;
			}else {
				var max = Math.max.apply(null, data);
				var res = (max > 1000 && 3) || (max > 100 && 2) || 1;
				return res;
			}
		},
		_formatDate: function(arr) {					// 将时间戳转为YYYY/MM/DD的格式显示在X轴
			var res  = [],
				n    = arr.length,
				date = new Date();
			while (n --) {
				date.setTime(arr[n]);
				var str = date.toLocaleDateString();
				res.unshift(str);
			}
			return res;
		},
		_getTime: function() {
			var time  = $('.reportrange span').html();
			var stime = time.substr(0, 8),
				etime = time.substr(-8);
			return {stime: stime, etime: etime}
		},
		_bind: function() {									// 绑定事件
			var me = this;
			var	context = me,
				events  = me.events,
				$wrap   = me.wrap;

			events && $.each(events, function(k, v) {
				var handler;
				me[v[2]] && (handler = me[v[2]]);

				var ev = v[0] + '.' + k,
					el = v[1];
				$wrap.off(ev).on(ev, el, function(e) {
					handler.apply(context, [e, this]);
				})
			})
		},
		_ajax: function(url, data, callback, type) {		// ajax
			var me = this;
			var type 	 = type || 'POST',
				callback = callback || noop;
			var oLoading = document.getElementById('loadingWrapper');
			// 每次查询检查时间条件
			var otime    = me._getTime();
			data.stime   = otime.stime;
			data.etime 	 = otime.etime;
			data.orgType = me.orgType;
			$.ajax({
				url: url,
				type: type,
				dataType: 'json',
				data: data,
				success: function(res) {
                    console.log(res);
                    oLoading.classList.remove('show');
					callback(res, me);
				},
				beforeSend: function() {
					oLoading.classList.add('show');
				},
				error: function() {
					// alert('请求{' + url + '}出错!')
					oLoading.classList.remove('show');
				}
			})
		}
	}
	banma._init();
})