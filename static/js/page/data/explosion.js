;$(function() {
	var bm = {
		wrap: $('body'),
		citys: {},
		timeLine: [],
		isInit: false,
		count: 0,
		events: [
			['change', '#orgType', '_orgTypeChange']
			,['change', '#city', '_cityChange']
			,['click', '#searchBtn', '_search']
			,['click', '#exportAllBtn', '_export']
		],
		_init: function() {
			var me = this;

			me._getCityInfo(-1);
			me._datepickerInit();

			me._bind();
		},
		_orgTypeChange: function(e) {
			var me = this;
			var orgType = $(e.target).val();
			me._getCityInfo(orgType)
		},
		_getCityInfo: function(orgType) {				// 获取城市列表
			var me = this;
			var callback = function(res) {
				me.citys = res;

				dayData.citys = res;
				dayData._init();

				var html = '<option value=\"-1\">全部</option>';
				for (k in res) {
					res[k] && (html += '<option value=\"' + res[k].cityId + '\">' + res[k].cityName + '</option>');
				}
				$('#city').html(html).select2({width: 100})
				me._setCityArea(-1)
			}
			me._ajax('get', '/data/monitor/explosion/cityAreaInfo', {orgType: orgType}, callback)
		},
		_cityChange: function(e) {						// 改变城市选择
			var me = this;
			var cityId = $(e.target).val();
			me._setCityArea(cityId);
		},
		_setCityArea: function(id) {					// 设置城市区域列表
			var me = this;
			var html = '<option value=\"-1\">全部</option>';
			if (id != -1) {
				var areas = me.citys[id].areaList;
				for (var i = 0, n = areas.length; i < n; i ++) {
					html += '<option value=\"' + areas[i].areaId + '\">' + areas[i].areaName + '</option>';
				}
			}

			$('#area').html(html).select2({width: 120});

			if (!me.isInit) {
				me._search();
				me.isInit = true;
			}
		},
		_search: function() {
			if ($('#searchBtn').hasClass('disabled')) {
				return false;
			}
			var me = this;
			var form = me._formatForm();

			me.count = 0;
			$('#searchBtn').addClass('disabled');

			me._getTimesInfo(form);		// 获取爆单次数、时长
			me._getWaybillInfo();		// 获取准时、超时单量
			me._getOrgInfo(form);		// 获取爆单、非爆单站点
		},
		_getTimesInfo: function(data) {
			var me = this;
			var callback = function(res) {
				if (me.count == 3) {
					$('#searchBtn').removeClass('disabled');
				}else {
					me.count ++;
				}
				var settings = {
					colors: ['#058DC7', '#FF9655'],
					chart: {backgroundColor: '#F9F9F9',},
					title: {text: '爆单次数+持续时长趋势图'},
					credits: {enabled: false,},
					xAxis: {categories: res.dateAxis},
					yAxis: [
						{
							title: {text: null},
							floor: 0
						},
						{
							title: {text: null},
							floor: 0,
							opposite: true,
						}
					],
					series: [
						{
							name: '爆单次数',
							type: 'column',
							yAxis: 0,
							data: []
						},
						{
							name: '持续时长(分钟)',
							yAxis: 1,
							data: []
						}
					]
				}
				var list = res.dataList;
				for (var i = 0, n = list.length; i < n; i ++) {
					settings.series[0].data.push(list[i].explosionTimes);
					settings.series[1].data.push(list[i].durationMin);
				}
				$('#chartTime').highcharts(settings)
			}
			me._ajax('get', '/data/monitor/explosion/timesInfo', data, callback)
		},
		_getWaybillInfo: function() {
			var me = this;
			var form = me._formatForm(),
				time = me._formatTime();
			if (time.startTime >= time.endTime || time.endTime > 2400) {
				alert('请选择正确的时间范围');
				return false;
			}
			var data = $.extend({}, form, time);
			var callback = function(res) {
				if (me.count == 3) {
					$('#searchBtn').removeClass('disabled');
				}else {
					me.count ++;
				}
				var settings = {
					colors: ['#058DC7', '#FF9655', '#50B432', '#7CB5EC'],
					credits: {enabled: false},
					chart: {type: 'pie',backgroundColor: '#F9F9F9',},
			        title: {text: '骑手送达时间分布图'},
			        tooltip: {pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'},
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                dataLabels: {
			                    enabled: true,
			                    format: '<b>{point.name}</b>: {point.y}单',
			                }
			            }
			        },
			        series: [
			        	{
			        		name: '送达时间分布',
			        		data: [
			        			{
			        				name: '完成订单准时单量',
			        				y: res.waybillArrivedOntimeCnt
			        			},
			        			{
			        				name: '完成订单瑕疵单量',
			        				y: res.waybillArrivedDefectiveCnt
			        			},
			        			{
			        				name: '完成订单超时单量',
			        				y: res.waybillArrivedIncidentCnt
			        			},
			        			{
			        				name: '完成订单严重超时单量',
			        				y: res.waybillArrivedAccidentCnt
			        			}
			        		]
			        	}
			        ]
				}
				$('#chartWaybill').highcharts(settings)
			}
			me._ajax('get', '/data/monitor/explosion/waybillInfo', data, callback)
		},
		_getOrgInfo: function(data) {
			var me = this;
			var callback = function(res) {
				if (me.count == 3) {
					$('#searchBtn').removeClass('disabled');
				}else {
					me.count ++;
				}
				var settings = {
					colors: ['#058DC7', '#FF9655', '#50B432'],
					chart: {
						backgroundColor: '#F9F9F9',
						type: 'column',
					},
					title: {text: '爆单站点趋势图'},
					credits: {enabled: false},
					xAxis: {categories: res.dateAxis},
					yAxis: [
						{
							title: {text: '数值'},
							floor: 0
						},
						{
							title: {text: '比值'},
							opposite: true,
							floor: 0,
						}
					],
					plotOptions: {
						column: {
							stacking: 'normal',
							dataLabels: {
								enabled: true
							}
						}
					},
					tooltip: {shared: true,},
					series: [
						{
							name: '爆单站点数',
							data: []
						},
						{
							name: '非爆单站点数',
							data: []
						},
						{
							name: '爆单站点与非爆单站点比例',
							data: [],
							type: 'line',
							yAxis: 1,
						}
					]
				}
				var list = res.dataList;
				for (var i = 0, n = list.length; i < n; i ++) {
					settings.series[0].data.push(list[i].explosionOrgCount);
					settings.series[1].data.push(list[i].normalOrgCount);
					var ratio = list[i].orgRatio;
					ratio = ratio ? parseFloat(ratio) : null;
					settings.series[2].data.push(ratio);
				}
				$('#chartOrg').highcharts(settings);
			}
			me._ajax('get', '/data/monitor/explosion/orgInfo', data, callback)
		},
		_export: function() {			// 导出
			var me = this;
			var form = me._formatForm();

			var href = '/data/monitor/explosion/daySumAreaExport?startDate=' + form.startDate + '&endDate=' + form.endDate;
			window.open(href);
		},
		_formatForm: function() {
			var timeStr = $('#datepicker').val().split(/\s/);

			var data = {
				orgType: $('#orgType').val(),
				cityId: $('#city').val(),
				areaId: $('#area').val(),
				status: $('#grade').val(),
				startDate: timeStr[0].split(/\//).join(''),
				endDate: timeStr[2].split(/\//).join(''),
			}

			return data;
		},
		_formatTime: function() {
			var time = {
				startTime: $('#sHour').val() + $('#sMin').val(),
				endTime: $('#eHour').val() + $('#eMin').val()
			}
			return time;
		},
		_datepickerInit: function() {
			var me = this;
			var week = moment().subtract(7, 'days').format('YYYY/MM/DD') + ' - ' + moment().subtract(1, 'days').format('YYYY/MM/DD');
			var $datepicker = $('#datepicker');
			$datepicker.val(week);
			$datepicker.daterangepicker({
		        startDate: moment().subtract(7, 'days').format('YYYY/MM/DD'),
		        endDate: moment().subtract(1, 'days').format('YYYY/MM/DD'),
		        minDate: moment().subtract(30, 'days'),			// 最早不能超过30天前
		        maxDate: moment().subtract(0, 'days'),			// 未来日期不可选
		        dateLimit: {									// 时间间隔不超过6天（一周）
		        	'days': 6
		        },
				format: 'YYYY/MM/DD',
				locale: {
		            applyLabel: '应用',
		            cancelLabel: '取消',
		            fromLabel: '从',
		            toLabel: '到',
		            customRangeLabel: '定制',
		            daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
		            monthNames: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
		            firstDay: 1
		        }
			}, function(start, end, label) {
				$datepicker.val(start.format('YYYY/MM/DD') + ' - ' + end.format('YYYY/MM/DD'));
			})
		},
		_bind: function() {									// 绑定事件
			var me = this;
			var	context = me,
				events  = me.events,
				$wrap   = me.wrap;

			events && $.each(events, function(k, v) {
				var handler;
				me[v[2]] && (handler = me[v[2]]);

				var ev = v[0] + '.' + k,
					el = v[1];
				$wrap.off(ev).on(ev, el, function(e) {
					handler.apply(context, [e, this]);
				})
			})
		},
		_ajax: function(type, url, data, callback) {
			var me = this;
			var callback = callback || function() {};
			$.ajax({
				url: url,
				type: type,
				dataType: 'json',
				data: data,
				success: function(res) {
					$('#searchBtn').removeClass('disabled');
					if (res.code == 0) {
						callback(res.data);
					}else {
						alert(res.msg);
					}
				},
				beforeSend: function() {
					$('#searchBtn').addClass('disabled')
				}
			})
		}
	}
	// 时间选择
	var timepicker = {
		wrap: $('#timepicker'),
		$timepicker: $('#timepicker'),
		$sHour: $('#sHour'),
		$sMin: $('#sMin'),
		$eHour: $('#eHour'),
		$eMin: $('#eMin'),
		_init: function() {									// 初始化时间选择
			var me = this;
			// todo
			var $selects = me.$timepicker.find('select');
			$selects.each(function() {
				var type = $(this).data('type');
				var id   = $(this).siblings('input').attr('id');
				if (type == 'hour') {
					var oFrag = document.createDocumentFragment();
					for (var i = 0; i < 25; i ++) {
						var option = document.createElement('option');
						option.innerHTML =
						option.value 	 = i < 10 ? '0' + i : i;
						(id == 'eHour' && i == 24) && (option.selected = true);
						oFrag.appendChild(option);
					}
					$(this).html($(oFrag))
				}else {
					var html = '';
					for (var i = 0; i < 6; i ++) {
						html += '<option value=\"' + (i + '0') + '\">' + (i + '0') + '</option>';
					}
					$(this).html(html);
				}
			})
			me._render(0, 0, 24, 0);

			me._bind()
		},
		_render: function(sh, sm, eh, em) {
			var me = this;
			var shLimit, smLimit, ehLimit, emLimit;
			if (sh < eh) {
				if (em == 0) {			// em为0，则sh与eh不能相等
					shLimit = eh;
				}else {
					shLimit = eh + 1;
				}
				if (sm == 50) {			// sm为50，则sh与eh不能想等
					ehLimit = sh;
				}else {
					ehLimit = sh - 1;
				}
				if (eh == 24) {
					emLimit = 0;
				}else {
					emLimit = 50;
				}
			}
			me._renderStartHours(shLimit);
			me._renderEndHours(ehLimit);
		},
		_renderStartHours: function(limit) {
			var me = this;
			var $select = me.$sHour.siblings('select');
			$select.find('option').each(function(index) {
				if (index >= limit) {
					$(this).prop('disabled', true);
				}else {
					$(this).prop('disabled', false)
				}
			})
		},
		_renderEndHours: function(limit) {
			var me = this;
			var $select = me.$eHour.siblings('select');
			$select.find('option').each(function(index) {
				if (index <= limit) {
					$(this).prop('disabled', true);
				}else {
					$(this).prop('disabled', false)
				}
			})
		},
		_timeSelect: function(e) {							// 时间选择
			var me = this;
			var $select = $(e.target);
			var time = $select.val();
			$select.siblings('input').val(time);
			$select.blur();
			var sh = parseInt(me.$sHour.val()),
				sm = parseInt(me.$sMin.val()),
				eh = parseInt(me.$eHour.val()),
				em = parseInt(me.$eMin.val());
			me._render(sh, sm, eh, em);
			bm._getWaybillInfo();
		},
		_bind: function() {									// 绑定事件
			var me = this;
			$('.time_select').on('change', function(e) {
				me._timeSelect(e);
			})
		},
	}
	// 单日数据查询
	var dayData = {
		wrap: $('#dayData'),
		citys: {},
		isInit: false,
		events: [
			['change', '#dayDataOrgType', '_orgTypeChange']
			,['change', '#dayDataCity', '_cityChange']
			,['click', '#searchDayData', '_search']
			,['click', '#exportDayData', '_export']
		],
		_init: function() {
			var me = this;

			me._datepickerInit();
			me._renderCityList();
			me._bind();
		},
		_orgTypeChange: function(e) {
			var me = this;
			var orgType = $(e.target).val();
			me._getCityInfo(orgType)
		},
		_getCityInfo: function(orgType) {				// 获取城市列表
			var me = this;
			var callback = function(res) {
				me.citys = res;
				me._renderCityList();
			}
			me._ajax('get', '/data/monitor/explosion/cityAreaInfo', {orgType: orgType}, callback)
		},
		_renderCityList: function() {
			var me = this;
			var list = me.citys;
			var html = '';
			for (k in list) {
				list[k] && (html += '<option value=\"' + list[k].cityId + '\">' + list[k].cityName + '</option>');
			}
			$('#dayDataCity').html(html).select2({width: 100})
			var firstCity = $('#dayDataCity').find('option').eq(0).val();
			me._setCityArea(firstCity)
		},
		_cityChange: function(e) {						// 改变城市选择
			var me = this;
			var cityId = $(e.target).val();
			me._setCityArea(cityId);
		},
		_setCityArea: function(id) {					// 设置城市区域列表
			var me = this;
			var html = '';
			var areas = me.citys[id].areaList;
			for (var i = 0, n = areas.length; i < n; i ++) {
				html += '<option value=\"' + areas[i].areaId + '\">' + areas[i].areaName + '</option>';
			}

			$('#dayDataArea').html(html).select2({width: 120});

			if (!me.isInit) {
				me._search();
				me.isInit = true;
			}
		},
		_search: function() {
			var me = this;
			if ($('#searchDayData').hasClass('disabled')) {
				return false;
			}
			var timeStr = $('#datepickerSingle').val().split(/\//).join('');

			var data = {
				orgType: $('#dayDataOrgType').val(),
				cityId: $('#dayDataCity').val(),
				areaId: $('#dayDataArea').val(),
				endDate: timeStr
			}

			$('#searchDayData').addClass('disabled');

			me._getDayTable(data);		// 获取日监表
		},
		_export: function() {			// 导出
			var me = this;

			var timeStr = $('#datepickerSingle').val().split(/\//).join('');

			var form = {
				orgType: $('#dayDataOrgType').val(),
				cityId: $('#dayDataCity').val(),
				areaId: $('#dayDataArea').val(),
				endDate: timeStr
			}

			var str = '';
			for (var k in form) {
				str += k + '=' + form[k] + '&';
			}
			str = str.substring(0, str.length - 1);
			var href = '/data/monitor/explosion/dayTableExport?' + str;
			window.open(href);
		},
		_getDayTable: function(data) {
			var me = this;
			delete data.startDate;

			var callback = function(res) {
				me._renderTable(res.table);
				me._renderChart(res.chart);

			}
			me._ajax('get', '/data/monitor/explosion/dayTable', data, callback);
		},
		_renderTable: function(data) {			// 渲染表格
			if (!data) {
				$('#tableBox').empty();
				$('#fixedHead').empty();
				return false;
			}
			var table = template('J_table_tpl', data);
			$('#tableBox').html(table);
			var $thead = $('#tableBox').find('thead');
			var arr = [];
			$thead.find('th').each(function() {
				var w = $(this).width() + 24;
				arr.push(w);
			})
			var $theadCopy = $thead.clone();
			$theadCopy.find('th').each(function(index) {
				$(this).css({
					minWidth: arr[index] + 'px',
					maxWidth: arr[index] + 'px'
				})
			})
			$('#fixedHead').empty().prepend($theadCopy);
			$('#tableBox').width($('#fixedHead').width() + 2)
		},
		_renderChart: function(data) {
			var defaultSeries = ['档位', '负载', '在岗骑手数', '产生订单超时率'];
			var settings = {
				chart: {backgroundColor: '#F9F9F9'},
				title: {text: '爆单日监表'},
				credits: {enabled: false},
				xAxis: {categories: []},
				yAxis: [
					{
						title: {text: null},
						floor: 0,
					},
					{
						title: {text: null},
						floor: 0,
						opposite: true
					}
				],
				tooltip: {
					shared: true,
					crosshairs: {
						width: 1,
						color: 'orange'
					}
				},
				series: []
			}
			if (!data) {
				$('#chartToday').highcharts(settings);
				return false;
			}
			for (var i = 0, n = data.chartAxis.length; i < n; i ++) {
				settings.xAxis.categories.push(data.chartAxis[i]);
			}
			for (var k in data.chartData) {
				var obj = {
					name: k,
					data: data.chartData[k],
					yAxis: Math.max.apply(Math, data.chartData[k]) > 100 ? 1 : 0,
					visible: ($.inArray(k, defaultSeries) < 0) ? false : true
				}
				settings.series.push(obj);
			}
			$('#chartToday').highcharts(settings);
		},
		_datepickerInit: function() {
			var me = this;
			var week = moment().subtract(1, 'days').format('YYYY/MM/DD');
			var $datepicker = $('#datepickerSingle');
			$datepicker.val(week);
			$datepicker.daterangepicker({
				singleDatePicker: true,
		        startDate: moment().subtract(1, 'days').format('YYYY/MM/DD'),
		        minDate: moment().subtract(30, 'days'),			// 最早不能超过30天前
		        maxDate: moment().subtract(0, 'days'),			// 未来日期不可选
				format: 'YYYY/MM/DD',
				locale: {
		            applyLabel: '应用',
		            cancelLabel: '取消',
		            fromLabel: '从',
		            toLabel: '到',
		            customRangeLabel: '定制',
		            daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
		            monthNames: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
		            firstDay: 1
		        }
			}, function(start, end, label) {
				$datepicker.val(start.format('YYYY/MM/DD'))
			})
		},
		_bind: function() {									// 绑定事件
			var me = this;
			var	context = me,
				events  = me.events,
				$wrap   = me.wrap;

			events && $.each(events, function(k, v) {
				var handler;
				me[v[2]] && (handler = me[v[2]]);

				var ev = v[0] + '.' + k,
					el = v[1];
				$wrap.off(ev).on(ev, el, function(e) {
					handler.apply(context, [e, this]);
				})
			})
		},
		_ajax: function(type, url, data, callback) {
			var me = this;
			var callback = callback || function() {};
			$.ajax({
				url: url,
				type: type,
				dataType: 'json',
				data: data,
				success: function(res) {
					$('#searchDayData').removeClass('disabled');
					if (res.code == 0) {
						callback(res.data);
					}else {
						alert(res.msg);
					}
				},
				beforeSend: function() {
					$('#searchDayData').addClass('disabled')
				}
			})
		}
	}

	timepicker._init();
	bm._init();

})