function search(obj) {
    window.location.href = "/data/mgr/template/list?tbl_id=" + obj.value;
}

function edit(id, name, tips, chart_seq, table_seq, field_name, field_type, mselect_key, select_key,
    sum_mselect_key, sum_select_key, online, tbl_id, isAdd) {
    console.log(id, name, tips, chart_seq, table_seq, field_name, field_type, mselect_key, select_key,
        sum_mselect_key, sum_select_key, online, tbl_id, isAdd);
    $("#myModalLabel").text("修改");
    $("#id").val(id).attr("disabled", true);
    $("#name").val(name);
    $("#tips").val(tips);
    $("#chart_seq").val(chart_seq);
    $("#table_seq").val(table_seq);
    $("#field_name").val(field_name);
    $("#field_type").val(field_type);
    $("#mselect_key").val(mselect_key);
    $("#select_key").val(select_key);
    $("#sum_mselect_key").val(sum_mselect_key);
    $("#sum_select_key").val(sum_mselect_key);
    $("#online").val(online);
    $("#tbl_id").val(tbl_id);
    $("#isAdd").val(isAdd);

    $("#myModal").modal();
}

function add(tbl_id) {
    $("#myModalLabel").text("新增");
    $("#id").val('');
    $("#name").val('');
    $("#tips").val('');
    $("#chart_seq").val('');
    $("#table_seq").val('');
    $("#field_name").val('');
    $("#field_type").val('');
    $("#mselect_key").val('');
    $("#select_key").val('');
    $("#sum_mselect_key").val('');
    $("#sum_select_key").val('');
    $("#online").val('');
    $("#tbl_id").val(tbl_id);
    $("#isAdd").val(0);
    $("#myModal").modal();
    /** isAdd=0表示新增，1表示更新 */
}

function deleteById(id, tbl_id) {
    if (confirm("确认要删除该条数据么，请慎重！")) {
        console.log(id, tbl_id);
        var url = "/data/mgr/template/delete";
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                "id": id,
                "tbl_id": tbl_id
            },
            success: function(result) {
                window.location.href = "/data/mgr/template/list?tbl_id=" + result.data;
            }
        });
    }
}

function save() {
    if ($("#name").val() == '' || $("#field_name").val() == '' || $("#field_type").val() == '' || $("#online").val() == '') {
        alert("指标中文名，字段名，字段类型，发布状态四个字段必填！")
        return;
    }
    if ($("#isAdd").val() == 1) {
        var url = "/data/mgr/template/update";
    } else {
        var url = "/data/mgr/template/add";
    }
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            "id": $("#id").val(),
            "name": $("#name").val(),
            "tips": $("#tips").val(),
            "chart_seq": $("#chart_seq").val(),
            "table_seq": $("#table_seq").val(),
            "field_name": $("#field_name").val(),
            "field_type": $("#field_type").val(),
            "mselect_key": $("#mselect_key").val(),
            "select_key": $("#select_key").val(),
            "sum_mselect_key": $("#sum_mselect_key").val(),
            "sum_select_key": $("#sum_select_key").val(),
            "online": $("#online").val(),
            "tbl_id": $("#tbl_id").val()
        },
        success: function(result) {
            $("#myModal").modal("hide");
            window.location.href = "/data/mgr/template/list?tbl_id=" + result.data;
        }
    });
}

$(function() {
    $('#tb_key').DataTable({
        scrollX: true,
        paging: false,
        columns: [{
            "width": "60px"
        }, {
            "width": "60px"
        }, {
            "width": "100px"
        }, {
            "width": "150px"
        }, {
            "width": "80px"
        }, {
            "width": "80px"
        }, {
            "width": "120px"
        }, {
            "width": "100px"
        }, {
            "width": "150px"
        }, {
            "width": "150px"
        }, {
            "width": "80px"
        }, {
            "width": "80px"
        }, {
            "width": "60px"
        }],
        scrollY: 'auto',
        orderClasses: true,
        deferRender: true,
        order: [], // 默认不排序
        iDisplayLength: 20, // 默认显示20条
        autoWidth: true,
        aoColumnDefs: [ { "bSortable": false, "aTargets": [ 2,3,6,7,8,9,10,11,12 ] }],
        lengthMenu: [
            [20, 50, -1],
            [20, 50, "全部"]
        ], // 规定表格每页显示多少条        
        language: {
            search: '搜索：',
            searchPlaceholder: '输入关键词',
            lengthMenu: '每页显示 _MENU_ 条',
            infoFiltered: "",
            paginate: {
                next: '下一页',
                previous: '上一页'
            },
            zeroRecords: "没有搜索到相关记录"
        }
    });
    $('#save').click(save);
});