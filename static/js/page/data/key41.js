;$(function() {
	//$("#tab .tab_item").removeClass("on");
	$("#tab").html('<span class="tab_item power" data-level="400" style="display:none">总部</span>\
		<span class="tab_item power power_city" data-level="419" style="display:none">物理城市</span>\
		<span class="tab_item power power_orgcity" data-level="420" style="display:none">众包城市</span>\
		<span class="tab_item" data-level="430">站点</span>\
		<span class="tab_item" data-level="0">骑手</span>\
		<span class="tab_item" data-level="0">商家</span>');
	var noop = function() {};
	// 默认显示的数据列
	var defaultSeries = ['推单数（运单）','完成订单数', '配送订单准时率'];
	// highcharts
	var oChart;
	var chartDefaults = {
		title: {text: '数据监控'},
		credits:{ enabled:false },
		chart: {
			renderTo: 'highcharts',
			type: 'spline',
			events: {}
		},
		xAxis: {
			title: {text: '时间'},
			categories: []
		},
		yAxis: [
			{		// yAxis: 0, 百分比维度
				title: {text: '', style: {color: '#333'}},
				labels: {
					style: {
						color: '#333'
					},
					formatter: function() {
						return this.value + '%';
					}
				}
			},
			{		// yAxis: 1, 小数维度, 1~100
				title: {text: '', style: {color: '#E0850D'}},
				labels: {
					style: {
						color: '#E0850D'
					}
				}
			},
			{		// yAxis: 2, 大数量级, 1~1k
				title: {text: '', style: {color: '#0D08CC'}},
				labels: {
					style: {
						color: '#0D08CC'
					}
				},
				opposite: true
			},
			{		// yAxis: 3, 超大数量级, 1~1000K
				title: {text: '', style: {color: '#059C1A'}},
				labels: {
					style: {
						color: '#059C1A'
					}
				},
				opposite: true
			}
		],
		series: [],
		tooltip: {
			shared: true,
			crosshairs: {
				width: 1,
				color: 'orange'
			},
			headerFormat: '<b>{point.x}</b><br/>'
		},
		plotOptions: {
			series: {
				events: {
					legendItemClick: function() {
						var name = this.name;
						var visible = this.visible;
						if (visible) {		// 当前该数据列是可见的，即点击是为了隐藏数据列。故将对应name从默认显示中移除
							var index = $.inArray(name, defaultSeries);
							defaultSeries.splice(index, 1);
						}else {
							defaultSeries.push(name);
						}
					}
				}
			}
		}
	};
	// dataTable
	$.extend($.fn.dataTableExt.oSort, {						// datatables中文排序
	    "chinese-string-asc" : function (s1, s2) {
	        return s1.localeCompare(s2);
	    },
	    "chinese-string-desc" : function (s1, s2) {
	        return s2.localeCompare(s1);
	    }
	});
	// extend dataTable default settings
	$.extend($.fn.dataTable.defaults, {
		scrollX: true,
		scrollY: 'auto',
		paging: true,										// 分页
		orderClasses: true,
		deferRender: true,
		order: [],											// 默认不排序
		iDisplayLength: 10,									// 默认显示10条
		lengthMenu: [ [10, 20, 50, -1], [10, 20, 50, "全部"] ],		// 规定表格每页显示多少条
		language: {
			search: '搜索',
			searchPlaceholder: '输入关键词',
			lengthMenu: '每页显示 _MENU_ 条',
			infoFiltered: "",
			paginate: {
				next: '下一页',
				previous: '上一页'
			}
		},
		columnDefs: [										// 按照中文字符排序
	       { type: 'chinese-string', targets: 0 }
	    ]
	});
	// banma
	var banma = {
		data: {},//存放datatable数据源
		dataRes : [],//存放返回值
		type:"store",//商家需要单独处理
		page : 1,
		pageSize :10,
		groupDef: [
				{group:"全部指标",list:[]},
				{group:"核心指标",list:[7,8,11,12,15,19,21,22,25,26,35,37,38,39,40,43,45,51,54,57,58,61,85,98,100,101,102,103]},
				{group:"单量",list:[7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]},
				{group:"商家",list:[22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]},
				{group:"骑手",list:[37,38,39,40,41,42,43,44]},
				{group:"用户体验",list:[45,46,47,48,49,50,51,52,53,54,55,56,57,58,61,85,86,87,94,95,96,97]},
				{group:"商户体验",list:[62,63,66,88,89,90,98,99,100,101,102,103]},
				{group:"活动补贴",list:[91,92,93]}
				
			],
		wrap: $('#main-container'),
		events: [
			['click','#tabGroup .tab_item','_showGroupTable'],
			['click', '#test tr', '_chooseTableItem'],			// 点击表格单元行将对应行数据绘制为highcharts
			['mouseover', '#exportBtn', '_showExportMenu1'],	// 导出
			['mouseout', '#exportBtn', '_showExportMenu2'],		// 导出
			['click', '.export_table', '_exportTable'],				// 导出表格
			['click', '.export_chart', '_exportChart'],				// 导出图表
			['click', '#tab .tab_item', '_tab'],					// 点击Tab栏查询全部
			['click', '#searchBtn', '_search'],						// 查询
			['mouseover', '.icon_tip', '_showTip'],
			['mouseout', '.icon_tip', '_removeTip']
		],
		$dataTable: null,
		orgType: window.orgType,
		parentId: 0,
		level: 400,

		_init: function() {
			var me = this;
			me._initForm();
			me._bind();

		},
		_initForm: function() {
			var me = this;
			me.parentId = me.level * -1;
			var post = {
				parentId: 0,
				version:41
			}
			me._ajax('/data/monitor/orgTableData', post, me._paintTable);
		},
		_resetMenu:function(){
			/*var type = $("#tab .on").text();
			switch(type){
				case "商家":
					$("#tabGroup").hide();
				break;
				default:
					$("#tabGroup").show();
					$("#tabGroup .tab_item").removeClass("on");
					$("#tabGroup .tab_item").eq(1).addClass("on");
				break;
			}*/

		},
		_paint:function(data,columns,columnDefs,tips,callback){
			var me = this;
			var index = 0;
			banma.data = {};
			for(var j=0;j<data.tableBody.length;j++){
				if(j==0)
					index = data.tableBody[j][0];
				banma.data[data.tableBody[j][0]] = data.tableBody[j];
			}

			$("#tableArea").html("<table id='test'></table>");
			var table = $('#test').DataTable( {
				data: data.tableBody,
			        columns: columns,
				    retrieve: true,
				    "columnDefs": columnDefs,
			        "initComplete": function () {
			            $("#test tbody tr").eq(0).addClass("active");
			            $("#tableArea th").each(function(i){
			            	if(tips[i]!="")
			            		$(this).attr("data-text",tips[i]).addClass("tip_icon").append('&nbsp;<i class="icon_tip fa fa-question-circle fa-lg"></i>');
			            
			            });

			        }
				} );
			$("#tableArea").resize();//字段小的时候可以能样式变乱，修复一下

			if(callback!=undefined){
				callback(index,me);
			}

		},
		_paintTable: function(res, me, groupDefIdx) {
			banma.type  = "other";
			me._resetMenu();
			var groupDefIdx = groupDefIdx||0;
			var paintLine = true;
			if(res==undefined)
				paintLine = false;
			res = res || banma.dataRes;

			//根据返回数据判断选中的标签
			switch(res.data.levelType){
				case 400:
					$("#tab .tab_item").eq(0).addClass("on");
					break;
				case 419:
					$("#tab .tab_item").eq(1).addClass("on");
					break;
				case 420:
					$("#tab .tab_item").eq(2).addClass("on");
					break;
				case 430:
					$("#tab .tab_item").eq(3).addClass("on");
					break;
				default:
					break;
			}

			//根据返回数据判断是否显示总部菜单
			switch(res.data.roleAuth){
				case 400:
					$("#tab .power").show();
					break;
				case 419:
					$("#tab .power_city").show();
					$("#tab .power_orgcity").show();
					break;
				case 420:
					$("#tab .power_orgcity").show();
					break;
				default:
					break;
			}

			me.level = res.data.levelType;
			banma.dataRes = res;
			var data = res.data;
			var head = data.tableHead;
			
			
			var columnDefs = [
				{"targets": [ 0 ], "visible": false,"searchable": false},
				{"targets": [ 1 ], "visible": false,"searchable": false},
				{"targets": [ 2 ], "visible": false,"searchable": false},
				{"targets": [ 3 ], "visible": false,"searchable": false},
			    {"targets": [ 4 ],"render" : function(text,visible,list){return "<b data-orgid='"+list[0]+"'>"+text+"</b>";}}
			    ];

			var columns = [];
			var tips = [];
			for(var i=0;i<data.tableHead.length;i++){
				columns.push({title:data.tableHead[i]});
				if(i>=4)
					tips.push(data.tableTips[i]);
				if(banma.groupDef[groupDefIdx].list.length>0){
					if(i>4){
						if($.inArray(i,banma.groupDef[groupDefIdx].list)==-1){
							columnDefs.push({"targets": [ i ], "visible": false,"searchable": false});
						}
						
						
					}
				}
			}
			var call;
			if(paintLine)
				call = me._paintIndexLine;

			me._paint(data,columns,columnDefs,tips,call);
			
		},
		_paintTableStore: function(res, me) {
			banma.type = "store";
			me._resetMenu();
			var data = res.data;
			var head = data.tableHead;
			var title = [];
			for (var i = 0, n = head.length; i < n; i ++) {
				title.push({head: head[i]});
			}
			// 要绘制的table数据
			var table = {
				title: title,
				body:  data.tableBody
			}
			
			var columnDefs = [
				{"targets": [ 0 ], "visible": false,"searchable": false},
				{"targets": [ 1 ], "visible": false,"searchable": false},
			    //{"targets": [ 2 ],"render" : function(text,visible,list){return "<a target='_blank' href='"+getUrl(list[0],list[1],list[2])+"'><b data-orgid='"+list[1]+"'>"+text+"</b></a>";}}
			     {"targets": [ 2 ],"render" : function(text,visible,list){return "<b data-orgid='"+list[1]+"'>"+text+"</b>";}}
			    ];

			var columns = [];
			var tips = [];
			for(var i=0;i<data.tableHead.length;i++){
				columns.push({title:data.tableHead[i]});
				if(i>=2&&data.tableTips!=undefined){
					tips.push(data.tableTips[i]);
				}
			}

			
			var index = 0;
			banma.data = {};
			for(var j=0;j<data.tableBody.length;j++){
				if(j==0)
					index = data.tableBody[j][1];
				banma.data[data.tableBody[j][1]] = data.tableBody[j];
			}

			var buildPage = function(currentPage,pageTotal){
				var split = 5;
				var pageHtml = '<a class="paginate_button previous" data-page="'+(currentPage-1==0?currentPage:(currentPage-1))+'">上一页</a>';
				var getPageHtml = function(i){
					return '<span><a class="paginate_button '+(currentPage==i?'current':'')+'" data-page="'+i+'">'+i+'</a></span>';
				}
				var preHtml = "";
				for(var i = currentPage; i >= (currentPage-split>1?(currentPage-split):1); i--){
					preHtml = getPageHtml(i)+preHtml;
				}
				pageHtml = pageHtml + preHtml;
				for(var i=currentPage+1; i<= (currentPage+split>pageTotal?pageTotal:(currentPage+split)); i++){
					pageHtml+= getPageHtml(i);
				}
				pageHtml += '<a class="paginate_button previous" data-page="'+(currentPage+1>pageTotal?pageTotal:(currentPage+1))+'">下一页</a>';
				$("#test_wrapper").append('<div class="dataTables_paginate paging_simple_numbers">'+pageHtml+'</div>');
			}


			$("#tableArea").html("<table id='test'></table>");

			var table = $('#test').DataTable( {
				ordering: false,
				searching:false,
				paging:false,
				lengthMenu : [],
				data: data.tableBody,
			        columns: columns,
				    retrieve: true,
				    "columnDefs": columnDefs,
			        "initComplete": function () {

			        	$("#test_wrapper").prepend('<div class="dataTables_filter"><label>搜索<input type="search" class="" placeholder="输入关键字回车搜索" value="'+(banma.keyword==undefined?'':banma.keyword)+'" aria-controls="test"></label></div>').prepend('<div class="dataTables_length"><label>每页显示 <select name="test_length" aria-controls="test" class=""><option value="10">10</option><option value="20">20</option><option value="50">50</option></select> 条</label></div>');

			        	$(".dataTables_length select").val(banma.pageSize).change(function(){
			        		banma.pageSize = $(this).val();
			        		$("#searchBtn").trigger("click");
			        	});


			        	$(".dataTables_filter").keyup(function(e) { 
					       if(e.which == 13) {
					       		banma.orderIndex = undefined;
					       		banma.orderInfo = undefined;
					       		banma.page = 1;
					       		banma.keyword = $(".dataTables_filter input").val();

					   			me.parentId = me.level * -1;
								var post = {
									parentId: me.parentId,
									version:41,
									page : 1,
									pageSize:banma.pageSize,
									searchLike:banma.keyword
								}
								me._ajax('/data/monitor/poiTableData', post, me._paintTableStore);
					       }  
					   }); 

			        	$(".dataTables_info").text("总共"+data.total+"条数据 共"+data.pageCount+"页 当前显示第"+banma.page+"页");
			        	buildPage(banma.page,data.pageCount);
			        	$(".dataTables_paginate").on("click",".paginate_button",function(){
			        			banma.page = parseInt($(this).data("page"));
			        			me.parentId = me.level * -1;
								var post = {
									parentId: me.parentId,
									version:41,
									page : banma.page,
									pageSize:banma.pageSize,
									orderBy:banma.order
								}
								if(banma.keyword!=undefined)
									post.searchLike = banma.keyword;
								me._ajax('/data/monitor/poiTableData', post, me._paintTableStore);
			        	});

			            $("#test tbody tr").eq(0).addClass("active");
			           
			            $("#tableArea th").each(function(i){
			            	if(tips[i]!="")
			            		$(this).attr("data-text",tips[i]).attr("data-idx",i).addClass("tip_icon").append('&nbsp;<i class="icon_tip fa fa-question-circle fa-lg"></i>');
			            	else
			            		$(this).attr("data-idx",i);

			            });

			            if(banma.orderIndex!=undefined){
			            	$("#tableArea th").eq(banma.orderIndex).removeClass("sorting").addClass("sorting_"+banma.orderInfo);
			            }

			            $("#tableArea th").removeClass("sorting_disabled").addClass("sorting").click(function(){

							banma.orderInfo= "desc";
							if($(this).hasClass("sorting_desc"))
								banma.orderInfo = "asc";
							banma.orderIndex = $(this).attr("data-idx");
							var index = parseInt($(this).attr("data-idx"))-2;
							banma.page = 1;	
							banma.order = $(this).text().trim()+"."+banma.orderInfo;
							me.parentId = me.level * -1;
							var post = {
								parentId: me.parentId,
								version:41,
								page : 1,
								pageSize:banma.pageSize,
								orderBy:banma.order
							}
							me._ajax('/data/monitor/poiTableData', post, me._paintTableStore);
							
						});

			        }
				} );
			$("#tableArea").resize();//字段小的时候可以能样式变乱，修复一下

			

			me._paintIndexLineStore(index);

			
			
		},
		_paintTableRider: function(res, me) {
			banma.type = "rider";
			me._resetMenu();
			var data = res.data;
			var head = data.tableHead;
			var title = [];
			for (var i = 0, n = head.length; i < n; i ++) {
				title.push({head: head[i]});
			}
			// 要绘制的table数据
			var table = {
				title: title,
				body:  data.tableBody
			}
			
			var columnDefs = [
				{"targets": [ 0 ], "visible": false,"searchable": false},
				{"targets": [ 1 ], "visible": false,"searchable": false},
			    //{"targets": [ 2 ],"render" : function(text,visible,list){return "<a target='_blank' href='"+getUrl(list[0],list[1],list[2])+"'><b data-orgid='"+list[1]+"'>"+text+"</b></a>";}}
			     {"targets": [ 2 ],"render" : function(text,visible,list){return "<b data-orgid='"+list[1]+"'>"+text+"</b>";}}
			    ];

			var columns = [];
			var tips = [];
			for(var i=0;i<data.tableHead.length;i++){
				columns.push({title:data.tableHead[i]});
				if(i>=2&&data.tableTips!=undefined){
					tips.push(data.tableTips[i]);
				}
			}

			
			var index = 0;
			banma.data = {};
			for(var j=0;j<data.tableBody.length;j++){
				if(j==0)
					index = data.tableBody[j][1];
				banma.data[data.tableBody[j][1]] = data.tableBody[j];
			}

			var buildPage = function(currentPage,pageTotal){
				var split = 5;
				var pageHtml = '<a class="paginate_button previous" data-page="'+(currentPage-1==0?currentPage:(currentPage-1))+'">上一页</a>';
				var getPageHtml = function(i){
					return '<span><a class="paginate_button '+(currentPage==i?'current':'')+'" data-page="'+i+'">'+i+'</a></span>';
				}
				var preHtml = "";
				for(var i = currentPage; i >= (currentPage-split>1?(currentPage-split):1); i--){
					preHtml = getPageHtml(i)+preHtml;
				}
				pageHtml = pageHtml + preHtml;
				for(var i=currentPage+1; i<= (currentPage+split>pageTotal?pageTotal:(currentPage+split)); i++){
					pageHtml+= getPageHtml(i);
				}
				pageHtml += '<a class="paginate_button previous" data-page="'+(currentPage+1>pageTotal?pageTotal:(currentPage+1))+'">下一页</a>';
				$("#test_wrapper").append('<div class="dataTables_paginate paging_simple_numbers">'+pageHtml+'</div>');
			}


			$("#tableArea").html("<table id='test'></table>");

			var table = $('#test').DataTable( {
				ordering: false,
				searching:false,
				paging:false,
				lengthMenu : [],
				data: data.tableBody,
			        columns: columns,
				    retrieve: true,
				    "columnDefs": columnDefs,
			        "initComplete": function () {

			        	$("#test_wrapper").prepend('<div class="dataTables_filter"><label>搜索<input type="search" class="" placeholder="输入关键字回车搜索" value="'+(banma.keyword==undefined?'':banma.keyword)+'" aria-controls="test"></label></div>').prepend('<div class="dataTables_length"><label>每页显示 <select name="test_length" aria-controls="test" class=""><option value="10">10</option><option value="20">20</option><option value="50">50</option></select> 条</label></div>');

			        	$(".dataTables_length select").val(banma.pageSize).change(function(){
			        		banma.pageSize = $(this).val();
			        		$("#searchBtn").trigger("click");
			        	});


			        	$(".dataTables_filter").keyup(function(e) { 
					       if(e.which == 13) {
					       		banma.orderIndex = undefined;
					       		banma.orderInfo = undefined;
					       		banma.page = 1;
					       		banma.keyword = $(".dataTables_filter input").val();

					   			me.parentId = me.level * -1;
								var post = {
									parentId: me.parentId,
									version:41,
									page : 1,
									pageSize:banma.pageSize,
									searchLike:banma.keyword
								}
								
								me._ajax('/data/monitor/riderTableData', post, me._paintTableRider);
					       }  
					   }); 

			        	$(".dataTables_info").text("总共"+data.total+"条数据 共"+data.pageCount+"页 当前显示第"+banma.page+"页");
			        	buildPage(banma.page,data.pageCount);
			        	$(".dataTables_paginate").on("click",".paginate_button",function(){
			        			banma.page = parseInt($(this).data("page"));
			        			me.parentId = me.level * -1;
								var post = {
									parentId: me.parentId,
									version:41,
									page : banma.page,
									pageSize:banma.pageSize,
									orderBy:banma.order
								}
								if(banma.keyword!=undefined)
									post.searchLike = banma.keyword;
								me._ajax('/data/monitor/riderTableData', post, me._paintTableRider);
			        	});

			            $("#test tbody tr").eq(0).addClass("active");
			           
			            $("#tableArea th").each(function(i){
			            	if(tips[i]!="")
			            		$(this).attr("data-text",tips[i]).attr("data-idx",i).addClass("tip_icon").append('&nbsp;<i class="icon_tip fa fa-question-circle fa-lg"></i>');
			            	else
			            		$(this).attr("data-idx",i);

			            });

			            if(banma.orderIndex!=undefined){
			            	$("#tableArea th").eq(banma.orderIndex).removeClass("sorting").addClass("sorting_"+banma.orderInfo);
			            }

			            $("#tableArea th").removeClass("sorting_disabled").addClass("sorting").click(function(){

							banma.orderInfo= "desc";
							if($(this).hasClass("sorting_desc"))
								banma.orderInfo = "asc";
							banma.orderIndex = $(this).attr("data-idx");
							var index = parseInt($(this).attr("data-idx"))-2;
							banma.page = 1;	
							banma.order = $(this).text().trim()+"."+banma.orderInfo;
							me.parentId = me.level * -1;
							var post = {
								parentId: me.parentId,
								version:41,
								page : 1,
								pageSize:banma.pageSize,
								orderBy:banma.order
							}
							
							me._ajax('/data/monitor/riderTableData', post, me._paintTableRider);
							
						});

			        }
				} );
			$("#tableArea").resize();//字段小的时候可以能样式变乱，修复一下

			

			me._paintIndexLineRider(index);

			
			
		},
		_tdStyle: function() {
			var $a = $('.td_a_btn');
			$a.each(function() {
				var text = $(this).text();
				if (text.length > 14) {
					text = text.substr(0, 14) + '...'
					$(this).text(text)
					$(this).addClass('has_full_name')
				}
			});
		},
		_paintIndexLine: function(index,me) {//绘制第index行的数据
			var me = me||this;
			var post = {
				orgId: banma.data[index][0],
				levelType: banma.data[index][1],
				version:41
			}
			me._ajax('/data/monitor/orgChartData', post, me._paintChart);
		},
		_paintIndexLineRider: function(index) {//绘制第index行的数据
			var me = this;
			var post = {
				orgId: banma.data[index][0],
				riderId: banma.data[index][1],
				version:41
			}
			
			me._ajax('/data/monitor/riderChartData', post, me._paintChart);
		},
		_paintIndexLineStore: function(index) {//绘制第index行的数据
			var me = this;
			var post = {
				orgId: banma.data[index][0],
				poiId: banma.data[index][1],
				version:41
			}
			me._ajax('/data/monitor/poiChartData', post, me._paintChart);
		},
		_paintChart: function(res, me) {
			var data = res.data;
			var series = me._formatSeries(data.chartData);
			var xTime  = me._formatDate(data.chartAxis);
			var chartSettings = {
				xAxis: {
					title: {text: '时间'},
					categories: xTime
				},
				series: series
			}
			var settings = $.extend({}, chartDefaults, chartSettings);
			oChart = new Highcharts.Chart(settings);
		},
		_chooseTableItem: function(e) {
			var me = this;
			var $dom = $(e.target);
			if ($dom[0].tagName.toLowerCase() == 'b') {
				$dom = $dom.parent("td");
			}
			if ($dom[0].tagName.toLowerCase() == 'td') {		// 确保点击的不是下钻按钮
				var $tr = $dom.parent("tr").children("td");
				$("#test tr").removeClass("active");
				$($dom.parent("tr")).addClass("active");
				//console.log($($tr[1]).text());
				var index = $($tr[0]).children("b").data("orgid");
				//console.log(index);

				switch(banma.type){
					case "rider":
						me._paintIndexLineRider(index);
					break;
					case "store":
						me._paintIndexLineStore(index);
					break;
					default:
						me._paintIndexLine(index);
					break;
				}
				
			}
		},
		_showGroupTable: function(e){
			var me = this;
			var $tab = $(e.target);
			$tab.addClass('on').siblings('.tab_item').removeClass('on');
			var idx = $tab.data('index');
			me._paintTable(undefined,me,idx);
		},
		_tab: function(e) {
			var me = this;
			var $tab = $(e.target);
			var level = $tab.data('level');
			me.level = level;
			me.parentId = me.level * -1;
			$tab.addClass('on').siblings('.tab_item').removeClass('on');
			var post = {
				parentId: me.parentId,
				version:41
			}
			me._resetMenu();
			switch($("#tab .on").text()){
				case "商家":
					me.orgType = 4;
					post.page = 1;
					post.pageSize = banma.pageSize;
					me._ajax('/data/monitor/poiTableData', post, me._paintTableStore);
				break;
				case "骑手":
					me.orgType = 4;
					post.page = 1;
					post.pageSize = banma.pageSize;
					me._ajax('/data/monitor/riderTableData', post, me._paintTableRider);
				break;
				default:
					me.orgType = 4;
					me._ajax('/data/monitor/orgTableData', post, me._paintTable);
				break;

			}
					
		},
		_search: function() {
			var me = this;
			me.parentId = me.level * -1;
			var post = {
				parentId: me.parentId,
				version:41
			}

			switch($("#tab .on").text()){
				case "商家":
					me.orgType = 4;
					post.page = 1;
					post.pageSize = banma.pageSize;
					me._ajax('/data/monitor/poiTableData', post, me._paintTableStore);
				break;
				case "骑手":
					me.orgType = 4;
					post.page = 1;
					post.pageSize = banma.pageSize;
					me._ajax('/data/monitor/riderTableData', post, me._paintTableRider);
				break;
				default:
					me.orgType = 4;
					me._ajax('/data/monitor/orgTableData', post, me._paintTable);
				break;

			}

		},
		_showTip: function(e) {

			var $dom = $(e.target).parents("th");

			var name = $dom.data('text');
			if (!name) {
				return false;
			}
			var rect = $dom[0].getBoundingClientRect();
			var x = e.pageX,
				y = rect.top + 30;

			$dom.append('<b class="tip">' + name + '</b>');
			$b = $dom.find('b');
			$icon = $(e.target);
			if($icon.offset().left + $b.width() > $(window).width()){
				$b.css({
					left: ($(window).width()-$b.width()+16),
					top: y + 'px'
				});
			}else{
				$b.css({
					left: x + 'px',
					top: y + 'px'
				});
			}
		},
		_removeTip: function(e) {
			$(".tip_icon .tip").remove();
		},
		_showExportMenu: function() {
			$('#exportMenu').toggleClass('show');
		},
		_showExportMenu1: function() {
			$('#exportMenu').addClass('show');
		},
		_showExportMenu2: function() {
			$('#exportMenu').removeClass('show');
		},
		_exportTable: function() {
			var me = this;
			var time = me._getTime();
			var url = '';
			switch(banma.type){
				case "other":
					url = "/data/monitor/orgTableDataExport";
				break;
				case "store":
					url = "/data/monitor/poiTableDataExport";
				break;
				case "rider":
					url = "/data/monitor/riderTableDataExport";
				break;
			}
			me.parentId = me.level * -1;
			url += '?version=41&orgType=' + me.orgType + '&parentId=' + me.parentId + '&stime=' + time.stime + '&etime=' + time.etime;
			window.open(url);
		},
		_exportChart: function() {
			var me = this;
			var time = me._getTime();
			var index = $('#test').find('.active b').data("orgid");
			var orgId = banma.data[index][0];
			var poiId = banma.data[index][1];
			var riderId = banma.data[index][1];

			var url = '';
			switch(banma.type){
				case "other":
					url = '/data/monitor/orgChartDataExport?levelType=' + me.level;
				break;
				case "store":
					url  = '/data/monitor/poiChartDataExport?poiId=' + poiId;
				break;
				case "rider":
					url  = '/data/monitor/riderChartDataExport?riderId=' + riderId;
				break;
			}
			url += '&version=41&orgType=' + me.orgType + '&orgId=' + orgId + '&stime=' + time.stime + '&etime=' + time.etime;
			window.open(url);
		},
		_formatSeries: function(obj) {					//将数据格式化为hightcharts中的数据列格式
			var me = this;
			var res = [];
			for (k in obj) {
				var temp = {};
				temp.name = k;
				temp.data = obj[k];
				temp.yAxis = me._yAxis(obj[k]);
				if(banma.type=="rider")
					temp.visible = ($.inArray(k, ['完成订单数','接单完成率','普通订单总配送时长']) < 0) ? false : true;
				else
					temp.visible = ($.inArray(k, defaultSeries) < 0) ? false : true;
				res.push(temp);
			}
			return res;
		},
		_yAxis: function(data) {						// 判断数据列对应的Y轴
			if (/%/.test(data[0])) {
				return 0;
			}else {
				var max = Math.max.apply(null, data);
				var res = (max > 1000 && 3) || (max > 100 && 2) || 1;
				return res;
			}
		},
		_formatDate: function(arr) {					// 将时间戳转为YYYY/MM/DD的格式显示在X轴
			var res  = [],
				n    = arr.length,
				date = new Date();
			while (n --) {
				date.setTime(arr[n]);
				var str = date.toLocaleDateString();
				res.unshift(str);
			}
			return res;
		},
		_getTime: function() {
			var time  = $('.reportrange span').html();
			var stime = time.substr(0, 8),
				etime = time.substr(-8);
			return {stime: stime, etime: etime}
		},
		_bind: function() {									// 绑定事件
			var me = this;
			var	context = me,
				events  = me.events,
				$wrap   = me.wrap;

			events && $.each(events, function(k, v) {
				var handler;
				me[v[2]] && (handler = me[v[2]]);

				var ev = v[0] + '.' + k,
					el = v[1];
				$wrap.off(ev).on(ev, el, function(e) {
					handler.apply(context, [e, this]);
				})
			})
		},
		_ajax: function(url, data, callback, type) {		// ajax
			var me = this;
			var type 	 = type || 'POST',
				callback = callback || noop;
			var oLoading = document.getElementById('loadingWrapper');
			// 每次查询检查时间条件
			var otime    = me._getTime();
			data.stime   = otime.stime;
			data.etime 	 = otime.etime;
			data.orgType = me.orgType;
			$.ajax({
				url: url,
				type: type,
				dataType: 'json',
				data: data,
				success: function(res) {
					//console.log(res);
					oLoading.classList.remove('show');
					callback(res, me);
				},
				beforeSend: function() {
					oLoading.classList.add('show');
				},
				error: function() {
					// alert('请求{' + url + '}出错!')
					oLoading.classList.remove('show');
				}
			})
		}
	}
	banma._init();
})