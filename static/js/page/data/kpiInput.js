$(document).ready(function () {
    var cb = function (start, end, label) {
        $('.reportrange span').html(start.format('YYYYMMDD') + ' - ' + end.format('YYYYMMDD'));
    };

    var ajaxbg = $("#background,#progressBar");
    ajaxbg.hide();

    //初始值
    var toyear = new Date().getFullYear();
    var toMonth = new Date().getMonth();//上月  0
    var fromyear = toyear;
    var frommonth = toMonth;

    if(toMonth==0){
        fromyear = new Date().getFullYear()-1;
        frommonth = 12;
    }

    var lastMonthFirstDay = formatDate3(fromyear, frommonth, 1);//上月第一天
    var lastMonthEndDate = formatDate3(fromyear, frommonth, getMonthDays(frommonth));//上月月末
    //一天的毫秒数
    var week = new Date().getDay();
    var millisecond = 1000*60*60*24;
    //减去的天数
    var minusDay = week!=0 ? week-1:6;
    //获得当前周的第一天
    var currentWeekDayOne = new Date(new Date().getTime()-(millisecond*minusDay));
    //上周最后一天即本周开始的前一天
    var priorWeekLastDayTemp = new Date(currentWeekDayOne.getTime()-millisecond);
    var priorWeekLastDay = formatDate2(new Date(currentWeekDayOne.getTime()-millisecond));
    //上周的第一天
    var priorWeekFirstDay = formatDate2(new Date(priorWeekLastDayTemp.getTime()-(millisecond*6)));
    var optionSet1 = {
        startDate: moment().subtract(7, 'days').format('YYYYMMDD'),
        endDate: moment().subtract(1, 'days').format('YYYYMMDD'),
        minDate: '20120101',
        maxDate: moment().subtract(1, 'days').format('YYYYMMDD'),
        dateLimit: {days: 60},
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            '昨天': [moment().subtract(1, 'days').format('YYYYMMDD'), moment().subtract(1, 'days').format('YYYYMMDD')],
            '最近三天': [moment().subtract(3, 'days').format('YYYYMMDD'), moment().subtract(1, 'days').format('YYYYMMDD')],
            '最近一周': [priorWeekFirstDay, priorWeekLastDay],
            '最近一月': [lastMonthFirstDay, lastMonthEndDate]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-sm btn-success',
        cancelClass: 'btn-sm',
        format: 'YYYYMMDD',
        separator: ' to ',
        locale: {
            applyLabel: '应用',
            cancelLabel: '取消',
            fromLabel: '从',
            toLabel: '到',
            customRangeLabel: '定制',
            daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
            monthNames: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
            firstDay: 1
        }
    };
    window.NeedDay = optionSet1.ranges;

    $('.reportrange span').html(moment().subtract(7, 'days').format('YYYYMMDD') + ' - ' + moment().subtract(1, 'days').format('YYYYMMDD'));
    $('.reportrange').daterangepicker(optionSet1, cb);

    $("#expButton").click(function () {
        var timerange = $('.reportrange span')[0].innerHTML;
        if (timerange != null && timerange.length > 0) {
            var alertstart = timerange.split(" - ")[0];
            var alertend = timerange.split(" - ")[1];
            var subtime = parseInt(Math.abs(getDateByParam(alertend).getTime()-getDateByParam(alertstart).getTime()) / (1000   *   60   *   60   *   24)) ;
            var tableflag = -1;
            if(subtime == 0){
                tableflag = 1;// day
            } else if(subtime == 2){
                tableflag = 2;// 3 day
            }  else if(subtime == 6){
                if(getDateByParam(alertend) < new Date()
                        && getDateByParam(alertstart).getDay()==1){//结束日期小于当前时间，且start时间为某个周一
                    tableflag = 3;//  a week
                }
            } else if(subtime>=26 && subtime<=31){
                if(getDateByParam(alertstart).getDate()==1
                        && getDateByParam(alertend).getDate()==getMonthDays(getDateByParam(alertend).getMonth()+1)){
                    //起始日期为月首，结束日期为月末
                    tableflag = 4;//  a month
                }
            } else {
                alert("请选择单日或3日或自然周或自然月！");
                return ;
            }
            if( tableflag < 0 ) {
                alert("请选择单日或3日或自然周或自然月！");
                return ;
            }
            location.href = '/banmadata/order/export?start=' + alertstart + '&end=' + alertend + '&flag=' + tableflag;
        } else {
            alert("请选择日期！");
        }
    });


    $("#queryButton").click(function () {
        var timerange = $('.reportrange span')[0].innerHTML;
        if (timerange != null && timerange.length > 0) {
            var alertstart = timerange.split(" - ")[0];
            var alertend = timerange.split(" - ")[1];
            var subtime = parseInt(Math.abs(getDateByParam(alertend).getTime()-getDateByParam(alertstart).getTime()) / (1000   *   60   *   60   *   24)) ;
            var tableflag = -1;
            if(subtime == 0){
                tableflag = 1;// day
            } else if(subtime == 2){
                tableflag = 2;// 3 day
            }  else if(subtime == 6){
                if(getDateByParam(alertend) < new Date()
                        && getDateByParam(alertstart).getDay()==1){//结束日期小于当前时间，且start时间为某个周一
                    tableflag = 3;//  a week
                }
            } else if(subtime>=26 && subtime<=31){
                if(getDateByParam(alertstart).getDate()==1
                    && getDateByParam(alertend).getDate()==getMonthDays(getDateByParam(alertend).getMonth()+1)){
                    //起始日期为月首，结束日期为月末
                    tableflag = 4;//  a month
                }
            } else {
                alert("请选择单日或3日或自然周或自然月！");
                return ;
            }
            if( tableflag < 0 ) {
                alert("请选择单日或3日或自然周或自然月！");
                return ;
            }

                $.ajax({
                    type: 'post',
                    url: '/banmadata/order/query?start=' + alertstart + '&end=' + alertend + '&flag=' + tableflag +  '&toPage=1',
                    data: {},
                    cache: false,
                    dataType: 'json',
                    beforeSend:function(xhr){
                        ajaxbg.show();
                    },
                    success: function (data) {
                            organizeTable(data.data, data.name_keyMap, data.toPage, data.orderedColumns, data.page, alertstart, alertend, tableflag);
                    },
                    complete:function(xhr){
                        ajaxbg.hide();
                    },
                    error: function () {
                        alert("error");
                    }
                });
            } else {
                alert("请选择日期！");
            }
        });
    });


            //格式化日期：yyyyMMdd
            function formatDate2(ddate) {
                var myyear = ddate.getFullYear();
                var mymonth = ddate.getMonth()+1;
                var myweekday = ddate.getDate();
                if(mymonth==0){
                    mymonth = 12;
                    myyear = parseInt(myyear)-1;
                }

                if(mymonth < 10){
                    mymonth = "0" + mymonth;
                }
                if(myweekday < 10){
                    myweekday = "0" + myweekday;
                }
                return (myyear+""+mymonth + "" + myweekday);
            }


            //格式化日期：yyyy-MM-dd
            function formatDate(myyear, mymonth,myweekday) {
//                var myyear = ddate.getFullYear();
//                var mymonth = ddate.getMonth()+1;
//                var myweekday = ddate.getDate();
                if(mymonth==0){
                    mymonth = 12;
                    myyear = parseInt(myyear)-1;
                }

                if(mymonth < 10){
                    mymonth = "0" + mymonth;
                }
                if(myweekday < 10){
                    myweekday = "0" + myweekday;
                }
                return (myyear+"-"+mymonth + "-" + myweekday);
            }


            //格式化日期：yyyyMMdd
            function formatDate3(myyear, mymonth,myweekday) {
//                var myyear = ddate.getFullYear();
//                var mymonth = ddate.getMonth()+1;
//                var myweekday = ddate.getDate();
                if(mymonth==0){
                    mymonth = 12;
                    myyear = parseInt(myyear)-1;
                }

                if(mymonth < 10){
                    mymonth = "0" + mymonth;
                }
                if(myweekday < 10){
                    myweekday = "0" + myweekday;
                }
                return (myyear+""+mymonth + "" + myweekday);
            }


            //获得某月的天数
            function getMonthDays(myMonth){
                var date = new Date();
                var monthStartDate = new Date(date.getFullYear(), parseInt(myMonth) - 1, 1);
                var monthEndDate = new Date(date.getFullYear(), parseInt(myMonth), 1);
                var days =   (monthEndDate.getTime() - monthStartDate.getTime())/(1000 * 60 * 60 * 24);
                return days;
            }

            function getDateByParam( param ){
                var dd = new Date();
                dd.setFullYear(param.substr(0,4));
                dd.setMonth(parseInt(param.substr(4,2))-1);
                dd.setDate(param.substr(6,2));
                dd.setHours(0);
                dd.setMinutes(0);
                dd.setSeconds(0);
                dd.setMilliseconds(0);
                return dd;
            }


            function checkDate() {
                var timerange = $('.reportrange span')[0].innerHTML;
                if (timerange != null && timerange.length > 0) {
                    var alertstart = timerange.split(" - ")[0];
                    var alertend = timerange.split(" - ")[1];
                    if (alertstart == null || alertend == null) {
                        alert("请选择日期！");
                        return false;
                    }
                    if (alertstart != alertend) {
                        alert("目前只支持单日查询，请重新选择！");
                        return false;
                    }
                } else {
                    alert("请选择日期！");
                    return false;
                }
                return true;
            }


            function organizeTable(jsondata, namekeymap, toPage, orderedColumns, page, start, end, tableflag) {
                var htmlTemp = "";
                if(jsondata==null || jsondata.length==0){
                    htmlTemp += "<div align=\"center\">没有符合条件的查询结果</div>";
                    $("#realtable").html(htmlTemp);
                    return ;
                }
                htmlTemp += "<div style=\"overflow-y: auto;\"><table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" class=\"table table-striped\" id=\"searchtableshow\" style=\"margin: 10px;word-wrap: break-word;word-break:break-all; min-width:3400px;min-height: 500px;\">";
                htmlTemp += "<thead><tr>";
                for (var i = 0; i < orderedColumns.length; i++) {
                    if (orderedColumns[i] == '站点') {
                        htmlTemp += "<th style='width:80px;text-align:center;'> " + orderedColumns[i] + "</th>";
                    } else {
                        htmlTemp += "<th style=\"text-align:center\"> " + orderedColumns[i] + "</th>";
                    }
                }
                htmlTemp += "</tr></thead>";


                for (var i = 0; i < jsondata.length; i++) {
                    htmlTemp += "<tr>";
                    for (var j = 0; j < orderedColumns.length; j++) {
                        if(jsondata[i][namekeymap[orderedColumns[j]]]==undefined){
                            htmlTemp += "<td style=\"text-align:center\">(空)</td>";
                        } else {
                            htmlTemp += "<td style=\"text-align:center\">" + jsondata[i][namekeymap[orderedColumns[j]]] + "</td>";
                        }

                    }
                    htmlTemp += "</tr>";
                }
                htmlTemp += "</table></div>";

                if (page != null) {
                    var leftNum = page.leftNum;
                    var rightNum = page.rightNum;
                    htmlTemp += "<div><ul class=\"pagination\" style=\"float: right;\">";


                    if(page.totalPage < 7){ //不隐藏页码
                        if (page.currentPage == 1) {
                            htmlTemp += "<li class=\"disabled\"><a id = \"prev\" data-page = \"" + page.currentPage + "\" href=\"javascript:void(0);\">«</a></li>";
                        } else {
                            htmlTemp += "<li ><a id = \"prev\" data-page = \"" + page.currentPage + "\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + (page.currentPage-1) + ")\" >«</a></li>";
                        }

                        //修正for 循环的写法
                        for (var p = 1; p <= page.totalPage; p++) {
                            if (p == page.currentPage) {
                                htmlTemp += "<li class=\"active\" ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + p + ")\"  >" + p + "</a></li>";
                            } else {
                                htmlTemp += "<li ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + p + ")\" >" + p + "</a></li>";
                            }
                        }

                        if (page.currentPage == page.totalPage) {
                            htmlTemp += "<li class=\"disabled\"><a id= \"next\" data-page = \"" + (page.currentPage) + "\" href=\"javascript:void(0);\">»</a></li>";
                        } else {

                            //修正分页的ftl错误
                            htmlTemp += "<li ><a id= \"next\" class=\"pageclick\" data-page = \"" + (page.currentPage) + "\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + (page.currentPage+1) + ")\" >»</a></li>";
                        }

                    } else {//隐藏页码
                        if (page.currentPage == 1) {
                            htmlTemp += "<li class=\"disabled\"><a id = \"prev\" data-page = \"" + page.currentPage + "\" href=\"javascript:void(0);\">«</a></li>";
                        } else {
                            htmlTemp += "<li ><a id = \"prev\" data-page = \"" + page.currentPage + "\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + (page.currentPage-1) + ")\" >«</a></li>";
                        }



                        for (var p = 1; p <= 2; p++) {
                            if (p == page.currentPage) {
                                htmlTemp += "<li class=\"active\" ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + p + ")\"  >" + p + "</a></li>";
                            } else {
                                htmlTemp += "<li ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + p + ")\" >" + p + "</a></li>";
                            }
                        }
                        if(page.currentPage == 1){
                            htmlTemp += "<li class=\"disabled\" ><a name = \"pageCode\"  href=\"javascript:void(0);\"  >...</a></li>";
                        } else if(page.currentPage == 2){
                            htmlTemp += "<li  ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + 3 + ")\"  >3</a></li>";
                            htmlTemp += "<li class=\"disabled\" ><a name = \"pageCode\"  href=\"javascript:void(0);\"  >...</a></li>";
                        } else if(page.currentPage == 3){
                            htmlTemp += "<li class=\"active\" ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + 3 + ")\"  >3</a></li>";
                            htmlTemp += "<li  ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + 4 + ")\"  >4</a></li>";
                            htmlTemp += "<li class=\"disabled\" ><a name = \"pageCode\"  href=\"javascript:void(0);\"  >...</a></li>";
                        } else if (page.currentPage == 4){
                            htmlTemp += "<li  ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + 3 + ")\"  >3</a></li>";
                            htmlTemp += "<li class=\"active\" ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + 4 + ")\"  >4</a></li>";
                            htmlTemp += "<li  ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + 5 + ")\"  >5</a></li>";
                            htmlTemp += "<li class=\"disabled\" ><a name = \"pageCode\"  href=\"javascript:void(0);\"  >...</a></li>";
                        } else if (page.currentPage > 4 && (page.currentPage <= page.totalPage-4)){
                            htmlTemp += "<li class=\"disabled\" ><a name = \"pageCode\"  href=\"javascript:void(0);\"  >...</a></li>";
                            htmlTemp += "<li  ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + (page.currentPage - 1 ) + ")\"  >" + (page.currentPage - 1 ) + "</a></li>";
                            htmlTemp += "<li class=\"active\" ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + page.currentPage + ")\"  >" + page.currentPage + "</a></li>";
                            htmlTemp += "<li  ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + (page.currentPage + 1 ) + ")\"  >" + (page.currentPage + 1 ) + "</a></li>";
                            htmlTemp += "<li class=\"disabled\" ><a name = \"pageCode\" href=\"javascript:void(0);\"  >...</a></li>";
                        } else if (page.currentPage == page.totalPage-3){
                            htmlTemp += "<li class=\"disabled\" ><a name = \"pageCode\"  href=\"javascript:void(0);\"  >...</a></li>";
                            htmlTemp += "<li  ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + (page.currentPage - 1 ) + ")\"  >" + (page.currentPage - 1 ) + "</a></li>";
                            htmlTemp += "<li class=\"active\" ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + page.currentPage + ")\"  >" + page.currentPage + "</a></li>";
                            htmlTemp += "<li  ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + (page.currentPage + 1 ) + ")\"  >" + (page.currentPage + 1 ) + "</a></li>";
                        } else if (page.currentPage == page.totalPage-2){
                            htmlTemp += "<li class=\"disabled\" ><a name = \"pageCode\"  href=\"javascript:void(0);\"  >...</a></li>";
                            htmlTemp += "<li  ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + (page.currentPage - 1 ) + ")\"  >" + (page.currentPage - 1 ) + "</a></li>";
                            htmlTemp += "<li class=\"active\" ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + page.currentPage + ")\"  >" + page.currentPage + "</a></li>";
                        } else if (page.currentPage == page.totalPage-1){
                            htmlTemp += "<li class=\"disabled\" ><a name = \"pageCode\"  href=\"javascript:void(0);\"  >...</a></li>";
                            htmlTemp += "<li  ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + (page.currentPage - 1 ) + ")\"  >" + (page.currentPage - 1 ) + "</a></li>";
                        } else if (page.currentPage == page.totalPage){
                            htmlTemp += "<li class=\"disabled\" ><a name = \"pageCode\"  href=\"javascript:void(0);\"  >...</a></li>";
                        }

                        for (var p = page.totalPage-1; p <= page.totalPage; p++) {
                            if (p == page.currentPage) {
                                htmlTemp += "<li class=\"active\" ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + p + ")\"  >" + p + "</a></li>";
                            } else {
                                htmlTemp += "<li ><a name = \"pageCode\" class=\"pageclick\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + p + ")\" >" + p + "</a></li>";
                            }
                        }

                        if (page.currentPage == page.totalPage) {
                            htmlTemp += "<li class=\"disabled\"><a id= \"next\" data-page = \"" + (page.currentPage) + "\" href=\"javascript:void(0);\">»</a></li>";
                        } else {

                            //修正分页的ftl错误
                            htmlTemp += "<li ><a id= \"next\" class=\"pageclick\" data-page = \"" + (page.currentPage) + "\" onclick=\"pageRedirect(" + start + ", " + end + "," + tableflag + "," + (page.currentPage+1) + ")\" >»</a></li>";
                        }

                    }



                    htmlTemp += "</ul></div>";

                }
                $("#realtable").html(htmlTemp);

            }


            function pageRedirect(start, end,tableflag , toPage) {
//                alert(start);
                var ajaxbg = $("#background,#progressBar");
                $.ajax({
                    type: 'post',
                    url: '/banmadata/order/query?start=' + start + '&end=' + end + '&flag=' + tableflag +  '&toPage='+toPage,
                    data: {},
                    cache: false,
                    dataType: 'json',
                    beforeSend:function(xhr){
                        ajaxbg.show();
                    },
                    success: function (data) {
                        if (data.data != null && data.data.length > 0) {
//                            alert(data.data);
                            organizeTable(data.data, data.name_keyMap, data.toPage, data.orderedColumns, data.page, start, end, tableflag);
                        } else {
                            //no data

                        }
                    },
                    complete:function(xhr){
                        ajaxbg.hide();
                    },
                    error: function () {
                        alert("error");
                    }
                });

            }
