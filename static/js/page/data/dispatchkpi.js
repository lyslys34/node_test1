$(document).ready(function() {
    var ajaxbg = $("#background,#progressBar");
        ajaxbg.hide();
    var localStorage = window.localStorage;
    var basicInfo = ['配送方类型', '外卖区域', '物理城市', '站点'];

    var cityMap = {};

    var arr = {};
    var selectData, chartData, selectedIndex = {}, compareAreas = [];

    var indexCategories = {
      "基础": [
        "总运单数",
        "有订单骑手数",
        "有订单商家数"
      ],
      "效率": [
        "骑手人均单量",
        "单均行驶距离",
        "骑手完成单量标准差",
        "单均消耗时间"
      ],
      "体验": [
        "单均配送时长",
        "单均配送周期",
        "最坏配送时长",
        "送达准时率",
        "预订单准时率"
      ],
      "异常": [
        "badcase比例",
        "运单改派率"
      ],
    };

    var compareSwitch = false;

	  //图表配置
    var areaOption = {
    	chart: {
            type: 'spline'
        },
    	title: {
    	    text: '区域数据总览',
    	    x: -20 //center
    	},
    	xAxis: { 
    		categories: []
    	},
    	yAxis: [
          {// 0 ~ 100
              title: {
                  text: '主维度'
              },
              opposite: true,//左右浮动
              labels: {
                  formatter: function() {
                      return this.value
                  }
              }
          },
          {// 100~10000
              title: {
                  text: '超数维度'
              },
              plotLines: [{
                  value: 1,
                  width: 3
              }],
              labels: {
                  formatter: function() {
                     return this.value + "个";
                  }
              },
              opposite: true  //是否左右浮动的
          },
          {// yAxis:0  1000->1K 1000,0000->1M  1,000 ~ 1,0000,0000
              title: {
                  text: '大数维度'
              },
              plotLines: [{
                  value: 0,
                  width: 2
              }],
              labels: {
                  formatter: function() {
                      if( this.value >= 0 && this.value <= 10000 ){
                          return this.value;
                      }else if ( this.value <= 100000 && this.value > 10000 ) {
                          return this.value/1000 + "K";
                      }else{
                          return this.value/1000000 +'M';
                      }
                  }
              }
          },
          {// yAxis:1 %0 ~ 100%
              title: {
                  text: '小数维度'
              },
              plotLines: [{
                  value: 1,
                  width: 3
              }],
              min: 0,
              max: 100,
              labels: {
                  formatter: function() {
                     return this.value + "%";
                  }
              }
          }
      ],
      legend: {
          verticalAlign: 'bottom'
      },
      plotOptions: {
          line: {
              enableMouseTracking: true
          },
          spline: {
              marker: {
                  radius: 4,
              }
          },
          series: {
              events: {
              //控制图标的图例legend不允许切换
                  legendItemClick: function (event) {

                  }
              }
          }

      },
      credits:{ enabled:false },
      tooltip: {
          crosshairs: {
              width: 1,
              color: '#ff6900',
              dashStyle: 'line'
          },
          shared: true
      },
  		series: []
    };

    var DispatchKpi = {
      init: function() {
        this.initSelect();
      },

      initSelect: function() {
        var _ = this;
        _.ajax("/data/monitor/dispatchkpi/getAreaAndPeriod", function(res) {
          if (res.msg === "成功") {
            var data = res.data,
                body = "";
            selectData = data;
            var cityArea = data.cityArea;
            Object.keys(cityArea).forEach(function(id) {
              body += "<option value='" + id + "'>"+ cityArea[id].cityName + "</option>";
            });
            $("#city").html(body);
            $("#city").select2({matcher: matcher});
            $("#diffCity").html(body);
            $("#diffCity").select2({matcher: matcher});

            _.renderAreaSelect("#peisongArea", "#peisongType", $("#city").val());
            _.renderAreaSelect("#diffPeisongArea", "#diffPeisongType", $("#diffCity").val());

            if (!arr["areaIds"]) {
              arr["areaIds"] = [];
            }
            arr["areaIds"].push($("#peisongArea").val());

            body = "";
            var periods = data.periods;
            periods.forEach(function(item) {
              body += "<option value='" + item.period_group_id + "'>" + item.period_group_desc + "</option>";
            });

            $("#areaSearchTime").html(body);

            _.getJsonAndChart();
            _.bind();
          }else{
            alert("数据获取失败！");
          }
        });
      },

      bind: function() {
        var _ = this;

        $(".j-btn-export").on('click', function(event) {
            var href = "/data/monitor/dispatchkpi/tableDataExport?";
            href += "stime=" + arr["start"] + "&etime=" + arr["end"] + "&areaIds=" + arr["areaIds"] + "&period=" + arr["period"];
            $(this).attr({
                "href": href,
                "target": '_blank'
            });
        });

        $("#areaSearchBtn").on('click', _.getJsonAndChart.bind(_));

        $('.different-area').on('click', 'a', function() {
          if ($('.different-area-select').css('display') === 'block') {
            $('.different-area-select').hide();
            $("body").off('click');
          } else {
            $('.different-area-select').show();
            $("body").on('click', function(e) {
              if (!$.contains($('.different-area')[0], e.target) && $.contains($('.select2-container')[0], e.target)) {
                $("body").off('click');
                $('.different-area-select').hide();
              }
            });
          }
        });

        $('.different-footer').on('click', '.confirm', function() {
          var diffPeisongArea = $('#diffPeisongArea option:selected').text();
          if (diffPeisongArea === $('#peisongArea option:selected').text()) {
            return ;
          }
          compareAreas[$('#diffPeisongArea').val()] = diffPeisongArea;
          $('#compareSwitch').prop('checked', true);
          _.renderHasSelectedArea();
        });

        $('.different-area-select ul').on('click', ' li i', function() {
          var id = $(this).parent().data('id');
          delete compareAreas[id];
          _.renderHasSelectedArea();
        })

        $('.different-footer').on('click', '.cancel', function() {
          $('.different-area-select').hide();
          $('#compareSwitch').prop('checked', true);
        });

        $('#city').on('change', function() {
          _.renderAreaSelect('#peisongArea', "#peisongType", $(this).val());
        });

        $('#peisongType').on('change', function() {
          _.renderAreaSelect('#peisongArea', "#peisongType", $('#city').val());
        });

        $('#diffCity').on('change', function() {
          _.renderAreaSelect('#diffPeisongArea', "#diffPeisongType", $(this).val());
        });

        $('#diffPeisongType').on('change', function() {
          _.renderAreaSelect('#diffPeisongArea', "#diffPeisongType", $('#diffCity').val());
        });

        $('.j-btn-display-index').on('click', function() {
          if ($('.display-index').css('display') === 'block') {
            $('.display-index').hide();
            $("body").off('click');
          } else {
            $('.display-index').show();
            $("body").on('click', function(e) {
              if (!$.contains($('.display-index-wrapper')[0], e.target)) {
                $("body").off('click');
                $('.display-index').hide();
              }
            });
          }
        });

        $('.display-index').on('click', 'input', function() {
          selectedIndex[$(this).val()] = $(this).prop('checked');
          if (compareSwitch) {
            $('.j-btn-display-index').text('显示指标: ' + $('.display-index input:checked').val());
            _.renderChart();
          }
        });

        $('.display-index').on('click', 'button', function() {
          $("body").off('click');
          $('.display-index').hide();
          if (!compareSwitch) {
            _.renderChart();
          }
        });
      },

      getJsonAndChart: function() {
        var _ = this;
        var time  = $('.reportrange span').html();
        var stime = time.substr(0, 8),
          etime = time.substr(-8);
        arr["start"] = stime;
        arr["end"] = etime;
        compareSwitch = $("#compareSwitch").prop('checked');
        arr["period"] = $("#areaSearchTime").val();
        if (compareSwitch) {
          var peisongAreaId = $('#peisongArea').val();
          var diffPeisongAreaId = $('#diffPeisongArea').val();
          arr["areaIds"] = [peisongAreaId].concat(Object.keys(compareAreas));
          cityMap[peisongAreaId] = $('#peisongArea option:selected').text();
          for (compareAreaId in compareAreas) {
            cityMap[compareAreaId] = compareAreas[compareAreaId];
          }
          arr["areaIds"] = arr["areaIds"].toString();
        } else {
          arr["areaIds"] = $("#peisongArea").val();
        }

        //edit by panql 判断有木有
        var params = {
          stime: arr["start"],
          etime: arr["end"],
          areaIds: arr["areaIds"],
          period: arr["period"]
        };

        _.ajax("/data/monitor/dispatchkpi/chartData", "POST", params, function(json) {
          var json = json,
              msg = json.msg,
              data = json.data;
          if (msg === "成功") {
              if (data.length === 0) {
                  alert("当前日期没有数据，请重新选择！");
                  areaOption.xAxis.categories = [];
                  areaOption.series = [];
                  $('.chartsRend').highcharts(areaOption);
                  return ;
              }
              $('.j-btn-display-index').text('显示指标');

              selectedIndex = {};

              areaOption.series = []; //每次重新渲染的时候，先清空再写入

              areaOption.xAxis.categories = data.chartAxes;

              chartData = data;

              var tempChartData = data.chartData[0];

              _.renderIndex();

              _.renderChart();

              _.rendTable(params);
          }else{
              alert("error");
          }
        });
      },

      renderHasSelectedArea: function() {
        var html = "";
        for (item in compareAreas) {
          var areaName = compareAreas[item];
          html += "<li data-id=" + item + " title='" + areaName + "'><i></i>" + areaName + "</li>";
        }
        $('.different-area-select ul').html(html);
      },

      renderAreaSelect: function(element, type, cityId) {
        var areaList = selectData.cityArea[cityId].areaList;
        var html = '';
        var peisongType = $(type).val();
        if (peisongType == 0) {
          areaList.forEach(function(item) {
             html += "<option value='" + item.areaId + "'>"+ item.areaName + "</option>";
          });
        } else {
          areaList.forEach(function(item) {
            if (peisongType == item.type) {
              html += "<option value='" + item.areaId + "'>"+ item.areaName + "</option>";
            }
          });
        }
        $(element).html(html);
        $(element).select2({matcher: matcher, width: 130});
      },

      rendTable: function(params) {
        this.ajax('/data/monitor/dispatchkpi/tableData', 'post', params, function(data) {
          var temp = "";
          if (data.msg === "成功") {
              var tableHead = data.data.tableHead;
              if (!tableHead || !tableHead.length) {
                  temp += "数据暂时没有！";
              }else{
                  temp += "<table class=\"table table-waimai display\" cellspacing=\"0\"><thead ><tr>";
                  tableHead.forEach(function(head) {
                    temp += "<th>" + head + "</th>";
                  });

                  temp += "</tr></thead>";
                  var tableBody = data.data.tableBody;
                  var len = tableBody.length;
                  for (var i = 0,len = tableBody.length; i < len; i++) {
                      temp += '<tr role="row">';
                      for (var j = 0,tdLength = tableBody[i].length; j < tdLength; j++) {
                        temp += "<td title=\""+ tableBody[i][j] +"\">" + formatNumber(tableBody[i][j]) + "</td>";
                      }
                      temp += "</tr>";
                  }
                  temp += "</table>";
              }
              $('.charts-table').html(temp);
              $('.charts-table table').dataTable({
                  "scrollX": true,
                  "scrollY": 550,
                  "dom": "frtiS",
                  "deferRender": true,
                  "oLanguage": {
                      "sSearch" : "搜索"
                  },
                  "aaSorting":[]
              });
              var customLevel = $('.j-custom-btn').data('level');
              var chartArr = localStorage[customLevel + "_menuArr"] ? localStorage[customLevel + "_menuArr"].split(",") : '';
              if (!!chartArr) {
                  var indexArr = [];
                  for (var x in headRes) {
                      indexArr.push(x);
                      for (var y in chartArr) {
                          if (headRes[x].caption === chartArr[y] || ($.inArray(headRes[x].caption, basicInfo)) >= 0 ) {
                              indexArr.pop();
                              break;
                          }
                      }
                  }
                  $('.charts-table table').DataTable().columns(indexArr).visible(false, false);
                  $('.charts-table table').DataTable().columns.adjust().draw(false);
              }
          }else{
              alert("error");
          }
        });
      },

      renderIndex: function() {
        var html = '';
        if (compareSwitch) {
          for (index in indexCategories) {
            html += "<div><label class='index-category'>" + index + ": </label>";
            indexCategories[index].forEach(function(key) {
              html += "<span>";
              html += "<label><input type='radio' name='radio-index' value='" + key + "'/>" + key + "</label>";
              html += "</span>";
            });
            html += "</div>";
          }
        } else {
          var count = 3;
          for (index in indexCategories) {
            html += "<div><label class='index-category'>" + index + ": </label>";
            indexCategories[index].forEach(function(key) {
              html += "<span>";
              if (count > 0) {
                selectedIndex[key] = true;
                html += "<label><input type='checkbox' checked value='" + key + "'/>" + key + "</label>";
                count--;
              } else {
                html += "<label><input type='checkbox' value='" + key + "'/>" + key + "</label>";
              }
              html += "</span>";
            });
            html += "</div>";
          }

        }
        html += "<button class='btn'>确定</button>";

        $(".display-index").html(html);

        compareSwitch ? $(".display-index input")[0].click() : '';
      },

      renderChart: function() {
        areaOption.series = []; //每次重新渲染的时候，先清空再写入
        var yAxisSwitch = false, dataObj;
        if (compareSwitch) {
          var tempChartData = chartData.chartData;
          var showIndex = $('.display-index input:checked').val();
          tempChartData.forEach(function(dataList, index) {
            for (item in dataList) {
              if (item === showIndex) {
                yAxisSwitch = dataList[item].every(function (data){
                  if(data <= 1 && data >= 0) {
                    return true;
                  } else if(data > 1) {
                    return false;
                  }
                });
                dataObj = {
                  name: cityMap[chartData.areaIds[index]],
                };
                if (yAxisSwitch) {
                  dataObj.data = dataList[item].map(function(data) {
                    return Number((data * 100).toFixed(2));
                  });
                  dataObj.yAxis = 3;
                } else {
                  dataObj.data = dataList[item];
                }
                areaOption.series.push(dataObj);
              }
            }
          });
        } else {
          var tempChartData = chartData.chartData[0];
          Object.keys(tempChartData).forEach(function(key) {
            if (!selectedIndex[key]) {
              return ;
            }
            yAxisSwitch = tempChartData[key].every(function (item){
              if(item <= 1 && item >= 0) {
                return true;
              } else if(item > 1) {
                return false;
              }
            });
            dataObj = {
              name: key,
            };
            if (yAxisSwitch) {
              dataObj.data = tempChartData[key].map(function(data) {
                return Number((data * 100).toFixed(2));
              });
              dataObj.yAxis = 3;
            } else {
              dataObj.data = tempChartData[key];
            }
            areaOption.series.push(dataObj);
          });
        }
        $('.chartsRend').highcharts(areaOption);
      },

      ajax: function(url, type, data, callback) {
        if (!callback) {
          if (data) {
            callback = data;
            data = type;
          } else {
            callback = type;
            data = {};
          }
          type = "GET";
        }
        $.ajax({
          url: url,
          type: type,
          dataType: 'json',
          data: data,
          beforeSend: function(xhr){
              ajaxbg.show();
          },
          success: function(data) {
            callback(data);
          },
          complete:function(xhr){
              ajaxbg.hide();
          },
          error: function(){
              alert("网络连接失败！")
          }
        });
      }
    };

    function matcher(params, data) {
      var term =$.trim(params.term);
      if (term === '') {
        return data;
      }
      if (data.id.indexOf(term) !== -1 || data.text.indexOf(term) !== -1) {
        return data;
      }
      return null;
    }

    function formatNumber(str) {
      if (!str || isNaN(str)) {
        return str;
      }
      str = Number.parseFloat(str);
      if (str >= 1) {
        return str;
      }
      return (str * 100).toFixed(2) + '%';
    };

    DispatchKpi.init();
});