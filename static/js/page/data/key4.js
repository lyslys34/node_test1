;$(function() {
	var noop = function() {};
	// 默认显示的数据列
	var defaultSeries = ['完成配送骑手数','配送完成订单数', '配送成功率'];
	// highcharts
	var oChart;
	var chartDefaults = {
		title: {text: '数据监控'},
		credits:{ enabled:false },
		chart: {
			renderTo: 'highcharts',
			type: 'spline',
			events: {}
		},
		xAxis: {
			title: {text: '时间'},
			categories: []
		},
		yAxis: [
			{		// yAxis: 0, 百分比维度
				title: {text: '', style: {color: '#333'}},
				labels: {
					style: {
						color: '#333'
					},
					formatter: function() {
						return this.value + '%';
					}
				}
			},
			{		// yAxis: 1, 小数维度, 1~100
				title: {text: '', style: {color: '#E0850D'}},
				labels: {
					style: {
						color: '#E0850D'
					}
				}
			},
			{		// yAxis: 2, 大数量级, 1~1k
				title: {text: '', style: {color: '#0D08CC'}},
				labels: {
					style: {
						color: '#0D08CC'
					}
				},
				opposite: true
			},
			{		// yAxis: 3, 超大数量级, 1~1000K
				title: {text: '', style: {color: '#059C1A'}},
				labels: {
					style: {
						color: '#059C1A'
					}
				},
				opposite: true
			}
		],
		series: [],
		tooltip: {
			shared: true,
			crosshairs: {
				width: 1,
				color: 'orange'
			},
			headerFormat: '<b>{point.x}</b><br/>'
		},
		plotOptions: {
			series: {
				events: {
					legendItemClick: function() {
						var name = this.name;
						var visible = this.visible;
						if (visible) {		// 当前该数据列是可见的，即点击是为了隐藏数据列。故将对应name从默认显示中移除
							var index = $.inArray(name, defaultSeries);
							defaultSeries.splice(index, 1);
						}else {
							defaultSeries.push(name);
						}
					}
				}
			}
		}
	};
	// dataTable
	$.extend($.fn.dataTableExt.oSort, {						// datatables中文排序
	    "chinese-string-asc" : function (s1, s2) {
	        return s1.localeCompare(s2);
	    },
	    "chinese-string-desc" : function (s1, s2) {
	        return s2.localeCompare(s1);
	    }
	});
	// extend dataTable default settings
	$.extend($.fn.dataTable.defaults, {
		scrollX: true,
		scrollY: 'auto',
		paging: false,										// 分页
		orderClasses: true,
		deferRender: true,
		order: [],											// 默认不排序
		iDisplayLength: 200,									// 默认显示20条
		lengthMenu: [ [20, 50, -1], [20, 50, "全部"] ],		// 规定表格每页显示多少条
		language: {
			search: '搜索：',
			searchPlaceholder: '输入关键词',
			lengthMenu: '每页显示 _MENU_ 条',
			infoFiltered: "",
			paginate: {
				next: '下一页',
				previous: '上一页'
			}
		},
		columnDefs: [										// 按照中文字符排序
	       { type: 'chinese-string', targets: 0 }
	    ]
	});
	// banma
	var banma = {
		wrap: $('#main-container'),
		events: [
			['click', '#mainTable tr', '_chooseTableItem']			// 点击表格单元行将对应行数据绘制为highcharts
			,['mouseover', '#exportBtn', '_showExportMenu1']		// 导出
			,['mouseout', '#exportBtn', '_showExportMenu2']			// 导出
			,['click', '.export_table', '_exportTable']				// 导出表格
			,['click', '.export_chart', '_exportChart']				// 导出图表
			,['click', '.tab_item', '_tab']					// 点击Tab栏查询全部
			,['click', '#searchBtn', '_search']						// 查询
			,['mouseover', '.td_tip', '_showTip']
			,['mouseout', '.td_tip', '_removeTip']
		],
		$dataTable: null,
		orgType: window.orgType,
		parentId: 0,
		level: 400,
		_init: function() {
			var me = this;

			me._initForm();
			me._bind();
		},
		_initForm: function() {
			var me = this;
			me.parentId = me.level * -1;
			var post = {
				parentId: me.parentId
			}
			me._ajax('/data/monitor/orgTableData', post, me._paintTable);
		},
		_paintTable: function(res, me) {
			var data = res.data;
			var head = data.tableHead,
				tips = data.tableTips;
			var title = [];
			for (var i = 0, n = head.length; i < n; i ++) {
				title.push({head: head[i], tip: tips[i]});
			}
			// 要绘制的table数据
			var table = {
				title: title,
				body:  data.tableBody
			}
			// 利用js模板引擎渲染数据同时实例化tableData
			var html = template('J_table_tpl', table);
			var $table = $('#tableArea');
			$table.html(html);
			me.$dataTable = $table.find('#mainTable').dataTable();
			me.$dataTable.api().columns.adjust().draw(false);
			me._tdStyle();
			// 将table中的第一行数据绘制为highcharts
			me._paintIndexLine(0);
		},
		_tdStyle: function() {
			var $a = $('.td_a_btn');
			$a.each(function() {
				var text = $(this).text();
				if (text.length > 14) {
					text = text.substr(0, 14) + '...'
					$(this).text(text)
					$(this).addClass('has_full_name')
				}
			});
		},
		_paintIndexLine: function(index) {				//绘制第index行的数据
			var me = this;
			var $tr = $('#mainTable').find('tbody').find('tr').eq(index);
			var $td = $tr.find('td').eq(0);
			$tr.addClass('active').siblings('tr').removeClass('active');

			// 请求图表数据
			var orgId = $td.data('org');
			var level = $td.data('leveltype')
			// 标记当前level
			me.level = level;
			var post = {
				orgId: orgId,
				levelType: level
			}
			me._ajax('/data/monitor/orgChartData', post, me._paintChart);
		},
		_paintChart: function(res, me) {
			var data = res.data;
			var series = me._formatSeries(data.chartData);
			var xTime  = me._formatDate(data.chartAxis);
			var chartSettings = {
				xAxis: {
					title: {text: '时间'},
					categories: xTime
				},
				series: series
			}
			var settings = $.extend({}, chartDefaults, chartSettings);
			oChart = new Highcharts.Chart(settings);
		},
		_chooseTableItem: function(e) {
			var me = this;
			var $dom = $(e.target);
			if ($dom[0].tagName.toLowerCase() == 'td') {		// 确保点击的不是下钻按钮
				var $tr = $dom.parent();
				var index = $tr.index();
				me._paintIndexLine(index);
			}
		},
		_tab: function(e) {
			var me = this;
			var $tab = $(e.target);
			var level = $tab.data('level');
			me.level = level;
			me.parentId = me.level * -1;

			$tab.addClass('on').siblings('.tab_item').removeClass('on');
			var post = {
				parentId: me.parentId
			}
			me._ajax('/data/monitor/orgTableData', post, me._paintTable);
		},
		_search: function() {
			var me = this;

			var post = {
				parentId: me.parentId
			}
			var url = '/data/monitor/orgTableData';

			me._ajax(url, post, me._paintTable);
		},
		_showTip: function(e) {
			var $dom = $(e.target);
			var name = $dom.data('text');
			if (!name) {
				return false;
			}
			var rect = $dom[0].getBoundingClientRect();
			var x = e.pageX,
				y = rect.top + 20;

			$dom.append('<b class="tip">' + name + '</b>');
			$dom.find('b').css({
				left: x + 'px',
				top: y + 'px'
			});
		},
		_removeTip: function(e) {
			var $dom = $(e.target);
			$dom.find('b').remove();
		},
		_showExportMenu: function() {
			$('#exportMenu').toggleClass('show');
		},
		_showExportMenu1: function() {
			$('#exportMenu').addClass('show');
		},
		_showExportMenu2: function() {
			$('#exportMenu').removeClass('show');
		},
		_exportTable: function() {
			var me = this;
			var time = me._getTime();
			var url = '/data/monitor/orgTableDataExport';
			url += '?orgType=' + me.orgType + '&parentId=' + me.parentId + '&stime=' + time.stime + '&etime=' + time.etime;
			window.open(url);
		},
		_exportChart: function() {
			var me = this;
			var time = me._getTime();
			var $td = $('#mainTable').find('.active').find('td').eq(0);
			var orgId = $td.data('org');
			var id = $td.data('id');
			var url = '/data/monitor/orgChartDataExport?';
			url += 'levelType=' + me.level + '&orgId=' + orgId + '&stime=' + time.stime + '&etime=' + time.etime;
			window.open(url);
		},
		_formatSeries: function(obj) {					//将数据格式化为hightcharts中的数据列格式
			var me = this;
			var res = [];
			for (k in obj) {
				var temp = {};
				temp.name = k;
				temp.data = obj[k];
				temp.yAxis = me._yAxis(obj[k]);

				temp.visible = ($.inArray(k, defaultSeries) < 0) ? false : true;
				res.push(temp);
			}
			return res;
		},
		_yAxis: function(data) {						// 判断数据列对应的Y轴
			if (/%/.test(data[0])) {
				return 0;
			}else {
				var max = Math.max.apply(null, data);
				var res = (max > 1000 && 3) || (max > 100 && 2) || 1;
				return res;
			}
		},
		_formatDate: function(arr) {					// 将时间戳转为YYYY/MM/DD的格式显示在X轴
			var res  = [],
				n    = arr.length,
				date = new Date();
			while (n --) {
				date.setTime(arr[n]);
				var str = date.toLocaleDateString();
				res.unshift(str);
			}
			return res;
		},
		_getTime: function() {
			var time  = $('.reportrange span').html();
			var stime = time.substr(0, 8),
				etime = time.substr(-8);
			return {stime: stime, etime: etime}
		},
		_bind: function() {									// 绑定事件
			var me = this;
			var	context = me,
				events  = me.events,
				$wrap   = me.wrap;

			events && $.each(events, function(k, v) {
				var handler;
				me[v[2]] && (handler = me[v[2]]);

				var ev = v[0] + '.' + k,
					el = v[1];
				$wrap.off(ev).on(ev, el, function(e) {
					handler.apply(context, [e, this]);
				})
			})
		},
		_ajax: function(url, data, callback, type) {		// ajax
			var me = this;
			var type 	 = type || 'POST',
				callback = callback || noop;
			var oLoading = document.getElementById('loadingWrapper');
			// 每次查询检查时间条件
			var otime    = me._getTime();
			data.stime   = otime.stime;
			data.etime 	 = otime.etime;
			data.orgType = me.orgType;
			$.ajax({
				url: url,
				type: type,
				dataType: 'json',
				data: data,
				success: function(res) {
					console.log(res);
					oLoading.classList.remove('show');
					callback(res, me);
				},
				beforeSend: function() {
					oLoading.classList.add('show');
				},
				error: function() {
					// alert('请求{' + url + '}出错!')
					oLoading.classList.remove('show');
				}
			})
		}
	}
	banma._init();
})