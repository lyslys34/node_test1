/**
 * User: xuyc
 * Date: 14-11-1
 * Time: 下午7:11
 */
require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root', 'module/validator','module/cookie'], function (t, validator,cookie) {

    // 添加配送人员
    $(document).delegate('.btn-add-dispatcher', 'click', function () {
        _showEditPanel();
    });

    // 编辑配送人员
    $(document).delegate('.btn-edit-dispatcher', 'click', function () {
        _showEditPanel($(this).parents('tr'));
    });

    // 删除配送人员
    $(document).delegate('.btn-delete-dispatcher', 'click', function () {
        _showDeletePanel($(this).parents('tr'));
    });

    var _openPois;

    // 已开通的商家
    function _queryOpenPois(callback) {
        if (!_openPois) {
            t.utils.post('team_manager/list', function (r) {
                if (r.httpSuccess && r.data.code == 0) {
                    callback(true, _openPois = r.data.data);
                }
                else {
                    callback(false);
                }
            });
        }
        else {
            callback(true, _openPois);
        }
    }

    // 添加/编辑配送人员
    function _showEditPanel(d) {
        var append = !d, _prompt = '', _dispatcherId;
        var body = $(
            '<div class="dialog">' +
            '   <table>' +
            '       <tr>' +
            '           <td colspan="2"><label class="err-msg" style="padding-left: 10px;"></label></td>' +
            '       </tr>' +
            '       <tr>' +
            '           <td>姓名：</td>' +
            '           <td><input type="text" class="name form-control"  maxlength="20"/></td>' +
            '           <td>手机号：</td>' +
            '           <td><input type="text" class="mobile form-control" maxlength="11" /></td>' +
            '       </tr>' +
            '       <tr>' +
            '           <td colspan="2"><label></label></td>' +
            '       </tr>' +
            '   </table>' +
            '</div>'
        );

        if (d) {
            $('.name', body).val(d.data('name'));
            $('.mobile', body).val(d.data('mobile'));
        }

        //var poiList = $('#poi-list', body);
        //_queryOpenPois(function (success, list) {
        //
        //    success && $(list).each(function (k, it) {
        //
        //        var li = $('<li><input type="checkbox" /><label></label></li>').appendTo(poiList)
        //            .data('id', it.wmPoiId).attr('id', 'li-poi-' + it.wmPoiId);
        //
        //        $('input[type=checkbox]', li).attr('id', 'li-checkbox-' + it.wmPoiId);
        //        $('label', li).text(it.poiName).attr('for', 'li-checkbox-' + it.wmPoiId);
        //    });
        //
        //    // 所负责商家复选框
        //    if (d) {
        //        t.utils.post('team_manager/listOfDispatcherId', {dispatcher_id: d.data('dispatcherid')}, function (r) {
        //            if (r.httpSuccess && r.data.code == 0) {
        //                $(r.data.data).each(function (k, it) {
        //                    var input = $('#li-poi-' + it.wmPoiId + ' input', body);
        //                    input.attr('checked', true);
        //                });
        //            }
        //            else {
        //                alert('获取商家列表时出现错误');
        //            }
        //
        //        });
        //    }
        //});

        var e_email = $('.email', body), e_name = $('.name', body), e_mobile = $('.mobile', body), e_errMsg = $('.err-msg', body);

        function _setErrMsg(msg, clear) {
            if (clear) {
                e_name.val('');
                //e_mobile.val('');
            }
            e_errMsg.text(msg);
        }

        //$('.mobile', body).bind('change', function() {
        //
        //    var e_mobile = e_mobile.val();
        //    if(validator.isMobile(e_mobile))
        //    {
        //        _prompt = '正在获取用户信息';
        //        t.utils.post('team_manager/getUserInfo', { mobile: e_mobile }, function(r) {
        //
        //            if(r.httpSuccess && r.data.code == 0)
        //            {
        //                var d = r.data.data;
        //                if(!d)
        //                {
        //                    _setErrMsg(_prompt = '该用户不存在', true);
        //                }
        //                else
        //                {
        //                    _dispatcherId = d.dispatcherId;
        //                    if($('.dispatcher').any(function() { return $(this).data('dispatcherid') == _dispatcherId }))
        //                    {
        //                        _setErrMsg(_prompt = '该用户已经在成员列表中');
        //                    }
        //                    /*else if(d.teamId)
        //                    {
        //                        _setErrMsg(_prompt = '该用户已属于其它配送团队');
        //                    }*/
        //                    else
        //                    {
        //                        _setErrMsg(_prompt = '');
        //                    }
        //
        //                    e_name.val(d.name);
        //                    e_mobile.val(d.mobile);
        //                }
        //            }
        //            else
        //            {
        //                _setErrMsg(_prompt = '获取该用户信息时出现错误', true);
        //            }
        //
        //        });
        //    }
        //});

        //function _getPoiIds() {
        //    var ids = [];
        //    $('li', body).each(function () {
        //        li = $(this);
        //        if ($('input', li)[0].checked) ids.push(li.data('id'));
        //    });
        //
        //    return ids;
        //}

        var dlg = t.ui.showModalDialog({
            title: d ? '修改配送员' : "添加配送员", body: body, buttons: [

                t.ui.createDialogButton('ok', function () {        // 确定

                    var $errorMsgTd = $(".err-msg", body);

                    var name = e_name.val();
                    var mobile = e_mobile.val();
                    //var poiIds = _getPoiIds().join(',');

                    //if (validator.isBlank(poiIds)) {
                    //    $errorMsgTd.text("请至少选一个商家");
                    //    return;
                    //}

                    if (validator.isBlank(name)) {
                        $errorMsgTd.text("配送员名称不能为空");
                        return;
                    }

                    if (!validator.isMobile(mobile)) {
                        $errorMsgTd.text("请输入格式正确的配送员手机号");
                        return;
                    }

                    if (_prompt) {
                        t.ui.showNotification(_prompt);
                        return false;
                    }

                    var postData = {
                        name: e_name.val(),
                        mobile: e_mobile.val()
                        //poiIds: _getPoiIds().join(',')
                    };

                    if (d) {
                        postData['bmUserId'] = d.data('id');
                    }


                    t.utils.post(append ? 'create.ajax' : 'update.ajax', postData, function (r) {
                        if (r.httpSuccess) {
                            if(r.data.code == 0) {
                                window.location.href=append ? "list?pageNo=1" : window.location.href;
                            } else {
                                alert((append ? '添加' : '更新') + '配送员信息失败,' + r.data.msg);
                                return false;
                            }

                        } else {
                            alert((append ? '添加' : '更新') + '配送员信息失败, 系统错误');
                            return false;
                        }
                    });

                    return true;
                }),

                t.ui.createDialogButton('close', function () {     // 取消

                })

            ]
        });
    }

    // 删除配送人员
    function _showDeletePanel(d) {
        if (!confirm('确定要删除“' + d.data('name') + "“吗?")) return;
        t.utils.post('delete.ajax', {bmUserId: d.data('id')}, function (r) {

            if (r.httpSuccess) {
                if(r.data && r.data.code == 0) {
                    window.location.reload();
                } else {
                    alert("删除配送员信息失败，" + r.data.msg);
                }
            } else {
                //alert(JSON.stringify(r));
                alert('删除配送人员时出现错误');
            }
        });
    }

});
