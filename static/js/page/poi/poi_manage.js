require.config({
  baseUrl: MT.STATIC_ROOT + '/js',
  urlArgs: 'ver=' + pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});

require(['module/root', 'module/validator', 'module/cookie'], function(t, validator, cookie) {

  var map, polygon;
  var pageNum = 1;
  var isManager = '0';
  var canOperation = '0';
  var globalPoiIds = '';
  var globalPoiIdNames = '';
  var globalWorkstatus = '';
  var cityAreaInfo = null;


  var poi_data_dynamic = new Array("poiId",
    "poiName",
    "poiPhone",
    "bdName",
    "poiAddress",
    "cityName",
    "deliveryAreaName",
    "worktime",
    "level",
    "online",
    "workstatus",
    "priceRegion",
    "postageRegion",
    "waybillCount",
    "normalDeliveryTime",
    "extendDeliveryTime");
  var cityId = '';
  var deliveryAreaId = '';
  var poiName = '';
  var poiId = '';
  var workstatus = '';
  var flag = 0;

    $(document).ready(function() {
        isManager = $('#isManager').text();
        canOperation = $('#canOperation').text();
        isBaseManager = $('#isBaseManager').text();
        map_init();

        search_init();
        fillCitySearchList();

        map_init();
        if( poiType != 1 ){
            hidePoiOpBtn();
        }else{
            showPoiOpBtn();
        }
        $('#search').click(function () {
                pageNum=1;
                poiName = $('#poi-name').val();
                poiId = $('#poi-id').val();
                console.log( poiName + " " + poiId);
                workstatus = $('#work-status').val();
                get_poi_data();
        });
        $('#agency-tab').click(function () {
                poiType = '1';
                showPoiOpBtn();
                $("#view-map-btn").hide();
                search_init();
                pageNum = 1;
                get_poi_data();
        });
        $('#normal-tab').click(function () {
                poiType = '-1';
                hidePoiOpBtn();
                $("#view-map-btn").show();
                search_init();
                pageNum = 1;
                get_poi_data();
        });
        $('#poi-lbs-tab').click(function () {
            poiType = '-1';
            hidePoiOpBtn();
            $("#view-map-btn").hide();
            search_init();
            pageNum = 1;
            get_poi_data();
        });
        $('#view-map-btn').click(function(){
            cityId = city_s.select2("val");
            deliveryAreaId = region_s.select2("val");
            if( ( !isNaN(cityId) && cityId!=undefined ) && (!isNaN(deliveryAreaId) && deliveryAreaId!=undefined) ){
                url = "/poi/mapview?cityId="+cityId+"&areaId="+deliveryAreaId;
                window.open(url,'_blank');
            }else{
                alert("请选定城市或者区域");
            }

        });
        $('#checkbox-all').change(function() {

          if ($(this).val() == 'check') {
            $(this).val('nocheck');
            $("input[name='check-poi']:checked").each(function() {
              $(this).removeAttr('checked');
            })
          } else {
            $(this).val('check')
            $("input[name='check-poi']").each(function() {
              $(this).prop('checked', true)
            });
          }
        });
        $('.check-poi').change(function() {
          $('#checkbox-all').removeAttr('checked');
          $('#checkbox-all').val('nocheck');
        });
        $('#commit-extend-delivery-time').click( function(){
            if( globalPoiIds == '' ){
                getPoiIdsStr();
            }

            if( globalPoiIds == '' ){
                alert("请选择指定商家再操作");
                return ;
            }

            var poiData = {poiIds:globalPoiIds,poiIdNames:globalPoiIdNames};
            var timeData = getExtendDeliveryTime();

            poiData = $.extend(poiData,timeData);

            ajaxExtenddeliveryTime(poiData);
        });
        $('#commit-close-pois').click( function(){
            if( globalPoiIds == '' ){
                getPoiIdsStr();
            }

            if( globalPoiIds == '' ){
                alert("请选择指定商家再操作");
                return ;
            }

            var poiData = {poiIds:globalPoiIds,poiIdNames:globalPoiIdNames, workstatus:0};
            var timeData = getCloseEndTime();
            var target = $("#cancel-close-pois");

            poiData = $.extend(poiData,timeData);

            ajaxUpdatePoiWorkStatus(poiData, target);
        });
        $("#commit-open-pois").click( function(){
            if( globalPoiIds === '' ){
                getPoiIdsStr();
            }

            if( globalPoiIds === '' ){
                alert("请选择指定商家再操作");
                return ;
            }

            var poiData = {poiIds:globalPoiIds,poiIdNames:globalPoiIdNames,workstatus:1};
            var target = $("#cancel-open-pois");

            ajaxUpdatePoiWorkStatus(poiData, target);
        });
        $("#extend-delivery-time-btn").click(function (){
            getPoiIdsStr();
            if( globalPoiIds == '' ){
                alert("请选择指定商家再操作");
                return;
            }else{
                fillSelectContent(extend_end_time_s);
                $("#extend-delivery-time").modal("show");
            }
        });

        $("#open-poi-btn").click( function(){

            getPoiIdsStr();
            if( globalPoiIds == '' ){
                alert("请选择指定商家再操作");
                return;
            }else{
                $("#open-pois").modal("show");
            }
        });
        $("#close-poi-btn").click( function(){
            getPoiIdsStr();

            if( globalPoiIds == '' ){
                alert("请选择指定商家再操作");
                return;
            }else{
                fillSelectContent(close_end_time_s);
                $("#close-pois").modal("show");
            }
        });
        city_s.on("change",function(){
            cityId = city_s.select2("val");
            //region_s.select2("val","");
            fillAreaSearchList();
        });
        region_s.on("change",function(){
            deliveryAreaId = region_s.select2("val");
        });
        extend_end_time_s.on("click", function(){
          fillSelectContent(extend_end_time_s);
        });
        close_end_time_s.on("click", function(){
          fillSelectContent(close_end_time_s);
        });
        removeSome();
  });

  pager_click = function(link_num) {
    pageNum = link_num;
    get_poi_data();
  }

  view_area = function(view_link) {
    $('#area-map').modal('show');
    var id = view_link.parent().parent().find(".poiId").text();
    $.getJSON("/poi/deliveryArea", {
        poiId: id
      })
      .done(function(json) {
        code = json.code;
        data = json.data.area;
        if (code === 0) {
          if( data.length == 0 ){
            alert("暂时没有营业范围数据！");
            return ;
          }
          var polygonArrs = new Array();
          map_init();
          drawLocation( json.data.location );
          for (var i in data) {
            var polygonArr = new Array();
            var area = JSON.parse(data[i]);

            area.forEach(function(e) {
              polygonArr.push(new AMap.LngLat(e.y / 1000000.0, e.x / 1000000.0));
            })
            draw_area(polygonArr);
          }
        } else {
          alert("获取数据失败");
        }
      })
      .fail(function() {
        alter("请求数据失败");
      })

  }

    displayFetchPoints = function (item) {
        var liitem = item.parent();
        var id = liitem.attr("data-poi-id");
        liitem.parent().find('.active').removeClass('active');
        liitem.addClass('active');

        var itemCount = 0;
        var fetchTimes = 0;
        var endTime = 0;
        var poiLocation = [];

        var fetchNextPage = function() {
            fetchPageData();
        };

        initPoiFetchPointsMap();

        var heatmap = null;

        var deliveryAreaFunc = function() {
            $.getJSON("/poi/deliveryArea", {
                poiId: id
            }).done(function(json) {
                code = json.code;
                data = json.data.area;
                if (code === 0) {
                    drawPoiLocation( json.data.location );

                    fetchPageData();
                }else {
                    $("#point-loading").hide();
                }
            }).fail(function() {
                    $("#point-loading").hide();
                    alter("请求数据失败");
                });
        }

        var drawPoiLocation = function(location) {
            poiLocation = location;
            lat = location.latitude/1000000.0;
            long = location.longitude/1000000.0;
            marker = new AMap.Marker({
                //icon: "/static/imgs/getGoods.png",
                position: [long, lat]
            });
            marker.setMap(map);

            var info = [];
            info.push("<b>" + liitem.attr('data-poi-name') + "</b>");
            info.push("  商家ID : " + liitem.attr('data-poi-id'));
            info.push("  状态: " + liitem.attr('data-poi-status'));
            info.push("  配送区 : " + liitem.attr('data-poi-cityName') + " " + liitem.attr('data-poi-deliveryAreaName'));
            info.push("  商家地址 : " + liitem.attr('data-poi-poiAddress') + " " + liitem.attr('data-poi-poiAddress'));

            var inforWindow = new AMap.InfoWindow({
                offset : new AMap.Pixel(0, -20),
                content: info.join("<br>"),
            });

            AMap.event.addListener(marker,"click",function(e){
                inforWindow.open(map, marker.getPosition());
            });
            map.setFitView();
        }

        var updateProgress = function(percent) {
            if(percent < 0) {
                percent = fetchTimes * 100 / 50;
                if(percent > 100) {
                    percent = 100;
                }
            }

            $('#point-loading .progress-bar').attr('style', 'width:' + percent + '%').text(percent + '%');
            $('#point-loading .progress-value').text("已经加载 " + itemCount + " 个取餐数据……");
        }

        var fetchPageData = function() {
            updateProgress(-1);
            $.getJSON("/poi/getPoiRecentFetchPoints", {
                poiId: id,
                endTime: endTime,
            }).done(function (json) {
                if(json.code == 0) {
                    endTime = json.data.endTime;
                    itemCount += json.data.traces.length;
                    ++fetchTimes;
                    if(json.data.traces.length > 0) {
                        drawFetchPoints(json.data.traces);
                    }
                    if(itemCount < 250 && fetchTimes < 50) {
                        fetchNextPage();
                    } else {
                        updateProgress(100);
                        $("#point-loading").hide();

                        if(itemCount == 0) {
                            alert("最近没有骑手取餐数据！");
                        }else {
                            map.setFitView();
                        }
                    }
                } else {
                    $("#point-loading").hide();
                }
            }).fail(function () {
                $("#point-loading").hide();
                alert('获取数据失败！');
            });
        };

        var drawFetchPoints = function (traces) {
            if(heatmap == null) {
                map.plugin(["AMap.Heatmap"],function() {      //加载热力图插件
                    heatmap = new AMap.Heatmap(map, {radius: 20, opacity: [0, 0.8]});    //在地图对象叠加热力图
                    heatmap.setDataSet({data:[], max:100}); //设置热力图数据集
                });
            }
            for (var i in traces) {
                var point = traces[i];
                lat = point.lat / 1000000.0;
                lng = point.lng / 1000000.0;
                heatmap.addDataPoint(lng, lat, 10);
            }
        }

        updateProgress(0);
        $("#point-loading").show();
        deliveryAreaFunc();
    }




  function get_poi_data() {
    workstatus = poi_s.select2("val");
    cityId = city_s.select2("val");
    deliveryAreaId = region_s.select2("val");
    poiId = $("#poi-id").val();
    poiName = $("#poi-name").val();
    $('#search')[0].disabled=true;
    $('#poi-table').empty();
    $.getJSON("/poi/search", {
        pageSize: 20,
        pageNum: pageNum,
        poiType: poiType,
        cityId: cityId,
        deliveryAreaId: deliveryAreaId,
        poiName: poiName,
        poiId: poiId,
        workstatus: workstatus
      })
      .done(function(json) {
            if($('#tool-poi-lbs.active').length == 0) {
                rendererPoiTable(json);
            }else{
                 rendererPoiList(json);
            }
            $('#search')[0].disabled=false;
      })
      .fail(function(jqxhr, textStatus, error) {
        $('#search')[0].disabled=false;
        var err = textStatus + ", " + error;
      });

  }
    function rendererPoiList(json) {
        var code = json.code;
        if( code != 0 ){
            alert("获取数据失败");
            $('#search')[0].disabled=false;
            return;
        }
        pageSize = json.data.pageSize;
        pageTotal = json.data.totalPage;
        total = json.data.total;
        if (json.data.pageSize > json.data.total) {
            pageSize = json.data.total;
        };
        var poiItemHtml = '<li class="list-group-item" data-poi-id="{data-poi-id}" data-poi-name="{data-poi-name}" data-poi-status="{data-poi-status}" data-poi-cityName="{data-poi-cityName}" data-poi-deliveryAreaName="{data-poi-deliveryAreaName}" data-poi-poiAddress="{data-poi-poiAddress}"><a href="javascript:;" onclick="displayFetchPoints($(this));"><p><b>{POINAME}</b></p><p class="poi-detail">商家ID：{data-poi-id} 状态：{STATUS}</p></a></li>';
        gen_pager(pageTotal, pageNum, total);

        $('#poi-list').empty();
        for (var i = 0; i < json.data.poiList.length; i++) {
            var poiItem = json.data.poiList[i];
            var pi = poiItem.poiName;
            var poiId = poiItem.poiId;
            var status = poiItem.workstatus == 3 ? '休息' : '营业';
            var deliveryAreaName = poiItem.deliveryAreaName;
            var cityName = poiItem.cityName;
            var poiAddress = poiItem.poiAddress;

            var itemHtml = poiItemHtml.replace('{POINAME}', pi)
                .replace('{data-poi-id}', poiId)
                .replace('{data-poi-id}', poiId)
                .replace('{data-poi-cityName}', cityName)
                .replace('{data-poi-deliveryAreaName}', deliveryAreaName)
                .replace('{data-poi-poiAddress}', poiAddress)
                .replace('{data-poi-name}', pi)
                .replace('{data-poi-status}', status)
                .replace('{STATUS}', status);
            $('#poi-list').append( itemHtml );
            removeSome();
        }
    }

    function rendererPoiTable(json) {
        var code = json.code;
        if( code != 0 ){
            alert("获取数据失败");
            $('#search')[0].disabled=false;
            return;
        }
        pageSize = json.data.pageSize;
        pageTotal = json.data.totalPage;
        total = json.data.total;
        if (json.data.pageSize > json.data.total) {
            pageSize = json.data.total;
        };

        var poiDataHtml = '<tr> <td class="cannot_see"><input style="opacity: 1;padding-top:0px;margin-top:1px;" id="check-poi" type="checkbox" name="check-poi"/></td> <td class="poiId"></td> <td class="poiName" style="width:150px;"></td> <td class="cityName"></td> <td class="deliveryAreaName" style="width:100px;"></td> <td class="worktime"></td><td class="poiPhone"></td> <td class="poiAddress" style="width:150px;"></td> <td class="level agency_cansee" style="display: none;"></td> <td class="online"></td> <td class="workstatus"></td> <td>¥ <span class="priceRegion"></span></td> <td>¥ <span class="postageRegion"></span></td> <td><span class="waybillCount"></span></td> <td><span class="normalDeliveryTime"></span>分钟<span class="extendDeliveryTime" style="display: none;color: red;"></span></td> <td class="bdName"></td><td><a href="javascript:;" onclick="view_area($(this));">查看</a></td> <td class="cannot_see"><a href="javascript:void(0)" onclick="extendPoiDeliveryTime($(this));" style="padding:8px">延长配送时间</a>';
        if( poiType == 1 ){
            poiDataHtml = poiDataHtml+'<a href="javascript:void(0)" onclick="updatePoiAsOpened($(this))" style="padding:8px" class="open-poi-a">开始营业</a><a href="javascript:void(0)" onclick="updatePoiAsClosed($(this))" style="padding:8px" class="close-poi-a">停止营业</a>';
        }
        poiDataHtml = poiDataHtml + "</td> </tr>";

        gen_pager(pageTotal, pageNum, total);
        $('#poi-table').empty();
        for (var i = 0; i < json.data.poiList.length; i++) {
            $('#poi-table').append( poiDataHtml );
            removeSome();
        }
        var i = 0;
        json.data.poiList.forEach(function(poi) {
            for (x in poi_data_dynamic) {
                fill_text = poi[poi_data_dynamic[x]];
                if (poi_data_dynamic[x] == 'extendDeliveryTime' && fill_text != 0) {
                    $('.extendDeliveryTime:eq(' + i + ')').show();
                    fill_text = '+延长' + fill_text + '分钟';
                };
                if (poi_data_dynamic[x] == 'online') {
                    if (fill_text == 1) {
                        fill_text = '上线';
                    } else {
                        fill_text = '下线';
                    };
                };
                if( poi_data_dynamic[x] == "waybillCount" ){
                    fill_text = fill_text.toFixed(1);
                }
                if (poi_data_dynamic[x] == 'workstatus') {
                    if (fill_text == 3) {
                        fill_text = '休息';
                    } else {
                        if( poiType == 1 ){
                        }
                        fill_text = '营业';
                    };
                };
                if (poi_data_dynamic[x] == 'worktime' || poi_data_dynamic[x] == 'poiPhone' || poi_data_dynamic[x] == 'poiAddress'){
                    if( fill_text == '' ){
                        fill_text = '无';
                    };
                    fill_text = fill_text.replace(/,/g,"<br/>")
                };
                if (poi_data_dynamic[x] == 'bdName'){
                    var bdName = poi['bdName']||"无BD";
                    var bdPhone = poi['bdPhone']||"无电话";
                    fill_text = bdName+"/"+bdPhone;
                }
                $('.' + poi_data_dynamic[x] + ':eq(' + i + ')').append(fill_text);
            }
            i++;
        })
    }

  function removeSome() {
    if (canOperation == '' || canOperation == '0') {
      $('.cannot_see').remove();
    };
    if (poiType == '1') {
      $('.agency_cansee').show();
    } else {
      $('.agency_cansee').hide();
    };
  }

  function gen_pager(pageTotal, pageNum, total) {
    $('#pager').empty();
    if (total==0) {
      $('#pager').append('<li class="disabled"><a href="javascript:;">上一页</a></li>');
      $('#pager').append('<li class="disabled"><a href="javascript:;">下一页</a></li>');
      return false;
    }
    if (pageNum == 1) {
      $('#pager').append('<li class="disabled"><a href="javascript:;">上一页</a></li>');
    } else {
      $('#pager').append('<li><a href="javascript:;" onclick="pager_click(pageNum-1);">上一页</a></li>');
    };
    if (pageTotal <= 7) {
      for (var i = 1; i <= pageTotal; i++) {
        if (pageNum == i) {
          $('#pager').append('<li class="active page-n"><a>' + i + '</a></li>');
          continue;
        };
        $('#pager').append('<li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">' + i + '</a></li>');
      }
    } else {
      pageNum = parseInt(pageNum);
      diff_t = pageTotal - pageNum;
      diff_1 = pageNum - 1;
      if (diff_1 > 2) {
        pageNum = parseInt(pageNum);
        if (diff_t < 3) {
          $('#pager').append('<li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">1</a></li><li><a class="page-n" href="javascript:;">...</a></li>');
          for (var i = parseInt(pageNum) - 1; i <= pageTotal; i++) {
            if (pageNum == i) {
              $('#pager').append('<li class="active page-n"><a>' + i + '</a></li>');
              continue;
            };
            $('#pager').append('<li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">' + i + '</a></li>');
          }
        } else {
          $('#pager').append('<li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">1</a></li><li><a class="page-n" href="javascript:;">...</a></li>');
          for (var i = parseInt(pageNum) - 1; i <= pageNum + 1; i++) {
            if (pageNum == i) {
              $('#pager').append('<li class="active page-n"><a>' + i + '</a></li>');
              continue;
            };
            $('#pager').append('<li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">' + i + '</a></li>');
          }
          $('#pager').append('<li><a class="page-n" href="javascript:;">...</a></li><li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">' + pageTotal + '</a></li>');
        };
      } else {
        for (var i = 1; i <= parseInt(pageNum) + 1; i++) {
          if (pageNum == i) {
            $('#pager').append('<li class="active page-n"><a>' + i + '</a></li>');
            continue;
          };
          $('#pager').append('<li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">' + i + '</a></li>');
        }
        $('#pager').append('<li><a class="page-n" href="javascript:;">...</a></li>');
        $('#pager').append('<li><a class="page-n" href="javascript:;" onclick="pager_click($(this).text());">' + pageTotal + '</a></li>');
      };
    };
    if (pageNum == pageTotal) {
      $('#pager').append('<li class="disabled"><a href="javascript:;">下一页</a></li>');
    } else {
      $('#pager').append('<li><a href="javascript:;" onclick="pager_click(pageNum+1);">下一页</a></li>');
    };
  }

    function search_init () {
        city_s.select2('val', selectedCityId);
        region_s.select2('val', selectedAreaId);

        //poi_s.select2('val', '-1');
        cityId = '';
        deliveryAreaId = '';
        poiName = '';
        poiId = '';
        workstatus = '';


        //$("#poi-id").val("");
        //$("#poi-name").val("");

    }

  function map_init() {
    //初始化地图对象，加载地图
    map = new AMap.Map("mapContainer", {
      resizeEnable: true,
      //二维地图显示视口
      view: new AMap.View2D({
        center: new AMap.LngLat(116.397428, 39.90923), //地图中心点
        zoom: 100 //地图显示的缩放级别
      })
    });
    //添加地图侧边栏插件
    map.plugin(["AMap.ToolBar"], function() {
      toolBar = new AMap.ToolBar();
      map.addControl(toolBar);
    });
    //addmarker

  }

    function initPoiFetchPointsMap() {
        //初始化地图对象，加载地图
        if(map != null) {
            map.clearMap();
        }
        map = new AMap.Map("poiFetchPointsMapContainer", {
            resizeEnable: true,
            //二维地图显示视口
            view: new AMap.View2D({
                center: new AMap.LngLat(116.397428, 39.90923), //地图中心点
                zoom: 100 //地图显示的缩放级别
            })
        });

        //添加地图侧边栏插件
        map.plugin(["AMap.ToolBar"], function() {
            toolBar = new AMap.ToolBar();
            map.addControl(toolBar);
        });

        map.on( 'click', function (e) {
            var lngX = e.lnglat.getLng() * 1000000.0;
            var latY = e.lnglat.getLat() * 1000000.0;
            $('#locationXY').val(lngX + ',' + latY);
        });

    }

  function draw_area(polygonArr) {
    polygon = new AMap.Polygon({
      map: map,
      path: polygonArr, //设置多边形边界路径
      strokeColor: "#FF33FF", //线颜色
      strokeOpacity: 0.2, //线透明度
      strokeWeight: 3, //线宽
      fillColor: "#1791fc", //填充色
      fillOpacity: 0.35 //填充透明度
    });
    polygon.setMap(map);
    map.setFitView();

  }

  function drawLocation( address ) {
        lat = address.latitude/1000000.0;
        long = address.longitude/1000000.0;
        marker = new AMap.Marker({
          //icon: "http://webapi.amap.com/images/marker_sprite.png",
          position: [long, lat]
        });
        marker.setMap(map);
  }

  function getCityList() {
  if(isManager == '0') {
    return false;
  }
    initPost('/org/citySearchList', {}, function(data) {
      for (var i in data.data.data) {
        var city = data.data.data[i].value;
        var id = data.data.data[i].id;
        $('#city-selector').append('<option value="' + id + '" >' + city + '</option>');
      }
    });
  }

  function initPost(url, data, callback) {
    if (typeof(data) == 'function') {
      callback = data;
      data = null;
    }
    $.ajax({
      type: 'POST',
      url: url,
      data: data,
      success: function(r) {

        callback({
          httpSuccess: true,
          data: r
        });

      },
      error: function(XmlHttpRequest, textStatus, errorThrown) {

        callback({
          httpSuccess: false,
          statusCode: XmlHttpRequest.status
        });

      }
    });
  }

  function getPoiIdsStr(){
    var allCheck = $('#checkbox-all').val();
    var poiIds = new Array();
    var poiIdNames = new Array();

    $('input[name="check-poi"]:checked').each( function(){
        var parent = $(this).parent()
        var poiId = ( parent.next().text());
        var poiName = (parent.next().next().text());
        poiIdNames.push( poiId+"##"+poiName);
        poiIds.push( poiId);
    });
    globalPoiIds = poiIds.join(",");
    globalPoiIdNames = poiIdNames.join(",");

  }

  function ajaxExtenddeliveryTime(data){
    $.post("/poi/extendDeliveryTime", data)
    .done(function(json) {
        var code = json.code;
        if( code==0 ){
            alert("修改设置成功");
            $("#cancel-extend-delivery-time").click();
            $('#search').click();
        }else{
            alert(json.msg);
            $("#cancel-extend-delivery-time").click();
        }

        globalPoiIdNames = '';
        globalPoiIds = '';
    });
  }

  function getExtendDeliveryTime(){
    var time = $("#extend-time").val();
    var endTime = $("#extend-end-time").val();

    return {time:time, endTime:endTime};
  }

  function getCloseEndTime(){
     var endTime = $("#close-end-time").val();
     //var compareTime = $("#close-end-time").find("option")[0].value;

     return {endTime:endTime};
  }

  extendPoiDeliveryTime = function (extendLink){
    $('#extend-delivery-time').modal('show');
    fillSelectContent(extend_end_time_s);
    globalPoiIds = extendLink.parent().parent().find(".poiId").text();
    globalPoiIdNames = extendLink.parent().parent().find(".poiName").text();
    globalPoiIdNames = globalPoiIds+"##"+globalPoiIdNames;
  }

  function hidePoiOpBtn(){
    $("#close-poi-btn").hide();
    $("#open-poi-btn").hide();
  }

  function showPoiOpBtn(){
    $("#close-poi-btn").show();
    $("#open-poi-btn").show();
  }

  updatePoiAsOpened = function(openLink){
    $('#open-pois').modal('show');
    globalWorkstatus = 1;
    globalPoiIds = openLink.parent().parent().find(".poiId").text();
    globalPoiIdNames = openLink.parent().parent().find(".poiName").text();
    globalPoiIdNames = globalPoiIds+"##"+globalPoiIdNames;
  }

  updatePoiAsClosed = function(closeLink){
    $('#close-pois').modal('show');
    fillSelectContent(close_end_time_s);
    globalWorkstatus = 0;
    globalPoiIds = closeLink.parent().parent().find(".poiId").text();
    globalPoiIdNames = closeLink.parent().parent().find(".poiName").text();
    globalPoiIdNames = globalPoiIds+"##"+globalPoiIdNames;
  }

  function ajaxUpdatePoiWorkStatus(data, target){
   $.post("/poi/setWorkstatus", data)
       .done(function(json) {
           var code = json.code;
           if( code==0 ){
               alert("修改设置成功");
               target.click();
               $('#search').click();
           }else{
               alert(json.msg);
               target.click();
           }

           globalPoiIdNames = '';
           globalPoiIds = '';
           globalWorkstatus = '';
       });
  }

  function fillSelectContent(target){
    //target.select2('val','');
    //target.empty();
    $.get("/poi/getSelectTime",{})
        .done( function (json){
            var code = json.code;
            target.empty()
            if( code == 0 ){
                data = json.data;
                var first = 0;
                for( i in data ){
                    var obj = data[i];
                    for( j in obj ){
                        target.append('<option value="' + j + '" >' + obj[j] + '</option>');
                        if( first === 0){
                            target.val(j);
                        }
                        first = first + 1;
                    }
                }
            }else{
                alert("获取时间列表失败，请重新获取");
            }
        })
        .fail( function (){
        });
  }

  function fillCitySearchList(){
     $.get("/poi/getCityAreaInfo",{})
     .done(function(json){
         var code = json.code;
         var data = json.data;
         if( code == 0 ){
             cityAreaInfo = data;
             var count = 0;
             var firstCityId = -1;
             for(i in data){
                 var city = data[i];
                 if( count == 0 ){
                    firstCityId = city.cityId;
                 }
                 count += 1;
                 $("#city-selector").append("<option value='"+city.cityId+"'>"+city.cityName+"</option>");
             }
             if(selectedCityId==-1){
                selectedCityId = firstCityId;
             }
             city_s.select2("val",selectedCityId);
         }else{
             alert("获取城市搜索列表数据失败");
         }
     })
     .fail(function(){
         alert("获取城市搜索列表请求失败");
     });
  }

  function fillAreaSearchList(){

      cityId = city_s.select2("val")
      if( cityId==null || cityId=="" ){
          return ;
      }
      //areaSelector.select2('val','')
      region_s.empty();

      var areaInfoList = cityAreaInfo[ cityId ].areaInfoVoList;
      var count = 0;
      var firstAreaId;
      for(i in areaInfoList){
          var areaDetailInfo = areaInfoList[i];

          if( count==0 ){
            firstAreaId = areaDetailInfo.areaId;
          }
          count += 1;
          region_s.append("<option value='"+areaDetailInfo.areaId+"'>"+areaDetailInfo.areaName+"</option>");
      }
      if(selectedAreaId==-1){
        selectedAreaId = firstAreaId;
      }
      region_s.select2("val",selectedAreaId);
      if( flag ==0 ){
        get_poi_data();
        flag += 1;
      }

  }
});
