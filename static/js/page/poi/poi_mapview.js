require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu',
        ],
        'lib/autocomplete' : {
            deps : ['page/common']
        },
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});
require(['module/root', 'module/validator',  'module/cookie', 'lib/autocomplete'], function (t, validator, cookie, autocomplete) {

    var map;
    var poiLocationMarker = new Object();
    var areaPolygon = null;
    var aorPolygonList = new Array();
    var poiAreaPolygonList = new Array();
    var poiInfoObj;
    var poiIdObj;
    var areaInfo;
    var currentPoiLocationMarker = null;
    var aorInfo;
    var cityId=null;
    var areaId=null;
    var cityAreaInfo = null;
    var aorNameMarkerList = new Array();

    var infoWindow;
    var choseArray=["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"];


    $(document).ready(function(){
    	map = new AMap.Map("mapContainer", {
              resizeEnable: true,
              view: new AMap.View2D({
               resizeEnable: true,
                      zoom:14//地图显示的缩放级别
                    }),
              keyboardEnable:false
            });
        //在地图中添加ToolBar插件
        map.plugin(["AMap.ToolBar"],function(){
        	toolBar = new AMap.ToolBar();
          map.addControl(toolBar);
        });

        //加载比例尺插件
        map.plugin(["AMap.Scale"], function(){
        	scale = new AMap.Scale();
        	map.addControl(scale);
        });

        AMap.event.addListener(map, "click", function(){
            cleanCurrentPoiArea();
        });

        $("#show-delivery-area").click(function(){
            showDeliveryArea();
        });

        $("#show-aor-area").click(function(){
            showAorArea();

        });
        $("#way-bill-level-1").click(function(){
            showPoiInfoWithWaybillLevel($("#way-bill-level-1"));
        });
        $("#way-bill-level-2").click(function(){
            showPoiInfoWithWaybillLevel($("#way-bill-level-2"));
        });
        $("#way-bill-level-3").click(function(){
            showPoiInfoWithWaybillLevel($("#way-bill-level-3"));
        });
        $("#way-bill-level-4").click(function(){
            showPoiInfoWithWaybillLevel($("#way-bill-level-4"));
        });

        cityId = $("#city-id").text();
        areaId = $("#area-id").text();

        fillCitySearchList();
        citySelector.on("change", function(e){
            cityId = citySelector.select2("val");
            fillAreaSearchList();
        });

        areaSelector.on("change", function(){
            cityId = citySelector.select2("val");
            areaId = areaSelector.select2("val");

            if( areaId != undefined || areaId != null){
                cleanCurrentAreaData();
                searchAreaPoi();
            }
        });

    });


    function searchAreaPoi(){
        $.get("/poi/seachAreaPoi",{cityId:cityId,areaId:areaId})
        .done(function(JSON){
            var code = JSON.code;
            if( code!="0"){
                alert("获取数据失败");
            }else{
                areaInfo = JSON.data.areaInfo;
                aorInfo  = JSON.data.aorMap;
                poiInfoObj = JSON.data.poiInfoList;

                initAreaPolygon();
                initAorPolygonList();
                initPoiLocationMarkerMap();
            }
        }).fail(function(){
            alert("数据请求失败");
        });
    }

    function initAreaPolygon(){
        if( areaInfo != null ){
            var strPoints = areaInfo.deliveryAreaCoordinates;
            var points = JSON.parse(strPoints);
            var pointArray = new Array();
            for(i in points){
                var point = points[i];
                pointArray.push(new AMap.LngLat(point.y, point.x));
            }

            areaPolygon = new AMap.Polygon({
                  path: pointArray, //设置多边形边界路径
                  strokeColor: "#3c8ca3", //线颜色
                  strokeOpacity: 0.6, //线透明度
                  strokeWeight: 3, //线宽
                  fillColor: "#3c8ca3", //填充色
                  fillOpacity: 0.4 //填充透明度
                });

            showDeliveryArea();
        }
    }

    function initPoiLocationMarkerMap(){
        if( poiInfoObj != null ){
            for(i in poiInfoObj){
                drawPoiLocationMarker( poiInfoObj[i] );
            }
            map.setFitView();
            filterOutPoiByWaybillLevel();
        }
    }

    function initAorPolygonList(){
        if( aorInfo != null ){
            for(i in aorInfo){
                var tempPointList = new Array();
                var aorObj = aorInfo[i];
                var pointsStr = aorObj.pointString;
                if( null == pointsStr || "" == pointsStr ){
                    continue;
                }
                var longSum = 0;
                var latiSum = 0;
                var pointList = pointsStr.split(";");
                var length = pointList.length;
                for(j in pointList){
                    var pointStr = pointList[j];
                    var x_y_list = pointStr.split(",");
                    var longitude = x_y_list[1];
                    var latitude  = x_y_list[0];
                    longSum += parseFloat(longitude,10);
                    latiSum += parseFloat(latitude, 10);

                    tempPointList.push(new AMap.LngLat(longitude, latitude));
                }
                var tempAorPolygon = new AMap.Polygon({
                      path: tempPointList, //设置多边形边界路径
                      strokeColor: "#e33614", //线颜色
                      strokeOpacity: 0.60, //线透明度
                      strokeWeight: 3, //线宽
                      fillColor: "#e33614", //填充色
                      fillOpacity: 0.40 //填充透明度
                    });

                var aorNameMarker = new AMap.Marker({
                      content:"<p style=\"color:white;font-size:20px\">"+aorObj.title+"</p>",
                      position: [ longSum/length, latiSum/length ]
                });

                aorPolygonList.push(tempAorPolygon);
                aorNameMarkerList.push(aorNameMarker);
            }

            showAorArea();
        }
    }

    function drawAreaPolygon(){
        areaPolygon.setMap( map );
        map.setFitView();
    }

    function drawPoiLocationMarker(poiInfo){
        var longitude = poiInfo.longitude/1000000.0;
        var latitude  = poiInfo.latitude/1000000.0;
        var iconUrl = getPoiMapLocationIcon( poiInfo.avgWayBillCount );
        var content = "<img src=\""+iconUrl+"\""+" width=\"20\""+" height=\"35\">"
        var poiLocationMarkerObj = new AMap.Marker({
                                                   map:map,
                                                   icon:iconUrl,
                                                   content:content,
                                                   position: [ longitude, latitude ],
                                                 });
        poiLocationMarker[ poiInfo.poiId ] = poiLocationMarkerObj;

        AMap.event.addListener(poiLocationMarkerObj, "click", function(){
            showPoiInfo(poiInfo,poiLocationMarker[ poiInfo.poiId ]);
        });
    }

    function cleanCurrentAreaData(){
        for(i in poiLocationMarker){
            poiLocationMarker[i].setMap(null);
        }
        if( areaPolygon != null){
            areaPolygon.setMap(null);
        }

        for(i in aorPolygonList){
            aorPolygonList[i].setMap(null);
        }
        cleanCurrentPoiArea();

        poiLocationMarker = new Object();
        areaPolygon = null;
        aorPolygonList = new Array();
        poiAreaPolygonList = new Array();
        aorNameMarkerList = new Array();
    }

    function cleanCurrentPoiArea(){
        for(i in poiAreaPolygonList){
            poiAreaPolygonList[i].setMap(null);
        }

        poiAreaPolygonList=[];
        map.clearInfoWindow();
        $("#poi-price-info").hide();

        if( currentPoiLocationMarker != null ){
             var icon = currentPoiLocationMarker.getIcon();
             var icon = icon.replace("poi_chose","poi");
             var content = "<img src=\""+icon+"\""+" width=\"20\""+" height=\"35\">";
             currentPoiLocationMarker.setContent(content);

             currentPoiLocationMarker = null;
        }
    }

    function showPoiInfo(poiInfo,poiMarker){
        cleanCurrentPoiArea();
        currentPoiLocationMarker = poiMarker;
        var intAvgWayBillCount = poiInfo.avgWayBillCount.toFixed(1);
        var info = [];
        var content = '<div style="padding-top:5px;width:300px;color:#000000;background-color:transparent">';
        content += "<div><div class=\"inline-block\">"+poiInfo.name+'</div><div class="pull-right inline-block" style="background-color:#5EA3D1;padding-right:20px;color:white">周日均单量: '+intAvgWayBillCount+'</div></div>';
        content += "<hr style=\"margin-top:2px;margin-bottom:2px;\"/>";
        content += "地址: "+poiInfo.address+"<br/>";
        content += "电话: "+poiInfo.poiCallNum+"<br/>";
        content += "负责BD: "+poiInfo.bdUserName+"/"+poiInfo.bdMobileNum+"<br/>";
        content += "所属蜂窝: "+poiInfo.cityName+"/"+poiInfo.aorName+"<br/>";
        content += "</div>"
        infoWindow = new AMap.InfoWindow({
                //content:info.join("<br/>"),  //使用默认信息窗体框样式，显示信息内容
                content:content,
                closeWhenClickMap:true,
                offset:new AMap.Pixel(0, -25)
            });
        infoWindow.open(map,poiMarker.getPosition());


        //替换商家坐标icon
        var icon = poiMarker.getIcon();
        var icon = icon.replace("poi","poi_chose");
        var content = "<img src=\""+icon+"\""+" width=\"25\""+" height=\"40\">";
        poiMarker.setContent(content);
        poiMarker.setMap(null);
        poiMarker.setMap(map);

        $.get("/poi/getPoiAllDeliveryArea",{poiId:poiInfo.poiId})
        .done( function(json){
            var code = json.code;
            var data = json.data;
            if( code == 0 ){
                $("#poi-price-info").empty();

                for(i in data){
                    var poiPriceInfoCss = "";
                    var poiArea = data[i];
                    var color = generateColor();
                    var price_info = "起送价 "+poiArea.min_price+"元  "+"配送费 "+poiArea.shipping_fee+"元";
                    poiPriceInfoCss = poiPriceInfoCss + "<div style=\"position:relative;padding-left:2px;padding-top:2px;\">"+
                                                                            "<div style=\"display:inline-block;background-color:"+color+";width:10px;height:20px;\"></div>"+
                                                             	            "<p style=\"position:absolute;left:14px;top:2px;margin-top:3px;\">"+price_info+"</p></div>"
                    $("#poi-price-info").append(poiPriceInfoCss);
                    var pointList = JSON.parse( poiArea.area );
                    var polygon = new Array();
                    for(j in pointList){
                        var longitude = pointList[j].y/1000000.0;
                        var latitude  = pointList[j].x/1000000.0;

                        polygon.push( new AMap.LngLat(longitude,latitude) );
                    }

                    polygon = new AMap.Polygon({
                          map: map,
                          path: polygon, //设置多边形边界路径
                          strokeColor: color, //线颜色
                          strokeOpacity: 0.7, //线透明度
                          strokeWeight: 3, //线宽
                          fillColor: color, //填充色
                          fillOpacity: 0.4 //填充透明度
                        });
                    poiAreaPolygonList.push(polygon);
                    //map.setFitView();
                }

                $("#poi-price-info").show();
            }else{
                alert("获取商家区域信息失败");
            }
        })
        .fail( function(){
            alert("获取商家区域信息数据请求失败");
        });
    }

    showDeliveryArea = function(){
        if($("#show-delivery-area").is(":checked")){
            areaPolygon.setMap(map);
            map.setFitView();
        }else{
            areaPolygon.setMap(null);
            map.setFitView();
        }

    }

    showAorArea = function(){
        if($("#show-aor-area").is(":checked")){
            for( i in aorPolygonList){
                aorPolygonList[i].setMap(map);
            }
            for( j in aorNameMarkerList){
                aorNameMarkerList[j].setMap(map);
            }
        }else{
            for( i in aorPolygonList){
                aorPolygonList[i].setMap(null);
            }
            for( j in aorNameMarkerList){
                aorNameMarkerList[j].setMap(null);
            }
        }
        map.setFitView();
    }

    function fillCitySearchList(){
        $.get("/poi/getCityAreaInfo",{})
        .done(function(json){
            var code = json.code;
            var data = json.data;
            if( code == 0 ){
                cityAreaInfo = data;
                for(i in data){
                    var city = data[i];
                    $("#select-city").append("<option value='"+city.cityId+"'>"+city.cityName+"</option>");
                }
                if( cityId!=null || cityId!="" ){
                    citySelector.select2("val",cityId);
                }
            }else{
                alert("获取城市搜索列表数据失败");
            }
        })
        .fail(function(){
            alert("获取城市搜索列表请求失败");
        });
    }

    function fillDeliveryAreaList(){
        var cityId = $("#select-city").val();
    }

    function showPoiInfoWithWaybillLevel(DOMObj){

        var level = DOMObj.val();
        if( DOMObj.is(":checked") ){
            var poiIdList = poiIdObj[level];
            for(i in poiIdObj[level]){
                var poiId = poiIdObj[level][i];
                poiLocationMarker[ poiId ].setMap( map );
            }
        }else{
            var poiIdList = poiIdObj[level];
            for(i in poiIdObj[level]){
                var poiId = poiIdObj[level][i];
                poiLocationMarker[ poiId ].setMap( null );
            }
        }

    }

    function filterOutPoiByWaybillLevel(){
        level1 = new Array();
        level2 = new Array();
        level3 = new Array();
        level4 = new Array();

        for(i in poiInfoObj){
            var waybillCount = poiInfoObj[i].avgWayBillCount;
            var poiId = poiInfoObj[i].poiId;
            if( waybillCount <= 1 ){
                level1.push( poiId );
            }else{
                if(1<waybillCount && waybillCount <=9 ){
                    level2.push( poiId );
                }else{
                    if( 9<waybillCount && waybillCount <=19 ){
                        level3.push( poiId );
                    }else{
                        level4.push( poiId )
                    }
                }
            }
        }

        poiIdObj = new Object();
        poiIdObj["1"] = level1;
        poiIdObj["2"] = level2;
        poiIdObj["3"] = level3;
        poiIdObj["4"] = level4;

    }

    function fillAreaSearchList(){
        cityId = citySelector.select2("val")
        if( cityId==null || cityId=="" ){
            return ;
        }
        //areaSelector.select2('val','')
        areaSelector.empty();

        var areaInfoList = cityAreaInfo[ cityId ].areaInfoVoList;
        for(i in areaInfoList){
            var areaDetailInfo = areaInfoList[i];
            areaSelector.append("<option value='"+areaDetailInfo.areaId+"'>"+areaDetailInfo.areaName+"</option>");
        }
        if( areaId!=null || areaId!="" ){
            areaSelector.select2("val",areaId);
        }
    }

    function generateColor(){
        var ColorStr = '#' + ('00000' + (Math.random() * 0x1000000 << 0).toString(16)).slice(-6);
        return ColorStr;
    }

    function getPoiMapLocationIcon(avgWayBillCount){
        if( avgWayBillCount<= 1 ){
            return "/static/imgs/poi_red.png";
        }else{
            if( 1<avgWayBillCount && avgWayBillCount<=9 ){
                return "/static/imgs/poi_blue.png";
            }else{
                if(9<avgWayBillCount && avgWayBillCount<=19){
                    return "/static/imgs/poi_green.png";
                }else{
                    return "/static/imgs/poi_yellow.png";
                }
            }
        }
    }
});
