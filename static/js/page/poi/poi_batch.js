 require.config({
  baseUrl: MT.STATIC_ROOT + '/js',
  urlArgs: 'ver=' + pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});


requirejs(['module/ui','module/head-nav'], function( ui ,nav) {
     

    var buildPage = function(currentPage,pageTotal,pagerSelector){
    	
    	var pagerSelector = pagerSelector||"#pager";
		var pageHtml = '<li><a data-page="'+(currentPage-1==0?currentPage:(currentPage-1))+'">上一页</a></li>';
		var getPageHtml = function(i){
			return '<li '+(currentPage==i?'class = "active"':'')+'><a data-page="'+i+'">'+i+'</a></li>';
		}
		var preHtml = "";
		for(var i = currentPage; i >= (currentPage-5>1?(currentPage-5):1); i--){
			preHtml = getPageHtml(i)+preHtml;
		}
		pageHtml = pageHtml + preHtml;
		for(var i=currentPage+1; i<= (currentPage+5>pageTotal?pageTotal:(currentPage+5)); i++){
			pageHtml+= getPageHtml(i);
		}
		pageHtml += '<li><a data-page="'+(currentPage+1>pageTotal?pageTotal:(currentPage+1))+'">下一页</a></li>';
		
		$(pagerSelector).html(pageHtml);

	}


	

	var batchBiz = {
		cityList:{},
		cityHtml : "",
		areaList:{},
		batchType : 1,//1－活动开启关闭，2-营业休息，3-配送费
		history : "citylist",
		logAreaId:0,
		submitting:false,
		cityAreaInit:function(){
			
			//获取全部城市列表
			$.ajax({
					url : "/batchpoi/api/area/citylist",
					type :"get",
					dataType :"json",
					//jsonpCallback:"callback",
					success : function(result){
						if(result.code==0&&result.data!=null){
							var htmlCity = "";
							
							for(var i=0;i<result.data.length;i++){
								var d = result.data[i];
								htmlCity+='<option value="'+d.cityId+'">'+d.name+'</option>';
							}
							

							//$("#areaId").append(htmlArea).select2();
							$("#cityId").append(htmlCity);
							
							$("#cityId").select2();
							batchBiz.cityHtml = htmlCity;
							//根据城市列表获取区域ID
							$("#cityId").change(function(){
								
								var cityId = $("#cityId").val();
								var cityName = $("#cityId option:selected").text();

								if(cityId=="-1"){
									$("#areaId option").remove();
									$("#areaId").append('<option value="-1">请先选择城市</option>').select2();
								}else{
									$.ajax({
										url : "/batchpoi/api/area/arealist",
										type :"get",
										data : {cityId:cityId},
										dataType :"json",
										success : function(json){
											var htmlArea = '<option value="-1">全部'+cityName+'</option>';
											if(json.code ==0 && json.data!=null){

												for(var i=0;i<json.data.length;i++){
													var d = json.data[i];
													htmlArea+='<option value="'+d.id+'">'+d.deliveryAreaName+'</option>';
												}
												$("#areaId option").remove();
												$("#areaId").append(htmlArea).select2();

											}
										},
										error:function(){
											alert("获取区域信息失败");
										}
									});
								}

								

								
								
							});

							

							$("#btnSearch").trigger("click");
						}else{
							alert("获取城市信息失败");
						}

					},
					error:function(){
						alert("服务器遇到问题，请稍后再试");
					}
				});
			
		},

		//批量设置
		batchSetInit:function(){

			var body_act_Batch  = $('\
				<div class="opContainer">\
				<div class="row">\
				<div class="col-md-2">类型</div>\
				<div class="col-md-10">\
				<label class="radio-inline">\
				  <input type="radio" name="actType" checked="checked" id="actType1" value="city"> 按城市\
				</label>\
				<label class="radio-inline">\
				  <input type="radio" name="actType" id="actType2" value="area"> 按区域\
				</label>\
				</div></div>\
				<div class="row actType1Con">\
					<div class="col-md-2"></div>\
					<div class="col-md-6"><select id="batchCityId" class="form-control input-sm" style="width:160px;"><option value="">请选择</option></select></div>\
				</div>\
				<div class="row actType1Con">\
					<div class="col-md-2">区域类型</div>\
					<div class="col-md-6"><select id="batchCityType" class="form-control input-sm" style="width:160px;">\
					<option value="">请选择</option><option value="1">自建</option><option value="2">加盟</option><option value="4">众包</option><option value="7">城市代理</option>\
					</select></div>\
				</div>\
				<div class="row" id="actType2Con" style="display:none">\
					<div class="col-md-2"></div>\
					<div class="col-md-8"><textarea id="areaIds" rows="4" placeholder="将调整此区域内的全部商户。请输入区域id，每行一个。" class="form-control input-sm"></textarea></div>\
				</div>\
				<div class="row" id="actStatus" style="display:none">\
					<div class="col-md-2 memo">活动设置</div>\
					<div class="col-md-2"><select id="actionStatus" class="form-control input-sm"><option value="">请选择</option><option value="1">开启</option><option value="0">关闭</option></select></div>\
				</div>\
				<div class="row" id="opStatus">\
					<div class="col-md-2 memo">营业状态</div>\
					<div class="col-md-2"><select id="businessStatus" class="form-control input-sm"><option value="">请选择</option><option value="1">营业</option><option value="0">休息</option></select></div>\
				</div>\
				<div class="row opStatusHour" style="display:none">\
					<div class="col-md-2 memo">时长</div>\
					<div class="col-md-2"><input class="form-control input-sm" id="duration"/></div>\
					<div class="col-md-2 memo">分钟</div>\
				</div>\
				<div class="row">\
					<div class="col-md-2">操作原因</div>\
					<div class="col-md-8"><textarea id="reason" rows="4" placeholder="操作原因会展示给商家和BD，请仔细填写" class="form-control input-sm"></textarea></div>\
				</div>\
				<div class="row" style="display:none" id="opPrice">\
					<div class="col-md-2 memo">配送费<input type="hidden" value="0"/></div>\
					<div class="col-md-4"><div class="btn-group btn-group-sm"><div class="btn btn-success btnMinus">-</div>\
					<div class="btn btn-default btnValue">0</div><div class="btn btn-success btnPlus">+</div></div></div>\
				</div>\
				</div>');

 			//绑定城市
 			var cityBuilt = false;
 			var buildCity = function(){
 				if(!cityBuilt){
	 				cityBuilt = true;
	 				$("#batchCityId",body_act_Batch).append(batchBiz.cityHtml).select2();
	 			}else{
	 				$(".opStatusHour").hide();
	 			}
 			}

 			$(".btnMinus",body_act_Batch).click(function(){
				var v = parseInt($("#opPrice input").val());
				$("#opPrice input").val(v-1);
				$("#opPrice .btnValue").text(v-1);
			});
			$(".btnPlus",body_act_Batch).click(function(){
				var v = parseInt($("#opPrice input").val());
				$("#opPrice input").val(v+1);
				$("#opPrice .btnValue").text(v+1);
			});

			$("#actType1",body_act_Batch).click(function(){
				$(".actType1Con").show();
		 		$("#actType2Con").hide();
			});

			$("#opStatus select",body_act_Batch).change(function(){
				if($(this).val()=="0"){
					$(".opStatusHour").show();
				}else{
					$(".opStatusHour").hide();
				}
			});

			$("#actType2",body_act_Batch).click(function(){
				$("#actType2Con").show();
		 		$(".actType1Con").hide();
			});

			
		    var dialog = ui.createDialog( { title: "活动开启/关闭" ,body: body_act_Batch,buttons:[
		    		{text:"确定",primary:true,callback:function() {


		    			var postData = {};
		    			postData.reason = $("#reason").val();
		    			
		    			
		    			if($("input[name='actType']:checked").val()=="city"){
		    				postData.cityId = $("#batchCityId").val();
		    				postData.areaType = $("#batchCityType").val();
		    				if(postData.cityId==""){
			    				alert("请选择城市");
			    				return;
			    			}
			    			if(postData.areaType==""){
			    				alert("请选择区域类型");
			    				return;
			    			}
		    			}
		    			else{
		    				postData.areaType = "-1";
		    				postData.areaIds = $("#areaIds").val().replace(/[\r\n]/g,",");
		    				if(!/[0-9]/.test(postData.areaIds)){
		    					alert("区域ID必填且必须为数字");
		    					return;
		    				}
		    			}
		    			
		    			if(postData.reason==""){
		    				alert("操作原因会展示给商家和BD，请仔细填写");
		    				return;
		    			}

		    			var url = "";
		    			switch(batchBiz.batchType){
		    				case 1:
		    					url = "/batchpoi/api/setActionStatus";//批量设置活动状态
		    					postData.status = $("#actionStatus").val();
		    					if(postData.status==""){
		    						alert("请选择活动状态");
		    						return;
		    					}
		    				break;
		    				case 2:
		    					url = "/batchpoi/api/setBusinessStatus";;//批量设置营业状态
		    					postData.status = $("#businessStatus").val();
		    					postData.duration =  $("#duration").val();
		    					if(postData.status==""){
		    						alert("请选择营业状态");
		    						return;
		    					}
		    					if(postData.status==0&&postData.duration==""){
		    						alert("请填写置休时长");
		    						return;
		    					}
		    				break;
		    				case 3:
		    					url = "/batchpoi/api/setDeliveryPrice";//批量设置配送费
		    					postData.deliveryPrice = $("#opPrice input").val();
		    				break;
		    			}
		    			if(postData.reason==""){
    						alert("操作原因会展示给商家及BD,请认真填写");
    						return;
    					}

		    			dialog.modal("hide");
		    			

		    			myConfirm(function(){
		    				var btn = $("#confirmDialog .submit");
		    				if(!batchBiz.submitting){
		    					batchBiz.submitting = true;
				    			$.ajax({
				    				url : url,
				    				data : postData,
				    				type : "get",
				    				dataType : "json",
				    				success: function (result) {
				    					if(result.code==0){
				    						btn.text("保存成功");
				    						
							    			setTimeout(function(){
							    				$("#confirmDialog").modal("hide");
							    				btn.text("确定");
							    				$("#list").html("<tr><td colspan='9'>设置中...</td></tr>");
								    			setTimeout(function() {
								    				$("#btnSearch").trigger("click");
								    			},2000);
							    			},1000);

							    		}else{
							    			btn.text("保存失败");
							    			alert(result.msg);
							    			setTimeout(function(){
							    				btn.text("确定");
							    				batchBiz.submitting = false;
							    			},1000);
							    		}

				    				},
									error:function(){
										alert("服务器遇到问题，请稍后再试");
										batchBiz.submitting = false;
									}
				    			});
				    		}
		    			});

		    			

		    		}},
		    		ui.createDialogButton('close','取消') 
		    ] });

		    $("#btnActStatus").click(function(){
		    	buildCity();
		    	$("#opStatus,.opStatusHour",body_act_Batch).hide();
		    	$("#actStatus",body_act_Batch).show();
		    	$("#opPrice",body_act_Batch).hide();
		    	$("#reason",body_act_Batch).val("");
		    	dialog.modal("show");
		    	batchBiz.batchType = 1;
		    	$(dialog).find(".modal-title").text("活动开启/关闭");
		    });

		    $("#btnStoreStatus").click(function(){
		    	buildCity();
		    	$("#opStatus",body_act_Batch).show();
		    	$("#actStatus",body_act_Batch).hide();
		    	$("#opPrice",body_act_Batch).hide();
		    	$("#opStatus select",body_act_Batch).trigger("change");
		    	$("#reason",body_act_Batch).val("");
		    	dialog.modal("show");
		    	batchBiz.batchType = 2;
		    	$(dialog).find(".modal-title").text("商家营业/置休");
		    });

		    $("#btnPriceStatus").click(function(){
		    	buildCity();
		    	batchBiz.batchType = 3;
		    	$("#opStatus",body_act_Batch).hide();
		    	$("#actStatus",body_act_Batch).hide();
		    	$("#opPrice",body_act_Batch).show();
		    	$("#reason",body_act_Batch).val("");
		    	dialog.modal("show");
		    	
		    	$(dialog).find(".modal-title").text("批量调整配送费");
		    });
		    

		},


		statusEditInit:function(){
			var body_act_Single  = $('\
				<div class="opContainer">\
				<div class="row">\
					<div class="col-md-2">城市</div>\
					<div class="col-md-10" id="opCity">北京市</div>\
				</div>\
				<div class="row">\
					<div class="col-md-2">区域</div>\
					<div class="col-md-10" id="opArea">酒仙桥</div>\
				</div>\
				<div class="row">\
					<div class="col-md-2">活动设置</div>\
					<div class="col-md-2"><select id="opAct" class="form-control input-sm"><option value="">请选择</option><option value="1" text="开启">开启</option><option value="0" text="关闭">关闭</option></select></div>\
				</div>\
				<div class="row" id="opStatusSingle">\
					<div class="col-md-2">营业状态</div>\
					<div class="col-md-2"><select class="form-control input-sm"><option value="">请选择</option><option value="1" text="营业">营业</option><option value="0" text="休息">休息</option></select></div>\
				</div>\
				<div class="row opStatusSingleHour" style="display:none">\
					<div class="col-md-2">时长</div>\
					<div class="col-md-2"><input class="form-control input-sm" /></div>\
					<div class="col-md-2">分钟</div>\
				</div>\
				<div class="row">\
					<div class="col-md-2">操作原因</div>\
					<div class="col-md-8"><textarea id="opReason" rows="4" placeholder="操作原因会展示给商家和BD，请仔细填写" class="form-control input-sm"></textarea></div>\
				</div>\
				<div class="row" id="opDeliveryStatus">\
					<div class="col-md-2 memo">配送费<input type="hidden" value="0"/></div>\
					<div class="col-md-4"><div class="btn-group btn-group-sm"><div class="btn btn-success btnMinus">-</div>\
					<div class="btn btn-default btnValue">0</div><div class="btn btn-success btnPlus">+</div></div></div>\
				</div>\
				</div>');


			$("#opStatusSingle select",body_act_Single).change(function(){
				if($(this).val()=="0"){
					$(".opStatusSingleHour").show();
				}else{
					$(".opStatusSingleHour").hide();
				}
			});

			$(".btnMinus",body_act_Single).click(function(){
				var v = parseInt($("#opDeliveryStatus input").val());
				$("#opDeliveryStatus input").val(v-1);
				$("#opDeliveryStatus .btnValue").text(v-1);
			});
			$(".btnPlus",body_act_Single).click(function(){
				var v = parseInt($("#opDeliveryStatus input").val());
				$("#opDeliveryStatus input").val(v+1);
				$("#opDeliveryStatus .btnValue").text(v+1);
			});

		    var dialogEdit = ui.createDialog( { title: "编辑" ,body: body_act_Single,buttons:[
		    		{text:"确定",primary:true,callback:function(){

		    			var areaId = $("#opArea",body_act_Single).attr("data-areaId");
		    			var postData = {
		    				areaId : areaId,
		    				actionStatus: $("#opAct",body_act_Single).val(),
		    				businessStatus: $("#opStatusSingle select",body_act_Single).val(),
		    				duration:$(".opStatusSingleHour input",body_act_Single).val(),
		    				reason:$("#opReason",body_act_Single).val(),
		    				deliveryPrice:parseInt($("#opDeliveryStatus input",body_act_Single).val())
		    			};

		    			var checked = true;
		    			if(checked==true&&postData.actionStatus==""){
		    				checked = "请选择活动状态";
		    			}
		    			if(checked==true&&postData.businessStatus==""){
		    				checked = "请选择营业状态";
		    			}
		    			if(checked==true&&postData.businessStatus==0&&postData.duration==""){
		    				checked = "请填写置休时长";
		    			}
		    			if(checked==true&&postData.reason=="")
		    				checked = "操作原因会展示给商家及BD,请认真填写";

		    			if(checked!==true){
		    				alert(checked);

		    			}else{

			    			dialogEdit.modal("hide");

			    			myConfirm(function(){
				    			if(!batchBiz.submitting){
				    				batchBiz.submitting = true;

				    				var btn = $("#confirmDialog .submit");
					    			btn.text("保存中");
					    			
					    			if(postData.duration=="")
					    				postData.duration = "0";

					    			$.ajax({
										url : "/batchpoi/api/setAreaStatus",
										type :"get",
										dataType :"json",
										data: postData,
										success : function(result){
											//console.log(result);
											if(result.code==0){
					    						btn.text("保存成功");

					    						$("#list").html("<tr><td colspan='9'>设置中...</td></tr>");
					    						
								    			setTimeout(function() {
								    				setTimeout(function() {
									    				$("#btnSearch").trigger("click");
									    			},1000);
								    				$("#confirmDialog").modal("hide");
								    				btn.text("确定");
								    			},1000);
								    			
								    		}else{
								    			btn.text("保存失败");
								    			alert(result.msg);
								    			setTimeout(function(){
								    				btn.text("确定");
								    				batchBiz.submitting = false;
								    			},1000);
								    		}
										},
										error:function(){
											alert("服务器遇到问题，请稍后再试");
										}
									});
 								}
			    			});
						}
		    		}},
		    		ui.createDialogButton('close','取消') 
		    ] });
		    

		    $("#list").on("click",".iconEdit",function(){
		    	var tds = $(this).parents("td").prevAll("td");
		    	
		    	$("#opCity",body_act_Single).text($(tds[7]).text());
		    	$("#opArea",body_act_Single).text($(tds[5]).text()).attr("data-areaId",$(tds[4]).text());
		    	if($(tds[2]).attr("data-time")!="undefined")
		    		$(".opStatusSingleHour input",body_act_Single).val($(tds[2]).attr("data-time"));
		    	$("#opAct",body_act_Single).find("option[text='"+$(tds[1]).text()+"']").attr("selected",true);
		    	$("#opStatusSingle select",body_act_Single).find("option[text='"+$(tds[2]).text()+"']").attr("selected",true);
		    	if($(tds[2]).text()=="休息"){
		    		$(".opStatusSingleHour",body_act_Single).show();
		    	}else{
		    		$(".opStatusSingleHour",body_act_Single).hide();
		    	}
		    	$("#opReason",body_act_Single).val($(this).parents("tr").attr("data-reason"));
		    	//console.log($(".opContainer textarea",body_act_Single));
		    	$("#opDeliveryStatus input",body_act_Single).val("0");
		    	$("#opDeliveryStatus .btnValue",body_act_Single).text("0");
		    	dialogEdit.modal("show");
		    });


		    $("#list").on("click",".iconLog",function(){
		    	var id = $(this).parents("tr").data("id");
		    	batchBiz.logAreaId = id;
		    	batchBiz.getLogList(1,id);
		    });


		},

		//查询
		searchInit:function(){

			$("#pager").on("click","a",function() {
				getList($(this).data("page"))
			});

			var setDeliveryPrice = function (price) {
					if(price==0)
						return "原价";
					else if(price>0)
						return "<b style='color:red'>增加"+price+"元</b>";
					else
						return "<b style='color:red'>减少"+(0-price)+"元</b>";
			};

			var getAreaType = function(type){
				
				switch(type){
					case 1:
						return "自建";
					break;
					case 2:
						return "加盟";
					break;
					case 4:
						return "众包";
					break;
					case 7:
						return "城市代理";
					break;
					default:
						return "";
					break;
				}
			};

			var buildList = function(data,pageNum){

				if(batchBiz.history!="citylist")
					history.pushState("citylist", "", "");

				var html = [];
				for(var i=0;i<data.viewPage.length;i++){
					var d = data.viewPage[i];
					html.push('<tr data-id="'+d.areaId+'" data-reason="'+(d.reason==undefined?"":d.reason)+'">\
						<td>'+d.cityName+'</td>\
						<td>'+d.cityId+'</td>\
						<td>'+d.areaName+'</td>\
						<td>'+d.areaId+'</td>\
						<td>'+getAreaType(d.areaType)+'</td>\
						<td data-time="'+d.closePoiDuration+'">'+(d.businessStatus==1?'营业':'休息')+'</td>\
						<td>'+(d.actionStauts==1?'开启':'关闭')+'</td>\
						<td data-price="'+d.adjustDeliverypriceValue+'">'+setDeliveryPrice(d.adjustDeliverypriceValue)+'</td>\
						<td><a class="iconEdit" data-toggle="tooltip" title="编辑" data-placement="top" data-original-title="编辑"><i class="fa fa-edit fa-lg opration-icon"></i></a>&nbsp;<a class="iconLog" data-toggle="tooltip" title="操作记录" data-placement="top" data-original-title="操作记录"><i class="fa fa-list fa-lg opration-icon"></i></a></td>\
						</tr>');
				}

				$("#list").html(html.join(""));
				$("#countLable").text("共"+data.totalRecords+"项");
				buildPage(pageNum,data.totalPages);
				$('[data-toggle="tooltip"]').tooltip();
			}

			var getList = function(page){
				batchBiz.submitting = false;
				$("#divCityList").show();
				$("#divLogList").hide();
				var filterData = {
					cityId : $("#cityId").val(),
					areaId : $("#areaId").val(),
					areaType : $("#areaType").val(),
					businessStatus: $("#openStatus").val(),
					actionStatus :$("#actStatus").val(),
					deliveryPriceStatus:$("#deliveryStatus").val(),
					pageSize:20,
					pageNum:1
				};

				if(filterData.cityId=="")
					filterData.cityId = "-1";
				if(filterData.areaId=="")
					filterData.areaId = "-1";
					
				if(page!=undefined)
					filterData.pageNum = page;	

				var url = "/batchpoi/api/area/list";
				$.ajax({
					url : url,
					type :"get",
					data : filterData,
					//dataType :"jsonp",
					//jsonpCallback:"callback",
					success : function(result){
						if(result.code == 0)
							buildList(result.data,filterData.pageNum);
						else 
							alert(result.msg);
					},
					error:function(){
						alert("服务器遇到问题，请稍后再试");
					}
				});
			
			};

			$("#btnSearch").click(function(){
				$("#list").html("<tr><td colspan='9'>加载中...</td></tr>")
				getList();
			});

			batchBiz.getList = getList;
		},

		initLog:function(){
			$("#btnLogList").click(function(){
				batchBiz.logAreaId = 0;
				batchBiz.getLogList(1,0);
			});

			$("#logPager").on("click","a",function() {
				batchBiz.getLogList($(this).data("page"),batchBiz.logAreaId);
			});

		},

		getLogList:function(page,areaId){
			if(batchBiz.history!="loglist")
				history.pushState("loglist", "", "");
			$("#divCityList").hide();
			$("#divLogList").show();
			$("#logList").html('<tr><td colspan="5">加载中……</td></tr>');
			var filterData = {pageNum:page,pageSize:20};
			var url = "/batchpoi/api/batchOpLogs";
			if(areaId!=0){
				filterData.areaId = areaId;
				url = "/batchpoi/api/areaOpLogs";
			}
			$.ajax({
				url : url,
				type :"get",
				data : filterData,
				dataType :"json",
				//jsonpCallback:"callback",
				success : function(result){
					if(result.code == 0){
						//buildList(result.data,filterData.pageNum);
						var opTypeName = ['','关闭活动','开启活动','门店置休','门店营业','增加配送费','减少配送费'];
						var tmpl = [];
						if(result.data.views.length>0){
							for(var i=0;i<result.data.views.length;i++){
								var d= result.data.views[i];
								tmpl.push('<tr>')
								tmpl.push('<td>'+opTypeName[d.opType]+'</td>');
								tmpl.push('<td>');
								if(d.opType==5||d.opType==6)
									tmpl.push(opTypeName[d.opType]+'：'+Math.abs(d.opValue)+'元<br/>');
								if(d.opType==3){
									tmpl.push(opTypeName[d.opType]+'：'+d.opValue+'分钟<br/>');
								}else if(d.opType==4){
									tmpl.push(opTypeName[d.opType]+'<br/>');
								}
								if(areaId==0){
									if(d.cityName!=null)
										tmpl.push('城市：'+d.cityName+'<br/>');
									tmpl.push('区域：<br/>');
									for(var j=0;j<d.areaNames.length;j++){
										tmpl.push(d.areaNames[j]+'（ID:'+d.areaIds[j]+'）<br/>');
									}
								}
								tmpl.push('</td>');
								tmpl.push('<td>'+d.opName+'</td>');
								tmpl.push('<td>'+(d.reason==undefined?"":d.reason)+'</td>');
								tmpl.push('<td>'+getLocalTime(d.opTime)+'</td>');
								tmpl.push('</tr>');
							}
							$("#logList").html(tmpl.join(""));
							$("#countLogLabel").text("共"+result.data.totalRecords+"项");
							
						}else{
							$("#logList").html('<tr><td colspan="5">暂时没有操作记录</td></tr>');
							$("#countLogLabel").text("共0项");
						}
						buildPage(page,result.data.totalPages,"#logPager");
						//console.log(result);
					}
					else 
						alert(result.msg);
				},
				error:function(){
					alert("服务器遇到问题，请稍后再试");
				}
			});
		}

	};


	batchBiz.searchInit();
	//初始化城市
	batchBiz.cityAreaInit();
   	//批量设置
   	batchBiz.batchSetInit();
   	//单个编辑
   	batchBiz.statusEditInit();

   	batchBiz.initLog();

	function myConfirm(callback){

		$("#confirmDialog").modal("show");
		$("#confirmDialog .submit").unbind("click").bind("click",function(){
			if(callback)
				callback();
		});
		$("#confirmDialog .cancel").unbind("click").bind("click",function(){
			$("#confirmDialog").modal("hide");
		});
	};

	function getLocalTime(nS) {  

		var addZero = function(d){
			if(d<10){
				d = "0"+""+d;
			}
			return d;
		};
		var now = new Date(parseInt(nS) * 1000);  
		var year=now.getFullYear();     
		var month=now.getMonth()+1;
		var date=now.getDate();     
		var hour=now.getHours();
		var minute=now.getMinutes();
		var second=now.getSeconds();     
      	return   year+"-"+addZero(month)+"-"+addZero(date)+"  "+addZero(hour)+":"+addZero(minute)+":"+addZero(second);   
    }   

    window.onpopstate = function(e){
    	if(e.state=="loglist"){
    		$("#divCityList").hide();
			$("#divLogList").show();
    	}else{
    		$("#divCityList").show();
			$("#divLogList").hide();
    	}
    }  
   

});
