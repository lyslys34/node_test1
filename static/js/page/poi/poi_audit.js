 require.config({
  baseUrl: MT.STATIC_ROOT + '/js',
  urlArgs: 'ver=' + pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});


requirejs(['module/ui','module/head-nav'], function( ui ,nav) {
    
    var auditBiz = {
       
        statusEditInit:function(){

            var isDisagree = true;

            var body_act_Single  = $('\
                <div class="opContainer" style="display:none">\
                <div class="row radio">\
                    <div class="col-md-12"><label><input class="form-control input-sm" name="disagree" value="该区域未开通美团众包服务" type="radio"/>该区域未开通美团众包服务</label></div>\
                </div>\
                <div class="row radio">\
                   <div class="col-md-12"><label><input class="form-control input-sm" name="disagree" value="其他" type="radio"/>其他</label></div>\
                </div>\
                <div class="row" id="remark" style="display:none">\
                   <div class="col-md-8"><textarea class="form-control" rows="3"></textarea></div>\
                </div>\
                </div>\
                <div id="confirm" style="display:none"><h4>确定要审核通过吗？</h4>\
                </div>');


            $("label",body_act_Single).click(function(){
                $("#remark textarea").val($(this).text());
                if($(this).text()=="其他")
                    $("#remark").show();
                else
                    $("#remark").hide();
            });


            var dialogEdit = ui.createDialog( { title: "驳回原因" ,body: body_act_Single,buttons:[
                    {text:"确定",primary:true,callback:function(){

                        var btn = $(this);
                        btn.text("设置中");
                        var poiId = $("#remark",body_act_Single).attr("data-poiId");
                        //单个设置事件
                        var postData = {
                            poiId : poiId,
                            auditRemark: $("#remark textarea",body_act_Single).val(),
                            auditStatus: isDisagree?2:1
                        };

                        $.ajax({
                            url : "/poi/auditZbPoi",
                            type :"get",
                            dataType :"json",
                            data: postData,
                            success : function(result){
                                //console.log(result);
                                if(result.code==0){
                                    btn.text("驳回成功");
                                    setTimeout(function(){
                                        dialogEdit.modal("hide");
                                        btn.text("确定");
                                    },1000);
                                }else if(result.code==-1){
                                    dialogEdit.modal("hide");
                                    btn.text("确定");
                                    alert(result.msg);

                                }else{
                                    btn.text("驳回失败");
                                    alert(result.msg);
                                    setTimeout(function(){
                                        btn.text("确定");
                                    },1000);
                                }
                            },
                            error:function(){
                                alert("服务器遇到问题，请稍后再试");
                            }
                        });
                        
                    }},
                    ui.createDialogButton('close','取消')
            ] });
            
            $("#list").on("click",".disagree",function(){
                isDisagree = true;
                $("#remark",body_act_Single).attr("data-poiId",$(this).attr("data-id"));
                $(dialogEdit).find(".modal-title").text("驳回原因");
                $(dialogEdit).find(".opContainer").show();
                $(dialogEdit).find("#confirm").hide();
                dialogEdit.modal("show");
            });

            $("#list").on("click",".approve",function(){
                isDisagree = false;
                $("#remark",body_act_Single).attr("data-poiId",$(this).attr("data-id"));
                $(dialogEdit).find(".opContainer").hide();
                $(dialogEdit).find("#confirm").show();
                $(dialogEdit).find(".modal-title").text("提示");
                dialogEdit.modal("show");
            });


        },

        //查询
        searchInit:function(){

            var buildPage = function(currentPage,pageTotal){
                var pageHtml = '<li><a data-page="'+(currentPage-1==0?currentPage:(currentPage-1))+'">上一页</a></li>';
                var getPageHtml = function(i){
                    return '<li '+(currentPage==i?'class = "active"':'')+'><a data-page="'+i+'">'+i+'</a></li>';
                }
                var preHtml = "";
                for(var i = currentPage; i >= (currentPage-5>1?(currentPage-5):1); i--){
                    preHtml = getPageHtml(i)+preHtml;
                }
                pageHtml = pageHtml + preHtml;
                for(var i=currentPage+1; i<= (currentPage+5>pageTotal?pageTotal:(currentPage+5)); i++){
                    pageHtml+= getPageHtml(i);
                }
                pageHtml += '<li><a data-page="'+(currentPage+1>pageTotal?pageTotal:(currentPage+1))+'">下一页</a></li>';
                $("#pager").html(pageHtml);

            }

            $("#pager").on("click","a",function() {
                getList($(this).data("page"))
            });

            var buildList = function(data,pageNum){
                var html = [];

                if(data==null)
                    data = undefined;
                else{
                    if(data.poiAuditVos==null)
                        data.poiAuditVos = [];
                }


                if(data==undefined||data.poiAuditVos.length==0){
                    html.push("<tr><td colspan='6'>没有符合条件的记录</td></tr>");
                    $("#countLable").text("");
                }
                else{
                    for(var i=0;i<data.poiAuditVos.length;i++){
                        var d = data.poiAuditVos[i];
                        html.push('<tr data-id="'+d.wmPoiId+'">\
                            <td>'+d.poiName+'</td>\
                            <td>'+d.wmPoiId+'</td>\
                            <td>'+d.poiPhone+'</td>\
                            <td>'+d.poiAddress+'</td>\
                            <td>'+d.applyTime+'</td>\
                            <td><button data-id="'+d.wmPoiId+'" class="btn btn-default disagree">驳回</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button data-id="'+d.wmPoiId+'" class="btn btn-default approve">通过</button></td>\
                            </tr>');
                    }

                    
                    $("#countLable").text("共"+data.recordCount+"项");
                    var totalPages = Math.ceil(data.recordCount/data.pageSize);
                    buildPage(pageNum,totalPages);

                }

                $("#list").html(html.join(""));
                $('[data-toggle="tooltip"]').tooltip();
                
            }



            var getList = function(page){

                var filterData = {
                    poiName : $("#storeName").val(),
                    poiId : $("#storePOI").val(),
                    pageSize:20,
                    pageNum:1
                };

                    
                if(page!=undefined)
                    filterData.pageNum = page;  

                var url = "/poi/getZbPoiAuditList";
                $.ajax({
                    url : url,
                    type :"get",
                    data : filterData,
                    //dataType :"jsonp",
                    //jsonpCallback:"callback",
                    success : function(result){
                        if(result.code == 0)
                            buildList(result.data,filterData.pageNum);
                        else 
                            alert(result.msg);
                    },
                    error:function(){
                        alert("服务器遇到问题，请稍后再试");
                    }
                });
            
            };

            $("#btnSearch").click(function(){
                $("#list").html("<tr><td colspan='6'>加载中...</td></tr>")
                getList();
            });

            getList();
        }

    };


    auditBiz.searchInit();
    

    auditBiz.statusEditInit();

    
   

});
