$(document).ready(function(){
	init();
	CheckBox.checkBoxChecked();
	//$('[data-placement="top"]').tooltip();
});

function init() {

	$('#batchDiv a').attr("disabled", "disabled");	//未选中任何时，批量操作按钮置为不可用

	//弹出框的确认按钮
	$("#commit").click(function() {
		fromAudit('agreeForm', 'commit');
	});

	$("#refuseBtn").click(function() {
		fromAudit('disagreeForm', 'refuseBtn');
	});

	$('#confirmBtn').click(function(){
		window.location.reload();
	});

	//单个审核
	$(".agree_link").click(function() {
		var id = $(this).attr('rel');
		$("#agreeForm").attr("action", "/settleAudit/audit?auditStatus=1&billIds=" + id);
	});

	$(".disagree_link").click(function() {
		var id = $(this).attr('rel');
		$("#disagreeForm").attr("action", "/settleAudit/audit?auditStatus=2&billIds=" + id);
	});

	//批量
	$("#agree_button").click(function() {
		var checkbox = $("tbody input[type='checkbox']:checked");
		var ids = "";
		for(var i=0; i<checkbox.length; i++){
			var id = checkbox[i].value;
			ids += id + ",";
		}
		if (checkbox.length > 0) {
			ids = ids.substring(0,ids.length-1);
		}
		$("#agreeForm").attr("action", "/settleAudit/audit?auditStatus=1&billIds=" + ids);
	});

	$("#disagree_button").click(function() {
		var checkbox = $("tbody input[type='checkbox']:checked");
		var ids = "";
		for(var i=0; i<checkbox.length; i++){
			var id = checkbox[i].value;
			ids += id + ",";
		}
		if (checkbox.length > 0) {
			ids = ids.substring(0,ids.length-1);
		}
		$("#disagreeForm").attr("action", "/settleAudit/audit?auditStatus=2&billIds=" + ids);
	});
}

/*
 * 普通的ajax请求，暂时弃用
 */
function ajaxAudit(auditStatus, billIds, commend, btnId) {
	$("#" + btnId).attr("disabled", "disabled");	//确认按钮置为不可用
	$.ajax({
		dataType: 'json',
		type : 'post',
		url : "/settleAudit/audit",
		data : {
			auditStatus:auditStatus,
			billIds:billIds,
			commend:commend
		},
		success : function(data){
			console.dir(data);
			var msg = "操作失败，请稍后重试";
			if(data.success!=null && data.success != undefined && data.success == true){
				msg = "操作成功！";
			}
			$('#infoMsg').html(msg);
			$('#remainBtn').css('display', 'none');
			$('#confirmBtn').css('display', 'block');
			$('#infoModal').modal('show');
		},
		error:function(XMLHttpRequest ,errMsg){
			console.log("fail");
			$('#remainBtn').css('display', 'block');
			$('#confirmBtn').css('display', 'none');
			$('#infoMsg').html("网络连接失败，请稍后重试");
			$('#infoModal').modal('show');
			$("#" + btnId).removeAttr("disabled");	//使按钮可用
		}
	});
}

/*
 * 直接提交表格，auditStatus&billIds写入form的action中
 */
function fromAudit(formId, btnId) {
	$("#" + btnId).attr("disabled", "disabled");	//确认按钮置为不可用
	$.ajax({
		dataType: 'json',
		type : 'post',
		url : $('#' + formId).attr('action'),
		data : $('#' + formId).serialize(),
		success : function(data){
			console.dir(data);
			var msg = "操作失败，请稍后重试";
			$('#remainBtn').css('display', 'block');
			$('#confirmBtn').css('display', 'none');
			if(data.success){
				msg = "操作成功！";
				$('#remainBtn').css('display', 'none');
				$('#confirmBtn').css('display', 'block');
			}
			$('#infoMsg').html(msg);
			$('#infoModal').modal('show');
		},
		error:function(XMLHttpRequest ,errMsg){
			console.log("fail");
			$('#remainBtn').css('display', 'block');
			$('#confirmBtn').css('display', 'none');
			$('#infoMsg').html("网络连接失败，请稍后重试");
			$('#infoModal').modal('show');
			$("#" + btnId).removeAttr("disabled");	//使按钮可用
		}
	});
}

(function(){
	function Message(){};
	Message.prototype = {
		showMessage: function(type, message) {
			$('#result').html(
				"<div class='alert " + type + " alert-dismissible' role='alert'>" +
				"<button type='button' class='close' data-dismiss='alert'>" +
				"<span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span>" +
				"</button>" +
				"<strong>Success! " + message + "</strong></div>"
			);
		},
		type: {
			'SUCCESS' : 'alert-success',
			'INFO' : 'alert-info',
			'WARNING' : 'alert-warning',
			'DANGER' : 'alert-danger'
		}
	};

	function CheckBox(){};
	CheckBox.prototype = {
		checkBoxChecked : function checkBoxChecked() {
			var checkAllBox = $("thead input[type='checkbox']");
			var selectSpan = $("#selectNum");
			$(checkAllBox).click(function(){
				if($(this).prop("checked") == true){
					//$("tbody input[type='checkbox']").attr('checked', 'checked');
					$("tbody input[type='checkbox'][disabled!='disabled']").prop('checked',true);
				}else{
					$("tbody input[type='checkbox']").removeAttr('checked');
				}
				selectSpan.text($("tbody input[type='checkbox']:checked").size());
			});

			$('tbody').on('click', ' input[type=checkbox] ', function(){
				$(this).prop('checked')
					? $("tbody input[type='checkbox']:not(:checked)").size() == 0 && checkAllBox.prop('checked',true)
					: checkAllBox.removeAttr('checked');
				var size = $("tbody input[type='checkbox']:checked").size();
				selectSpan.text(size);
				if(size<=0) {
					$('#batchDiv a').attr("disabled", "disabled");	//未选中任何时，批量操作按钮置为不可用
				} else {
					$('#batchDiv a').removeAttr("disabled");
				}
			});
		}
	};

	window.Message = new Message();
	window.CheckBox = new CheckBox();
})();
