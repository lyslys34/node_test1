require.config({
	baseUrl: MT.STATIC_ROOT + '/js',
	urlArgs: 'ver=' + pageVersion,
});

require(['module/root', 'lib/artTemplate', 'page/settle/waybillDetail'], function(r, template, waybillDetail) {

	var infos = $('#infos_tmpl').html();

	var TheMap = {
		init: function() {
			var map = new BMap.Map('the-map');
			map.centerAndZoom(new BMap.Point(121.491, 31.233), 11);
			map.addControl(new BMap.NavigationControl());
			map.addControl(new BMap.ScaleControl());
			map.addControl(new BMap.OverviewMapControl());
			TheMap.map = map;
		},
		fillMap: function(orderDetail) {
			TheMap.clearMap();
			var map = TheMap.map;
			var startPoint = new BMap.Point(orderDetail.senderLngBd, orderDetail.senderLatBd);
			var endPoint = new BMap.Point(orderDetail.recipientLngBd, orderDetail.recipientLatBd);
			var walking = new BMap.WalkingRoute(map, {
				renderOptions: {
					map: map,
					autoViewport: true,
					panel: "the-map-info"
				}
			});
			walking.search(startPoint, endPoint);
			$('#the-map-info').show();
			$('#gaode-map-info').hide();
			setTimeout(function(){
		        var convertor = new BMap.Convertor();
		        var pointArr = [];
		        pointArr.push(new BMap.Point(orderDetail.senderLngCorrect,orderDetail.senderLatCorrect));
		        convertor.translate(pointArr, 3, 5, function(data){
		        	if(data.status==0){
		        		var pt = data.points[0];
						var myIcon = new BMap.Icon("/static/imgs/way_btn.png", new BMap.Size(30,45),{imageOffset:new BMap.Size(-4,-60)});
						var marker = new BMap.Marker(pt,{icon:myIcon});  
						map.addOverlay(marker); 
		        	}
		        })
		    }, 1000);
		},
		clearMap: function() {
			TheMap.map.clearOverlays();
		}
	}

	var gaodeMap = {
		init:function(){
			var scale = new AMap.Scale({
		        visible: true
		    }),
		    toolBar = new AMap.ToolBar({
		        visible: true
		    }),
		    overView = new AMap.OverView({
		        visible: true
		    });
		    this.map = new AMap.Map("gaode-map", {
		        resizeEnable: true
		    });
		    this.map.addControl(scale);
		    this.map.addControl(toolBar);
		    this.map.addControl(overView);
		    return this;
		},
		fillMap:function(orderDetail){
			var amap = this.map;
			this.clearMap();
			var driving = new AMap.Driving({
		        map: amap,
		        panel: "gaode-map-info",
		        policy:2
		    }); 
		    driving.search(new AMap.LngLat(orderDetail.senderLng, orderDetail.senderLat), new AMap.LngLat(orderDetail.recipientLng, orderDetail.recipientLat),{},function(){
		    	setTimeout(function(){
		    		$('.amap-lib-driving').before('<h4 style="padding:0 10px;font-size:16px;"> 开车前往 终点 的路线</h4>')
		    //		amap.setZoom(amap.getZoom()-1);
		    	},0)
		    });
		    $('#the-map-info').hide();
			$('#gaode-map-info').show();
		    this.maker = new AMap.Marker({
		        map: amap,
				position: [orderDetail.senderLngCorrect,orderDetail.senderLatCorrect],
		        icon: new AMap.Icon({            
		            size: new AMap.Size(30, 50),  //图标大小
		            image: "/static/imgs/way_btn.png",
		            imageOffset: new AMap.Pixel(0, -35)
		        })        
		    });
		    return this;
		},
		clearMap:function(){
			if(this.maker){
				this.maker.setMap(null);
		    	this.maker = null;
			} 
		}
	}

	var Query = {

		init: function() {
			Query.bindEvents();
		},
		bindEvents: function() {
			$('#search').on('click', function(event) {
				event.preventDefault();
				$(this).blur();

				Query.doSearch();
			});
			$('.infos').on('click', '#order-id', function(event) {
				event.preventDefault();
				var waybillId = $(this).attr('value');

				waybillDetail._showWaybillDetail(waybillId);
			});

			$('.infos').on('click', '#shop-name', function(event) {
				event.preventDefault();
				$.getJSON("/settleDistance/deliveryArea", {
			        poiId: $(this).data('value')
			      })
			      .done(function(json) {
			        code = json.code;
			        data = json.data.area;
			        if (code === 0) {
			          if( data.length == 0 ){
			            alert("暂时没有营业范围数据！");
			            return ;
			          }
			          var polygonArrs = new Array();
			          map_init();
			          drawLocation( json.data.location );
			          for (var i in data) {
			            var polygonArr = new Array();
			            var area = JSON.parse(data[i]);

			            area.forEach(function(e) {
			              polygonArr.push(new AMap.LngLat(e.y / 1000000.0, e.x / 1000000.0));
			            })
			            draw_area(polygonArr);
			          }
			          $('#area-map').modal('show');
			        } else {
			          alert("获取数据失败");
			        }
			      })
			      .fail(function() {
			        alert("请求数据失败");
			      })
			});

			$('.infos').on('click', '.cost-info .caret,.order-info .caret,.subsidy-info .caret', function(event) {
				event.preventDefault();
				$(this).parents('.cost-info,.order-info,.subsidy-info').toggleClass('detail-open');
				$(this).toggleClass('collapse')
			});
			$('[role=group]').find('button').bind('click',function(){
				$(this).parents('.btn-group-justified').find('.btn-success').removeClass('btn-success').addClass('btn-default');
				$(this).removeClass('btn-default').addClass('btn-success');
				$('#'+$(this).data('for')).show().siblings().hide();
				$('#'+$(this).data('for')+'-info').show().siblings().hide();
				if(Query.lastOrderDetail){
					$(this).data('for')=='the-map'?TheMap.fillMap(Query.lastOrderDetail):gaodeMap.fillMap(Query.lastOrderDetail);
				}
			})
		},
		doSearch: function(){
			var orderId = $('.search-field input').val();
			orderId = orderId.trim();
			if (orderId.length < 1) {
				return;
			};
			$.ajax({
					url: '/settleDistance/detail',
					type: 'GET',
					dataType: 'json',
					data: {
						platfromOrderId: orderId
					},
				})
				.done(function(result) {
					if (result['success'] == 0) {
						this.fillContents(result);
						$('#riderTrack').attr('href','http://dispatch.peisong.meituan.com/partner/dispatch/riderTrack?platformOrderId='+orderId)
					} else if (result['success'] == 2) {
						alert('查询失败！请检查订单编号是否正确');
					} else if (result['success'] == 3){
						alert('查询失败！订单编号不在查询范围以内');
					}else {
						alert('获取信息失败，请重试');
					}
				}.bind(this))
		},
		fillContents: function(result) {
			var resultStr = JSON.stringify(result).split('\\"').join('"').split('"{').join('{').split('}"').join('}');
			var result = JSON.parse(resultStr);
	//		TheMap.fillMap(result.orderDetail);
	//		gaodeMap.fillMap(result.orderDetail);
			this.lastOrderDetail = result.orderDetail;
			$('[role=group]').find('button.btn-success').trigger('click');
			var render = template.compile(infos);
	/*		var ob = JSON.parse(result.freightPlan.pkgPlan);
			result.planRule = ob;
			result.orderDetail.senderLngF = result.orderDetail.senderLng?result.orderDetail.senderLng.toFixed(6):'0';
			result.orderDetail.senderLatF = result.orderDetail.senderLat?result.orderDetail.senderLat.toFixed(6):'0';
			result.orderDetail.recipientLngF = result.orderDetail.recipientLng?result.orderDetail.recipientLng.toFixed(6):'0';
			result.orderDetail.recipientLatF = result.orderDetail.recipientLat?result.orderDetail.recipientLat.toFixed(6):'0'; */
			var html = render(result);
			$('.infos')[0].innerHTML = html;
			$('[data-toggle="tooltip"]').tooltip()
		}
	}

	function map_init() {
	    //初始化地图对象，加载地图
	    map = new AMap.Map("mapContainer", {
	      resizeEnable: true,
	      //二维地图显示视口
	      view: new AMap.View2D({
	        center: new AMap.LngLat(116.397428, 39.90923), //地图中心点
	        zoom: 100 //地图显示的缩放级别
	      })
	    });
	    //添加地图侧边栏插件
	    map.plugin(["AMap.ToolBar"], function() {
	      toolBar = new AMap.ToolBar();
	      map.addControl(toolBar);
	    });
	    //addmarker

	 }

	function drawLocation( address ) {
        lat = address.latitude/1000000.0;
        long = address.longitude/1000000.0;
        marker = new AMap.Marker({
          //icon: "http://webapi.amap.com/images/marker_sprite.png",
          position: [long, lat]
        });
        marker.setMap(map);
  	}

  	function draw_area(polygonArr) {
	    polygon = new AMap.Polygon({
	      map: map,
	      path: polygonArr, //设置多边形边界路径
	      strokeColor: "#FF33FF", //线颜色
	      strokeOpacity: 0.2, //线透明度
	      strokeWeight: 3, //线宽
	      fillColor: "#1791fc", //填充色
	      fillOpacity: 0.35 //填充透明度
	    });
	    polygon.setMap(map);
	    map.setFitView();
	}

	function init() {
		gaodeMap.init();
		TheMap.init();
		Query.init();
	}
	init();

});