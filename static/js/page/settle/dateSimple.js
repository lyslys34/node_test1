require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root', 'module/validator',  'module/cookie', 'page/common','lib/datepicker'], function (t, validator,cookie, common,datepicker) {

    $(document).delegate(".js_submitbtn", "click", function() {
            var url=$(this).attr("actionurl");
            submit(url);
        })

    function validate() {
        var startTimeStr = $(".js_date_start").val();
        var endTimeStr = $(".js_date_end").val();
        var startTime = new Date(Date.parse(startTimeStr.replace(/-/g,   "/")));
        var endTime = new Date(Date.parse(endTimeStr.replace(/-/g,   "/")));

        if(startTime > endTime) {
            alert("开始时间不能晚于结束时间");
            return false;
        }

        return true;
    }
    function submit(url){
        if(validate()){
            $("#formValidate1").attr("action",url);
            $("#formValidate1").submit();
        }
    }
});