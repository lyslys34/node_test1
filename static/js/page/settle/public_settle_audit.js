
$(document).ready(function(){
	init();
	CheckBox.checkBoxChecked();
	$('[href="#payModal"]').tooltip(); 
});

function init() {
	$(".pay_link").click(function() {
		var id = $(this).attr('rel');
	    $("#payForm").attr("action", "/publicSettleAudit/virtualPay?billIds=" + id);
	});
	
	$("#pay_button").click(function() {
		var checkbox = $("tbody input[type='checkbox']:checked");
		var ids = "";
		for(var i=0; i<checkbox.length; i++){ 
			var id = checkbox[i].value;
			ids += id + ",";
		}
		if (checkbox.length > 0) {
			ids = ids.substring(0,ids.length-1);
		}
	    $("#payForm").attr("action", "/publicSettleAudit/virtualPay?billIds=" + ids);
	});
}

(function(){
	function Message(){};
	Message.prototype = {
		showMessage: function(type, message) {
			$('#result').html(		
					"<div class='alert " + type + " alert-dismissible' role='alert'>" +
					"<button type='button' class='close' data-dismiss='alert'>" +
					"<span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span>" +
					"</button>" +
					"<strong>Success! " + message + "</strong></div>"
			);
		},
		type: {
			'SUCCESS' : 'alert-success',
			'INFO' : 'alert-info',
			'WARNING' : 'alert-warning',
			'DANGER' : 'alert-danger'
		}
	};
	
	function CheckBox(){};
	CheckBox.prototype = {
			checkBoxChecked : function checkBoxChecked() {
				var checkAllBox = $("thead input[type='checkbox']");
				var selectSpan = $("#selectNum");
				$(checkAllBox).click(function(){ 
					if($(this).prop("checked") == true){ 
						//$("tbody input[type='checkbox'][disabled!='disabled']").attr('checked', 'checked');
						$("tbody input[type='checkbox'][disabled!='disabled']").prop('checked',true);
					}else{ 
						$("tbody input[type='checkbox'][disabled!='disabled']").removeAttr('checked');
					}
					selectSpan.text($("tbody input[type='checkbox']:checked").size());
				});
				
				$('tbody').on('click', ' input[type=checkbox] ', function(){
					$(this).prop('checked')
					? $("tbody input[disabled!='disabled']:not(:checked)").size() == 0 && checkAllBox.attr('checked', 'checked')
							: checkAllBox.removeAttr('checked');
					selectSpan.text($("tbody input[type='checkbox']:checked").size());
				});
			}
		};

	window.Message = new Message();
	window.CheckBox = new CheckBox();
})();
