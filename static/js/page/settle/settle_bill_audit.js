require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
      'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
      'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
      'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
      'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
      'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
          'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
          'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
          'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
          'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu',
      ],
      'lib/autocomplete' : {
          deps : ['page/common']
      },
      'lib/bootstrap': {
          deps: ['lib/jquery']
      }
  }
});

require(['lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete'], function (autocomplete) {
    //组织名称联想搜索
    $('#openAccountName').autocomplete({
        source:function(request, response){
            $.ajax({
                url: "/settleBillAudit/nameList",
                dataType: "json",
                data: {
                    keyWords: request.term
                },
                success: function(data) {
                    response($.map(data, function(item) {
                        return { label: item, value: item }
                    }));
                }
            });
        },
        minLength: 1,
        autoFocus: false,
        delay: 200
    });

    //绑定回车事件
    $('#comment').keydown(function(e){
        if(e.keyCode==13){
            $("#refuseBtn").click();
        }
    });

    $('#batchDiv a').attr("disabled", "disabled");	//未选中任何时，批量操作按钮置为不可用

    //弹出框的确认按钮
    //确认通过
    $("#commit").click(function() {
        if($('input[name="invoiceType"]:checked').length < 1) {
            $('#invoice_error').css('display', 'inline-block');
            return false;
        }

        $('#invoice_error').css('display', 'none');
        fromAudit('agreeForm', 'commit');
    });

    //确认拒绝
    $("#refuseBtn").click(function() {
        if($('#comment').val() == "") {
            $('#alert_error').css('display', 'inline-block');
            return false;
        }

        $('#alert_error').css('display', 'none');
        fromAudit('disagreeForm', 'refuseBtn');
    });

    //确认停付
    $("#stopBtn").click(function() {
        if($('#stopComment').val() == "") {
            $('#stop_error').css('display', 'inline-block');
            return false;
        }

        $('#stop_error').css('display', 'none');
        fromAudit('stopForm', 'stopBtn');
    });

    //确定
    $('#confirmBtn').click(function(){
        window.location.reload();
    });

    /*
     * 处理checkbox
     */
    //单个审核
    $(".agree_link").click(function() {
        var id = $(this).attr('rel');
        $("#agreeForm").attr("action", "/settleBillAudit/audit?auditStatus=0&billIds=" + id);
    });

    $(".disagree_link").click(function() {
        var id = $(this).attr('rel');
        $("#disagreeForm").attr("action", "/settleBillAudit/audit?auditStatus=5&billIds=" + id);
    });

    $(".stop_link").click(function() {
        var id = $(this).attr('rel');
        $("#stopForm").attr("action", "/settleBillAudit/audit?auditStatus=6&billIds=" + id);
    });

    //批量
    //$("#agree_button").click(function() {
    //    var checkbox = $("tbody input[type='checkbox']:checked");
    //    var ids = "";
    //    for(var i=0; i<checkbox.length; i++){
    //        var id = checkbox[i].value;
    //        ids += id + ",";
    //    }
    //    if (checkbox.length > 0) {
    //        ids = ids.substring(0,ids.length-1);
    //    }
    //    $("#agreeForm").attr("action", "/settleBillAudit/audit?auditStatus=0&billIds=" + ids);
    //});
    //
    //$("#disagree_button").click(function() {
    //    var checkbox = $("tbody input[type='checkbox']:checked");
    //    var ids = "";
    //    for(var i=0; i<checkbox.length; i++){
    //        var id = checkbox[i].value;
    //        ids += id + ",";
    //    }
    //    if (checkbox.length > 0) {
    //        ids = ids.substring(0,ids.length-1);
    //    }
    //    $("#disagreeForm").attr("action", "/settleBillAudit/audit?auditStatus=5&billIds=" + ids);
    //});

    //全选相关
    //$(checkAllBox).click(function(){
    //    if($(this).prop("checked") == true){
    //        $("tbody input[type='checkbox'][disabled!='disabled']").prop('checked',true);
    //    }else{
    //        $("tbody input[type='checkbox']").removeAttr('checked');
    //    }
    //    selectSpan.text($("tbody input[type='checkbox']:checked").size());
    //});
    //
    //$('tbody').on('click', ' input[type=checkbox] ', function(){
    //    $(this).prop('checked')
    //        ? $("tbody input[type='checkbox']:not(:checked)").size() == 0 && checkAllBox.prop('checked',true)
    //        : checkAllBox.removeAttr('checked');
    //    var size = $("tbody input[type='checkbox']:checked").size();
    //    selectSpan.text(size);
    //    if(size<=0) {
    //        $('#batchDiv a').attr("disabled", "disabled");	//未选中任何时，批量操作按钮置为不可用
    //    } else {
    //        $('#batchDiv a').removeAttr("disabled");
    //    }
    //});

    /*
     * 直接提交表格，auditStatus&billIds写入form的action中
     */
    function fromAudit(formId, btnId) {
        $("#" + btnId).attr("disabled", "disabled");	//确认按钮置为不可用
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : $('#' + formId).attr('action'),
            data : $('#' + formId).serialize(),
            success : function(data){
                console.dir(data);
                var msg = "操作失败，请稍后重试!";
                $('#remainBtn').css('display', 'block');
                $('#confirmBtn').css('display', 'none');
                if(data.success){
                    msg = "操作成功！";
                    $('#remainBtn').css('display', 'none');
                    $('#confirmBtn').css('display', 'block');
                } else {
                    msg = msg + "原因：" + data.failMsg;
                }
                $('#infoMsg').html(msg);
                $('#infoModal').modal('show');
            },
            error:function(XMLHttpRequest ,errMsg){
                console.log("fail");
                $('#remainBtn').css('display', 'block');
                $('#confirmBtn').css('display', 'none');
                $('#infoMsg').html("网络连接失败，请稍后重试");
                $('#infoModal').modal('show');
                $("#" + btnId).removeAttr("disabled");	//使按钮可用
            }
        });
    }
});
