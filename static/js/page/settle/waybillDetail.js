define(['module/ui'], function(ui) {

	var detailRequest;
  	var detailDia;
	function showWaybillDetail(waybillId) {

		if (detailDia) {
			detailDia.remove();
			detailDia = null;
		};
		if (detailRequest) {
			detailRequest.abort();
		};
		var body = '<div id="detailCon">这是ID＝' + waybillId + '的订单详情</div>';
		detailRequest = $.ajax({
			url: "/partner/waybill/detailV2.ajax?waybillId=" + waybillId,
			success: function(result) {

				body = '<div>';
				body = body + result + '</div>';
				detailDia = ui.showModalDialog({
					title: '订单详情',
					body: body,
					buttons: [],
					style: {
						".modal-footer": {
							"display": "none"
						},
						".modal-title": {
							"display": "none"
						},
						".modal-dialog": {
							"width": "900px"
						},
						".modal-body": {
							"padding-top": "0"
						},
						".modal-header .close": {
							"margin-right": "-8px",
							"margin-top": "-10px"
						},
						".modal-header": {
							"border-bottom": "none"
						}
					}
				});
				detailDia.on("hidden.bs.modal", function() {
					this.remove();
					detailDia = null;
				});
			},
			error: function() {}
		});
	}

	return {

		_showWaybillDetail: function(waybillId) {

			showWaybillDetail(waybillId);
		},
	};
});