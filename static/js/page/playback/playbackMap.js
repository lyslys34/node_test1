define(['module/ui', 'lib/moment-with-locales.min', 'module/utils', 'page/mapUtil'], function(ui, moment, utils, mapUtil) {

  //一些变量

  var back_waybill;
  var back_dispatchDetail;
  var back_seq;
  var riderMarkers = [];
  var startMarker;
  var endMarker;
  var curRiderList;
  var detailDia;
  var drawLineMarkers = new Array();
  var map_selRider = $('#map_select_rider');
  var keyList = {};

  function isOnElement(element, bottom_off) {

    if (!element) {
      return;
    };
    var boff = 0;
    if (bottom_off) {
      boff = bottom_off;
    };
    var x = event.pageX;
    var y = event.pageY;
    var top = element.offset().top;
    var left = element.offset().left;
    var width = element.width();
    var height = element.height();

    if (x < left || y < top || x > width + left || y > top + height + boff) {

      return false;
    };

    return true;
  }

  function getSelid(ele) {
    var cityid;
    var selector = ele + " option:selected"
    $(selector).each(function() {
      cityid = $(this).val();
    });
    return cityid;
  }

  function getWaybillInfo(waybillId) {

    loadingWaybillInfo();
    $.ajax({
        url: '/playback/waybillDetail',
        type: 'POST',
        dataType: 'json',
        data: {
          platformOrderId: waybillId
        },
      })
      .done(function(result) {

        fillWaybillInfo(result);
      })
      .fail(function(err) {
        $('#loading_info').text('请求出错 请重试');
      })
      .always(function() {

        $('#btn-search').blur();
      });

  }

  function loadingWaybillInfo() {
    $('#waybill_infobody').empty();
    $('#loading_info').removeClass('hidden');
    $('#loading_info').text('加载中');
  }

  function getTime(timestamp) {
    var date = new Date(timestamp);
    var str = getFormatedString(date.getMonth() + 1) + '-' + getFormatedString(date.getDate()) + ' ' + getFormatedString(date.getHours()) + ':' + getFormatedString(date.getMinutes()) + ':' + getFormatedString(date.getSeconds());
    return str;
  }

  function getFormatedString(value) {
    if (value > 9) {
      return new String(value);
    }
    return '0' + new String(value);
  }


  function drawWaybillPath() {

    var scale = 1000000;
    var start_xy = new AMap.LngLat(back_waybill.senderLng / scale, back_waybill.senderLat / scale);
    var end_xy = new AMap.LngLat(back_waybill.recipientLng / scale, back_waybill.recipientLat / scale);
    //根据起点、终点  画订单的路径
    AMap.service('AMap.Driving', function() {
      var walking = new AMap.Driving();
      walking.search(start_xy, end_xy, function(status, data) {
        if (status === 'complete') {
          // 画订单的路径
          fnDrawBillWay(start_xy, end_xy, data);
        } else {
          map.clearMap();
          console.log("高德地图没有能为你规划好路径，请刷新重试！")
        }
        getRiderList();
      });
    });
  }

  //画订单的路线
  function fnDrawBillWay(start_xy, end_xy, data) {

    map.clearMap();
    addStartEndMarker(start_xy, end_xy);
    var steps = data.routes[0].steps;
    var ways = data.waypoints;

    //起点到路线的起点 路线的终点到终点 绘制无道路部分
    var startPath = [];
    startPath.push(start_xy);
    startPath.push(steps[0].path[0]);
    var startLine = new AMap.Polyline({
      map: map,
      path: startPath,
      strokeColor: "green",
      strokeWeight: 6,
      strokeDasharray: [10, 5]
    });
    var endPath = [];
    var path_xy = steps[(steps.length - 1)].path;
    endPath.push(end_xy);
    endPath.push(path_xy[(path_xy.length - 1)]);
    var endLind = new AMap.Polyline({
      map: map,
      path: endPath,
      strokeWeight: 6,
      strokeColor: "green",
      strokeDasharray: [10, 5]
    });
    //绘制服务返回路径 有道路的部分
    var path = [];
    for (var s = 0; s < steps.length; s++) {
      path.push.apply(path, steps[s].path);
    }
    var polyline = new AMap.Polyline({
      map: map,
      path: path,
      strokeWeight: 6,
      strokeColor: "green",
      strokeDasharray: [10, 5]
    });
  }

  function fillWaybillInfo(result) {

    if (result.code != 0 || result.data.length == 0) {

      $('#loading_info').text('没有可回放记录');
      return;
    };
    keyList = result.data.scoreDes;

    var html = '';
    $('#loading_info').addClass('hidden');
    $('#waybill_infobody').empty();

    back_waybill = result.data;
    var waybill = result.data;


    var orderTime = getTime(waybill.orderTime * 1000);
    var deliveredTime = getTime(waybill.deliveredTime * 1000);
    var riderName = '';
    if (!waybill.riderName) {
      riderName = '-';
    } else {
      riderName = waybill.riderName;
    }

    html = html + '<tr>';
    html = html + '<td>' + getStatusString(waybill.status) + '</td>';
    html = html + '<td>' + waybill.orgName + '.' + waybill.deliveryAreaName + '</td>';
    html = html + '<td>' + waybill.senderName + '#' + waybill.poiSeq + '</td>';
    html = html + '<td>' + waybill.recipientAddress + '</td>';
    html = html + '<td>' + orderTime + '</td>';
    if (waybill.isPrebook) {
      html = html + '<td>' + '<span class="success_color">[预]</span>' + deliveredTime + '</td>';
    } else {
      html = html + '<td>' + deliveredTime + '</td>';
    }

    html = html + '<td>' + riderName + '</td>';
    html = html + '<td style="cursor:pointer"><span data-toggle="tooltip" title="详情" data-placement = "top" name = "detail">' + '<i class="fa fa-info-circle fa-lg opration-icon"></i></span></td>';
    html = html + '</tr>';


    $('#waybill_infobody').append(html);
    $('[data-toggle="tooltip"]').tooltip();
    drawWaybillPath();
    addMapEvents();
  }

  function getStatusString(status) {
    var str = '';
    switch (status) {
      case 0:
        str = '未调度';
        break;
      case 10:
        str = '未接单';
        break;
      case 15:
        str = '派单后未确认';
        break;
      case 20:
        str = '已接单';
        break;
      case 30:
        str = '已取货';
        break;
      case 50:
        str = '已送达';
        break;
      case 99:
        str = '已取消';
        break;
    }
    return str;
  }

  var detailRequest;

  function showWaybillDetail(waybillId) {

    if (detailDia) {
      detailDia.remove();
      detailDia = null;
    };
    if (detailRequest) {
      detailRequest.abort();
    };
    var body = '<div id="detailCon">这是ID＝' + waybillId + '的订单详情</div>';
    detailRequest = $.ajax({
      url: "/partner/waybill/detailV2.ajax?waybillId=" + waybillId,
      success: function(result) {

        body = '<div>';
        body = body + result + '</div>';
        detailDia = ui.showModalDialog({
          title: '订单详情',
          body: body,
          buttons: [],
          style: {
            ".modal-footer": {
              "display": "none"
            },
            ".modal-title": {
              "display": "none"
            },
            ".modal-dialog": {
              "width": "900px"
            },
            ".modal-body": {
              "padding-top": "0"
            },
            ".modal-header .close": {
              "margin-right": "-8px",
              "margin-top": "-10px"
            },
            ".modal-header": {
              "border-bottom": "none"
            }
          }
        });
        detailDia.on("hidden.bs.modal", function() {
          this.remove();
          detailDia = null;
        });
      },
      error: function() {}
    });
  }

  function addMapEvents() {

    map.on('moveend', function(e) {
      reDrawArcsIfNeeded();
    });

    map.on('zoomend', function(e) {
      reDrawArcsIfNeeded();
    });

    map.on('resize', function(e) {
      mapUtil.removeCanvas();
      reDrawArcsIfNeeded();
    });

    map.on('dragstart', function(e) {
      mapUtil.clearCanvas();
    });

    map.on('dragend', function(e) {});

    map.on('dragging', function(e) {});
  }

  function reDrawArcsIfNeeded() {
    mapUtil.clearCanvas();
    for (var i = 0; i < drawLineMarkers.length; i++) {
      var marker = drawLineMarkers[i];
      drawMarkerArcLines(marker);
    };
  }

  function drawMarkerArcLines(marker) {
    var lineArr = marker.arcLines;
    if (lineArr && lineArr.length > 0) {
      mapUtil.createCanvasIfNeeded();
      for (var i = 0; i < lineArr.length; i++) {
        var line = lineArr[i];
        drawCurve(line.startX, line.startY, line.endX, line.endY, line.lineColor);
      };
    };
  }

  function drawCurve(startX, startY, endX, endY, lineColor) {

    if (!startX || !startY || !endX || !endY) {
      return null;
    };
    var sPos = getDomPos(startX, startY);
    var ePos = getDomPos(endX, endY);

    mapUtil.drawCurve(sPos, ePos, lineColor);
    return {
      startX: startX,
      startY: startY,
      endX: endX,
      endY: endY,
      lineColor: lineColor,
    };
  }

  function getTimeString(time) {
    var text = moment.unix(time).locale('zh-cn').format('YYYY-MM-DD HH:mm:ss');
    return text;
  }

  function clearMarkersAndLine() {
    for (var i = riderMarkers.length - 1; i >= 0; i--) {
      var marker = riderMarkers[i];
      marker.setMap(null);
    };
    closeInfoWindow();
    riderMarkers = [];
  }

  function getScoreString(score){

    var str = '';
    if (score >= 0) {
      str = '[' + score + '分]';
    };
    return str;
  }

  function fillRiderSelect(riderList) {

    map_selRider.empty();
    var option = '<option selected="selected" value="0">选择骑手定位</option>';
    for (var i = 0; i < riderList.length; i++) {
      var rider = riderList[i];
      if (isA) {
        var showScore = getScoreString(rider.score);
        option = option + '<option value="' + rider.riderId + '">' + showScore + rider.riderName + '</option>';
      } else {
        option = option + '<option value="' + rider.riderId + '">' + rider.riderName + '</option>';
      }

    };
    map_selRider.append(option);
    $.fn.select2.amd.require(['select2/compat/matcher'], function(oldMatcher) {
      map_selRider.select2({
        matcher: oldMatcher(matchPinyin)
      });
    });
    map_selRider.unbind('change');
    map_selRider.on("change", function(e) {
      var riderId = getSelid('#map_select_rider');
      makeRiderCenter(riderId);
      selectKeyValueTr(riderId);
    });

    setTimeout(function() {
      map_selRider.val(back_waybill.riderId).trigger('change');
    }, 500);
  }

  function makeRiderCenter(riderId) {
    $('.marker-text-container').removeClass('select_rider_color');
    for (var i = riderMarkers.length - 1; i >= 0; i--) {
      var marker = riderMarkers[i];
      if (marker.id == riderId) {
        map.setCenter(marker.getPosition());
        marker.setTop(true);
        $('#markerSpan' + riderId).addClass('select_rider_color');
      } else {
        marker.setTop(false);
      }
    };
  }

  function addStartEndMarker(start_xy, end_xy) {
    //dd start and end

    var waybill = back_waybill;
    var scale = 1000000;


    var startMarker = new AMap.Marker({
      position: start_xy,
      offset: new AMap.Pixel(-16, -41), //相对于基点的偏移位置
      icon: '/static/imgs/marker-start.png',
      map: map,
    });

    var endMarker = new AMap.Marker({
      position: end_xy,
      offset: new AMap.Pixel(-16, -41), //相对于基点的偏移位置
      icon: '/static/imgs/marker-end.png',
      map: map
    });

    startMarker.setMap(map);
    endMarker.setMap(map);
  }

  function fillMap() {

    //add riders
    var riderList = getMapRider();
    $.each(riderList, function(index, rider) {

      addRiderMarker(rider);
    });
    map.setFitView();
    fillKeyValueTable(riderList);
    fillRiderSelect(riderList);
  }

  function addRiderMarker(rider) {
    var lng = rider.riderLng;
    var lat = rider.riderLat;
    if (lng == 0 || lat == 0) {

      return;
    };

    lng = lng / 1000000;
    lat = lat / 1000000;

    var markerName = "normal-color";
    if (rider.isSelect == 1) {
      markerName = "busy-color";
    }

    var riderStr = rider.riderName + '[' + rider.riderWaybillIdsSize + '单]';
    if (rider.isSelect == 2 && rider.reason) {
      riderStr += '[' + rider.reason + ']';
    };

    if (isA) {
      var showScore = getScoreString(rider.score);
      riderStr = riderStr + showScore;
    };

    var markerContent = document.createElement("div");
    markerContent.className = "bm_rider_marker_container";

    var markerBack = document.createElement("div");
    markerBack.className = "bm_circle_marker " + markerName;
    markerContent.appendChild(markerBack);

    var textContainer = document.createElement("div");
    textContainer.className = "marker-text-container";
    textContainer.id = 'markerSpan' + rider.riderId;
    markerContent.appendChild(textContainer);

    var textBack = document.createElement("div");
    textBack.className = "marker-text-back";
    textContainer.appendChild(textBack);

    var markerSpan = document.createElement("span"); //点标记中的文本
    markerSpan.className = "marker-span";
    markerSpan.innerHTML = riderStr;
    markerSpan.style.width = riderStr.length * 11 + 'px';
    textContainer.appendChild(markerSpan);

    var marker = new AMap.Marker({
      map: map,
      position: new AMap.LngLat(lng, lat), //基点位置
      offset: new AMap.Pixel(-10, -10), //相对于基点的偏移位置
      draggable: false, //是否可拖动
      topWhenClick: true,
      topWhenMouseOver: true,
      content: markerContent //自定义点标记覆盖物内容
    });

    marker.title = rider.riderName;
    marker.id = rider.riderId;
    marker.isSelect = rider.isSelect;

    marker.setMap(map); //在地图上添加点

    AMap.event.addListener(marker, 'click', function() {

      if (marker == startMarker || marker == endMarker) {
        return;
      };
      if (marker.infoWindow && marker.infoWindow.getIsOpen()) {

        marker.infoWindow.close();
        marker.infoWindow = null;
        clearPositionMakerAndLines(marker);
      } else {
        var rider = riderForId(marker.id);
        getRiderWaybills(marker);
      }

    });
    riderMarkers.push(marker);

    marker.marker1 = new Array();
    marker.marker2 = new Array();

    AMap.event.addListener(marker, 'mouseover', function() {

      if (marker == startMarker || marker == endMarker || rider.isSelect == 0) {
        return;
      }
      $('.bm_rider_marker_container').addClass('opacity3');
      var co = marker.getContent();
      co.className = 'bm_rider_marker_container';

      if (hasMarkerId(marker.id)) {
        return;
      };

      addArcLines(rider, marker);
    });
    AMap.event.addListener(marker, 'mouseout', function() {
      $('.bm_rider_marker_container').removeClass('opacity3');
      if (marker.infoWindow && marker.infoWindow.getIsOpen()) {

      } else {
        clearPositionMakerAndLines(marker);
      }
    });
  }

  function addPositionMarker(x, y, lineColor, text) {
    var markerContent = mapUtil.getSendMarkerContent(lineColor, text);

    var marker = new AMap.Marker({
      map: map,
      position: new AMap.LngLat(x, y), //基点位置
      offset: new AMap.Pixel(0, 0), //相对于基点的偏移位置
      draggable: false, //是否可拖动
      content: markerContent //自定义点标记覆盖物内容
    });
    marker.setMap(map);
    return marker;
  }

  function clearPositionMakers(marker) {
    for (var i = 0; i < marker.marker1.length; i++) {
      if (marker.marker1[i]) marker.marker1[i].setMap(null);
    }
    for (var i = 0; i < marker.marker2.length; i++) {
      if (marker.marker2[i]) marker.marker2[i].setMap(null);
    }

    marker.marker1 = new Array();
    marker.marker2 = new Array();
  }

  function removeArcMarker(marker) {
    var newArr = new Array();
    for (var i = 0; i < drawLineMarkers.length; i++) {
      var other = drawLineMarkers[i];
      if (marker.id != other.id) {
        newArr.push(other);
      };
    };
    drawLineMarkers = newArr;
  }

  function redrawOrRemoveCanvas() {
    if (drawLineMarkers.length < 1) {
      mapUtil.removeCanvas();
      return;
    };
    reDrawArcsIfNeeded();
  }

  function hasMarkerId(markerId) {
    var has = false;
    for (var i = 0; i < drawLineMarkers.length; i++) {
      var other = drawLineMarkers[i];
      if (other.id == markerId) {
        has = true;
        break;
      };
    };
    return has;
  }

  function clearOtherMarker(marker) {
    var has = false;
    for (var i = 0; i < drawLineMarkers.length; i++) {
      var other = drawLineMarkers[i];
      if (other.id != marker.id) {
        has = true;
        clearPositionMakers(other);
      };
    };
    return has;
  }

  function clearPositionMakerAndLines(marker) {

    clearPositionMakers(marker);
    removeArcMarker(marker);
    redrawOrRemoveCanvas();
  }

  function addArcLines(rider, marker) {

    marker.arcLines = new Array();
    mapUtil.createCanvasIfNeeded();
    drawLineMarkers.push(marker);

    var lng = rider.riderLng / 1000000;
    var lat = rider.riderLat / 1000000;

    var riderX = lng;
    var riderY = lat;

    var len = rider.waybillCoordinateListSize;
    for (var i = 0; i < len; i++) {
      var getX = rider.waybillCoordinateList[i].senderLng / 1000000;
      var getY = rider.waybillCoordinateList[i].senderLat / 1000000;
      var sendX = rider.waybillCoordinateList[i].recipientLng / 1000000;
      var sendY = rider.waybillCoordinateList[i].recipientLat / 1000000;

      var getXY = new AMap.LngLat(getX, getY);
      var sendXY = new AMap.LngLat(sendX, sendY);

      lineColor = mapUtil.getArcLineColor(i);
      if (rider.waybillCoordinateList[i].status == 20 || rider.waybillCoordinateList[i].status == 15) {

        getMarker = addPositionMarker(getX, getY, lineColor, '取');
        marker.marker1.push(getMarker);

        sendMarker = addPositionMarker(sendX, sendY, lineColor);
        marker.marker1.push(sendMarker);

        curve = drawCurve(getX, getY, riderX, riderY, lineColor);
        if (curve) {
          marker.arcLines.push(curve);
        };
        curve = drawCurve(getX, getY, sendX, sendY, lineColor);
        if (curve) {
          marker.arcLines.push(curve);
        };

      } else if (rider.waybillCoordinateList[i].status == 30) {

        sendMarker = addPositionMarker(sendX, sendY, lineColor);
        marker.marker2.push(sendMarker);
        curve = drawCurve(sendX, sendY, riderX, riderY, lineColor);
        if (curve) {
          marker.arcLines.push(curve);
        };
      }
    }
  }

  function getDomPos(lng, lat) {
    var lll = new AMap.LngLat(lng, lat);
    return map.lngLatToContainer(lll);
  }

  function riderForId(riderid) {
    var ret;
    $.each(curRiderList, function(index, rider) {

      if (rider.riderId == riderid) {

        ret = rider;
      }
    });

    return ret;
  }

  function getRiderWaybills(marker) {
    var i = getSelectIndex();
    if (i >= back_dispatchDetail.length) {
      return;
    };
    time = back_dispatchDetail[i].dispatchTime;
    $.ajax({
        url: '/playback/riderWaybillListDetail',
        type: 'POST',
        dataType: 'json',
        data: {
          waybillId: back_waybill.waybillId,
          dispatchTime: time,
          riderId: marker.id
        },
      })
      .done(function(result) {

        var infoWindow = new AMap.InfoWindow({
          isCustom: true, //使用自定义窗体
          content: createInfoWindow(marker, result),
          offset: new AMap.Pixel(20, -35) //-113, -140
        });
        infoWindow.open(map, marker.getPosition());
        marker.infoWindow = infoWindow;
      })
      .fail(function() {})
      .always(function() {

      });
  }

  function getPhoneType(type) {
    var str = '未知';
    if (type == 1) {
      str = 'Android';
    } else if (type == 2) {
      str = 'iOS';
    }
    return str;
  }

  //构建自定义信息窗体
  function createInfoWindow(title, result) {
    var info = document.createElement("div");
    info.className = "info";

    //可以通过下面的方式修改自定义窗体的宽高
    //info.style.width = "400px";

    // 定义顶部标题
    var top = document.createElement("div");
    var titleD = document.createElement("div");
    var closeX = document.createElement("span");
    top.className = "info-top";

    var rider = riderForId(title.id);

    if (title.isSelect == 1) {
      top.style.backgroundColor = '#ff5a5a';
    } else {
      top.style.backgroundColor = '#00abe4';
    }
    top.style.filter = 'alpha(opacity:80)';
    top.style.opacity = 0.8;
    if (isA) {
      var showScore = getScoreString(rider.score);
      titleD.innerHTML = showScore;
    };

    titleD.innerHTML = titleD.innerHTML + title.title + '[' + result.bmUserView.mobile + '][' + result.bmWaybillViewList.length + '单]';
    titleD.innerHTML = titleD.innerHTML + '&nbsp;&nbsp;[' + rider.deviceType + '_' + getPhoneType(rider.osType) + rider.appVersion + ']';
    titleD.style.color = '#ffffff';

    closeX.innerHTML = '×'
    closeX.setAttribute("aria-hidden", "true");
    closeX.style.position = 'absolute';
    closeX.style.right = '0';
    closeX.style.color = 'white';
    closeX.style.margin = '2px 10px 0 0';
    closeX.style.fontSize = '20px';
    closeX.style.cursor = 'pointer';
    closeX.onclick = closeInfoWindow;



    // 定义中部内容
    var middle = document.createElement("div");
    middle.className = "info-middle";
    middle.style.backgroundColor = 'white';

    var table = document.createElement("table");
    table.className = "table";

    var tbody = document.createElement('tbody');

    var lineColor = '#f00';
    var i = 0;
    $.each(result.bmWaybillViewList, function(index, waybill) {


      var tr = document.createElement("tr");

      var td = document.createElement("td");
      td.className = 'width80';
      lineColor = mapUtil.getArcLineColor(i);
      var innerHTML = '<div class="color-block" style="background-color:' + lineColor + '"></div>';
      td.innerHTML = innerHTML + waybill.remark;
      tr.appendChild(td);

      var deliveredTime = new Date(waybill.deliveredTime * 1000);
      td = document.createElement("td");
      td.className = 'width80';
      if (waybill.isPrebook) {
        td.innerHTML = '<span style="color:#00ABE4">[预]</span>'
        td.innerHTML += get2Format(deliveredTime.getHours()) + ':' + get2Format(deliveredTime.getMinutes());
      } else {
        td.innerHTML += get2Format(deliveredTime.getHours()) + ':' + get2Format(deliveredTime.getMinutes());
      }

      tr.appendChild(td);


      td = document.createElement("td");
      td.innerHTML = '取：' + waybill.senderName + '#' + waybill.poiSeq;
      tr.appendChild(td);

      td = document.createElement("td");
      td.innerHTML = '送：' + waybill.recipientAddress;
      tr.appendChild(td);

      var dis = waybill.distant / 1000;
      dis = dis.toFixed(1);
      td = document.createElement("td");
      td.innerHTML = dis + 'km';
      tr.appendChild(td);

      tbody.appendChild(tr);
      ++i;
    });

    //titleD.innerHTML += '【'+ result.length + '单】' ;
    top.appendChild(titleD);
    top.appendChild(closeX);
    info.appendChild(top);

    table.appendChild(tbody);
    middle.appendChild(table);
    info.appendChild(middle);

    // 定义底部内容
    var bottom = document.createElement("div");
    bottom.className = "info-bottom";
    bottom.style.position = 'relative';
    bottom.style.top = '0px';
    bottom.style.margin = '0 auto';
    var sharp = document.createElement("img");
    sharp.src = "http://webapi.amap.com/images/sharp.png";
    bottom.appendChild(sharp);
    info.appendChild(bottom);
    return info;
  }

  function get2Format(number) {
    if (number > 9) {
      return number
    };
    return '0' + number;
  }

  function closeInfoWindow() {
    map.clearInfoWindow();
    for (var i = drawLineMarkers.length - 1; i >= 0; i--) {
      var marker = drawLineMarkers[i];
      clearPositionMakers(marker);
    };
    drawLineMarkers = new Array();
    mapUtil.removeCanvas();
  }


  function selectRider(rider, check) {
    if (rider.siblings(check).is(':checked')) {
      rider.siblings(check).prop("checked", false);
      console.log(1)
    } else {
      rider.siblings(check).prop("checked", true);
      console.log(2);
    }

  }

  function getTotal(array) {
    var total = 0;
    for (var i = 0; i < array.length; i++) {
      total = total + array[i];
    }
    return total;
  }

  function fillRecordList(result) {

    var playTable = $('#playRecord');
    playTable.empty();
    back_dispatchDetail = [];
    curRiderList = [];
    var len = result.length;
    if (len == 0) {
      return;
    };

    back_dispatchDetail = result;

    var sel = len - 1;
    curRiderList = result[len - 1].playbackRiderViewList;
    if (back_seq > 0 && back_seq <= len) {
      curRiderList = result[back_seq - 1].playbackRiderViewList;
      sel = back_seq - 1;
    };

    var html = '';
    var countArray = new Array();
    var dispatchStrategyDes = '';
    $.each(result, function(index, riderView) {

      var count = 0;
      dispatchStrategyDes = riderView.dispatchStrategyDes;
      var max = riderView.playbackRiderViewList.length;
      for (var j = 0; j < max; j++) {
        if (riderView.playbackRiderViewList[j].isSelect == 2) {
          count++;
        }
      }
      var date = new Date(riderView.dispatchTime * 1000);
      html = html + '<tr>';
      html = html + '<td style="width:85px">[' + (index + 1) + ']' + riderView.dispatchTypeDes + '</td>';
      html = html + '<td style="width:65px">' + getFormatedString(date.getHours()) + ':' + getFormatedString(date.getMinutes()) + ':' + getFormatedString(date.getSeconds()) + '</td>';
      html = html + '<td style="width:65px">' + count + '</td>';
      html = html + '<td style="width:28px">' + '<input type="radio" style="opacity:1;position:static;" class="choose" name="chooseRider"/>' + '</td>';
      html = html + '</tr>';
      countArray.push(count);
    })

    $('#playRecord').append(html);
    // $('#dispatch-des').text(dispatchStrategyDes);
    var check = $('#playRecord').find('.choose')[sel];
    check.checked = true;
    selectTr(true);
  }

  function getRiderList() {

    $.ajax({
        url: '/playback/waybillPlaybackDetail',
        type: 'POST',
        dataType: 'json',
        data: {
          waybillId: back_waybill.waybillId
        },
      })
      .done(function(result) {

        fillRecordList(result.data);
        fillMap();
      })
      .fail(function() {})
      .always(function() {

      });
  }

  function getMapRider() {

    var rList = [];
    var check2 = $('.rider-recommend').prop('checked');
    var check1 = $('.rider-notRecommend').prop('checked');
    for (var i = curRiderList.length - 1; i >= 0; i--) {
      var rider = curRiderList[i];
      if ((rider.isSelect == 1 && check1) || (rider.isSelect == 2 && check2)) {
        rList.push(rider);
      };
    };
    rList.sort(function(rider1, rider2) {

      if (rider1.score < 0) {
        return 1;
      } else if (rider2.score < 0) {
        return -1;
      }
      return rider1.score - rider2.score;
    });
    return rList;
  }

  function getId() {

    return utils.urlArg('id');
  }

  function btnSearch() {
    var waybillId = new String($('#waybillId_input').val());
    waybillId = waybillId.trim();

    if (waybillId.length > 0) {
      clearMarkersAndLine();
      map.clearMap();
      getWaybillInfo(waybillId);
    }
  }

  function initMap() {
    //初始化地图对象，加载地图
    map = new AMap.Map("mapContainer", {
      resizeEnable: true,
      view: new AMap.View2D({
        resizeEnable: true,
        zoom: 14 //地图显示的缩放级别
      }),
      keyboardEnable: false
    });

    map.plugin(["AMap.Scale"], function() {
      scale = new AMap.Scale();
      map.addControl(scale);
    });

    //在地图中添加ToolBar插件
    map.plugin(["AMap.ToolBar"], function() {
      toolBar = new AMap.ToolBar({
        direction: false,
      });
      toolBar.hideLocation();
      map.addControl(toolBar);
    });
    var rangingTool;
    map.plugin(["AMap.RangingTool"], function() {
      rangingTool = new AMap.RangingTool(map);
      AMap.event.addListener(rangingTool, "end", function(e) {
        rangingTool.turnOff();
        $('#RangingTool').removeClass('active');
      });
    });

    $('#RangingTool').click(function(event) {

      $(this).addClass('active');
      rangingTool.turnOn();
    });
    $('#full_screen_btn').click(function(event) {

      if ($('#mapContainer').hasClass('full')) {
        $('span', $(this)).text('全屏');
      } else {
        $('span', $(this)).text('退出');
      }
      $('#mapContainer').toggleClass('full');
    });
  }

  function bindEvents() {
    $('#waybill_infobody').delegate('[name=detail]', 'click', function(event) {

      if (event && event.stopPropagation) {
        event.stopPropagation();
      } else {
        window.event.cancelBubble = true;
      }

      showWaybillDetail(back_waybill.waybillId);
    });

    $('#waybill_infobody').delegate('[name=detail]', 'dblclick', function(event) {

      if (event && event.stopPropagation) {
        event.stopPropagation();
      } else {
        window.event.cancelBubble = true;
      }
    });

    $('#btn-search').click(btnSearch);

    $('.rider-recommend').click(function() {
      clearMarkersAndLine();
      fillMap();
    });

    $('.rider-notRecommend').click(function() {
      clearMarkersAndLine();
      fillMap();
    });

    $('#playRecord').on('click', '.choose', function(event) {

      var i = $(this).parents('tr').index();
      if (i < back_dispatchDetail.length) {
        curRiderList = back_dispatchDetail[i].playbackRiderViewList;
      };
      selectTr();
      clearMarkersAndLine();
      fillMap();
    });

    $('#no_score_count').hover(function() {

      var tooltip = $('#listTip').empty();
      var body = '';
      for (var i = 0; i < curRiderList.length; i++) {
        var rider = curRiderList[i];
        if (rider.isSelect == 0) {
          if (rider.workStatus == 0) {
            var riderStr = rider.riderName + '[离岗]';
            body = body + '<p>' + riderStr + '</p>';
          } else if (rider.online == 0) {
            var riderStr = rider.riderName + '[掉线]';
            body = body + '<p>' + riderStr + '</p>';
          }

        };
      };
      if (body == '') {
        return;
      };
      tooltip.append(body);
      tooltip.removeClass('hidden');
      tooltip.css({
        bottom: 33,
        left: 114
      });
    }, function() {

      if (!isOnElement($('#listTip'), 10)) {
        $('#listTip').addClass('hidden');
      };
    });
    $('#listTip').mouseleave(function(event) {
      $('#listTip').addClass('hidden');
    });

    $('#recommend_count').hover(function() {

      var tooltip = $('#listTip').empty();
      var body = '';
      for (var i = 0; i < curRiderList.length; i++) {
        var rider = curRiderList[i];
        if (rider.isSelect == 2 && rider.online == 0) {
          var riderStr = rider.riderName + '[' + rider.riderWaybillIdsSize + '单]';
          body = body + '<p>' + riderStr + '</p>';
        };
      };
      if (body == '') {
        return;
      };
      tooltip.append(body);
      tooltip.removeClass('hidden');
      tooltip.css({
        bottom: 102,
        left: 112
      });
    }, function() {
      $('#listTip').addClass('hidden');
    });
  }

  function getSelectIndex() {

    var choose = $('#playRecord .choose:checked');
    var i = choose.parents('tr').index();
    return i;
  }

  function selectTr(scroll) {
    $('#playRecord .choose').each(function(index, el) {

      var jel = $(el);
      if (el.checked) {
        jel.parents('tr').addClass('select_back');
        if (scroll) {
          var top2 = jel.parents('tr').position().top;
          var top1 = $('#playRecord').position().top;
          var dis = top2 - top1;
          $('#playRecordTable').scrollTop(dis - 68);
        };
      } else {
        jel.parents('tr').removeClass('select_back');
      }
    });

    fillCounts();
  }

  function fillCounts() {
    var rcount = 0;
    var nrcount = 0;
    var offcount = 0;
    var notWorkCount = 0;
    for (var i = curRiderList.length - 1; i >= 0; i--) {
      var rider = curRiderList[i];
      if (rider.isSelect == 2) {
        rcount = rider.online ? rcount : rcount + 1;
      };
      if (rider.isSelect == 1) {
        nrcount++;
      };
      if (rider.isSelect == 0) {
        if (rider.workStatus == 0) {
          notWorkCount++;
        } else if (!rider.online) {
          offcount++;
        }
      };
    };

    $('#recommend_count').text('含' + rcount + '个掉线');
    if (rcount == 0) {
      $('#recommend_count').text('');
    };
    $('#not_recommend_count').text('共' + nrcount);
    $('#no_score_count').text(offcount + '个掉线  ' + notWorkCount + '个离岗');
    // if (offcount == 0) {
    //   $('#no_score_count').text('');
    // };
  }

  function selectKeyValueTr(riderId) {

    $('#key-value-table tr').removeClass('select_back');
    $('#key-value-table tr[value="' + riderId + '"]').addClass('select_back');
  }

  function fillKeyValueTable(riderList) {

    $('#key-value-table').empty();

    var ch = $('#playRecord .choose');
    var index = getSelectIndex();
    if (index >= back_dispatchDetail.length) {
      return;
    };
    var des = back_dispatchDetail[index].dispatchTypeDes;
    $('#key-value-title').text('[' + (index + 1) + '/' + back_dispatchDetail.length + ']' + des + 'key_value');

    if (riderList.length < 1) {
      return;
    };

    var head = '<thead><tr><th>骑手姓名</th>';
    for (key in keyList) {

      var name = keyList[key];
      head = head + '<th>' + name + '</th>';
    }
    head += '</tr></thead>';

    var body = '';

    for (var i = 0; i < riderList.length; i++) {
      var rider = riderList[i];

      body = body + '<tr' + ' value="' + rider.riderId + '"' + '>';
      body = body + '<td>' + rider.riderName + '</td>';
      for (key in keyList) {

        var showScore = rider[key] < -10000?'-':rider[key];
        body = body + '<td>' + showScore + '</td>';
      }
      body = body + '</tr>';
    };

    $('#key-value-table').append(head + body);
  }

  return {
    init: function(waybillId, seq) {

      if (!this.inited) {
        initMap();
        bindEvents();
        this.inited = true;
      };

      if (waybillId) {
        $('#waybillId_input').val(waybillId);
        btnSearch();
        if (seq) {
          back_seq = seq;
        }
      } else {
        back_seq = 0;
        var urlid = getId();
        if (urlid) {
          $('#waybillId_input').val(urlid);
          btnSearch();
        };
      }
    },

    _showWaybillDetail: function(waybillId) {

      showWaybillDetail(waybillId);
    },
  };
});