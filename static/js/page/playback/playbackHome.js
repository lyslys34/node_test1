require.config({
  baseUrl: MT.STATIC_ROOT + '/js',
  urlArgs: 'ver=' + pageVersion,
  shim: {
    'lib/dataTables.scroller': {
      deps: ['lib/jquery.dataTables']
    },
  }
});


require(['module/root', 'module/cookie', 'page/playback/playbackMap', 'page/playback/badcase', 'module/validator', 'lib/datepicker', 'lib/moment-with-locales.min',
    'lib/jquery.dataTables', 'lib/dataTables.scroller'
  ],
  function(r, cookie, OrderPage, Badcase, validator, datepicker, moment) {
    document.title = '调度回放';

    var deliveryAreaList = {};
    var allOrgList = {};
    var selDay = $('#selectDay');
    var selCity = $('#selectCity');
    var selArea = $('#selectArea');
    var selOrg = $('#selectOrg');
    var mySelect2 = $.fn.select2;
    var cityListGot = false;
    var cityListRequest;

    var rider_list = $('#rider_selectrider');

    $(".content-head .nav li").click(function(event) {

      var sel = $(this).attr('toid');
      if (!$(this).hasClass('active')) {

        if (sel == "#summary_content") {
          Summary.init();
        } else if (sel == "#rider_content") {
          RiderPage.init();
        } else if (sel == "#order_content") {
          initOrderPage();
        } else if (sel == '#badcase_content') {
          Badcase.init();
        }
      };

    });

    $('#reload-base').click(function(event) {
      getLoginInfo();
      $('#base-info').text('加载城市列表中...');
      $(this).addClass('hidden');
    });

    function getFilterString() {

      if ($('#main-filters').hasClass('hidden')) {
        return null;
      };
      var allFilter = selDay.val() + '-' + selCity.val() + '-' + selArea.val() + '-' + selOrg.val();
      return allFilter;
    }

    function getCurTabId() {
      var btn = $('.content-head .nav li.active');
      return btn.attr('toid');
    }

    function getSelid(ele) {
      var cityid;
      var selector = ele + " option:selected"
      $(selector).each(function() {
        cityid = $(this).val();
      });
      return cityid;
    }

    function getFromTime() {
      var selDayBefore = $('#selectDay').val();
      var fromTime = 0;
      fromTime = r.utils.getDaystartUtime(selDayBefore, true) / 1000;
      fromTime = fromTime.toFixed(0);
      return fromTime;
    }

    function getToTime() {
      var selDayBefore = $('#selectDay').val();
      var toTime = 0;
      toTime = r.utils.getDaystartUtime(selDayBefore, false) / 1000;
      toTime = toTime.toFixed(0);
      return toTime;
    }

    function getFromTotime() {
      var data = {};
      data.fromTime = getFromTime();
      data.toTime = getToTime();
      return data;
    }

    var Summary = {

      init: function() {

        showTab('#summary_content');
        sessionStorage.setItem('playback_tab', 1);
        if (!Summary.inited) {
          Summary.inited = true;
          Summary.bindEvents();
          Summary.setDataTable();
        };
        Summary.reloadDataIfNeeded();
      },
      bindEvents: function() {

        $('#dataTableArea').on('click', '.rider_name', function(event) {
          event.preventDefault();
          var phone = $(this).attr('value');
          RiderPage.init(phone);
        });
      },
      setDataTable: function() {
        // dataTable
        $.extend($.fn.dataTableExt.oSort, { // datatables中文排序
          "chinese-string-asc": function(s1, s2) {
            return s1.localeCompare(s2);
          },
          "chinese-string-desc": function(s1, s2) {
            return s2.localeCompare(s1);
          }
        });
        // extend dataTable default settings
        $.extend($.fn.dataTable.defaults, {
          scrollX: false,
          scrollY: 400,
          paging: false, // 分页
          orderClasses: true,
          deferRender: true,
          order: [], // 默认不排序
          iDisplayLength: 200, // 默认显示20条
          lengthMenu: [
            [20, 50, -1],
            [20, 50, "全部"]
          ], // 规定表格每页显示多少条
          language: {
            sEmptyTable: '没有数据',
            search: '搜索：',
            searchPlaceholder: '输入关键词',
            lengthMenu: '每页显示 _MENU_ 条',
            infoFiltered: "",
            paginate: {
              next: '下一页',
              previous: '上一页'
            }
          },
          columnDefs: [ // 按照中文字符排序
            {
              type: 'chinese-string',
              targets: 0
            }
          ]
        });
      },
      reloadDataIfNeeded: function() {

        var filterStr = getFilterString();
        if (!filterStr) {
          return;
        }

        // if (!Summary.filterStr || Summary.filterStr != filterStr) {
        // Summary.filterStr = filterStr;
        Summary.loadSummary();
        // }
      },
      loadSummary: function() {
        $('#loading_summary_table').removeClass('hidden');
        $('#dataTableArea').addClass('hidden');
        var orgId = getSelid('#selectOrg');
        var orgList = new Array();
        if (orgId == undefined) {
          return;
        };
        if (orgId != 0) {
          orgList.push(orgId);
        } else {
          orgList = getOrgIdList();
        }
        if (orgList.length < 1) {
          return;
        };
        var time = getFromTotime();
        $.ajax({
            url: '/playback/allRiderPushRecord',
            type: 'POST',
            dataType: 'json',
            data: {
              fromTime: time.fromTime,
              toTime: time.toTime,
              orgList: orgList
            },
            traditional: true,
          })
          .done(function(result) {

            $('#loading_summary_table').addClass('hidden');
            $('#dataTableArea').removeClass('hidden');
            Summary.fillSummaryTable(result);

          });
      },
      fillSummaryTable: function(result) {
        $table = $('#dataTableArea');
        $table.empty();
        var body = '<table><thead><tr>' + '<th class="text-center">骑手</th>' + '<th class="text-center">站点</th>' 
                  + '<th class="text-center">开工时长(分钟)</th>' + '<th class="text-center">忙碌时长(分钟)</th>'
                  + '<th class="text-center">推送记录</th>' + '<th class="text-center">已读记录</th>' + '<th class="text-center">已接单记录</th>' + '<th class="text-center">已完成记录</th>' + '</tr></thead><tbody>';

        for (var i = 0; i < result.length; i++) {

          var summ = result[i];
          body = body + '<tr>';

          body = body + '<td style="text-align:center">';
          body = body + '<span class="rider_name" title="查看详细调度记录" value="' + summ.riderPhone + '">' + summ.riderName + '</span>';
          body = body + '</td>'

          body = body + '<td style="text-align:center">';
          body = body + summ.orgName;
          body = body + '</td>'

          var busy = (summ.onJobTime == undefined || summ.onJobTime == -1)?'暂未统计':summ.onJobTime;
          body = body + '<td>';
          body = body + busy;
          body = body + '</td>';

          busy = (summ.busyTime == undefined || summ.busyTime == -1)?'暂未统计':summ.busyTime;
          body = body + '<td>';
          body = body + busy;
          body = body + '</td>';

          body = body + '<td>';
          body = body + summ.pushNum;
          body = body + '</td>';

          body = body + '<td>';
          body = body + summ.checkNum;
          body = body + '</td>';

          body = body + '<td>';
          body = body + summ.grabWaybillNum;
          body = body + '</td>';

          body = body + '<td>';
          body = body + summ.finishedWaybillNum;
          body = body + '</td>';

          body = body + '</tr>';
        };


        body = body + '</tbody></table>';
        $table.append(body);
        var $dataTable = $table.find('table').dataTable({

          paging: false,
          searching: false,
          "autoWidth": true,
          "order": [
            [4, "desc"]
          ],

        });
        $dataTable.api().columns.adjust().draw(false);
      },
    };

    var Badcase = {
      init: function() {
        showTab('#badcase_content');
        sessionStorage.setItem('playback_tab', 4);
        if (!Badcase.inited) {
          Badcase.bindEvents();
          Badcase.field = 'waybill_ctime';
          Badcase.updown = -1;
        };
        Badcase.inited = true;
        Badcase.reloadDataIfNeeded();
      },
      reloadDataIfNeeded: function() {
        var filterStr = getFilterString();
        if (!filterStr) {
          return;
        }
        Badcase.loadBadcase();
      },
      loadBadcase: function(pageNum) {

        if (Badcase.loadBadcaseReq) {
          Badcase.loadBadcaseReq.abort();

        };
        $('#badcase_footer').empty();
        $('#badcase_body').empty();

        var orgId = getSelid('#selectOrg');
        var orgList = new Array();
        if (orgId == undefined) {
          return;
        };
        if (orgId != 0) {
          orgList.push(orgId);
        } else {
          orgList = getOrgIdList();
        }
        if (orgList.length < 1) {
          return;
        };
        var data = {};
        var time = getFromTotime();
        data.fromTime = time.fromTime;
        data.toTime = time.toTime;
        data.pageNum = pageNum == undefined ? 1 : pageNum;
        data.pageSize = 15;
        data.waybillType = $('#waybill_type_select').val();
        data.badcaseType = $('#bad_type_select').val();
        data.orgList = orgList;
        if (Badcase.field) {
          data.inputOrderField = Badcase.field;
          data.inputOrderSeq = Badcase.updown;
        };

        Badcase.loadBadcaseReq = $.ajax({
            url: '/playback/badCaseListByAreaAndSearchParam',
            type: 'POST',
            dataType: 'json',
            data: data,
            traditional: true,
          })
          .done(function(result) {
            if (result.code != 0) {

            } else {
              Badcase.fillBadcase(result.data);
            }
          })
          .fail(function() {})
          .always(function() {})
      },
      fillBadcase: function(result) {

        var list = result.playbackWaybillBadCaseViewViewList;

        // var list = [];
        // var data = {
        //   waybillId: 111,
        //   waybillTypeName: 111,
        //   waybillCtime: 111,
        //   firstRoundDispatchedTime: 111,
        //   grabbedTime: 111,
        //   finishedTime: 111,
        //   riderFeedback: 111,
        //   redispatchReason: 111,
        //   badcaseTypeDesc: 111,
        //   remark: 'sessionStorage002-',
        // };
        // list.push(data);
        var body = '';
        for (var i = 0; i < list.length; i++) {
          var entry = list[i];

          body += '<tr value="' + entry.waybillId + '">';
          body += '<td>' + entry.waybillId + '</td>';
          body += '<td>' + entry.waybillTypeName + '</td>';
          body = body + '<td>' + moment.unix(entry.waybillCtime).format('HH:mm:ss') + '</td>';
          body = body + '<td>' + moment.unix(entry.firstRoundDispatchedTime).format('HH:mm:ss') + '</td>';
          body = body + '<td>' + moment.unix(entry.grabbedTime).format('HH:mm:ss') + '</td>';
          body = body + '<td>' + moment.unix(entry.finishedTime).format('HH:mm:ss') + '</td>';

          var feedBack = entry.riderFeedback ? entry.riderFeedback : '-';
          body += '<td>' + feedBack + '</td>';

          var redispatchReason = entry.redispatchReason ? entry.redispatchReason : '-';
          body += '<td>' + redispatchReason + '</td>';
          body += '<td>' + entry.badcaseTypeDesc + '</td>';
          var remark = entry.remark ? entry.remark : '-';
          var remarkSpan = '<span class="remark">' + remark + '</span>';
          var remarkInput = '<input class="remark_input hidden">';
          body += '<td class="remark_td">' + remarkSpan + remarkInput + '</td>';
          var className = 'success_color';
          body = body + '<td class="' + className + '">'

          + '<span data-toggle="tooltip" title="详情" data-placement="top" name="detail">' + '<i class="fa fa-info-circle fa-lg"></i></span>' + '<span data-toggle="tooltip" title="回放" data-placement="top" name="playback" style="padding-left:10px;">' + '<i class="fa fa-eye fa-lg"></i></span>' + '</td>';
          body += '</tr>';
        };
        $('#badcase_body').append(body);
        $('[data-toggle="tooltip"]').tooltip();
        //设置分页信息
        var page = result;
        if (page.totalPage > 1) {

          var pageHtml = r.ui.generatePagerHtml(page);
          $("#badcase_footer").append(pageHtml);
        }
        var totalText = '<span class="pull-right total_text">共' + page.total + '条记录</span>';
        $("#badcase_footer").append(totalText);
      },
      saveMark: function(waybillId, remark) {
        var data = {
          waybillId: waybillId,
          remark: remark,
        };
        $.ajax({
            url: '/playback/updateBadCaseRemark',
            type: 'POST',
            dataType: 'json',
            data: data,
            traditional: true,
          })
          .done(function(result) {

          })
          .fail(function() {})
          .always(function() {})
      },
      bindEvents: function() {

        $('#badcase_content').on('click', '.remark_td', function(event) {
          event.preventDefault();
          var input = $(this).children('input');
          input.removeClass('hidden');
          input.focus();
          var span = $(this).children('span');
          span.addClass('hidden');
          var text = span.text();
          if (text == '-') {
            text = '';
          };
          input.val(text);
        });

        $('#badcase_content').on('keyup', '.remark_input', function(event) {
          event.preventDefault();
          if (event.keyCode == 13) {
            $(this).blur();
          };
        });
        $('#badcase_content').on('blur', '.remark_input', function(event) {
          event.preventDefault();
          $(this).addClass('hidden');
          var remark = $(this).val();
          $(this).siblings('span').removeClass('hidden');
          if (remark.length < 1) {
            remark = '-';
          };
          $(this).siblings('span').text(remark);
          var waybillId = $(this).parents('tr').attr('value');
          Badcase.saveMark(waybillId, remark);
        });
        $('#bad_type_select').on('change', function(event) {
          event.preventDefault();
          Badcase.loadBadcase();
        });
        $('#waybill_type_select').on('change', function(event) {
          event.preventDefault();
          Badcase.loadBadcase();
        });
        $('#badcase_content .click_sort').on('click', function(event) {
          event.preventDefault();
          $(this).siblings('.click_sort').addClass('sort_none');
          $(this).siblings('.click_sort').removeClass('sort_up');
          $(this).siblings('.click_sort').removeClass('sort_down');

          var field = $(this).attr('value');
          Badcase.field = field;

          if ($(this).hasClass('sort_none') || $(this).hasClass('sort_down')) {
            $(this).removeClass('sort_down');
            $(this).removeClass('sort_none');

            $(this).addClass('sort_up');
            Badcase.updown = 1;
          } else {
            $(this).addClass('sort_down');
            $(this).removeClass('sort_up');
            $(this).removeClass('sort_none');
            Badcase.updown = -1;
          }
          Badcase.loadBadcase();
        });
        //点击页码事件
        $('#badcase_content').on("click", ".pagination li", function() {
          if ($(this).hasClass("disabled") || $(this).hasClass("active")) {
            return false;
          }
          var pageNum = $(this).attr("page");
          Badcase.loadBadcase(pageNum);
        });

        $('#badcase_body').on('click', '.fa-eye', function(event) {
          event.preventDefault();
          if ($(this).hasClass('disabled')) {
            return;
          }
          var par = $(this).parents('tr');
          var arr = par.attr('value');
          var waybillId = '~' + arr;
          initOrderPage(waybillId);
        });

        $('#badcase_body').on('click', '.fa-info-circle', function(event) {
          event.preventDefault();
          if ($(this).hasClass('disabled')) {
            return;
          }
          var par = $(this).parents('tr');
          var arr = par.attr('value');
          OrderPage._showWaybillDetail(arr);
        });
      },
    };

    var RiderPage = {

      init: function(phone) {

        showTab('#rider_content');
        sessionStorage.setItem('playback_tab', 2);
        if (!RiderPage.inited) {

          RiderPage.pushFilter = '';
          RiderPage.checkFilter = '';
          RiderPage.grabFilter = '';

          RiderPage.bindEvents();

          RiderPage.field = 'dispatch_time';
          RiderPage.updown = -1;
        };

        RiderPage.inited = true;

        if (phone) {
          RiderPage.phone = phone;
          rider_list.select2('val', 0);
          $('#rider_input').val(RiderPage.phone);
          RiderPage.loadRiderRecord();
        };
        RiderPage.loadRiderIfNeed();
      },

      bindEvents: function() {
        $('#filter input').unbind('change')
        $('#filter input').on('change', function(event) {
          event.preventDefault();
          var key = $(this).attr('value');
          RiderPage[key] = '';
          if (this.checked) {
            RiderPage[key] = 1;
          };
          RiderPage.loadRiderRecord();
        });

        $('#rider_input').on('change', function(event) {
          event.preventDefault();
          rider_list.select2('val', 0);
          RiderPage.loadRiderRecord();
        });

        $('#rider_record_table .click_sort').unbind('click');
        $('#rider_record_table .click_sort').on('click', function(event) {
          event.preventDefault();
          $(this).siblings('.click_sort').addClass('sort_none');
          $(this).siblings('.click_sort').removeClass('sort_up');
          $(this).siblings('.click_sort').removeClass('sort_down');

          var field = $(this).attr('value');
          RiderPage.field = field;

          if ($(this).hasClass('sort_none') || $(this).hasClass('sort_down')) {
            $(this).removeClass('sort_down');
            $(this).removeClass('sort_none');

            $(this).addClass('sort_up');
            RiderPage.updown = 1;
          } else {
            $(this).addClass('sort_down');
            $(this).removeClass('sort_up');
            $(this).removeClass('sort_none');
            RiderPage.updown = -1;
          }
          RiderPage.loadRiderRecord();
        });

        $('#load_record').on('click', function(event) {
          event.preventDefault();
          RiderPage.loadRiderRecord();
        });

        //点击页码事件
        $('#rider_content').on("click", ".pagination li", function() {
          if ($(this).hasClass("disabled") || $(this).hasClass("active")) {
            return false;
          }
          var pageNum = $(this).attr("page");
          RiderPage.loadRiderRecord(pageNum);
        });

        $('#rider_record_tbody').on('click', '.fa-eye', function(event) {
          event.preventDefault();
          if ($(this).hasClass('disabled')) {
            return;
          }
          var par = $(this).parent().parent();
          var arr = par.attr('value').split(',');
          initOrderPage(arr[0], arr[1]);
        });

        $('#rider_record_tbody').on('click', '.fa-info-circle', function(event) {
          event.preventDefault();
          var par = $(this).parent().parent();
          if (par.hasClass('disabled')) {
            return;
          }
          var arr = par.attr('value').split(',');
          OrderPage._showWaybillDetail(arr[2]);
        });
      },

      loadRiderIfNeed: function() {
        var filterStr = getFilterString();
        if (!filterStr) {
          return;
        }

        if (!RiderPage.filterStr || RiderPage.filterStr != filterStr) {
          RiderPage.filterStr = filterStr;
          var orgId = getSelid('#selectOrg');
          if (orgId != undefined && orgId != 0) {
            RiderPage.loadRiderList(orgId);
          }
        }
      },

      loadRiderList: function(orgId) {

        $.ajax({
            url: '/playback/orgRiderInfo',
            type: 'POST',
            dataType: 'json',
            data: {
              orgId: orgId
            },
          })
          .done(function(result) {

            RiderPage.fillRiderList(result);
          })
          .always(function() {});

      },

      fillRiderList: function(result) {

        var option = '<option selected="selected" value="0">未选择骑手</option>';
        for (var i = result.length - 1; i >= 0; i--) {

          option = option + '<option value="' + result[i].mobile + '">' + result[i].name + '(' + result[i].mobile + ')</option>';
        };
        rider_list.empty();
        rider_list.append(option);
        mySelect2.amd.require(['select2/compat/matcher'], function(oldMatcher) {
          rider_list.select2({
            matcher: oldMatcher(matchPinyin)
          });
        });
        rider_list.unbind('change');
        rider_list.on("change", function(e) {

          if (rider_list.val() != 0) {
            $('#rider_input').val(rider_list.val());
            RiderPage.loadRiderRecord();
          };
        });
      },

      loadRiderRecord: function(pageNum) {

        if (RiderPage.loadRecordReq) {
          RiderPage.loadRecordReq.abort();

        };
        $('#record_footer').empty();
        $('#rider_record_tbody').empty();

        phone = $('#rider_input').val();
        if (!validator.isMobile(phone)) {
          alert('请输入或选择骑手');
          return;
        };

        var data = {};
        var time = getFromTotime();
        data.fromTime = time.fromTime;
        data.toTime = time.toTime;
        data.pageNum = pageNum == undefined ? 1 : pageNum;
        data.pageSize = 15;
        data.riderPhone = phone;
        data.pushFilter = RiderPage.pushFilter;
        data.checkFilter = RiderPage.checkFilter;
        data.grabFilter = RiderPage.grabFilter;

        if (RiderPage.field) {
          data.inputOrderField = RiderPage.field;
          data.inputOrderSeq = RiderPage.updown;
        };

        RiderPage.loadRecordReq = $.ajax({
            url: '/playback/riderPushRecordDetail',
            type: 'POST',
            dataType: 'json',
            data: data,
          })
          .done(function(result) {
            if (result.code != 0) {
              $('#rider_data_info').text(result.msg);
            } else {
              RiderPage.fillRecordTable(result.data);
            }
          })
          .fail(function() {})
          .always(function() {})
      },

      fillRecordTable: function(result) {

        var list = result.playbackRiderPushRecordDetailViewList;
        $('#only_push_count').text('(' + result.pushNum + ')');
        $('#only_check_count').text('(' + result.checkNum + ')');
        $('#only_grab_count').text('(' + result.grabNum + ')');

        var info = '【' + result.riderName + '】当前记录(' + result.total + ')';
        $('#rider_data_info').text(info);
        var curRider = result.riderName;
        var body = '';
        if (!list) {
          return;
        };
        for (var i = 0; i < list.length; i++) {

          var data = list[i];

          body = body + '<tr>';

          body = body + '<td>' + moment.unix(data.dispatchTime).format('HH:mm:ss') + '</td>';

          body = body + '<td>[' + data.dispatchSeq + '/' + data.totalDispatchCount + ']' + data.dispatchTypeDes + '</td>';

          if (data.pushTime == 0) {
            body = body + '<td class="disable_color">' + '未知' + '</td>';
          } else if (data.pushTime == -21) {
            body = body + '<td class="disable_color">' + '未知－Android' + '</td>';
          } else if (data.pushTime == -22) {
            body = body + '<td class="fail_color">' + '推送失败' + '</td>';
          } else if (data.pushTime == -31) {
            body = body + '<td class="disable_color">' + '未知－iOS离线' + '</td>';
          } else if (data.pushTime == -32) {
            body = body + '<td class="fail_color">' + '推送失败' + '</td>';
          } else {
            body = body + '<td>' + moment.unix(data.pushTime).format('HH:mm:ss') + '</td>';
          }

          if (data.checkTime == 0) {
            body = body + '<td class="disable_color">' + '未读' + '</td>';
          } else {
            body = body + '<td>' + moment.unix(data.checkTime).format('HH:mm:ss') + '</td>';
          }

          if (data.grabResultDes == '抢单成功') {
            body = body + '<td class="success_color">' + data.grabResultDes + '</td>';
          } else if (data.grabResultDes == '未参与') {
            body = body + '<td class="disable_color">' + data.grabResultDes + '</td>';
          } else {
            body = body + '<td>' + data.grabResultDes + '</td>';
          }
          body = body + '<td>' + data.senderName + '#' + data.poiSeq + '</td>';

          var className = '';
          if (data.finishRiderName == curRider) {
            className = "success_color";
          };
          if (data.grabTime == 0) {
            body = body + '<td class="' + className + '">' + '-' + '</td>';
          } else {
            body = body + '<td class="' + className + '">' + moment.unix(data.grabTime).format('HH:mm:ss') + '</td>';
          }

          var rname = data.finishRiderName ? data.finishRiderName : '-';
          var orgname = data.orgName ? data.orgName : '-';
          if (rname == '-') {
            body = body + '<td class="' + className + '">' + rname + '</td>';
          } else {
            body = body + '<td class="' + className + '">' + rname + '<span class="disable_color">.' + orgname + '</span>' + '</td>';
          }

          className = 'disabled disable_color';
          if (data.auth == 1) {
            className = 'success_color';
          };
          body = body + '<td class="' + className + '" value="' + data.platformOrderId + ',' + data.dispatchSeq + ',' + data.waybillId + '">'

          + '<span data-toggle="tooltip" title="详情" data-placement="top" name="detail">' + '<i class="fa fa-info-circle fa-lg"></i></span>' + '<span data-toggle="tooltip" title="回放" data-placement="top" name="playback" style="padding-left:10px;">' + '<i class="fa fa-eye fa-lg"></i></span>' + '</td>';
          body = body + '</tr>';
        };

        $('#rider_record_tbody').append(body);
        $('[data-toggle="tooltip"]').tooltip();

        //设置分页信息
        var page = result;
        if (page.totalPage > 1) {

          var pageHtml = r.ui.generatePagerHtml(page);
          $("#record_footer").append(pageHtml);
        }
      },

    };

    function showTab(name) {

      $(name).removeClass('hidden');
      $(name).siblings('div').addClass('hidden');
      var filter = '[toid="' + name + '"]';
      $(filter).siblings().removeClass('active');
      $(filter).addClass('active');
      if (name == '#order_content') {
        $('#main-filters-container').addClass('hidden');
        $('#order-input-field').removeClass('hidden');
      } else {
        $('#main-filters-container').removeClass('hidden');
        $('#order-input-field').addClass('hidden');
        if (!cityListGot) {
          getLoginInfo();
        };
      }
    }

    function initOrderPage(waybillId, seq) {
      showTab('#order_content');
      sessionStorage.setItem('playback_tab', 3);

      OrderPage.init(waybillId, seq);
    }

    function getLoginInfo() {
      var cityId = getStoredCity();
      _getLoginInfo(cityId);
    }

    function _getLoginInfo(cityId) {
      if (!cityId) {
        cityId = 0;
      };
      if (cityListRequest) {
        return;
      };
      cityListRequest = $.ajax({
          url: '/partner/dispatch/homeLoginInfo',
          type: 'GET',
          dataType: 'json',
          data: {
            cityId: cityId
          },
        })
        .done(function(result) {
          if (result.code == 0) {
            $('#main-filters').removeClass('hidden');
            $('#loading-content').addClass('hidden');

            fillCityList(result.data.bmCityList, result.data.selectCityId);
            refreshAreaDta(result.data.bmDeliveryAreaList);
            refreshOrgData(result.data.bmOrgList);

            fillAreaList(true);
          } else {
            $('#reload-base').removeClass('hidden');
            $('#base-info').text(result.msg);
          }
        })
        .fail(function() {
          $('#reload-base').removeClass('hidden');
          $('#base-info').text('加载失败，请重试');
        })
        .always(function() {
          cityListRequest = null
        });
    }

    function getAreaListWithCity(cityId) {
      $.ajax({
          url: '/partner/dispatch/homeLoginInfo',
          type: 'GET',
          dataType: 'json',
          data: {
            cityId: cityId
          },
        })
        .done(function(result) {
          if (result.code == 0) {

            refreshAreaDta(result.data.bmDeliveryAreaList);
            refreshOrgData(result.data.bmOrgList);
            fillAreaList();
          } else {
            alert('获取区域信息失败，请重试');
          }
        })
        .fail(function() {
          refreshAreaDta();
          refreshOrgData();
        })
        .always(function() {});
    }

    function getSelCityid() {

      return getSelid('#selectCity');
    }

    function refreshAreaDta(data) {

      deliveryAreaList = {};
      if (data) {

        for (var i = 0; i < data.length; i++) {
          var deliveryArea = data[i];
          var area = {};
          area.id = deliveryArea.areaId;
          area.cityId = deliveryArea.cityId;
          // area.orgId = deliveryArea.bmOrgId;
          area.name = '-';
          if (deliveryArea.areaName) {
            area.name = deliveryArea.areaName;
          }
          deliveryAreaList[area.id] = area;
        };
      }
    }

    function refreshOrgData(data) {
      allOrgList = {};
      if (data) {
        for (var i = 0; i < data.length; i++) {
          var orgView = data[i];
          var org = {};
          org.orgid = orgView.orgId;
          org.name = orgView.orgName;
          org.dispatchStrategyDes = orgView.dispatchStrategyDes;
          org.areaId = orgView.areaId;

          allOrgList[orgView.orgId] = org;
        };
      }
    }

    function fillCityList(data, selCityId) {
      cityListGot = true;
      var body = '';
      for (var i = 0; i < data.length; i++) {
        var city = data[i];
        if (selCityId == city.city_id) {
          body += '<option selected="selected" value="' + city.city_id + '">' + city.name + '</option>';
        } else {
          body += '<option value="' + city.city_id + '">' + city.name + '</option>';
        }
      };
      selCity.append(body);
      $.fn.select2.amd.require(['select2/compat/matcher'], function(oldMatcher) {
        selCity.select2({
          matcher: oldMatcher(matchPinyin)
        });
      });
      selCity.select2();

      // 城市的下拉框
      selCity.on("change", function(e) {
        var cityId = getSelCityid();
        storeCityId(cityId);
        getAreaListWithCity(cityId);
      });
      var cityId = getSelCityid();
      storeCityId(cityId);
    }

    function fillAreaList(useArg) {
      $('#selectArea').empty();
      var options = '';
      var areaArg = r.utils.urlArg('areaId');
      if (!areaArg && useArg) {
        areaArg = getStoredArea();
      };
      var selected = '';
      var showUrlArg = false;
      for (areaId in deliveryAreaList) {

        if (useArg && areaArg && (areaArg == areaId)) {
          selected = ' selected="selected"';
          showUrlArg = true;
        };
        var area = deliveryAreaList[areaId];
        options = options + '<option' + selected + ' value=' + area.id;
        options = options + '>' + area.name;
        options = options + '</option>';
        selected = '';
      }

      selArea.unbind('change');
      selArea.append(options);
      $.fn.select2.amd.require(['select2/compat/matcher'], function(oldMatcher) {
        selArea.select2({
          matcher: oldMatcher(matchPinyin)
        });
      });
      selArea.select2();

      selArea.on("change", function(e) {
        fillOrgList();
        storeAreaId();
      });
      storeAreaId();
      fillOrgList(useArg);
    }

    function fillOrgList(useArg) {
      $('#selectOrg').empty();
      var options = '';
      var orgArg = r.utils.urlArg('orgId');
      if (useArg && !orgArg) {
        orgArg = getStoredOrg();
      };
      var selected = '';
      var showUrlArg = false;
      var count = 0;
      for (orgid in allOrgList) {

        var org = allOrgList[orgid];
        if (areaIdInSel(org.areaId)) {

          if (useArg && orgArg && (orgArg == orgid)) {
            selected = ' selected="selected"';
            showUrlArg = true;
          };
          options = options + '<option' + selected + ' value=' + org.orgid;
          options = options + '>' + org.name + '[' + org.dispatchStrategyDes + ']';
          options = options + '</option>';
          selected = '';
          count++;
        };
      }
      if (count > 1) {
        if (showUrlArg) {
          options = '<option value="0">全部站点</option>' + options;
        } else {
          options = '<option value="0" selected="selected">全部站点</option>' + options;
        }
      };
      selOrg.unbind('change');
      selOrg.append(options);
      $.fn.select2.amd.require(['select2/compat/matcher'], function(oldMatcher) {
        selOrg.select2({
          matcher: oldMatcher(matchPinyin)
        });
      });
      selOrg.select2();

      selOrg.on("change", function(e) {
        loadSelTabData();
        storeOrgId();
      });
      storeOrgId();
      loadSelTabData();
    }

    function getOrgIdList() {
      // var orgIdList = Object.keys(allOrgList);
      var orgIdList = [];
      $('#selectOrg option').each(function(index, el) {
        
        if ($(el).attr('value') != 0) {
          orgIdList.push($(el).attr('value'));  
        };
      });
      return orgIdList;
    }

    function getSelAreas() {
      var selAreas = new Array();
      var selId = selArea.val();
      if (selId == 0) {
        selAreas = Object.keys(deliveryAreaList);
      } else {
        selAreas.push(selId);
      }
      return selAreas;
    }

    function areaIdInSel(areaId) {
      var selAreas = getSelAreas();
      var has = false;
      for (var i = 0; i < selAreas.length; i++) {
        if (selAreas[i] == areaId) {
          has = true;
          break;
        };
      };
      return has;
    }

    function loadSelTabData() {
      var selTab = getCurTabId();
      switch (selTab) {
        case '#summary_content':
          Summary.init();
          break;
        case '#rider_content':
          RiderPage.init();
          break;
        case '#badcase_content':
          Badcase.init();
          break;
        default:
      }
    }

    function initDateSelect() {
      var selectHtml = "";
      for (var i = 0; i < 7; i++) {
        var option = fnDateFormat(i);
        if (i == 0) {
          selectHtml += '<option selected="selected" value="' + i + '">' + option + '</option>';
        } else {
          selectHtml += '<option value="' + i + '">' + option + '</option>';
        }
      };
      $("#selectDay").html(selectHtml);
      $("#selectDay").select2();
      $("#selectDay").on("change", function(e) {
        loadSelTabData();
      });
    }

    function fnDateFormat(days) {
      var myDate = new Date(new Date().getTime() - 1000 * 60 * 60 * 24 * days),
        myMonth = myDate.getMonth() + 1,
        myDay = myDate.getDate(),
        myWeek = myDate.getDay();
      if (myMonth < 10) {
        myMonth = "0" + myMonth;
      };
      if (myDay < 10) {
        myDay = "0" + myDay;
      };
      var dateObj = "";

      switch (days) {
        case 0:
          dateObj = myMonth + "-" + myDay + "(今天)";
          break;
        case 1:
          dateObj = myMonth + "-" + myDay + "(昨天)";
          break;
        case 2:
          dateObj = myMonth + "-" + myDay + "(前天)";
          break;
        default:
          dateObj = myMonth + "-" + myDay + fnWeekDeal(myWeek);
      }
      return dateObj;
    }

    function fnWeekDeal(week) {
      // 0 1 2 3 4 5 6 
      //天 1 2 3 4 5 6
      var weekStr = "";
      switch (week) {
        case 0:
          weekStr = "日";
          break;
        case 1:
          weekStr = "一";
          break;
        case 2:
          weekStr = "二";
          break;
        case 3:
          weekStr = "三";
          break;
        case 4:
          weekStr = "四";
          break;
        case 5:
          weekStr = "五";
          break;
        case 6:
          weekStr = "六";
          break;
      }
      return "(周" + weekStr + ")";
    }

    function storeOrgId() {
      var userId = cookie.getCookie("userId");
      var key = userId + 'playback_selectedOrg';

      var op = getSelid('#selectOrg');
      localStorage.setItem(key, op);
    }

    function getStoredOrg() {
      var orgId = 0;
      var userId = cookie.getCookie("userId");
      var storedOrg = localStorage.getItem(userId + 'playback_selectedOrg');
      if (storedOrg) {
        orgId = storedOrg;
      };
      return orgId;
    }

    function storeAreaId() {
      var userId = cookie.getCookie("userId");
      var key = userId + 'playback_selectedArea';

      var op = getSelid('#selectArea');
      localStorage.setItem(key, op);
    }

    function getStoredArea() {
      var areaId = 0;
      var userId = cookie.getCookie("userId");
      var storedArea = localStorage.getItem(userId + 'playback_selectedArea');
      if (storedArea) {
        areaId = storedArea;
      };
      return areaId;
    }

    function getStoredCity() {
      var cityId = 0;
      var argCity = r.utils.urlArg('cityId');
      if (argCity) {

        cityId = argCity;
      } else {
        var userId = cookie.getCookie("userId");
        var storedCity = localStorage.getItem(userId + 'playback_selectedCity');
        if (storedCity && storedCity != 'undefined') {
          cityId = storedCity;
        };
      }

      return cityId;
    }

    function storeCityId(cityId) {
      var userId = cookie.getCookie("userId");
      localStorage.setItem(userId + 'playback_selectedCity', cityId);
    }

    function init() {

      $('.j-toggle-menu').click();
      initDateSelect();
      $('#rider_selectrider').select2();
      var tab = r.utils.urlArg('tab');
      if (!tab) {
        tab = sessionStorage.getItem('playback_tab');
      }
      if (tab == 2) {
        var phone = r.utils.urlArg('phone');
        if (phone && validator.isMobile(phone)) {
          RiderPage.init(phone);
        } else {
          RiderPage.init();
        }
        getLoginInfo();

      } else if (tab == 3) {
        initOrderPage();
      } else if (tab == 4) {
        if (isA) {
          Badcase.init();
        } else {
          getLoginInfo();
        }
      } else {
        getLoginInfo();
      }
    }

    init();

  });