
require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  },
});

require(['module/root', 'lib/moment-with-locales.min'], function (r, moment) {
    document.title = '调度回放';

    //一些变量

    var waybillList;      //key = 'waybillId-stage'
    var riderMarkers;
    var startMarker;
    var endMarker;
    var curWaybillKey;
    var curRiderList;
    var detailDia;


    //初始化地图对象，加载地图
    map = new AMap.Map("mapContainer", {
        resizeEnable: true,
        view: new AMap.View2D({
         resizeEnable: true,
                zoom:14//地图显示的缩放级别
              }),
        keyboardEnable:false
    });

    map.plugin(["AMap.Scale"], function(){
        scale = new AMap.Scale();
        map.addControl(scale);
    });

    //在地图中添加ToolBar插件
    map.plugin(["AMap.ToolBar"],function(){
        toolBar = new AMap.ToolBar();
    //      toolBar.setOffset(new AMap.Pixel(10,50));
    map.addControl(toolBar);
  });
    // 地图中添加全屏自定义插件
    AMap.fullScreen = function() {};
    AMap.fullScreen.prototype = {
      addTo: function(map, dom) {
        dom.appendChild(this._getHtmlDom(map));
      },
      _getHtmlDom: function(map) {
        this.map = map;
        var controlUI = document.createElement('div');
        controlUI.className = 'full_screen_btn'
        controlUI.innerHTML = '全屏';

        controlUI.onclick = function() {
          var oMap = document.getElementById('mapContainer');
          if (oMap.classList.contains('full')) {
            oMap.classList.remove('full');
            this.innerHTML = '全屏';
          }else {
            oMap.classList.add('full')
            this.innerHTML = '退出';
          }
        }
        return controlUI;
      }
    }
    var fullScreenControl = new AMap.fullScreen(map);
    map.addControl(fullScreenControl);

    function btnSearch(){
      var waybillId = new String($('#waybillId_input').val());
      waybillId = waybillId.trim();

      if (waybillId.length > 0) {
        clearMap();
        getWaybillInfo(waybillId);
        //getRiderList($('#waybill_infobody tr td[name="detail"]').val());
      }
    }
    $('#btn-search').click(btnSearch);


    function getWaybillInfo(waybillId){

      loadingWaybillInfo();
      $.ajax({
        url: '/playback/waybillInfo',
        type: 'GET',
        dataType: 'json',
        data: {platformOrderId: waybillId},
      })
      .done(function(result) {

        console.log("getWaybillInfo success");
        fillWaybillInfo(result);
      })
      .fail(function(err) {
          console.log("error" + err.msg);
          $('#loading_info').text('请求出错 请重试');
      })
      .always(function() {

        $('#btn-search').blur();
      });

    }

    function loadingWaybillInfo()
    {
      $('#waybill_infobody').empty();
      $('#loading_info').removeClass('hidden');
      $('#loading_info').text('加载中');
    }


    function getTime(timestamp){
      var date = new Date(timestamp);
      var str = getFormatedString(date.getMonth()+1) + '-' + getFormatedString(date.getDate()) + ' ' + getFormatedString(date.getHours()) + ':' +getFormatedString(date.getMinutes()) + ':' + getFormatedString(date.getSeconds());
      return str;
    }


    function getFormatedString(value){
      if(value > 9){
        return new String(value);
      }
      return '0' + new String(value);
    }

    function getWaybillKey(waybill)
    {
      return waybill.waybillId + '-' + waybill.dispatchStage;
    }

    function fillWaybillInfo(result){

      if (result.dispatchTpye == 2) {

        $('#loading_info').text('没有可回放记录');
        return;
      };

      waybillList = new Array();
      var html = '';

      $('#loading_info').addClass('hidden');
      $('#waybill_infobody').empty();



        var len = result.playbackWaybillViewList.length;
        var waybill = result.playbackWaybillViewList[len-1];
        var maxKey = getWaybillKey(waybill);


        $.each(result.playbackWaybillViewList, function(index,waybill){
              var key = getWaybillKey(waybill);

             waybillList[key] = waybill;

        })




        var orderTime = getTime(waybill.orderTime * 1000);
        var riderName = '';
        if(!waybill.riderName){
          riderName = '-';
        }
        else{
          riderName = waybill.riderName;
        }


        html = html + '<tr>';
        html = html + '<td>' +  getStatusString(waybill.status)  + '</td>';
        html = html + '<td>' +  waybill.senderName + '#' + waybill.poiSeq + '</td>';
        html = html + '<td>' + waybill.senderAddress + '</td>';
        html = html + '<td>' + waybill.recipientAddress + '</td>';
        html = html + '<td>' + waybill.orgName +'</td>';
        html = html + '<td>' + riderName + '</td>';
        html = html + '<td>' + orderTime +  '</td>';
        html = html + '<td style="cursor:pointer"><span data-toggle="tooltip" title="详情" data-placement = "top" name = "detail" value="'+ maxKey +'">'
                    + '<i class="fa fa-info-circle fa-lg opration-icon"></i></span></td>';
        html = html + '</tr>';


      $('#waybill_infobody').append(html);
      $('[data-toggle="tooltip"]').tooltip();
         clearMap();
         getRiderList(maxKey);
    }

    function getStatusString(status){
        var str = '';
        switch(status){
          case 0:
            str = '未调度';
            break;
          case 10:
            str = '未接单';
            break;
          case 15:
            str = '派单后未确认';
            break;
          case 20:
            str = '已接单';
            break;
          case 30:
            str = '已取货';
            break;
          case 50:
            str = '已送达';
            break;
          case 99:
            str = '已取消';
            break;
        }
        return str;
    }

   $('#waybill_infobody').delegate('[name=detail]', 'click', function(event) {

            if ( event && event.stopPropagation ){
                event.stopPropagation();
            }else{
                window.event.cancelBubble = true;
            }

            var orderid = $(this).attr('value').split('-')[0];
            showWaybillDetail(orderid);
        });

    $('#waybill_infobody').delegate('[name=detail]', 'dblclick', function(event) {

        if ( event && event.stopPropagation ){
            event.stopPropagation();
        }else{
            window.event.cancelBubble = true;
        }
    });

    var detailRequest;
    function showWaybillDetail(waybillId)
    {

        if (detailDia) {
            detailDia.remove();
            detailDia = null;
        };
        if (detailRequest) {
          detailRequest.abort();
        };
        var body = '<div id="detailCon">这是ID＝' + waybillId+ '的订单详情</div>';
        detailRequest = $.ajax({
             url: "/partner/waybill/detailV2.ajax?waybillId="+ waybillId,
             success: function(result){

                body = '<div>';
                body = body + result + '</div>';
                detailDia = r.ui.showModalDialog({
                    title: '订单详情', body: body, buttons: [],
                    style:{
                        ".modal-footer":{"display":"none"},
                        ".modal-title":{"display":"none"},
                        ".modal-dialog":{"width":"900px"},
                        ".modal-body":{"padding-top":"0"},
                        ".modal-header .close":{"margin-right": "-8px","margin-top": "-10px"},
                        ".modal-header":{"border-bottom": "none"}
                        }
                });
                detailDia.on("hidden.bs.modal", function(){
                    this.remove();
                    detailDia = null;
                });
            },
            error:function(){
            }});
    }



    function getTimeString(time)
    {
      var text = moment.unix(time).locale('zh-cn').format('YYYY-MM-DD HH:mm:ss');
      return text;
    }

    function clearMap()
    {
      map.clearMap();
    }

    function fillMap(waybill, riderList)
    {
      //dd start and end

      curWaybillKey = getWaybillKey(waybill);


      var scale = 1000000;
      var lng = (waybill.senderLng + waybill.recipientLng) / 2;

      lng = lng / scale;
      var lat = (waybill.senderLat + waybill.recipientLat) / 2;
      lat = lat / scale;

      map.setZoomAndCenter(14, new AMap.LngLat(lng, lat));

      startMarker = new AMap.Marker({
        icon:"/static/imgs/getGoods.png",
        position:new AMap.LngLat(waybill.senderLng/scale, waybill.senderLat/scale),
        offset: new AMap.Pixel(-16,-41), //相对于基点的偏移位置

      });
      startMarker.setMap(map);

      endMarker = new AMap.Marker({
        icon:"/static/imgs/sendGoods.png",
        position:new AMap.LngLat(waybill.recipientLng/scale, waybill.recipientLat/scale),
        offset: new AMap.Pixel(-16,-32), //相对于基点的偏移位置
      });

      endMarker.setMap(map);

      //add riders
      $.each(riderList, function(index, rider) {

        addRiderMarker(rider,waybill);
      });
        map.setFitView();
    }

    function addRiderMarker(rider,waybill)
    {
      var lng = rider.riderLng;
      var lat = rider.riderLat;
      if (lng == 0 || lat == 0) {

        return;
      };

      lng = lng / 1000000;
      lat = lat / 1000000;
      // console.log('rider lng = ' + lng + '---lat = ' + lat);


      var imgSrc = "/static/imgs/rider-noScore1.png";
      if (rider.isSelect == 1) {
        imgSrc = "/static/imgs/rider-notRecommend1.png";
      }else if(rider.isSelect == 2){
        imgSrc = "/static/imgs/rider-recommend1.png";

      }
        var riderStr = rider.riderName+'['+rider.riderWaybillIdsSize+'单]';
      //自定义点标记内容
      var markerContent = document.createElement("div");
      markerContent.className = "markerContentStyle";

      //点标记中的图标
      var markerImg = document.createElement("img");
      markerImg.className = "markerlnglat";
      markerImg.src = imgSrc;
      markerContent.appendChild(markerImg);

      //点标记中的文本
      var markerSpan = document.createElement("span");
      markerSpan.innerHTML = riderStr;
      markerSpan.className = "markerName";
      markerSpan.style.width = riderStr.length*11 + 'px';

      markerContent.appendChild(markerSpan);

      var marker = new AMap.Marker({
        map:map,
        position: new AMap.LngLat(lng,lat), //基点位置
        offset: new AMap.Pixel(-16,-41), //相对于基点的偏移位置
        draggable: false,  //是否可拖动
        content: markerContent   //自定义点标记覆盖物内容
      });

      marker.title = rider.riderName;
      marker.id = rider.riderId;
      marker.isSelect = rider.isSelect;

      marker.setMap(map);  //在地图上添加点

      AMap.event.addListener(marker,'click',function(){



        if (marker == startMarker || marker == endMarker) {

          return;
        };
        var rider = riderForId(marker.id);
        if (rider && rider.isSelect == 0) {

          var infoWindow = new AMap.InfoWindow({
            isCustom:true,  //使用自定义窗体
            content:createInfoWindow2(marker, getNoscoreReason(rider)),
            offset:new AMap.Pixel(10, -85)//-113, -140
          });
          infoWindow.open(map,marker.getPosition());

          return;
        };
        getRiderWaybills(marker);
      });

        var polyline,polyline1,polyline2,polyline3;
        var arr1=new Array();
        var arr2 = new Array();
        var marker1 = new Array();
        var marker2 =  new Array();

        AMap.event.addListener(marker,'mouseover',function(){



            if(marker == startMarker || marker == endMarker || rider.isSelect == 0){
                return;
            }
            var senderX = waybill.senderLng/1000000;
            var senderY = waybill.senderLat/1000000;
            var recipientX = waybill.recipientLng/1000000;
            var recipientY = waybill.recipientLat/1000000;
            var riderX = lng;
            var riderY = lat;


            var startXY = new AMap.LngLat(senderX,senderY);
            var endXY = new AMap.LngLat(recipientX,recipientY);
            var riderXY = new AMap.LngLat(riderX,riderY);

            var drawpath = new Array();
            drawpath.push(startXY);
            drawpath.push(endXY);
            var drawpath1 = new Array();
            drawpath1.push(startXY);
            drawpath1.push(riderXY);

            polyline = new AMap.Polyline({
                map: map,
                path: drawpath,
                strokeColor: "#0000FF",
                strokeOpacity: 0.7,
                strokeStyle: "dashed",
                strokeWeight: 4,
                strokeDasharray: [10, 5]
            });

            polyline1 = new AMap.Polyline({
                map: map,
                path: drawpath1,
                strokeColor: "#0000FF",
                strokeOpacity: 0.7,
                strokeStyle: "dashed",
                strokeWeight: 4,
                strokeDasharray: [10, 5]
            });
            var len = rider.waybillCoordinateListSize;
            for(var i=0;i<len;i++){
              var getX = rider.waybillCoordinateList[i].senderLng/1000000;
              var getY = rider.waybillCoordinateList[i].senderLat/1000000;
              var sendX = rider.waybillCoordinateList[i].recipientLng/1000000;
              var sendY = rider.waybillCoordinateList[i].recipientLat/1000000;

              var getXY = new AMap.LngLat(getX,getY);
              var sendXY = new AMap.LngLat(sendX,sendY);

              if(rider.waybillCoordinateList[i].status == 20){
                    var drawpath2 = new Array();
                    drawpath2.push(getXY);
                    drawpath2.push(riderXY);
                    polyline2 = new AMap.Polyline({
                          map: map,
                          path: drawpath2,
                          strokeColor: "#FF0000",
                          strokeOpacity: 0.7,
                          strokeStyle: "dashed",
                          strokeWeight: 4,
                          strokeDasharray: [10, 5]
                  });
                    arr1.push(polyline2);

                    getMarker = new AMap.Marker({
                      icon:"/static/imgs/get.png",
                      position:new AMap.LngLat(getX, getY),
                      offset: new AMap.Pixel(-16,-41), //相对于基点的偏移位置

                    });
                    marker1.push(getMarker);
                    getMarker.setMap(map);

              }
              else if(rider.waybillCoordinateList[i].status == 30){
                    var drawpath3 = new Array();
                    drawpath3.push(sendXY);
                    drawpath3.push(riderXY);
                    polyline3 = new AMap.Polyline({
                          map: map,
                          path: drawpath3,
                          strokeColor: "#FF0000",
                          strokeOpacity: 0.7,
                          strokeStyle: "dashed",
                          strokeWeight: 4,
                          strokeDasharray: [10, 5]
                  });
                    arr2.push(polyline3);
                   sendMarker = new AMap.Marker({
                      icon:"/static/imgs/send.png",
                      position:new AMap.LngLat(sendX, sendY),
                      offset: new AMap.Pixel(-16,-41), //相对于基点的偏移位置

                    });
                   marker2.push(sendMarker);
                    sendMarker.setMap(map);
              }
            }
                //map.setFitView();


        })
        AMap.event.addListener(marker,'mouseout',function(){
            if(polyline)polyline.setMap(null);
            if(polyline1)polyline1.setMap(null);
            for(var i=0;i<arr1.length;i++){
              if(arr1[i])arr1[i].setMap(null);
            }
            for(var i=0;i<arr2.length;i++){
              if(arr2[i])arr2[i].setMap(null);
            }
            for(var i=0;i<marker1.length;i++){
              if(marker1[i])marker1[i].setMap(null);
            }
            for(var i=0;i<marker2.length;i++){
              if(marker2[i])marker2[i].setMap(null);
            }


        })
    }

    function getNoscoreReason(rider)
    {
      var reason = '';
      if (rider.workStatus == 0) {

        reason = '骑手处于离岗状态';
      }else if(rider.workStatus == 1){

        if (rider.riderWaybillIdsSize >= 12){
          reason = '骑手单数过多';
        }else{
          reason = '超时未上报状态';
        }

      }else{

        reason = '骑手处于忙碌状态';
      }

      return reason;
    }

    function riderForId(riderid)
    {
      var ret;
      $.each(curRiderList, function(index, rider) {

        if(rider.riderId == riderid){

          ret = rider;
        }
      });

      return ret;
    }

    function getRiderWaybills(marker)
    {
      stage = waybillList[curWaybillKey].dispatchStage;
      $.ajax({
        url: '/playback/riderWaybillList',
        type: 'POST',
        dataType: 'json',
        data: {waybillId: waybillList[curWaybillKey].waybillId, dispatchStage: stage, riderId: marker.id},
      })
      .done(function(result) {



            var infoWindow = new AMap.InfoWindow({
                isCustom:true,  //使用自定义窗体
                content:createInfoWindow(marker, result),
                offset:new AMap.Pixel(10, -85)//-113, -140
            });





       infoWindow.open(map,marker.getPosition());

      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {

      });

    }

    function createInfoWindow2(title, reason){
      var info = document.createElement("div");
      info.className = "info";

      //可以通过下面的方式修改自定义窗体的宽高
      //info.style.width = "400px";

      // 定义顶部标题
      var top = document.createElement("div");
      var titleD = document.createElement("div");
      var closeX = document.createElement("span");
      top.className = "info-top";
      top.style.backgroundColor = '#9D9D9D';
      top.style.filter = 'alpha(opacity:80)';
      top.style.opacity = 0.8;

      titleD.innerHTML = title.title;
      titleD.style.color = 'white';


      closeX.innerHTML = '×'
      closeX.setAttribute("aria-hidden","true");
      closeX.style.float = 'right';
      closeX.style.color = 'white';
      closeX.style.margin = '2px 10px 0 0';
      closeX.style.fontSize = '20px';
      closeX.style.cursor = 'pointer';
      closeX.onclick = closeInfoWindow;

      top.appendChild(titleD);
      top.appendChild(closeX);
      info.appendChild(top);


      // 定义中部内容
      var middle = document.createElement("div");
      middle.className = "info-middle text-center";
      middle.style.backgroundColor='white';
      middle.innerHTML = reason;

      info.appendChild(middle);

      // 定义底部内容
      var bottom = document.createElement("div");
      bottom.className = "info-bottom";
      bottom.style.position = 'relative';
      bottom.style.top = '0px';
      bottom.style.margin = '0 auto';
      var sharp = document.createElement("img");
      sharp.src = "http://webapi.amap.com/images/sharp.png";
      bottom.appendChild(sharp);
      info.appendChild(bottom);
      return info;
    }

    //构建自定义信息窗体
    function createInfoWindow(title, result){
      var info = document.createElement("div");
      info.className = "info";

      //可以通过下面的方式修改自定义窗体的宽高
      //info.style.width = "400px";

      // 定义顶部标题
      var top = document.createElement("div");
      var titleD = document.createElement("div");
      var closeX = document.createElement("span");
      top.className = "info-top";


      if(title.isSelect == 1)
      {
        top.style.backgroundColor = '#DC4D3F';
      }
      else {
        top.style.backgroundColor = '#6AB92B';
      }
      top.style.filter = 'alpha(opacity:80)';
      top.style.opacity = 0.8;

      titleD.innerHTML = title.title +'【' + result.bmUserView.mobile + '】【' + result.bmWaybillViewList.length + '单】' ;
      titleD.style.color = '#ffffff';

      closeX.innerHTML = '×'
      closeX.setAttribute("aria-hidden","true");
      closeX.style.float = 'right';
      closeX.style.color = 'white';
      closeX.style.margin = '2px 10px 0 0';
      closeX.style.fontSize = '20px';
      closeX.style.cursor = 'pointer';
      closeX.onclick = closeInfoWindow;




      // 定义中部内容
      var middle = document.createElement("div");
      middle.className = "info-middle";
      middle.style.backgroundColor='white';

      var table = document.createElement("table");
      table.className = "table table-bordered table-striped";
      var thead = document.createElement('thead');

      var titleArr = new Array('状态', '商家', '收货地址');
      var tr = document.createElement("tr");
      for (var j = 0; j < 3; j++) {

          var th = document.createElement("th");
          th.className = "text-center";
          th.innerHTML = titleArr[j];
          tr.appendChild(th);
        };
      thead.appendChild(tr);

      var tbody = document.createElement('tbody');

      $.each(result.bmWaybillViewList, function(index, waybill) {


        var tr = document.createElement("tr");

        var td = document.createElement("td");
        td.innerHTML = waybill.remark;
        tr.appendChild(td);

        //td = document.createElement("td");
        //td.innerHTML = waybill.deliveredTime;
        //tr.appendChild(td);


        td = document.createElement("td");
        td.innerHTML = waybill.senderName + '#' + waybill.poiSeq;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = waybill.recipientAddress;
        tr.appendChild(td);

        tbody.appendChild(tr);

      });

      //titleD.innerHTML += '【'+ result.length + '单】' ;
      top.appendChild(titleD);
      top.appendChild(closeX);
      info.appendChild(top);

      table.appendChild(thead);
      table.appendChild(tbody);
      middle.appendChild(table);
      info.appendChild(middle);

      // 定义底部内容
      var bottom = document.createElement("div");
      bottom.className = "info-bottom";
      bottom.style.position = 'relative';
      bottom.style.top = '0px';
      bottom.style.margin = '0 auto';
      var sharp = document.createElement("img");
      sharp.src = "http://webapi.amap.com/images/sharp.png";
      bottom.appendChild(sharp);
      info.appendChild(bottom);
      return info;
    }

    function closeInfoWindow(){
      map.clearInfoWindow();
    }



    function selectRider(rider,check){
      if(rider.siblings(check).is(':checked')){
          rider.siblings(check).prop("checked",false);
          console.log(1)
      }
      else{
        rider.siblings(check).prop("checked",true);
        console.log(2);
      }

    }


    function getTotal(array){
        var total = 0;
        for(var i=0;i<array.length;i++){
          total = total + array[i];
        }
        return total;
    }






    function getRiderList(key)
    {
      var dispatchStage = waybillList[key].dispatchStage;

      $.ajax({
        url: '/playback/riderList',
        type: 'POST',
        dataType: 'json',
        data: {waybillId: waybillList[key].waybillId, stage:dispatchStage},
      })
      .done(function(result) {
        console.log("getRiderList success");



        var len = result.length;

        if(len!=0){
          curRiderList = result[len-1].playbackRiderViewList;
        }
        else{
          curRiderList = [];
        }

        var html = '';
        var i = 0;

        var countArray = new Array();
        var disTime = new Array();

        for(var list in waybillList){
            disTime.push(waybillList[list].dispatchTime);

        }
        var noScoreCount = new Array();
        for(var j=0;j<len;j++){
          noScoreCount[j] = 0;
        }


        var playTable = $('#playRecord');
        playTable.empty();
        $('.allStage').prop("checked",true);
        $('.check').prop("checked",true);

        $.each(result,function(index,riderView){

              var count =0;

              var max = riderView.playbackRiderViewList.length;
              for(var j = 0;j < max;j++){
                if(riderView.playbackRiderViewList[j].isSelect ==2){
                  count++;
                }
                if(riderView.playbackRiderViewList[j].isSelect == 0){

                  noScoreCount[i]++;

                }

              }
               var date = new Date(disTime[i] * 1000);
            var stage=riderView.stage+1;
              html = html + '<tr>';
              html = html + '<td class = "stage" style="width:42px;">' + stage + '</td>';
              html = html + '<td style="width:65px;">' + getFormatedString(date.getHours()) + ':' +getFormatedString(date.getMinutes()) + ':' + getFormatedString(date.getSeconds()) + '</td>';
              html = html + '<td style="width:65px;">' +  count + '</td>';
              html = html + '<td class = "chooseRider" style="width:28px">' + '<input type="checkbox" style="opacity:1;position:static;" checked="checked" class="choose"/>' + '</td>';
              html = html + '</tr>';
              i=i+1;

              countArray.push(count);




        })


        var total = getTotal(countArray);
        if(len != 0){
          html = html + '<tr>';
          html = html + '<td colspan="2">总计</td>';
          html = html + '<td>' + total + '</td>';
          html = html +'<td></td>';
          html = html + '</tr>';

        }


        $('#playRecord').append(html);


            var riderList=new Array();
             var allCount = 0;
            for(var i=0;i<noScoreCount.length;i++){
               allCount = allCount + noScoreCount[i];
            }
          $('#noScoreCount').html('含掉线:'+ allCount + '人');

          $('#rider-recommend').unbind('click');
          $('#rider-recommend,.rider-recommend').click(function(){

                // 注释掉模拟选中的函数，而用<label for="id">和<input id="id">来代替
                // selectRider($(this),'.rider-recommend');



                $('#playRecord input[type="checkbox"]:checked').each(function(){

                    var stage = Number($(this).parent().siblings(".stage").html())-1;

                    riderList = changeRider('.chooseRider',result,stage,riderList);

                })


                clearMap();
                fillMap(waybillList[key],riderList);
                riderList=[];

          });

          $('#rider-notRecommend').unbind('click');
          $('#rider-notRecommend,.rider-notRecommend').click(function(){


               // selectRider($(this),'.rider-notRecommend');


              $('#playRecord input[type="checkbox"]:checked').each(function(){

                  var stage = Number($(this).parent().siblings(".stage").html())-1;

                  riderList = changeRider('.chooseRider',result,stage,riderList);

              })


              clearMap();
              fillMap(waybillList[key],riderList);
              riderList=[];

          });
          $('#rider-noScore').unbind('click');
          $('#rider-noScore,.rider-noScore').click(function(){


                // selectRider($(this),'.rider-noScore');

              $('#playRecord input[type="checkbox"]:checked').each(function(){

                  var stage = Number($(this).parent().siblings(".stage").html())-1;

                  riderList = changeRider('.chooseRider',result,stage,riderList);

              })


              clearMap();
              fillMap(waybillList[key],riderList);
              riderList=[];
          });

          $('.chooseRider').click(function(){
              var stageArray = new Array();

                $('#playRecord input[type="checkbox"]:checked').each(function(){

                        var stage = Number($(this).parent().siblings(".stage").html())-1;

                        stageArray.push(stage);

                        riderList = changeRider('.chooseRider',result,stage,riderList);


                })
              var stageCount = 0;
              for(var i=0;i<stageArray.length;i++){

                  stageCount = stageCount+noScoreCount[stageArray[i]];
              }
              $('#noScoreCount').html( '含掉线:' + stageCount + '人');
              stageArray = [];

                if($('.choose:checked').length == $('#playRecord tr').length-1 ){
                    $('.allStage').prop("checked",true);

                    $('#noScoreCount').html( '含掉线:' + allCount + '人');

                }
                else{
                     $('.allStage').prop("checked",false);

                }
                if($('.choose:checked').length == 0){
                    $('#noScoreCount').html('含掉线:0人');
                }


                 clearMap();
                 fillMap(waybillList[key],riderList);
                riderList=[];

          })
          $('.allStage').click(function(){



              if($(this).is(":checked")){
                  riderList = changeRider('.allStage',result,-1,riderList)
                  $('.choose').each(function(){
                  $(this).prop("checked",true);


                })

                  clearMap();
                  fillMap(waybillList[list],riderList);
                  riderList=[]

                  $('#noScoreCount').html(  '含掉线:' + allCount + '人');

              }
              else{

                  $('.choose').each(function(){
                    $(this).prop("checked",false);
                  })

                  clearMap();
                  fillMap(waybillList[key],riderList);
                  $('#noScoreCount').html('含掉线:0人');
              }




          })


          fillMap(waybillList[key], curRiderList);

      })
      .fail(function() {
        console.log("getRiderList error");
      })
      .always(function() {

      });
    }

    function changeRider(check,result,stage,rList){
      for(var i=0;i<result.length;i++){
          var lens = result[i].playbackRiderViewList.length;
          var list = result[i].playbackRiderViewList;

          if (check == '.allStage'){



            for(var j = 0;j < lens;j++){
              rList.push(list[j]);
            }
        }
        else if(result[i].stage == stage){
            for(var j = 0;j < lens;j++){

              $('.check:checked').each(function(){
                  var val = Number($(this).val());
                  if(val == list[j].isSelect){
                    rList.push(list[j]);
                  }
              })


            }
        }
      }

      return rList;
    }




    function getId(){
      var url = window.location.href;
      var uid = url.split('?');
      var id = '';
      if(uid.length == 1)id='';
      else id = uid[1];
      if(id != '') id = id.split('=')[1];

      return id;
    }
      var uid = getId();
      $('#waybillId_input').val(uid);
      btnSearch();



});
