
var citySelect = $("#orgCity");
var orgSelect = $("#parentId");

$(document).ready(function () {

    $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
        citySelect.select2({
            matcher: oldMatcher(matchPinyin)
        });
        orgSelect.select2({
            matcher: oldMatcher(matchPinyin)
        });
    });

    function submit() {
        var orgName = $("#orgName").val();
        var parentId = $("#parentId").val();
        var orgCity = $("#orgCity").val();
        var emergencyPhone = $("#emergencyPhone").val();

        if(isBlank(orgName)) {
            showTip("请输入正确的自营分部名称", 2);
            return false;
        }
        if(isBlank(parentId) || parentId == 0) {
            showTip("请选择所属自营片区", 2);
            return false;
        }
        if(isBlank(emergencyPhone) || !isMobile(emergencyPhone)) {
            showTip("请输入正确的分部联系电话", 2);
            return false;
        }

        var data = {}
        data.orgName = orgName;
        data.parentId = parentId;
        data.orgCity = orgCity;
        data.emergencyPhone = emergencyPhone;


        _showPop({
            type:1,
            content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + "确认创建自营分部：“" + orgName + "”</div>",
            ok: {
                display: true,
                name: "确定",
                callback: function(){
                    unbindSubmit();
                    $.ajax({
                        dataType: 'json',
                        type : 'post',
                        url : "/org/branch/doCreateOrg",
                        data : data,
                        success : function(r){
                            if (r) {
                                if(r.code == 0) {
                                    window.location.href='/org/deliveryDivisionList';
                                } else {
                                    alert(r.msg);
                                    bindSubmit();
                                }
                            } else {
                                alert("添加自营分部失败!");
                                bindSubmit();
                            }
                        },
                        error: function() {
                            alert("添加自营分部失败!网络异常!");
                            bindSubmit();
                        }
                    });
                }
            }
        });
    }

    function showTip(content, type) {
        _showPop({
            type:type,
            content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + content + "</div>"
        });
    }

    function bindSubmit() {
        $("#js_save").on("click", function() {
            submit();
        }).removeAttr("disabled");
    }

    function unbindSubmit() {
        $("#js_save").off("click").attr("disabled", "disabled");
    }

    $("#js_save").on("click", function() {
        submit();
    });

    $("#js_cancel").on("click", function() {
        window.location.href = '/org/deliveryDivisionList';
    });

    $(document).keypress(function(e) {
        if(e.which == 13) {
            $("#js_save").click();
        }
    });
});



