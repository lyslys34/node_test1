require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/root'], function (t) {
    var commonOption = [];
    var poxyLevelType = [];
    var selfLevelType = [];
    $(document).ready(function(){
        $(".other_level_org").each(function() {
            commonOption.push($("<p>").append($(this).clone()).html());
        });

        $(".type_proxy").each(function() {
            poxyLevelType.push($("<p>").append($(this).clone()).html());
        });

        $(".type_self").each(function() {
            selfLevelType.push($("<p>").append($(this).clone()).html());
        });

        $("#add").bind("click",addBusinessHourses);
        $("#delete").bind("click",deleteBusinessHourses);

        $("[name=businessHoursBegin],[name=businessHoursEnd]").blur(function(){
            var temp = $(this).val();
            var time = temp.trim().replace("：",":");
            $(this).val(time);
        });

        $("#phone").blur(function(){
            var temp = $(this).val().trim();
            $(this).val(temp);
        });

        $("#submit").click(function(){
            if(submitCheck()){
                var orgName = $("#orgName").val();
                var content = "点击“下一步”，加盟站信息进入审核流程，不可更改。待审核过后，方可修改" ;
                var buttonName = "下一步";
                var stationLevelType = $("#levelType").val();

                if(stationLevelType == 330 || stationLevelType == 430 || stationLevelType == 770){
                    content = "确认创建此站点？";
                    buttonName = "确定";
                }

                //弹窗
                _showPop({
                    type:1,
                    content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + content + "</div>",
                    ok: {
                        display: true,
                        name: buttonName,
                        callback: function(){
                            $.ajax({
                                dataType: 'json',
                                type : 'post',
                                url : "/org/isUnique",
                                data : {name:orgName},
                                success : function(r){
                                    if (r) {
                                        if(r.code == 0) {
                                            if(r.data) {
//                                                $('#formValidate1').submit();
                                                  $("#realSubmit").click();
                                            } else {
                                                alert("此站点已存在，请更换其它名称");
                                            }
                                        } else {
                                            alert("系统错误");
                                        }
                                    } else {
                                        alert("系统错误");
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });


        $('#formValidate1').submit(function () {
            //设置营业时间
            var businessHourses = getBuisnessHourses();
            $("#businessHourses").val(businessHourses);
        });
        $('#formValidate1').keypress(function(event){
            if(event.which == 13){
                event.preventDefault();
                $("#submit").click();
            }
        });

        initpage();
    });

function submitCheck(){
    if ($.trim($('#orgName').val()) == "") {
        alert("站点名称不能为空");
        return false;
    }

    if($.trim($('#phone').val()) == ""){
        alert("联系电话不能为空");
        return false;
    }
    if(!isPhone($.trim($('#phone').val()))){
        alert("联系电话格式不正确");
        return false;
    }

    var stationLevelType = $("#levelType").val();
    if(stationLevelType == 230) {
        //设置组织类别为加盟
        $("#orgType").val(2);
    } else if(stationLevelType == 430) {
        //设置组织类型为众包
        $("#orgType").val(4);
    }  else if(stationLevelType == 770) {
        //设置组织类型为城市代理
        $("#orgType").val(7);
    } else {
        //设置组织类别为自建
        $("#orgType").val(1);
    }

    var parentId = 0;
    $('#parentId option:selected').each(function() {
        parentId = $(this).val();
    });
    if (typeof parentId == 'undefined' || parentId == '' || parentId == 0) {
        alert("请选择上级组织");
        return false;
    }
    var businessHourses = getBuisnessHourses();
    if(!businessHourses){
        return false;
    }
    return true;
}
function isPhone(str){
    var mobile = "^0?(13|15|18|14|17)[0-9]{9}$";
    var tel = "^[0-9\-()（）]{7,18}$";
    return new RegExp(mobile).test(str) || new RegExp(tel).test(str);
}

function addBusinessHourses(){
    $(this).parent().next().removeClass("hide");
    $("[name=businessHoursEnd]").eq(0).val("24:00").attr("disabled",true);
    $("[name=businessHoursBegin]").eq(1).val("00:00").attr("disabled",true);
    $("[name=businessHoursEnd]").eq(1).val("");
    $(this).find("i").removeClass("opration-icon");

    $(this).unbind();
}
function deleteBusinessHourses(){
    $(this).parent().addClass("hide");
    $("#add").bind("click",addBusinessHourses);
    $("#add").find("i").addClass("opration-icon");
    $("[name=businessHoursEnd]").eq(0).val("").attr("disabled",false);
}

function getBuisnessHourses(){
    var businessHourses = "";
    var flag = true;
    $(".businessHourses").each(function(){
        if(!$(this).hasClass("hide")){
            var beginTime = $(this).find("[name=businessHoursBegin]").val();
            var endTime = $(this).find("[name=businessHoursEnd]").val();
            if(isTime(beginTime) && isTime(endTime) && compareTime(endTime,beginTime) == 1){
                businessHourses = businessHourses + "{beginTime:\"" +beginTime +"\",endTime:\""+endTime+"\"}";
            }else{
                alert("请设置正确的营业时间！");
                flag = false;
                return false;
            }
        }

    });
    if(businessHourses.length > 0 && flag){
        businessHourses = "["+businessHourses+"]";
        return businessHourses;
    }
    return false;
}

function isTime(str){
    var times = str.split(":");
    if(times.length ==2){
        if(times[0].length > 2 || times[1].length > 2){
            return false;
        }
        //时
        var numPatt = new RegExp("^\\d*$");
        if(!(numPatt.test(times[0]) && numPatt.test(times[1]))){
            return false;
        }
        var hour = parseInt(times[0]);
        var minute = parseInt(times[1]);
        if(hour>=0 && hour <= 24 && minute >=0 && minute <=59 && !(hour==24 && minute >0)){
            return true;
        }
    }
    return false;
}

function compareTime(time1,time2){
    var time1Arr = time1.split(":");
    var time2Arr = time2.split(":");
    var i = 0;
    for( ; i < time1Arr.length && i < time2Arr.length; i++){
        if(parseInt(time1Arr[i]) > parseInt(time2Arr[i])){
            return 1;
        }
        if(parseInt(time1Arr[i]) < parseInt(time2Arr[i])){
            return -1;
        }
    }
    if(i == time1Arr.length && i == time2Arr.length){
        return 0;
    }
    if(i == time2Arr.length){
        return -1;
    }
    return 1;
}

function initpage() {
    var levelType = $("#levelType").val();
    setInfoByLevelType(levelType);
    $('#levelType').on('change', function(){
        var levelType = $("#levelType").val();
        if(levelType == 330 || levelType == 430 || levelType == 770){
            $("#head-message").hide();
        }else{
            $("#head-message").show();
        }
        setInfoByLevelType(levelType);
    });
}

function setInfoByLevelType(levelType){
    if(levelType == 330){
        $("#head-message").hide();
        $("#parentLabel").text("自营分部");

        $("#settleProgress").remove();
        $(".nav-wizard li").css("width","45%");
        $("#isProxyDiv").hide();
    } else if(levelType == 430) {
        $("#head-message").hide();
        $("#parentLabel").text("众包城市组");

        $("#settleProgress").remove();
        $(".nav-wizard li").css("width","45%");
        $("#isProxyDiv").hide();
    } else if(levelType == 770) {
        $("#head-message").hide();
        $("#parentLabel").text("城市代理城市组");

        $("#settleProgress").remove();
        $(".nav-wizard li").css("width","45%");
    } else {
        $("#head-message").show();
        $("#parentLabel").text("加盟商");

        $(".nav-wizard").append('<li id="settleProgress"><a>step3:结算设置</a></li>');
        $(".nav-wizard li").css("width","30%");
        $("#isProxyDiv").show();
    }
    getOrgByLevelType(levelType);
}

function getOrgByLevelType(levelType) {
    $.ajax({
        dataType: 'json',
        type : 'post',
        url : "/org/levelOrgList",
        data: {
            levelType : levelType
        },
        success : function(r){
            if (r && r.code == 0) {
                if (r.data) {
                    $("#parentId").empty();
                    $("#parentId").append("<option value='0' selected='selected'>请选择上级组织</option>");
                    $.each(r.data, function(index, value){

                        $("#parentId").append("<option value='"+value.id+"'>"+value.value+"</option>");
                    });
                    $parentId.select2();

                } else {
                    $("#parentId").empty().append(commonOption.join(''));
                }
            }
        }

    });
}

});

