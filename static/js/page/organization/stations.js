require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu'
        ],
        'lib/autocomplete' : {
            deps : ['page/common']
        },
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/root', 'module/validator',  'module/cookie', 'lib/autocomplete'], function (t, validator, cookie, autocomplete){
	
	var orgNameMap = {};
	var orgIdMap = {};
	var cityNameMap = {};
	var cityIdMap = {};
	var zhongbaoType = 4;
	
	$(document).ready(function(){
		//initSearchOrg();
		//initSearchCity();
		initBindAndUnbindAreaBtn();
		initcancelBtn();
		initBindAreaSubmitBtn();
		// initSearchAllCity();
		initSearchBtn();
	});
	
	function initSearchBtn() {
		$("#search").click(function() {
	        var orgId = $("#orgId").val().trim();
	        var orgName = $("#orgName").val().trim();
	        if(orgId == 0 && orgName != "") {
	          showError("请填写正确的组织名称");
	          return false;
	        }
	        if(orgId == "") {
	          showError("请填写组织名称");
	          return false;
	        }
	        
	        var cityId = $("#cityId").val().trim();
	        var cityName = $("#orgCity").val().trim();
	        if(cityId == 0 && cityName != "") {
	          showError("请填写正确的组织名称");
	          return false;
	        }
	        if(cityId == "") {
	          showError("请填写组织名称");
	          return false;
	        }

		    $("#formValidate1").submit();
		});
	}
	
	function initSearchOrg(){
		myPOST('/org/orgSearchList', {}, function (r) {
	         if (r.httpSuccess) {
	            if(r.data && r.data.code == 0) {          
	                $(".js_org_name").autocomplete({
	                    delay:100,
	                    source : r.data.data,
	                    focus: function( event, ui ) {
	                        $( ".js_org_name" ).val( ui.item.value );
	                        $( ".js_org_id" ).val( ui.item.id );
	                        return false;
	                    },
	                    select: function(event, ui) {
	                        $(".js_org_name").val(ui.item.value );
	                        $(".js_org_id" ).val( ui.item.parentId );
	                    },
	                    change: function(event, ui) {
	                        var orgName = $(".js_org_name").val();
	                        if(validator.isBlank(orgName)) {
	                            $(".js_org_id").val(0);
	                        } 
	                    },
	                    open: function(event, ui) {
	                        $(".js_org_id").val('0');
	                    }

	                }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	                    return $( "<li>" )
	                        .append( "<a>" + item.value + "</a>" )
	                        .appendTo( ul );
	                };
	                if(r.data.data) {
	                    var orgId = $(".js_org_id").val();
	                    $.each(r.data.data, function(index, item) {
	                        orgNameMap[item.value] = item;
	                        orgIdMap[item.id] = item;
	                        if(orgId) {
	                          if(orgId == item.id) {
	                            $(".js_org_name").val(item.value);
	                          }
	                        }
	                    });   
	                }

	                var riderId = $(".js_org_id").val();
	                $(".js_org_name").val(orgIdMap[riderId] != null && typeof orgIdMap[riderId] != 'undefined' ? orgIdMap[riderId].value : "");

	            } else {
	                alert("获取组织信息失败，" + r.data.msg);
	            }
	        } else {
	            //alert(JSON.stringify(r));
	            // alert('获取组织信息失败，系统错误');
	        }
	    });
	}
	
	function initSearchCity() {

	    myPOST('/org/citySearchList', {}, function (r) {
	         if (r.httpSuccess) {
	            if(r.data && r.data.code == 0) {     
	                $(".js_city_name").autocomplete({
	                    delay:100,
	                    source : r.data.data,
	                    focus: function( event, ui ) {
	                        $( ".js_city_name" ).val( ui.item.value );
	                        $( ".js_city_id" ).val( ui.item.id );
	                        return false;
	                    },
	                    select: function(event, ui) {

	                        $(".js_city_name").val(ui.item.value );
	                        $(".js_city_id").val(ui.item.id );
	                    },
	                    change: function(event, ui) {
	                        var cityName = $(".js_city_name").val();
	                        if(validator.isBlank(cityName)) {
	                            $(".js_city_id").val(0);
	                        }
	                    },
	                    open: function(event, ui) {
	                        $(".js_city_id").val('0');
	                    }

	                }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	                    return $( "<li>" )
	                        .append( "<a>" + item.value + "</a>" )
	                        .appendTo( ul );
	                };
	                if(r.data.data) {
	                    var cityId = $(".js_city_id").val();
	                    $.each(r.data.data, function(index, item) {
	                        cityNameMap[item.value] = item;
	                        cityIdMap[item.id] = item;
	                        if(cityId) {
	                          if(cityId == item.id) {
	                            $(".js_city_name").val(item.value);
	                          }
	                        }
	                    });
	                    
	                }

	                var riderId = $(".js_city_id").val();
	                $(".js_city_name").val(cityIdMap[riderId] != null && typeof cityIdMap[riderId] != 'undefined' ? cityIdMap[riderId].value : "");

	            } else {
	                alert("获取城市信息失败，" + r.data.msg);
	            }
	        } else {
	            //alert(JSON.stringify(r));
	            // alert('获取组城市息失败，系统错误');
	        }
	    });
	}
	
	function myPOST(url, data, callback)  // 发送POST请求
    {
        if(typeof(data) == 'function') { callback = data; data = null; }
        $.ajax({ type: 'POST', url: url, data:data, success:function(r) {

            callback({ httpSuccess: true, data: r });

        }, error: function(XmlHttpRequest, textStatus, errorThrown) {

            callback({ httpSuccess: false, statusCode:XmlHttpRequest.status });

        } });
    }

    function isBlank(value) {
	if(typeof String.prototype.trim !== 'function') {
	    String.prototype.trim = function() {
	        return this.replace(/^\s+|\s+$/g, '');
	    }
	}
	return !_valueExist(value) || value.trim() == "";
	}

	function showError(errMsg) {
		$("#alert_error").empty();
		$("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
	}


	function showBindAreaError(errMsg) {
		$("#alert_bind_area_error").empty();
		$("#alert_bind_area_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
	}

	function initBindAndUnbindAreaBtn() {
        $(".bindArea_link").click(function() {
            var orgIdAndCity = $(this).attr('rel'); 

            if (!orgIdAndCity || orgIdAndCity == '') {
            	showError("无法获取站点ID和对应城市");
            	return false;
            }

            var orgIdAndCityDatas = orgIdAndCity.split(",");

            if (!orgIdAndCityDatas || orgIdAndCityDatas.length < 4) {
            	showError("站点数据有错误，无法绑定");
            	return false;
            }

            var orgId = orgIdAndCityDatas[0];
            var cityId = orgIdAndCityDatas[1];
            var cityName = orgIdAndCityDatas[2];
            var orgType = orgIdAndCityDatas[3];

            if (!orgId || orgId <= 0) {
            	showError("无法获取站点Id");
            	return false;
            }

            if (!cityId || cityId <= 0) {
            	showError("无法获取城市id");
            	return false;
            }

            if (!cityName || cityName == '') {
            	showError("无法获取城市名称");
            	return false;
            }

            if (!orgType || orgType <= 0) {
            	showError("无法获取站点类型");
            	return false;
            }

            $(".js_bind_city_name").val(cityName);
			$(".js_bind_city_id").val(cityId);
			$(".js_bind_org_type").val(orgType);
			SearchAreaByCity(cityId, orgType);

            $("#bindOrgId").val(orgId);
            $("#alert_bind_area_error").empty();
            $("#bindDeliveryAreaModal").modal();


        });
        $(".unbindArea_link").click(function() {  //解绑入口
			var orgId = $(this).attr('rel'); //org_id
			deliveryAreaCheck(orgId);
		});
    }


    function initcancelBtn() {
        $('#cancelBindArea').on('click', function() {
            $("#bindCityName").val('');
            $("#bindAreaName").val('');
            $("#bindOrgType").val('');
            $("#isChecked" ).val(0);
            $("#modelChangeTips").hide();
            $("#bindDeliveryAreaModal").modal('hide');
        });
        $('#bindAreaCloseButton').on('click', function() {
			$("#bindCityName").val('');
			$("#bindAreaName").val('');
			$("#bindOrgType").val('');
			$("#isChecked" ).val(0);
			$("#modelChangeTips").hide();
			$("#bindDeliveryAreaModal").modal('hide');
		});
        $('#cancelunBindArea').on('click', function() {
			$("#unbindAreaId").val(0);
			$("#unBindOrgId").val(0);
			$("#unBindAreaTipsModal").modal('hide');
		});
    }

    function initBindAreaSubmitBtn() {
        $('#bindAreaSubmit').on('click', function(){
        	var isChecked = $("#isChecked").val();
        	if (isChecked == 1) {
        		$( "#isChecked" ).val(0);
        		bindDeliveryArea();
        	}else{
				orgNumOnAreaCheck();
        	}
        });
	    $('#unBindAreaResubmit').on('click', function() {
			var areaId = $("#unBindAreaId").val();
			var orgId = $("#unBindOrgId").val();
			$("#unBindAreaId").val(0);
			$("#unBindOrgId").val(0);
			$("#unBindAreaTipsModal").modal('hide');
	   		unbindDeliveryArea(areaId, orgId);
	    });

    }

	// function initSearchAllCity(){
	// 	myPOST('/org/citySearchList', {}, function (r) {
	// 		 if (r.httpSuccess) {
	// 			if(r.data && r.data.code == 0) {
	// 				$(".js_bind_city_name").autocomplete({
	// 					delay:100,
	// 					source : r.data.data,
	// 					focus: function( event, ui ) {
	// 						$( ".js_bind_city_name" ).val( ui.item.value );
	// 						$( ".js_bind_city_id" ).val( ui.item.id );
	// 						return false;
	// 					},
	// 					select: function(event, ui) {
	// 						$("#modelChangeTips").hide();
	// 						$(".js_bind_city_name").val(ui.item.value );
	// 						$(".js_bind_city_id").val(ui.item.id );
	// 						SearchAreaByCity(ui.item.id);
	// 					},
	// 					change: function(event, ui) {
	// 						$("#modelChangeTips").hide();
	// 						var cityName = $(".js_bind_city_name").val();
	// 						if(validator.isBlank(cityName)) {
	// 							$(".js_bind_city_id").val(0);
	// 						}
	// 					},
	// 					open: function(event, ui) {
	// 						$(".js_bind_city_id").val('0');
	// 					}

	// 				}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	// 					return $( "<li>" )
	// 						.append( "<a>" + item.value + "</a>" )
	// 						.appendTo( ul );
	// 				};
	// 				if(r.data.data) {
	// 					var cityId = $(".js_bind_city_id").val();
	// 					$.each(r.data.data, function(index, item) {

	// 						if(cityId) {
	// 						  if(cityId == item.id) {
	// 							$(".js_bind_city_name").val(item.value);
	// 						  }
	// 						}
	// 					});

	// 				}

	// 				var areaCityId = $(".js_bind_city_id").val();
	// 				$(".js_bind_city_name").val(cityIdMap[areaCityId] != null && typeof cityIdMap[areaCityId] != 'undefined' ? cityIdMap[areaCityId].value : "");

	// 			} else {
	// 				alert("获取城市信息失败，" + r.data.msg);
	// 			}
	// 		} else {
	// 			//alert(JSON.stringify(r));
	// 			// alert('获取组城市息失败，系统错误');
	// 		}
	// 	});
	// }

	function orgNumOnAreaCheck(){
		var bindAreaId = $("#bindAreaId").val();
		$.ajax({
			 dataType: 'json',
			 type : 'post',
			 url : "/delivery/getOrgCountOfArea",
			 data: {
				 areaId: bindAreaId
			 },
			 success : function(data){
				 var count = data.data;
				 if(count > 0){ //t提示信息
				 	var type = $("#bindOrgType").val();
				 	if (type && type == zhongbaoType) {
				 		showBindAreaError("该众包区域已绑定其它众包站点，请选择其它区域");
				 		return false;
				 	}

				 	isMoreOrgArea();

				 	// if (count == 1) {
				 	// 	$("#modelChangeOneTips").show();
				 	// } else {
				 	// 	$("#modelChangeMoreTips").show();
				 	// }
					// $( "#isChecked" ).val(1);
				 }else{  //赋值
//				 	$( "#isChecked" ).val(1);
//					alert("无提示，直接调用绑定");
				 	bindDeliveryArea();
				 }
			 },
			 error:function(XMLHttpRequest ,errMsg){
				 alert("网络连接失败");
			 }
		 });
	}

	function isMoreOrgArea() {
		var bindAreaId = $("#bindAreaId").val();
		$.ajax({
			dataType: 'json',
			type : 'post',
			url : "/delivery/isMoreOrgArea",
			data: {
				areaId: bindAreaId
			},
			success : function(data){
				var isMoreOrgArea = data.data;
					 if(!isMoreOrgArea){ //t提示信息
					 	showBindAreaError('当前调度模式不支持一个区域绑定多站点，如需绑定，请先将调度模式改为"推抢结合"');
					 	return false;
					 }else{
					 	bindDeliveryArea();
					 }
					},
					error:function(XMLHttpRequest ,errMsg){
						alert("网络连接失败");
					}
				});
	}

	function bindDeliveryArea() {
			var orgId = $("#bindOrgId").val();
			var bingCityId = $("#bindCityId").val();
			var bindCityName = $("#bindCityName").val().trim();
			if(bindCityId == "") {
			  showBindAreaError("请填写城市名称");
			  return false;
			}
			if(bindCityId == 0 && bindCityName != "") {
			  showBindAreaError("请填写正确的城市名称");
			  return false;
			}

            var bindAreaId = $("#bindAreaId").val();
            var bindAreaName = $("#bindAreaName").val().trim();
			if(bindAreaId == "") {
			  showBindAreaError("请填写配送区域名称");
			  return false;
			}
            if(bindAreaId == 0 && bindAreaName != "") {
			  showBindAreaError("请填写正确的配送区域名称");
			  return false;
			}

             $.ajax({
                 dataType: 'json',
                 type : 'post',
                 url : "/delivery/bindOrg",
                 data: {
                       id: bindAreaId,
                     orgId: orgId
                 },
                 success : function(data){
//                 	alert(data.success);
                     if(data.success){
                         $("#resMsg").html("<h4>绑定成功</h4>");
                         $("#bindDeliveryAreaModal").modal('hide');
                         $("#showBindAreaRes").modal();
                         setTimeout(function(){
							 $("#showBindAreaRes").modal("hide");
							 window.location.href = "/org/stations";
						 },2000);
                     }else {
                         showBindAreaError(data.errMsg);
                     }
                 },
                 error:function(XMLHttpRequest ,errMsg){
                     alert("网络连接失败");
                 }
             });

    }

    function deliveryAreaCheck(orgId){
		$.ajax({
			 dataType: 'json',
			 type : 'post',
			 url : "/delivery/getOrgCountOnAreaByOrgId",
			 data: {
				 orgId: orgId
			 },
			 success : function(data){
				 var orgCount = data.data[1];
				 var areaId = data.data[0];
				 $("#unBindOrgId").val(orgId);
				 $("#unBindAreaId").val(areaId);
				 if(orgCount == 1){   //弹窗提醒
				 	$("#unbind_one_massage").show();
				 	$("#unbind_more_massage").hide();
				 	$("#unBindAreaTipsModal").modal();
				 	
				 }else{  //该区域上多于一个站点,直接解绑
				 	// unbindDeliveryArea(areaId, orgId);
				 	$("#unbind_one_massage").hide();
				 	$("#unbind_more_massage").show();
				 	$("#unBindAreaTipsModal").modal();
				 }
			 },
			 error:function(XMLHttpRequest ,errMsg){
				 alert("网络连接失败");
			 }
		});

    }

	function unbindDeliveryArea(areaId, orgId){
		$.ajax({
			 dataType: 'json',
			 type : 'post',
			 url : "/delivery/unBindOrg",
			 data: {
				   id: areaId,
				 orgId: orgId
			 },
			 success : function(data){
				 if(data.success){
					 $("#unBindResMsg").html("<h4>解绑成功</h4>");
					 $("#showUnbindAreaRes").modal();
					 setTimeout(function(){
                        $("#showUnbindAreaRes").modal("hide");
                        window.location.href = "/org/stations";
                      },2000);
				 }else {
					 $("#unBindResMsg").html("<h4>解绑失败</h4>");
                     $("#showUnbindAreaRes").modal();
				 }
			 },
			 error:function(XMLHttpRequest ,errMsg){
				 alert("网络连接失败");
			 }
		});
	}

	function SearchAreaByCity(cityId, orgType){
		myPOST('/delivery/getCityAreas', {cityId : cityId, orgType : orgType}, function (r) {
			  if (r.httpSuccess) {
					if(r.data && r.data.code == 0) {
						$(".js_bind_area_name").autocomplete({
							delay:100,
							source : r.data.data,
							focus: function( event, ui ) {
								$( ".js_bind_area_name" ).val( ui.item.value );
								$( ".js_bind_area_id" ).val( ui.item.id );
								return false;
							},
							select: function(event, ui) {
								$("#modelChangeTips").hide();
								$(".js_bind_area_name").val(ui.item.value );
								$(".js_bind_area_id").val(ui.item.id );
							},
							change: function(event, ui) {
								$("#modelChangeTips").hide();
								var cityName = $(".js_bind_area_name").val();
								if(validator.isBlank(cityName)) {
									$(".js_bind_area_id").val(0);
								}
							},
							open: function(event, ui) {
								$(".js_bind_area_id").val('0');
							}

						}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
							return $( "<li>" )
								.append( "<a>" + item.value + "</a>" )
								.appendTo( ul );
						};
						if(r.data.data) {
							var cityId = $(".js_bind_area_id").val();
							$.each(r.data.data, function(index, item) {

								if(cityId) {
								  if(cityId == item.id) {
									$(".js_bind_area_name").val(item.value);
								  }
								}
							});

						}
					} else {
						alert("获取城市配送区域信息失败，" + r.data.msg);
					}
				} else {
					//alert(JSON.stringify(r));
					// alert('获取组城市息失败，系统错误');
				}
        });
    }

    function showError(errMsg) {
        $("#alert_error").empty();
        $("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
    }

	$(".js_btn_status").click(function() {
		var thisTag = $(this);
		var stationId = thisTag.siblings(".js_station_id").val();
		showTip(function() {
			var name = thisTag.parents('tr').data('orgname');
			var deleteName = $('#js_delete_name').val();

			if(name != deleteName) {
				alert("输入的组织名称与将要设置不合作的不符，不允许设置不合作");
			} else {
				$.ajax({
					dataType: 'json',
					type : 'post',
					url : "/org/disoperate",
					data: {
						orgId: stationId
					},
					success : function(data){
						if(data.code == 0){
							window.location.reload();
						}else {
							alert(data.msg);
						}
					},
					error:function(XMLHttpRequest ,errMsg){
						alert("网络连接失败");
					}
				});
			}
		});
	});


	function showTip(callback) {
		var content = "";
		content += "<div style='text-align: center'>此操作将删除站点及人员的一切信息，次日生效<br/>信息不可恢复，请谨慎操作!</div><br/>";
		content += "<div style=\"text-align: center; font-size:14px;\">站点名称：<input type=\"text\" class=\"form-control input-sm\" style=\"display: inline-block; width: 220px;\" id=\"js_delete_name\" placeholder=\"请输入不合作站点完整名称\"/></div><br/>";
		//content += "<div id=\"js_delete_error\" style=\"color:red; \"></div><br/>";
		content += "<div style='color:red; font-size:14px;'>如站点已绑定配送区域，解绑后商家将被置休，建议先将其它站点绑定此配送区域，再修改此站点为“不合作”</div>";
		_showPop({
			type:1,
			content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + content + "</div>",
			ok: {
				display:true,
				name:'确认不合作',
				callback: function() {
					callback();
				}
			},
			cancel: {
				display:true,
				name:'我再想想'
			}
		});
	}


});