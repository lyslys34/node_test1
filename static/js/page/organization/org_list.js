require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu'
        ],
        'lib/autocomplete' : {
            deps : ['page/common']
        },
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/root', 'module/validator',  'module/cookie', 'lib/autocomplete'], function (t, validator, cookie, autocomplete){
	
	var orgNameMap = {};
	var orgIdMap = {};
	var cityNameMap = {};
	var cityIdMap = {};
	
	$(document).ready(function(){
		initBindAndUnbindAreaBtn();
		initcancelBtn();
		initBindAreaSubmitBtn();
		initSearchAllCity();
	});
	
	function myPOST(url, data, callback)  // 发送POST请求
    {
        if(typeof(data) == 'function') { callback = data; data = null; }
        $.ajax({ type: 'POST', url: url, data:data, success:function(r) {

            callback({ httpSuccess: true, data: r });

        }, error: function(XmlHttpRequest, textStatus, errorThrown) {

            callback({ httpSuccess: false, statusCode:XmlHttpRequest.status });

        } });
    }

    function isBlank(value) {
	if(typeof String.prototype.trim !== 'function') {
	    String.prototype.trim = function() {
	        return this.replace(/^\s+|\s+$/g, '');
	    }
	}
	return !_valueExist(value) || value.trim() == "";
	}

	function showError(errMsg) {
		$("#alert_error").empty();
		$("#alert_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
	}


	function showBindAreaError(errMsg) {
		$("#alert_bind_area_error").empty();
		$("#alert_bind_area_error").append("<div class='alert alert-danger' role='alert'>" + errMsg + "</div>");
	}

	function initBindAndUnbindAreaBtn() {
        $(".bindArea_link").click(function() {
            var orgId = $(this).attr('rel'); //org_id
            $("#bindOrgId").val(orgId);
            $("#alert_bind_area_error").empty();
            $("#bindDeliveryAreaModal").modal();


        });
        $(".unbindArea_link").click(function() {  //解绑入口
			var orgId = $(this).attr('rel'); //org_id
			deliveryAreaCheck(orgId);
		});
    }


    function initcancelBtn() {
        $('#cancelBindArea').on('click', function() {
            $("#bindCityName").val('');
            $("#bindAreaName").val('');
            $("#isChecked" ).val(0);
            $("#modelChangeTips").hide();
            $("#bindDeliveryAreaModal").modal('hide');
        });
        $('#bindAreaCloseButton').on('click', function() {
			$("#bindCityName").val('');
			$("#bindAreaName").val('');
			$("#isChecked" ).val(0);
			$("#modelChangeTips").hide();
			$("#bindDeliveryAreaModal").modal('hide');
		});
        $('#cancelunBindArea').on('click', function() {
			$("#unbindAreaId").val(0);
			$("#unBindOrgId").val(0);
			$("#unBindAreaTipsModal").modal('hide');
		});
    }

    function initBindAreaSubmitBtn() {
        $('#bindAreaSubmit').on('click', function(){
        	var isChecked = $("#isChecked").val();
        	if (isChecked == 1) {
        		$( "#isChecked" ).val(0);
        		bindDeliveryArea();
        	}else{
				orgNumOnAreaCheck();
        	}
        });
	    $('#unBindAreaResubmit').on('click', function() {
			var areaId = $("#unBindAreaId").val();
			var orgId = $("#unBindOrgId").val();
			$("#unBindAreaId").val(0);
			$("#unBindOrgId").val(0);
			$("#unBindAreaTipsModal").modal('hide');
	   		unbindDeliveryArea(areaId, orgId);
	    });

    }

	function initSearchAllCity(){
		myPOST('/org/citySearchList', {}, function (r) {
			 if (r.httpSuccess) {
				if(r.data && r.data.code == 0) {
					$(".js_bind_city_name").autocomplete({
						delay:100,
						source : r.data.data,
						focus: function( event, ui ) {
							$( ".js_bind_city_name" ).val( ui.item.value );
							$( ".js_bind_city_id" ).val( ui.item.id );
							return false;
						},
						select: function(event, ui) {
							$("#modelChangeTips").hide();
							$(".js_bind_city_name").val(ui.item.value );
							$(".js_bind_city_id").val(ui.item.id );
							SearchAreaByCity(ui.item.id);
						},
						change: function(event, ui) {
							$("#modelChangeTips").hide();
							var cityName = $(".js_bind_city_name").val();
							if(validator.isBlank(cityName)) {
								$(".js_bind_city_id").val(0);
							}
						},
						open: function(event, ui) {
							$(".js_bind_city_id").val('0');
						}

					}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
						return $( "<li>" )
							.append( "<a>" + item.value + "</a>" )
							.appendTo( ul );
					};
					if(r.data.data) {
						var cityId = $(".js_bind_city_id").val();
						$.each(r.data.data, function(index, item) {

							if(cityId) {
							  if(cityId == item.id) {
								$(".js_bind_city_name").val(item.value);
							  }
							}
						});

					}

					var areaCityId = $(".js_bind_city_id").val();
					$(".js_bind_city_name").val(cityIdMap[areaCityId] != null && typeof cityIdMap[areaCityId] != 'undefined' ? cityIdMap[areaCityId].value : "");

				} else {
					alert("获取城市信息失败，" + r.data.msg);
				}
			} else {
				//alert(JSON.stringify(r));
				// alert('获取组城市息失败，系统错误');
			}
		});
	}

	function orgNumOnAreaCheck(){
		var bindAreaId = $("#bindAreaId").val();
		$.ajax({
			 dataType: 'json',
			 type : 'post',
			 url : "/delivery/getOrgCountOfArea",
			 data: {
				 areaId: bindAreaId,
			 },
			 success : function(data){
				 var count = data.data;
				 if(count > 0){ //t提示信息
					$("#modelChangeTips").show();
					$( "#isChecked" ).val(1);
				 }else{  //赋值
//				 	$( "#isChecked" ).val(1);
//					alert("无提示，直接调用绑定");
				 	bindDeliveryArea();
				 }
			 },
			 error:function(XMLHttpRequest ,errMsg){
				 alert("网络连接失败");
			 }
		 });
	}

	function bindDeliveryArea() {
			var orgId = $("#bindOrgId").val();
			var bingCityId = $("#bindCityId").val();
			var bindCityName = $("#bindCityName").val().trim();
			if(bindCityId == "") {
			  showBindAreaError("请填写城市名称");
			  return false;
			}
			if(bindCityId == 0 && bindCityName != "") {
			  showBindAreaError("请填写正确的城市名称");
			  return false;
			}

            var bindAreaId = $("#bindAreaId").val();
            var bindAreaName = $("#bindAreaName").val().trim();
			if(bindAreaId == "") {
			  showBindAreaError("请填写配送区域名称");
			  return false;
			}
            if(bindAreaId == 0 && bindAreaName != "") {
			  showBindAreaError("请填写正确的配送区域名称");
			  return false;
			}

             $.ajax({
                 dataType: 'json',
                 type : 'post',
                 url : "/delivery/bindOrg",
                 data: {
                       id: bindAreaId,
                     orgId: orgId
                 },
                 success : function(data){
//                 	alert(data.success);
                     if(data.success){
                         $("#resMsg").html("<h4>绑定成功</h4>");
                         $("#bindDeliveryAreaModal").modal('hide');
                         $("#showBindAreaRes").modal();
                         setTimeout(function(){
							 $("#showBindAreaRes").modal("hide");
							 window.location.href = "/org/list";
						 },2000);
                     }else {
                         showBindAreaError(data.errMsg);
                     }
                 },
                 error:function(XMLHttpRequest ,errMsg){
                     alert("网络连接失败");
                 }
             });

    }

    function deliveryAreaCheck(orgId){
		$.ajax({
			 dataType: 'json',
			 type : 'post',
			 url : "/delivery/getOrgCountOnAreaByOrgId",
			 data: {
				 orgId: orgId,
			 },
			 success : function(data){
				 var orgCount = data.data[1];
				 var areaId = data.data[0];
				 if(orgCount == 1){   //弹窗提醒
				 	$("#unBindAreaTipsModal").modal();
				 	$("#unBindOrgId").val(orgId);
				 	$("#unBindAreaId").val(areaId);
				 }else{  //该区域上多于一个站点,直接解绑
				 	unbindDeliveryArea(areaId, orgId);
				 }
			 },
			 error:function(XMLHttpRequest ,errMsg){
				 alert("网络连接失败");
			 }
		});

    }

	function unbindDeliveryArea(areaId, orgId){
		$.ajax({
			 dataType: 'json',
			 type : 'post',
			 url : "/delivery/unBindOrg",
			 data: {
				   id: areaId,
				 orgId: orgId
			 },
			 success : function(data){
				 if(data.success){
					 $("#unBindResMsg").html("<h4>解绑成功</h4>");
					 $("#showUnbindAreaRes").modal();
					 setTimeout(function(){
                        $("#showUnbindAreaRes").modal("hide");
                        window.location.href = "/org/list";
                      },2000);
				 }else {
					 $("#unBindResMsg").html("<h4>解绑失败</h4>");
                     $("#showUnbindAreaRes").modal();
				 }
			 },
			 error:function(XMLHttpRequest ,errMsg){
				 alert("网络连接失败");
			 }
		});
	}

	function SearchAreaByCity(cityId){
		myPOST('/delivery/getCityAreas', {cityId : cityId}, function (r) {
			  if (r.httpSuccess) {
					if(r.data && r.data.code == 0) {
						$(".js_bind_area_name").autocomplete({
							delay:100,
							source : r.data.data,
							focus: function( event, ui ) {
								$( ".js_bind_area_name" ).val( ui.item.value );
								$( ".js_bind_area_id" ).val( ui.item.id );
								return false;
							},
							select: function(event, ui) {
								$("#modelChangeTips").hide();
								$(".js_bind_area_name").val(ui.item.value );
								$(".js_bind_area_id").val(ui.item.id );
							},
							change: function(event, ui) {
								$("#modelChangeTips").hide();
								var cityName = $(".js_bind_area_name").val();
								if(validator.isBlank(cityName)) {
									$(".js_bind_area_id").val(0);
								}
							},
							open: function(event, ui) {
								$(".js_bind_area_id").val('0');
							}

						}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
							return $( "<li>" )
								.append( "<a>" + item.value + "</a>" )
								.appendTo( ul );
						};
						if(r.data.data) {
							var cityId = $(".js_bind_area_id").val();
							$.each(r.data.data, function(index, item) {

								if(cityId) {
								  if(cityId == item.id) {
									$(".js_bind_area_name").val(item.value);
								  }
								}
							});

						}
					} else {
						alert("获取城市配送区域信息失败，" + r.data.msg);
					}
				} else {
					//alert(JSON.stringify(r));
					// alert('获取组城市息失败，系统错误');
				}
        });
    }


});