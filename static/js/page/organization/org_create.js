require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/root'], function (t) {
    var commonOption = [];
    var poxyLevelType = [];
    var selfLevelType = [];
    $(document).ready(function(){
        $(".other_level_org").each(function() {
            commonOption.push($("<p>").append($(this).clone()).html());
        });

        $(".type_proxy").each(function() {
            poxyLevelType.push($("<p>").append($(this).clone()).html());
        });

        $(".type_self").each(function() {
            selfLevelType.push($("<p>").append($(this).clone()).html());
        });

        if ($('#orgType option:selected').val() != 2) {
            $('#isProxy option[value=1]').attr("selected","selected");
            $('#orgIntroduction').val("");
            $('#isProxy').attr('disabled', 'disabled');
            $('#orgIntroduction').attr('disabled', 'disabled');

            if ($('#orgType option:selected').val() == 1) {
                $("#div_level_type").show();
                $("#levelType").empty().append(selfLevelType.join(''));
                $('#levelType option[value=330]').attr("selected","selected");
                getOrgByLevelType(330);
            }
        } else {
            $("#div_level_type").show();
            $("#levelType").empty().append(poxyLevelType.join(''));
            $('#levelType option[value=220]').attr("selected","selected");
            getOrgByLevelType(220);
        }

        $('#formValidate1').submit(function () {
            if ($.trim($('#orgName').val()) == "") {
                alert("组织名称不能为空");
                return false;
            }

            if($("#levelType").val() == 230) {
                if ($.trim($('#adminNo').val()) != "" && $.trim($('#orgAdmin').val()) == "") {
                    alert("组织负责人不能为空");
                    return false;
                }
                if ($.trim($('#orgAdmin').val()) != "" && $.trim($('#adminNo').val()).length != 11) {
                    alert("手机号格式不正确");
                    return false;
                }
            } else {
                if ($.trim($('#orgAdmin').val()) == "") {
                    alert("组织负责人不能为空");
                    return false;
                }
                if ($.trim($('#adminNo').val()).length != 11) {
                    alert("手机号格式不正确");
                    return false;
                }
            }

            var parentId = $("#parentId").val();
            if (typeof parentId == 'undefined' || parentId == '' || parentId == 0) {
                alert("请选择上级组织");
                return false;
            } 

            var orgType = $("#orgType").val();
            var levelType = $("#levelType").val();
            if (typeof levelType == 'undefined' || levelType == '' || levelType == 0) {
                if (orgType ==1 || orgType == 2) {
                    alert("请选择组织层级");
                    return false;
                }
                $("#levelType").val(0);
            } else {
                if (orgType !=1 && orgType !=2) {
                    $("#levelType").val(0);
                }
            } 

        });

        initpage();
    });

function initpage() {
    $('#orgType').on('change', function(){
        if ($('#orgType option:selected').val() == 2) {
            $('#isProxy').removeAttr('disabled');
            $('#orgIntroduction').removeAttr('disabled');
            $('#isProxy option[value=0]').attr("selected","selected");

            $("#div_level_type").show();
            $("#levelType").empty().append(poxyLevelType.join(''));
            $('#levelType option[value=220]').attr("selected","selected");
            getOrgByLevelType(220);
        } else {
            $('#isProxy option[value=1]').attr("selected","selected");
            $('#orgIntroduction').val("");
            $('#isProxy').attr('disabled', 'disabled');
            $('#orgIntroduction').attr('disabled', 'disabled');

            if ($('#orgType option:selected').val() == 1) {
                $("#div_level_type").show();
                $("#levelType").empty().append(selfLevelType.join(''));
                $('#levelType option[value=330]').attr("selected","selected");
                getOrgByLevelType(330);
            } else {
                $("#div_level_type").hide();
                $("#parentId").empty().append(commonOption.join(''));
            }
        }
    });

$('#levelType').on('change', function(){
    var levelType = $("#levelType").val();
    getOrgByLevelType(levelType);
});
}

function getOrgByLevelType(levelType) {
    $.ajax({
        dataType: 'json',
        type : 'post',
        url : "/org/levelOrgList",
        data: {
            levelType : levelType
        },
        success : function(r){
            if (r && r.code == 0) {
                if (r.data) {
                    $("#parentId").empty();
                    $("#parentId").append("<option value='0'>请选择上级组织</option>");
                    $.each(r.data, function(index, value){
                        
                        $("#parentId").append("<option value='"+value.id+"'>"+value.value+"</option>");
                    });
                     
                } else {
                    $("#parentId").empty().append(commonOption.join(''));
                }      
            }
        }

    });
}

});

