$(document).ready(function(){
    initAddMerBtn();
    $('[data-placement="top"]').tooltip();
});

function initAddMerBtn() {
    $('#B_addMer').on('click', function(){
        var merId = $("#merchant").val().trim();
        var orgId = $("#orgId").val().trim();
        $("#alert_error").empty();
        if(merId == "") {
            $("#alert_error").append("<div class='alert alert-danger' role='alert'>商家POI不能为空</div>");
            return;
        }
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/org/merchantadd",
            data: {
                orgId: orgId,
                merId:merId
            },
            success : function(data){
                if(!data.result.success){
                    $("#alert_error").append("<div class='alert alert-danger' role='alert'>" + data.result.resultMsg + "</div>");
                }else {
                    window.location.reload();
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });
    });
}