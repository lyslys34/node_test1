/**
 * Created by lixiangyang on 15/8/18.
 */
var citySelect = $("#cityId");
$.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
    citySelect.select2({
        matcher: oldMatcher(matchPinyin)
    });
});