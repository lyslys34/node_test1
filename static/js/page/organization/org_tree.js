require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
});

require(['module/root','lib/jqueryPlugs/jquery.ztree.all-3.5','lib/jqueryPlugs/jquery.organStructrureTree', 'lib/juicer'], 
    function (r,tree,jquery_organStructrureTree){

    var curNodedata;
    var treeData;
    var popDialog;
    var Manage = {
        init:function(){
            //构造树形结构数据
            Manage._createTree();
            //新增下级节点，修改，转移，删除功能
            Manage._operateTree();
            //搜索功能
            Manage._searchAuthTree();
        },            
                       
        //构造树形结构数据
        _createTree:function(){
            //请求组织架构

            $.ajax({
                url: '/orgtree/list',
                type: 'POST',
                dataType: 'json'
            })
            .done(function(ret) {

                if(ret.code == 0){
                      //左边的组织架构树的展现，加注册事件
                      Manage._initTree(ret.data, $("#treeMenu"));
                      //右边的组织架构图的展现，加注册事件
                      $(".tree").organStructrureTree({
                          data:ret.data,
                          //点击节点展示当前下级节点
                          events:[Manage._clickCityNode]
                      });
                }
            })
            .fail(function() {
            })
            .always(function() {
            });
        },

        _operateTree:function(){

            //新增下级结构
            var addOrReviseUrl = "/orgtree/";
            $("#J-addNode").click(function(){

                var parentId = curNodedata.id;
                var parentType = curNodedata.type;
                var parentLevelType = curNodedata.levelType;
                if (parentLevelType == -1) {
                    parentId = curNodedata.parentId;
                };
                var url = addOrReviseUrl+"create?parentId="+parentId+"&parentType="+parentType+"&parentLevelType="+parentLevelType;
                window.location.href = url;
            });
            //修改
            $("#J-revise").click(function(){

                    var url = addOrReviseUrl+"edit?id="+curNodedata.id+"&type="+curNodedata.type+"&levelType="+curNodedata.levelType;
                    window.location.href = url;
            });
            //转移
            $("#J-move").click(function(){

                var parentNodes = Manage._getTopArray(treeData, curNodedata.type);
                if (parentNodes) {

                    if (popDialog) {
                        popDialog.remove();
                        popDialog = null;
                    }; 

                    var body = '<div style="padding-left:50px;padding-top:20px;"><h5>当前组织：' + curNodedata.name + '</h5><br>转移至：<select id="selectRegion" class="input-sm">';
                    for (var i = 0; i < parentNodes.length; i++) {
                        var oid = parentNodes[i].id;
                        var oname = parentNodes[i].name;

                        body = body + '<option value="' + oid + '">' + oname + '</option>';
                    };
                    body = body + '</select></div>';

                    var sbody = $(body);
                    var sel = $('select', sbody);

                    popDialog = r.ui.showModalDialog({
                        title: '组织转移', body: sbody, buttons: [

                            r.ui.createDialogButton('ok','确定', function () {     // 确认改派

                                $('option:selected', sel).each(function() {

                                    // alert($(this).val());
                                    Manage._moveOrg(curNodedata, $(this).val());
                                });
                                return true;
                            })
                        ],

                        style:{
                            ".modal-body":{"padding-top":"0"},
                            ".modal-footer":{"border-top":"none", "padding-top":"0"},
                            ".modal-dialog":{"top":"200px", "width":"420px"},
                            ".btn-success":{"width":"80px"}
                            }
                    });

                    popDialog.on("hidden.bs.modal", function(){
                        this.remove();
                        detailDia = null;
                    });
                }
            });
        },

        _moveOrg:function(moveNode, toID){

            //console.log('moveNode: ' + moveNode.name + 'to id ' + toID);

            $.ajax({
                url: '/orgtree/move',
                type: 'POST',
                dataType: 'json',
                data: {id: moveNode.id, parentId: toID}
            })
            .done(function (result) {
                console.log("success");
                if (result.code == 0) {
                    alert('成功');
                    window.location.reload();
                } else {
                   alert('失败:' + result.msg);
                }
            })
            .fail(function () {
                //console.log("error");
            })
            .always(function () {
                // console.log("complete");
            });

        },

        //构造数据结构
        _initTree:function(data, treeNode){
            //重新构造数据结构
            var districts = data;
            treeData = data;
            //封装了所有的节点数据
            var tree = [];
            Manage._addNodes(0, districts, tree);

            var setting = {
                data: {
                    simpleData: {  //是否采用简单数组的json格式
                        enable: true
                    }
                 },
                callback: {
                    onClick: Manage._showTrees
                }
            };
            $.fn.zTree.init($(treeNode), setting, tree);  //初始化左边组织架构树的展现和数据填充，以及事件绑定
        },    

        _addNodes:function(pid, nodeData, tree){

            for(var index = 0,lenindex = nodeData.length; index < lenindex; index++) {
                var node = nodeData[index];
                var children = node.children;
                var id = node.id;

                Manage._addUserNodes(pid, node, tree);

                tree.push({
                    id: id,
                    pId: pid,
                    name: node.name,
                    isRole: false,
                    trueId: node.id,
                    tlevel: node.level,
                    ownData:node,
                    nodeData: children
                });
                Manage._addNodes(id, children, tree);
            }
        },

        _addUserNodes:function(pid, nodeData, tree){
            if (nodeData['roleGroup']) {
                for (var i = 0; i < nodeData.roleGroup.length; i++) {
                    var role = nodeData.roleGroup[i];
                    var name = role.role.name + ': ';
                    for (var j = 0; j < role.users.length; j++) {
                        if (j != 0) {
                            name += ',';
                        };
                        name += role.users[j].name;
                    };
                    var nodeId = nodeData.id + '_user' + i;
                    tree.push({
                        id:nodeId,
                        pId:pid,
                        name:name,
                        isRole: true,
                        trueId: nodeData.id,
                        tlevel: nodeData.level,
                        ownData:nodeData,
                        nodeData: nodeData.children,
                        icon:'/static/imgs/tree/treePerson.png',
                    });
                };
            };
        },

        _showTrees:function(event, treeId, treeNode){
            //让右边的树形结构滚动条到最左边
            $(".inner-box1").scrollLeft(0);

            //重新组装节点数据（包括子节点）
            var nodeData = treeNode.nodeData;
            var name = treeNode.isRole?treeNode.ownData.name:treeNode.name;
            var data = {
                id:treeNode.trueId,
                pId:treeNode.pId,
                name:name,
                level: treeNode.tlevel,
                children:nodeData
            };
            //用数据填充右边的模板（组织架构图）
            $(".tree").organStructrureTree({
                    data:[data],
                    events:[Manage._clickCityNode]
            });

            curNodedata = treeNode.ownData;


            $('#J-nodeName').html(curNodedata.name);

            $('#J-addNode').addClass('hidden');
            $('#J-revise').addClass('hidden');
            $('#J-move').addClass('hidden');
            if (curNodedata.type === 1) {

                if (curNodedata.levelType == 310) {
                    $('#J-revise').removeClass('hidden');
                    $('#J-move').removeClass('hidden');
                }
                if (curNodedata.levelType === -1) {
                    $('#J-addNode').removeClass('hidden');
                }
                if (curNodedata.levelType === 305) {
                    $('#J-revise').removeClass('hidden');
                    $('#J-addNode').removeClass('hidden');
                }
            };
            if (curNodedata.type === 2) {
                if (curNodedata.levelType === 200) {
                    $('#J-addNode').removeClass('hidden');
                    $('#J-revise').removeClass('hidden');
                };
                if (curNodedata.levelType === -1) {
                    $('#J-addNode').removeClass('hidden');
                };
                if (curNodedata.levelType === 210) {
                    $('#J-revise').removeClass('hidden');
                    $('#J-move').removeClass('hidden');
                };
            };

            if (curNodedata.type === 4) {
                if (curNodedata.levelType === 400) {
                    $('#J-addNode').removeClass('hidden');
                    //$('#J-revise').removeClass('hidden');
                };
                if (curNodedata.levelType === -1) {
                    //$('#J-addNode').removeClass('hidden');
                };
                if (curNodedata.levelType === 420) {
                    $('#J-revise').removeClass('hidden');
                    //$('#J-move').removeClass('hidden');
                };
            };
        },

        //右边，组织架构图中的节点点击事件
        _clickCityNode:function(){
            $(".btn-node").click(function() {
                var id = $(this).attr("nodeid");
                var treeObj = $.fn.zTree.getZTreeObj("treeMenu");
                var treeNode = treeObj.getNodeByParam("id", id, null);
                // treeObj.expandNode(treeNode, true, false, true);

                var flag = treeObj.expandNode(treeNode, true, false, true);
                //如果是最后一个节点，没有子结点，就展开父节点
                if(!flag){
                    treeObj.expandNode(treeNode.getParentNode(), true, false, true);
                }

                var spanId = treeNode.tId + "_span";
                //模拟点击节点
                $("#" + spanId).click();
            });
        },

            //搜索功能
        searchNode :function(e, word) {
            var nodeList = [];
            var zTree = $.fn.zTree.getZTreeObj("treeMenu");
            var value = $.trim($("#J-searchAuthTree").val());
            if(word) value = word; 
            this.updateNodes(false, nodeList);
            nodeList = zTree.getNodesByParamFuzzy("name", value);
            this.updateNodes(true, nodeList);
        }, 

        updateNodes:function(highlight, nodeList) {
            var zTree = $.fn.zTree.getZTreeObj("treeMenu");
            for( var i=0, l=nodeList.length; i<l; i++) {
                var flag = zTree.expandNode(nodeList[i], highlight, false, true);
                //如果是最后一个节点，没有子结点，就展开父节点
                if(!flag){
                    zTree.expandNode(nodeList[i].getParentNode(), highlight, false, true);
                }
                nodeList[i].highlight = highlight;
                if(highlight){
                    $("#"+nodeList[i].tId+"_a").css({"color":"red"});
                }
                zTree.updateNode(nodeList[i]);
            }
        },

        _searchAuthTree:function(){
                var timeFlag;   //给700ms反应时间
                $("#J-searchAuthTree").on('input',function(e){
                    if(timeFlag){
                        clearTimeout(timeFlag);

                        //展开之前，先把红色的收起来
                        var zTree = $.fn.zTree.getZTreeObj("treeMenu"),
                            nodeList1 = zTree.transformToArray(zTree.getNodes());
                        zTree.expandAll(false);

                        for( var i=0, l=nodeList1.length; i<l; i++) {
                            $("#"+nodeList1[i].tId+"_a").css({"color":"#666"});
                        }
                    }
                    timeFlag = setTimeout(function(){
                        var word = $("#J-searchAuthTree").val().trim();
                        if(word.match(/[\w\u4e00-\u9fa5]/)){
                            Manage.searchNode(e, null);
                        }
                    },  700)
                });
        },

        _getTopArray:function(nodeData, type){

            for(var index = 0,lenindex = nodeData.length; index < lenindex; index++) {
                var node = nodeData[index];
                if (node.type == type && node.levelType != -1) {

                    return nodeData;
                }else
                {
                    var ret = Manage._getTopArray(node.children, type);
                    if (ret) {
                        return ret;
                    };
                }
            }
        }

    }

    Manage.init();

});