require.config({
    baseUrl: MT.STATIC_ROOT + '/js', urlArgs: 'ver=' + pageVersion
});

require(['module/root'], function (t) {

    $(document).ready(function () {

        //$("#employeeId").keydown(function (event) {
        //    if (event.keyCode == 8) {
        //        $('#mis-id').val("");
        //        $('#employeeId').val("");
        //        $('.mis-search').hide();
        //    }
        //});

        var lastTime;
        $(".panel-body").delegate(".js_mis_box", "keyup", function (event) {
            lastTime = event.timeStamp;
            var misId = $(this).val();
            var showResultTag = $(this).parent().children(".mis-search");
            setTimeout(function () {
                if (lastTime - event.timeStamp == 0) {
                    var date = new Date();
                    $.getJSON('/employee/query?q=' + misId).success(function (data) {
                        var city_list = new Array();
                        if (data.data.length == 0) {
                            return;
                        }
                        showResultTag.show();
                        var listTag = showResultTag.find('.mis-search-ul');
                        listTag.empty();
                        for (var i in data.data) {
                            var name = data.data[i].name;
                            var id = data.data[i].id;
                            var misId = data.data[i].login;
                            var realName = name ? name.replace('(' + misId + ')', '') : '';
                            listTag.append('<li onclick="add_to_input($(this))" class="js_mis_item" uid="' + id + '" misid="' + misId + '" uname="' + name + '" realname="' + realName + '" >' + name + '</li>');
                        }
                    }).error(function (data) {
                        alert.log('网络错误');
                    });
                }
            }, 300);
        });

        $(".panel-body").delegate(".js_mis_box", "focus", function(){
            $(this).val('');
            var showResultTag = $(this).parent().children(".mis-search");
            showResultTag.find(".js_employee_id").val('');
            showResultTag.find(".js_mis_id").val('');
            showResultTag.find(".js_user_name").val('');
        });

        add_to_input = function (e) {
            var searchBox = e.parents(".mis-search");
            searchBox.prev().val(e.text());
            searchBox.find(".js_employee_id").val(e.attr('uid'));
            searchBox.find(".js_mis_id").val(e.attr('misid'));
            searchBox.find(".js_user_name").val(e.attr('realname'));
            searchBox.hide();
        }


        $("#js_save").click(function () {
            submit();
        });


        function bindSubmit() {
            $("#js_save").on("click", function () {
                submit();
            }).removeAttr("disabled");
            $(".js_loading").toggleClass("hidden");
        }

        function unbindSubmit() {
            $("#js_save").off("click").attr("disabled", "disabled");
            $(".js_loading").toggleClass("hidden");
        }

        function submit() {
            var orgId = $("#orgId").val();
            var orgName = $('#orgName').val();
            var cityId = $("#cityId").val();

            var roleTagList = $(".js_role_list");
            var users = []
            if (roleTagList) {
                $.each(roleTagList, function (index, item) {
                    var roleCode = $(item).find(".js_role_code").val();
                    var userItems = $(item).find(".js_user_item");
                    if (userItems) {
                        $.each(userItems, function (index1, item1) {
                            var employeeId = $(item1).find(".js_employee_id").val();
                            var misId = $(item1).find(".js_mis_id").val();
                            var userName = $(item1).find(".js_user_name").val();
                            var phone = $(item1).find(".js_phone_box").val();
                            var user = {}
                            user.employeeId = employeeId;
                            user.bmRoleCode = roleCode;
                            user.mobile = phone;
                            user.misId = misId;
                            user.name = userName;
                            if (misId != '') {
                                users.push(user);
                            }
                        });
                    }
                });
            }

            if ($.trim(orgName) == "") {
                alert("组织名称不能为空");
                return false;
            }

            if (orgName.length > 30) {
                alert("组织名称太长");
                return false;
            }

            if (typeof cityId == 'undefined' || cityId == '' || cityId == 0) {
                alert("所在城市不能为空");
                return false;
            }

            var len = users.length;
            var misIds = [];
            for (var i = 0; i < len; i++) {
                var user = users[i];
                var misId = user['misId'];
                if (misId != "") {
                    if($.inArray(misId, misIds) > -1) {
                        alert(misId + '已经输入，请勿重复输入');
                        return false;
                    }
                    if ($.trim(user['mobile']) == '') {
                        alert('请填写' + misId + '的手机号码');
                        return false;
                    }
                    if (!/^1\d{10}$/.test($.trim(user['mobile']))) {
                        alert('请正确填写' + misId + '的手机号码');
                        return false;
                    }
                    misIds.push(misId);
                }
            }

            var data = {}
            data.orgId = orgId;
            data.orgName = orgName;
            data.cityId = cityId;
            data.users = users;

            unbindSubmit();
            $.ajax({
                dataType: 'json',
                contentType: 'application/json;charset=utf-8',
                type: 'post',
                url: "/orgtree/doedit",
                data: JSON.stringify(data),
                success: function (r) {
                    if (r) {
                        if (r.code == 0) {
                            var content = '保存成功';
                            _showPop({
                                type: 1,
                                content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + content + "</div>",
                                cancel: {
                                    display: true,
                                    name: "确定",
                                    callback: function () {
                                        window.location.reload();
                                    }
                                }
                            });
                            bindSubmit();
                        } else {
                            if (r.data && r.data != '') {
                                var errorMsg = '';
                                if (r.msg) {
                                    errorMsg = r.msg.replace(/;/g, '<br/>');
                                }
                                showTip('组织保存成功!<br/>' + errorMsg, 2);
                                bindSubmit();
                            } else {
                                showTip(r.msg, 2);
                                bindSubmit();
                            }
                        }
                    } else {
                        showTip("保存失败!", 2);
                        bindSubmit();
                    }
                },
                error: function () {
                    showTip("保存失败!网络异常!", 2);
                    bindSubmit();
                }
            });
        }

        var userAddItemHtml = $("#js_user_item_html").html();

        $(".panel-body").delegate(".js_add_user", "click", function () {
            $(this).after(userAddItemHtml);
        });

        $(".panel-body").delegate(".js_user_remove", "click", function () {
            $(this).parent().remove();
        });

        $("body").click(function () {
            $(".mis-search").hide();
        });

        function showTip(content, type) {
            _showPop({
                type: type,
                content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + content + "</div>"
            });
        }


    });

});
