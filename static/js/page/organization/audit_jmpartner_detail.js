require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/root'], function (t) {
    $(document).ready(function () {
        $('#reject').click(function () {
            var cmt = $.trim($('#comment').val());
            if (cmt == "") {
                alert("驳回原因不能为空");
                return false;
            }
            $("#formValidate3").submit();
        });
    });
});