/**
 * Created by lixiangyang on 15/8/18.
 */

var citySelect = $("#orgCity");
var orgSelect = $("#parentId");

$(document).ready(function () {

    $(".js_uploadbtn").click(function () {
        $(this).siblings(".js_fileupload").click();
    });

    $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
        citySelect.select2({
            matcher: oldMatcher(matchPinyin)
        });
        if(!isOrgManager){
            orgSelect.select2({
                matcher: oldMatcher(matchPinyin)
            });
        }

    });

    var uploadOption = {
        url: "/file/imgupload",
        add: function (e, data) {
            var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
            var maxFileSize = 5242880;
            if (data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                showTip("文件格式不正确(gif,jpg,png)", 2);
                return false;
            }
            if (data.originalFiles[0]['size'] && data.originalFiles[0]['size'] > maxFileSize) {
                showTip("文件太大(5M)", 2);
                return false;
            }
            data.context = $(this).siblings(".js_uploadbtn").addClass("disabled").text("正在上传。。。");
            data.submit();
        },
        done: function (e, data) {
            $(this).siblings(".js_uploadbtn").removeClass("disabled").text("重新上传");

            if (data.result) {
                if (data.result.code == 0) {
                    if (data.result.data[0] && data.result.data[0].code == 0) {
                        var backUrl = data.result.data[0].url;
                        if (typeof backUrl == undefined || backUrl == null || backUrl == '') {
                            showTip("上传失败，获取图片链接失败", 2);
                            return false;
                        }
                        $(this).siblings("img").css("display", "block").attr("src", backUrl);
                        $(this).siblings(".js_upload_back_url").val(backUrl);
                    } else {
                        showTip("上传失败," + (data.result.data[0] ? data.result.data[0].msg : "未知错误"), 2);
                    }
                } else {
                    showTip("上传失败，" + data.result.msg, 2);
                }
            } else {
                showTip("上传失败");
            }
        }
    };

    function submit() {
        var orgId = $("#orgId").val();
        var orgName = $("#orgName").val();
        var parentId = $("#parentId").val();
        var orgCity = $("#orgCity").val();
        var orgType = $("#orgType").val();
        var emergencyContact = $("#emergencyContact").val();
        var emergencyPhone = $("#emergencyPhone").val();
        var teamSize = $("#teamSize").val();
        var workingTime = $("#workingTime").val();
        var experienceType = $("input[name='experienceType']:checked").val();
        var hasInsurance = $("input[name='hasInsurance']:checked").val();
        var foodLicenseNumber = $("#foodLicenseNumber").val();
        var foodLicenseFile = $("#foodLicenseFile").val();
        var orgType = $("#orgType").val();
        var levelType = $("#levelType").val();
        var isProxy = $("#isProxy").val();

        if(isBlank(orgId) || orgId == 0) {
            showTip("组织不存在", 2);
            return false;
        }
        if(isBlank(orgName)) {
            showTip("请输入正确的加盟商名称", 2);
            return false;
        }
        if(isBlank(parentId) || parentId == 0) {
            showTip("请选择所属加盟城市", 2);
            return false;
        }
        if(isBlank(emergencyContact)) {
            showTip("请输入正确的紧急联系人", 2);
            return false;
        }
        if(isBlank(emergencyPhone) || !isMobile(emergencyPhone)) {
            showTip("请输入正确的紧急联系人电话", 2);
            return false;
        }
        if(isBlank(teamSize) || !isInteger(teamSize)) {
            showTip("请输入正确的团队规模", 2);
            return false;
        }
        if(isBlank(workingTime) || !isInteger(workingTime)) {
            showTip("请输入正确的从业时长", 2);
            return false;
        }
        if(isBlank(hasInsurance)) {
            showTip("请选择是否有骑手商业意外险", 2);
            return false;
        }

        var data = {}
        data.orgId = orgId;
        data.orgName = orgName;
        data.parentId = parentId;
        data.orgCity = orgCity;
        data.orgType = orgType;
        data.emergencyContact = emergencyContact;
        data.emergencyPhone = emergencyPhone;
        data.teamSize = teamSize;
        data.workingTime = workingTime;
        data.experienceType = experienceType;
        data.hasInsurance = hasInsurance;
        data.foodLicenseNumber = foodLicenseNumber;
        data.foodLicenseFile = foodLicenseFile;
        data.orgType = orgType;
        data.levelType = levelType;
        data.isProxy = isProxy;

        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/org/join/doUpdateOrg",
            data : data,
            success : function(r){
                if (r ) {
                    if(r.code == 0) {
                        window.location.reload();
                    } else {
                        showTip("修改加盟商失败:" + r.msg, 3);
                    }
                } else {
                    showTip("修改加盟商失败!", 3);
                }
            }
        });
    }

    function showTip(content, type, callback) {
        _showPop({
            type:type,
            content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + content + "</div>",
            cancel: {
                callback: callback
            }
        });
    }

    $("#js_save").click(function() {
        submit();
    });

    $(".js_fileupload").fileupload(uploadOption);

    $(".js_img_click_open").click(function(){
        window.open($(this).attr("src"));
    });
});



