
var citySelect = $("#orgCity");
var orgSelect = $("#parentId");

$(document).ready(function () {

    $(".js_uploadbtn").click(function () {
        $(this).siblings(".js_fileupload").click();
    });

    $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
        citySelect.select2({
            matcher: oldMatcher(matchPinyin)
        });
        orgSelect.select2({
            matcher: oldMatcher(matchPinyin)
        });
    });

    var uploadOption = {
        url: "/file/imgupload",
        add: function (e, data) {
            var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
            var maxFileSize = 5242880;
            if (data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                showTip("文件格式不正确(gif,jpg,png)", 2);
                return false;
            }
            if (data.originalFiles[0]['size'] && data.originalFiles[0]['size'] > maxFileSize) {
                showTip("文件太大(5M)", 2);
                return false;
            }
            data.context = $(this).siblings(".js_uploadbtn").addClass("disabled").text("正在上传。。。");
            data.submit();
        },
        done: function (e, data) {
            $(this).siblings(".js_uploadbtn").removeClass("disabled").text("重新上传");

            if (data.result) {
                if (data.result.code == 0) {
                    if (data.result.data[0] && data.result.data[0].code == 0) {
                        var backUrl = data.result.data[0].url;
                        if (typeof backUrl == undefined || backUrl == null || backUrl == '') {
                            showTip("上传失败，获取图片链接失败", 2);
                            return false;
                        }
                        $(this).siblings("img").css("display", "block").attr("src", backUrl);
                        $(this).siblings(".js_upload_back_url").val(backUrl);
                    } else {
                        showTip("上传失败," + (data.result.data[0] ? data.result.data[0].msg : "未知错误"), 2);
                    }
                } else {
                    showTip("上传失败，" + data.result.msg, 2);
                }
            } else {
                showTip("上传失败", 2);
            }
        }
    };

    function submit() {
        var orgName = $("#orgName").val();
        var parentId = $("#parentId").val();
        var orgCity = $("#orgCity").val();
        var isProxy = $("#isProxy").val();
        var emergencyContact = $("#emergencyContact").val();
        var emergencyPhone = $("#emergencyPhone").val();
        var teamSize = $("#teamSize").val();
        var workingTime = $("#workingTime").val();
        var experienceType = $("input[name='experienceType']:checked").val();
        var hasInsurance = $("input[name='hasInsurance']:checked").val();
        var foodLicenseNumber = $("#foodLicenseNumber").val();
        var foodLicenseFile = $("#foodLicenseFile").val();


        if(isBlank(orgName)) {
            showTip("请输入正确的加盟商名称", 2);
            return false;
        }
        if(isBlank(parentId) || parentId == 0) {
            showTip("请选择所属加盟城市", 2);
            return false;
        }
        if(isBlank(emergencyContact)) {
            showTip("请输入正确的紧急联系人", 2);
            return false;
        }
        if(isBlank(emergencyPhone) || !isMobile(emergencyPhone)) {
            showTip("请输入正确的紧急联系人电话", 2);
            return false;
        }
        if(isBlank(teamSize) || !isInteger(teamSize)) {
            showTip("请输入正确的团队规模", 2);
            return false;
        }
        if(isBlank(workingTime) || !isInteger(workingTime)) {
            showTip("请输入正确的从业时长", 2);
            return false;
        }
        if(isBlank(hasInsurance)) {
            showTip("请选择是否有骑手商业意外险", 2);
            return false;
        }


        var data = {}
        data.orgName = orgName;
        data.parentId = parentId;
        data.orgCity = orgCity;
        data.isProxy = isProxy;
        data.emergencyContact = emergencyContact;
        data.emergencyPhone = emergencyPhone;
        data.teamSize = teamSize;
        data.workingTime = workingTime;
        data.experienceType = experienceType;
        data.hasInsurance = hasInsurance;
        data.foodLicenseNumber = foodLicenseNumber;
        data.foodLicenseFile = foodLicenseFile;


        _showPop({
            type:1,
            content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + "点击“下一步”，加盟商信息进入审核流程，不可更改。待审核过后，方可修改" + "</div>",
            ok: {
                display: true,
                name: "下一步",
                callback: function(){
                    $.ajax({
                        dataType: 'json',
                        type : 'post',
                        url : "/org/join/doCreateOrg",
                        data : data,
                        success : function(r){
                            if (r) {
                                if(r.code == 0) {
                                    if(r.data && r.data.orgId) {
                                        window.location.href="/rider/goCreateOrgCharge?orgId=" + r.data.orgId;
                                    } else {
                                        alert("添加加盟商成功,请在人员管理中添加加盟商负责人");
                                    }
                                } else {
                                    alert(r.msg);
                                }
                            } else {
                                alert("添加加盟商失败!");
                            }
                        }
                    });
                }
            }
        });
    }

    function showTip(content, type) {
        _showPop({
            type:type,
            content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + content + "</div>"
        });
    }

    $("#js_save_and_next").click(function() {
        submit();
    });

    $(".js_fileupload").fileupload(uploadOption);

    $(".js_img_click_open").click(function(){
        window.open($(this).attr("src"));
    });
});



