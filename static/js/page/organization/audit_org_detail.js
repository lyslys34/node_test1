require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/root'], function (t) {
    $(document).ready(function(){
        var orgType = $("#orgType").val();
        if (orgType == 1 || orgType == 2) {
            var levelType = $("#levelType").val();
            if (levelType > 0) {
                getOrgByLevelType(levelType);
            }
            
        }
        
    });

    $('#formValidate1').submit(function () {
            $("#orgType").attr("disabled", false);
            $("#levelType").attr("disabled", false);
        });


function getOrgByLevelType(levelType) {
    $.ajax({
        dataType: 'json',
        type : 'post',
        url : "/org/levelOrgList",
        data: {
            levelType : levelType
        },
        success : function(r){
            if (r && r.code == 0) {
                if (r.data) {
                    var parentId = $("#parentId").val();
                    $("#parentId").empty();
                    $("#parentId").append("<option value='0'>请选择上级组织</option>");
                    $.each(r.data, function(index, value){
                        
                        if (parentId == value.id) {
                            $("#parentId").append("<option selected='selected' value='"+value.id+"'>"+value.value+"</option>");
                        } else {
                            $("#parentId").append("<option value='"+value.id+"'>"+value.value+"</option>");
                        }
                     
                    });
                     
                } else {
                    $("#parentId").empty();
                }      
            }
        }

    });
}

});

