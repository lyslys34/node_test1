require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});
require(['module/root'], function (t) {
    var commonOption = [];
    $(document).ready(function(){
        $(".other_level_org").each(function() {
            commonOption.push($("<p>").append($(this).clone()).html());
        });

        $(".type_proxy").each(function() {
            poxyLevelType.push($("<p>").append($(this).clone()).html());
        });

        $(".type_self").each(function() {
            selfLevelType.push($("<p>").append($(this).clone()).html());
        });

//        $(document).on("click","[name=modifyBusinessHours]",function(){
//            var parent = $(this).parent();
//            if(parent.next(".businessHourses").length>0){
//                //删除
//                $(this).parent().remove();
//            }else{
//                //添加一行营业时间
//                var appendHtml = parent.html();
//                appendHtml = $('<div class="businessHourses">'+appendHtml+'</div>');
//                appendHtml.find("input").val("");
//                parent.after(appendHtml);
//                $(this).find("i").removeClass("fa-plus").addClass("fa-minus");
//            }
//
//        });
        if($("#add").find(".opration-icon").length > 0){
            $("#add").bind("click",addBusinessHourses);
        }
        $("#delete").bind("click",deleteBusinessHourses);

        $("[name=businessHoursBegin],[name=businessHoursEnd]").blur(function(){
            var temp = $(this).val();
            var time = temp.trim().replace("：",":");
            $(this).val(time);
        });

        $("#phone").blur(function(){
            var temp = $(this).val().trim();
            $(this).val(temp);
        });


        $('#formValidate1').submit(function () {
            if ($.trim($('#orgName').val()) == "") {
                alert("组织名称不能为空");
                return false;
            }

            if($.trim($('#phone').val()) == ""){
                alert("联系电话不能为空");
                return false;
            }
            if(!isPhone($.trim($('#phone').val()))){
                alert("联系电话格式不正确");
                return false;
            }

            var validLevelTypeTypes = new Array(0,1,2,4,7);

            var parentId = $("[name=parentId]").val();
            if (typeof parentId == 'undefined' || parentId == '' || parentId == 0) {
                alert("请选择上级组织");
                return false;
            }

            var orgType = $("#orgType").val();
            var levelType = $("#levelType").val();
            if (typeof levelType == 'undefined' || levelType == '' || levelType == 0) {
                if (orgType ==1 || orgType == 2) {
                    alert("请选择组织层级");
                    return false;
                }
                $("#levelType").val(0);

            } else {
                if($.inArray(parseInt(orgType), validLevelTypeTypes) < 0) {
                    $("#levelType").val(0);
                }
            }

            $("#orgType").attr("disabled", false);
            $("#levelType").attr("disabled", false);

            //设置营业时间
            var businessHourses = getBuisnessHourses();
            if(!businessHourses){
                return false;
            }
            $("#businessHourses").val(businessHourses);
        });

        initpage();

        var levelType = $("#levelType").val();
        if (levelType > 0) {
            getOrgByLevelType(levelType);
        }
    });
function isPhone(str){
    var mobile = "^0?(13|15|18|14|17)[0-9]{9}$";
    var tel = "^[0-9\-()（）]{7,18}$";
    return new RegExp(mobile).test(str) || new RegExp(tel).test(str);
}

function addBusinessHourses(){
    $(this).parent().next().removeClass("hide");
    $("[name=businessHoursEnd]").eq(0).val("24:00").attr("disabled",true);
    $("[name=businessHoursBegin]").eq(1).val("00:00").attr("disabled",true);
    $("[name=businessHoursEnd]").eq(1).val("");
    $(this).find("i").removeClass("opration-icon");

    $(this).unbind();
}
function deleteBusinessHourses(){
    $(this).parent().addClass("hide");
    $("#add").bind("click",addBusinessHourses);
    $("#add").find("i").addClass("opration-icon");
    $("[name=businessHoursEnd]").eq(0).val("").attr("disabled",false).attr("readonly",false);
}

function getBuisnessHourses(){
    var businessHourses = "";
    var flag = true;
    $(".businessHourses").each(function(){
        if(!$(this).hasClass("hide")){
            var beginTime = $(this).find("[name=businessHoursBegin]").val();
            var endTime = $(this).find("[name=businessHoursEnd]").val();
            if(isTime(beginTime) && isTime(endTime) && compareTime(endTime,beginTime) == 1){
                businessHourses = businessHourses + "{beginTime:\"" +beginTime +"\",endTime:\""+endTime+"\"}";
            }else{
                alert("请设置正确的营业时间！");
                flag = false;
                return false;
            }
        }

    });
    if(businessHourses.length > 0 && flag){
        businessHourses = "["+businessHourses+"]";
        return businessHourses;
    }
    return false;
}

function isTime(str){
    var times = str.split(":");
    if(times.length ==2){
        if(times[0].length > 2 || times[1].length > 2){
            return false;
        }
        //时
        var numPatt = new RegExp("^\\d*$");
        if(!(numPatt.test(times[0]) && numPatt.test(times[1]))){
            return false;
        }
        var hour = parseInt(times[0]);
        var minute = parseInt(times[1]);
        if(hour>=0 && hour <= 24 && minute >=0 && minute <=59 && !(hour==24 && minute >0)){
            return true;
        }
    }
//    if(times.length == 3){
//        //时
//        var hour = parseInt(times[0]);
//        var minute = parseInt(times[1]);
//        var second = parseInt(times[2]);
//        if(hour>=0 && hour <= 24 && minute >= 0 && minute <=59 && second >=0 && second <=59){
//            return true;
//        }
//    }
    return false;
}

function compareTime(time1,time2){
    var time1Arr = time1.split(":");
    var time2Arr = time2.split(":");
    var i = 0;
    for( ; i < time1Arr.length && i < time2Arr.length; i++){
        if(parseInt(time1Arr[i]) > parseInt(time2Arr[i])){
            return 1;
        }
        if(parseInt(time1Arr[i]) < parseInt(time2Arr[i])){
            return -1;
        }
    }
    if(i == time1Arr.length && i == time2Arr.length){
        return 0;
    }
    if(i == time2Arr.length){
        return -1;
    }
    return 1;
}

function initpage() {
    $('#orgType').on('change', function(){
        if ($('#orgType option:selected').val() == 2) {
            $('#isProxy').removeAttr('disabled');
            $('#orgIntroduction').removeAttr('disabled');
            $('#isProxy option[value=0]').attr("selected","selected");

            $("#div_level_type").show();
        } else {
            $('#isProxy option[value=1]').attr("selected","selected");
            $('#orgIntroduction').val("");
            $('#isProxy').attr('disabled', 'disabled');
            $('#orgIntroduction').attr('disabled', 'disabled');

            if ($('#orgType option:selected').val() == 1) {
                $("#div_level_type").show();
            } else {
                $("#div_level_type").hide();
                $("#parentId").empty().append(commonOption.join(''));
            }
        }
    });

$('#levelType').on('change', function(){
    var levelType = $("#levelType").val();
    getOrgByLevelType(levelType);
});
}

function getOrgByLevelType(levelType) {
    $.ajax({
        dataType: 'json',
        type : 'post',
        url : "/org/levelOrgList",
        data: {
            levelType : levelType
        },
        success : function(r){
            if (r && r.code == 0) {
                if (r.data) {
                    var parentId = $("#parentId").val();
                    $("#parentId").empty();
                    $("#parentId").append("<option value='0'>请选择上级组织</option>");
                    $.each(r.data, function(index, value){

                        if (parentId == value.id) {
                            $("#parentId").append("<option selected='selected' value='"+value.id+"'>"+value.value+"</option>");
                        } else {
                            $("#parentId").append("<option value='"+value.id+"'>"+value.value+"</option>");
                        }

                    });

                } else {
                    $("#parentId").empty().append(commonOption.join(''));
                }
            }
        }

    });
}

});

