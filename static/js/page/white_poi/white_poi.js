require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root','module/cookie','module/ajaxManager','module/popmsg'], function (t,cookie,ajaxManager,popmsg) {
    var ajaxUrl = {
      getWhiteListViews:'/whitelist/getWhiteListViews',
      addNewWhiteList:'/whitelist/addNewWhiteList',
      updateWhiteListDescription:'/whitelist/updateWhiteListDescription',
      updateWhiteListNote:'/whitelist/updateWhiteListNote',
      batchInsertWhiteList:'/whitelist/batchInsertWhiteList',
      batchDeleteWhiteList:'/whitelist/batchDeleteWhiteList'
    }

    getWhiteList();

    $('#add').bind('click',function(){
        if($(this).hasClass('disabled')){
          alert('请先保存当前的白名单！')
          return;
        }
        $(this).addClass('disabled');
        $('#whiteListTable').find('tbody').append($('#whiteAddTmpl').html())
    })

    $('#whiteListTable').on('click','.add-num-btn',function(){
        if($(this).hasClass('disabled')){
          alert('请先保存当前的新增白名单！')
          return;
        }
        $(this).addClass('disabled');
        $(this).parents('td').append($('#addTmpl').html())
    })

    $('#whiteListTable').on('click','.del-num-btn',function(){
        if($(this).hasClass('disabled')){
          alert('请先保存当前的删除白名单！')
          return;
        }
        $(this).addClass('disabled');
        $(this).parents('td').append($('#delTmpl').html())
    })

    $('#whiteListTable').on('click','.add-cancel',function(){
        $(this).parents('td').find('.add-num-btn').removeClass('disabled');
        $(this).parent().remove();
    })

    $('#whiteListTable').on('click','.del-cancel',function(){
        $(this).parents('td').find('.del-num-btn').removeClass('disabled');
        $(this).parent().remove();
    })

    $('#whiteListTable').on('click','.fa-edit',function(){
      var $dom = $(this).parents('td');
      var $span = $dom.find('span').eq(0);
      $dom.html('<input type="text" value="'+$span.html()+'" class="'+$span.attr("data-type")+'" data-id="'+$span.attr('data-id')+'">');
      $dom.find('input').focus();
    })

    $(window).bind('keydown',function(e){
      if(13===e.keyCode){
        $('.type:focus,.description:focus').blur();
      }
    })

    $('body').on('click','.add-item-cancel',function(){
      $(this).parents('tr').remove();
      $('#add').removeClass('disabled');
    })

    $('body').on('click','.add-item-comfirm',function(){
      var $dom = $(this).parents('tr');
      var description = $.trim($dom.find('input').eq(0).val());
      var type = $.trim($dom.find('input').eq(1).val());
      var note = $.trim($dom.find('input').eq(2).val());
      var whiteIds = $.trim($dom.find('textarea').eq(0).val());
      var tips = '';
      tips = !description?'请输入白名单类别！':!type?'请输入白名单编号！':!whiteIds?'请输入白名单列表！':''
      if(!!tips){
        return alert(tips);
      }
      if(!type.match(/^[0-9]*[1-9][0-9]*$/)){
        return alert('保存失败，编号必须是正整数！')
      }
      var whiteListArr = whiteIds.split(',');
      var numFlag = true;
      var arr = whiteListArr.map(function(item){
        var itemVal = $.trim(item);
        var notNum = !itemVal.match(/^[0-9]*[1-9][0-9]*$/);
        if(notNum){
          numFlag = false;
        }
        return itemVal;
      })
      if(!numFlag){
        return alert('白名单列表格式有误，请输入正整数，并用英文逗号分割！')
      }
      ajaxManager.getServerData(ajaxUrl.addNewWhiteList,{description:description,whiteIds:arr.join(','),note:note,type:type},function(data){
        if(data.code){
          alert(data.msg||'保存失败！');
          return;
        }
        $('#add').removeClass('disabled');
        getWhiteList();
      },false)
    })

    $('body').on('blur','.type',function(){
      var val = $.trim($(this).val());
      if(!val){
        alert('请输入白名单类别！');
        $(this).focus();
        return;
      }
      var id = $(this).attr('data-id');
      ajaxManager.getServerData(ajaxUrl.updateWhiteListDescription,{type:id,description:val},function(data){
        if(data.code){
          alert('保存失败！');
          return;
        }
        popmsg('修改成功！');
        var $dom = $(this).parent();
        $dom.html('<span data-type="type" data-id="'+id+'">'+val+'</span> <a data-toggle="tooltip" title="" data-placement="top" href="javascript:;" name="detail" data-original-title="修改"><i class="fa fa-edit fa-lg opration-icon"></i></a>')
        $dom.find('a').tooltip();
      }.bind(this))
    })

    $('body').on('blur','.description',function(){
      var val = $.trim($(this).val());
      var id = $(this).attr('data-id');
      ajaxManager.getServerData(ajaxUrl.updateWhiteListNote,{type:id,note:val},function(data){
        if(data.code){
          alert('保存失败！');
          return;
        }
        popmsg('修改成功！');
        var $dom = $(this).parent();
        $dom.html('<span data-type="description" data-id="'+id+'">'+val+'</span> <a data-toggle="tooltip" title="" data-placement="top" href="javascript:;" name="detail" data-original-title="修改"><i class="fa fa-edit fa-lg opration-icon"></i></a>')
        $dom.find('a').tooltip();
      }.bind(this))
    })

    $('body').on('click','.add-comfirm',function(){
      var id = $(this).parents('tr').attr('data-id');
      var existWhitelist = $.trim($(this).parents('tr').find('.whitelist-wrap').html()).split(',');
      addOrDelWhitelist.call($(this).parent().find('input[type="text"]'),'add',id,existWhitelist);
    })

    $('body').on('click','.del-comfirm',function(){
      var id = $(this).parents('tr').attr('data-id');
      var existWhitelist = $.trim($(this).parents('tr').find('.whitelist-wrap').html()).split(',');
      addOrDelWhitelist.call($(this).parent().find('input[type="text"]'),'del',id,existWhitelist);
    })

    function getWhiteList(){
      ajaxManager.getServerData(ajaxUrl.getWhiteListViews,{},function(data){
        if(data.code){
          alert(data.msg||'获取白名单列表失败！');
          return;
        }
        $('#whiteListTable').find('tbody').html($('#tbodyTmpl').tmpl(data));
      },false)
    }

    function addOrDelWhitelist(type,id,existWhitelist){
      var whiteIds = $.trim($(this).val());
      var url = type=='add'?ajaxUrl.batchInsertWhiteList:ajaxUrl.batchDeleteWhiteList;
      var tip = type=='add'?'增加':'删除';
      if(!whiteIds){
        alert('请输入要'+tip+'的白名单列表！');
        return;
      }
      var whiteListArr = whiteIds.split(',');
      var numFlag = true;
      var existList = [],notExistList = [];
      var arr = whiteListArr.map(function(item){
        var itemVal = $.trim(item);
        var notNum = !itemVal.match(/^[0-9]*[1-9][0-9]*$/);
        if(existWhitelist.indexOf(itemVal)>=0){
          existList.push(itemVal);
        }else{
          notExistList.push(itemVal);
        }
        if(notNum){
          numFlag = false;
        }
        return itemVal;
      })
      if(!numFlag){
        return alert('白名单列表格式有误，请输入正整数，并用英文逗号分割！')
      }
/*      if(existList.length==existWhitelist.length){
        return alert('删除失败！不能删除所有白名单。');
      } */
      if(existList.length>0&&type=='add'){
        if(!confirm(existList.join(',')+'已经存在，是否确认添加？')){
          return;
        }
      }
      if(notExistList.length>0&&type=='del'){
        if(!confirm(notExistList.join(',')+'不存在，是否确认删除？')){
          return;
        }
      }
      ajaxManager.getServerData(url,{whiteIds:arr.join(','),type:id},function(data){
        if(data.code){
          alert(data.msg||tip+'白名单列表失败！');
          return;
        }
        popmsg('操作成功！');
        getWhiteList();
      },false)
    }
});