require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root','module/cookie'], function (t,cookie) {
    var ajaxUrl = {
      getWhiteListOpTrack:'/whitelist/getWhiteListOpTrack'
    };
    $.ajax({
        url:ajaxUrl.getWhiteListOpTrack,
        type:'post',
        dataType:'json',
        data:{type:getParameter('id')}
    }).done(function(data){
        if(data.code){
          alert(code.msg||'获取操作记录失败！');
          return;
        }
        data.data.map(function(item){
          item.opTime = new Date(item.opTime*1000).Format("yyyy-MM-dd hh:mm:ss");
        })
        $('#config_table').find('tbody').html($('#tbodyTmpl').tmpl(data))
    }).fail(function(err){
        console.log(err);
        alert(err.msg||'获取操作记录失败！');
    })

    function getParameter(key){
      var val = window.location.search.match(new RegExp("[?&]"+key+'=([^&]*)(&?)'));
      return val?decodeURIComponent(val[1]):'';
    }

    Date.prototype.Format = function (fmt) { 
        var o = {
            "M+": this.getMonth() + 1, //月份 
            "d+": this.getDate(), //日 
            "h+": this.getHours(), //小时 
            "m+": this.getMinutes(), //分 
            "s+": this.getSeconds(), //秒 
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
            "S": this.getMilliseconds() //毫秒 
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }
});