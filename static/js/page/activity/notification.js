/**
 * Created by zhouyi on 6/23/15.
 */

$("#upload-button").click(function() {
    $("#upload-input").click();
});

$("#upload-input").fileupload({
    url: "/activity/uploadImage",
    dataType: "json",
    done: function(e, r) {
        var response = r.jqXHR.responseJSON;
        if (response.code == 0) {
            var imageUrl = response.data;
            var image = $("#activity-image");
            $(image).attr("src", imageUrl);
            $("#push-form").children("input[name='imageUrl']").val(imageUrl)
            $(image).css("height", "120px");
            var top = (120 - $("#upload-button").height()) / 2;
            console.log(top);
            $("#upload-button").css("margin-top", top + "px");
        } else {
            alert(response.msg);
        }
    }
});

$("#push").click(function(e) {

    var activityName = $("#name").val();
    if (activityName.length  == 0) {
        alert("请输入活动名称");
        return;
    }

    var imageUrl = $("#push-form").children("input[name='imageUrl']").val();
    if (imageUrl.length == 0) {
        alert("请上传一张图片");
        return;
    }

    var jumpUrl = $("#url").val();

    var orgIds = $("#org_ids").val();
    console.log(orgIds);
    if (orgIds.length == 0) {
        alert("请输入要推送的组织ID");
        return;
    }

    var pushForm = $("#push-form");
    $(pushForm).append(
        "<input type=hidden name=activityName value=\"" + activityName + "\">" +
        "<input type=hidden name=jumpUrl value=\"" + jumpUrl + "\">" +
        "<input type=hidden name=orgIds value=\"" + orgIds + "\">"
    );

    $("#push").attr("disabled", "disabled");
    $(pushForm).submit();

});