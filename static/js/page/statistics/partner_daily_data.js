
require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu'
        ],
        'lib/autocomplete' : {
            deps : ['page/common']
        },
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/root', 'module/validator',  'module/cookie',  'lib/autocomplete'], function (t, validator, cookie, autocomplete) {
    var primary = getPrimaryForDownLoad();//下载的唯一标识
    var isExporting = false;//是否正在导出
    var detailDia;
    var detailRequest;
    $(document).delegate(".js_submit_btn", "click", function() {
        submit();
        showLoading('#submitLoading', true);
    });

    $(".js_export_btn").click(function(){
        exportData();
    });

    $(document).delegate(".js_page_btn", "click", function() {
        var pageNo = $(this).attr('value');
        $(".js_page_num").val(pageNo);
        submit();
    });

    $(document).keypress(function(e) {
        if(e.which == 13) {
            setPoiId();
            showLoading('#submitLoading', true);
            submit();
        }
    });

    $(document).delegate('.waybillid', 'click', function(event) {
        
        showWaybillDetail($(this).attr('value'));
    });

    window.onbeforeunload = function(){
        if(isExporting){
            return "正在导出数据";
        }
    }

    var nameMap = {};
    var idMap = {};
    var orgId = $(".js_orgId").val();

    function submit(){
        if(validate()) {

            $("#fm").submit();
        }
    }

    function validate() {
        var startTimeStr = $(".js_date_start").val();
        var endTimeStr = $(".js_date_end").val();
        var startTime = new Date(Date.parse(startTimeStr.replace(/-/g,   "/")));
        var endTime = new Date(Date.parse(endTimeStr.replace(/-/g,   "/")));
        var orderId = $(".js_order_id").val();
        var phone = $(".js_phone").val();

        if(!validator.isBlank(phone)) {
            if(!validator.isMobile(phone)) {
                alert("请输入正确的手机号");
                return false;
            }
        }
        if(endTime - startTime > 30*24*60*60*1000) {
            alert("日期间隔不能大于31天");
            return false;
        }
        if(startTime > endTime) {
            alert("开始时间不能晚于结束时间");
            return false;
        }

        return true;
    }

    var exportCheck;
    function exportData(){
        //按钮置灰
        $(".js_export_btn").val("正在导出，请稍后").addClass("disabled");
        var startTimeStr = $(".js_date_start").val();
        isExporting = true;
        var endTimeStr = $(".js_date_end").val();
        var orderId = $(".js_order_id").val();
        var phone = $(".js_phone").val();
        var riderId = $(".js_rider_id").val();
        var platformPoiId = $(".js_platformPoiId").val();
        var payType = $(".js_payType").val();
        var isOrder = $(".js_isOrder").val();
        var orgId1 = $(".js_orgId").val();
        var status = $(".js_status").val();
        var pkgType = $(".js_pkgType").val();
        var commentScore = $(".js_commentScore").val();
        $.ajax({
             url: "/partner/statistics/dailyDataExport?ts=" + startTimeStr + "&te=" + endTimeStr
                + "&platformOrderId=" + orderId
                + "&recipientPhone=" + phone
                + "&riderId=" + riderId
                + "&platformPoiId=" + platformPoiId
                + "&payType=" + payType
                + "&isOrder=" + isOrder
                + "&orgId=" + orgId1
                + "&status=" + status
                + "&pkgType=" + pkgType
                + "&commentScore=" + commentScore
                + "&primary=" + primary,
             async:true,
             dataType:'json',
             success: function(data){
                if(data != null){
                    if(data.code == 1){
                        alert(data.msg);
                        clearInterval(exportCheck);
                        $(".js_export_btn").val("导出").removeClass("disabled");
                        isExporting = false;
                        return false;
                    }
                }else{
                }
            },
            error:function(){
            }});
        exportCheck = self.setInterval(checkExport,20*1000);
    }

    function checkExport(){
        $.ajax({
            url:"/partner/statistics/checkExport",
            data:{primary:primary},
            dataType:'json',
            success:function(data){
                if(data!=null && data.code == 0 && !validator.isBlank(data.data)){
                    clearInterval(exportCheck);
                    $(".js_export_btn").val("导出").removeClass("disabled");
                    isExporting = false;
                    window.location.href=data.data;
                }
                if(data == null || data.code == 1){
                    alert("导出错误，请重试！");
                    clearInterval(exportCheck);
                    $(".js_export_btn").val("导出").removeClass("disabled");
                    isExporting = false;
                }
            },
            error:function(){
                alert("导出错误，请重试！");
                clearInterval(exportCheck);
                $(".js_export_btn").val("导出").removeClass("disabled");
                isExporting = false;
            }
        });
    }

    function exportValidate(){
        var startTimeStr = $(".js_date_start").val();
        var endTimeStr = $(".js_date_end").val();
        var orderId = $(".js_order_id").val();
        var phone = $(".js_phone").val();
        var riderId = $(".js_rider_id").val();
        var platformPoiId = $(".js_platformPoiId").val();
        var payType = $(".js_payType").val();
        var isOrder = $(".js_isOrder").val();
        var orgId1 = $(".js_orgId").val();
        var status = $(".js_status").val();
        var pkgType = $(".js_pkgType").val();
        var commentScore = $(".js_commentScore").val();
        var result = false;
        $.ajax({
             url: "/partner/statistics/total?ts=" + startTimeStr + "&te=" + endTimeStr
                + "&platformOrderId=" + orderId
                + "&recipientPhone=" + phone
                + "&riderId=" + riderId
                + "&platformPoiId=" + platformPoiId
                + "&payType=" + payType
                + "&isOrder=" + isOrder
                + "&orgId=" + orgId1
                + "&status=" + status
                + "&pkgType=" + pkgType
                + "&commentScore=" + commentScore,
             async:false,
             dataType:'json',
             success: function(data){
                if(data != null){
                    if(data.code == 0){
                        result = true;
                    }else{
                        alert(data.msg);
                    }
                }else{
                    alert("访问出错，请重试");
                }
            },
            error:function(){
              alert("访问出错");              
            }});
            return result;
    }

    function showWaybillDetail(waybillId)
    {
        if (detailDia) {
            detailDia.remove();
            detailDia = null;
        }; 
        if (detailRequest) {
          detailRequest.abort();
        };
        var body = '<div id="detailCon">这是ID＝' + waybillId+ '的订单详情</div>';
        detailRequest =  $.ajax({
             url: "/partner/waybill/detailV2.ajax?waybillId="+ waybillId, 
             success: function(result){

                body = '<div>';
                body = body + result + '</div>';
                detailDia = t.ui.showModalDialog({
                    title: '订单详情', body: body, buttons: [],
                    style:{
                        ".modal-footer":{"display":"none"},
                        ".modal-title":{"display":"none"},
                        ".modal-dialog":{"width":"900px"},
                        ".modal-body":{"padding-top":"0"},
                        ".modal-header .close":{"margin-right": "-8px","margin-top": "-10px"},
                        ".modal-header":{"border-bottom": "none"}
                        }
                });
                detailDia.on("hidden.bs.modal", function(){
                    this.remove();
                    detailDia = null;
                });
            },
            error:function(){
            }});
    }

    var riders = $( "#riderIdInput" ).autocomplete({
          source:function( request, response ) {
             var nameOrPhone = $.trim(request.term);
             var name = "";
             var phone = "";
             var orgId = $(".js_orgId").val();
             if(validator.isMobile(nameOrPhone)){
                phone = nameOrPhone;
             }else{
                name = nameOrPhone;
             }
             $.ajax({
               url: "/partner/statistics/riders",
               dataType: "json",
               data: {
                 name:name,
                 phone:phone,
                 orgId:orgId
               },
               success: function( data ) {
                 if(data.code == 0){
                    response( data.data );
                 }
               }
             });
          },
          search: function() {
             var term = this.value;
             term = $.trim(term);
             var numReg = "^[1-9]\\d*|0$";
             var isNum = new RegExp(numReg).test(term);
             if(isNum && !validator.isMobile(term)){
                return false;
             }
             var nameReg = "^[A-Za-z0-9_()（）\\-\\u4e00-\\u9fa5]+$";
             var isName = new RegExp(nameReg).test(term);
             if(term.length < 1 || (!validator.isMobile(term) && !isName)){
                return false;
             }
            showLoading('#riderLoading', true);
          },
          open: function(){
            showLoading('#riderLoading', false);
          },
          select: function( event, ui ) {
             $(".js_rider_id").val(ui.item.id);
          },
          change: function(event, ui){
             if(ui.item){
                $(".js_rider_id").val(ui.item.id);
             }else{
                var riderName = $("#riderIdInput").val();
                if(validator.isBlank(riderName)){
                    $(".js_rider_id").val(0);
                }else{
                    $(".js_rider_id").val(-1);
                }
             }
          }
        }).focus(function(event) {
            $('#riderIdInput').removeClass('invalid_value');
            $(this).autocomplete("search");
        }).blur(function(event) {
            showLoading('#riderLoading', false);
            var riderid = $(".js_rider_id").val();
            if (riderid == -1) {
              $('#riderIdInput').addClass('invalid_value');
            };

        }).data( "ui-autocomplete" );
        if(riders != null){
            riders._renderItem = function( ul, item ) {
                        return $( "<li>" )
                            .append( "<a>" + item.value + "</a>" )
                            .appendTo( ul );
                    };
        }
    var poiNames = $("#poiName").autocomplete({
        source: function( request, response ) {
             $.ajax({
               url: "/partner/statistics/pois",
               dataType: "json",
               data: {
                 name:$.trim(request.term)
               },
               success: function( data ) {
                 if(data.code == 0){
                    response( data.data );
                 }
               }
             });
          },
        search: function() {
             var term = this.value;
             term = $.trim(term);
             if(term.length < 1){
                return false;
             }
             showLoading('#poiNameLoading', true);
        },
        open: function(){
            showLoading('#poiNameLoading', false);
        },
        select: function( event, ui ) {
             $(".js_platformPoiId").val(ui.item.id);
        },
        change: function(event, ui){
             if(ui.item){
                $(".js_platformPoiId").val(ui.item.id);
             }else{
                setPoiId();
             }
        },
    }).focus(function(event) {
        $('#poiName').removeClass('invalid_value');
        $(this).autocomplete("search")
    }).blur(function(event) {
        showLoading('#poiNameLoading', false);
        var poiid = $(".js_platformPoiId").val();
        if (!validator.isBlank(poiid) && !validator.isNumber(poiid)) {
          setPoiNameInvalid();
        };
    }).data( "ui-autocomplete" );
    if(poiNames != null){
        poiNames._renderItem = function( ul, item ) {
                          return $( "<li>" )
                              .append( "<a>" + item.value + "</a>" )
                              .appendTo( ul );
                      };
    }

    function getPrimaryForDownLoad(){
        return userId + new Date().getTime();
    }

    function setPoiId()
    {
        var poiName = $("#poiName").val();
        poiName = $.trim(poiName);
        if(validator.isBlank(poiName)){
            $(".js_platformPoiId").val("");
        }else{
          var poiId = isPoiNameValid(poiName);
          if (poiId) {
              $(".js_platformPoiId").val(poiId);
          }else{
            $(".js_platformPoiId").val(poiName);
            setPoiNameInvalid();
          }
        }
    }

    function isPoiNameValid(name){

      if (validator.isNumber(name)) {
        return name;
      }else{
        var found = false;
        $(".poiLink").each(function(index, el) {
          if ($.trim($(this).text()) == name) {
            found = $(this).attr('value');
          };
        });
        return found;
      }
    }

    function showLoading(id, loading)
    {
      if (loading) {
        $(id).removeClass('hidden');
      }else{
        $(id).addClass('hidden');
      }
    }

    function setPoiNameInvalid(){
      $('#poiName').addClass('invalid_value');
    }

});