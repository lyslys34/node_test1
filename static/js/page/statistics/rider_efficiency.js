
require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
});

    require(['module/root', 'module/validator', 'lib/highcharts'], function (t, validator, highcharts) {

    $(document).delegate(".js_time_btn", "click", function() {
        var timeType = $(this).attr("data");
        $("#js_time_type").val(timeType);
        $(".js_time_btn").removeClass("active");
        showLoading();
        $(this).addClass("active");

        var exportBtn = $("#exportData");
        var exportUrl = exportBtn.attr('href');
        if(exportUrl.indexOf('?') > 0) {
            exportUrl = exportUrl.substr(0, exportUrl.indexOf('?')) + '?t=' + timeType;
            exportBtn.attr("href", exportUrl);
        }

        postAndChart();
    });




    var avgOption ={
        title: { text: '平均送达时间'},
        xAxis: { },
        yAxis: {
            title: { text: '时间(分)' },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        lang: {
            loading: '正在加载...'
        },
        noData: { },
        credits:{ enabled:false },
        tooltip: {
            enabled: true,
            formatter: function() {
                return '<b>'+ this.series.name +'</b><br>'+this.x +' : '+ this.y +'分';
            }
        },
        subtitle: {
            text: '单位(分钟)',
            x: 0
        },
        plotOptions: {
            line: {
                dataLabels: { enabled: true },
                enableMouseTracking: true
            }
        },
        loading:{
            labelStyle: {"font-size":"20px"},
            style : {"color":"#222222", "backgroundColor": "#e8e8ea"}
        }
    }

    var spreadOption =  {
        chart: { type: 'column' },
        title: { text: '配送时间分布' },
        xAxis: {  },
        yAxis: {
            min: 0,
            title: { text: '百分比' },
            stackLabels: {
                enabled: false,
                style: { fontWeight: 'bold', color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray' }
            },
            showEmpty:false
        },
        lang: {
            loading: '正在加载...'
        },
        legend: {
            reversed: true
        },
        tooltip: {
            formatter: function() {
                return '<b>'+ this.x +'</b><br>'+ this.series.name +'占比 : '+ percentNmber(this.y) + '%';
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false, color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                }
            }
        },
        loading:{
            labelStyle: {"font-size":"20px"},
            style : {"color":"#eeeeee", "backgroundColor": "#e8e8ea"}
        },
        credits:{ enabled:false },
        series: []
    }



    var avgSeries = [];
    var avgData = [];
    var timeX = [];
    var spreadSeries = [];
    var countLevel1 = [];
    var countLevel2 = [];
    var countLevel3 = [];
    var countLevel4 = [];
    var countLevel5 = [];
    var countLevel6 = [];



    $('#container').highcharts(avgOption);
    $('#container1').highcharts(spreadOption);
    var charts = $('#container').highcharts();
    var charts1 = $('#container1').highcharts();
    charts.showLoading();
    charts1.showLoading();



    postAndChart = function() {
        var timeType = $("#js_time_type").val();
        var orgId = $('#orgId').val();
        t.utils.post('efficiency.ajax', {t : timeType, orgId: orgId}, function (r) {
            if (r.httpSuccess) {
                if(r.data.code == 0) {
                    if(r.data.data) {
                        dataProcessing(r.data.data);
                        avgOption.series = avgSeries;
                        avgOption.xAxis.categories=timeX;

                        spreadOption.xAxis.categories=timeX;
                        spreadOption.series = spreadSeries;
                        $('#container').highcharts(avgOption);
                        $('#container1').highcharts(spreadOption);

                        $("#js_table").empty().append(aseembledTable(r.data.data));
                    } else {
                        alert("加载数据失败,数据错误");
                    }
                } else {
                    alert('加载数据失败,' + r.data.msg);
                    return false;
                }

            } else {
                alert('加载数据失败,' + r.data.msg);
                return false;
            }
        });
    }

    var showLoading = function() {
        $('#container').highcharts().showLoading();
        $('#container1').highcharts().showLoading();
    }


    dataProcessing = function(data) {
        avgData.length = 0;
        timeX.length = 0;
        countLevel1.length = 0;
        countLevel2.length = 0;
        countLevel3.length = 0;
        countLevel4.length = 0;
        countLevel5.length = 0;
        countLevel6.length = 0;
        $.each(data, function(index, item) {
            avgData.push(formatNumber(item.avgDeliveryTime));
            timeX.push(item.timeMD);
            countLevel1.push(item.pkgCountPercentLevel1);
            countLevel2.push(item.pkgCountPercentLevel2);
            countLevel3.push(item.pkgCountPercentLevel3);
            countLevel4.push(item.pkgCountPercentLevel4);
            countLevel5.push(item.pkgCountPercentLevel5);
            countLevel6.push(item.pkgCountPercentLevel6);
        });
        avgSeries.length = 0;
        spreadSeries.length = 0;
        avgSeries.push({name:"平均送达时间", data:avgData});
        spreadSeries.push({name:"预订>正负10分钟", data:countLevel6, color:'#6BC154'});
        spreadSeries.push({name:"预订≤正负10分钟", data:countLevel5, color:'#5282CE'});
        spreadSeries.push({name:">90分钟", data:countLevel4, color:'#FFC900'});
        spreadSeries.push({name:"60~90分钟", data:countLevel3, color:'#B4B4B4'});
        spreadSeries.push({name:"40~60分钟", data:countLevel2, color:'#FE8827'});
        spreadSeries.push({name:"0~40分钟", data:countLevel1, color:'#60ABDF'});
    }

    var aseembledTable = function(data) {
        var tableHtml = '';

        var sumAvgDeliveredTime = 0; //平均送达时间总和
        var sumPkgCountLevel1 = 0;
        var sumPkgCountLevel2 = 0;
        var sumPkgCountLevel3 = 0;
        var sumPkgCountLevel4 = 0;
        var sumPkgCountLevel5 = 0;
        var sumPkgCountLevel6 = 0;

        tableHtml += '<table class="table">';
        tableHtml += '  <tr>';
        tableHtml += '      <th>日期</th>';
        tableHtml += '      <th>平均送达时间(分钟)</th>';
        tableHtml += '      <th>0~40分钟订单数</th>';
        tableHtml += '      <th>40~60分钟订单数</th>';
        tableHtml += '      <th>60~90分钟订单数</th>';
        tableHtml += '      <th>&gt90分钟订单数</th>';
        tableHtml += '      <th>预订≤正负10分钟订单数</th>';
        tableHtml += '      <th>预订&gt正负10分钟订单数</th>';
        tableHtml += '  </tr>';
        if(data) {
            $.each(data, function(index, item) {
                sumAvgDeliveredTime += item.avgDeliveryTime;
                sumPkgCountLevel1 += item.pkgCountLevel1;
                sumPkgCountLevel2 += item.pkgCountLevel2;
                sumPkgCountLevel3 += item.pkgCountLevel3;
                sumPkgCountLevel4 += item.pkgCountLevel4;
                sumPkgCountLevel5 += item.pkgCountLevel5;
                sumPkgCountLevel6 += item.pkgCountLevel6;

                tableHtml += '  <tr ' + (index % 2 == 1 ? 'class="even"' : '') + '>';
                tableHtml += '      <td>' + item.timeYMD + '</td>';
                tableHtml += '      <td>' + formatNumber(item.avgDeliveryTime) + '</td>';
                tableHtml += '      <td>' + formatNumber(item.pkgCountLevel1) + '</td>';
                tableHtml += '      <td>' + formatNumber(item.pkgCountLevel2) + '</td>';
                tableHtml += '      <td>' + formatNumber(item.pkgCountLevel3) + '</td>';
                tableHtml += '      <td>' + formatNumber(item.pkgCountLevel4) + '</td>';
                tableHtml += '      <td>' + formatNumber(item.pkgCountLevel5) + '</td>';
                tableHtml += '      <td>' + formatNumber(item.pkgCountLevel6) + '</td>';
                tableHtml += '  </tr>';
            });

            tableHtml += '  <tr>';
            tableHtml += '      <td>平均</td>';
            tableHtml += '      <td>' + getFormatDivistion(sumAvgDeliveredTime, data.length) + '</td>';
            tableHtml += '      <td>' + getFormatDivistion(sumPkgCountLevel1, data.length) + '</td>';
            tableHtml += '      <td>' + getFormatDivistion(sumPkgCountLevel2, data.length) + '</td>';
            tableHtml += '      <td>' + getFormatDivistion(sumPkgCountLevel3, data.length) + '</td>';
            tableHtml += '      <td>' + getFormatDivistion(sumPkgCountLevel4, data.length) + '</td>';
            tableHtml += '      <td>' + getFormatDivistion(sumPkgCountLevel5, data.length) + '</td>';
            tableHtml += '      <td>' + getFormatDivistion(sumPkgCountLevel6, data.length) + '</td>';
            tableHtml += '  </tr>';

            tableHtml += '  <tr>';
            tableHtml += '      <td> 汇总 </td>';
            tableHtml += '      <td> - </td>';
            tableHtml += '      <td>' + formatNumber(sumPkgCountLevel1) + '</td>';
            tableHtml += '      <td>' + formatNumber(sumPkgCountLevel2) + '</td>';
            tableHtml += '      <td>' + formatNumber(sumPkgCountLevel3) + '</td>';
            tableHtml += '      <td>' + formatNumber(sumPkgCountLevel4) + '</td>';
            tableHtml += '      <td>' + formatNumber(sumPkgCountLevel5) + '</td>';
            tableHtml += '      <td>' + formatNumber(sumPkgCountLevel6) + '</td>';
            tableHtml += '  </tr>';

        }
        tableHtml += '</table>';

        return tableHtml;
    }

    var getFormatDivistion = function(x, y) {
        return y == 0 || x == 0 ? 0 : (x/y).toFixed(2) * 100 / 100;  //*100/100是为了去除后面的0
    }

    var formatNumber = function(src) {
        if(src) {
            return src.toFixed(2) * 100 / 100;
        }
        return 0;
    }

    var percentNmber = function(src){

        return formatNumber(src) * 100;
    }

    postAndChart();

});
