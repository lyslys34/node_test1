require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
});
require(['module/root','lib/highcharts'], function (t, highcharts) {
    var category = $('#category').val();
    //交易额总数
    var tradingVolume = $('#tradingVolume').val();
    //订单总数
    var orderCount = $('#orderCount').val();
    //订单均价
    var averageOrder = $('#averageOrder').val();
    //商家总数
    var poiCount = $('#poiCount').val();
    //人均交易额
    var tradingVolumePerPerson = $('#tradingVolumePerPerson').val();
    //人均订单
    var orderPerPerson = $('#orderPerPerson').val();

    var categoryArr = category.split(',');

    var tradingVolumeArr = tradingVolume.split(',');

    var orderCountArr = orderCount.split(',');

    var averageOrderArr = averageOrder.split(',');

    var poiCountArr = poiCount.split(',');

    var tradingVolumePerPersonArr = tradingVolumePerPerson.split(',');

    var orderPerPersonArr = orderPerPerson.split(',');

    var summaryOption = {
        chart: {
            renderTo: 'container'
        },
        title: {
            text: '交易额总数&订单总数'
        },
        xAxis: {
            categories: []
        },
        yAxis: {
            type: 'logarithmic',
            //minorTickInterval: 0,
            min:0.001,
            labels : {
                formatter : function () {
                    return this.value;
                }
            },
            minorTickInterval: 'auto'
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '{point.y}'
        },
        credits:{ enabled:false },
        series: []
    };



    var tradingVolumeSer = {
        name: '交易额总数',
        data: []
    };

    var orderCountSer = {
        name: '订单总数',
        data: []
    };

    var averageOrderSer = {
        name: '订单均价',
        data: []
    };

    var poiCountSer = {
        name: '商家总数',
        data: []
    };

    for (var d in tradingVolumeArr) {
        var num = parseFloat($.trim(tradingVolumeArr[d]));
        if (num == 0) {
            num = 0.001;
        }
        tradingVolumeSer.data.push(num);
    }

    for (var d in orderCountArr) {
        var num = parseFloat($.trim(orderCountArr[d]));
        if (num == 0) {
            num = 0.001;
        }
        orderCountSer.data.push(num);
    }

    for (var d in averageOrderArr) {
        var num = parseFloat($.trim(averageOrderArr[d]));
        if (num == 0) {
            num = 0.001;
        }
        averageOrderSer.data.push(num);
    }

    for (var d in poiCountArr) {
        var num = parseFloat($.trim(poiCountArr[d]));
        if (num == 0) {
            num = 0.001;
        }
        poiCountSer.data.push(num);
    }

    summaryOption.series.pointStart = 1;
    summaryOption.series.push(tradingVolumeSer);
    summaryOption.series.push(orderCountSer);
    summaryOption.series.push(averageOrderSer);
    summaryOption.series.push(poiCountSer);


    var averageOption = {
        chart: {
            renderTo: 'averageContainer'
        },
        title: {
            text: '人均交易额&人均订单数'
        },
        xAxis: {
            categories: []
        },
        yAxis: {
            type: 'logarithmic',
            //minorTickInterval: 0,
            //min:0,
            min:0.001,
            labels : {
                formatter : function () {
                    return this.value;
                }
            },
            minorTickInterval: 'auto'
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br />',
            pointFormat: '{point.y}'
        },
        credits:{ enabled:false },
        series: []
    };

    var tradingVolumePerPersonSer = {
        name: '人均交易额',
        data: []
    };

    var orderPerPersonSer = {
        name: '人均订单数',
        data: []
    };

    for (var d in tradingVolumePerPersonArr) {
        var num = parseFloat(tradingVolumePerPersonArr[d]);
        if (num == 0) {
            num = 0.001;
        }
        tradingVolumePerPersonSer.data.push(num);
    }

    for (var d in orderPerPersonArr) {
        var num = parseFloat(orderPerPersonArr[d]);
        if (num == 0) {
            num = 0.001;
        }
        orderPerPersonSer.data.push(num);
    }


    for (d in categoryArr) {
        summaryOption.xAxis.categories.push(categoryArr[d]);
        averageOption.xAxis.categories.push(categoryArr[d]);
    }

    averageOption.series.pointStart = 1;
    averageOption.series.push(tradingVolumePerPersonSer);
    averageOption.series.push(orderPerPersonSer);

    new Highcharts.Chart(summaryOption);
    new Highcharts.Chart(averageOption);


    $(document).delegate('input[name=timeBtn]', 'click', function () {
        window.location.href = "/partner/businessDataChart?timeType="+ $(this).attr('data-type') + "&poiId=" + $('#poiId option:selected').val()  + "&orgId=" + $('#orgId option:selected').val();
    });

    $(document).delegate('#excelReport', 'click', function () {
        var timeType = 1;
        $('input[name=timeBtn]').each(function(){
            if ($(this).hasClass('active')) {
                timeType = $(this).attr('data-type');
            }
        });
        window.location.href = "/partner/businessDataChart/excelReport?timeType="+ timeType + "&poiId=" + $('#poiId option:selected').val()  + "&orgId=" + $('#orgId option:selected').val();
    });

    $(document).delegate('#poiId', 'change', function () {
        var timeType = 1;
        $('input[name=timeBtn]').each(function(){
            if ($(this).hasClass('active')) {
                timeType = $(this).attr('data-type');
            }
        });
        window.location.href = "/partner/businessDataChart?timeType="+ timeType + "&poiId=" + $('#poiId option:selected').val()  + "&orgId=" + $('#orgId option:selected').val();
    });
});