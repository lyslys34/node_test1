

require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/bootstrap': {
      deps: ['lib/jquery']
    }
  }
});
require(['module/root', 'module/validator',  'module/cookie', 'page/common'], function (t, validator,cookie, common) {

    $(document).delegate(".js_submit_btn", "click", function() {
        submit();
    });

    $(document).delegate(".js_export_btn", "click", function() {
        exportData();
    });

    $(document).delegate(".js_page_btn", "click", function() {
        var pageNo = $(this).attr('value');
        $(".js_page_num").val(pageNo);
        submit();
    });

    $(document).keypress(function(e) {
        if(e.which == 13) {
            submit();
        }
    });


    function submit(){
        if(validate()) {
            $("#fm").submit();
        }
    }

    function validate() {
        var startTimeStr = $(".js_date_start").val();
        var endTimeStr = $(".js_date_end").val();
        var startTime = new Date(Date.parse(startTimeStr.replace(/-/g,   "/")));
        var endTime = new Date(Date.parse(endTimeStr.replace(/-/g,   "/")));

        if(endTime - startTime > 6*24*60*60*1000) {
            alert("日期间隔不能大于7天");
            return false;
        }
        if(startTime > endTime) {
            alert("开始时间不能晚于结束时间");
            return false;
        }

        return true;
    }

    function exportData(){
        var startTimeStr = $(".js_date_start").val();
        var endTimeStr = $(".js_date_end").val();
        if(validate()) {
            window.location.href="dailyDataExport?ts=" + startTimeStr + "&te=" + endTimeStr;
        }
    }


});