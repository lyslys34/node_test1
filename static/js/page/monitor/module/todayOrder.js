/*
 *当日订单
 */
define(function() {
    var charts;

    function renderBar(param) {
        if (!param) {
            renderBarHasData({
                "data": {
                    "countList": [0, 0, 0, 0, 0, 0, 0]
                }
            });
            return;
        }
        $.get(action_config["todayOrder"], param, function(data) {
            if (data.code == 0 && data['data'] && data['data']['data']) {
                renderBarHasData(data['data']['data']);
            }
        })
    }

    /**
     * 有数据之后描绘
     * @return {[type]} [description]
     */
    function renderBarHasData(data) {
        var yAxisCategories = ["已取消", "已完成", "已取餐", "已接单", "已派单", "已调度", "总订单"].reverse();
        var option = {
            chart: {
                type: 'bar',
                spacingTop: 50, //dom对图的padding-top
                spacingRight: 40,
                spacingLeft: 40
            },
            colors: ['rgb(28, 172, 225)'],
            title: {
                text: '',
                style: {
                    fontSize: '16px'
                }
            },
            yAxis: {
                labels: {
                    enabled: false
                },
                gridLineWidth: 0,
                title: {
                    text: null
                },
                stackLabels: { // 堆积的总数
                    enabled: true,
                    formatter: function() {
                        if (this.total > 0)
                            return this.total
                    },
                    style: {
                        fontWeight: 'normal',
                        textShadow: 'none',
                        color: '##666'
                    }
                }
            },
            xAxis: {
                min: 0,
                minPadding: 0.2,
                tickWidth: 0, // 不显示坐标的小线
                lineWidth: 0, // 坐标轴线
                categories: yAxisCategories
            },
            tooltip: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                bar: {
                    stacking: 'normal', // 是否堆积
                    dataLabels: { // 图形上的数字
                        enabled: false
                    },
                    maxPointLength:50,
                    pointWidth: 22,
                    pointPadding: 10
                }
            },
            series: [{
                data: data
            }]
        };
        $('#todayOrder').highcharts(option);
    }
    /**
     * 查看数据
     * @return {string} 解析后的字符串
     */
    function showData() {
        var data = $dom.data('dataShow');
        return tmpl(data);
    }
    return {
        name: 'todayOrder',
        refreshData: function(param) {
            renderBar(param);
        },
        showData: showData
    }
});