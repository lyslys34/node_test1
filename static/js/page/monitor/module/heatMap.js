/**
 * 热力地图
 * @author tzw
 * @Date(2015/12/8)
 */
define(['page/monitor/module/areaPoly', 'page/monitor/module/common'], function(areaPoly, common) {　
    var areaPoly = areaPoly;
    var map;
    var heatmap;
    var ajaxparam;
    var gradientRule;
    var isFillColor;
    var mapStyle = (localStorage.getItem('map-style') == "normal") ? "normal" : "blue_night";
    map = new AMap.Map($('#heatMap').get(0), {
        resizeEnable: true,
        mapStyle: mapStyle,
        zoom: (location.href.indexOf('cityId') == -1 || location.href.indexOf('cityId=-1') > 0) ? 4 : 11
    });
    var urlParam = getParam();
    urlParam['cityId'] != '-1' && map.setCity(urlParam['cityId']);
    //map.setCity("");
    map.plugin(['AMap.Geocoder'], function() {});
    map.plugin(['AMap.ToolBar'],
        function() {
            var tool = new AMap.ToolBar({
                direction: false
            });
            tool.hideLocation();
            map.addControl(tool);
        });
    map.plugin(['AMap.Scale'],
        function() {
            map.addControl(new AMap.Scale());
        });
    map.plugin(["AMap.Heatmap"], function() {
        //初始化heatmap对象
        heatmap = new AMap.Heatmap(map, {
            radius: 25, //给定半径
            opacity: [0, 1],
            gradient: {
                0.3: "blue",
                0.4: "#21bb0c",
                0.5: "#f5a700",
                0.6: "#ff7200",
                1.0: "#fd3023"
            }
        });
    });
    AMap.event.addListener(map, 'zoomend', function() {
        var polygons, show;
        if ($('.heatMap-wrap .nav-tabs li.active').index() == 0) return;
        show = (localStorage.getItem('map-poly') == '1') ? true : false;
        if (!show) return;
        polygons = map.getAllOverlays('polygon');

        // 放大到一定情况下才显示矩形
        if (map.getZoom() >= 10) {
            $.each(polygons, function(i, item) {
                item.show()
            })
        } else {
            $.each(polygons, function(i, item) {
                item.hide()
            })
        }
    });

    function getParam() {
        return common.getUrlParam();
        
    }
    // 地图style切换
    $('.map-change button').on('click', function(event) {
        var $self = $(this);
        if ($self.hasClass('active')) return;
        var style = $self.attr('data-style');
        $self.siblings(".active").removeClass('active').end().addClass("active");
        map.setMapStyle(style);
        localStorage.setItem('map-style', style);
    });
    // 显示区域切换
    $('.map-poly-control .poly-show').on('change', function() {
        var polygons = map.getAllOverlays('polygon');
        if ($(this).is(':checked')) {
            localStorage.setItem('map-poly', '1');
            polygons.length ? (map.getZoom() >= 10) && $.each(polygons, function(i, item) {
                item.show()
            }) : areaPoly.init(ajaxparam, map, gradientRule, isFillColor);
        } else {
            localStorage.setItem('map-poly', '0');
            $.each(polygons, function(i, item) {
                item.hide();
            });
        }
    });
    return {
        name: 'heatMap',
        refreshData: function(param, option, data) {
            map.clearMap();
            ajaxparam = param;
            gradientRule = option.gradientRule;
            isFillColor = !!option.isFillColor;
            emphasisItem = option.emphasisItem;
            setTimeout(function() {
                areaPoly.init(param, map, isFillColor, emphasisItem, option.isShow);
            }, 100);
            if (!data) {
                heatmap.setDataSet({
                    data: []
                });
                return;
            }
            // heatmap.hide();
            heatmap.setOptions({
                radius: 25, //给定半径
                opacity: [0, 1],
                gradient: option.gradientRule || {
                    0.3: "blue",
                    0.4: "#21bb0c",
                    0.5: "#f5a700",
                    0.6: "#ff7200",
                    1.0: "#fd3023"
                }
            });
            heatmap.setDataSet({
                data: data || [],
                max: option.max || 100
            });
            // setTimeout(function(){heatmap.show()},1000);
            // map.setFitView();

        }
    };　　
});