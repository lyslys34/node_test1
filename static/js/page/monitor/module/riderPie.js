define(['page/monitor/module/common'], function(common) {
    var charts;
    var text = window['isZb'] ? '在岗' : '工作中';
    var dataStyle = {
        normal: {
            label: {
                show: false
            },
            labelLine: {
                show: false
            }
        }
    };
    var placeHolderStyle = {
        normal: {
            color: 'rgba(0,0,0,0)',
            label: {
                show: false
            },
            labelLine: {
                show: false
            }
        },
        emphasis: {
            color: 'rgba(0,0,0,0)'
        }
    };
    /**
     * 请求数据并调用描绘
     * @param  {object} param 参数
     * @return 
     */
    function renderPie(param) {
        $.get(action_config["riderPie"], param, function(data) {
            if (data.code == 0) {
                var dataAfterDeal = prepareData(data['data']);
                renderPieHasData(dataAfterDeal);
            }
        })
    }
    /**
     * 有数据之后描绘
     * @return {[type]} [description]
     */
    function renderPieHasData(dataAfterDeal) {
        var option = {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                spacingTop: 10,
                plotShadow: false
            },
            colors: window['isZb'] ? ['rgb(28, 172, 225)', 'rgb(186, 230, 246)', 'rgb(186, 230, 246)'] : ['rgb(28, 172, 225)', 'rgb(119, 205, 237)', 'rgb(186, 230, 246)'],
            subtitle: {
                text: "",
                style: {
                    "color": "#777",
                    "fontSize": "16"
                }
            },
            title: {
                text: '共' + dataAfterDeal['duty'] + '人' + text + '<br>总骑手数' + dataAfterDeal['rider'] + '人',
                align: 'center',
                style: {
                    "color": "#777",
                    "fontSize": "12"
                },
                verticalAlign: 'middle',
                y: 10
            },
            tooltip: {
                enabled: false,
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                itemMarginBottom: 4,
                verticalAlign: 'top',
                borderWidth: 0,
                borderRadius: 5,
                floating: true, // float后不会影响到环形图
                y: 20,
                labelFormatter: function() {
                    return common.subNumber(this.percentage, 0, 2) + "%" + this.name + '(' + this.y + '人)';
                }
            },
            plotOptions: {
                pie: {
                    size: "76%",
                    dataLabels: {
                        enabled: false,
                    },
                    states: {
                        hover: {
                            halo: { //光环
                                size: 6
                            }
                        }
                    },
                    center: ['50%', '55%'],
                    showInLegend: true //显示legend
                }
            },
            series: [{
                type: 'pie',
                innerSize: '70%',
                point: {
                    events: {
                        mouseOver: function() {
                            this.series.chart.setTitle({
                                text: '共' + this.y + '人' + this.name + '<br>占比' + common.subNumber(this.percentage, 2) + "%"
                            })
                        },
                        mouseOut: function() {
                            this.series.chart.setTitle({
                                text: '共' + dataAfterDeal['duty'] + '人' + text + '<br>总骑手数' + dataAfterDeal['rider'] + '人'
                            })
                        }
                    }
                },
                data: dataAfterDeal['series']
            }]
        };
        $('#riderPie').highcharts(option);
    }
    /**
     * 准备option中series项
     * @param  {obj} data [ajax请求的数据]
     * @return {obj}      根据data格式化的数据
     */
    function prepareData(data) {
        var suffix = "占比及人数",
            start = 45,
            step = 20,
            series = [],
            legend = [],
            ratio, text;
        if (!data) return;
        for (var i = 0, j = data['data']; i < j.length; i++) {
            ratio = j[i]['ratio'];
            text = j[i]['text'];
            legend.push(text);
            series.push([text, j[i]['quota']]);
        }
        return {
            duty: data['duty'],
            rider: data['rider'],
            series: series,
            legend: legend
        };
    }

    return {
        name: 'riderPie',
        charts: charts,
        refreshData: function(param) {
            renderPie(param);
        }
    }
});