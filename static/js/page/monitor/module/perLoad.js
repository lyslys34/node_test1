/*
 *人均负载
 */
define(['page/monitor/module/common'], function(common) {
    var charts;
    var nameList = ["0:00", "0:15", "0:30", "0:45", "1:00", "1:15", "1:30", "1:45", "2:00", "2:15", "2:30", "2:45", "3:00", "3:15", "3:30", "3:45", "4:00", "4:15", "4:30", "4:45", "5:00", "5:15", "5:30", "5:45", "6:00", "6:15", "6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00", "8:15", "8:30", "8:45", "9:00", "9:15", "9:30", "9:45", "10:00", "10:15", "10:30", "10:45", "11:00", "11:15", "11:30", "11:45", "12:00", "12:15", "12:30", "12:45", "13:00", "13:15", "13:30", "13:45", "14:00", "14:15", "14:30", "14:45", "15:00", "15:15", "15:30", "15:45", "16:00", "16:15", "16:30", "16:45", "17:00", "17:15", "17:30", "17:45", "18:00", "18:15", "18:30", "18:45", "19:00", "19:15", "19:30", "19:45", "20:00", "20:15", "20:30", "20:45", "21:00", "21:15", "21:30", "21:45", "22:00", "22:15", "22:30", "22:45", "23:00", "23:15", "23:30", "23:45"];
    var colors = ["rgba(28, 172, 225, 0.25)", "rgba(28, 172, 225,0.4)", "rgba(28, 172, 225,0.55)", "rgba(28, 172, 225,0.7)", "rgba(28, 172, 225,0.85)", "rgba(28, 172, 225,1)"];

    function render(param) {
        $.get(action_config["perLoad"], param, function(data) {
            if (data.code == 0) {
                var dataAfterDeal = prepareData(data['data']);
                var option = {
                    title: {
                        text: ''
                    },
                    chart: {
                        spacingTop: 20, //dom对图的padding-top
                        spacingLeft: 44,
                        spacingRight: 44
                    },
                    colors: colors,
                    xAxis: {
                        categories: dataAfterDeal['xAxis'],
                        labels: {
                            enabled: true,
                        }
                    },
                    yAxis: {
                        opposite: true,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        align: 'center',
                        itemMarginBottom: 8,
                        verticalAlign: 'top',
                        fontWeight: "normal",
                        symbolWidth: 12,
                        symbolHeight: 12,
                        shadow: false
                    },
                    labels: {
                        items: [{
                            html: '当前状态',
                            style: {
                                left: 155,
                                top: 35,
                                color: "#666",
                                fontSize: "12px"
                            }
                        }]
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal', // 是否堆积
                            dataLabels: { // 图形上的数字
                                enabled: false
                            }
                        },
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            center: [160, 20],
                            size: 100,
                            showInLegend: false,
                            states: {
                                hover: {
                                    halo: { //光环
                                        size: 4
                                    }
                                }
                            },
                            dataLabels: {
                                enabled: true,
                                distance: 10,
                                format: '<b>{point.name}:</b>{point.y}',
                                style: {
                                    color: "#666",
                                    fontWeight: "normal",
                                    fontSize: "12px"
                                }
                            }
                        },
                        spline: {
                            lineWidth: 2,
                            lineColor: '#f8c833',
                            radius: 1.5,
                            color: '#f8c833',
                            dashStyle: 'solid',
                            fillColor: '#f8c833',
                            marker: {
                                enabled: false, //有没有标注点
                            }
                        }
                    },
                    tooltip: {
                        valueSuffix: '单',
                        hideDelay: 0,
                        backgroundColor: '#FCFFC5',
                        borderColor: '#f8c833',
                        formatter: function() {
                            if (this.points) {
                                // 人均负载放到第一个
                                var tmp = this.points.pop();
                                this.points.unshift(tmp);
                                var s = ['<span>' + this.x + '</span><br/>'];
                                $.each(this.points, function(i, point) {
                                    if (point.series.name == '人均负载') {
                                        s.push('<em style="width:4px;height:4px;display:block;background-color:' + point.series.color + '"></em><span>' + point.series.name + ': <b>' +
                                            this.point.id + '</b><br/></span>');
                                    } else {
                                        s.push('<em style="width:4px;height:4px;display:block;background-color:' + point.series.color + '"></em><span>' + point.series.name + ': <b>' +
                                            point.y + '</b><br/></span>');
                                    }

                                });
                                return s.join('');
                            } else if (this.point) { // 饼图
                                return '<em style="width:4px;height:4px;display:block;background-color:' + this.point.color + '"></em><b>当前爆单区域数</b><br/><span>' + this.point.name + ': <b>' +
                                    this.point.y + '个</b><br/></span>';
                            }

                        },
                        crosshairs: {
                            width: 2,
                            color: '#48B'
                        },
                        shared: true // 共享数据给别的series，因此在formatter中能遍历访问
                    },
                    series: dataAfterDeal['series']
                };
                $('#perLoad').highcharts(option);
            }
        })
    }
    /**
     * 准备option中series项
     * @param  {obj} data [ajax请求的数据]
     * @return {obj}      根据data格式化的seires和legend数据
     */
    function prepareData(data) {
        // 柱状图
        var columns = [];
        for (var i = 0; i < data['bar'].length; i++) {
            data['bar'][i].type = "column";
        }
        columns = data['bar'];

        // 饼图
        var pie = {
            type: 'pie',
            name: '',
            data: []
        };
        for (var i = 0; i < data['pie'].length; i++) {
            pie.data.push({
                name: data['pie'][i]['name'],
                y: data['pie'][i]['y'] || data['pie'][i]['value'],
            });
        }

        //曲线
        var spline = {
            name: '人均负载',
            type: 'spline',
            data: (function(data) {
                var arr = [];
                var id = "";
                var datay;
                var infin = 0;
                var dataline = data['line'];
                // 如果只有data，且data中某项数据为‘Infinity’时，显示的数据为最大值的2倍
                for (var i = 0; i < dataline['data'].length; i++) {
                    if (typeof(dataline['data'][i]) == "number") {
                        infin = infin > dataline['data'][i] ? infin : dataline['data'][i];
                    }
                }
                if(common.getUrlParam()['areaId'] != '-1'){
                    datay = dataline['data'];
                }else if(dataline['datashow'].length){
                    datay = dataline['datashow'];
                }else{
                    datay = dataline['data'];
                }
                for (var i = 0; i < data['timeLine'].length; i++) {
                    id = dataline['data'][i];
                    arr.push({
                        name: data['timeLine'][i],
                        id: (id == 'Infinity') ? '∞' : common.subNumber(dataline['data'][i], 2, 2),
                        y: typeof datay[i] == 'undefined' ? null : datay[i] == 'Infinity' ? infin * 2 : datay[i]
                    });
                }
                return arr;
            })(data)
        };

        //xAsix 
        var xAsix = data['timeLine'];

        return {
            series: columns.concat(spline).concat(pie),
            // legend: legend,
            xAxis: xAsix,
            yTipData: data['line']['data']
        }
    }

    return {
        refreshData: function(param) {
            render(param);
        },
        name: 'perLoad'
    }
});