/*
 * 在岗骑手数,超时单占比,人均负载,今日订单量整合
 */

define(['page/monitor/module/common'], function(common) {
    var View = function(name, ajaxNode, $dom, callback, step) {
        this.name = name;
        this.ajaxNode = ajaxNode;
        this.$dom = $dom;
        this.$parent = $dom.closest('.panel-stat3');
        this.callback = callback;
        this.step = step;
    };
    var Model = function(viewList, url, param) {
        this.viewList = viewList;
        this.url = url;
        this.param = param;
    };
    Model.prototype.getData = function() {
        var viewList = this.viewList;
        $.get(this.url, this.param, function(data) {
            var $tip = null,
                datas = null,
                strplit,
                $loadingOverlay = null;
            for (var i = 0, j = viewList.length; i < j; i++) {
                datas = data[viewList[i].ajaxNode];
                $tip = viewList[i].$parent.find('.m-left-xs');
                $loadingOverlay = viewList[i].$parent.find('.loading-overlay')
                if (datas && datas['code'] == 0) {
                    // 根据状态设置响应的背景颜色
                    datas['data']['status'] && common.setBgColor(viewList[i].$parent || viewList[i].$dom.parent(), datas['data']['status']);
                    //在岗骑手、订单数据增加千位分隔符
                    strplit = viewList[i].name == 'rider' || viewList[i].name == 'totalOrder' ? splitThousand(datas['data']['quota']) : datas['data']['quota'];
                    common.setAnimate(viewList[i].$dom, datas['data']['quota'], viewList[i].step, strplit);
                    $tip.text(datas['data']['quotaTips'] || "");
                    // 回调，用于monitor_com
                    typeof viewList[i].callback != 'undefined' && viewList[i].callback(viewList[i].name, $.extend(true, datas['data'], {
                        'quota': strplit
                    }));
                }
            }
        }).always(function() {
            $('.loading-overlay').removeClass('active');
        })
    };
    /**
     * 千位分隔符
     * @param  {number} num 
     * @return {string} 分割后的字符，如1234567->'1,234,567'
     */
    function splitThousand(num) {
        var str = (num + "").split('.')[0],
            arr = str.split('').reverse(),
            newarr = [];
        while (arr.length > 0) {
            newarr = newarr.concat(arr.splice(0, 3));
            newarr.push(',');
        }
        return newarr.reverse().join('').replace(/^,|,$/g, '') || '';
    }

    return {
        name: 'quota',
        refreshData: function(param, callback) {
            var quotaList = [];
            // 监控中心
            $('#riderCount').length && quotaList.push(new View('rider', 'rider', $('#riderCount'), callback));
            $('#timeoutCount').length && quotaList.push(new View('timeout', 'timeout', $('#timeoutCount'), callback, 10));
            $('#loadCount').length && quotaList.push(new View('perLoad', 'load', $('#loadCount'), callback, 100));
            $('#totalCount').length && quotaList.push(new View('totalOrder', 'waybill', $('#totalCount'), callback));

            // 众包不回调修改上方数据
            $('#zb-delivering').length && quotaList.push(new View('rider', 'delivering', $('#zb-delivering ')));
            $('#zb-timeoutCount').length && quotaList.push(new View('timeout', 'timeout', $('#zb-timeoutCount'), callback, 10));
            $('#zb-load').length && quotaList.push(new View('perLoad', 'load', $('#zb-load')));
            $('#zb-totaltoday').length && quotaList.push(new View('totalOrder', 'waybill', $('#zb-totaltoday')));

            var model = new Model(quotaList, action_config["quota"], param);
            model.getData();
        }
    }
});