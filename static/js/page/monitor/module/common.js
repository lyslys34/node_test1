/*
 * 动画和其它通用函数
 */
define(['page/monitor/module/url'], function(URL) {　
    var urlParam;
    return {
        name: 'common',
        // 数字跳转动画
        setAnimate: function($dom, num, n, strplit) {
            //数据不变的时候不刷新
            if ($dom.text() == num) return;
            var show = typeof n != 'undefined';
            //非数字的情况
            if (parseInt(num) != parseInt(num)) {
                $dom.text(num);
                return;
            }
            //支持数字倒序
            $({
                numberValue: parseInt($dom.text())
            }).animate({
                numberValue: num
            }, {
                duration: 1500,
                easing: 'linear',
                step: function() {
                    if (show) {
                        $dom.text(Math.ceil(this.numberValue * n) / n);
                    } else {
                        $dom.text(Math.ceil(this.numberValue));
                    }
                },
                complete: function() {
                    // 完成的时候赋值，否则因为四舍五入的问题会显示不准确
                    // 有些数据显示会有千位逗号的需求
                    strplit && (typeof strplit == 'string') && $dom.text(strplit);
                }
            });
        },
        // 改变dom的背景色
        setBgColor: function($dom, status) {
            var classname = $dom.get(0).className.replace(/bg-([a-zA-Z]+)/, "");
            var colorList = ['bg-blue', 'bg-yellow', 'bg-yellow', 'bg-red'];
            $dom.get(0).className = classname + " " + colorList[status - 1];
        },
        /**
         * 截取浮点型n位小数
         * @param  {number} num          要截取的浮点数
         * @param  {number} decimalCount 保留number位小数
         * @param  {number} type 转换类型[0, 1, 2]，默认为截取法[0]，分别代表[0]截取法,[1]进位法,[2]四舍五入法
         * @return {[type]}              [description]
         */
        subNumber: function(num, decimalCount, type) {
            if (!arguments[2]) {
                type = 0;
            }
            if (!num && num != 0) return;
            if (typeof num != 'number') return num;
            var str = num.toString();
            var arr = str.split('.');
            1 in arr && (arr[1] = arr[1].substring(0, decimalCount));
            var res = parseFloat(arr.join('.'));
            switch (type) {
                case 0:
                    return res.toString();
                case 1:
                    return (res + Math.pow(0.1, decimalCount)).toFixed(decimalCount).toString();
                case 2:
                    return num.toFixed(decimalCount).toString();
            }
        },
        /**
         * 将对象解析为对象字符串
         * @param  {object } obj  要解析的对象
         * @param  {string} split1 大分隔符,默认为&
         * @return        
         */
        parseObjToStr: function(obj, split1) {
            !split1 && (split1 = "&");
            var arr = [];
            for (var i in obj) {
                arr.push(i + "=" + obj[i]);
            }
            return arr.join(split1);
        },
        /**
         * 千位分隔符
         * @param  {number} num 
         * @return {string} 分割后的字符，如1234567->'1,234,567'
         */
        splitThousand: function(num) {
            var str = (num + "").split('.')[0],
                arr = str.split('').reverse(),
                newarr = [];
            while (arr.length > 0) {
                newarr = newarr.concat(arr.splice(0, 3));
                newarr.push(',');
            }
            return newarr.reverse().join('').replace(/^,|,$/g, '') || '';
        },
        /**
         * 获取地址中的链接属性
         * @return {object} 
         */
        getUrlParam: function() {
            if (urlParam) return urlParam;
            var isZb = location.href.indexOf('/monitor/zb/') > 0;
            var type = isZb ? "4" : (URL.getQueryString("type") || "-1");
            urlParam = {
                'type': type,
                'cityId': URL.getQueryString("cityId") || "-1",
                'areaId': URL.getQueryString("areaId") || "-1",
                'orgId': URL.getQueryString("orgId") || "-1"
            };
            return urlParam;
        }
    }
});