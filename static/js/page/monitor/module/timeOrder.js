/*
 *实时订单，众包今日单量分布图
 */
define(function() {
    var charts, title, legendData;

    function renderBar(param) {
        $.get(action_config["timeOrder"], param, function(data) {
            if (data.code == 0) {
                data['data'] && data['data'].length && renderBarHasData(data['data'].reverse());
            }
        })
    }
    /**
     * 有数据之后描绘
     * @return {[type]} [description]
     */
    function renderBarHasData(data) {
        var xAxisCategories = window['isZb'] ? ['总订单', '新订单', '已调度', '已派单', '已接单', '已取餐', '已完成', '已取消'] : ['新订单', '已调度', '已派单', '已接单', '已取餐'];
        var option = {
            chart: {
                type: 'column',
                // width: 350,
                spacingTop: 70, //dom对图的padding-top
                spacingLeft: 50,
                spacingRight: 50,
                zoomType: 'xy' //缩放的坐标
            },
            colors: ['rgb(255, 90, 90)', 'rgb(28, 172, 225)'],
            title: {
                text: '',
                style: {
                    color: '#777',
                    fontSize: '16px'
                }
            },
            xAxis: {
                tickWidth: 0, // 不显示坐标的小线
                lineWidth: 0, // 坐标轴线
                categories: xAxisCategories,
                labels: {
                    // autoRotation: 0                    
                }
            },
            yAxis: {
                min: 0,
                stackLabels: { // 堆积的总数
                    enabled: true,
                    style: {
                        textShadow: 'none',
                        fontWeight: 'normal',
                        color: '##666'
                    },
                    formatter: function() {
                        if (this.total > 0)
                            return this.total
                    }
                },
                labels: {
                    enabled: false
                },
                gridLineWidth: 0,
                title: {
                    text: null
                }
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                floating: true,
                itemMarginBottom: 4,
                // rtl: true,
                verticalAlign: 'top',
                x: -38,
                y: -50,
                borderWidth: 0,
                shadow: false,
                floating: true, // float后不会影响到环形图
            },
            tooltip: {
                enabled: true,
                backgroundColor: '#FCFFC5',
                borderColor: '#f8c833',
                formatter: function() {
                    var s = ['<span>' + this.x + '</span><br/>'];
                    $.each(this.points, function(i, point) {
                        s.push('<em style="width:4px;height:4px;display:block;background-color:' + point.series.color + '"></em><span>' + point.series.name + ': <b>' +
                            point.y + '</b><br/><span>');
                    });
                    return s.join('');
                },
                shared: true
            },
            plotOptions: {
                column: {
                    stacking: 'normal', // 是否堆积
                    dataLabels: { // 图形上的数字
                        enabled: false
                    },
                    pointPadding: 0,
                    pointWidth: 22
                }
            },
            series: data
        };
        $("#timeOrder").highcharts(option);
    }
    return {
        name: 'timeOrder',
        refreshData: function(param) {
            renderBar(param);
        }
    }
});