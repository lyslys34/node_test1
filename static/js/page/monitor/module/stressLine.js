/*
 * 进单量曲线图
 */
define(['page/monitor/module/common'], function(common) {
    var url = action_config["pressure"],
        $dom = $('#stressLine'),
        statusSeries = {}, //选择条件“昨天、上周、今天”需保留
        backseries = null,
        backseriesData = null;    
    /**
     * 根据数据画曲线图，没有参数的时候设置默认值
     * @param  {obj} series  数据
     * @param  {boolean} seriesData 缓存的数据
     * @param  {jquery object} $dom  展示的对象 
     * @return {[type]}         [description]
     */
    function renderPower(series, seriesData, $dom) {
        if (typeof series === 'undefined') {

            series = [{
                name: '上周',
                data: []
            }, {
                name: '昨日',
                data: []
            }, {
                name: '今天',
                data: []
            }];
        }

        $dom = $dom || $('#stressLine');
        backseries = series;
        backseriesData = seriesData;
        $dom.highcharts({
            chart: {
                borderWidth: 0,
                borderRadius: 5,
                backgroundColor: {
                    linearGradient: [0, 0, 100, 500],
                    stops: [
                        [0, 'rgb(255, 255, 255)'],
                        [1, 'rgb(255, 255, 255)']
                    ]
                },
                spacingTop: 50, //dom对图的padding-top
                zoomType: 'xy' //缩放的坐标
            },
            // 今天用蓝色#00abe4，昨天用绿色#99d465，上周用黄色f8c833
            colors: ['#99d465', '#f8c833', '#00abe4'],
            title: {
                text: '',
                style: {
                    color: '#777',
                    fontSize: '16px'
                },
                x: -20 //center
            },
            xAxis: {
                title: {
                    text: '时间'
                },
                categories: ["0:00", "0:15", "0:30", "0:45", "1:00", "1:15", "1:30", "1:45", "2:00", "2:15", "2:30", "2:45", "3:00", "3:15", "3:30", "3:45", "4:00", "4:15", "4:30", "4:45", "5:00", "5:15", "5:30", "5:45", "6:00", "6:15", "6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00", "8:15", "8:30", "8:45", "9:00", "9:15", "9:30", "9:45", "10:00", "10:15", "10:30", "10:45", "11:00", "11:15", "11:30", "11:45", "12:00", "12:15", "12:30", "12:45", "13:00", "13:15", "13:30", "13:45", "14:00", "14:15", "14:30", "14:45", "15:00", "15:15", "15:30", "15:45", "16:00", "16:15", "16:30", "16:45", "17:00", "17:15", "17:30", "17:45", "18:00", "18:15", "18:30", "18:45", "19:00", "19:15", "19:30", "19:45", "20:00", "20:15", "20:30", "20:45", "21:00", "21:15", "21:30", "21:45", "22:00", "22:15", "22:30", "22:45", "23:00", "23:15", "23:30", "23:45"]
            },
            yAxis: {
                title: {
                    text: '运力压力'
                },
                min: 0, // 定义最小值
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },

            plotOptions: {
                series: {
                    marker: {
                        radius: 3,
                        enabled: false, //有没有标注点
                        style: {
                            opacity: 0.2
                        },
                        symbol: 'circle' //标志点的图形 "circle", "square", "diamond", "triangle","triangle-down"，默认是"circle"
                    }
                },
                line: {
                    marker: {
                        states: {
                            hover: {
                                enabled: false
                            }
                        }
                    },
                    events: {
                        // 点击series的时候保存显示状态
                        legendItemClick: function() {
                            statusSeries[this.index + ""] = !this.visible;
                        }
                    }
                }
            },
            tooltip: {
                valueSuffix: '',
                hideDelay: 0,
                backgroundColor: '#FCFFC5',
                formatter: function() {
                    var s = ['<span>' + this.x + '</span><br/>'];
                    $.each(this.points, function(i, point) {
                        s.push('<em style="width:4px;height:4px;display:block;background-color:' + point.series.color + '"></em><span>' + point.series.name + ': <b>' +
                            common.subNumber(point.y, 2, 2) + '</b><br/><span>');
                    });
                    return s.join('');
                },
                crosshairs: {
                    width: 2,
                    color: '#48B'
                },
                shared: true // 共享数据给别的series，因此在formatter中能遍历访问
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            series: series
        });

        var chart = $dom.highcharts(),
            len = seriesData ? seriesData.length : 0;
        // 隐藏不显示的series
        if (typeof seriesData != 'undefined') {
            //如果状态已经保存，根据保存的结果做隐藏和显示
            if ('0' in statusSeries) {
                for (var i in statusSeries) {
                    statusSeries[i] ? chart.series[i].select() : chart.series[i].hide();
                }
            } else {
                $.each(seriesData, function(i, item) {
                    statusSeries[(len - i - 1) + ""] = true;
                    if (item['display'] == '0') {
                        chart.series[len - i - 1].hide();
                        //将显示状态改为false
                        statusSeries[(len - i - 1) + ""] = false;
                    }
                })
            }
        }
    }
    /**
     * 获取数据
     * @param  {object}   param    请求参数
     * @return {[type]}            [description]
     */
    function render(param) {
        $.ajax({
            url: url,
            data: param,
            success: function(data) {
                if (data.code != 0 || data['data'].length == 0) {
                    renderPower();
                    return;
                }
                var series = [],
                    seriesData = data['data'];
                $.each(seriesData, function(i, item) {
                    series.unshift({
                        'name': item['name'],
                        'display': item['display'],
                        'data': item['countList']
                    });
                });
                renderPower(series, seriesData);
            },
            error: function(err) {
                console.info(err)
            }
        });
    }
    /**
     * 查看数据
     * @return {string} 解析后的字符串
     */
    function showData() {
        var data = $dom.data('dataShow');
        var dataShowTpl = ['<h4 class="text-center">数据报表</h4>',
        '       <table class="table table-fixed">',
        '           <thead>',
        '             <tr>',
        '               <th class="col-xs-4">Firstname</th>',
        '               <th class="col-xs-4">Lastname</th>',
        '               <th class="col-xs-4">Email</th>',
        '             </tr>',
        '           </thead>',
        '           <tbody>',
        '             <% for(var i=0,i<series.length;i++){ %>',
        '             <tr>',
        '               <% for(var j=0,j<series.length;i++){ %>',
        '               <td class="col-xs-4">John</td>',
        '               <td class="col-xs-4">Doe</td>',
        '               <td class="col-xs-4">john@example.com</td>',
        '             </tr>',
        '             <% } %>',
        '           </tbody>',
        '       </table>'
    ].join("");
    var tmpl = _.template(tpl.modal_tpl);
        //retrun tmpl(data);
    }
    return {
        name: 'stressLine',
        refreshData: function(param) {
            render(param);
        },
        showData: showData
    }
});