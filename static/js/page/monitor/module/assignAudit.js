/*
 * 改价订单,指派任务
 */
define(function(require, exports, module) {
    var Quota = function(name, domSel, url, param, callback) {
        this.name = name;
        this.id = domSel.replace('#', '');
        this.url = url;
        this.param = param;
        this.callback = callback;
    };
    Quota.prototype.render = function() {
        var self = this,
            num = 0,
            txt;
        $.get(this.url, this.param, function(data) {
            if (data.code == 0 && data['data']['quota'] != '0') {
                num = parseInt(data['data']['quota']) || 0;
                if (!$("#" + self.id).length) {
                    $('.' + self.id + '-wrap').addClass('hascount').append("<span id='" + self.id + "' class='shortcut-alert'></span>");
                }
                txt = num > 999 ? "99+" : num + "";
                $("#" + self.id).text(txt).attr('title', data['data']['quota']);
            }

        });
    };

    return {
        name: 'assignAudit',
        refreshData: function(param) {
            var quotaList = [];
            $('#audit').length && quotaList.push(new Quota('audit', '#auditCount', action_config["audit"], param));
            $('#assign').length && quotaList.push(new Quota('assign', '#assignCount', action_config["assign"], param));

            $.each(quotaList, function(i, item) {
                item.render();
            })
        }
    }

});