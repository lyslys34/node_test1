/**
 * 在指定的地图上画区域
 * @author tzw
 * @Date(2015/12/9)
 */
define(['page/monitor/module/common'], function(common) {
    var colorList = ['#dee5d9', '#1cace2', '#f9d777', 'fc5c5e'];
    var isFirst = 1;
    var AreaPoly = function(param, map, isFillColor, emphasisItem, isShow) {
        this.param = param;
        this.map = map;
        this.isFillColor = isFillColor;
        this.emphasisItem = emphasisItem;
        this.isShow = isShow;
        this.init();
    }
    AreaPoly.prototype.init = function() {
        var self = this;
        this.getData(function(data) {
            self.render(data);
            setTimeout(function() {
                $('.heatMap-wrap .map-loading').hide();
            }, 300);
        });
    }
    AreaPoly.prototype.render = function(data) {
        var self = this;
        var map = self.map;
        var isShow = self.isShow || (localStorage.getItem('map-poly') == '1' && map.getZoom() >= 10) ? true : false;

        function getColorByPress(pressure) {
            if (pressure <= 0.3) return "blue";
            if (pressure <= 0.5) return "#21bb0c";
            if (pressure <= 0.8) return "#f5a700";
            if (pressure <= 1.0) return "#ff7200";
            if (pressure > 1.0) return "#fd3023";
        }
        $.each(data, function(i, item) {
            var polygonArr = []; //多边形覆盖物节点坐标数组
            var deliveryPressure = common.subNumber(item['deliveryPressure'], 2, 2);

            item.coordinate && $.each(JSON.parse(item.coordinate), function(index, el) {
                polygonArr.push([el.y, el.x]);
            });
            var polygon = new AMap.Polygon({
                path: polygonArr, //设置多边形边界路径
                strokeColor: "#6791E5", //线颜色
                strokeOpacity: 0.8, //线透明度
                strokeWeight: 3, //线宽
                fillColor: self.isFillColor ? getColorByPress(deliveryPressure) : "#7A8AA9", //填充色
                fillOpacity: deliveryPressure == '0.00' ? 0 : (self.isFillColor ? 0.9 : 0.1) //填充透明度

            });
            !isShow && polygon.hide();
            polygon.setMap(map);
            // map.setFitView();
            polygon.on('click', function(e) {
                //构建信息窗体中显示的内容
                var info = [];
                info.push("<div style='background-color:#FCFFC5;'><b style='margin-bottom:5px;'>" + item['name'] + "</b> ");
                info.push("<div style='padding:0px 0px 0px 4px;'>");
                info.push("<span style='" + (self.emphasisItem == "deliveryPressure" ? "color:#00abe4;" : "") + "'>运力压力: " + ((common.subNumber(item['deliveryPressure'], 2, 2) == "Infinity") ? "∞" : common.subNumber(item['deliveryPressure'], 2, 2)) + "单/人</span><br/>");
                info.push("<span style='" + (self.emphasisItem == "waybill" ? "color:#00abe4;" : "") + "'>积压单量: " + item['waybill'] + "单</span><br/>");
                info.push("<span style='" + (self.emphasisItem == "workingRider" ? "color:#00abe4;" : "") + "'>在岗骑手: " + item['workingRider'] + "人</span></div></div>");
                infoWindow = new AMap.InfoWindow({
                    content: info.join("") //使用默认信息窗体框样式，显示信息内容
                });
                //在指定位置打开信息窗体
                infoWindow.open(map, polygonArr[0]);
            })
        });
        isFirst && setTimeout(function() {
            map.setFitView();
        }, 0);
        isFirst = 0;
    }

    AreaPoly.prototype.getData = function(callback) {
        // coordinate: [{x:a,y:b}]
        // deliveryPressure: 0
        // name: null
        // waybill: 0
        // workingRider: 0
        $.ajax({
                url: action_config['coordinate'],
                data: this.param,
                dataType: 'json'
            })
            .done(function(data) {
                data.code == 0 && callback(data.data);
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {});
    }
    return {
        init: function(param, map, isFillColor, emphasisItem, isShow) {
            new AreaPoly(param, map, isFillColor, emphasisItem, isShow);
        }
    }
});