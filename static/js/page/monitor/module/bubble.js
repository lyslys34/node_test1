/*
 * 运动气泡图
 */
define(['page/monitor/module/common'], function(common) {
    var url = action_config["bubble"],
        $dom = $('#bubble');
    /**
     * 通过url获取响应的参数
     * @return {object} id对象
     */
    function getQueryCityOrOrg() {
        return common.getUrlParam();
    }
    /**
     * 点击非浮层的气泡时
     * @param  {string} id 气泡设置的id
     * @param  {string} name 城市，站点，区域，骑手名字
     * @return {[type]}    [description]
     */
    function changeCityOrOrg(id, name) {
        var cityAndOrg = getQueryCityOrOrg();
        var type = cityAndOrg['type'] || -1,
            cityId = cityAndOrg['cityId'],
            areaId = cityAndOrg['areaId'],
            orgId = cityAndOrg['orgId'];
        if (!cityId || cityId == '-1') {
            location.search = "?type=" + type + "&cityId=" + id;
        } else if (!areaId || areaId == '-1') {
            location.search = "?type=" + type + "&cityId=" + cityId + "&areaId=" + id;
        } else if (!orgId || orgId == '-1') {
            location.search = "?type=" + type + "&cityId=" + cityId + "&areaId=" + areaId + "&orgId=" + id;
        } else if (orgId && orgId != '-1') { // 骑手气泡跳到指派任务页面
            name && linkToDispatch(name);
        }
    }
    /**
     * 跳转到控制台
     * @param  {string} riderName 骑手名字
     * @return {} 
     */
    function linkToDispatch(riderName) {
        var cityAndOrg = getQueryCityOrOrg();
        var param;
        // 如果url中已经有站点参数
        if (cityAndOrg['orgId'] && cityAndOrg['orgId'] != '-1') {
            param = common.parseObjToStr(getQueryCityOrOrg())
        } else { //从气泡图一层一层点进去的时候，此时url中没有城市，区域，站点的参数
            param = common.parseObjToStr($dom.data("nowDatas")[$dom.data("nowDatas").length - 1]);
        }
        var dispatchLink = document.createElement('a');
        dispatchLink.href = (url_config.dispatch || url_config.admin) + '/partner/dispatch/home?' + param + "&riderName=" + escape(riderName || "") + "#100";
        dispatchLink.target = "_blank";
        dispatchLink.click();
    }
    /**
     * 返回上一级按钮
     * @return 
     */
    function back() {
        var $cover = $dom;
        $cover.data('nowDatas').pop();
        renderTo($cover.data('datas').pop());
        return false;
    }
    /**
     * 点击point时调用跳转描绘
     * @param  {obj} 跳转的城市，区域，站点id
     * @return 
     */
    function renderTo(obj) {
        var $cover = $dom;
        render({
            'type': getQueryCityOrOrg()['type'],
            'cityId': obj["cityId"],
            'areaId': obj["areaId"],
            'orgId': obj["orgId"]
        }, function(series) {
            renderBubble(series, true);
            $cover.data('datas').length > 0 && !$dom.find('.btn-back').length && $cover.append("<a class='btn btn-default btn-back'><i class='fa fa-reply fa-lg'></i> 返回</a>");
            // 返回
            $dom.find('.btn-back').on('click', back);
        });
    }
    /**
     * 点击point时事件
     * @param  {string} id 点击气泡图上面的某个点的id
     * @param  {string} name 城市，站点，区域，骑手名字
     * @return 
     */
    function changeCityOrOrgCover(id, name) {
        // datas是回退的时候使用的数据，回退的时候从datas中取
        // nowDatas 是记录点击后进入的数据，描绘的时候从nowDatas中取
        var cityOrg,
            $cover = $dom,
            cityAndOrg = getQueryCityOrOrg();
        var cityId = cityAndOrg['cityId'] || '-1',
            areaId = cityAndOrg['areaId'] || '-1',
            orgId = cityAndOrg['orgId'] || '-1';
        //第一次才缓存
        if (!$cover.data('datas')) {
            $cover.data("datas", []).data('nowDatas', []);
        }!$cover.data('nowDatas') && $cover.data('nowDatas', []);
        var nowDatas = $cover.data('nowDatas'),
            cityId = nowDatas.length ? nowDatas[nowDatas.length - 1]['cityId'] : cityId,
            areaId = nowDatas.length ? nowDatas[nowDatas.length - 1]['areaId'] : areaId,
            orgId = nowDatas.length ? nowDatas[nowDatas.length - 1]['orgId'] : orgId,
            cityIdNew = cityId,
            areaIdNew = areaId,
            orgIdNew = orgId;
        // v1.4修改，骑手也可以跳转到控制台
        // //已经到达最后一级
        // if (orgId != "-1") return false;
        if (orgId != "-1") {
            name && linkToDispatch(name);
        }

        $cover.data("datas").push({
            "cityId": cityId,
            "areaId": areaId,
            "orgId": orgId
        });
        if (id) {
            if (cityId == '-1') {
                cityIdNew = id;
                // 追加datas
                $cover.data("nowDatas").push({
                    "cityId": cityIdNew,
                    "areaId": '-1',
                    "orgId": '-1'
                });
            } else if (areaId == '-1') {
                areaIdNew = id;
                // 追加datas
                $cover.data("nowDatas").push({
                    "cityId": cityIdNew,
                    "areaId": areaIdNew,
                    "orgId": '-1'
                });
            } else if (orgId == '-1') {
                orgIdNew = id;
                // 追加datas
                $cover.data("nowDatas").push({
                    "cityId": cityIdNew,
                    "areaId": areaIdNew,
                    "orgId": orgIdNew
                });
            } else {
                return;
            }
        }

        renderTo({
            "cityId": cityIdNew,
            "areaId": areaIdNew,
            "orgId": orgIdNew
        });
    }
    /**
     * 根据数据画气泡
     * @param  {obj} series  数据
     * @param  {jquery object} $dom  展示的对象 
     * @return {[type]}         [description]
     */
    function renderBubble(series) {
        var cursor,
            nowDatas,
            xAxisText = window['isZb'] ? "运力压力" : "人均负载";
        if (typeof series === 'undefined') {
            series = [{
                data: []
            }];
        }

        var option = {
            chart: {
                borderWidth: 0,
                borderRadius: 5,
                type: 'bubble',
                backgroundColor: {
                    linearGradient: [0, 0, 100, 500],
                    stops: [
                        [0, 'rgb(255, 255, 255)'],
                        [1, 'rgb(255, 255, 255)']
                    ]
                },
                spacingTop: 50, //dom对图的padding-top
                spacingRight: 20,
                zoomType: 'xy' //zoom的对象
            },
            title: {
                text: ""
            },
            xAxis: {
                title: {
                    enabled: true,
                    text: xAxisText
                },
                startOnTick: true,
                endOnTick: true,
                showLastLabel: true,
                plotLines: [{
                    color: '#000',
                    value: 0
                }]
            },
            yAxis: {
                title: {
                    text: '超时单占比（%）'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                backgroundColor: '#FCFFC5',
                hideDelay: 0
            },
            plotOptions: {
                bubble: {
                    tooltip: {
                        hideDelay: 0, //鼠标离开后延迟时间
                        headerFormat: '<b>{series.name}</b><br><br>',
                        pointFormat: '<p>' + xAxisText + '：</p><b>{point.x}</b> <br><p>超时占比：</p><b>{point.y}%</b><br><p>今日单量：</p><b>{point.z}</b>'
                    },
                    marker: {
                        enabled: true,
                        fillColor: "rgb(28, 172, 225)", //标志点的颜色
                        lineColor: "#FFFFFF",
                        states: {
                            hover: {
                                lineColor: 'white'
                            }
                        },
                        symbol: 'circle'
                    }
                },
                series: {
                    cursor: "pointer",
                    point: {
                        events: {
                            click: function(e) {
                                if (this.series.name == '未接单') return;
                                //cover打开的时候另行处理
                                $dom.closest('.modal').length ?
                                    changeCityOrOrgCover(this.series.dataId, this.series.name) :
                                    changeCityOrOrg(this.series.dataId, this.series.name);
                            },
                            select: function() {
                                //选中数据点时 不改变数据点内部颜色值
                            }
                        }
                    }
                }
            },
            series: series
        };
        $dom.highcharts(option);
        var chart = $dom.highcharts();
        len = series ? series.length : 0;
        typeof series != 'undefined' && $.each(series, function(i, item) {
            if (item['dataId']) {
                chart.series[i].dataId = item['dataId'];
            }
        });
    }
    /**
     * 获取数据
     * @param  {object}   param    请求参数
     * @param  {Function} callback 回调函数
     * @return {[type]}            [description]
     */
    function render(param, callback) {
        return $.ajax({
            url: url,
            data: param,
            success: function(data) {
                if (data.code != 0 || data['data'].length == 0) {
                    callback();
                    return;
                }
                callback.call(null, prepareData(data['data']));
            },
            error: function(err) {
                console.info(err)
            }
        });
    }

    function prepareData(seriesData) {
        var series = [];
        $.each(seriesData, function(i, item) {
            //超时订单的统计含”未接单“，骑手层级 需要展示“未接单”的气泡，坐标（0,100%）
            try {
                series.push({
                    name: item['name'],
                    dataId: item['id'],
                    sizeBy: 'area',
                    data: [
                        [Number(common.subNumber(item['argWaybillLoad'], 2, 2)), item['timeoutWaybillRatio'], item['waybillCount']]
                    ]
                });
            } catch (e) {}
        });
        return series;
    }
    /**
     * 查看数据
     * @return {string} 解析后的字符串
     */
    function showData() {
        var data = $dom.data('dataShow');
        return tmpl(data);
    }
    return {
        name: 'bubble',
        //刷新请求数据描绘
        refreshData: function(param) {
            $dom.data('datas', null)
            render(param, renderBubble).then(function(data) {
                $dom.data('minShowData', prepareData(data['data']));
            });
        },
        renderWithData: function(series){
            $dom.data('datas', null);
            renderBubble(series)
        },
        showData: showData
    }
});