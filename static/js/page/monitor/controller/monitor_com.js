(function() {
    var root = this,
        Temp = {};
    var escapes = {
            "'": "'",
            '\\': '\\',
            '\r': 'r',
            '\n': 'n',
            '\t': 't',
            '\u2028': 'u2028',
            '\u2029': 'u2029'
        },
        escaper = /\\|'|\r|\n|\t|\u2028|\u2029/g;
    // Save bytes in the minified (but not gzipped) version:
    var ArrayProto = Array.prototype,
        ObjProto = Object.prototype,
        FuncProto = Function.prototype;
    var nativeKeys = Object.keys,
        nativeForEach = ArrayProto.forEach;
    // Create quick reference variables for speed access to core prototypes.
    var push = ArrayProto.push,
        slice = ArrayProto.slice,
        concat = ArrayProto.concat,
        toString = ObjProto.toString,
        hasOwnProperty = ObjProto.hasOwnProperty;

    Temp.has = function(obj, key) {
        return hasOwnProperty.call(obj, key);
    };
    Temp.each = Temp.forEach = function(obj, iterator, context) {
        if (obj == null) return;
        if (nativeForEach && obj.forEach === nativeForEach) {
            obj.forEach(iterator, context);
        } else if (obj.length === +obj.length) {
            for (var i = 0, l = obj.length; i < l; i++) {
                if (iterator.call(context, obj[i], i, obj) === breaker) return;
            }
        } else {
            for (var key in obj) {
                if (Temp.has(obj, key)) {
                    if (iterator.call(context, obj[key], key, obj) === breaker) return;
                }
            }
        }
    };
    // Fill in a given object with default properties.
    Temp.defaults = function(obj) {
        Temp.each(slice.call(arguments, 1), function(source) {
            if (source) {
                for (var prop in source) {
                    if (obj[prop] == null) obj[prop] = source[prop];
                }
            }
        });
        return obj;
    };
    // Invert the keys and values of an object. The values must be serializable.
    Temp.invert = function(obj) {
        var result = {};
        for (var key in obj)
            if (Temp.has(obj, key)) result[obj[key]] = key;
        return result;
    };
    Temp.keys = nativeKeys || function(obj) {
        if (obj !== Object(obj)) throw new TypeError('Invalid object');
        var keys = [];
        for (var key in obj)
            if (Temp.has(obj, key)) keys[keys.length] = key;
        return keys;
    };
    // List of HTML entities for escaping.
    var entityMap = {
        escape: {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#x27;',
            '/': '&#x2F;'
        },
    };
    entityMap.unescape = Temp.invert(entityMap.escape);
    // Regexes containing the keys and values listed immediately above.
    var entityRegexes = {
        escape: new RegExp('[' + Temp.keys(entityMap.escape).join('') + ']', 'g'),
        unescape: new RegExp('(' + Temp.keys(entityMap.unescape).join('|') + ')', 'g')
    };
    // Functions for escaping and unescaping strings to/from HTML interpolation.
    Temp.each(['escape', 'unescape'], function(method) {
        Temp[method] = function(string) {
            if (string == null) return '';
            return ('' + string).replace(entityRegexes[method], function(match) {
                return entityMap[method][match];
            });
        };
    });
    Temp.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
    };
    // JavaScript micro-templating, similar to John Resig's implementation.
    // Underscore templating handles arbitrary delimiters, preserves whitespace,
    // and correctly escapes quotes within interpolated code.
    Temp.template = function(text, data, settings) {
        var render;
        settings = Temp.defaults({}, settings, Temp.templateSettings);

        // Combine delimiters into one regular expression via alternation.
        var matcher = new RegExp([
            (settings.escape || noMatch).source, (settings.interpolate || noMatch).source, (settings.evaluate || noMatch).source
        ].join('|') + '|$', 'g');

        // Compile the template source, escaping string literals appropriately.
        var index = 0;
        var source = "TmpTmpp+='";
        text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
            source += text.slice(index, offset)
                .replace(escaper, function(match) {
                    return '\\' + escapes[match];
                });

            if (escape) {
                source += "'+\n((TmpTmpt=(" + escape + "))==null?'':Temp.escape(TmpTmpt))+\n'";
            }
            if (interpolate) {
                source += "'+\n((TmpTmpt=(" + interpolate + "))==null?'':TmpTmpt)+\n'";
            }
            if (evaluate) {
                source += "';\n" + evaluate + "\nTmpTmpp+='";
            }
            index = offset + match.length;
            return match;
        });
        source += "';\n";

        // If a variable is not specified, place data values in local scope.
        if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';

        source = "var TmpTmpt,TmpTmpp='',TmpTmpj=Array.prototype.join," +
            "print=function(){TmpTmpp+=TmpTmpj.call(arguments,'');};\n" +
            source + "return TmpTmpp;\n";

        try {
            render = new Function(settings.variable || 'obj', 'Temp', source);
        } catch (e) {
            e.source = source;
            throw e;
        }

        if (data) return render(data, Temp);
        var template = function(data) {
            return render.call(this, data, Temp);
        };

        // Provide the compiled function source as a convenience for precompilation.
        template.source = 'function(' + (settings.variable || 'obj') + '){\n' + source + '}';

        return template;
    };

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' && module.exports) {
            exports = module.exports = Tem;
        }
        exports.Temp = Temp;
    } else {
        root.Temp = Temp;
    }
}).call(this);
/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function(factory) {
    // Browser globals
    factory(jQuery);
}(function($) {

    var pluses = /\+/g;

    function encode(s) {
        return config.raw ? s : encodeURIComponent(s);
    }

    function decode(s) {
        return config.raw ? s : decodeURIComponent(s);
    }

    function stringifyCookieValue(value) {
        return encode(config.json ? JSON.stringify(value) : String(value));
    }

    function parseCookieValue(s) {
        if (s.indexOf('"') === 0) {
            // This is a quoted cookie as according to RFC2068, unescape...
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }

        try {
            // Replace server-side written pluses with spaces.
            // If we can't decode the cookie, ignore it, it's unusable.
            // If we can't parse the cookie, ignore it, it's unusable.
            s = decodeURIComponent(s.replace(pluses, ' '));
            return config.json ? JSON.parse(s) : s;
        } catch (e) {}
    }

    function read(s, converter) {
        var value = config.raw ? s : parseCookieValue(s);
        return $.isFunction(converter) ? converter(value) : value;
    }

    var config = $.cookie = function(key, value, options) {

        // Write

        if (arguments.length > 1 && !$.isFunction(value)) {
            options = $.extend({}, config.defaults, options);

            if (typeof options.expires === 'number') {
                var days = options.expires,
                    t = options.expires = new Date();
                t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
            }

            return (document.cookie = [
                encode(key), '=', stringifyCookieValue(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path ? '; path=' + options.path : '',
                options.domain ? '; domain=' + options.domain : '',
                options.secure ? '; secure' : ''
            ].join(''));
        }

        // Read

        var result = key ? undefined : {},
            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling $.cookie().
            cookies = document.cookie ? document.cookie.split('; ') : [],
            i = 0,
            l = cookies.length;

        for (; i < l; i++) {
            var parts = cookies[i].split('='),
                name = decode(parts.shift()),
                cookie = parts.join('=');

            if (key === name) {
                // If second argument (value) is a function it's a converter...
                result = read(cookie, value);
                break;
            }

            // Prevent storing a cookie that we couldn't decode.
            if (!key && (cookie = read(cookie)) !== undefined) {
                result[name] = cookie;
            }
        }

        return result;
    };

    config.defaults = {};

    $.removeCookie = function(key, options) {
        // Must not alter options, thus extending a fresh object...
        $.cookie(key, '', $.extend({}, options, {
            expires: -1
        }));
        return !$.cookie(key);
    };

}));

(function() {
    var colorList = ['co-white', 'co-yellow', 'co-yellow', 'co-yellow'];
    var tpl0 = "<li class='li0 <%= status %>' title='工作中骑手数'><span data-num='<%= quota %>人' data-hide='**人'><%= monitorEyeOpen?'**':quota %>人</span></li>",
        tpl1 = "<li class='li1 <%= status %>' title='超时单占比'><span data-num='<%= quota %>%超时' data-hide='**超时'><%= monitorEyeOpen?'**':(quota+'%') %>超时</span></li>",
        tpl2 = "<li class='li2 <%= status %>' title='人均负载'><span data-num='<%= quota %>负载' data-hide='**负载'><%= monitorEyeOpen?'**':quota %>负载</span></li>",
        tpl3 = "<li class='li3 <%= status %>' title='今日订单量'><span data-num='<%= quota %>单' data-hide='**单'><%= monitorEyeOpen?'**':quota %>单</span></li>";
    var html_monitor = ['<a class="monitor-wrap" target="_self" href="<%= linkTo %>">',
        '<ul id="monitor-ul" class="monitor-ul">',
        '   <% if(data.rider && data.rider.data){ %>',
        '     <li class="li0 <%= colorList[data.rider.data.status-1] %>" title="工作中骑手数"><span data-hide="**人" data-num="<%= data.rider.data.quota %>人"><%= monitorEyeOpen?"**":data.rider.data.quota %>人</span></li>',
        '   <%}else{ %>',
        '     <li class="li0" title="工作中骑手数"><span>0人</span></li>',
        '   <% } %>',
        '   <% if(data.timeout && data.timeout.data){ %>',
        '     <li class="li1 <%= colorList[data.timeout.data.status-1] %>" title="超时单占比"><span data-hide="**超时" data-num="<%= data.timeout.data.quota %>%超时"><%= monitorEyeOpen?"**":(data.timeout.data.quota+"%") %>超时</span>',
        '     </li>',
        '   <%}else{ %>',
        '     <li class="li1" title="超时单占比"><span>0.0%超时</span></li>',
        '   <% } %>',
        '   <% if(data.load && data.load.data){ %>',
        '     <li class="li2 <%= colorList[data.load.data.status-1] %>" title="人均负载"><span data-hide="**负载" data-num="<%= data.load.data.quota %>负载"><%= monitorEyeOpen?"**":data.load.data.quota %>负载</span></li>',
        '   <%}else{ %>',
        '     <li class="li2" title="人均负载"><span>0.00负载</span></li>',
        '   <% } %>',
        '   <% if(data.waybill && data.waybill.data){ %>',
        '     <li class="li3 <%= colorList[data.waybill.data.status-1] %>" title="今日订单量"><span data-hide="**单" data-num="<%= data.waybill.data.quota %>单"><%= monitorEyeOpen?"**":data.waybill.data.quota %>单</span></li>',
        '   <%}else{ %>',
        '     <li class="li3" title="今日订单量><span>0单</span></li>',
        '   <% } %>',
        '   <li class="monitor-eye <%= monitorEyeOpen && "eye-close" %>">',
        '       <span class="vertical-line"></span>',
        '       <i class="fa fa-eye" title=<%= monitorEyeOpen?"显示数据":"隐藏数据" %>></i>',
        '   </li>',
        '</ul>',
        '</a>'
    ].join("");
    var html_zb = ['<a class="monitor-wrap" target="_self" href="<%= linkTo %>">',
        '<ul id="monitor-ul" class="monitor-ul">',
        '   <% if(data.delivering && data.delivering.data){ %>',
        '     <li class="li0 <%= colorList[data.delivering.data.status-1] %>" title="在岗骑手数"><span data-hide="**人" data-num="<%= data.delivering.data.quota %>人"><%= monitorEyeOpen?"**":data.delivering.data.quota %>人</span></li>',
        '   <%}else{ %>',
        '     <li class="li0" title="在岗骑手数"><span>0人</span></li>',
        '   <% } %>',
        '   <% if(data.timeout && data.timeout.data){ %>',
        '     <li class="li1 <%= colorList[data.timeout.data.status-1] %>" title="超时单占比"><span data-hide="**超时" data-num="<%= data.timeout.data.quota %>%超时"><%= monitorEyeOpen?"**":(data.timeout.data.quota+"%") %>超时</span>',
        '     </li>',
        '   <%}else{ %>',
        '     <li class="li1" title="超时单占比"><span>0.0%超时</span></li>',
        '   <% } %>',
        '   <% if(data.load && data.load.data){ %>',
        '     <li class="li2 <%= colorList[data.load.data.status-1] %>" title="人均负载"><span data-hide="**负载" data-num="<%= data.load.data.quota %>负载"><%= monitorEyeOpen?"**":data.load.data.quota %>负载</span></li>',
        '   <%}else{ %>',
        '     <li class="li2" title="人均负载"><span>0.00负载</span></li>',
        '   <% } %>',
        '   <% if(data.waybill && data.waybill.data){ %>',
        '     <li class="li3 <%= colorList[data.waybill.data.status-1] %>" title="今日订单量"><span data-hide="**单" data-num="<%= data.waybill.data.quota %>单"><%= monitorEyeOpen?"**":data.waybill.data.quota %>单</span></li>',
        '   <%}else{ %>',
        '     <li class="li3" title="今日订单量"><span>0单</span></li>',
        '   <% } %>',
        '   <li class="monitor-eye <%= monitorEyeOpen && "eye-close" %>">',
        '       <span class="vertical-line"></span>',
        '       <i class="fa fa-eye" title=<%= monitorEyeOpen?"显示数据":"隐藏数据" %>></i>',
        '   </li>',
        '</ul>',
        '</a>'
    ].join("");

    var protoString = Object.prototype.toString;
    var cookieName = "monitor-eye";
    /**
     * 字典方法
     * @class Util
     * @static
     */
    var Util = {
        isObject: function(obj) {
            return protoString.call(obj) == "[object Object]";
        },
        isArray: function(obj) {
            return protoString.call(obj) == "[object Array]";
        },
        extend: function(child, parent, deep) {
            for (var i in parent) {
                if (deep && (Util.isObject(parent[i]) || Util.isArray(parent[i]))) {
                    if (Util.isObject(parent[i]) && !Util.isObject(child[i])) {
                        child[i] = {};
                    } else if (Util.isArray(parent[i]) && !Util.isArray(child[i])) {
                        child[i] = [];
                    }
                    Util.extend(child[i], parent[i], deep);
                } else if (parent[i] !== undefined) {
                    child[i] = parent[i];
                }
            }
        },
        getQueryData: function(queryString) {
            /* 去掉字符串前面的"?"，并把&amp;转换为& */
            queryString = queryString.replace(/^\?+/, '').replace(/&amp;/, '&');
            var querys = queryString.split('&'),
                i = querys.length,
                _URLParms = {},
                item;

            while (i--) {
                item = querys[i].split('=');
                if (item[0]) {
                    var value = item[1] || '';
                    try {
                        value = decodeURIComponent(value);
                    } catch (e) {
                        value = unescape(value);
                    }
                    _URLParms[decodeURIComponent(item[0])] = value;
                }
            }
            return _URLParms;
        },
        /**
         * 千位分隔符
         * @param  {number} num 
         * @return {string} 分割后的字符，如1234567->'1,234,567'
         */
        splitThousand: function(num) {
            var str = (num + "").split('.')[0],
                arr = str.split('').reverse(),
                newarr = [];
            while (arr.length > 0) {
                newarr = newarr.concat(arr.splice(0, 3));
                newarr.push(',');
            }
            return newarr.reverse().join('').replace(/^,|,$/g, '') || '';
        }
    };

    var View = function(parSelector, name, classname, tpl, model) {
        // 从$pardom替换到selector，因为ul此时还没有加载，headerjs动态加载的
        this.parSelector = parSelector;
        this.classname = classname;
        this.tpl = tpl;
        this.model = model;
        this.timer = 0;
        this.name = name;
        this.initialize();
    };
    View.prototype.initialize = function() {

    };
    View.prototype.stopInterval = function() {
        clearInterval(this.timer);
    };
    View.prototype.render = function(data) {
        var tpl = this.tpl,
            self = this,
            classname = this.classname,
            parSelector = this.parSelector;

        function callback(data) {
            var html = Temp.template(tpl, {
                'status': colorList[data['status'] - 1],
                'quota': data['quota'],
                'quotaTips': data['quotaTips'],
                'monitorEyeOpen': ($.cookie(cookieName) == 1)
            });
            $(parSelector).find('.' + classname).get(0).outerHTML = html;
        }
        if (!data) {
            this.fetch(callback);
        } else {
            //有数据直接使用
            callback(data);
        }
    };
    View.prototype.fetch = function(callback) {
        return this.model.getData(callback);
    };

    var query = Util.getQueryData(location.search);
    var Model = function(url, param, def) {
        this.url = url;
        if (param && param['quota'] != undefined) {
            def = param;
            param = null;
        }
        this.def = $.extend({
            'status': 1,
            'quota': 0,
            'quotaTips': ''
        }, def);
        if (!param) {
            param = {
                'cityId': query['cityId'] || '-1',
                'orgId': query['orgId'] || '-1'
            }
        }
        this.param = param;
    };
    Model.prototype.getData = function(callback) {
        var rtn = this.def;
        $.get(this.url, this.param, function(data) {
            if (data.code == 0) {
                callback({
                    'status': data['data']['status'],
                    'quota': data['data']['quota'],
                    'quotaTips': data['data']['quotaTips']
                });
            } else {
                //v1.2出错后页面显示不变，不使用默认值
                //callback(rtn);
            }
        }).fail(function() {
            //v1.2失败后页面显示不变，不使用默认值
            //callback(rtn);
        });
    };
    Model.getAllData = function(callback) {
        var param = location.href.indexOf('/monitor/index') > -1 ? {
            'cityId': query['cityId'] || '-1',
            'areaId': query['areaId'] || '-1',
            'orgId': query['orgId'] || '-1'
        } : {
            'cityId': '-1',
            'areaId': '-1',
            'orgId': '-1'
        };
        $.ajax({
            url: url_config.monitor + '/api/quota',
            data: param,
            dataType: 'jsonp',
            jsonp: 'callback',
            success: function(data) {
                callback(data);
            },
            error: function(data) {
                callback({});
            }
        });
    };
    var Monitor = function() {
        this.monitorList = [];
    };
    Monitor.prototype.add = function(view) {
        view && view instanceof View && this.monitorList.push(view);
        return this;
    };
    Monitor.prototype.renderOne = function(name, data) {
        for (var i = this.monitorList.length - 1; i >= 0; i--) {
            if (name == this.monitorList[i]['name']) {
                this.monitorList[i].render(data);
                //检测到了监控中心页面给的数据，则停止自动刷新，使用监控中心给的数据
                this.monitorList[i].stopInterval();
                break;
            }
        }
    };
    var monitor = new Monitor;
    var Main = {
        init: function() {
            var parSelector = '#monitor-ul';
            var rider = new View(parSelector, 'rider', 'li0', tpl0, new Model(''));
            var timeout = new View(parSelector, 'timeout', 'li1', tpl1, new Model('', {
                'quota': '0.0'
            }));
            var perLoad = new View(parSelector, 'perLoad', 'li2', tpl2, new Model('', {
                'quota': '0.00'
            }));
            var totalOrder = new View(parSelector, 'totalOrder', 'li3', tpl3, new Model(''));
            monitor.add(rider).add(timeout).add(perLoad).add(totalOrder);
            //v1.2 由单独请求修改为同一个接口请求
            // $.each(monitor.monitorList, function(i, item) {
            //     item.render();
            //     item.timer = setInterval(function() {
            //         item.render.call(item, null)
            //     }, 60000);
            // });
            var getAllData = function() {
                Model.getAllData(function(data) {
                    if (!('load' in data) && !('timeout' in data)) return;
                    var linkTo, html;
                    var html = data['orgType'] == 4 ? html_zb : html_monitor;
                    linkTo = url_config.monitor;
                    // orgType:4 众包  rider,timeout,load,waybill
                    // orgType:0 监控 delivering,timeout,load,waybill

                    var noData = false;
                    for (var i in data) {
                        if (data[i]['code'] == 1) {
                            noData = true;
                            break;
                        }
                    }
                    $('#top-nav .monitor-wrap').remove();
                    if (!noData) {
                        //配送中的骑手，在岗骑手，订单数据增加千位分隔符
                        data['delivering'] && (data['delivering']['data']['quota'] = Util.splitThousand(data['delivering']['data']['quota']));
                        data['rider'] && (data['rider']['data']['quota'] = Util.splitThousand(data['rider']['data']['quota']));
                        data['waybill'] && (data['waybill']['data']['quota'] = Util.splitThousand(data['waybill']['data']['quota']));
                        var monitorWrap = Temp.template(html, {
                            'data': data,
                            'colorList': colorList,
                            'linkTo': linkTo,
                            'monitorEyeOpen': ($.cookie(cookieName) == 1)
                        });
                        var timer = setInterval(function() {
                            if ($('#top-nav .pull-right').length) {
                                clearInterval(timer);
                                $('#top-nav').append(monitorWrap);
                            }
                        }, 100);
                    }
                });
            };
            getAllData();
            monitor.timer = setInterval(getAllData, 60000);

            // 开关数据功能,如果cookie中为true则说明功能启用着
            $('#top-nav').delegate('.monitor-eye', 'click', function(event) {
                var isClose,
                    $self = $(this);
                $self.toggleClass('eye-close');
                $.removeCookie(cookieName, {
                    'path': '/',
                    'domain': Main.getCookieDomain()
                });
                isClose = $self.hasClass('eye-close');
                isClose ? $self.find('.fa-eye').attr('title', '显示数据') : $self.find('.fa-eye').attr('title', '隐藏数据');
                $.cookie(cookieName, (isClose ? 1 : 0), {
                    'expire': 30 * 24,
                    'path': '/',
                    'domain': Main.getCookieDomain()
                });
                $self.closest('.monitor-ul').find('li[class^="li"]').each(function(i, item) {
                    var $span = $(item).find('span');
                    var text = $span.text();
                    isClose ? $span.text($span.attr('data-hide')) : $span.text($span.attr('data-num'));
                });
                return false;
            });
        },
        /**
         * 获取要存的cookie的domain
         * @return {string} 主域domain
         */
        getCookieDomain: function() {
            var arr = [],
                tmp,
                admin = url_config.admin.split('.'),
                monitor = url_config.monitor.split('.');
            while (monitor.length) {
                if ((tmp = monitor.pop()) == admin.pop()) {
                    arr.unshift(tmp);
                } else {
                    break;
                }
            }
            return arr.join('.');
        }
    };
    Main.init();
    window['MonitorCom'] = (function(monitor) {
        return {
            'triggerRender': function(name, data) {
                //清除monitor本身的interval
                clearInterval(monitor.timer);
                monitor.renderOne(name, data);
            }
        };
    })(monitor);
})();