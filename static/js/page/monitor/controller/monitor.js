/*
 * 控制层，程序入口
 */
require.config({
    baseUrl: MT.STATIC_ROOT + '/js',
    urlArgs: 'ver=' + pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        },
        'lib/highcharts-4.2.1/highcharts-more': {
            deps: ['http://xs01.meituan.net/cdn/highcharts/4.2.1/highcharts.js']
        }
    }
});
var moudleList = ['page/monitor/module/common', 'page/monitor/module/assignAudit',
    'page/monitor/module/quota', 'page/monitor/module/url', 'module/validator',
    'module/cookie', 'module/head-nav', 'lib/highcharts-4.2.1/highcharts-more'
];
if (location.href.indexOf('/monitor/zb/') > 0) { // 众包
    moudleList = moudleList.concat([
        'page/monitor/module/power', 'page/monitor/module/bubble',
        'page/monitor/module/riderPie', 'page/monitor/module/timeOrder',
        'page/monitor/module/stressLine', 'page/monitor/module/heatMap'
    ]);
} else { // 直营加盟
    moudleList = moudleList.concat([
        'page/monitor/module/power', 'page/monitor/module/bubble',
        'page/monitor/module/riderPie', 'page/monitor/module/timeOrder',
        'page/monitor/module/todayOrder', 'page/monitor/module/perLoad'
    ]);
}

var isZb = location.href.indexOf('/monitor/zb/') > 0;

require(moudleList, function() {
    var self = {};
    var args = arguments;
    $.each(args, function(index, el) {
        if (typeof el != 'undefined' && 'name' in el) {
            self[el['name']] = el;
        }
    });

    var refList = [];
    /*
     * interval构造
     * time: setInterval的时间
     * execute: 是否立即执行
     */
    var InterVal = function(fun, time, execute) {
        this.fun = fun;
        this.time = time;
        this.id = this.timer = setInterval(fun, time);
        execute && this.fun();
    };
    InterVal.prototype = {
        start: function() {
            !!this.timer && this.stop();
            this.timer = setInterval(this.fun, this.time);
        },
        stop: function() {
            clearInterval(this.timer);
        }
    };

    var Main = {
        $city: $('#city'),
        $station: $('#station'),
        $dataCover: $('.data-cover'),
        refreshtime: null,
        date: null,
        clicktime: null,
        list2: [self['quota']],
        monitor: null, //monitor_com
        list4: (function(isZb) {
            if (!isZb) {
                return [self['assignAudit'], self['bubble'], self['power'], self['riderPie'], self['timeOrder'], self['todayOrder'], self['perLoad']]
            } else {
                return [self['assignAudit'], self['bubble'], self['power'], self['riderPie'], self['timeOrder'], self['stressLine']]
            }
        }(isZb)),
        init: function() {
            //如果存在MonitorCom则保存对象
            if (window['MonitorCom'] && window['MonitorCom']) {
                this.monitor = window['MonitorCom'];
            }
            // 动态配置订单改价和指派任务两个外链地址
            $('.shortcut-wrapper').children('a').each(function(i, item) {
                var _href = $(item).attr('href'),
                    domain = "";
                // 任务指派
                if ($(item).hasClass('assignCount-wrap')) {
                    _href = (url_config.dispatch || url_config.admin) + "/partner/dispatch/home?" + self['common'].parseObjToStr(Main.getParam()) + "#-10";
                    $(item).attr('href', _href);
                } else {
                    domain = (url_config.audit || url_config.admin);
                    $(item).attr('href', domain + _href);
                }
                _href && $(item).css('cursor', 'pointer');
            });

            Main.initMenu();
            Main.initChart();
            Main.initMap();
            Main.initSelect();
            Main.initEvent();
            Main.startInterval("1");
        },
        /**
         * 初始化画图相关
         * @return 
         */
        initChart: function() {
            Highcharts.setOptions({
                lang: {
                    resetZoom: "还原",
                    resetZoomTitle: '还原为全部展示'
                },
                chart: {
                    style: {
                        color: "#777",
                        fontFamily: "'microsoft yahei', Arial, Helvetica, sans-serif"
                    }
                },
                xAxis: {
                    labels: {
                        style: {
                            color: "#666",
                            fontFamily: "'microsoft yahei', Arial, Helvetica, sans-serif"
                        }
                    }
                },
                yAxis: {
                    labels: {
                        style: {
                            color: "#666",
                            fontFamily: "'microsoft yahei', Arial, Helvetica, sans-serif"
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    itemStyle: {
                        color: "#666",
                        fontSize: "12px",
                        fontFamily: "'microsoft yahei', Arial, Helvetica, sans-serif",
                        fontWeight: 'normal'
                    },
                    symbolWidth: 12,
                    symbolHeight: 12
                }
            });

            $(window).bind('resize', function() {
                $('.highchart').highcharts().reflow();
            });
        },
        /**
         * 左侧菜单的展开和选中，因为headerjs要等请求的菜单数据返回后才加载，
         * 而此时headnav-js执行的代码找不到菜单，导致没有展开
         * @return
         */
        initMenu: function() {
            var inter = new InterVal(setActiveMenu, 100);

            function setActiveMenu() {
                if ($(".submenu").length) {
                    var pathname = location.pathname;
                    $(".submenu").find("a").each(function() {
                        var $self = $(this);
                        if ($self.attr('href').indexOf(pathname) > -1) {
                            $self.parents("li").addClass("active")
                            $self.parent().parent().addClass("in");
                            inter.stop();
                            return false;
                        }
                    });
                }
            }
        },
        /**
         * 初始化地图相关
         * @return 
         */
        initMap: function() {
            var hash = location.hash;
            // 地图style按钮初期状态
            (localStorage.getItem('map-style') == "normal") ? $('.map-change button').eq(0).addClass('active'): $('.map-change button').eq(1).addClass('active');
            // 区域选择框状态
            (localStorage.getItem('map-poly') == "1") && $('.map-poly-control .poly-show').attr('checked', 'checked');

            // 地图tab
            $('.heatMap-wrap .nav-tabs li').bind('click', function() {
                var $self = $(this);
                if ($self.hasClass('active')) return;
                // 运力压力固定显示区域
                $self.index() == 0 ? $('.map-poly-control').hide() : $('.map-poly-control').show();
                $self.siblings('.active').removeClass('active').end().addClass('active');
                Main.refreshMapErea();
            });
        },
        initSelect: function() {
            var type = self['URL'].getQueryString('type') || '-1',
                cityId = self['URL'].getQueryString('cityId') || '-1',
                areaId = self['URL'].getQueryString('areaId'),
                orgId = self['URL'].getQueryString('orgId'),
                $typeOp,
                $cityOp,
                $areaOp,
                $orgOp,
                $typeSel = $("select[name='type']"),
                $citySel = $("select[name='cityId']"),
                $areaSel = $("select[name='areaId']"),
                $orgSel = $("select[name='orgId']"),
                txt = "<span class='totalstation'>共{count}个{name}</span>",
                getItemText = function($doms, attribute, val) {
                    for (var i = 0, j = $doms.length; i < j; i++) {
                        if ($doms.eq(i).attr(attribute) == val) {
                            return $doms.eq(i).text();
                        }
                    }
                    return "";
                };
            if (!isZb && type) {
                $typeOp = $typeSel.find("option[data-type='" + type + "']");
                // 设置select2的值
                //$typeOp.length && type != -1 && $("select[name='type']").select2().select2("val", $typeOp.text());
                $typeSel.length && $typeSel.select2().select2("val", type);
            }
            //城市处理
            if (cityId) {
                $cityOp = $citySel.find("option[data-cityId='" + cityId + "']");
                $cityOp.length && $citySel.val($cityOp.text());
                $citySel.select2().select2("val", cityId);
                // cityId != -1 && $("select[name='cityId']").select2().select2("val", $cityOp.text() || '全部站点');
                // $("span[title='全部城市']").text($cityOp.length ? $cityOp.text() : '全部城市');
            }
            // 只有一个城市，移除掉‘全部城市’
            if ($citySel.find('option').length == 2) {
                $citySel.find('option').eq(0).remove();
            }

            // 区域处理
            if (areaId) {
                $areaOp = $areaSel.find("option[data-areaId='" + areaId + "']");
                $areaOp.length && $areaSel.val($areaOp.text());
                $areaSel.select2().select2("val", areaId);
                // $("span[title='全部区域']").text($areaOp.length ? $areaOp.text() : '全部区域');
            } else {
                $areaSel.select2().select2("val", '-1');
            }
            //只有一个区域，移除掉‘全部区域’
            if ($areaSel.find('option').length == 2) {
                $areaSel.find('option').eq(0).remove();
            }

            // 站点处理
            if (orgId) {
                $orgOp = $orgSel.find("option[data-orgId='" + orgId + "']");
                $orgOp.length && $orgSel.val($orgOp.text());
                $orgSel.select2().select2("val", orgId);
                // $("span[title='全部站点']").text($orgOp.length ? $orgOp.text() : '全部站点');
            } else {
                $orgSel.select2().select2("val", '-1');
            }
            //只有一个站点，移除掉‘全部站点’
            if ($orgSel.find('option').length == 2) {
                $orgSel.find('option').eq(0).remove();
            }
        },
        initEvent: function() {
            // 左上角菜单切换的时候使用
            $(".j-toggle-menu").bind("click", function() {
                setTimeout(function() {
                    $(window).resize()
                }, 0)
            });
            // 左上角菜单点击
            $('#menuToggle').on('click', function() {
                $('#wrapper').toggleClass('sidebar-hide');
            });
            // 刷新按钮
            $('.btn-refresh').on('click', function() {
                //放置连续触发
                if (Main.date && Date.now() - Main.date < 1000) return;
                var $self = $(this);
                Main.addRefreshAnimation();
                Main.startInterval();
            });
            // select值改变
            $("select[name='type'],select[name='cityId'],select[name='areaId'],select[name='orgId']").on('change', function(e) {
                var $self = $(this),
                    $orgOption,
                    type,
                    cityId,
                    areaId,
                    orgId;
                type = self['URL'].getQueryString("type") || '-1';
                //更新站点列表
                if ($self.attr('name') == 'cityId') {
                    location.search = "?type=" + type + "&cityId=" + $self.find('option:selected').attr('data-cityId') || '-1';
                }
                if ($self.attr('name') == 'areaId') {
                    areaId = $self.find('option:selected').attr('data-areaId') || '-1';
                    location.search = "?type=" + type + "&cityId=" + self['URL'].getQueryString("cityId") + "&areaId=" + areaId;
                }
                if ($self.attr('name') == 'orgId') {
                    orgId = $self.find('option:selected').attr('data-orgId') || '-1';
                    location.search = "?type=" + type + "&cityId=" + self['URL'].getQueryString("cityId") + "&areaId=" + (self['URL'].getQueryString("areaId") || '-1') + "&orgId=" + orgId;
                }
                if ($self.attr('name') == 'type') {
                    type = $self.find('option:selected').attr('data-type') || '-1';
                    location.search = "?type=" + type;
                }
            });
            // highcharts modal
            $('.chart-container').delegate('.modal .fa-times', 'click', function(event) { //关闭
                $(this).closest('.modal').trigger('click');
                event.stopPropagation();
            }).delegate('.modal', 'click', function(event) { //关闭
                var $highchart = $(this).find('.highchart');
                if ($(event.target).hasClass('modal')) {
                    $(this).find('.btn-back').remove();
                    $(this).data('datas', null);
                    $(this).removeClass('modal');
                    $('body').removeClass('overflow-h');
                    $highchart.length && $highchart.highcharts();
                    // 如果有数据
                    if ($highchart.data('minShowData')) {
                        self[$highchart.attr('name')].renderWithData($highchart.data('minShowData'));
                    } else {
                        $highchart.highcharts().reflow();
                    }
                }
            }).delegate('.chart-wrap .fa-external-link', 'click', function(event) { // 全屏查看
                var $col = $(this).closest('[class^=col-]');
                var $highchart = $col.addClass('modal').find('.highchart');
                $highchart.length && $highchart.highcharts() && $highchart.highcharts().reflow();
                $('body').addClass('overflow-h');
            }).delegate('.fa-bars', 'click', function(event) { //查看数据
                var name = $(event.target).closest('.chart-wrap').find('.highchart').attr('name');
                // timeOrder服务器端接收到的是两个不同的接口，所以字段要区分开
                if (!isZb && name === 'timeOrder') {
                    name = 'pendingOrder';
                }
                Main.$dataCover.removeClass('hidden').attr('name', name);
                $('body').addClass('overflow-h');
                Main.createDataTable(Main.getParam(), name);
                event.stopPropagation();
            });
            // data-cover
            Main.$dataCover.on('click', function(event) { //关闭
                $(this).addClass('hidden');
                $(this).data('prevParam', null);
                $('body').removeClass('overflow-h');
            }).delegate('.cover', 'click', function(event) {
                event.stopPropagation();
            }).delegate('.fa-times', 'click', function(event) { //关闭
                event.stopPropagation();
                Main.$dataCover.trigger('click');
            }).delegate('.export', 'click', function(event) { //导出   
                var name = Main.$dataCover.attr('name');
                name && Main.exportChartData(name);
                event.stopPropagation();
            }).delegate('table a', 'click', function(event) { //内部链接请求 
                var $self = $(this);
                var attrs = $self.get(0).attributes;
                var param = {};
                var prevParam;
                $.each(attrs, function(i, attr) {
                    var nodename = attr.nodeName;
                    var nodeval = attr.nodeValue;
                    var regex = /^data-/;
                    // 值的形式为 nodename:value，所以要split一下
                    var vals = nodeval.split(':');
                    if (nodename.match(regex)) {
                        param[vals[0]] = vals[1];
                    }
                });
                Main.createDataTable(param, Main.$dataCover.attr('name'));
                event.stopPropagation();
            }).delegate('.btn-back', 'click', function(event) { // 返回
                prevParam = Main.$dataCover.data("prevParam");
                prevParam.pop();
                Main.createDataTable(prevParam.splice(prevParam.length - 1)[0], Main.$dataCover.attr('name'));
            });
            //右上角数字点击消失
            $('.click-num').bind('click', function() {
                if (!$(this).attr('href')) return;
                $(this).find('.shortcut-alert').remove();
            });
            // feedback
            var $feedback = $('.feedback'),
                $feedbacka = $('.feedback').find('a');
            $feedbacka.attr('href', url_config.admin + $feedbacka.attr('href'));
            $feedback.delegate('.fb-mt-icon', 'click', function() {
                $feedback.toggleClass('slidehide');
            }).delegate('.fb-arrow', 'click', function() {
                $feedback.toggleClass('slidehide');
            });
        },
        /**
         * 生成数据表
         * @return
         */
        createDataTable: function(param, name) {
            var $cover = Main.$dataCover.find('.cover');
            var param = param;
            var prevParam = Main.$dataCover.data("prevParam");

            if (!prevParam) {
                Main.$dataCover.data("prevParam", []);
            }
            Main.$dataCover.data("prevParam").push(param);

            $cover.empty();
            $('.loading-wrap').show();
            prevParam && (prevParam.length > 1) ? Main.showBtnBack() : Main.removeBtnBack();
            $.ajax({
                    url: action_config["tableData"],
                    dataType: 'json',
                    data: $.extend({}, param, {
                        indicator: name
                    })
                })
                .done(function(data) {
                    if (data['code'] == 0 && data['data']) {
                        var tableData = data['data'];
                        var $cover = $('.data-cover.modal .cover')
                            .hide()
                            .append('<h4 class="text-center">' + tableData['tableTitle'] + '<button type="button" class="btn export btn-primary">导出</button></h4>')
                            .append(table.createHead(tableData['showIndex'], tableData['tableHead']) + table.createBody(tableData))
                            .show();
                        var $tbodyWrap = $cover.find('.tbody-wrap');
                        if ($tbodyWrap.height() < $tbodyWrap.find('.table').height()) {
                            $tbodyWrap.width($tbodyWrap.width() + 7);
                        }
                    }
                })
                .fail(function(err) {

                })
                .always(function() {
                    $('.loading-wrap').hide();
                });

            // 生成
            var table = {
                columns: 0,
                colWidth: 0,
                createTr: function(data) {
                    var list = ['<tr>'];
                    for (var i = 0; i < data.length; i++) {
                        list.push('<th class="text-center" width="' + table.colWidth + 'px" >' + data[i]['label'] + '</th>');
                    }
                    list.push('</tr>');
                    return list.join('');
                },
                createHead: function(index, data) {
                    var list = [],
                        tmp = [],
                        colspan = "",
                        rowspan = "";
                    list.push('<table class="table table-condensed table-bordered"><thead><tr>');
                    for (var i = index; i < data.length; i++) {
                        if (data[i]['subColumns'] && data[i]['subColumns'].length) {
                            tmp = tmp.concat(data[i]['subColumns']);
                        }
                    }
                    for (var i = index; i < data.length; i++) {
                        rowspan = !!data[i]['subColumns'] ? "" : 2;
                        rowspan == 2 && table.columns > 0 && (table.columns++)
                    }
                    table.columns = tmp.length || data.length;
                    table.colWidth = ($(window).height() * 0.8 - 30) / table.columns
                    table.colWidth = ($(window).height() * 0.8 - 30) / table.columns
                    for (var i = index; i < data.length; i++) {
                        colspan = !!data[i]['subColumns'] ? data[i]['subColumns'].length : "";
                        rowspan = !!data[i]['subColumns'] ? "" : 2;
                        list.push('<th class="text-center" width="' + table.colWidth + 'px" colspan="' + colspan + '" rowspan="' + rowspan + '">' + data[i]['label'] + '</th>');
                    }
                    list.push('</tr>');
                    list.push(table.createTr(tmp));
                    list.push('</thead></table>');
                    return list.join('');
                },
                createBody: function(tableData) {
                    var data = tableData['tableBody'],
                        datahead = tableData['tableHead'],
                        index = tableData['showIndex'],
                        hasSubTable = (tableData['hasSubTable'] == 1);
                    var dataShowTpl = [
                        '      <div class="tbody-wrap" style="height:<%= height %>px;"><table class="table table-condensed table-bordered">',
                        '         <% for(var i=0;i<data.length;i++){ %>',
                        '         <tr>',
                        '             <% for(var j=index;j<data[i].length;j++){ %>',
                        '               <td width="<%= tdwidth %>px"> ',
                        '               <% if(index > 0 && j == index){ %>',
                        '                   <% if(hasSubTable){ %>',
                        '                       <a href="#" title="查看下一级"',
                        '                   <% }else{ %>',
                        '                       <span',
                        '                   <% } %>',
                        '                   <% for(var k=0;k<index;k++){ %>',
                        '                       data-<%= datahead[k]["label"] %>="<%= datahead[k]["label"] %>:<%= data[i][k] %>" ',
                        '                   <% } %>',
                        '                   <% if(hasSubTable){ %>',
                        '                       ><%= data[i][j] %></a>',
                        '                   <% }else{ %>',
                        '                       ><%= data[i][j] %></span>',
                        '                   <% } %>',
                        '               <% }else{ %>',
                        '                   <%= data[i][j] %></td>',
                        '               <% } %>',
                        '             <% } %>',
                        '          </tr>',
                        '          <% } %>',
                        '       </table></div>'
                    ].join("");
                    var tmpl = _.template(dataShowTpl);
                    return tmpl({
                        'data': data,
                        'datahead': datahead,
                        'index': index,
                        'tdwidth': table.colWidth,
                        'height': $(window).height() * 0.6 - 150,
                        'hasSubTable': hasSubTable
                    });
                }
            };

        },
        // 增加返回上一级
        showBtnBack: function() {
            if (!Main.$dataCover.find('.btn-back').length) {
                Main.$dataCover.find('.cover').append('<a class="btn btn-default btn-back"><i class="fa fa-reply fa-lg"></i> 返回</a>');
            }
        },
        // 删除返回上一级按钮
        removeBtnBack: function() {
            Main.$dataCover.find('.btn-back').remove();
        },
        /**
         * 导出
         * @param  {jquery object} $highchart 图表
         * @return {}
         */
        exportChartData: function(name) {
            var prevParam = Main.$dataCover.data("prevParam");
            prevParam && prevParam.length &&
                window.open(action_config["export"] + "?" + self['common'].parseObjToStr($.extend({}, prevParam[prevParam.length - 1], {
                    indicator: name
                })));

        },
        /**
         * 添加加载的旋转动画
         */
        addRefreshAnimation: function() {
            var $refresh = $('.btn-refresh');
            $refresh.addClass('rotating');
            setTimeout(function() {
                $refresh.removeClass('rotating');
            }, 1000);
        },
        /**
         * 中间四个区域的数据刷新
         * @return 
         */
        refreshMidErea: function() {
            var param = Main.getParam();
            $('.loading-overlay').addClass('active');
            Main.list2[0]['refreshData'](param, function(name, data) {
                // 众包监控上方数据完全由jsonp接口决定
                !isZb && Main.refreshCallback(name, data);
            });
        },
        /**
         * 中间四个区域以外的数据刷新
         * @return 
         */
        refreshMidOtherErea: function() {
            var param = Main.getParam();
            $.each(Main.list4, function(i, item) {
                item['refreshData'](param);
            });
            isZb && Main.refreshMapErea();
        },
        /**
         * 地图的数据刷新
         * @return 
         */
        refreshMapErea: function() {
            var param = Main.getParam();
            var url = "";
            var gradientRule, max, emphasisItem;
            var index = $('.heatMap-wrap .nav-tabs li.active').index();

            changeActiveAndLegend(index);

            function changeActiveAndLegend(index) {
                var list = [];
                switch (index) {
                    case 0:
                        list = ['压力<0.3单/人', '压力<0.5单/人', '压力<0.8单/人', '压力<1.0单/人', '压力>1.0单/人'];
                        break;
                    case 1:
                        list = ['积压少于30单', '积压少于40单', '积压少于50单', '积压少于60单', '积压大于60单'];
                        break;
                    case 2:
                        list = ['骑手少于100', '骑手少于150', '骑手少于200', '骑手少于250', '骑手多于250'];
                        break;
                }
                $('.heatMap-wrap .map-legend').html(['<ul>',
                    '   <li class="">图例</li>',
                    '   <li class="color1">' + list[0] + '</li>',
                    '   <li class="color2">' + list[1] + '</li>',
                    '   <li class="color3">' + list[2] + '</li>',
                    '   <li class="color4">' + list[3] + '</li>',
                    '   <li class="color5">' + list[4] + '</li>',
                    '</ul>'
                ].join(''));
            }
            $('.heatMap-wrap .map-loading').show();
            switch (index) {
                case 0:
                    self['heatMap']['refreshData'](param, {
                        'gradientRule': {
                            0.2: "blue",
                            0.3: "#21bb0c",
                            0.4: "#f5a700",
                            0.5: "#ff7200",
                            1.0: "#fd3023"
                        },
                        'max': 1,
                        'isFillColor': true,
                        'emphasisItem': 'deliveryPressure',
                        'isShow': true
                    }, null);
                    return;
                case 1:
                    url = action_config['heatMapPress'];
                    gradientRule = {
                        0.3: "blue",
                        0.4: "#21bb0c",
                        0.5: "#f5a700",
                        0.6: "#ff7200",
                        1.0: "#fd3023"
                    };
                    max = 100;
                    emphasisItem = 'waybill';
                    break;
                case 2:
                    url = action_config['heatMapRider'];
                    gradientRule = {
                        0.2: "blue",
                        0.3: "#21bb0c",
                        0.4: "#f5a700",
                        0.5: "#ff7200",
                        1.0: "#fd3023"
                    };
                    max = 500;
                    emphasisItem = 'workingRider';
                    break;
            }

            var $promise = $.ajax({
                url: url,
                data: param,
                dataType: 'json',
            });
            $promise.done(function(data) {
                (data.code == 0) && (rtn = data.data) && $.each(data.data, function(i, item) {
                    item.lng /= 1e6;
                    item.lat /= 1e6;
                });
                self['heatMap']['refreshData'](param, {
                    'gradientRule': gradientRule,
                    'max': max,
                    'emphasisItem': emphasisItem
                }, data.data);
            }).fail(function() {
                console.log("error");
            }).always(function() {
                $('.heatMap-wrap .map-loading').hide();
            });
        },
        // 开始所有的interval
        startInterval: function(first) {
            //1秒以内取消
            if (Main.date && Date.now() - Main.date < 1000) return;
            Main.date = Date.now();
            Main.stopInterval();
            //初次请求
            Main.refreshMidOtherErea()
            Main.refreshMidErea();
            setTimeout(Main.startRefreshTime, 1000);
        },
        //刷新回调
        //data: ajax data['data']
        refreshCallback: function(name, data) {
            if (Main.monitor && Main.monitor.triggerRender) {
                try {
                    Main.monitor.triggerRender(name, data);
                } catch (e) {}
            }
        },
        // 停止所有的interval
        stopInterval: function() {
            if (!refList.length) return;
            $.each(refList, function(i, item) {
                item.stop();
            });
            refList.length = 0;
        },
        startRefreshTime: function() {
            $('.refresh-time').text('60');
            var param = Main.getParam();
            //每秒跳数字并判断是否到0需要刷新
            Main.refreshtime = new InterVal(function() {
                var num = Number($('.refresh-time').text()) - 1;
                var ready = 0;
                $('.refresh-time').text(num);
                if (num == '0') {
                    Main.refreshtime.stop();
                    Main.addRefreshAnimation();
                    // 几个6000毫秒的同步
                    Main.refreshMidErea();
                    Main.refreshMidOtherErea();
                    //延迟1秒重新开始
                    setTimeout(function() {
                        Main.startRefreshTime();
                    }, 1000)
                }
            }, 1000, false);
            refList.push(Main.refreshtime);
        },
        // 获取参数
        getParam: function() {
            return self['common'].getUrlParam();
        },
        // 函数绑定
        bind: function(fun, args) {
            return function() {
                fun.call(null, args);
            }
        }
    };
    Main.init();
});