require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/head-nav'], function (nav) {
    
	var sysErrorMsg = "网络遇到问题，请稍后重试";

   	var myAlert = function(msg){
   		$("#alertDialog .msg").html(msg);
   		$("#alertDialog").modal("show");
   	}

   	var myConfirm = function(msg,callback){
   		$("#confirmDialog .msg").html(msg);
   		$("#confirmDialog").modal("show");
   		$("#confirmDialog .submit").unbind("click").bind("click",function(){
   			if(callback)
   				callback();
   			$("#confirmDialog").modal("hide");
   		});
   	}



   	var gradeBiz = {

   		ruleData :{
   			rewardsRuleList:[],
   			punishmentRule:{
   				finishRuleList:[],
   				flawRateRuleList:[],
   				onTimeRuleList:[],
   				timeoutRateList:[],
   				permanentRule:{}
   			},
   			ruleName : ""
   		},
   		submitting:false,
   		/*fieldMap:{
   			finish : {
   				list : "finishRuleList",
   				status :  "finishRateStatus",
   				tolerance : "finishRateTolerance"
   			},
   			flaw : {
   				list : "flawRateRuleList",
   				status :  "flawRateStatus",
   				tolerance : "flawRateTolerance"
   			},
   			onTime : {
   				list : "onTimeRuleList",
   				status :  "onTimeRateStatus",
   				tolerance : "onTimeRateTolerance"
   			},
   			timeout : {
   				list : "timeoutRateList",
   				status :  "timeoutRateStatus",
   				tolerance : "timeoutRateTolerance"
   			}
   		},*/
   		action :{
   			awardEdit : false,
   			punishEdit : false,
   			currentPage : 1
   		},

   		init:function(){

   			$("#btnCancelEdit").click(function(){
				//判断编辑状态
				myConfirm("您所做的修改将无法保存，是否退出此次编辑？",function(){
					$("#btnCreate").show();
					$(".editDiv").hide();
				});
				
			});

   			$("#pager").on("click","a",function() {
   				gradeBiz.action.currentPage = $(this).data("page");
				gradeBiz.getAll(gradeBiz.action.currentPage);
			});

			$("#tableList").on("click",".iconEdit",function(){
				gradeBiz.getDetail($(this).parents("tr").attr("data-id"));
			});

			$("#tableList").on("click",".iconLink",function(){
				$("#cityDialog").modal("show").attr("data-id",$(this).parents("tr").attr("data-id"));
			});

			$("#tableList").on("click",".iconDel",function(){
				var id = $(this).parents("tr").attr("data-id");
				myConfirm("关联城市将解除关联，是否确认删除",function(){
					$("tr[data-id='"+id+"']").remove();

					$.ajax({
						url : "/grade/deleteRuleById",
						type :"post",
						data : {ruleId:id},
						dataType :"json",
						success : function(data){
							if(data.code==0){
								myAlert("删除成功");
								gradeBiz.getAll();
							}else{
								myAlert(data.msg);
							}
						},
						error:function(){
							myAlert(sysErrorMsg);
						}
					});
					
				});
			});

			this.getAll();

			$("#rewardsRuleList").on("click",".iconEdit",function(){

				var json = JSON.parse(decodeURIComponent($(this).parents("td").attr("data-json")));
				gradeBiz.action.awardEdit = true;
				gradeBiz.awardEdit(json);


			});

			$("#btnCreate button").click(function(){

				//初始化
				$(".editDiv").show();
				$("#btnCreate").hide();

				gradeBiz.ruleData = {
		   			rewardsRuleList:[],
		   			punishmentRule:{
		   				finishRuleList:[],
		   				flawRateRuleList:[],
		   				onTimeRuleList:[],
		   				timeoutRateList:[],
		   				permanentRule:{}
		   			},
		   			ruleName : ""
		   		};

		   		$("form")[0].reset();
		   		$(".ruleList").addClass("statusDisable").removeClass("statusEnable");
		   		$("#rewardsRuleList").html("");
		   		$(".punishRuleList tbody").html("");
		   		$(".punishRuleList input:checkbox").attr("checked",false);
		   		$(".permanentRule input:checkbox").attr("checked",true);
		   		$(".punishRuleList .per").text("0");



			});

			$(".cancel").click(function(){
				$(this).parents(".modal").modal("hide");
			});
			

			$("#cityDialog .submit").click(function(){
				if(!gradeBiz.submitting){
					gradeBiz.submitting = true;
					$.ajax({
						url : "/grade/batchBindRuleForCity",
						type :"post",
						data : {ruleId:$("#cityDialog").attr("data-id"),cityIdStr:$("#cityDialog textarea").val().replace(/[\r\n]/g,",")},
						dataType :"json",
						success : function(data){
							if(data.code==0){
								var msg = data.msg;
								if(msg==""&&msg==null)
									msg = "保存成功";
								if(data.data.length>0){
									var tmp = "<table class='table table-striped' style='width:60%'><tr><th>城市ID</th><th>城市ID</th><th>关联方案</th></tr>"
									for(var i=0;i<data.data.length;i++){
										var d = data.data[i];
										tmp+="<tr><td>"+d.cityId+"</td><td>"+d.cityName+"</td><td>"+d.ruleName+"</td></tr>"
									}
									tmp+="</table>";
									msg = "<div>"+msg+"</div>"+tmp;
								}

								myAlert(msg);
								gradeBiz.getAll();
							}else{
								myAlert(data.msg);
							}
							gradeBiz.submitting = false;
						},
						error:function(){
							myAlert(sysErrorMsg);
							gradeBiz.submitting = false;
						}
					});

					$("#cityDialog").modal("hide");
				}
			});

			$("#btnAward").click(function(){
		   		gradeBiz.action.awardEdit = false;
		   		gradeBiz.awardAdd();
		   	});

			$("#awardDialog .submit").click(function(){
				var checked = gradeBiz.awardCheck();
				if(checked===true){
					gradeBiz.awardSave();
					$("#awardDialog").modal("hide");
				}else{
					$("#awardDialog .notice").text(checked).show();
					setTimeout(function(){
						$("#awardDialog .notice").hide();
					},1500);
				}
				
			});

			$("#punishDialog .submit").click(function(){
				var checked = gradeBiz.punishCheck();
	   			if(checked!==true){
	   				$("#punishDialog .notice").text(checked).show();
	   				setTimeout(function(){
	   					$("#punishDialog .notice").hide();
	   				},1500);
	   			}
	   			else{
					gradeBiz.punishSave();
					$("#punishDialog").modal("hide");
				}
			});

			$("#confirmDialog .cancel").click(function(){
				$("#confirmDialog").modal("hide");
			});

			$("#punishDialog .cancel").click(function(){
				$("#punishDialog").modal("hide");
			});

			$("#btnSaveAll").click(function(){

				var checked = gradeBiz.punishAwardCheck();

				if(checked===true)
					checked = gradeBiz.permanentCheck();
				if(checked!==true){
					myAlert(checked);
				}else{

					var d = {};

					var getValue =function(ctrlName){
						if($("#"+ctrlName+"Status").prop("checked")==true){
							d[ctrlName+"Status"] = 1;
						}else{
							d[ctrlName+"Status"] = 0;
						}
						if($("input[name='"+ctrlName+"Radio']:checked").val()=="1"){
							d[ctrlName+"MoneyStatus"] = 1;
							d[ctrlName+"PercentageStatus"] = 0;
						}else{
							d[ctrlName+"MoneyStatus"] = 0;
							d[ctrlName+"PercentageStatus"] = 1;
						}
						
						d[ctrlName+"Percentage"] = $("#"+ctrlName+"Percentage").val();
						if(d[ctrlName+"Percentage"]=="")
							d[ctrlName+"Percentage"] = 0;
						d[ctrlName+"Money"] = $("#"+ctrlName+"Money").val();
						if(d[ctrlName+"Money"]=="")
							d[ctrlName+"Money"] = 0;
					};

					getValue("complaint");
					getValue("doubleHit");
					getValue("finishedAhead");
					getValue("poorRate");
					d.doubleHitSeconds = $("#doubleHitSeconds").val();
					if(d.doubleHitSeconds=="")
						d.doubleHitSeconds = 0;

					gradeBiz.ruleData.punishmentRule.permanentRule = d;
					gradeBiz.ruleData.ruleName = $("#ruleName").val();

					if(!gradeBiz.submitting){
						gradeBiz.submitting = true;
						$.ajax({
							url : "/grade/updateRule",
							type :"post",
							data : {data:JSON.stringify(gradeBiz.ruleData)},
							dataType :"json",
							success : function(data){
								if(data.code==0){
									myAlert("保存成功");

									$(".editDiv").hide();
									$("#btnCreate").show();

									gradeBiz.getAll();

								}else{
									myAlert(data.msg);
								}
								gradeBiz.submitting = false;
							},
							error:function(){
								myAlert(sysErrorMsg);
								gradeBiz.submitting = false;
							}
						});
					}
				}
			});

			$(".punishRuleList").on("click",".iconDel",function(){

				var level = $(this).attr("data-level");
				var type = $(this).attr("data-type");
				myConfirm("删除后无法恢复，确定要删除第"+level+"档吗？",function(){
					for(var i=0;i<gradeBiz.ruleData.punishmentRule[type].length;i++){
						if(gradeBiz.ruleData.punishmentRule[type][i].level==level){
							gradeBiz.ruleData.punishmentRule[type].splice(i,1);
						}
					}
					for(var i=0;i<gradeBiz.ruleData.punishmentRule[type].length;i++){
						gradeBiz.ruleData.punishmentRule[type][i].level = i+1;
					}
					gradeBiz.buildUIPunish(gradeBiz.ruleData.punishmentRule[type],type,1);

				});

			});

			$("#rewardsRuleList").on("click",".iconDel",function(){
				var level = $(this).attr("data-level");
				myConfirm("删除后无法恢复，确定要删除第"+level+"档吗？",function(){
					for(var i=0;i<gradeBiz.ruleData.rewardsRuleList.length;i++){
						if(gradeBiz.ruleData.rewardsRuleList[i].level==level){
							gradeBiz.ruleData.rewardsRuleList.splice(i,1);
						}
					}
					for(var i=0;i<gradeBiz.ruleData.rewardsRuleList.length;i++){
						gradeBiz.ruleData.rewardsRuleList[i].level = i+1;
					}
					gradeBiz.buildUIAward(gradeBiz.ruleData.rewardsRuleList);
				});
				
			});

			$(".editSubTitle").on("click",".btnEdit",function(){
		   		
		   		var jsonList = gradeBiz.ruleData.punishmentRule[$(this).attr("data-type")];
				gradeBiz.action.punishEdit = true;
				gradeBiz.action.punishEditTitle = $(this).parents(".editSubTitle").find("h2").text()+"惩罚档位设置";
				gradeBiz.action.punishEditType = $(this).attr("data-type");
				if(jsonList.length>0)
					gradeBiz.punishEdit(jsonList);

		   	});


		   	$(".editSubTitle").on("click",".btnCreate",function(){
		   		
			   	var	jsonList = [{
			   			level: gradeBiz.ruleData.punishmentRule[$(this).attr("data-type")].length+1,
						punishmentMoney: "",
						punishmentMoneyStatus: 0,
						punishmentPercentage: "",
						punishmentPercentageStatus: 1,
						rateValue: ""
					}];
				
				gradeBiz.action.punishEdit = false;
				gradeBiz.action.punishEditTitle = $(this).parents(".editSubTitle").find("h2").text()+"惩罚档位设置";
				gradeBiz.action.punishEditType = $(this).attr("data-type");
				gradeBiz.punishEdit(jsonList);

		   	});

		   	$(".punishRuleList input:checkbox").click(function(){
		   		if($(this).prop("checked")==true){
		   			$(this).parents(".ruleList").removeClass("statusDisable").addClass("statusEnable");
		   			gradeBiz.ruleData.punishmentRule[$(this).data("name")+"Status"] = 1; 
		   		}else{
		   			$(this).parents(".ruleList").removeClass("statusEnable").addClass("statusDisable");
		   			gradeBiz.ruleData.punishmentRule[$(this).data("name")+"Status"] = 0;
		   		}
		   	});

		   	$("[data-toggle='tooltip']").tooltip();

		   	this.bindStatusCheck();
   		},
   		bindStatusCheck:function(){
   			$(".permanentRule input:checkbox").change(function(){
   				if($(this).prop("checked")==false){
   					$(this).parents("td").next("td").css({"visibility":"hidden"});
   				}else{
   					$(this).parents("td").next("td").css({"visibility":"visible"});
   				}
   			});
   		},
   		awardCheck:function(){
   			var checked = true;

   			$("#awardDialog .number").each(function(){
   				if($(this).val()!=""){
	   				if(parseInt($(this).val())<0){
	   					checked = "奖励方案参数不能设置为负值";
	   					return checked;
	   				}
	   			}
   			});

   			var check = function(typeName,memo){
   				if($("#awardDialog ."+typeName+"Status").prop("checked")==false)
   					return true;
   				if($("#awardDialog ."+typeName+"Status").prop("checked")==true&&$("#awardDialog ."+typeName).val()!="")
   					return true;
   				return "启用指标项"+memo+"不能为空";
   			}
   			if(checked===true)
   				checked = check("finishRate","完成率");
   			if(checked===true)
   				checked = check("flawRate","瑕疵率");
   			if(checked===true)
   				checked = check("onTimeRate","准时率");
   			if(checked===true)
   				checked = check("timeoutRate","超时率");
   			if(checked===true&&$("#awardDialog input:checkbox:checked").length==0){
   				checked = "尚未启用任何指标";
   			}
	   		if(checked===true&&$("#awardDialog input[name='rewardsMoneyRadio']:checked").val()==1&&$("#awardDialog .rewardsMoney").val()==""){
	   			checked = "奖励金额不能为空";
	   		}
   			if(checked===true&&$("#awardDialog input[name='rewardsMoneyRadio']:checked").val()==0&&$("#awardDialog .rewardsPercentage").val()==""){
   				checked = "奖励金额不能为空";
   			}

   			return checked;
   			
   		},
   		punishAwardCheck:function(){
   			
   			var checked = true;
			if($("#ruleName").val()=="")
				checked = "评级方案名称不能为空";
			/*if(checked===true&&gradeBiz.ruleData.rewardsRuleList.length==0){
				checked = "奖励档位尚未设置";
			}*/
			var checkPunish = function(typeNames){
   				for(var i=0;i<typeNames.length;i++){
   					if($("#"+typeNames[i]+" input:checkbox:checked").length==1){
	   					if(gradeBiz.ruleData.punishmentRule[typeNames[i]].length>0){
	   						return true;
	   					}
	   				}
   				}
   				return "启用的惩罚档位尚未设置";
   			};
			if(checked===true&&$(".punishRuleList input:checkbox:checked").length>0){
				checked = checkPunish(["finishRuleList","flawRateRuleList","onTimeRuleList","timeoutRateList"]);
			}
			return checked;

   		},
   		permanentCheck:function(){

   			var checked = true;

   			$(".permanentRule input.number").each(function(){
   				if($(this).val()!=""){
	   				if(parseInt($(this).val())<0){
	   					checked = "固定指标参数不能设置为负值";
	   					return checked;
	   				}
	   			}
   			});

   			if(checked===true&&$(".permanentRule tbody tr").find("input:checkbox:checked").length==0){
   				checked = "固定惩罚指标尚未设置";
   			}

   			if(checked===true){
	   			$(".permanentRule tbody tr").each(function(){
					var dom = $(this);
					var memo = $(dom.find("td")[0]).text();

	   				if(checked===true&&$(dom.find("input:checkbox")).prop("checked")==true){
	   					if(dom.find("input:radio:checked").length==0)
	   						checked = "固定指标"+memo+"配置不得为空";
	   					if(checked===true&&dom.find("input:radio:checked").val()==1&&$(dom.find("input:text")[0]).val()=="")
	   						checked = "固定指标"+memo+"配置不得为空";
	   					if(checked===true&&dom.find("input:radio:checked").val()==0&&$(dom.find("input:text")[1]).val()=="")
	   						checked = "固定指标"+memo+"配置不得为空";
	   				}
	   				
	   			});
	   		}
   			return checked;
   		},
   		punishCheck:function(){
   			var checked = true;
   			var old = "";
   			$("#punishDialog input.number").each(function(){
   				if($(this).val()!=""){
	   				if(parseInt($(this).val())<0){
	   					checked = "指标参数不能设置为负值";
	   					return checked;
	   				}
	   			}
   			});

   			$("#punishDialog .editDialog div").each(function(){
   				var dom = $(this);
   				var level = dom.find(".level").text();
   				var value = dom.find(".rateValue").val();
   				if(gradeBiz.action.punishEdit!==true){
   					var length = gradeBiz.ruleData.punishmentRule[gradeBiz.action.punishEditType].length;
   					if(length>0)
						old = gradeBiz.ruleData.punishmentRule[gradeBiz.action.punishEditType][length-1].rateValue;
   				}

   				if(checked===true&&value=="")
   					checked = "第"+level+"档档位比率不能为空";
   				if(checked===true){
   					//添加的时候需要验证之前档的大小
					if(old!=""){
		   				if(gradeBiz.action.punishEditType=="flawRateRuleList"||gradeBiz.action.punishEditType=="timeoutRateList"){
		   					if(value<=old)
		   						checked = "第"+level+"档比率不能小于上一档";
		   				}else{
		   					if(value>=old)
		   						checked = "第"+level+"档比率不能大于上一档";
		   				}
					}
   				}

   				if(checked===true&&dom.find("input:radio:checked").val()==1&&dom.find(".punishmentMoney").val()==""){
   					checked = "第"+level+"档奖励金额不能为空，请填写元/单";
   				}
   				if(checked===true&&dom.find("input:radio:checked").val()==0&&dom.find(".punishmentPercentage").val()==""){
   					checked = "第"+level+"档奖励金额不能为空，请填写%邮资/单";
   				}
   				old = value;
   				if(checked!==true)
   					return checked;
   			});
   			return checked;
   		},
   		punishEdit:function(json,title){
   			var tmpl = [];
   			for(var i=0;i<json.length;i++){
   				tmpl.push('\
					<div class="col-md-12">\
						档位<span class="level">'+json[i].level+'</span>：<input value="'+json[i].rateValue+'" class="input-sm rateValue number" type="number" min="0" max="100"/>%&nbsp;&nbsp;惩罚金额：\
						<label class="radio-inline"><input value="1" name="punishRadio'+i+'" type="radio" '+(json[i].punishmentMoneyStatus==1?"checked=\"checked\"":"")+'></label>\
						<span class="fa fa-minus">&nbsp;</span><input type="number" min="0" class="input-sm punishmentMoney number" value="'+json[i].punishmentMoney+'" >元/单&nbsp;&nbsp;\
				        <label class="radio-inline"><input value="0" name="punishRadio'+i+'" type="radio" '+(json[i].punishmentPercentageStatus==1?"checked=\"checked\"":"")+'></label>\
				        <span class="fa fa-minus">&nbsp;</span><input type="number" min="0" class="input-sm punishmentPercentage number" value="'+json[i].punishmentPercentage+'">%邮资/单\
			        </div>\
				');
   			}
   			$("#punishDialog .editDialog").html(tmpl.join(""));
   			$("#punishDialog h4").text(gradeBiz.action.punishEditTitle);
   			$("#punishDialog").modal("show");

   		},
   		punishSave:function(){
   			
   			var max = 0,min=100;
   			if(gradeBiz.action.punishEdit == true)
   				gradeBiz.ruleData.punishmentRule[gradeBiz.action.punishEditType] = [];
   			

   			$("#punishDialog .editDialog div").each(function(){
   				var d = {};
   				var dom = $(this);

   				d.level = dom.find(".level").text();
   				d.punishmentMoney = dom.find(".punishmentMoney").val();
   				if(d.punishmentMoney=="")
   					d.punishmentMoney = 0;
   				d.punishmentPercentage = dom.find(".punishmentPercentage").val();
   				if(d.punishmentPercentage=="")
   					d.punishmentPercentage = 0;
   				if(dom.find("input:radio:checked").val()==1){
   					d.punishmentMoneyStatus = 1;
   					d.punishmentPercentageStatus = 0;
   				}else{
   					d.punishmentMoneyStatus = 0;
   					d.punishmentPercentageStatus = 1;
   				}
   				d.rateCprSign = "<";
   				if(gradeBiz.action.punishEditType=="flawRateRuleList"||gradeBiz.action.punishEditType=="timeoutRateList"){
   					d.rateCprSign = ">";
   				}
   				d.rateValue = parseInt(dom.find(".rateValue").val());
   				if(d.rateValue>max)
   					max = d.rateValue;
   				if(d.rateValue<min)
   					min = d.rateValue;
   				gradeBiz.ruleData.punishmentRule[gradeBiz.action.punishEditType.replace("RuleList","RateStatus").replace("List","Status")] = 1;
   				
   				gradeBiz.ruleData.punishmentRule[gradeBiz.action.punishEditType].push(d);
   				
   			});

			
			
			switch(gradeBiz.action.punishEditType){
				case "flawRateRuleList":
					gradeBiz.ruleData.punishmentRule["flawRateTolerance"] = min;
				break;
				case "timeoutRateList":
					gradeBiz.ruleData.punishmentRule["timeoutRateTolerance"] = min;
				break;
				case "onTimeRuleList":
					gradeBiz.ruleData.punishmentRule["onTimeRateTolerance"] = 100-max;
				break;
				case "finishRuleList":
					gradeBiz.ruleData.punishmentRule["finishRateTolerance"] = 100-max;
				break;

			}
	
			
			//console.log(gradeBiz.ruleData.punishmentRule);
   			//console.log(gradeBiz.ruleData.punishmentRule[gradeBiz.action.punishEditType]);

   			this.buildUIPunish(gradeBiz.ruleData.punishmentRule[gradeBiz.action.punishEditType],gradeBiz.action.punishEditType,1);
   		

   		},
   		awardSave:function(){
   			var d = {};
			var setValue = function(typeNames){
				for(var i=0;i<typeNames.length;i++){
					d[typeNames[i]] = $("#awardDialog ."+typeNames[i]).val();
					if(d[typeNames[i]]=="")
						d[typeNames[i]] = 0;
				}
			}
			
			var setStatus = function(typeNames){
				for(var i=0;i<typeNames.length;i++){
					if($("#awardDialog ."+typeNames[i]).prop("checked")==true)
						d[typeNames[i]] = 1;
					else
						d[typeNames[i]] = 0;
				}
			}
			setValue(['finishRate','flawRate','onTimeRate','rewardsMoney','rewardsPercentage','timeoutRate']);
			setStatus(['finishRateStatus','flawRateStatus','onTimeRateStatus','timeoutRateStatus']);
			d.finishRateCprSign = ">=" ;
			d.flawRateCprSign = "<=" ;
			d.onTimeRateCprSign = ">=";
			d.timeoutRateCprSign = "<=";
			
			
			if($("input[name='rewardsMoneyRadio']:checked").val()==1){
				d.rewardsMoneyStatus = 1;
				d.rewardsPercentageStatus = 0;
			}
			else{
				d.rewardsMoneyStatus = 0;
				d.rewardsPercentageStatus = 1;
			}
			
			d.level = $("#awardDialog h4").attr("data-level");

			//console.log(d);
			
			if(gradeBiz.action.awardEdit){
				var list = gradeBiz.ruleData.rewardsRuleList;
				for(var i=0;i<list.length;i++){
					if(d.level==list[i].level)
						list[i] = d;
				}
			}else{
				gradeBiz.ruleData.rewardsRuleList.push(d);
			}

			//更新UI
			gradeBiz.buildUIAward(gradeBiz.ruleData.rewardsRuleList);

   		},
   		awardAdd:function(){
   			var level = gradeBiz.ruleData.rewardsRuleList.length+1;
   			$("#awardDialog h4").text("奖励方案第"+level+"挡").attr("data-level",level);
   			$("#awardDialog").modal("show");
   		},
   		awardEdit:function(json){

			$("#awardDialog h4").text("奖励方案第"+json.level+"挡").attr("data-level",json.level);

   			if(json.finishRateStatus==1)
   				$("#awardDialog .finishRateStatus").attr("checked","checked");
   			if(json.flawRateStatus==1)
   				$("#awardDialog .flawRateStatus").attr("checked","checked");
   			if(json.onTimeRateStatus==1)
   				$("#awardDialog .onTimeRateStatus").attr("checked","checked");
   			if(json.rewardsPercentageStatus==1)
   				$("#awardDialog .rewardsPercentageStatus").attr("checked","checked");
   			if(json.rewardsMoneyStatus == 1)
   				$("#awardDialog input[name='rewardsMoneyRadio']")[0].checked = true;
   			else
   				$("#awardDialog input[name='rewardsMoneyRadio']")[1].checked = true;
   			
			$("#awardDialog .finishRate").val(json.finishRate);
			$("#awardDialog .flawRate").val(json.flawRate);
			$("#awardDialog .onTimeRate").val(json.onTimeRate);
			$("#awardDialog .timeoutRate").val(json.timeoutRate);
			$("#awardDialog .rewardsMoney").val(json.rewardsMoney);
			$("#awardDialog .rewardsPercentage").val(json.rewardsPercentage);

			$("#awardDialog").modal("show");	
		},
		buildUIAward:function(awardList){
			var tmpl = [];
   			for(var a=0;a<awardList.length;a++){
   				var d = awardList[a];
   				tmpl.push("<tr>");
   				tmpl.push("<td>第"+d.level+"档</td>");
   				if(d.finishRateStatus=="1")
   					tmpl.push("<td>"+d.finishRateCprSign+d.finishRate+"%</td>");
   				else
   					tmpl.push("<td>未启用</td>");
   				if(d.onTimeRateStatus=="1")
   					tmpl.push("<td>"+d.onTimeRateCprSign+d.onTimeRate+"%</td>");
   				else
   					tmpl.push("<td>未启用</td>");
   				if(d.timeoutRateStatus=="1")
   					tmpl.push("<td>"+d.timeoutRateCprSign+d.timeoutRate+"%</td>");
   				else
   					tmpl.push("<td>未启用</td>");
   				if(d.flawRateStatus=="1")
   					tmpl.push("<td>"+d.flawRateCprSign+d.flawRate+"%</td>");
   				else
   					tmpl.push("<td>未启用</td>");
   				if(d.rewardsMoneyStatus==1)
   					tmpl.push("<td>"+d.rewardsMoney+"元/单</td>");
   				else
   					tmpl.push("<td>"+d.rewardsPercentage+"%邮资/单</td>")
   				tmpl.push('<td data-json="'+encodeURIComponent(JSON.stringify(d))+'"><a class="iconEdit" data-toggle="tooltip" title="编辑" data-placement="top"><i class="fa fa-edit fa-lg opration-icon"></i></a>\
	            	<a class="iconDel" data-toggle="tooltip" data-level="'+d.level+'" title="删除" data-placement="top"><i class="fa fa-trash-o fa-lg opration-icon"></i></a></td>');
   				tmpl.push("</tr>");
   			}
   			$("#rewardsRuleList").html(tmpl.join(""));
		},
		buildUIPunish:function(punishList,listName,punishStatus){
			//console.log(punishList,listName,punishStatus);
			var tmpl = [];

			var max = 0,min=100;
   			for(var a=0;a<punishList.length;a++){
   				var d = punishList[a];
   				if(d.rateValue>max)
   					max = d.rateValue;
   				if (d.rateValue<min)
   					min = d.rateValue;
   				var d1 = punishList[a+1];
   				
   				tmpl.push("<tr>");
   				tmpl.push("<td>第"+d.level+"档</td>");
   				if(listName=="flawRateRuleList"||listName=="timeoutRateList"){
   					if(d1!=undefined){
	   					d1 = "比率<="+d1.rateValue+"%";
	   					tmpl.push("<td>"+d.rateValue+"%<"+d1+"</td>");
	   				}else{
	   					tmpl.push("<td>>"+d.rateValue+"%</td>");
	   				}
   					
   				}else{
   					if(d1!=undefined){
	   					d1 = d1.rateValue+"%"+d1.rateCprSign+"=比率";
	   				}else{
	   					d1 = "";
	   				}
   					tmpl.push("<td>"+d1+d.rateCprSign+d.rateValue+"%</td>");
   				}
   				if(d.punishmentMoneyStatus==1)
   					tmpl.push("<td>-"+d.punishmentMoney+"元/单</td>");
   				else
   					tmpl.push("<td>-"+d.punishmentPercentage+"%邮资/单</td>")
   				tmpl.push('<td><a class="iconDel" data-type="'+listName+'" data-level="'+d.level+'" data-toggle="tooltip" title="删除" data-placement="top"><i class="fa fa-trash-o fa-lg opration-icon"></i></a></td>');
   				tmpl.push("</tr>");
   			}
   			if(punishStatus==1){
   				$("#"+listName+" input:checkbox")[0].checked = true;
   				$("#"+listName).addClass("statusEnable").removeClass("statusDisable");
   			}else{
   				$("#"+listName+" input:checkbox")[0].checked = false;
   				$("#"+listName).addClass("statusDisable").removeClass("statusEnable");
   			}
   			$("#"+listName+" tbody").html(tmpl.join(""));
   			if(listName=="flawRateRuleList"||listName=="timeoutRateList"){
	   			$("#"+listName+" i.per").text(min);
	   		}else{
	   			$("#"+listName+" i.per").text(100-max);
	   		}

		},
   		buildUI:function(json){
   			
   			$(".editDiv").show();
			$("#btnCreate").hide();

   			$("#ruleName").val(json.ruleName);
   			this.buildUIAward(json.rewardsRuleList);

   			this.buildUIPunish(json.punishmentRule["finishRuleList"],"finishRuleList",json.punishmentRule["finishRateStatus"]);
   			this.buildUIPunish(json.punishmentRule["onTimeRuleList"],"onTimeRuleList",json.punishmentRule["onTimeRateStatus"]);
   			this.buildUIPunish(json.punishmentRule["flawRateRuleList"],"flawRateRuleList",json.punishmentRule["flawRateStatus"]);
   			this.buildUIPunish(json.punishmentRule["timeoutRateList"],"timeoutRateList",json.punishmentRule["timeoutRateStatus"]);

   			var permanentRender =function(typeName){
   				var d = json.punishmentRule.permanentRule;
   				if(d[typeName+"Status"]==1){
   					$("#"+typeName+"Status").attr("checked","checked");
   				}else{
   					$("#"+typeName+"Status").attr("checked","");
   				}
   				$("#"+typeName+"Money").val(d[typeName+"Money"]);
   				$("#"+typeName+"Percentage").val(d[typeName+"Percentage"]);
   				if(d[typeName+"MoneyStatus"]==1){
   					$("input[name='"+typeName+"Radio']")[0].checked = true;
   				}else{
   					$("input[name='"+typeName+"Radio']")[1].checked = true;
   				}

   			};

   			permanentRender("complaint");
   			permanentRender("doubleHit");
   			permanentRender("finishedAhead");
   			permanentRender("poorRate");
   			$(".permanentRule input:checkbox").trigger("change");
   			$("#doubleHitSeconds").val(json.punishmentRule.permanentRule.doubleHitSeconds);

   		},
   		getDetail:function(id){
   			

   			$.ajax({
				url : "/grade/ruleDetailInfo",
				type :"get",
				data : {ruleId:id},
				dataType :"json",
				success : function(data){
					if(data.code==0){
						
						gradeBiz.ruleData = data.data;
						//console.log(gradeBiz.ruleData);
						gradeBiz.buildUI(data.data);
					}else{
						myAlert(data.msg);
					}

				},
				error:function(){
					
				}
			});

   		},
   		getAll:function(pageNum){
   			var pageNum = pageNum||gradeBiz.action.currentPage;

   			var buildPage = function(currentPage,pageTotal){
				var pageHtml = '<li><a data-page="'+(currentPage-1==0?currentPage:(currentPage-1))+'">上一页</a></li>';
				var getPageHtml = function(i){
					return '<li '+(currentPage==i?'class = "active"':'')+'><a data-page="'+i+'">'+i+'</a></li>';
				}
				var preHtml = "";
				for(var i = currentPage; i >= (currentPage-5>1?(currentPage-5):1); i--){
					preHtml = getPageHtml(i)+preHtml;
				}
				pageHtml = pageHtml + preHtml;
				for(var i=currentPage+1; i<= (currentPage+5>pageTotal?pageTotal:(currentPage+5)); i++){
					pageHtml+= getPageHtml(i);
				}
				pageHtml += '<li><a data-page="'+(currentPage+1>pageTotal?pageTotal:(currentPage+1))+'">下一页</a></li>';
				$("#pager").html(pageHtml);

			}

			function getLocalTime(nS) {  

				var addZero = function(d){
					if(d<10){
						d = "0"+""+d;
					}
					return d;
				};
				var now = new Date(parseInt(nS) * 1000);  
				var year=now.getFullYear();     
				var month=now.getMonth()+1;
				var date=now.getDate();     
				var hour=now.getHours();
				var minute=now.getMinutes();
				var second=now.getSeconds();     
		      	return   year+"-"+addZero(month)+"-"+addZero(date)+"  "+addZero(hour)+":"+addZero(minute)+":"+addZero(second);   
		    }     

   			$.ajax({
				url : "/grade/ruleList",
				type :"get",
				data : {pageNum:pageNum,pageSize:20},
				dataType :"json",
				success : function(data){
					if(data.code==0){
						var html = [];
						for(var i=0;i<data.data.length;i++){
							var d = data.data[i];
							html.push('<tr data-id="'+d.ruleId+'">\
								<td>'+d.ruleName+'</td>\
								<td>'+d.cityCount+'</td>\
								<td>'+getLocalTime(d.ctime)+'</td>\
								<td>'+getLocalTime(d.utime)+'</td>\
								<td>\
									<a class="iconEdit" data-toggle="tooltip" title="编辑" data-placement="top"><i class="fa fa-edit fa-lg opration-icon"></i></a>\
			            			<a class="iconLink" data-toggle="tooltip" title="关联城市" data-placement="top"><i class="fa fa-retweet fa-lg opration-icon"></i></a>\
			            			<a class="iconDel" data-toggle="tooltip" title="删除" data-placement="top"><i class="fa fa-trash-o fa-lg opration-icon"></i></a>\
			            		</td>\
								/tr>');
						}
						$("[data-toggle='tooltip']").tooltip();
						$("#list").html(html.join(""));
						$('[data-toggle="tooltip"]').tooltip();
						$("#countLable").text("共"+data.totalCount+"项");
						var pageCount = Math.ceil(data.totalCount/data.pageSize);
						buildPage(pageNum,pageCount);
					}else{
						myAlert(data.msg);
					}

				},
				error:function(){
					myAlert(sysErrorMsg);
				}
			});

   		},

   	};


   	gradeBiz.init();


});

