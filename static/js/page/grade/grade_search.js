require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/head-nav','page/grade/datepicker'], function (nav,datepicker) {

	//初始化月份
	var dt = new Date();
	var year = dt.getFullYear();
	var month = dt.getMonth();
	if(month==0){
		year = year - 1;
		month = 12;
	}
	if(month<10){
		month = "0"+""+month;
	}
	$("#timeStr").val(year+"-"+month);

	datepicker.set({ dateFmt: 'yyyy-MM', isShowToday: false, isShowClear: false});

	var myAlert = function(msg){
		alert(msg);
	};

	var buildHead = function(arr){
		var tmpl = [];
		tmpl.push("<tr>");
		for(var i=0;i<arr.length;i++){
			tmpl.push("<th>"+arr[i]+"</th>");
		}
		tmpl.push("</tr>");
		return tmpl.join("");
	};

	var buildPage = function(currentPage,pageTotal){

		var pageHtml = '<li><a data-page="'+(currentPage-1==0?currentPage:(currentPage-1))+'">上一页</a></li>';
		var getPageHtml = function(i){
			return '<li '+(currentPage==i?'class = "active"':'')+'><a data-page="'+i+'">'+i+'</a></li>';
		}
		var preHtml = "";
		for(var i = currentPage; i >= (currentPage-5>1?(currentPage-5):1); i--){
			preHtml = getPageHtml(i)+preHtml;
		}
		pageHtml = pageHtml + preHtml;
		for(var i=currentPage+1; i<= (currentPage+5>pageTotal?pageTotal:(currentPage+5)); i++){
			pageHtml+= getPageHtml(i);
		}
		pageHtml += '<li><a data-page="'+(currentPage+1>pageTotal?pageTotal:(currentPage+1))+'">下一页</a></li>';
		$("#pager").html(pageHtml);

	};


	var getStations = function(callback){
		$.ajax({
			url : "/grade/getOrgList",
			type :"get",
			data : {pageNum:1,pageSize:20},
			dataType :"json",
			success : function(data){
				if(data.code==0){
					var html = [];
					for(var key in data.org){
						html.push('<option value="'+key+'">'+data.org[key]+'</option>');
					}
					$("#orgId").append(html.join("")).select2();

				}else{
					myAlert("获取加盟站点失败");
				}
			},
			error:function(){
				myAlert(sysErrorMsg);
			}
		});
	};

	var isExporting = false;
	var download = function(orgId,timeStr,url){
		var primary = orgId + new Date().getTime();
		//console.log(orgId,timeStr,url);
        $("#btnDownload").text("正在请求下载,请稍后").addClass("disabled");
        isExporting = true;

        var exportCheck = setInterval(checkExport, 5*1000);

	    function checkExport() {
	        $.ajax({
	            url:"/grade/checkDownload",
	            data:{primary:primary},
	            dataType:'json',
	            success:function (data) {
	                if (data != null && data.code == 0) {
	                    clearInterval(exportCheck);
	                    $("#btnDownload").text("下载").removeClass("disabled");
	                   	isExporting = false;
	                    var urls = data.data.split(",");
	                    for (var i = 0; i < urls.length; i++) {
	                        var downIframe = '<iframe src="'+urls[i]+'" style="border-style:none;width:0;height:0;"></iframe>';
	                        $("body").append(downIframe);
	                    }
	                }
	                if (data == null || data.code == 1) {
	                    myAlert("下载错误,请重试");
	                    clearInterval(exportCheck);
	                    $("#btnDownload").text("下载").removeClass("disabled");
	                    searchRule.data.isExporting = false;
	                }
	            },
	            error:function () {
	                myAlert("下载错误,请稍后重试");
	                clearInterval(exportCheck);
	                $("#btnDownload").text("下载").removeClass("disabled");
	                isExporting = false;
	            }
	        });
	    }

        $.ajax({
            url:url+"?orgId=" + orgId + "&timeStr=" + timeStr + "&primary=" + primary,
            async:true,
            dataType:'json',
            success: function (data) {
                if (data != null) {
                    if (data.code == 0) {
                    	$("#btnDownload").text("正在生成数据，请稍后");
                    	checkExport();
                    }
                } else {
                	myAlert("生成数据失败");
                }
            },
            error: function () {}
        });

	};

	var searchRule = {

		data:{
			head:[
				"加盟站",
				"总订单数",
				"奖励订单数",
				"奖励金额",
				"未完成订单数",
				"订单完成率",
				"未完成惩罚单数",
				"未完成惩罚金额",
				"瑕疵订单数",
				"瑕疵惩罚金额",
				"不准时订单数",
				"不准时惩罚金额",
				"超时订单数",
				"超时惩罚金额",
				"客诉数量",
				"客诉处罚金额",
				"未送达点已送达数量",
				"未送达点已送达处罚金额",
				"连击数量",
				"连击处罚金额",
				"一、二星订单数",
				"一、二星订单处罚金额",
				"邮资奖惩总额"
				],
			column:[
				"orgName",
				"waybillCnt",
				"waybillRewardCnt",
				"waybillRewardAmt",
				"waybillUndeliveredCnt",
				"waybillDeliveredRatio",
				"waybillUndeliveredPunishCnt",
				"waybillUndeliveredPunishAmt",
				"waybillDefectiveCnt",
				"waybillDefectivePunishAmt",
				"waybillUnonTimeCnt",
				"waybillUnonTimePunishAmt",
				"waybillOvertimePunishCnt",
				"waybillOvertimePunishAmt",
				"waybillComplaintCnt",
				"waybillComplaintAmt",
				"waybillClickEarlyCnt",
				"waybillClickEarlyPunishAmt",
				"waybillBatterCnt",
				"waybillBatterPunishAmt",
				"waybillOneTwoStarPunishCnt",
				"waybillOneTwoStarPunishAmt",
				"postageAmt"
			],
			orgId:0,
			timeStr:"",
			primary:""
		},

		init:function(){

			getStations();
			searchRule.getAll();

			$("#btnSearch").click(function(){
				searchRule.getAll();
			});	

			$("#btnDownload").click(function(){
				download($("#orgId").val(),$("#timeStr").val(),"/grade/downloadFranchiseeGrade");
			});	

			$("#pager").on("click","a",function(){
				searchRule.getAll($(this).attr("data-page"));
			})
		},
		getAll:function(page){

			var pageNum = page||1;
			$.ajax({
				url : "/grade/franchiseeGradeData",
				type :"get",
				data : {pageNum:pageNum,pageSize:20,timeStr:$("#timeStr").val(),orgId:$("#orgId").val()},
				dataType :"json",
				success : function(data){
					if(data.code==0){
						var html = [];
						for(var i=0;i<data.gradeViewList.length;i++){
							var d = data.gradeViewList[i];
							html.push("<tr>");
							for(var j=0;j<searchRule.data.column.length;j++){
								html.push('<td>'+d[searchRule.data.column[j]]+'</td>');
							}
							html.push("</tr>");
						}
						$("thead").html(buildHead(searchRule.data.head));
						$("#list").html(html.join(""));
						$("#countLable").text("共"+data.totalCount+"项");
						var pageCount = Math.ceil(data.totalCount/data.pageSize);
						buildPage(pageNum,pageCount);
					}else{
						myAlert(data.msg);
					}

				},
				error:function(){
					myAlert(sysErrorMsg);
				}

			});
		}
	}

	

	var searchOrder = {
		data:{
			head:[
				"订单号",
				"加盟站名称",
				"是否奖励",
				"奖励金额",
				"是否完成",
				"未完成处罚金额",
				"是否超时",
				"超时出发金额",
				"是否瑕疵",
				"瑕疵处罚金额",
				"是否客诉",
				"客诉处罚金额",
				"是否准时",
				"不准时惩罚金额",
				"是否未送达已送达",
				"未送达已送达处罚金额",
				"是否连击",
				"连击处罚金额",
				"是否一、二星订单",
				"一、二星订单处罚金额",
				"原邮资",
				"邮资奖惩金额"
			],
			column:[
				"orderId",
				"orgName",
				"isReward",
				"rewardAmt",
				"isFinish",
				"unfinishPunishAmt",
				"isOverTime",
				"overTimePunishAmt",
				"isDefective",
				"defectivePunishAmt",
				"isComplain",
				"complainPunishAmt",
				"isOnTime",
				"unOnTimePunishAmt",
				"isClickEarly",
				"clickEarlyPunishAmt",
				"isBatter",
				"batterPunishAmt",
				"isOneTwoStar",
				"oneTwoStarPunishAmt",
				"originPostage",
				"postageAmt"
			],
			orgId:0,
			timeStr:"",
			primary:""
		},
		init:function(){
			
			getStations();
			searchOrder.getAll();

			$("#btnSearch").click(function(){
				searchOrder.getAll();
			});	

			$("#btnDownload").click(function(){
				download($("#orgId").val(),$("#timeStr").val(),"/grade/downloadPlatformOrder");
			});	

			$("#pager").on("click","a",function(){
				searchOrder.getAll($(this).attr("data-page"));
			});

			if(importAuth===true){
				$("#importDiv").show();
				$(".dropdown-menu a").click(function(){
					$("form")[0].reset();
					$("#upload-file-modal h4").text("导入"+$(this).text());
					$("#upload-file-type-input").val($(this).attr("data-type"));
					$("#failed-record-text").attr("data-name",$(this).text());
					$("#failed-record-text").hide();
					$("#upload-file-modal").modal("show");
				});
			}


			$("#upload-file-btn").click(function(){
		        var formData = new FormData(document.getElementById("upload-file-form"));
		        $.ajax({
		           type : 'post',
		           url : '/grade/config/upload_file',
		           data : formData,
		           cache: false,
		           dataType: 'json',
		           processData: false,
		           contentType: false,
		           success : function(result){
		              var code = result.code;
		              var data = result.data;
		              var str  = '';
		              if( data !== undefined ){
		                   if( toString.apply(data) ==="[object Object]" ){
		                        if( data['insertFailedIdList'].length || data['noExistsIdList'].length ){
		                            if( data['noExistsIdList'].length ){
		                                str += failStrInfo + "  ID:"+data['noExistsIdList'].toString();
		                            }
		                            if( data['insertFailedIdList'].length ){
		                                prefix = str===""?"":"\n";
		                                str += prefix + "插入失败记录 ID:"+data['insertFailedIdList'].toString();
		                            }
		                        }else{
		                            str = $("#failed-record-text").attr("data-name") + "录入成功/录入失败单号 ID:无";
		                        }
		                   }else{
		                        str = result.msg;
		                   }
		              }else{
		                   str = result.msg;
		              }
		              $("#failed-record-text").val(str).show();
		           }
		        })
		    });

		},
		getAll:function(page){

			var pageNum = page||1;
			$.ajax({
				url : "/grade/franchiseePlatformOrderData",
				type :"get",
				data : {pageNum:pageNum,pageSize:20,timeStr:$("#timeStr").val(),orgId:$("#orgId").val()},
				dataType :"json",
				success : function(data){
					if(data.code==0){
						var html = [];
						for(var i=0;i<data.platformOrderList.length;i++){
							var d = data.platformOrderList[i];
							html.push("<tr>");
							for(var j=0;j<searchOrder.data.column.length;j++){
								var value = d[searchOrder.data.column[j]];
								if(searchOrder.data.column[j].indexOf("is")==0){
									value = (value==1?"是":"否");
								}
								html.push('<td>'+value+'</td>');
							}
							html.push("</tr>");
						}
						$("thead").html(buildHead(searchOrder.data.head));
						$("#list").html(html.join(""));
						$("#countLable").text("共"+data.totalCount+"项");
						var pageCount = Math.ceil(data.totalCount/data.pageSize);
						buildPage(pageNum,pageCount);
					}else{
						myAlert(data.msg);
					}

				},
				error:function(){
					myAlert(sysErrorMsg);
				}

			});
		}
	};



	if(pageType == "searchRule")
		searchRule.init();
	else
		searchOrder.init();


});

