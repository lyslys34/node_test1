require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/head-nav'], function (ui,nav) {

	var sysErrorMsg = "网络遇到问题，请稍后重试";

   	var myConfirm = function(msg,callback){
   		$("#confirmDialog .msg").html(msg);
   		$("#confirmDialog").modal("show");
   		$("#confirmDialog .submit").unbind("click").bind("click",function(){
   			if(callback)
   				callback();
   			$("#confirmDialog").modal("hide");
   		});
   	}

   	var myAlert = function(msg){
   		$("#alertDialog .msg").text(msg);
   		$("#alertDialog").modal("show");
   	}


	var cityBiz = {
		data :{
			cityId: 0,
			ruleId: 0
		},
		action:"list",
		init:function(){
			this.getAll();

			$("#btnSearch").click(function(){
				cityBiz.action = "search";
				var cityId = $("#citySearch").val();
				if(cityId!="")
					cityBiz.searchCityList(cityId);
				else
					$("#solutions").trigger("change");
			});

			$("#solutions").change(function(){
				cityBiz.action = "list";
				cityBiz.getCityList($("#solutions").val());
			});

			$(".submit").click(function(){
				var ruleId = $("#ruleDialog select").val();
				var operation = "change";
				if(cityBiz.data.ruleId==0)
					operation = "add";
				if(ruleId!="0"){
		   			cityBiz.updateCityRule(cityBiz.data.cityId,ruleId,operation);
		   			$("#ruleDialog").modal("hide");
		   		}
		   	});

		   	$(".cancel").click(function(){
		   		$(this).parents(".modal").modal("hide");
		   	});

		   	$("#list").on("click",".iconEdit",function(){
		   		cityBiz.data.cityId = $(this).parents("tr").attr("data-cityId");
		   		cityBiz.data.ruleId = $(this).parents("tr").attr("data-ruleId");
		   		$("#ruleDialog").modal("show");
		   		$("#ruleDialog select").val(cityBiz.data.ruleId);
		   	});

		   	$("#list").on("click",".iconDel",function(){
		   		cityBiz.data.cityId = $(this).parents("tr").attr("data-cityId");
		   		cityBiz.data.ruleId = $(this).parents("tr").attr("data-ruleId");
		   		myConfirm("确定要删除城市("+cityBiz.data.cityId+")和规则("+cityBiz.data.ruleId+")关联吗？",function(){
		   			cityBiz.updateCityRule(cityBiz.data.cityId,cityBiz.data.ruleId,"delete");
		   		});
		   	});

		   	$("#btnDown").click(function(){
		   		window.open("/grade/downloadCityRule?ruleId="+$("#solutions").val());
		   	});

		   	$("#pager").on("click","a",function(){
				cityBiz.getCityList($("#solutions").val(),$(this).attr("data-page"));
		   	});
		},
		updateCityRule:function(cityId,ruleId,operation){
			//operation "delete",解除;"add",绑定;"change",修改
			$.ajax({
				url : "/grade/updateCityRule",
				type :"post",
				data : {cityId:cityId,ruleId:ruleId,operation:operation},
				dataType :"json",
				success : function(data){
					if(data.code==0){
						$("#solutions").trigger("change");
						myAlert("设置城市规则成功");
					}else{
						myAlert(data.msg);
					}
				},
				error:function(){
					myAlert(sysErrorMsg);
				}
			});
		},
		buildSolution:function(list){
			var html = [],ruleId;
			for(var i=0;i<list.length;i++){
				html.push('<option value="'+list[i].ruleId+'">'+list[i].ruleName+'</option>');
			}
			this.getCityList("-1");
			$("#solutions").append(html.join(""));
			
			$("#ruleDialog select").append(html.join(""));

		},
		getAll:function(pageNum){
			var pageNum = pageNum | 1;

			$.ajax({
				url : "/grade/getAllRuleList",
				type :"get",
				data : {pageNo:pageNum},
				dataType :"json",
				success : function(data){
					if(data.code==0){
						cityBiz.buildSolution(data.data);
					}else{
						myAlert(data.msg);
					}

				},
				error:function(){
					myAlert(sysErrorMsg);
				}
			});
		},
		buildCities:function(result,pageNum){
			var pageNum = pageNum||1;
			var data = result.data;
			var buildPage = function(currentPage,pageTotal){
				
				var pageHtml = '<li><a data-page="'+(currentPage-1==0?currentPage:(currentPage-1))+'">上一页</a></li>';
				var getPageHtml = function(i){
					return '<li '+(currentPage==i?'class = "active"':'')+'><a data-page="'+i+'">'+i+'</a></li>';
				}
				var preHtml = "";
				for(var i = currentPage; i >= (currentPage-5>1?(currentPage-5):1); i--){
					preHtml = getPageHtml(i)+preHtml;
				}
				pageHtml = pageHtml + preHtml;
				for(var i=currentPage+1; i<= (currentPage+5>pageTotal?pageTotal:(currentPage+5)); i++){
					pageHtml+= getPageHtml(i);
				}
				pageHtml += '<li><a data-page="'+(currentPage+1>pageTotal?pageTotal:(currentPage+1))+'">下一页</a></li>';
				$("#pager").html(pageHtml).show();

			}

			$("#list").html("");
			$("#countLable").html("");
			if(data!=null){
				var tmpl = [];
				for(var i=0;i<data.length;i++){
					var d = data[i];
					tmpl.push('\
						<tr data-cityId="'+d.cityId+'" data-ruleId="'+d.ruleId+'">\
		                    <td>'+d.cityId+'</td>\
		                    <td>'+d.cityName+'</td>\
		                    <td>'+d.ruleName+'</td>\
		                    <td>\
		                        <a class="iconEdit" data-toggle="tooltip" title="编辑" data-placement="top"><i class="fa fa-edit fa-lg opration-icon"></i></a>\
		                        <a style="'+(d.ruleId==0?"display:none":"")+'" class="iconDel" data-toggle="tooltip" title="删除" data-placement="top"><i class="fa fa-trash-o fa-lg opration-icon"></i></a>\
		                    </td>\
	                	</tr>');
				}
				$("#list").html(tmpl.join(""));
			}
			if(cityBiz.action=="list"){
				var pageCount = Math.ceil(result.totalCount/result.pageSize);
				buildPage(parseInt(pageNum),pageCount);
				$("#countLable").html("共"+result.totalCount+"项");
			}else{
				$("#countLable").html("共"+data.length+"项");
				$("#pager").hide();
			}

			
			
		},
		getCityList:function(ruleId,pageNum){

			var pageNum = pageNum||1;
			
				$.ajax({
					url : "/grade/queryCityListByRuleId",
					type :"get",
					data : {pageNum:pageNum,ruleId:ruleId},
					dataType :"json",
					success : function(data){
						if(data.code==0){
							cityBiz.buildCities(data,pageNum);
						}else{
							myAlert(data.msg);
						}

					},
					error:function(){
						myAlert(sysErrorMsg);
					}
				});
		},
		searchCityList:function(cityId){
			
			$.ajax({
					url : "/grade/queryCityRuleByCityId",
					type :"get",
					data : {cityId:cityId},
					dataType :"json",
					success : function(data){
						if(data.code==0){
							var list = [];
							list.push(data.data);
							cityBiz.buildCities({data:list});
						}else{
							myAlert(data.msg);
						}

					},
					error:function(){
						myAlert(sysErrorMsg);
					}
				});
		}

	};

	cityBiz.init();






});

