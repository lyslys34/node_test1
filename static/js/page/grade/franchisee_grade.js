require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/root', 'module/validator',  'module/cookie', 'page/grade/datepicker'], function (t, validator,cookie, datepicker) {
	$(document).ready(function() {
		datepicker.set({ dateFmt: 'yyyy-MM', isShowToday: false, isShowClear: false});
		
		$('#js_submit_btn').click(function() {
	        submit();
	    });

	    var exportCheck;
        var isExporting = false;
        var primary = getPrimaryForExport();

        $(".js_export_btn").click(function () {
            exportData();
        });

        function getPrimaryForExport() {
            return orgId + new Date().getTime();
        }

        window.onbeforeunload = function () {
            if (isExporting) {
                return "正在下载数据";
            }
        }

        function exportData() {
            $(".js_export_btn").text("正在下载,请稍后").addClass("disabled");
            isExporting = true;

            $.ajax({
                url:"/grade/downloadFranchiseeGrade?orgId=" + orgId + "&timeStr=" + timeStr + "&primary=" + primary,
                async:true,
                dataType:'json',
                success: function (data) {
                    if (data != null) {
                        if (data.code == 1) {
//                            alert(data.msg);
//                            clearInterval(exportCheck);
//                            $(".js_export_btn").val("下载").removeClass("disabled");
//                            isExporting = false;
//                            return false;
                        }
                    } else {

                    }
                },
                error: function () {}
            });
            exportCheck = self.setInterval(checkExport, 1*1000);
        }

        function checkExport() {
            $.ajax({
                url:"/grade/checkDownload",
                data:{primary:primary},
                dataType:'json',
                success:function (data) {
                    if (data != null && data.code == 0 && !validator.isBlank(data.data)) {
                        clearInterval(exportCheck);
                        $(".js_export_btn").text("下载").removeClass("disabled");
                        isExporting = false;
                        window.location.href = data.data;
                    }

                    if (data == null || data.code == 1) {
                        alert("下载错误,请重试");
                        clearInterval(exportCheck);
                        $(".js_export_btn").text("下载").removeClass("disabled");
                        isExporting = false;
                    }
                },
                error:function () {
                    alert("下载错误,请稍后重试");
                    clearInterval(exportCheck);
                    $(".js_export_btn").text("下载").removeClass("disabled");
                    isExporting = false;
                }
            });
        }
	});
		
});