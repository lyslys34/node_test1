/**
 * Created by lixiangyang on 15/10/22.
 */
require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion
});

require(['module/root'], function (t) {
    $('#js_submit').click(function() {
        submit();
    });


    function submit() {
        var items = $('.js_item');
        var configs = [];
        $.each(items, function(index, item) {
            var status = $(item).find('.js_status').prop('checked') ? 1 : 0;
            var value = $(item).find('.js_value').val();
            var key = $(item).data('key');
            var obj = {
                key: key,
                value: value,
                status: status
            };
            configs.push(obj);
        });

        var validateResult = validate(configs);

        if(validateResult) {
            save(configs);
        }
    }

    function validate(obj) {
        for (var i = 0; i < obj.length; i++) {
        	if (isNaN(obj[i].value)) {
        		return false;
        	}
        }
        
        return true;
    }

    function save(data) {
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/grade/config/save",
            data: JSON.stringify(data),
            contentType : 'application/json;charset=utf-8',
            success : function(r){
                if (r && r.code == 0) {
                    alert(r.msg);
                } else {
                	alert(r.msg);
                }
            }

        });
    }
    
    $('.js_status').on('change', function() {
    	var $checkbox = $(this);
    	var checked = $checkbox[0].checked;
    	var $input = $checkbox.parents('td').siblings('td').find('.js_value');
		$input.prop('disabled', checked ? false : true);
    })

});

