require.config({
    baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
    shim: {
        'lib/bootstrap': {
            deps: ['lib/jquery']
        }
    }
});

require(['module/root', 'module/validator','module/cookie', 'page/grade/datepicker'], function (t, validator,cookie,datepicker) {
  $(document).ready(function () {
    var failStrInfo = "";

    datepicker.set({ dateFmt: 'yyyy-MM', isShowToday: false, isShowClear: false});
    
    $("#upload-exception-order-btn").click(function(){
        $("#upload-file-type-input").val("1");
        $("#upload-file-modal").modal();
        failStrInfo = "异常单录入成功/录入失败单号";
    })
    $("#upload-complaint-order-btn").click(function(){
        $("#upload-file-type-input").val("3");
        $("#upload-file-modal").modal();
        failStrInfo = "投诉单录入成功/录入失败单号";
    });

    $("#upload-undelived-order-btn").click(function(){
        $("#upload-file-type-input").val("2");
        $("#upload-file-modal").modal();
        failStrInfo = "未送达单录入成功/录入失败单号";
    });

    $("#upload-org-blacklist-btn").click(function(){
        $("#upload-file-type-input").val("4");
        $("#upload-file-modal").modal();
        failStrInfo = "黑名单录入成功/录入失败单号";
    });

    $("#upload-file-cancel-btn").on("click",function(){
        clearUploadForm();
    })

    //文件上传结果处理第一方法
    $("#upload-file-btn").click(function(){
        var formData = new FormData(document.getElementById("upload-file-form"));
        $.ajax({
           type : 'post',
           url : '/grade/config/upload_file',
           data : formData,
           cache: false,
           dataType: 'json',
           processData: false,
           contentType: false,
           success : function(result){
              var code = result.code;
              var data = result.data;
              var str  = '';
              if( data !== undefined ){
                   if( toString.apply(data) ==="[object Object]" ){
                        if( data['insertFailedIdList'].length || data['noExistsIdList'].length ){
                            if( data['noExistsIdList'].length ){
                                str += failStrInfo + "  ID:"+data['noExistsIdList'].toString();
                            }
                            if( data['insertFailedIdList'].length ){
                                prefix = str===""?"":"\n";
                                str += prefix + "插入失败记录 ID:"+data['insertFailedIdList'].toString();
                            }
                        }else{
                            str = failStrInfo + " ID:无";
                        }
                   }else{
                        str = result.msg;
                   }
              }else{
                   str = result.msg;
              }
              document.getElementById("failed-record-text").style.display="block";
              $("#failed-record-text").val(str);
           }
        })
    });

    function closeUploadDialog(){
        $("#upload-file-cancel-btn").click();
    }

    function clearUploadForm(){
        $("#upload-file-input").val("");
        $("#upload-file-type-input").val("");
        $("#failed-record-text").val("");
        document.getElementById("failed-record-text").style.display="none";
    }

    var exportCheck;
    var isExporting = false;
    var primary = getPrimaryForExport();

    $(".js_export_btn").click(function () {
        exportData();
    });

    function getPrimaryForExport() {
        return orgId + new Date().getTime();
    }

    window.onbeforeunload = function () {
        if (isExporting) {
            return "正在下载数据";
        }
    }

    function exportData() {
        $(".js_export_btn").text("正在下载,请稍后").addClass("disabled");
        isExporting = true;

        $.ajax({
            url:"/grade/downloadPlatformOrder?orgId=" + orgId + "&timeStr=" + timeStr + "&primary=" + primary,
            async:true,
            dataType:'json',
            success: function (data) {
                if (data != null) {
                    if (data.code == 0) {
//                        alert(data.msg);
//                        clearInterval(exportCheck);
//                        $(".js_export_btn").text("下载").removeClass("disabled");
//                        isExporting = false;
//                        return false;
                    }
                } else {

                }
            },
            error: function () {}
        });
        exportCheck = self.setInterval(checkExport, 5*1000);
    }

    function checkExport() {
        $.ajax({
            url:"/grade/checkDownload",
            data:{primary:primary},
            dataType:'json',
            success:function (data) {
                if (data != null && data.code == 0 && !validator.isBlank(data.data)) {
                    clearInterval(exportCheck);
                    $(".js_export_btn").text("下载").removeClass("disabled");
                    isExporting = false;
                    var urls = data.data.split(",");
                    var downTemp = $('#down_temp');
                    for (var i = 0; i < urls.length; i++) {
                        var downIframe = '<iframe src="'+urls[i]+'" style="border-style:none;width:0;height:0;"></iframe>';
                        downTemp.append(downIframe);
//                        window.location.href = urls[i];
                    }
                }

                if (data == null || data.code == 1) {
                    alert("下载错误,请重试");
                    clearInterval(exportCheck);
                    $(".js_export_btn").text("下载").removeClass("disabled");
                    isExporting = false;
                }
            },
            error:function () {
                alert("下载错误,请稍后重试");
                clearInterval(exportCheck);
                $(".js_export_btn").text("下载").removeClass("disabled");
                isExporting = false;
            }
        });
    }

  });

});
