require.config({
  baseUrl: MT.STATIC_ROOT + '/js',urlArgs: 'ver='+ pageVersion,
  shim: {
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core':['page/common'],
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget':['page/common'],
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position':['page/common'],
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu':['page/common'],
            'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete':['page/common',
                'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
                'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
                'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
                'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu'
            ],
            'lib/autocomplete' : {
                deps : ['page/common']
            },
            'lib/bootstrap': {
                deps: ['lib/jquery']
            }
  }
});
require(['module/root', 'module/validator',  'module/cookie', 'page/common','lib/datepicker','lib/autocomplete'], function (t, validator,cookie, common,datepicker,autocomplete) {

    var cityNameMap = {};
    var cityIdMap = {};
    var bmUserId;
    var bmStaffBlacklistId;
    var opType;
    var opDescription;
//    var showBtn='<div class="row" style="margin:0px;width:450px;" id="addDescription">'+
//                     '<div class="form-group">'+
//                         '<label class="control-label">拉黑原因</label>'+
//                         '<span class="pull-right hidden" id="pullblack-reason" style="color:#f00">请填写拉黑原因</span>'+
//                         '<textarea rows="5" id="opDescription" class="form-control" style="width:450px" required="true" placeholder="请输入拉黑原因">'+
//                         '</textarea>'+
//                     '</div>'
//                 '</div>';
    $(document).ready(function(){
        initSearchCity();
    });

     $('#phoneSearch').bind('keypress',function(event){
         if(event.keyCode == "13")
         {
             $("#search").click();
             return false;
         }
     });

     $('#onlyPhoneSearch').bind('keypress',function(event){
         if(event.keyCode == "13")
         {
              $("#selectPhone").click();
              return false;
         }
     });

    $("#search").click(function(){
        url="/rider/getBlacklistList";
        phone=$("input[name='phone']").val();
        window.location.href=url+"?phone="+phone+"&searchType=1";
    });

    var showUserDetailDivStart='<div style="width:450px;height:auto; margin:10px 0px;" id="showDiv">';
    var divEnd='</div>';

    //var showAddBlacklistInputDivStart='';
    var showAddBlacklistInput='<div class="row" style="margin:0px;width:450px;">'+
                                     '<div class="form-group" style="margin:5px 0px;">'+
                                         '<label  class="control-label" >拉黑时间</label>'+
                                     divEnd+
                                     '<div class="col-lg-6" style="width:300px;" id="DayLimit">'+
                                         '<label class="radio-inline">'+
                                             '<input type="radio" name="dayLimitRadio" id="inlineRadio1" value="" style="margin-top:1%"> 拉黑<input type="text" name="dayLimit" id="dayLimitText" style="width:30px;" disabled="disabled"/>天'+
                                         '</label>'+
                                         '<label class="radio-inline">'+
                                              '<input type="radio" name="dayLimitRadio" id="inlineRadio2" value="-1"> 永久拉黑'+
                                          '</label>'+
                                     divEnd+
                                  divEnd+
                                  '<div class="row" style="margin:0px;width:450px;">'+
                                      '<div class="form-group" style="margin:5px 0px;">' +
                                          '<label  class="control-label" >拉黑原因</label>'+
                                      divEnd+
                                      '<div class="col-lg-6" style="width:300px;" id="showBlacklistOpLog">'+
                                          '<div class="checkbox">'+
                                              '<label>'+
                                                  '<input type="checkbox" name="remark" id="optionsRadios1" value="商家或用户真实投诉">商家或用户真实投诉'+
                                              '</label>'+
                                          divEnd+
                                          '<div class="checkbox">'+
                                              '<label>'+
                                                  '<input type="checkbox" name="remark" id="optionsRadios2" value="拒接美团众包客服电话或提供虚假信息">拒接美团众包客服电话或提供虚假信息'+
                                              '</label>'+
                                          divEnd+
                                          '<div class="checkbox">'+
                                              '<label>'+
                                                  '<input type="checkbox" name="remark" id="optionsRadios3" value="联合或盗用商家发送虚假单子进行骗取补贴">联合或盗用商家发送虚假单子进行骗取补贴'+
                                              '</label>'+
                                          divEnd+
                                          '<div class="checkbox">'+
                                              '<label>'+
                                                  '<input type="checkbox" name="remark" id="optionsRadios4" value="本人接单非本人派送">本人接单非本人派送'+
                                              '</label>'+
                                          divEnd+
                                          '<div class="checkbox">'+
                                              '<label>'+
                                                  '<input type="checkbox" name="remark" id="optionsRadios6" value="损害美团平台信誉">损害美团平台信誉'+
                                              '</label>'+
                                          divEnd+
                                          '<div class="checkbox">'+
                                              '<label>'+
                                                  '<input type="checkbox" name="remark" id="optionsRadios5" value="">其它'+
                                              '</label>'+
                                          divEnd+
                                          '<div id="showOtherDesc">'+divEnd+
                                          '<p id="showRemarkWarnMsg" style="color:red"></p>'+
                                      divEnd+
                                  divEnd;

    //var notFoundMsg='';
    //var notVlaidUserMsg='';

    var descSplitChar="$$";

    $("#showDiv2").on('click','#inlineRadio1',function(){
        //输入拉黑天数的radio Btn
        $("#dayLimitText").removeAttr("disabled");
    })

    $("#showDiv2").on('click','#inlineRadio2',function(){
        $("#dayLimitText").val("");
        $("#inlineRadio1").val("");
        $("#dayLimitText").attr("disabled","disabled");
    })

    //验证是否是正整数
    function isNum(num){
        //var rex="";
        if(!num.match(/^[1-9]\d*$/)){
            alert("非法数字");
            //$("#showDiv2").on('focus',"#dayLimitText",function(){
            //});
            $("#dayLimitText").val("");
            $("#inlineRadio1").val("");
            return false;
        }
        return true;
    }

    $("#showDiv2").on('blur','#dayLimitText',function(){
        //alert("选择的天数："+$("#dayLimitText").val());
        var day=$("#dayLimitText").val();
        if(isNum(day)){
           if(day<0||day>100){
               alert("非法数字");
               //$("#showDiv2").on('focus','#dayLimitText',function(){});
               $("#dayLimitText").val("");
               $("#inlineRadio1").val("");
           }else{
               $("#inlineRadio1").val(day);
           }
        }
    })

    $("#selectPhone").click(function(){
        url="/rider/getStaffByPhone.ajax";
        //var phone=$("input[name='phone']").val();
        var phone=$("#onlyPhoneSearch").val();
        postData={phone:phone};
            t.utils.post(url, postData, function (r) {
                if (r.httpSuccess) {
                    clearPanel();
                    if(r.data.success ) {
                        //$("#showDiv").css("visibility","visible");
                        if(r.data.resultCode==1){
                            $("#showMsg").html("提示:无对应配送员");
                            //$("#edit-container").append(notFoundMsg);
                        }else{
                            //$("#showDiv").css("visibility","visible");
                            var staffView=r.data.data;
                            $("#id").val(staffView.bmUserid);

                            var table="<table style='width:400px; margin-bottom:10px' rules='all'><tr style='background-color:#eee;'>";
                            table=table+"<th style='width:50%;text-align:center'>姓名</th><th style='width:50%;text-align:center'>手机号</th>";
                            table=table+"<tr> ";
                            table=table+"<td style='width:50%;text-align:center' bmName='"+staffView.bmUserName+"' bmPhone='"+staffView.phone+"' id='userInfo'>"+staffView.bmUserName+"</td>";
                            table=table+"<td style='width:50%;text-align:center'>"+staffView.phone+"</td>";
                            table=table+"</tr> ";
                            table=table+"</table> ";

                            var userDetail='<div class="row" style="margin:0px;width:450px;">'+
                                                '<div class="form-group" style="margin:5px 0px;">'+
                                                    '<label  class="control-label" >当前要拉黑的配送员</label>'+
                                                '</div>'+
                                                '<div class="col-lg-6" style="width:300px;" id="showUserTable">'+
                                                    table+
                                                '</div>' +
                                            '</div>';

                            var addBlacklistLogs=staffView.addBlacklistLog;
                            var addBlacklistLogArr=eval(addBlacklistLogs);
                            var blacklistTable="<table style='width:400px; margin-bottom:10px' rules='all'><tr style='background-color:#eee;'>";
                            blacklistTable=blacklistTable+"<th style='width:50%;text-align:center'>拉黑时限</th><th style='width:50%;text-align:center'>拉黑原因</th>";

                            if(addBlacklistLogArr.length==0){
                                blacklistTable=blacklistTable+"<tr> ";
                                blacklistTable=blacklistTable+"<td style='width:50%;text-align:center'>无</td>";
                                blacklistTable=blacklistTable+"<td style='width:50%;text-align:center'>无</td>";
                                blacklistTable=blacklistTable+"</tr> ";

                            }else{
                                for(var i=0;i<addBlacklistLogArr.length;i++){
                                    var blacklist=addBlacklistLogArr[i];
                                    blacklistTable=blacklistTable+"<tr> ";
                                    var desc=blacklist.description;  //格式：原因$$dayLimit:天数
                                    var descArr=desc.split(descSplitChar);

                                    var dayLimit=['dayLimit',-1];//兼容老的拉黑用户的驳回原因没有拼接

                                    if(descArr.length>1){
                                        dayLimit=descArr[1].split(":");
                                    }

                                    var dayLimitStr="";   //拉黑时限字符串
                                    if(dayLimit[1]==-1){
                                        dayLimitStr="永久拉黑";
                                    }else{
                                        dayLimitStr=dayLimit[1]+"天";
                                    }

                                    blacklistTable=blacklistTable+"<td style='width:50%;text-align:center'>"+dayLimitStr+"</td>";
                                    blacklistTable=blacklistTable+"<td style='width:50%;text-align:center'>"+descArr[0]+"</td>";
                                    blacklistTable=blacklistTable+"</tr> ";
                                }

                            }
                            blacklistTable=blacklistTable+'</table>';

                            var userBlacklistLog='<div class="row" style="margin:0px;width:450px;">'+
                                                     '<div class="form-group" style="margin:5px 0px;">'+
                                                         '<label  class="control-label" >历史拉黑记录</label>'+
                                                     '</div>'+
                                                     '<div class="col-lg-6" style="width:300px;" id="showBlacklistOpLog">'+
                                                         blacklistTable+
                                                     '</div>'+
                                                 '</div>';
                            //用户详情的div块儿（用户姓名手机号块和用户拉黑记录块）
                            var showUserDetailDiv=showUserDetailDivStart+userDetail+userBlacklistLog+divEnd;
                            //var showUserDetailDiv=userDetail+userBlacklistLog;
                            $("#showDiv1").append(showUserDetailDiv);

                            if(staffView.status==1){
                                $("#showStausMsg").html("提示：该配送员已经被拉黑");
                                //$("#edit-container").append(notVlaidUserMsg);
                            }else{
                                //var showAddBlacklistInputDiv=showAddBlacklistInputDivStart+showAddBlacklistInput+divEnd;
                                $("#showDiv2").append(showAddBlacklistInput);
                                $(".modal-footer").children(".btn-success").css("visibility","visible");
                            }

                            //$("#create-container").append(showBtn);

                        }
                        return false;
                    } else {
                        $("#showMsg").html(r.data.resultMsg);
                        return false;
                    }

                } else {
                    showMsg("提交失败,系统错误!");
                    return false;
                }
            });
    });

    var textarea='<textarea rows="3" id="otherDesc" name="otherDisaggreDesc" placeholder="Enter text" style="width:350px;" class="form-control"></textarea>';

    $("#showDiv2").on('click','#optionsRadios5',function(){
        if($(this).is(":checked")==true){
            $("#showOtherDesc").append(textarea);
            needAdditionalDesc=true; //如果选择了其他，则需要验证是否填写了输入框
        }else{
            $("#showRemarkWarnMsg").html("");
            $("#otherDesc").remove();
            needAdditionalDesc=false; //如果选择了其他，则需要验证是否填写了输入框
        }
    })

    $("#")
    var separator=';' //驳回原因的分隔符
    var needAdditionalDesc=false; //是否需要添加其它原因
    function getCheckboxVal(){

        var remark="";
        var temp="";
        var a = document.getElementsByName("remark");
        var len=a.length;
        for ( var i = 0; i < len; i++) {
            if (a[i].checked) {
                temp = a[i].value;
                //选择了其他，判断文本框中是否输入了内容
                if(isBlank(temp)){
                    continue;
                }
                remark = remark + separator +temp;
            }
        }

        if(!isBlank(remark)){
            remark=remark.substring(1);
        }

        return remark;

    }

    //新建
    $("#create").click(function(){
        _showEditPanel();
    });
    //移出黑名单
    $(".del").click(function(){
        var bmUserName=$(this).attr("bmName");
        var bmPhone=$(this).attr("bmPhone");
        bmStaffBlacklistId=$(this).attr("delId");
        bmUserId=$(this).attr("delUserId");
        opType=2;
        opDescription="";
        showTitle="现配送员 "+bmUserName+" 正处于拉黑状态，确认移出黑名单？"
        $("#showConfirmTitle").html(showTitle);
        $("#showConfirm").modal();
    });

    $("#confirm").click(function(){
        updateStaffBlacklist(bmUserId,bmStaffBlacklistId,opType,opDescription,dayLimit);
        $("#showConfirm").modal('hide');
    });
    //更新黑名单 --opType 1表示移入黑名单 2表示移出黑名单
    function updateStaffBlacklist(bmUserId,bmStaffBlacklistId,opType,opDescription,dayLimit){
        var url = "/rider/updateStaffBlacklist.ajax";
        var postData = {bmUserId:bmUserId,
                        bmStaffBlacklistId:bmStaffBlacklistId,
                        opType:opType,
                        opDescription:opDescription,
                        dayLimit:dayLimit};
        t.utils.post(url, postData, function (r) {
            if (r.httpSuccess) {
                if(r.data.success ) {
                    showReturnMsg(r.data.resultMsg,"success");
                    setTimeout('$("#selectBlacklist").click()',2000);
                    return false;
                } else {
                    if(r.data.resultCode==306){
                        showReturnMsg(r.data.resultMsg,"danger");
                    }else{
                        showReturnMsg("操作失败，请重新操作","danger");
                    }
                    setTimeout('$("#selectBlacklist").click()',2000);
                    return false;
                }
            } else {
                showMsg("提交失败,系统错误!");
                return false;
            }
        });
    }
    var dayLimit=-1; //拉黑时限
    function _showEditPanel() {
        var body = $("#create-container");
        body.removeClass("hide");
        $("input[name='phone']").val("");
        clearPanel();
        var buttons = [];
        buttons =  [
            t.ui.createDialogButton('ok','拉黑配送员', function () {

                opDescription=getCheckboxVal();
                var addDesc=$("#otherDesc").val();

                if(needAdditionalDesc){

                    if(isBlank(addDesc)){
                        $("#showRemarkWarnMsg").html("请填写拉黑原因");
                            return false;
                    }

                    //验证输入驳回原因长度，不能超过150字符
                    if(addDesc.length>150){
                        $("#showRemarkWarnMsg").html("输入的原因长度不能大于150个字符！");
                        return false;
                    }

                    if(isBlank(opDescription)){
                        opDescription=addDesc;
                    }else{
                        opDescription=opDescription+ separator +addDesc;
                    }

                }

                if(isBlank(opDescription)){
                    $("#showRemarkWarnMsg").html("请选择或者填写拉黑原因");
                    return false;
                }

                bmUserId=$("#id").val();
                opType=1;
                bmStaffBlacklistId=0;
                dayLimit=$("input[name='dayLimitRadio']:checked").val();
                var bmUserName=$("#userInfo").attr("bmName");
                var bmPhone=$("#userInfo").attr("bmPhone");

                var showTitle="";

                if(isBlank(dayLimit)){
                    alert("请选择拉黑天数");
                    return false;
                }

                if(dayLimit==-1){
                    showTitle="确认将配送员 "+bmUserName+" 永久拉黑？"
                }else{
                    showTitle="确认将配送员 "+bmUserName+" 拉黑"+dayLimit+"天？"
                }
                $("#showConfirmTitle").html(showTitle);
                closePanel();
                $("#showConfirm").modal();
                return true;
            }),
            t.ui.createDialogButton('close','取消', function(){
                closePanel();
                return true;
            })
        ];
        var dlg = t.ui.showModalDialog({
            body: body,buttons: buttons,
            style:{".modal-content":{"width":"530px"},".btn-success":{"visibility":"hidden"}}
        });
    }

    function getCheckPostData(checkType){

        var addDesc=$("#otherDesc").val();

        if(checkType==disagreeType ){

            desc=getCheckboxVal();

            if(needAdditionalDesc){
                if(isBlank(addDesc)){
                    alert("请输入其他原因");
                    return null;
                }

                 //验证输入驳回原因长度，不能超过150字符
                if(addDesc.length>150){
                    alert("输入的原因长度不能大于150个字符！");
                    return null;
                }

                if(isBlank(desc)){
                    desc=addDesc;
                }else{
                    desc=desc+ separator +addDesc;
                }
            }

            if(isBlank(desc)){
                alert("请选择驳回原因");
                return null;
            }

        }

        postData={
                  userId:userId,
                  id:id,
                  checkedType:checkType,
                  remark:desc}

        return postData;
    }

    function closePanel(){
        $("#create-container :hidden").val("0");
        $("#showDiv").css("visibility","hidden");
        clearPanel();
    }

    function clearPanel(){
        //$("#inlineRadio1").val("");
        $("#showDiv").remove();
        $("#showDiv2").html("");
        //$("#showUserTable").html("");
//        $("#showMsg").html("");
//        $("#showStausMsg").html("");
        $("#showMsg").html("");
        $("#showStausMsg").html("");
        //$("#showRemarkWarnMsg").html("");
        //$("#addDescription").remove();

        $(".modal-footer").children(".btn-success").css("visibility","hidden");
    }

    function initSearchCity() {
        myPOST('/org/citySearchList', {}, function (r) {
             if (r.httpSuccess) {
                if(r.data && r.data.code == 0) {
                    $(".js_city_name").autocomplete({
                        delay:100,
                        source : r.data.data,
                        focus: function( event, ui ) {
                            $( ".js_city_name" ).val( ui.item.value );
                            $( ".js_city_id" ).val( ui.item.id );
                            return false;
                        },
                        select: function(event, ui) {
                            $(".js_city_name").val(ui.item.value );
                            $(".js_city_id").val(ui.item.id );
                        },
                        change: function(event, ui) {
                            var cityName = $(".js_city_name").val();
                            if(validator.isBlank(cityName)) {
                                $(".js_city_id").val(0);
                            }
                        },
                        open: function(event, ui) {
                            $(".js_city_id").val('0');
                        }

                    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                        return $( "<li>" )
                            .append( "<a>" + item.value + "</a>" )
                            .appendTo( ul );
                    };
                    if(r.data.data) {
                        var cityId = $(".js_city_id").val();
                        $.each(r.data.data, function(index, item) {
                            cityNameMap[item.value] = item;
                            cityIdMap[item.id] = item;
                            if(cityId) {
                                if(cityId == item.id) {
                                $(".js_city_name").val(item.value);
                              }
                            }
                        });
                    }
                    var riderId = $(".js_city_id").val();
                    $(".js_city_name").val(cityIdMap[riderId] != null && typeof cityIdMap[riderId] != 'undefined' ? cityIdMap[riderId].value : "");

                } else {
                    alert("获取城市信息失败，" + r.data.msg);
                }
            } else {
                //alert(JSON.stringify(r));
                alert('获取城市信息失败，系统错误');
            }
        });
    }

    function myPOST(url, data, callback)  // 发送POST请求
    {
        if(typeof(data) == 'function') { callback = data; data = null; }
        $.ajax({ type: 'POST', url: url, data:data, success:function(r) {
            callback({ httpSuccess: true, data: r });
        }, error: function(XmlHttpRequest, textStatus, errorThrown) {
            callback({ httpSuccess: false, statusCode:XmlHttpRequest.status });
        } });
    }

    function isBlank(value) {
        if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
                return this.replace(/^\s+|\s+$/g, '');
            }
        }
        return value==null || value.trim() == "" || value==undefined;
    }

    function showReturnMsg(msg,returnType) {
        if(returnType=="success"){
            $("#showReturnRes").children(".modal-dialog").children(".modal-content").css("background-color","#dff0d8");
        }
        if(returnType=="danger"){
            $("#showReturnRes").children(".modal-dialog").children(".modal-content").css("background-color","#f2dede");
        }
        $("#alert_msg").html("<h4>"+msg+"</h4>");
//
//        if(returnType=="success"){
//            $("#alert_msg").append('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="right:0px;"><span aria-hidden="true">&times;</span></button>'+msg+'</div>');
//        }
//        if(returnType=="danger"){
//            $("#alert_msg").append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="right:0px;"><span aria-hidden="true">&times;</span></button>'+msg+'</div>');
//        }
//        if(returnType=="info"){
//            $("#alert_msg").append('<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="right:0px;"><span aria-hidden="true">&times;</span></button>'+msg+'</div>');
//        }
//        if(returnType=="warning"){
//            $("#alert_msg").append('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="right:0px;"><span aria-hidden="true">&times;</span></button>'+msg+'</div>');
//        }
        $("#showReturnRes").modal();



    }

    function showMsg(msg,reload){
        var body = $('<div style="height: 100px;padding: 15px;text-align: center;">'+msg+'</div>');
        t.ui.showModalDialog({
            body: body, buttons: [
                t.ui.createDialogButton('close','关闭', function () {     // 取消
                    if(reload){
                        location.reload(true);
                    }
                })
            ],
            style:{".modal-dialog":{"padding":"100px 110px"},
                   ".modal-footer":{"padding":"10px"},
                   ".btn-default":{"font-size":"12px"}
            }
        });
    }

});