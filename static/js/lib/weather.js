jQuery.fn.extend({
	// 是否要轮询
	Weather: function(orgId){
		var that = this;
		$.ajax({
			url: '/partner/getHomeData',
			type: 'GET',
			dataType: 'json',
			data: {
				curOrgId: orgId,
				modules: 'weather'
			},
			success: function(res){
				if ( res.code == 0 ) {
					that.setWeatherHtml(res.data.weather);
        			$('[data-toggle="tooltip"]').tooltip();
				}else{
					that.setErrorHtml();
				}
			},
			error: function(msg) {
				that.setErrorHtml();
			}
		});
	},
	setErrorHtml:function(){
		var weatherHtml = '<div class="text-center" style="font-size:20px; color:#666; margin-top:50px;">暂时无数据，请稍候</div>';
		weatherHtml += '<div class="text-center" style="margin-top:20px;"><a class="wether_link"  target="_blank" href="http://www.weather.com.cn/weather/101010100.shtml">中国天气网</a></div>';

		$(this).empty();
        $(this).append( weatherHtml );
	},
	setWeatherHtml: function(res){
		if (res.code != 0) {
			this.setErrorHtml();
			return;
		};
		res = res.data;
		var weatherHtml = "";
		var temperature = res.currentTemperature;
		weatherHtml += '<div class="weather">';
        weatherHtml += '<div class="weather-main">' +
                			'<h5 class="color-666"><span>'+ this.monthDayFormat() +'</span>'+
                				'<span class="left-padding-10">'+ this.weekFormat() +'</span></h5>' +
                			'<h1 class="color-666">'+ Math.floor( temperature ) +'<span>℃</span></h1>' +
            			'</div>';
        
        weatherHtml += '<div class="weather-imgshow">'+
                			'<div class="weather-imgshow-title">'+
                    			'<h5 class="pull-left color-666"><span>今天</span></h5>'+
                    			'<h5 class="pull-right right-sapn color-666">'+
                        			'<span data-toggle="tooltip" title="最高温度" data-placement="top">'+ res.maxTemperature +'°</span>'+
                        			'<span class="left-padding-10 color-999"'+
                        			' data-toggle="tooltip" title="最低温度" data-placement="top">'+ res.minTemperature +'°</span>'+
                    			'</h5>'+
                			'</div>';

        weatherHtml += '<div class="weather-imgshow-main clear-float">';
        for (var i = 0, len = res.weatherHours.length; i < len; i++) {
        	var weatherHours = res.weatherHours[i];
        	weatherHtml += '<div class="weather-imgshow-li">';
        	weatherHtml += '<h5>'+ this.forDayReturnHours(weatherHours.dateHour) +'时</h5>';
        	var dayTimeStr = weatherHours.weather;
        	if ( !dayTimeStr ) {
        		dayTimeStr = "未知情况";
        	};
        	weatherHtml += '<div data-toggle="tooltip" title="'+ dayTimeStr +'" data-placement="top">'+
                            	'<i class="wi weather-icon '+ this.weatherIconFormat(weatherHours.weather, weatherHours.dateHour) +'"></i>'+
                        	'</div>';
            weatherHtml += '<span>'+ weatherHours.temperature +'°</span>';

            weatherHtml += '</div>';
        };
        weatherHtml += '</div></div>';
        
        
        weatherHtml += '<div class="weather-after-day clear-float">';
       	for (var i = 0, len = res.weatherDays.length; i < len; i++) {
       		var weatherDays = res.weatherDays[i];
       		weatherHtml += '<div class="weather-after-day-li clear-float color-666">'+
       							'<div class="weather-after-day-week pull-left"><span>'+ weatherDays.dayOfWeek +'</span></div>';
       		weatherHtml += '<div class="weather-after-day-icon pull-left" data-toggle="tooltip" title="'+ weatherDays.weatherDaytime +
       									'" data-placement="top">'+
                   				'<i class="wi weather-icon '+ this.weatherIconFormat(weatherDays.weatherDaytime) +'"></i>'+
                    		'</div>';
            weatherHtml += '<div class="weather-after-day-temperature pull-right">'+
                        		'<span data-toggle="tooltip" title="最高温度"'+
                        				' data-placement="top">'+ weatherDays.maxTemperature +'°</span>'+
                        		'<span class="left-padding-10 color-999" data-toggle="tooltip"'+
                        				' title="最低温度" data-placement="top">'+ weatherDays.minTemperature +'°</span>'+
                    		'</div>';
            weatherHtml += '</div>';
       	};
       	weatherHtml += '</div></div>';
		
		$(this).empty();
        $(this).append( weatherHtml );
	},
	// 将今天的日期格式化返回成星期几
	weekFormat: function(){
		var now = new Date(),
			weekArr = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
		return weekArr[ now.getDay() ];
	},
	monthDayFormat: function(){
		var nowADay = new Date();
		return (nowADay.getMonth()+1) +"月"+ nowADay.getDate() +"日";
	},
	// 传入一个参数和时间(因为不止是现在的时间还有后面六个小时的时间) 返回对应的天气icon
	// 如没有时间 就直接不判断是否白天晚上，默认传白天的
	weatherIconFormat: function(is, time){
		var weatherObj = {
			"晴": "wi-day-sunny",
			// 雾和云
			"薄雾": "wi-cloudy", "雾": "wi-cloudy", "晴间多云": "wi-cloudy",
			"多云": "wi-cloudy", "少云": "wi-cloudy", 
			// 阴天有白天和晚上的区别
			"阴": "wi-day-haze",
			// 小风
			"有风": "wi-windy", "平静": "wi-windy",  
			"微风": "wi-windy", "和风": "wi-windy", "清风": "wi-windy",
			// 大风
			"强风/劲风": "wi-strong-wind", "疾风": "wi-strong-wind", "大风": "wi-strong-wind", "烈风": "wi-strong-wind",
			"风暴": "wi-strong-wind", 
			// 飓风
			"狂爆风": "wi-hurricane", "飓风": "wi-hurricane", "热带风暴": "wi-hurricane",
			// 龙卷风
			"龙卷风": "wi-tornado", 
			// 小雨
			"阵雨": "wi-raindrops", "雷阵雨": "wi-raindrops",
			"小雨": "wi-raindrops", "毛毛雨/细雨": "wi-raindrops","中雨": "wi-raindrops",
			// 大雨
			"强阵雨": "wi-raindrop","大雨": "wi-raindrop",
			"强雷阵雨": "wi-raindrop",
			"极端降雨": "wi-raindrop", "特大暴雨": "wi-raindrop",
			// 冷雨
			"冻雨": "wi-snow-wind",
			"雷阵雨伴有冰雹": "wi-snow-wind",
			"雨夹雪": "wi-snow-wind", "雨雪天气": "wi-snow-wind", "阵雨夹雪": "wi-snow-wind",
			// 雪
			"小雪": "wi-snowflake-cold", "中雪": "wi-snowflake-cold", "大雪": "wi-snowflake-cold",
			"阵雪": "wi-snowflake-cold",
			// 灰
			"霾": "wi-dust", "扬沙": "wi-dust", "浮尘": "wi-dust",
			"火山灰": "wi-dust", "沙尘暴": "wi-dust", "强沙尘暴": "wi-dust",
			// 具体的
			"热": "wi-hot", 
			"冷": "wi-thermometer-exterior", 
			"na": "wi-na"
		};
		if ( time ) {
			var hours = this.forDayReturnHours(time);
			if ( hours < 6 || hours > 18 ) {
				weatherObj["阴"] = "wi-night-alt-cloudy";
			};
		};
		if( is ){
			return weatherObj[is];
		}else{
			return weatherObj["na"];
		}
	},
	// 判断当前时间是白天还是晚上 判断为早上六点到下午六点是白天(返回true)
	dayOrNight: function(){
		var now = new Date();
		if ( now.getHours() >= 6 || now.getHours() <= 18 ) {
			return true;
		}else{
			return false;
		}
	},
	forDayReturnHours: function(time){
		var hour = '';
		var arr = time.split(' ');
		if (arr.length > 1) {

			var str = arr[1];
			hour = parseInt(str);
		};
		
		return hour;
	}

});