/* 
@author:  BaiduHI haiyume
@info: 	  hupload-2.1.js JS上传插件
@date:    2014-3-3
@edition: 2.1.beta
@relay:   依赖 jquery
@用法: 后台返回数据必须为JSON格式
*/
(function($){
	$.fn.hupload = function(options){
		var defaults = {
			url:'',			//发送请求的路径
			types:'',		//可上传文件的类型列表,逗号隔开
			noTypes:'',		//禁止上传的文件格式,逗号隔开
			auto:true,		//是否自动上传
			multi:false,	//是否多文件同步上传
			cover:false,	//是否开启覆盖模式
			fileName:'',	//后台接收的名字，如不配此参数则取上传按钮的name属性，如无name属性则用file,多文件同步上传模式为file_0,file_1,...
			timeout:30000,	//超时时间，默认30秒
			btClass:'',		//开始上传按钮需要加的类
			typeErrFn:'',	//上传类型错误调用的函数(参数：错误的文件名)
			startUpLoad:'',	//上传开始时调用的函数
			checkCb:function(result,names){},			//判断是否成功的函数，返回false代表上传失败
			cb:'',			//成功之后的回调(参数：返回JSON，names)
			errorCb:''		//失败的回调(参数：返回JSON，names)

		};
		var options = $.extend(defaults, options);
		this.each(function(){
			var $self = $(this);
			if(!options.url){alert('参数url缺失。');return;}
			if(options.cover){options.auto = true;}
			if(options.multi){options.auto = false;}
			if(!options.fileName){options.fileName = $self.attr("name") || 'file';}
			var frameId = 'HUPLOAD_' + new Date().getTime();

			//初始化
			$self.attr("frameId",frameId).after('<ul class="hupload-ul huploaded"></ul><ul class="hupload-ul hupload"></ul>');
			var existContainer = $self.parent().siblings(".hupload-container");
			if(!!existContainer){
				existContainer.html('<ul class="hupload-ul huploaded"></ul><ul class="hupload-ul hupload"></ul>');
			}else{
				$self.parent().siblings(".hupload-container").text('<ul class="hupload-ul huploaded"></ul><ul class="hupload-ul hupload"></ul>');
			}


			var iframe = $('<iframe id="'+frameId+'" src="" style="display:none;position:absolute;top:-9999px;left:-9999px;"></iframe>');	
			iframe.load(function(){

				var $ibody = $(iframe[0].contentDocument).find("body");	//iframe的body
				if($ibody.length == 0){$ibody = $(iframe[0].contentWindow.document).find("body");}//兼容IE 6/7
				var $ul;	//装载上传文件列表的容器

				if(!!existContainer){
					$ul  = existContainer.find('.hupload');
				}else{
					$ul = $self.next("ul").next(".hupload");
				}

				//文件名验证/覆盖模式验证
				var hyz = function(){
					var ree = true;
					//文件名验证
					if(options.types || options.noTypes){				
						$ul.find("b").each(function(){
							var text = $(this).text();
							var name = text.substr(text.lastIndexOf('.') + 1);
							var arr = options.types.replace(/，/g,',').replace(/\s/g,'').split(',');
							var noArr =  options.noTypes.replace(/，/g,',').replace(/\s/g,'').split(',');
							if((options.types && String.prototype.indexOf.call(['',arr,''],','+name+',') == -1) || (options.noTypes && String.prototype.indexOf.call(['',noArr,''],','+name+',') > -1) ){
								if(typeof options.typeErrFn == 'function'){
									options.typeErrFn(text);
								}else{
									alert('"' + text + '"文件类型不符合上传要求。');
								}
								ree = false;
								return false;
							}
						});
					}
					if(!ree){return ree;}
					//覆盖模式验证
					if(options.cover && $ul.prev(".huploaded").children().length){
						var co = window.confirm('您确定要覆盖之前上传的文件吗？');
						if(!co){
							$ul.find(".hupload-del").trigger("click");
							ree = false;
						}
					}
					return ree;
				};

				//兼容IEchange事件
				var ieT = null;
				var ieChange = function($em){
					if($em.val() != ''){
						window.clearInterval(ieT);
						ieT = null;
						$em.trigger("change");					
					}
				};

				//上传按钮事件
				$self.unbind( "click" ).click(function(){
					if($(this).hasClass("hupload-dis")){return;}
					if(!$ibody.children("form").length){$ibody.html('<form action="'+options.url+'" method="post" enctype="multipart/form-data" accept-charset="UTF-8"></form>');}
					var em;
					if(options.multi){
						var $form = $ibody.children("form");					
						var eq = $form.children(".hfile").length;
						$ibody.children("form").append('<input type="file" name="" class="hfile" />');
						em = $ibody.find(".hfile").eq(eq);
					}else{
						$ul.html('');
						$ibody.children("form").html('<input type="file" name="" class="hfile" />');
						em = $ibody.find(".hfile");
					}
					em.trigger("click");
					//兼容IE
					if(document.all){
						window.clearInterval(ieT);
						ieT = null;
						ieT = window.setInterval(function(){
							ieChange(em);
						},300);	
					}
				});

				//iframe事件
				$ibody.off().on("change",".hfile",function(){
					var list = '';
					$ibody.find(".hfile").each(function(){
						var val = $(this).val();
						val = val.substr(val.lastIndexOf('\\') + 1);
						var vtype = val.substr(val.lastIndexOf('.') + 1);
						if(val){
							list += '<li class="hupload-list hupload-'+vtype+'"><span class="hupload-del"></span><b title="'+val+'">'+val+'</b><span class="hupload-state">等待上传</span></li>';
						}else{
							$(this).remove();
						}
					});
					$ul.html(list + '<li class="hupload-bt-li" style="' + (options.auto ? 'display:none;' : '') +'"><a class="hupload-start '+options.btClass+'">开始上传</a></li>');
					if(options.auto){$ul.find("a.hupload-start").trigger("click");}
				});

				//异常停止事件处理
				var doStop = function(){
					if(options.auto){
						$ul.html('');
						$ibody.find(".hfile").remove();
					}
				};

				//$ul事件
				//开始上传
				$ul.off().on("click",".hupload-start",function(){
					if(!hyz()){doStop();return;}
					var eq = 0;
					var names = [];
					$ibody.find(".hfile").each(function(){
						if(!$(this).val()){$(this).remove();}
						else{
							if(options.auto){
								$(this).attr("name",options.fileName);
							}else{
								$(this).attr("name",options.fileName + '_' + eq);
							}			
							names[eq] = $(this).val();
							names[eq] = names[eq].substr(names[eq].lastIndexOf('\\') + 1);
							eq++;
						}
					});
					if(!$ibody.find(".hfile").length){alert('请选择上传文件。');return;}
					if(typeof options.startUpLoad == 'function'){
						if(options.startUpLoad() === false){doStop();return;}
					}
					$self.addClass("hupload-dis");
					//$ul.find(".hupload-state").text('上传中...').next(".hupload-del").remove();
					//模拟进度条
					$ul.prev(".huploaded").remove();
					$ul.find(".hupload-state").html('<span class="hupload-progress"><i></i></span>上传中').siblings(".hupload-del").remove();
					var progress = $ul.find(".hupload-progress");
					progress.children("i").animate({width:progress.width()*0.8},20000);
					var strStart = $ibody.html();
					$ibody.children("form").submit();
					var sTime = new Date().getTime();
					//接收数据
					var receive = function(){
						try{
							var strEnd = $(iframe[0].contentDocument).find("body").html();
							//兼容IE 6/7
							if($(iframe[0].contentDocument).length == 0){
								strEnd = $(iframe[0].contentWindow.document).find("body").html();
							}
						}catch(e){
							if(new Date().getTime() - sTime > options.timeout){
								$ul.find(".hupload-state").text('上传超时');
								alert('上传超时。');
								options.errorCb('上传超时。');
							}else{
								setTimeout(receive,500);
								return;
							}
						}
						//console.log(strEnd);
						if(!strEnd || strEnd == strStart){
							if(new Date().getTime() - sTime > options.timeout){
								$ul.find(".hupload-state").text('上传超时');
								alert('上传超时。');
								options.errorCb('上传超时。');
							}else{
								setTimeout(receive,500);
							}
						}else{
							//处理<pre>解析异常情况
							var body = $(iframe[0].contentDocument).length == 0 ? $(iframe[0].contentWindow.document).find("body") : $(iframe[0].contentDocument).find("body");
							strEnd = body.children().length ? body.children(":first").html() : strEnd;
							try{
								var info = $.parseJSON(strEnd);
							}catch(e){
								alert(body.html);
								return;
							}
							//处理错误情况
							var errFn = function(){			
								progress.children("i").stop();
								$ul.find(".hupload-state").addClass(".hupload-false").text('上传失败');
								if(typeof options.errorCb == 'function'){options.errorCb(info,names);}
								$self.removeClass("hupload-dis");
							};
							
							//start 处理返回数据
							if(typeof info == 'object' && info){
								//console.log(info);
								var isCb = true;
								if(options.checkCb(info,names) === false){
									errFn();
								}else{
									progress.children("i").stop().animate({width:progress.width()},500,function(){
										if(!isCb){return;}
										isCb = null;	//确保执行一次回调
//										if(options.cover){
											$ul.prev(".huploaded").html($ul.html()).find(".hupload-state").text('上传成功');
//										}else{
//											$ul.prev(".huploaded").append($ul.html()).find(".hupload-state").text('上传成功');
//										}
										$ul.prev(".huploaded").find(".hupload-bt-li").remove();
										$ul.html('');					
										if(typeof options.cb == 'function'){options.cb(info,names);}
										$self.removeClass("hupload-dis");
									});	
								}
							}else{
								errFn();
								alert(body.html());
							}
							//end 处理返回数据
							
							iframe.attr("src","");
							$ibody = $(iframe[0].contentDocument).find("body");
						}
					};//end receive
					receive();
				//取消上传
				}).on("click",".hupload-del",function(){
					var eq = $(this).closest("li").index();
					$ibody.find(".hfile").eq(eq).remove();
					$(this).closest("li").remove();
					if($ul.children().length == 1){$ul.html('');}
				});
			
		
			});	//end iframe load
			$("body").prepend(iframe);
			
		});//end each
	};
})(jQuery);