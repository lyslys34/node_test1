/**
 * User: xiamengli
 * Date: 14-9-18
 * Time: 下午2:12
 */

define(['lib/jquery-ui-1.10.4/js/jquery-ui-1.10.4'], function($ui){
  //set datepicker
  var datepickerSet = function (arg) {
    // set arguments
    var arg = arg || {};
    $.datepicker.regional['zh-CN'] = {
      closeText: '关闭',
      prevText: '&#x3c;上月',
      nextText: '下月&#x3e;',
      currentText: '今天',
      monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
        '七月', '八月', '九月', '十月', '十一月', '十二月'],
      monthNamesShort: ['一', '二', '三', '四', '五', '六',
        '七', '八', '九', '十', '十一', '十二'],
      dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
      dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
      dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
      weekHeader: '周',
      dateFormat: 'yy-mm-dd',
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: true,
      yearSuffix: '年'
    };
    //date-picker初始化设置
    //date-picker初始化设置
    $.datepicker.setDefaults({
      //可以在onfocus及button均触发
      showOn: "both",
      buttonImageOnly: true,
      buttonImage: "/static/imgs/calendar.png",
      buttonText: "Calendar"
    });
    $.datepicker.setDefaults($.datepicker.regional['zh-CN']);

    //构造date-picker，所有J-datepicker类都会被构造
    $(".J-datepicker").datepicker(arg);
  };

  return {
    'set' : datepickerSet
  };
});