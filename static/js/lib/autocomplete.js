

require.config({
    shim : {
        'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete' : {
            deps : ['page/common']
        }
    }
})

define(['page/common',
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.core',
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget',
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.position',
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu',
    'lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete'], function() {

})
