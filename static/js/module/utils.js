/**
 * Created with IntelliJ IDEA.
 * User: xuyc
 * Date: 14-11-4
 * Time: 下午4:46
 * To change this template use File | Settings | File Templates.
 */

define([], function() {

    jQuery.extend(jQuery.fn, {

        dataAll: function(obj) {

            if (obj) {
                for (var key in obj) {
                    this.data(key, obj[key]);
                }
            }

            return this;
        },

        // 判断数组里的元素是否至少有一个符合条件，返回符合条件的元素
        any: function(func) {

            if (typeof func != 'function') {
                var value = func;
                func = function(v) {
                    return value == v;
                };
            }

            var found;
            this.each(function() {

                if (func.apply(this, arguments)) {
                    found = this;
                    return false;
                }

            });

            return found;
        },

        // 判断数组里的元素是否都符合条件
        all: function(func) {

            if (typeof func != 'function') {
                var value = func;
                func = function(v) {
                    return value == v;
                };
            }

            var ret = true;
            this.each(function() {

                if (!func.apply(this, arguments)) {
                    ret = false;
                    return false;
                }

            });

            return ret;
        },

        select: function(func) {
            var arr = [];
            this.each(function() {
                arr.push(func.apply(this, arguments));
            });
            return arr;
        }

    });

    function getFormatedString(value) {

        if (value > 9) {

            return new String(value);
        };

        return '0' + new String(value);
    }

    return {

        post: function(url, data, callback) // 发送POST请求
            {
                if (typeof(data) == 'function') {
                    callback = data;
                    data = null;
                }
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    success: function(r) {

                        callback({
                            httpSuccess: true,
                            data: r
                        });

                    },
                    error: function(XmlHttpRequest, textStatus, errorThrown) {

                        callback({
                            httpSuccess: false,
                            statusCode: XmlHttpRequest.status
                        });

                    }
                });
            },

        urlArg: function(name, def) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return def;
        },

        ieDetection: function() {
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                return true;
            }
            return false;
        },
        timeString: function(timestamp) {

            var date = new Date(timestamp);
            var str = getFormatedString(date.getHours()) + ':' + getFormatedString(date.getMinutes());
            return str;
        },

        showDesktopNotification: function(title, body, op) {

            var notification;
            var options = {
                icon: '/static/imgs/deskNoti.png'
            };
            if (body && typeof body === 'string') {
                options.body = body;
            };
            if (body && typeof body === 'object') {
                op = body;
            };
            if (op && typeof op === 'object') {
                for (property in op) {
                    options[property] = op[property];
                };
            };
            if (!("Notification" in window)) {

            } else if (Notification.permission === "granted") {

                notification = new Notification(title, options);
            } else if (Notification.permission !== 'denied') {

                Notification.requestPermission(function(permission) {
                    // If the user is okay, let's create a notification
                    if (permission === "granted") {
                        notification = new Notification(title, options);
                    }
                });
            }

            return notification;
        },

        getDaystartUtime: function(dayBefore, start, currentServerTime) {
            
            var today = new Date();
            if (currentServerTime) {
                today = new Date(currentServerTime);
            };
            
            today.setHours(0);
            today.setMinutes(0);
            today.setSeconds(0);
            today.setMilliseconds(0);
            var be = dayBefore;
            if (!start) {
                be = dayBefore - 1
            };
            var days = 1000 * 60 * 60 * 24 * be;
            return today.getTime() - days;
        },
    };

});