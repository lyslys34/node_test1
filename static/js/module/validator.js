/**
 * 一些基础的验证方法,如：手机号验证，邮箱验证
 */

define(function () {

    function _valueExist(value) {
        if (typeof value == 'undefined') {
            return false;
        }
        if (value == null) {
            return false;
        }
        return true;
    }

    function isMobile(mobile) {
        if (_valueExist(mobile)) {
            return /^1\d{10}$/.test(mobile);
        }
        return false;
    }

    function isEmail(email) {
        if (_valueExist(email)) {
            return /^\w+@\w+\.\w+$/.test(email);
        }
        return false;
    }

    function isBlank(value) {
        if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
                return this.replace(/^\s+|\s+$/g, '');
            }
        }
        return !_valueExist(value) || value.trim() == "";
    }

    function endsWith(str, suffix) {
        if (!_valueExist(str)) {
            return false;
        }
        if (!_valueExist(suffix)) {
            return true;
        }
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    function checkInput(str) {
        var pattern = /^[\w\u4e00-\u9fa5]+$/;
        return pattern.test(str);
    }

    function replaceNum(value) {
        return value.replace(/[^\d\.]/g,'');  
    }

    function replaceInput(value) {
        var patten = /^[^\w\u4e00-\u9fa5\+$]/g;
        return value.replace(patten, '');
        
    }

    function isnNumber(value) {
        var pattern = /^\d*$/g;
        return pattern.test(value);
    }

    return {
        "isExist": _valueExist,
        "isMobile": isMobile,
        "isEmail": isEmail,
        "isBlank": isBlank,
        "endsWith": endsWith,
        "checkInput": checkInput,
        "replaceNum": replaceNum,
        "replaceInput": replaceInput,
        "isNumber":isnNumber,
    };
});
