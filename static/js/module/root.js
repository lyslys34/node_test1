/**
 * Created with IntelliJ IDEA.
 * User: xuyc
 * Date: 14-11-3
 * Time: 下午8:28
 * To change this template use File | Settings | File Templates.
 */

define(['page/home', 'module/cookie', 'module/ui', 'module/utils'], function(home, cookie, ui, utils) {

    if(cookie.getCookie('notice-bar-closed') != 'yes' && cookie.getCookie('tid'))
    {
        var div = '<div class="alert alert-warning alert-dismissible fade in" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" class="close-button">×</span><span class="sr-only">Close</span></button>' +
            '<strong class="notice">注意：</strong>系统只记录配送完成状态的订单数据，为方便最后结算，请让配送员每送达一单后及时点击配送完成按钮。' +
        '</div>';

        $($('body').children()[0]).before(div);

        // 添加配送人员
        $(document).delegate('.close-button', 'click', function() {
            cookie.setCookie('notice-bar-closed', 'yes', 24 * 3600);
        });
    }

    return {
        cookie: cookie,
        ui: ui,
        utils: utils
    };

});
