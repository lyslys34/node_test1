/**
 * Created with IntelliJ IDEA.
 * User: xuyc
 * Date: 14-11-3
 * Time: 下午8:31
 * To change this template use File | Settings | File Templates.
 */

define(['module/cookie', 'lib/jquery/slimscroll'], function(cookie) {

  // 退出登录
  $('.form-button').click(function() {
    $(this).parents('form').submit();
  });

  var role = cookie.getCookie("role");
  var roles = cookie.getCookie("roles");

  var userName = decodeURIComponent(cookie.getCookie('userName'));
  var len = userName.length;
  if (len > 1) {
      userName = userName.substr(1, len - 2);
  }

  $(".user-name").html(userName);
  // 头部切换左边nav样式的按钮
  $(document).on('click', '.j-toggle-menu', function(event) {
    var jQwrap = $('#wrapper');
  /*  if (!jQwrap.hasClass('container-mini')) {

      $(document).on('mouseenter', 'aside.skin-7 .slimScrollDiv', delMiniClass);
      $('#breadcrumb-bar').on('mouseenter', addMiniClass);
      $('#main-container').on('mouseenter', addMiniClass);
      $('#full-screen').on('mouseenter', addMiniClass);

    } else {
      $('aside.skin-7 .slimScrollDiv').unbind('mouseenter');
      $('#breadcrumb-bar').unbind('mouseenter', addMiniClass);
      $('#main-container').unbind('mouseenter', addMiniClass);
      $('#full-screen').unbind('mouseenter', addMiniClass);
    } 
    if (jQwrap.hasClass('container-mini') && !jQwrap.hasClass('wrapper-mini')) {
      jQwrap.toggleClass('container-mini');
    } else {*/
      jQwrap.toggleClass('wrapper-mini').toggleClass('container-mini');
  //  }
  });

  //注销用户的下拉菜单
  $('#top-nav .user-info-wrapper').bind('click',function(e){
    e.stopPropagation();
    $(this).toggleClass('hover');
  })

  $('body').bind('click',function(){
    $('#top-nav .user-info-wrapper').removeClass('hover');
  })

  var selectSubMenu = function() {

    try {
      var pathname = location.pathname;
      $(".submenu").find("a").each(function() {

        var quoteIndex = $(this).attr('href').indexOf('?');
        var aHref;
        if (quoteIndex > -1) {
          aHref = $(this).attr('href').substr(0, quoteIndex);
        } else {
          aHref = $(this).attr('href');
        }
        var pos = aHref.indexOf(pathname);

        if (pos >= 0 && (pos + pathname.length == aHref.length)) {
          $(this).parents("li").addClass("active")
          $(this).parent().addClass("active");
          $(this).parent().parent().addClass("in");
          $(this).parent().parent().prev().removeClass("collapsed");
        }
      });
    } catch (e) {

    }
  }

  var addMiniClass = function() {
    var jQwrap = $('#wrapper');
    jQwrap.addClass('wrapper-mini');
  };

  var delMiniClass = function() {
    var jQwrap = $('#wrapper');
    jQwrap.removeClass('wrapper-mini')
  };

  var bindEvents = function() {

    if ($('.scrollable-sidebar').length > 0) {

      $('.scrollable-sidebar').slimScroll({
        height: '100%',
        size: '0px'
      });

      selectSubMenu();

      $('.openable .nav-header').click(function(event) {
        event.preventDefault();
        if(($('#wrapper').hasClass('wrapper-mini')||$('#wrapper').hasClass('container-mini'))&&!$(this).hasClass('collapsed')){
          $(this).removeAttr('data-toggle');
          setTimeout(function(){
            $(this).attr('data-toggle','collapse');
          }.bind(this),0)
        }
        var curClick = $(this);
        if (curClick.hasClass('collapsed')) {
          setTimeout(function(){
            $('.openable .nav-header').each(function(index, el) {
              if ($(this) != curClick && !$(this).hasClass('collapsed')) {
                $(this).click();
              }
            });
          },0)
        };
        $('#wrapper').removeClass('wrapper-mini').removeClass('container-mini');

      });
    } else {
      setTimeout(bindEvents, 500);
    }
  }
  bindEvents();
});
