/**
 * Created with IntelliJ IDEA.
 * User: xuyc
 * Date: 14-11-4
 * Time: 上午10:06
 * To change this template use File | Settings | File Templates.
 */

define(['module/notification'], function(Notification) {

    return {

        // 创建一个对话框
        createDialog: function(option) {

            var op = { title:'', body:'', buttons: [ this.createDialogButton('close') ], callback: function() { } };
            $.extend(op, option);

            var dialog = $(
                '<div class="modal fade">' +
                '   <div class="modal-dialog">' +
                '       <div class="modal-content">' +
                '           <div class="modal-header">' +
                '           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
                '           <h4 class="modal-title"></h4></div>' +
                '           <div class="modal-body"></div>' +
                '           <div class="modal-footer"></div>' +
                '       </div>' +
                '   </div>' +
                '</div>');
            if(op.title != null && op.title != ''){
                $('.modal-title', dialog).text(op.title);
            }else{
                $('.modal-header', dialog).remove();
            }
            $('.modal-body', dialog).append(op.body);

            var footer = $('.modal-footer', dialog);

            $(op.buttons).each(function() {
                if (this === window) {
                  return;
                }
                var b = this;
                var btn = $('<button type="button" class="btn"></button>').appendTo(footer).text(b.text);
                b.dismiss && btn.attr('data-dismiss', b.dismiss);
                btn.addClass(b.primary? 'btn-success' : 'btn-default');
                btn.click(function() {

                    var r1 = b.callback && b.callback.apply(this, arguments);
                    if(r1 === false) return false;

                    var r2 = op.callback(b.name);
                    if(r2 === false) return false;

                    if(r1 === true || r2 === true) dialog.modal('hide');
                });
            });

            $('.close', dialog).click(function() {

                var r = op.callback('close');
                if(r === false) return false;
                else if(r === true) dialog.modal('hide');

            });
            var csses = op.style;
            if(csses != null){
                $.each(csses,function(clazz,css){
                    $(clazz,dialog).css(css);
                });
            }
            return dialog;
        },

        // 显示模态对话框
        showModalDialog: function(option) {
            var dialog = this.createDialog(option);
            dialog.modal('show');
            return dialog;
        },

        // 创建对话框按钮
        createDialogButton: function(name, text, callback) {

            if(typeof text == 'function') { callback = text; text = undefined; }

            switch(name)
            {
                case 'close':
                    return { text: text || '关闭', name: name, dismiss: 'modal', primary: false, callback: callback };

                case 'ok':
                    return { text: text || '确定', name: name, primary: true, callback: callback };

                default:
                    return { text: text || name, name: name, callback: callback};
            }

        },

        showNotification: function (msg, type, duration) {
            new Notification({ msg: msg || '', type: type || 'operation', duration: duration || 2000 }).show();
        },

        generatePagerHtml: function (page){
            //设置分页信息
            var pageHtml = '';
            var leftNum = 1;
            var rightNum = page.totalPage;
            if (page.currentPage > 3) {
                leftNum = page.currentPage - 2;
            }
            if (page.totalPage > 6) {
                rightNum = leftNum + 5;
                if (rightNum > page.totalPage) {
                    rightNum = page.totalPage;
                    leftNum = rightNum - 5;
                };
            };
            if(page.totalPage>1){
                var pageHtml = '<nav style="float:right;">'+
                               '  <ul class="pagination">';

                pageHtml+='<li';
                if(page.currentPage == 1){
                    pageHtml += ' class="disabled"';
                }
                pageHtml+=' page='+(page.currentPage-1)+'><a href="#'+status+'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                if ( page.totalPage > 6 ) {
                    pageHtml += '<li page="1"><a href="#'+status+'">首页</a></li>';
                };
                if ( page.totalPage > 6 && (page.totalPage-page.currentPage) <= 3 ) {
                    pageHtml += '<span class="disabled pull-left pagination-span">...</span>';
                };
                for(var i = leftNum; i <= rightNum;i++){
                    if(i == page.currentPage){
                        pageHtml+='<li class="active"><a href="#'+status+'">'+i+' <span class="sr-only">(current)</span></a></li>';
                    }else{
                        pageHtml+='<li page='+i+'><a href="#'+status+'">'+i+'</a></li>';
                    }
                }
                if ( page.totalPage > 6 && (page.totalPage-page.currentPage) > 3 ) {
                    pageHtml += '<span class="disabled pull-left pagination-span">...</span>';
                };
                if ( page.totalPage > 6 ) {
                    pageHtml += '<li page='+ page.totalPage +'><a href="#'+status+'">末页</a></li>';
                };
                pageHtml+='<li';
                if(page.currentPage == page.totalPage){
                    pageHtml += ' class="disabled"';
                }
                pageHtml+=' page='+(page.currentPage+1)+'><a href="#'+status+'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                pageHtml += '  </ul></nav>';
            }
            return pageHtml;
        },
    };

});


