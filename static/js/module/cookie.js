define(function(){
  function setCookie(cname, cvalue, expires) {
      if(expires)
      {
          var time = new Date();
          time.setTime(time.getTime() + expires * 1000);
          document.cookie = cname + "=" + encodeURIComponent(cvalue) + "; path=/" +"; expires=" + time.toGMTString();
      }
      else
      {
          document.cookie = cname + "=" + encodeURIComponent(cvalue);
      }
  }

  function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = $.trim(ca[i]);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
  }
  function setSessionCookie(name,value,cookiePath){
      var isIE=!-[1,];//判断是否是ie核心浏览器
      if(isIE){
          if(value){
              var expire = "; expires=At the end of the Session";
              var path="; path=/";
              document.cookie = name + "=" + escape(value) + expire+path;
          }
      }else{
          if(value){
              var expire = "; expires=Session";
              var path="; path=/";
              document.cookie = name + "=" + escape(value) + expire+path;
          }
      }
  }

　　return {
　　  'setCookie': setCookie,
     'getCookie':getCookie,
     'setSessionCookie':setSessionCookie
　　};

});