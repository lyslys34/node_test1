/**
 * ajax管理方法
 */

define(function(){
    var ajaxManager = {};
    var isEmptyObj = function(obj){
        var count = 0;
        for(var props in obj){
            count++;
        }
        return count>1;
    }
    ajaxManager.getServerData = function(url,data,callback,showloading){
        if(showloading!==false){
            $('#ajaxLoading').show();
        }
        if(ajaxManager[url]){
            ajaxManager[url].abort();
        }
        ajaxManager[url] = $.ajax({
            url:url,
            type:'post',
            dataType:'json',
            data:data
        }).done(function(data){
            ajaxManager[url] = void 0;
            if(isEmptyObj(ajaxManager)){
                $('#ajaxLoading').hide();
            }
            callback(data);
        }).fail(function(err){
            ajaxManager[url] = void 0;
            if(isEmptyObj(ajaxManager)){
                $('#ajaxLoading').hide();
            }
            console.log(err);
        })
    }

    return ajaxManager;
})
