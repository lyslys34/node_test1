/**
 * 消息提示组件脚本。
 * 请同时导入notification.css样式模块及jquery
 * 主要参数包括：
 *	@type：提示类型，warning/operation/loading
 *	@msg：提示的消息文本，操作失败等
 *	@duration：消息悬浮停留时间，默认为5秒
 *	通过option对象中修改默认值
 *  构建方式：
 *  	var note = new Notification({
 *         msg:'操作失败！',
 *          type: 'operation'
 *       });
 *
 *  	note.show();
 * 
 * @author xiamengli
 * @date 2014-05-05 星期一
 */

 define(function(){
  // var jQdom = $('');
  var Notification = function(options){
  	this._options = options || {};

  	this._type = this._options['type'] || 'warning';
  	this._msg = this._options['msg'] || '';
	    this._duration = this._options['duration'] || 5000; //持续时间，默认5秒
	  };

	  Notification.prototype = {
	  	show: function(){
	  		var n_view = $('<div class="notification">' +
	  			'<span class="n-body n-'+ this._type + '">' +
	  			this._msg +
	  			'</span>' +
	  			'</div>'
	  			)

	  		$('body').append(n_view);
			//todo：多个提示消息时候
			//$('body').remove('.notification');
			$('.notification').fadeIn(500);
			setTimeout(this.destruct, this._duration);
		},

		destruct: function () {
			$('.notification').fadeOut(500);
			$('.notification').remove();
		}
	};

	return Notification;
});
