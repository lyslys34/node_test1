/**
 * 弹出提示方法
 */

define(function(){
    function factory(str){
        if(!str){
            return;
        }
        var popEl = document.querySelector('#popMsg'),
            maskElId = document.querySelector('#mask');
        var hidemsg = function(){
            popEl.style.display = 'none';
            maskEl.style.display = 'none';
        }
        if(popEl&&maskEl){
            maskEl.style.display = 'block';
            popEl.innerHTML = str;
            popEl.style.display = 'block';
        }else{
            popEl = document.createElement('div');
            popEl.setAttribute('class','popMsg');
            popEl.setAttribute('id','popMsg');
            popEl.innerHTML = str;
            maskEl = document.createElement('div');
            maskEl.setAttribute('class','mask');
            maskEl.setAttribute('id','mask');
            document.body.appendChild(popEl);
            document.body.appendChild(maskEl);
        }
        setTimeout(function(){
            hidemsg();
        },2000);
    }
    return factory;
})
