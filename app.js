var express = require('express');
var path = require('path');
var http = require('http');
var bodyParser = require('body-parser');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var multer = require('multer');
var config = require('./config');

var routes = require('./routes/main');
var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null , 'uploads/');
	},
	filename : function (req, file, cb) {
		console.log('multer storage filename():', file);
		cb(null, Date.now() + '.jpg');
	}
})
var upload = multer({storage});

var app = express();
var port = process.env.PORT || 5000;


app.use(express.static(config.static_dir, ''));
app.use(express.static(path.join(__dirname, 'static')));
app.use(bodyParser.json());

app.set('port', port);
app.use(favicon());
app.use(logger('dev'));
// app.use(bodyParser.json());
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);

/*
app.get('/', function (req, res) {
	res.sendFile('./template/helloworld.html');
});*/

// 发放列表的mock接口，先简单写，后面再统一优化整合
app.post('/equip/grant/list', function (req, res) {
	console.log(req.body);
	var mobile = req.params.mobile || '';
	var type = req.params.type || 1; // 标识已完成/未完成？？
	var pageNo = req.params.pageNo || 1;
	var pageSize = req.params.pageSize || 10;

	console.log("mobile: %s;   type: %s;   paegNo: %s; pageSize  %s", mobile, type, pageNo, pageSize);

	// 其实这里直接读文件会更好
	var resObj = {"code":0,"msg":"success","data":{"total": "200", "list": [{"id": 1, "bmUserName" : "张三", "mobile" : "1231231312", "cardNo" : "324142341241234", "stationName" : "北京", "equipName" : "保湿箱", "ctime" : "1454311558", "utime" : "1454311630", "type" : "1"}, {"bmUserName" : "张三", "mobile" : "1231231312", "cardNo" : "324142341241234", "stationName" : "北京", "equipName" : "保湿箱", "ctime" : "1454311558", "utime" : "1454311630", "type" : "2"}]}};

	res.send(resObj);


});

// 用于简单模拟结算盈亏项目的指标树接口
app.post('/fa/subject/tree', function (req, res) {

	var resObj = '{"code":0,"msg":"读取成功","data":{"orgType":1,"orgName":"自建","subjects":[{"id":1,"name":"单量","catalog":true,"serial_num":1,"deadline":null,"children":null},{"id":2,"name":"收入","catalog":true,"serial_num":2,"deadline":null,"children":null},{"id":3,"name":"成本","catalog":true,"serial_num":3,"deadline":null,"children":[{"id":4,"name":"零级成本","catalog":true,"serial_num":1,"deadline":null,"children":[{"id":401,"name":"基本工资","catalog":false,"serial_num":1,"deadline":null,"children":null},{"id":402,"name":"基本补贴","catalog":false,"serial_num":2,"deadline":null,"children":[{"id":501,"name":"住房补助","catalog":false,"serial_num":1,"deadline":null,"children":null},{"id":502,"name":"车辆补助","catalog":false,"serial_num":2,"deadline":null,"children":null},{"id":503,"name":"话费补助","catalog":false,"serial_num":3,"deadline":null,"children":null}]},{"id":403,"name":"五险一金","catalog":false,"serial_num":3,"deadline":null,"children":[{"id":601,"name":"社保","catalog":false,"serial_num":1,"deadline":null,"children":null},{"id":602,"name":"住房公积金","catalog":false,"serial_num":2,"deadline":null,"children":null}]}]},{"id":5,"name":"一级成本","catalog":true,"serial_num":2,"deadline":null,"children":null},{"id":6,"name":"二级成本","catalog":true,"serial_num":3,"deadline":null,"children":null},{"id":7,"name":"三级成本","catalog":true,"serial_num":4,"deadline":null,"children":null},{"id":8,"name":"总计","catalog":true,"serial_num":5,"deadline":null,"children":null}]},{"id":9,"name":"BG分摊","catalog":true,"serial_num":4,"deadline":null,"children":[{"id":10,"name":"技术部","catalog":true,"serial_num":1,"deadline":null,"children":null},{"id":11,"name":"产品部","catalog":true,"serial_num":2,"deadline":null,"children":null},{"id":12,"name":"运营部","catalog":true,"serial_num":3,"deadline":null,"children":null},{"id":13,"name":"集团总部","catalog":true,"serial_num":4,"deadline":null,"children":null},{"id":14,"name":"总计","catalog":true,"serial_num":5,"deadline":null,"children":null}]},{"id":15,"name":"盈亏","catalog":true,"serial_num":5,"deadline":null,"children":[{"id":16,"name":"含BG分摊","catalog":true,"serial_num":1,"deadline":null,"children":null},{"id":17,"name":"不含BG分摊","catalog":true,"serial_num":2,"deadline":null,"children":null}]}]}}';

	res.send(resObj);
});


// 用于简单模拟结算盈亏项目的指标树接口
app.post('/fa/account/query', function (req, res) {

	var resObj = '{"code":0,"msg":"读取成功","data":{"orgType":1,"orgName":"自建","subjects":[{"id":1,"name":"单量","catalog":true,"serial_num":1,"deadline":null,"children":null},{"id":2,"name":"收入","catalog":true,"serial_num":2,"deadline":null,"children":null},{"id":3,"name":"成本","catalog":true,"serial_num":3,"deadline":null,"children":[{"id":4,"name":"零级成本","catalog":true,"serial_num":1,"deadline":null,"children":[{"id":401,"name":"基本工资","catalog":false,"serial_num":1,"deadline":null,"children":null},{"id":402,"name":"基本补贴","catalog":false,"serial_num":2,"deadline":null,"children":[{"id":501,"name":"住房补助","catalog":false,"serial_num":1,"deadline":null,"children":null},{"id":502,"name":"车辆补助","catalog":false,"serial_num":2,"deadline":null,"children":null},{"id":503,"name":"话费补助","catalog":false,"serial_num":3,"deadline":null,"children":null}]},{"id":403,"name":"五险一金","catalog":false,"serial_num":3,"deadline":null,"children":[{"id":601,"name":"社保","catalog":false,"serial_num":1,"deadline":null,"children":null},{"id":602,"name":"住房公积金","catalog":false,"serial_num":2,"deadline":null,"children":null}]}]},{"id":5,"name":"一级成本","catalog":true,"serial_num":2,"deadline":null,"children":null},{"id":6,"name":"二级成本","catalog":true,"serial_num":3,"deadline":null,"children":null},{"id":7,"name":"三级成本","catalog":true,"serial_num":4,"deadline":null,"children":null},{"id":8,"name":"总计","catalog":true,"serial_num":5,"deadline":null,"children":null}]},{"id":9,"name":"BG分摊","catalog":true,"serial_num":4,"deadline":null,"children":[{"id":10,"name":"技术部","catalog":true,"serial_num":1,"deadline":null,"children":null},{"id":11,"name":"产品部","catalog":true,"serial_num":2,"deadline":null,"children":null},{"id":12,"name":"运营部","catalog":true,"serial_num":3,"deadline":null,"children":null},{"id":13,"name":"集团总部","catalog":true,"serial_num":4,"deadline":null,"children":null},{"id":14,"name":"总计","catalog":true,"serial_num":5,"deadline":null,"children":null}]},{"id":15,"name":"盈亏","catalog":true,"serial_num":5,"deadline":null,"children":[{"id":16,"name":"含BG分摊","catalog":true,"serial_num":1,"deadline":null,"children":null},{"id":17,"name":"不含BG分摊","catalog":true,"serial_num":2,"deadline":null,"children":null}]}]}}';

	res.send(resObj);
});


// 发放列表的mock接口，先简单写，后面再统一优化整合
app.post('/equip/material/list', function (req, res) {
	// 这里需要处理的是form-data而不是json-data
	console.log(req);
	var mobile = req.params.mobile || '';
	var type = req.params.type || 1; // 标识已完成/未完成？？
	var pageNo = req.params.pageNo || 1;
	var pageSize = req.params.pageSize || 10;

	console.log("mobile: %s;   type: %s;   paegNo: %s; pageSize  %s", mobile, type, pageNo, pageSize);

	// 其实这里直接读文件会更好
	var resObj = {"code":0,"msg":"success","data":{"total": "200", "list": [{"id": "123412", "status": "1", "bmUserName" : "张三", "mobile" : "1231231312", "cardNo" : "324142341241234", "stationName" : "北京", "equipName" : "保湿箱", "ctime" : "1454311558", "utime" : "1454311630", "type" : "1"}, {"id": "123415", "status": "3", "bmUserName" : "张三", "mobile" : "1231231312", "cardNo" : "324142341241234", "stationName" : "北京", "equipName" : "保湿箱", "ctime" : "1454311558", "utime" : "1454311630", "type" : "2"}]}};

	res.send(resObj);


});

app.post('/file-upload', upload.single('imgFile'), function (req, res, next) {

	console.log("req.file:", req.file);

	var resObj = {
		"code" : 0,
		"msg" : "success",
		"data" : [
			{
				"code" : 0,
				"url" : '/uploads/' + req.file.filename
			}]
	};

	res.send(resObj);
});

app.get('/testHtml', function(req, res, next) {
	var html = "%0A%0A%3Cdiv%20class%3D%22container%22%3E%0A%20%20%20%20%20%20%20%20%3Cdiv%20id%3D%22content%22%3E%3Cp%3E23143241241241242141%3C%2Fp%3E%0A%0A%3Cp%3E23143241241241242141%3C%2Fp%3E%3C%2Fdiv%3E%0A%3C%2Fdiv%3E%0A%0A%3Cdiv%20id%3D%22ctn%22%20style%3D%22display%3A%20none%3B%22%3E%0A%0A%3C%2Fdiv%3E%0A%0A%3Cscript%20type%3D%22text%2Fjavascript%22%20src%3D%22%2Fstatic%2Fjs%2Flib%2Fzepto.min.js%22%3E%3C%2Fscript%3E%0A%3Cscript%3E%0A%20%20%20(function()%20%7B%0A%20%20%20%20%20%20%20%20if%20(code%20%26%26%20code%20%3D%3D%3D%200)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20var%20frag%20%3D%20decodeURIComponent(data)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20var%20ctn%20%3D%20document.getElementById('ctn')%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20ctn.innerHTML%20%3D%20frag%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20var%20html%20%3D%20ctn.innerText%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20document.getElementById('content').innerHTML%20%3D%20html%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%2F%2F%20%E5%9B%BE%E7%89%87%E5%8A%A0%E8%BD%BD%E6%97%B6%E7%9A%84%E5%AE%B9%E9%94%99%0A%20%20%20%20%20%20%20%20%24('img').on('error'%2C%20function()%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%24(this).attr('src'%2C%20'%2Fstatic%2Fimgs%2Fmall_default.jpg')%3B%0A%20%20%20%20%20%20%20%20%7D)%3B%0A%0A%20%20%20%20%20%20%20%20%24.get('%2FtestHtml'%2C%20function(res)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20console.log(res)%0A%20%20%20%20%20%20%20%20%20%20%20%20%24('%23content').html(res)%3B%0A%20%20%20%20%20%20%20%20%7D%2C%20function(err)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20console.error(res)%3B%0A%20%20%20%20%20%20%20%20%7D)%3B%0A%20%20%20%20%7D)()%3B%20%20%20%0A%3C%2Fscript%3E%0A%3Cform%20method%3D%22post%22%20id%3D%22params-form%22%3E%0A%20%20%20%20%3C%2Fform%3E%0A%0A%3Cform%20method%3D%22post%22%20id%3D%22common-form%22%3E%0A%3C%2Fform%3E%0A%0A%3Cscript%20type%3D%22text%2Fjavascript%22%3E%0A%20%20%20%20!function(e%2Ct%2Cn)%7Bfunction%20s()%7Bvar%20e%3Dt.createElement(%22script%22)%3Be.async%3D!0%2Ce.src%3D%22%2F%2Fs0.meituan.%22%2B(-1%3D%3D%3Dt.location.protocol.indexOf(%22https%22)%3F%22net%22%3A%22com%22)%2B%22%2Fbs%2Fjs%2F%3Ff%3Dmta-js%3Amta.min.js%22%3Bvar%20n%3Dt.getElementsByTagName(%22script%22)%5B0%5D%3Bn.parentNode.insertBefore(e%2Cn)%7Dif(e.MeituanAnalyticsObject%3Dn%2Ce%5Bn%5D%3De%5Bn%5D%7C%7Cfunction()%7B(e%5Bn%5D.q%3De%5Bn%5D.q%7C%7C%5B%5D).push(arguments)%7D%2C%22complete%22%3D%3D%3Dt.readyState)s()%3Belse%7Bvar%20i%3D%22addEventListener%22%2Cr%3D%22attachEvent%22%3Bif(e%5Bi%5D)e%5Bi%5D(%22load%22%2Cs%2C!1)%3Belse%20if(e%5Br%5D)e%5Br%5D(%22onload%22%2Cs)%3Belse%7Bvar%20a%3De.onload%3Be.onload%3Dfunction()%7Bs()%2Ca%26%26a()%7D%7D%7D%7D(window%2Cdocument%2C%22mta%22)%2Cfunction(e%2Ct%2Cn)%7Bif(t%26%26!(%22_mta%22in%20t))%7Bt._mta%3D!0%3Bvar%20s%3De.location.protocol%3Bif(%22file%3A%22!%3D%3Ds)%7Bvar%20i%3De.location.host%2Cr%3Dt.prototype.open%3Bt.prototype.open%3Dfunction(t%2Cn%2Ca%2Co%2Cl)%7Bif(this._method%3D%22string%22%3D%3Dtypeof%20t%3Ft.toUpperCase()%3Anull%2Cn)%7Bif(0%3D%3D%3Dn.indexOf(%22http%3A%2F%2F%22)%7C%7C0%3D%3D%3Dn.indexOf(%22https%3A%2F%2F%22)%7C%7C0%3D%3D%3Dn.indexOf(%22%2F%2F%22))this._url%3Dn%3Belse%20if(0%3D%3D%3Dn.indexOf(%22%2F%22))this._url%3Ds%2B%22%2F%2F%22%2Bi%2Bn%3Belse%7Bvar%20h%3Ds%2B%22%2F%2F%22%2Bi%2Be.location.pathname%3Bh%3Dh.substring(0%2Ch.lastIndexOf(%22%2F%22)%2B1)%2Cthis._url%3Dh%2Bn%7Dvar%20u%3Dthis._url.indexOf(%22%3F%22)%3B-1!%3D%3Du%3F(this._searchLength%3Dthis._url.length-1-u%2Cthis._url%3Dthis._url.substring(0%2Cu))%3Athis._searchLength%3D0%7Delse%20this._url%3Dnull%2Cthis._searchLength%3D0%3Breturn%20this._startTime%3D(new%20Date).getTime()%2Cr.apply(this%2Carguments)%7D%3Bvar%20a%3D%22onreadystatechange%22%2Co%3D%22addEventListener%22%2Cl%3Dt.prototype.send%3Bt.prototype.send%3Dfunction(t)%7Bfunction%20n(n%2Ci)%7Bif(0!%3D%3Dn._url.indexOf(s%2B%22%2F%2Ffrep.meituan.net%2F_.gif%22))%7Bvar%20r%3Bif(n.response)switch(n.responseType)%7Bcase%22json%22%3Ar%3DJSON%26%26JSON.stringify(n.response).length%3Bbreak%3Bcase%22blob%22%3Acase%22moz-blob%22%3Ar%3Dn.response.size%3Bbreak%3Bcase%22arraybuffer%22%3Ar%3Dn.response.byteLength%3Bcase%22document%22%3Ar%3Dn.response.documentElement%26%26n.response.documentElement.innerHTML%26%26n.response.documentElement.innerHTML.length%2B28%3Bbreak%3Bdefault%3Ar%3Dn.response.length%7De.mta(%22send%22%2C%22browser.ajax%22%2C%7Burl%3An._url%2Cmethod%3An._method%2Cerror%3A!(0%3D%3D%3Dn.status.toString().indexOf(%222%22)%7C%7C304%3D%3D%3Dn.status)%2CresponseTime%3A(new%20Date).getTime()-n._startTime%2CrequestSize%3An._searchLength%2B(t%3Ft.length%3A0)%2CresponseSize%3Ar%7C%7C0%7D)%7D%7Dif(o%20in%20this)%7Bvar%20i%3Dfunction(e)%7Bn(this%2Ce)%7D%3Bthis%5Bo%5D(%22load%22%2Ci)%2Cthis%5Bo%5D(%22error%22%2Ci)%2Cthis%5Bo%5D(%22abort%22%2Ci)%7Delse%7Bvar%20r%3Dthis%5Ba%5D%3Bthis%5Ba%5D%3Dfunction(t)%7Br%26%26r.apply(this%2Carguments)%2C4%3D%3D%3Dthis.readyState%26%26e.mta%26%26n(this%2Ct)%7D%7Dreturn%20l.apply(this%2Carguments)%7D%7D%7D%7D(window%2Cwindow.XMLHttpRequest%2C%22mta%22)%3B%0A%20%20%20%20mta(%22create%22%2C%2256ea40773e99a92f6fd96866%22)%3B%0A%20%20%20%20mta(%22send%22%2C%22page%22)%3B%0A%3C%2Fscript%3E%0A%0A%0A%0A";

	res.send({
		a: decodeURIComponent(html)
	});
});

/*
app.post('/save', function(req, res) {

	// console.log('req: ', req);

	console.log("post request body", req.body);
	// 需要依赖于前端发送过来的数据
	console.log("post request body data", req.body.data);

	res.send(req.body.data);
});*/

var server = http.createServer(app);

server.listen(app.get('port'), function () {
	console.log('Express start @' + app.get('port'));
});