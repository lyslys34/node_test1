var express = require('express');
var path = require('path');
var http = require('http');
var bodyParser = require('body-parser');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var routes = require('./routes/main');
var user = require('./user');

var app = express();
var port = process.env.PORT || 3434;

var returnUrl = encodeURIComponent('http://peisong.meituan.com/');


app.use(bodyParser.json());

app.set('port', port);
app.use(favicon());
app.use(logger('dev'));
// app.use(bodyParser.json());
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', routes);

app.get('/login', function(req, res) {
  var Cookies = {};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie) {
      var parts = Cookie.split('=');
      Cookies[parts[0].trim()] = (parts[1] || '').trim();
    });
    if (Cookies['fe_sid'] === undefined) {
      res.redirect('http://api.sso-in.sankuai.com/auth?service=' + returnUrl);
    } else {
      getUser(Cookies['fe_sid'], function(userInfo) {
        var userjson = JSON.parse(userInfo);
        console.log('userInfo: ', userjson);
        if (userjson.data.login === null) {
          res.redirect('http%3A%2F%2Ffe.banma.dev.sankuai.com%3A8416%2F%3Fact%3Dlogin');
        } else {
          res.send('success')
        }
      });
    }
});


var server = http.createServer(app);

server.listen(app.get('port'), function () {
  console.log('Express start @' + app.get('port'));
});