var config = {
	tpl_dir : '/Users/liyishan/workspace/banma_api_paotui/src/main/webapp/WEB-INF',     // 指定模板文件的地址
	static_dir : '/Users/liyishan/workspace1/banma_api/src/main/webapp',
	intf_host : '',    // 指定模板文件对应的接口host
	map : [
		['/views/area/', 'create_delivery_area.ftl', ''],
		['/views/equip/', 'list.ftl', ''],
		['/views/equip/add_batch.ftl', '', ''],
		['/views/equip/material_list.ftl', '', ''],
		['/views/equip/add_material.ftl', '', ''],
		['/views/mobile/', 'equip_detail.html', '']
	]
};

module.exports = config;