<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">


<content tag="javascript">/team_manager/list.js</content>
<content tag="cssmain">/team_manager/list.css</content>

<div id="main-container">



    <table class="table">
        <thead>
        <tr>
            <td colspan="3">
                <#--<span class="dispatcher_team_title">${dispatcher_team.name}</span>-->

                <div style="float: right">
                    <span class="dispatcher_team_count">共#{(view.total)!0}人</span>
                    <button id="add_dispatcher" class="btn btn-default btn-add-dispatcher">添加配送员</button>
                </div>
            </td>
        </tr>
        <tr>
            <td>姓名</td>
            <td>手机号</td>
            <td>操作</td>
        </tr>
        </thead>
        <#if (view.userList)?exists && (view.userList?size > 0) >
            <tbody id="tbody-dispatcher-list">
                <#list view.userList as d>
                <tr <@output_props obj = d /> class="dispatcher">
                    <td class="name">${d.name !''}</td>
                    <td class="mobile">${d.mobile !''}</td>
                    <td>
                        <button class="btn btn-default btn-delete-dispatcher">删除</button>
                        <#if orgType == 1 ><a class="btn btn-default" target="_blank" href="/partner/settle/ridersetting?userId=${d.id!""}">结算设置</a></#if>
                    </td>
                </tr>

                </#list>
            </tbody>
            <#if (view.total)?exists && (view.pageSize)?exists && (view.pageNum)?exists && view.total gt 0 && view.pageSize gt 0 && view.pageNum gt 0  >
                <tfoot>
                    <tr>
                        <#assign pageCount = (view.total/view.pageSize)?ceiling   />
                        <#assign size = 6 />
                        <td colspan="3" style="text-align:center">

                        <a href="list?pageNo=1" class="btn btn-sm btn-default">首页</a>
                        <#if view.pageNum gt size+1><span >...</span></#if>

                        <#if view.pageNum lte size>
                            <#if pageCount - view.pageNum gt size>
                                <#list 1..view.pageNum+size as i >
                                    <a href="list?pageNo=#{i}" <#if view.pageNum == i>disabled="disabled"</#if> class="btn btn-sm btn-default">#{i}</a>
                                </#list>
                            </#if>
                            <#if pageCount - view.pageNum lte size>
                                <#list 1..pageCount as i >
                                    <a href="list?pageNo=#{i}" <#if view.pageNum == i>disabled="disabled"</#if> class="btn btn-sm btn-default">#{i}</a>
                                </#list>
                            </#if>
                        </#if>
                        <#if view.pageNum gt size>
                            <#if pageCount - view.pageNum gt size>
                                <#list view.pageNum-size..view.pageNum+size as i >
                                    <a href="list?pageNo=#{i}" <#if view.pageNum == i>disabled="disabled"</#if> class="btn btn-sm btn-default">#{i}</a>
                                </#list>
                            </#if>
                            <#if pageCount - view.pageNum lte size>
                                <#list view.pageNum-size..pageCount as i >
                                    <a href="list?pageNo=#{i}" <#if view.pageNum == i>disabled="disabled"</#if> class="btn btn-sm btn-default">#{i}</a>
                                </#list>
                            </#if>
                        </#if>

                        <#if pageCount - view.pageNum gt size><span >...</span></#if>
                        <a href="list?pageNo=#{pageCount}" class="btn btn-sm btn-default">尾页</a>&nbsp;&nbsp;&nbsp;
                        <span style="display: inline-block; line-height: 28px;">共#{view.total !'0'}人</span>
                    </td>
                    </tr>

                </tfoot>
            </#if>
        </#if>

    </table>
</div>
