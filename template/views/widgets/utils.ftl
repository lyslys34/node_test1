<#-- 截取字符串 -->
<#macro substring str count>
  <#if str?length != 0>
    <#assign name = str>
    <#assign end = (!name?? || name?length lt count)?string((name?length-1)!0, count-1)>
    ${(name[0..end?number])}
    <#if name?length gt count>...</#if>
  </#if>
</#macro>

<#function getPassDomain>
  <#if __environment__?? && __environment__=='test'>
    <#return 'passport.test.meituan.com'>
  <#else>
    <#return 'passport.meituan.com'>
  </#if>
</#function>

<#function getLoginUrl continue>
  <#local settokenUrl = __host__ + '/account/settoken'>
  <#if continue?? && continue?length gt 0>
    <#local settokenUrl = settokenUrl + '?continue=' + continue?url>
  </#if>
  <#return 'https://'+getPassDomain()+'/account/unitivelprogin?service=waimai&continue='+settokenUrl?url>
</#function>

<#function getRegisterUrl continue>
  <#local settokenUrl = __host__ + '/account/settoken'>
  <#if continue?? && continue?length gt 0>
    <#local settokenUrl = settokenUrl + '?continue=' + continue?url>
  </#if>
  <#return 'https://'+getPassDomain()+'/account/unitivesignup?service=waimai&continue='+settokenUrl?url>
</#function>

<#function getLogoutUrl continue>
  <#local settokenUrl = __host__ + '/account/logout'>
  <#if continue?? && continue?length gt 0>
    <#local settokenUrl = settokenUrl + '?continue=' + continue?url>
  </#if>
  <#return 'https://'+getPassDomain()+'/account/unitivelogout?service=waimai&continue='+settokenUrl?url>
</#function>

<#-- 这只是一个简易的方法，应该用java实现一个效率更高的方法 -->
<#macro jsonEncode obj>
  <#if !obj??>null
  <#elseif obj?is_boolean>${obj?string}
  <#elseif obj?is_string>"${obj}"
  <#elseif obj?is_number>${obj}
  <#elseif obj?is_enumerable>[<#list obj as o><@jsonEncode o /><#if o_has_next>,</#if></#list>]
  <#elseif obj?is_hash>{<#list obj?keys as k>"${k}":<@jsonEncode obj[k]/><#if k_has_next>,</#if></#list>}
  <#else>${obj?string}
  </#if>
</#macro>

<#function arrayJoin arr separate=','>
  <#assign result = ''>
  <#if arr?? && arr?size gt 0>
    <#list arr as item>
      <#if item_has_next>
        <#assign result = result + item?string + separate>
      <#else>
        <#assign result = result + item?string>
      </#if>
    </#list>
  </#if>
  <#return result>
</#function>

<#-- partner使用 -->


<#macro output_props obj>
    <#assign keys = obj?keys>
    <#list keys as key>
        <#if key?length gt 3 && key[0..2] == "get" && key != "getClass">
            <#assign pName = key[3..key?length-1]?uncap_first>
            <#if obj[pName]?exists><#assign pValue = obj[pName]> <#if pValue?is_date>data-${pName}="${pValue?date}"<#elseif pValue?is_collection || pValue?is_sequence><#else>data-${pName}="${pValue?string}"</#if></#if>
        </#if>
    </#list>
</#macro>

<#macro output_status status>
    <#switch status>
        <#case 0>等待抢单<#break>
        <#case 1>取餐中<#break>
        <#case 2>配送完成<#break>
        <#case 3>分拣中<#break>
    </#switch>
</#macro>

<#macro output_mode mode>
    <#switch mode>
        <#case 1>每单固定抽成<#break>
        <#case 2>每单比例分成<#break>
        <#case 3>菜品价格折扣<#break>
    </#switch>
</#macro>

<#macro output_query>

    <#if pois?exists>
    &nbsp;&nbsp;商家名称：<select id="poiId">
        <#if deleteAll?exists && deleteAll == 1>
        <#else>
            <option value="0">所有商家</option>
        </#if>
            <#list pois as poi>
                <option value="${poi.poiId}" <#if poiId?exists && poiId == poi.poiId>selected</#if>>${poi.poiName}</option>
            </#list>
        </select>
    </#if>

    <#if dispatchers?exists>
    &nbsp;&nbsp;姓名：<select id="query-dispatchers">
        <#if deleteAll?exists && deleteAll == 1>
        <#else>
                <option value="0">全部</option>
        </#if>
        <#list dispatchers as dispatcher>
            <option value="${dispatcher.dispatcherId}" <#if dispatcherId?exists && dispatcherId == dispatcher.dispatcherId>selected</#if>>${dispatcher.name}</option>
        </#list>
    </select>
    </#if>

    <#if dispatchType?exists>
    &nbsp;&nbsp;任务：<select id="query-dispatch-type" class="query-dispatch-type query-select" data-value="${dispatchType}">
            <option value="0">全部</option>
            <option value="1">从商家到分拣中心/楼口接餐人</option>  <!-- 取餐员 -->
            <option value="2">分配单量</option> <!-- 分拣员 -->
            <option value="3">从分拣中心到用户</option> <!-- 送餐员 -->
        </select>
    </#if>

    &nbsp;&nbsp;
    日期：<input id="query-start-date" class='date-picker J-datepicker' readonly="readonly" value="<#if startDate?exists>${startDate?date}<#else>${.now?string('yyyy-MM-dd')}</#if>">
    至 <input id="query-end-date" class="date-picker J-datepicker" readonly="readonly" value="<#if endDate?exists>${endDate?date}<#else>${.now?string('yyyy-MM-dd')}</#if>">

    <#if timePart?exists>
    &nbsp;&nbsp;
    <select id="query-time-part" class="query-time-part query-select" data-value="${timePart}">
        <option value="0">全天订单</option>
        <option value="1">午市订单</option>
        <option value="2">晚市订单</option>
    </select>
    </#if>

    &nbsp;&nbsp;

    <button id="query-commit" class="btn btn-default">查询</button>
</#macro>


<#macro joinOrgNavi org isOrgManager navitype>
    <#if org?exists>
        <style>
            .tab-bar li a {text-shadow: none}
            .tab-bar li.active a { background: #65cea7; color: #fff;}
        </style>
        <#if org.orgChecked?exists && org.orgChecked == 2>
            <div style="font-size: 17px; font-weight: bold; color:#b22222;">驳回原因:${org.unPassReason !''}</div>
        </#if>
        <div style="font-size: 17px; font-weight: bold; margin-top:10px;">${org.orgName !''} &nbsp; (ID${org.orgId !''})</div>

        <ul class="tab-bar" style="margin:10px 0px;">
            <li <#if navitype?exists && navitype == 1>class="active"</#if>><a href="/org/join/updateOrg?orgId=${org.orgId}">加盟商信息</a></li>
            <#if isOrgManager?exists && isOrgManager == 1>
            <#else >
                <li <#if navitype?exists && navitype == 2>class="active"</#if>><a href="/settle/orgsetting?orgId=${org.orgId}&f=join_org">结算设置</a></li>
            </#if>
            <li <#if navitype?exists && navitype == 3>class="active"</#if>><a href="/rider/listByOrgId?orgId=${org.orgId}&f=join_org">人员列表</a></li>
            <#if isOrgManager?exists && isOrgManager == 1>
                <li <#if navitype?exists && navitype == 4>class="active"</#if>><a href="/org/franchiseeStationList?orgId=${org.orgId}">站点列表</a></li>
            <#else >
                <li <#if navitype?exists && navitype == 4>class="active"</#if>><a href="/org/franchiseeStationList?orgId=${org.orgId}">站点列表</a></li>
                <li <#if navitype?exists && navitype == 5>class="active"</#if>><a href="/log/staff?orgId=${org.orgId}&f=join_org">操作记录</a></li>
            </#if>
        </ul>
    </#if>
</#macro>


<#macro joinCreateOrgStep step>
    <ul class="wizard-steps" style="margin:10px 0px;">
        <li <#if !step?exists || step == 1>class="active"</#if>>
            <a href="javascript:void(0)">Step 1：新建加盟商</a>
        </li>
        <li <#if step?exists && step == 2>class="active"</#if>>
            <a href="javascript:void(0)">Step 2：新建负责人</a>
        </li>
        <li <#if step?exists && step == 3>class="active"</#if>>
            <a href="javascript:void(0)">Step 3：结算设置</a>
        </li>
    </ul>
</#macro>

<#macro commonPop>
<script type="text/javascript">
//    //demo
//    _showPop({
//        type:3,
//        content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + content + "</div>",
//        ok: {
//            display: true,
//            name: "确定",
//            callback: function() {
//                window.location.href="/settle/orgsetting?orgId=" + $("#orgId").val() +"&f=join_org_charge";
//            }
//        }
//    });
    function _showPop(obj) {
        if(obj) {

            var iconTag = $("#poparea").find(".icon_tip");
            iconTag.removeClass("fa-check-circle").removeClass("fa-exclamation-triangle").removeClass("fa-exclamation-trianglchee").removeClass("fa-info-circle");
            if(obj.type == 1) {  //info
                iconTag.addClass("fa-info-circle").css({"color":"#999999"})
            } else if(obj.type == 2) {  //warn
                iconTag.addClass("fa-exclamation-triangle").css({"color":"#f58220"})
            } else if(obj.type == 3) {  //error
                iconTag.addClass("fa-exclamation-triangle").css({"color":"#d93a49"})
            } else if(obj.type == 4) { //success
                iconTag.addClass("fa-check-circle").css({"color":"#1d953f"})
            } else {  //info
                iconTag.addClass("fa-info-circle").css({"color":"#999999"})
            }

            $("#poparea").find(".modal-body").empty().append(obj.content);
            var hasOk = false;
            if(obj.ok && obj.ok.display) {
                hasOk = true;
                $("#poparea_ok").val(obj.ok.name ? obj.ok.name : "确定");

                $("#poparea_ok").css({'display':'inline-block'}).unbind().click(function() {
                    $("#poparea").modal("hide");
                    if(obj.ok.callback) {
                        obj.ok.callback();
                    }
                    return true;
                })
            } else {
                $("#poparea_ok").css({"display":"none"});
            }

            if(obj.cancel && obj.cancel.display) {
                $("#poparea_cancel").val(obj.cancel.name ? obj.cancel.name : (hasOk ? "取消" : "关闭"));
            } else {
                $("#poparea_cancel").val("关闭");
            }

            $("#poparea_cancel").unbind().click(function() {
                $("#poparea").modal("hide");
                if(obj.cancel && obj.cancel.callback) {
                    obj.cancel.callback()
                }
                return false;
            })


            $("#poparea").modal();
        }
    }

</script>
<div class="modal fade" id="poparea">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="icon_tip fa fa-info-circle" style="font-size: 17px;"></i>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <input style="display:none;" type="button" id="poparea_ok" value="确定" class="btn btn-danger btn-sm" />
                <input type="button" class="btn btn-success btn-sm" id="poparea_cancel" value="关闭"/>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="poparea">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="icon_tip fa fa-info-circle" style="font-size: 17px;"></i>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <input style="display:none;" type="button" id="poparea_ok" value="确定" class="btn btn-danger btn-sm" />
                <input type="button" class="btn btn-success btn-sm" id="poparea_cancel" value="关闭"/>
            </div>

        </div>
    </div>
</div>
</#macro>


<#macro createHeadFranchiseeStep step>
<ul class="wizard-steps" style="margin:10px 0px;">
    <li <#if !step?exists || step == 1>class="active"</#if>>
        <a href="javascript:void(0)">Step 1：新建加盟商总账号</a>
    </li>
    <li <#if step?exists && step == 2>class="active"</#if>>
        <a href="javascript:void(0)">Step 2：新建负责人</a>
    </li>
</ul>
</#macro>

<#-- 加盟商总账号详情 -->
<#macro headFranchiseeNavi org navitype>
    <#if org?exists>
        <style>
            .tab-bar li a {text-shadow: none}
            .tab-bar li.active a { background: #65cea7; color: #fff;}
        </style>
        <div style="font-size: 21px; font-weight: bold; margin-top:10px;">${org.orgName !''} &nbsp; (ID:${org.orgId !''})</div>

        <ul class="tab-bar" style="margin:10px 0px;">
            <li <#if navitype?exists && navitype == 1>class="active"</#if>><a href="/headfranchisee/goUpdate?orgId=${org.orgId!'0'}">修改信息</a></li>
            <li <#if navitype?exists && navitype == 2>class="active"</#if>><a href="/headfranchisee/franchiseelist?parentId=${org.orgId!'0'}">子加盟商列表</a></li>
            <li <#if navitype?exists && navitype == 3>class="active"</#if>><a href="/rider/listByOrgId?orgId=${org.orgId!'0'}&f=headfranchisee">人员列表</a></li>
            <li <#if navitype?exists && navitype == 4>class="active"</#if>><a href="/log/staff?orgId=${org.orgId!'0'}&f=headfranchisee">操作记录</a></li>
        </ul>
    </#if>
</#macro>


<#macro zjNav org navitype>
    <#if org?exists>
    <style>
        .tab-bar li a {text-shadow: none}
        .tab-bar li.active a { background: #65cea7; color: #fff;}
    </style>
    <div style="font-size: 21px; font-weight: bold; margin-top:10px;">${org.orgName !''} &nbsp; (ID:${org.orgId !''})</div>

    <ul class="tab-bar" style="margin:10px 0px;">
        <li <#if navitype?exists && navitype == 1>class="active"</#if>><a href="/org/branch/updateOrg?orgId=${org.orgId!'0'}&f=zj_branch">分部信息</a></li>
        <li <#if navitype?exists && navitype == 2>class="active"</#if>><a href="/rider/listByOrgId?orgId=${org.orgId!'0'}&f=zj_branch">人员列表</a></li>
        <li <#if navitype?exists && navitype == 3>class="active"</#if>><a href="/org/zjStations?parentId=${org.orgId!'0'}">站点列表</a></li>
        <li <#if navitype?exists && navitype == 4>class="active"</#if>><a href="/log/staff?orgId=${org.orgId!'0'}&f=zj_branch">操作记录</a></li>
    </ul>
    </#if>
</#macro>