
<aside class="fixed skin-3">
    <div class="sidebar-inner scrollable-sidebar">
        <div class="size-toggle clearfix">
            <#--
            <a class="btn btn-sm" id="sizeToggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            -->
            <a class="btn btn-sm pull-right logoutConfirm_open" href="#logoutConfirm" data-popup-ordinal="1" id="open_81798137">
                <i class="fa fa-power-off"></i>
            </a>
        </div>
        <div class="user-block clearfix">
            <img src="http://cdn.v2ex.com/avatar/ec79/d4be/8612_large.png" alt="User Avatar">
            <div class="detail">
                <strong><#if loginUser??>${loginUser.name}</#if></strong>
            </div>
        </div>
        <div class="search-block">
            <div class="input-group">
                <input type="text" class="form-control input-sm" placeholder="search here...">
        <span class="input-group-btn">
          <button class="btn btn-default btn-sm" type="button"><i class="fa fa-search"></i></button>
        </span>
            </div><!-- /input-group -->
        </div>
        <div class="main-menu">
            <ul>
            <#--<li >-->
            <#--<a href="#">-->
            <#--<span class="text">包裹中心</span>-->
            <#--<span class="menu-hover"></span>-->
            <#--</a>-->
            <#--<ul class="submenu" style="display: block;">-->
            <#--<li>-->
            <#--<a href="#">-->
            <#--<span class="submenu-label">包裹管理</span>-->
            <#--</a>-->
            <#--</li>-->
            <#--</ul>-->
            <#--</li>-->
                <li>
                    <a href="#">
                        <span class="text">组织人员管理</span>
                        <span class="menu-hover"></span>
                    </a>
                    <ul class="submenu" style="display: block;">
                        <li>
                            <a href="/org/orglist">
                                <span class="submenu-label">组织管理</span>
                            </a>
                        </li>
                        <li>
                            <a href="/rider/list">
                                <span class="submenu-label">骑手管理</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</aside>