<aside class="fixed skin-7" style="z-index:999">
    <div class="sidebar-inner scrollable-sidebar">
        <div class="main-menu">
            <ul>
                <!-- UPM动态展示菜单-->
            <#if __menus__?exists && __menus__?size!=0>
              <#list __menus__ as menu>
                <#if menu.menus?exists && menu.menus?size!=0>
                    <li class="openable">
                        <a href="#${menu.title!''}" class="nav-header collapsed" data-toggle="collapse">
                            <@getNavIcon menu />
                            <span class="text">${menu.title!''}</span>
                            <span class="menu-hover"></span>
                        </a>
                        <ul id="${menu.title!''}" class="nav nav-list submenu collapse">
                          <#list menu.menus as sub_menu>
                              <li>
                                  <a href="${sub_menu.url!''}" <#if sub_menu.type?exists && sub_menu.type == 2> target="_blank" </#if>>
                                      <span class="submenu-label">${sub_menu.title!''}</span>
                                  </a>
                              </li>
                          </#list>
                        </ul>
                    </li>
                </#if>
              </#list>
            </#if>
            </ul>
        </div>
    </div>
</aside>

<#macro getNavIcon item>
  <#if item.title??>
    <div class="fa-wrapper">
      <#switch item.title>
        <#case '包裹中心'>
          <i class="fa fa-lg fa-custom-package"></i>
          <#break>
        <#case '配送员数据中心'>
          <i class="fa fa-lg fa-custom-delivery-clerk"></i>
          <#break>
        <#case '组织架构'>
          <i class="fa fa-lg fa-custom-architecture"></i>
          <#break>
        <#case '结算中心'>
          <i class="fa fa-lg fa-custom-clearing-center"></i>
          <#break>
        <#case '数据中心'>
          <i class="fa fa-lg fa-custom-data-center"></i>
          <#break>
        <#case '众包管理'>
          <i class="fa fa-lg fa-custom-crowdsourcing"></i>
          <#break>
        <#case '角马管理'>
          <i class="fa fa-lg fa-custom-wildebeest"></i>
          <#break>
        <#case '系统管理'>
          <i class="fa fa-lg fa-custom-system-manage"></i>
          <#break>
        <#case '业务管理'>
          <i class="fa fa-lg fa-custom-business-manage"></i>
          <#break>
        <#case '调度中心'>
          <i class="fa fa-lg fa-custom-dispatching-center"></i>
          <#break>
        <#case '报表'>
          <i class="fa fa-lg fa-custom-report-form"></i>
          <#break>
        <#default>
          <i class="fa fa-lg fa-custom-package"></i>
      </#switch>
  </div>
  </#if>
</#macro>