<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/poi/poi_audit.js</content>

<style type="text/css">
    #main-container{min-height: 0;}
    #wrapper{min-height: 0; }
    .opContainer .row{padding: 6px 0;}
    .iconEdit{cursor:pointer}
    #pager a{cursor:pointer}
</style>

<div id="breadcrumb-bar">
    <ul class="breadcrumb">
        <li style="color:#757986;font-size:14px;">众包管理<span class="divider"></span></li>
        <li style="color:#757986;font-size:14px;">商家审核<span class="active"></span></li>
    </ul>
</div>

<div id="main-container">

    <div class="panel panel-default">

        <div class="panel-body">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">商家名</label>
                        <input  class="form-control input-sm" id="storeName"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">商家POI</label>
                        <input  class="form-control input-sm" id="storePOI"/>
                    </div>
                </div>
               
                <div class="col-md-8">
                    <label class="control-label"></label>
                    <div class="form-group">
                        <button type="button" id="btnSearch" class="btn btn-success">查询</button>
                    </div>
                </div>

            </div>
           
        </div>

    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>商家名</th>
                <th>商家POI</th>
                <th>商家电话</th>
                <th>商家地址</th>
                <th>创建时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody id="list">
            

            </tbody>
        </table>
        <div style="float: left; padding: 10px; 15px"><span id="countLable"></span></div>

        <div class="panel-footer clearfix">
            <ul id="pager" class="pagination pagination-xs m-top-none pull-right">
            </ul>
        </div>

    </div>


    <div class="panel panel-default table-responsive" style="display:none">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>操作类型</th>
                <th>操作内容</th>
                <th>操作人</th>
                <th>操作时间</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td><td></td><td></td><td></td>
            </tr>

            </tbody>
        </table>
        <div style="float: left; padding: 10px; 15px"><span></span></div>

        <div class="panel-footer clearfix">
            <ul id＝"pager" class="pagination pagination-xs m-top-none pull-right">

                

            </ul>
        </div>

    </div>



</div>


