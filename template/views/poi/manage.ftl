<#include "poi_config.ftl">
<#include "../widgets/sidebar.ftl">

<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=820afe3932ebea15d73cd05b2a28e606"></script>

<content tag="cssmain">/poi/poi_manage.css</content>
<content tag="javascript">/poi/poi_manage.js</content>

<style type="text/css">
    #mapContainer {
        width: 600px;
        height: 600px;
      }
      .amap-layers img {max-width: none;}
</style>

<div id="main-container">
    <div class="panel panel-default">
    <#if isOpManager??>
        <#if (isOpManager)>
            <span id="canOperation" style="display: none;">1</span>
        </#if>
    </#if>
    <#if isManager>
        <h1 id="isManager" style="display: none;">1</h1>
    <#else>
        <h1 id="isManager" style="display: none;">0</h1>
    </#if>
    <#if !isBaseManager>
        <#if (isManager)>
            <h1 id="isBaseManager" style="display: none;">1</h1>
        <#else>
            <h1 id="isBaseManager" style="display: none;">0</h1>
        </#if>
    </#if>
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
            <#if (poiType!=1)>
                <li role="presentation" class="active" id="all-poi-type">
                    <a href="#normal" id="normal-tab" aria-controls="normal" role="tab" data-toggle="tab">全部商家管理</a>
                </li>
                <li role="presentation" id="proxy-poi-type">
                    <a href="#normal" id="agency-tab" aria-controls="normal" role="tab" data-toggle="tab">代购商家管理</a>
                </li>
            <#else>
                <li role="presentation" id="all-poi-type">
                    <a href="#normal" id="normal-tab" aria-controls="normal" role="tab" data-toggle="tab">全部商家管理</a>
                </li>
                <li role="presentation" class="active" id="proxy-poi-type">
                    <a href="#normal" id="agency-tab" aria-controls="normal" role="tab" data-toggle="tab">代购商家管理</a>
                </li>
            </#if>
            <#if (showCleanTool)>
                <li role="presentation" id="tool-poi-lbs">
                    <a href="#poi-lbs" id="poi-lbs-tab" aria-controls="poi-lbs" role="tab" data-toggle="tab">商家坐标维护</a>
                </li>
            </#if>
            </ul>
            <div style="width: 100%;height: 2px;background: #58B7B1;"></div>

            <div class="row" style="margin-top: 20px;padding-left: 0px;">
                <div class="form-horizontal" role="form">
                    <div class="col-md-12">
                        <div class="col-md-3" style="padding-left: 0px">
                            <div class="form-group">
                                <label for="city" class="col-sm-4 control-label"
                                       style="text-align: left;">城市</label>

                                <div class="col-sm-7" style="padding-top: 5px;">
                                    <select class="form-control" id="city-selector" style="width:100px">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="poi-status" class="col-sm-4 control-label"
                                       style="text-align: left;">商家状态</label>

                                <div class="col-sm-7" style="padding-top: 5px;">
                                    <select class="form-control" id="work-status">
                                        <option value="-1">全部</option>
                                        <option value="1">营业</option>
                                        <option value="3">休息</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" style="padding-left: 0px">
                            <div class="form-group">
                                <label for="region" class="col-sm-4 control-label"><a data-toggle="tooltip"
                                                                                      title="如果先选择城市就可以选择对应城市的配送区域哦"
                                                                                      data-placement="top"
                                                                                      href="javascript:;"
                                                                                      name="delete">
                                    <i class="fa fa-question-circle fa-lg opration-icon"></i></a>配送区域</label>

                                <div class="col-sm-7" style="padding-top: 5px;">
                                    <select class="form-control" id="region-selector">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-4" style="margin-left: 10px;">
                                    <button id="search" class="btn btn-default"
                                            style="width: 100px;background-color: #58B7B1;color: white;">搜索
                                    </button>
                                </div>
                                <div class="col-sm-offset-2 col-sm-4" style="margin-left: 10px;">
                                    <button id="view-map-btn" class="btn btn-default" style="width: 100px;">
                                        查看地图
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" style="padding-left: 0px">
                            <div class="form-group">
                                <label for="poi-name" class="col-sm-4 control-label">商户名称</label>

                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="poi-name" value="${poiName}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" style="padding-left: 0px">
                            <div class="form-group">
                                <label for="poi-id" class="col-sm-4 control-label">商户ID</label>

                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="poi-id" value="${poiId}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="normal">

                    <div class="row cannot_see" style="visibility: none;padding: 5px 15px;">

                    <span>
                      <h5 style="display: inline;margin-right: 10px;">批量操作</h5>
                      <a data-toggle="modal" href="" class="btn btn-default" style="display: inline;margin-left: 25px;"
                         id="extend-delivery-time-btn">延迟配送时长</a>
                      <a data-toggle="modal" href="" class="btn btn-default" style="display: inline;margin-left: 25px;"
                         id="close-poi-btn">停止营业</a>
                      <a data-toggle="modal" href="" class="btn btn-default" style="display: inline;margin-left: 25px;"
                         id="open-poi-btn"">开始营业</a>
                    </span>
                    <#if (isOpManager??)>
                        <#if isOpManager>
                        </#if>
                    </#if>
                    </div>
                    <div style="overflow-x:auto">
                    <table class="table" style="vertical-align: middle;min-width:1500px;">
                        <tr>
                            <th class="cannot_see">
                                <input style="opacity: 1;" type="checkbox" name="checkUser" id="checkbox-all"
                                       value="nocheck"/>
                            </th>
                            <th>商家ID</th>
                            <th>商家名称</th>
                            <th>城市</th>
                            <th>配送区</th>
                            <th>营业时间</th>
                            <th>商家电话</th>
                            <th>商家地址</th>
                            <th class="agency_cansee" style="display: none;">
                                分级
                                <a data-toggle="tooltip" title="在严重爆单的情况下,系统分批关闭代购店,先关闭一级代购店,然后关闭二级代购店" data-placement="top"
                                   href="javascript:;" name="delete">
                                    <i class="fa fa-question-circle fa-lg opration-icon"></i>
                                </a>
                            </th>
                            <th>上下线</th>
                            <th>营业状态</th>
                            <th>起送价</th>
                            <th>配送费</th>
                            <th><span title="">周日均单量</span><a data-toggle="tooltip" title="" data-placement="top"
                                                              href="javascript:;" name="delete"
                                                              data-original-title="过去7天，该商家的日均运单量">
                                <i class="fa fa-question-circle fa-lg opration-icon"></i></a></th>
                            <th>平均配送时长</th>
                            <th>负责BD</th>
                            <th>配送范围</th>
                            <th class="cannot_see">操作</th>
                        </tr>
                        <tbody id="poi-table">

                        </tbody>
                    </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="poi-lbs">
                <#include "pio_clean.ftl"/>
                </div>
            </div>
            <ul class="pagination pagination-xs pull-left" id="pager" style="margin-bottom: 10px;">
            </ul>
        </div>
    </div>
</div>

<div class="modal fade" id="area-map">
    <div class="modal-dialog" style="width: 630px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">查看配送区域</h4>
            </div>
            <div class="modal-body">
                <div id="mapContainer"></div>
            </div>
        </div>
    </div>
</div>
<div id="extend-delivery-time" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">延长配送时长</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal" style="margin-top: 15px;">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">延长配送时长</label>

                        <div class="col-sm-9">
                            <select class="form-control" id="extend-time">
                                <option value="30" selected="selected">30分钟</option>
                                <option value="60">60分钟</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">配置生效至</label>

                        <div class="col-sm-9">
                            <select class="form-control" id="extend-end-time">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cancel-extend-delivery-time">取消
                </button>
                <button id="commit-extend-delivery-time" class="btn btn-default">提交</button>
            </div>
        </div>
    </div>
</div>
<div id="close-pois" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">停止营业</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal" style="margin-top: 15px;">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">停止营业至</label>

                        <div class="col-sm-10">
                            <select class="form-control" id="close-end-time">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cancel-close-pois">取消</button>
                <button id="commit-close-pois" class="btn btn-default">提交</button>
            </div>
        </div>
    </div>
</div>
<div id="open-pois" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">开始营业</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal" style="margin-top: 15px;">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:60%;">选择的商家将营业开始接单，是否确认？</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cancel-open-pois">取消</button>
                <button id="commit-open-pois" class="btn btn-default">提交</button>
            </div>
        </div>
    </div>
</div>
<script>
    document.title = '商家管理';
    $('#city-selector').select2();
    $('#region-selector').select2();
    $('#work-status').select2();
    var city_s = $('#city-selector');
    var region_s = $('#region-selector');
    var poi_s = $('#work-status');
    var extend_end_time_s = $('#extend-end-time');
    var close_end_time_s = $('#close-end-time');

    poi_s.select2("val",${workStatus});
    var selectedAreaId = ${areaId};
    var selectedCityId = ${cityId};
    var poiType = ${poiType};
</script>
