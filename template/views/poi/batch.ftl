<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/poi/poi_batch.js</content>

<style type="text/css">
    #main-container{min-height: 0;}
    #wrapper{min-height: 0; }
    .opContainer .row{padding: 6px 0;}
    a.iconEdit{color:#428bca; margin-left:10px;}
    a.iconEdit:hover{text-decoration: underline}
    #pager a,.iconEdit,.iconLog,#logPager a{cursor:pointer}
    .btn-group{margin-right:10px}
    .memo{line-height:28px}
    .btnMemo{float: left;line-height: 28px;padding-left: 10px; color:red}
    input::-webkit-outer-spin-button,input::-webkit-inner-spin-button{-webkit-appearance: none !important;margin: 0; }
</style>

<div id="main-container">

    <div class="panel panel-default">

        <div class="panel-body">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">城市</label>
                        <select class="form-control input-sm" id="cityId" name="cityId"><option value="-1">全部</option></select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">配送区域</label>
                        <select class="form-control input-sm" id="areaId" name="areaId">
                            <option value="-1">请先选择城市</option>
                        </select>
                    </div>
                </div>
                 <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">区域类型</label>
                        <select class="form-control input-sm" id="areaType" name="areaType">
                            <option value="-1">全部类型</option>
                            <option value="1">自建</option>
                            <option value="2">加盟</option>
                            <option value="4">众包</option>
                            <option value="7">代理城市</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label class="control-label">是否置休</label>
                        <select class="form-control input-sm" id="openStatus" name="openStatus">
                            <option value="-1">全部</option>
                            <option value="0">是</option>
                            <option value="1">否</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-1">
                    <div class="form-group">
                        <label class="control-label">是否下活动</label>
                        <select class="form-control input-sm" id="actStatus" name="actStatus">
                            <option value="-1">全部</option>
                            <option value="0">是</option>
                            <option value="1">否</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-1">
                    <div class="form-group">
                        <label class="control-label">配送费</label>
                        <select class="form-control input-sm" id="deliveryStatus" name="deliveryStatus">
                            <option value="-1">全部</option>
                            <option value="1">原价</option>
                            <option value="0">增加</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <label class="control-label"></label>
                    <div class="form-group">
                        <button type="button" id="btnSearch" class="btn btn-success">查询</button>
                         <a target="_blank" href="/batchpoi/log" class="btn btn-warning">操作记录</a>
                    </div>
                </div>
                

            </div>
            <div class="row">&nbsp;</div>
            <div class="row">

                <div class="col-md-12">

                    <label class="control-label">批量设置&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <div class="btn-group">
                    <button type="button" id="btnActStatus" class="btn btn-success">活动开启/关闭</button>
                    </div>
                   <div class="btn-group">
                    <button type="button" id="btnStoreStatus" class="btn btn-warning">商户营业/置休</button>
                    </div>
                   <div class="btn-group">
                    <button type="button" id="btnPriceStatus" class="btn btn-info">调整配送费</button>
                </div>
               </div>



            </div>
        </div>

    </div>

    <div class="panel panel-default table-responsive" id="divCityList">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>城市</th>
                <th>城市ID</th>
                <th>区域</th>
                <th>区域ID</th>
                <th>区域类型</th>
                <th class="text-center">是否置休</th>
                <th class="text-center">是否下活动</th>
                <th class="text-center">配送费调整</th>
                <th style="width:150px">操作</th>
            </tr>
            </thead>
            <tbody id="list">
           
            </tbody>
        </table>
        <div style="float: left; padding: 10px; 15px"><span id="countLable"></span></div>

        <div class="panel-footer clearfix">
            <ul id="pager" class="pagination pagination-xs m-top-none pull-right">
            </ul>
        </div>

    </div>





</div>


<div class="modal fade" id="confirmDialog" style="z-index:2000">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
           <h4 class="modal-title">警告</h4></div>
          <div class="modal-body">
                <div class="row">
                <div class="text-center"><h5>本操作会引起全部商家状态变化，影响重大<br><br>确认是否继续</h5></div>
          </div>
           <div class="modal-footer">
                <button type="button" class="btn btn-success submit">确定</button>
                <button type="button" class="btn btn-default cancel">放弃</button>
           </div>
       </div>
   </div>
</div>



