
<#include "../config.ftl">

<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=820afe3932ebea15d73cd05b2a28e606"></script>
<content tag="javascript">/poi/poi_mapview.js</content>
<content tag="cssmain">/task_manage/mapRiderState.css</content>

<script type="text/javascript">

</script>

<div id="main-container" style="margin:0 0;">
	<div id="map-topbar" style="white-space:nowrap;">
	
		<ul id="org-info">
			<select class="input-sm" id="select-city" style="padding-top:10px;width:100px;">
			</select>
			<select class="input-sm" id="select-area" style="padding-top:10px;width:100px;">
            </select>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <div class="pull-right" style="padding-right:100px;padding-top:10px;">
                  <label class="control-label valtype inline" data-valtype="label" style="color:#FFFFFF">周日均单量:</label>
                  <span style="color:#ED794E">
                  <input type="checkbox" id="way-bill-level-1" value="1" checked="checked" id="rider-recommend-input" style="opacity:1;position:static;">
                  ≤ 1单
                  </span>
                  <span style="color:#4FBDDB">
                  <input type="checkbox" id="way-bill-level-2" value="2" checked="checked" id="rider-recommend-input" style="opacity:1;position:static;">
                  2 - 9 单
                  </span>
                  <span style="color:#48DBC0">
                  <input type="checkbox" id="way-bill-level-3" value="3" checked="checked" id="rider-recommend-input" style="opacity:1;position:static;">
                  10 - 19单
                  </span>
                  <span style="color:#FFFF80">
                  <input type="checkbox" id="way-bill-level-4" value="4" checked="checked" id="rider-recommend-input" style="opacity:1;position:static;">
                  ≥ 20单
                  </span>
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <div class="pull-right" style="padding-right:80px;padding-top:10px;">
                  <label class="control-label valtype inline" data-valtype="label"style="color:#FFFFFF">显示:</label>
                  <span style="color:#3c8ca3">
                  <input type="checkbox" id="show-delivery-area" checked="checked" value="1" style="opacity:1;position:static;">
                  配送区域
                  </span>
                  <span style="color:#e33614">
                  <input type="checkbox" id="show-aor-area" value="2"  style="opacity:1;position:static;">
                  蜂窝
                  </span>
                </div>
		</ul>
	</div>

	<div style="top:50px; position:absolute; min-height:800px;width:100%">
            
	    <div style="border:solid 1px; margin-top:0px; min-height:700px;" id="mapContainer">
	        
	    </div>

    </div>
    <div id="poi-price-info" style="border:solid 1px;background-color:white;position:absolute;bottom:70px;right:2px;width:200px;z-index:3000;display:none;max-height:200px;overflow-y:auto">

    </div>
    <span id="city-id" style="display:none">${cityId}</span>
    <span id="area-id" style="display:none">${areaId}</span>

</div>
<script>
    $("#select-city").select2();
    $("#select-area").select2();
    var citySelector = $("#select-city");
    var areaSelector = $("#select-area");

</script>
