<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/poi/poi_batch_log.js</content>
<link href="/static/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" rel="stylesheet" />
<style type="text/css">
    #main-container{min-height: 0;}
    #wrapper{min-height: 0; }
    #pager a,.iconEdit,.iconLog,#logPager a{cursor:pointer}
    .memo{line-height:28px}
</style>



<div id="main-container">

    <div class="panel panel-default" style="display:none">
        
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">操作类型</label>
                        <select class="form-control input-sm" id="opType" name="opType">
                            <option value="">全部</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">操作内容</label>
                        <input placeholder="请输入要包含的关键字" class="form-control input-sm" id="opComment"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">操作人</label>
                        <input class="form-control input-sm" id="opPerson"/>
                    </div>
                </div>
                <div class="col-md-6">
                     <div class="form-group">
                         <label class="control-label">操作时间</label>
                    <div class="form-inline">
                            <input type="text" class="form-control input-sm date-picker J-datepicker" id="timeStr1" value="">~
                            <input type="text" class="form-control input-sm date-picker J-datepicker" id="timeStr2" value="">
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="btn-group">
                        <button type="button" id="btnSearch" class="btn btn-success">查询</button>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <div class="panel panel-default table-responsive">
       <div class="panel-footer" id="listTitle">
         <h4 style="float: left">操作记录</h4>
      </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>操作类型</th>
                <th>操作内容</th>
                <th>操作人</th>
                <th>操作原因</th>
                <th>操作时间</th>
            </tr>
            </thead>
            <tbody id="logList">
            <tr><td colspan="5">加载中</td></tr>
            </tbody>
        </table>
        <div style="float: left; padding: 10px; 15px"><span id="countLogLabel"></span></div>

        <div class="panel-footer clearfix">
            <ul id="logPager" class="pagination pagination-xs m-top-none pull-right"> </ul>
        </div>

    </div>



</div>


