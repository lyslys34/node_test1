<div id="poi-list-container" class="row" style="margin-top: 20px;padding-left: 0px;">
    <div class="col-md-3">
        <ul id="poi-list" class="list-group">

        </ul>
    </div>
    <div class="col-md-9">
        <div id="poiFetchPointsMapContainer"></div>
    </div>

    <div id="locationPickerContainer">
        <div>
            坐标：(<b>鼠标在地图上单击获取经纬度坐标</b>)
            <br><input type="text" id="locationXY" class="form-control input-sm" onfocus="this.select();"/>
        </div>
    </div>
</div>

<div id="point-loading" class="alert alert-success" role="alert">
    <div class="progress" style="height: 20px;">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">0%</div>
    </div>
    <span class="progress-value">正在奋力加载……</span>
</div>





