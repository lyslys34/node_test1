<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">

<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=820afe3932ebea15d73cd05b2a28e606"></script>
<content tag="pageName">调度回放</content>
<content tag="javascript">/playback/playback.js</content>
<content tag="cssmain">/playback/playback.css</content>

<style type="text/css">

.amap-layers img {
    max-width: none;
}

#mapContainer {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
}
</style>

<div id="main-container">

    <div style="padding: 5px 20px;">

        <div class="text-left">
            <span>
                <!-- <select id="selectRider" class="input-sm" style="max-width:150px;padding:0 0;font-size:14px">
                    <option selected="selected" value="4">全部骑手</option>
                    <option value="2">推荐骑手</option>
                    <option value="1">非推荐骑手</option>
                    <option value="0">非参与骑手</option>
                </select> -->
                <span>订单号：</span>
                <input type="text" class="input-sm" id="waybillId_input" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn btn-default" id="btn-search">查询</button>
            </span>
        </div>

        <div style="padding-top:5px">
            <table class="table table-striped table-bordered" style="marigin-bottom:0">
                <thead>
                    <tr>
                        <th>状态</th>
                        <th>商家</th>
                        <th>取货地址</th>
                        <th>送货地址</th>
                        <th>站点</th>
                        <th>骑手</th>
                        <th>下单时间</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody id="waybill_infobody">
                </tbody>
            </table>
            <p class="hidden text-center" id="loading_info">加载中</p>
        </div>

        <div style="margin-top:5px; position:relative; min-height:600px;">

            <div style="margin-right:200px; border:solid 1px;" id="mapContainer">


            </div>
            <div class="pull-right" style="width:210px; height:100%; padding-left:15px;">

                <div class="text-center" style="font-size:14px; width:100%; color:#000;">调度记录</div>


                    <div>

                     <table class="table table-striped table-bordered" style="table-layout:fixed;width:200px;margin-bottom:0" >
                            <thead class="fixedThead" >
                                <tr>
                                    <th style="width: 42px;">次序</th>
                                    <th style="width: 65px;">调度时间</th>
                                    <th style="width: 65px;">推荐人数</th>
                                    <th style="width:28px;">
                                        <input type="checkbox" style="opacity:1;position:static;" checked="checked"  class="allStage"/>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                      </div>
                      <div id="playRecordTable">
                           <table class="table table-striped table-bordered" style="table-layout:fixed;width:200px;">
                             <tbody id="playRecord">

                            </tbody>
                            </table>
                      </div>









                <br>

                <span><img src="/static/imgs/getGoods.png"  >&nbsp;&nbsp;&nbsp;取货地址</span>
                <br><br>
                <span><img src="/static/imgs/sendGoods.png" >&nbsp;&nbsp;&nbsp;送货地址</span>
                <br><br>

                    <label id="rider-recommend" for="rider-recommend-input"><img src="/static/imgs/rider-recommend1.png">&nbsp;&nbsp;&nbsp;推荐骑手</label>&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="checkbox" value="2" checked="checked" id="rider-recommend-input" class="check rider-recommend" style="opacity:1;position:static;">


                <br><br>

                    <label id="rider-notRecommend" for="rider-notRecommend-input"><img src="/static/imgs/rider-notRecommend1.png">&nbsp;&nbsp;&nbsp;非推荐骑手</label>
                    <input type="checkbox" value="1" checked="checked" id="rider-notRecommend-input" class="check rider-notRecommend" style="opacity:1;position:static;">


                <br><br>

                    <label id="rider-noScore" for="rider-noScore-input"><img src="/static/imgs/rider-noScore1.png">&nbsp;&nbsp;&nbsp;非参与骑手</label>
                    <input type="checkbox" value="0" checked="checked" id="rider-noScore-input" class="check rider-noScore" style="opacity:1;position:static;">


                <span id="noScoreCount"></span>
                <br><br>
            </div>

        </div>

    </div>

</div>