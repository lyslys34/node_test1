<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">

<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=820afe3932ebea15d73cd05b2a28e606"></script>
<content tag="pageName">调度回放</content>
<content tag="javascript">/playback/playbackHome.js</content>
<content tag="cssmain">/playback/playbackHome.css</content>

<style type="text/css">

.amap-layers img {
    max-width: none;
}

#mapContainer {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
}
</style>

<div id="main-container">

    <div class="bm_btngroup">
        <span toid="#summary_content" class="bm_tabbtn selected">推单统计概览</span>
        <span toid="#rider_content" class="bm_tabbtn">骑手推单记录</span>
        <span toid="#order_content" class="bm_tabbtn">订单推送记录</span>
    </div>

    <div id="tab_content">
        
        <div id="summary_content" class="">

        
            <div id="loading_base_info">加载中...</div>
            <button id="refresh_base_info" class="btn btn-danger hidden">刷新</button>
            <div id="su_condition" class="hidden">
                <input type="text" value="${.now?string('yyyy-MM-dd')}" class="datepickerInput input-sm date-picker J-datepicker" maxlength="10"  readonly="readonly"  style="width: 224px; cursor: pointer">

                    <i class="fa fa-angle-left date-change-left"></i>
                    <i class="fa fa-angle-right date-change-right disabled"></i>
                </input>&nbsp;&nbsp;&nbsp;
                <select class="input-sm" id="selectCity"></select>&nbsp;&nbsp;&nbsp;
                <select id="selectArea" class="input-sm"></select>&nbsp;&nbsp;&nbsp;
                <select id="selectOrg" class="input-sm"></select>&nbsp;&nbsp;&nbsp;
                <button class="btn btn-success" id="load_summary">查询</button>
            </div>
            <div id="loading_summary_table" class="hidden">加载中...</div>
            <div id="dataTableArea" class="hidden">
            </div>
        </div>

        <div id="rider_content" class="hidden">
            <div id="loading_org">加载中...</div>
            <div id="rider_condition" class="hidden">
                <input type="text" value="${.now?string('yyyy-MM-dd')}" class="datepickerInput input-sm date-picker J-datepicker" maxlength="10"  readonly="readonly"  style="width: 224px; cursor: pointer">

                    <i class="fa fa-angle-left date-change-left"></i>
                    <i class="fa fa-angle-right date-change-right disabled"></i>
                </input>&nbsp;&nbsp;&nbsp;
                <select id="rider_selectOrg" class="input-sm"></select>&nbsp;&nbsp;&nbsp;
                <span id="rider_select2" class="hidden">
                <select id="rider_selectrider" class="input-sm"></select>&nbsp;&nbsp;&nbsp;
                </span>
                <input id="rider_input" class="input-sm hidden" type="text" placeholder="请输入骑手手机号"></input>&nbsp;&nbsp;&nbsp;
                <button class="btn btn-success" id="load_record">查询</button>

                <div id="filter" class="pull-right">
                    <input type="checkbox" id="filter1" value="pushFilter"/>
                    <label class="filter_l" for="filter1" >仅看推送成功</label>
                    <span id="only_push_count"></span>&nbsp;&nbsp;

                    <input type="checkbox" id="filter2" value="checkFilter" />
                    <label class="filter_l" for="filter2" >仅看已读</label>
                    <span id="only_check_count"></span>&nbsp;&nbsp;

                    <input type="checkbox" id="filter3" value="grabFilter" />
                    <label class="filter_l" for="filter3" >仅看抢单成功</label>
                    <span id="only_grab_count"></span>&nbsp;&nbsp;
                </div>
            </div>

            <div id="rider_head_info">未选中骑手</div>
            <table id="rider_record_table" class="table table-striped">
                <thead>
                    <tr>
                        <th class="click_sort sort_down" value="dispatchTime">被推单时间
                            <i class="fa fa-lg  pos-aboult fa-caret-down"></i>
                            <i class="fa fa-lg  pos-aboult fa-caret-up"></i>
                        </th>
                        <th class="click_sort sort_none" value="dispatchType">调度类型
                            <i class="fa fa-lg  pos-aboult fa-caret-down"></i>
                            <i class="fa fa-lg  pos-aboult fa-caret-up"></i>
                            </th>
                        <th class="click_sort sort_none" value="pushTime">订单推送成功时间
                            <i class="fa fa-lg  pos-aboult fa-caret-down"></i>
                            <i class="fa fa-lg  pos-aboult fa-caret-up"></i>
                        </th>
                        <th class="click_sort sort_none" value="checkTime">当前骑手首次已读时间
                            <i class="fa fa-lg  pos-aboult fa-caret-down"></i>
                            <i class="fa fa-lg  pos-aboult fa-caret-up"></i>
                        </th>
                        <th class="click_sort sort_none" value="grabResult">当前骑手抢单结果
                            <i class="fa fa-lg  pos-aboult fa-caret-down"></i>
                            <i class="fa fa-lg  pos-aboult fa-caret-up"></i>
                        </th>
                        <th class="click_sort sort_none" value="waybillId">被调度订单
                            <i class="fa fa-lg  pos-aboult fa-caret-down"></i>
                            <i class="fa fa-lg  pos-aboult fa-caret-up"></i>
                            
                        </th>
                        <th>订单接单时间</th>
                        <th>订单完成骑手</th>
                        <th style="width:58px" class="text-center">操作</th>
                    </tr>

                </thead>
                <tbody id="rider_record_tbody">
                    
                </tbody>
            </table>
            <div id="record_footer"></div>
        </div>

        <div id="order_content" class="hidden">
            <div style="padding: 5px;">

                    <div class="text-left">
                        <span>
                            <span>订单号：</span>
                            <input type="text" class="input-sm" id="waybillId_input" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <button class="btn btn-default" id="btn-search">查询</button>
                        </span>
                    </div>

                    <div style="padding-top:5px">
                        <table class="table table-striped table-bordered" style="marigin-bottom:0">
                            <thead>
                                <tr>
                                    <th>状态</th>
                                    <th>站点.商圈</th>
                                    <th>商家</th>
                                    <th>送货地址</th>
                                    <th>下单时间</th>
                                    <th>期望时间</th>
                                    <th>骑手</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody id="waybill_infobody">
                            </tbody>
                        </table>
                        <p class="hidden text-center" id="loading_info">加载中</p>
                    </div>

                    <div style="margin-top:5px; position:relative; min-height:600px;">

                        <div style="margin-right:260px; border:solid 1px;" id="mapContainer">
                            <select class="input-sm hidden" id="map_select_rider">
                            </select>
                        </div>
                        <div class="pull-right" style="width:260px; height:100%; padding-left:15px; position:relative">

                            <div class="text-center" style="font-size:14px; width:100%; color:#000;"></div>
                                <div>
                                    <table class="table table-striped table-bordered" style="table-layout:fixed;margin-bottom:0" >
                                        <thead class="fixedThead" >
                                            <tr>
                                                <th style="width: 85px;">调度类型</th>
                                                <th style="width: 65px;">调度时间</th>
                                                <th style="width: 65px;">推荐人数</th>
                                                <th style="width:28px;"></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="playRecordTable">
                                    <table class="table" style="table-layout:fixed;margin-bottom:0" >
                                        <tbody id="playRecord">

                                        </tbody>
                                    </table>
                                </div>

                            <br>

                            <span><img src="/static/imgs/getGoods.png"  >&nbsp;&nbsp;&nbsp;取货地址</span>
                            <br><br>
                            <span><img src="/static/imgs/sendGoods.png" >&nbsp;&nbsp;&nbsp;送货地址</span>
                            <br><br>

                                <label id="rider-recommend" for="rider-recommend-input"><img src="/static/imgs/rider-recommend1.png">&nbsp;&nbsp;&nbsp;推荐骑手</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="checkbox" value="2" checked="checked" id="rider-recommend-input" class="check rider-recommend" style="opacity:1;position:static;">
                                <span id="recommend_count" class="color_underline"></span>

                            <br><br>

                                <label id="rider-notRecommend" for="rider-notRecommend-input"><img src="/static/imgs/rider-notRecommend1.png">&nbsp;&nbsp;&nbsp;非推荐骑手</label>
                                <input type="checkbox" value="1" checked="checked" id="rider-notRecommend-input" class="check rider-notRecommend" style="opacity:1;position:static;">
                                <span id="not_recommend_count"></span>

                            <br><br>

                                <label id="rider-noScore" for="rider-noScore-input"><img src="/static/imgs/rider-noScore1.png">&nbsp;&nbsp;&nbsp;非参与骑手</label>&nbsp;&nbsp;

                                <span id="no_score_count" class="color_underline"></span>
                            <br><br>
                            <div id="listTip" class="hidden"></div>
                        </div>
                    </div>
                </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    var selCity = $('#selectCity');
    var selArea = $('#selectArea');
    var selOrg = $('#selectOrg');
    var mySelect2 = $.fn.select2;

    var rider_selOrg = $('#rider_selectOrg');
    var rider_list = $('#rider_selectrider');
    var map_selRider = $('#map_select_rider');
</script>