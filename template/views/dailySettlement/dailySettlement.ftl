<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/dailySettlement/dailySettlement.js</content>
<content tag="cssmain">/dailySettlement/dailySettlement.css</content>
<content tag="pageName">dailySettlement</content>
<div id="main-container">
    <div style="content_body">

        <div class="navi_head">
            <div class="navi_title">当日结款</div>
            <div class="navi_bottom_line">&nbsp;</div>
        </div>

        <div class="condition_area">
            <form class="form-inline">
                &nbsp;&nbsp;
                结款日期：<input id="query-start-date" style="cursor: pointer"
                          class='form-control input-sm date-picker J-datepicker js_date_start' readonly="readonly"
                          value="<#if startDate?exists>${startDate}<#else>${.now?string('yyyy-MM-dd')}</#if>">
                至 <input id="query-end-date" style="cursor: pointer"
                         class="form-control input-sm date-picker J-datepicker js_date_start" readonly="readonly"
                         value="<#if endDate?exists>${endDate}<#else>${.now?string('yyyy-MM-dd')}</#if>">
                &nbsp;&nbsp;<input type="button" id="query-commit" value="查看"
                                   class="btn btn-success js_submitbtn"/>
            </form>
        </div>

        <div class="result_area">
                <span><p>查询结果</p></span>
                <table class="table">
                    <tr>
                        <td>分类</td>
                        <td>款项</td>
                        <td>结算对象</td>
                        <td>总金额</td>
                        <td>款项明细</td>
                    </tr>
                    <tr>
                        <td rowspan="3">站点收入</td>
                        <td>应收垫付款</td>
                        <td>美团外卖</td>
                        <td>￥<#if poiDailySettlement?? && poiDailySettlement.advance??>${poiDailySettlement.advance?string('0.0')}<#else>0.0</#if></td>
                        <td><a href="javascript:void(0);" name="poiDetail">查看明细>></a></td>
                    </tr>
                    <tr>
                        <td>异常订单收入</td>
                        <td>配送员</td>
                        <td>下一版本开放</td>
                        <td>下一版本开放</td>
                    </tr>
                    <tr>
                        <td>总收入</td>
                        <td></td>
                        <td>￥<#if poiDailySettlement?? && poiDailySettlement.advance??>${poiDailySettlement.advance?string('0.0')}<#else>0.0</#if></td>
                        <td></td>
                    </tr>
                </table>



            <table class="table">
                <tbody>
                <tr>
                    <td>分类</td>
                    <td>款项</td>
                    <td>结算对象</td>
                    <td>总金额</td>
                    <td>款项明细</td>
                </tr>
                <tr>
                    <td rowspan="3">站点支出</td>
                    <td>应付垫付款</td>
                    <td>配送员</td>
                    <td>￥<#if dispatcherDailySettlement?? && dispatcherDailySettlement.advance??>${dispatcherDailySettlement.advance?string('0.0')}<#else>0.0</#if></td>
                    <td><a href="javascript:void(0);" name="dispatcherDetail">查看明细>></a></td>
                </tr>
                <tr>
                    <td>异常订单支出</td>
                    <td>美团配送</td>
                    <td>下一版本开放</td>
                    <td>下一版本开放</td>
                </tr>
                <tr>
                    <td>总支出</td>
                    <td></td>
                    <td>￥<#if dispatcherDailySettlement?? && dispatcherDailySettlement.advance??>${dispatcherDailySettlement.advance?string('0.0')}<#else>0.0</#if></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>

