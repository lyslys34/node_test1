<#include "audit_config.ftl">
<#include "../widgets/sidebar.ftl">
<div id="main-container">

    <style type="text/css">
        .amap-layers img {
            max-width: none;
        }
        #main-container {
            padding-top: 45px;
            margin-left: 98px;
        }
        #mapContainer{
            position: absolute;
            top:0;
            left: 0;
            right:0;
            bottom:0;
        }
        #tip{
            height:45px;
            background-color:#fff;
            padding-left:10px;
            padding-right:10px;
            border:1px solid #969696;
            position:absolute;
            font-size:12px;
            right:10px;
            bottom:30px;
            border-radius:3px;
            line-height:45px;
        }

        #tip input[type='button']{
            height:28px;
            line-height:28px;
            outline:none;
            text-align:center;
            padding-left:5px;
            padding-right:5px;
            color:#FFF;
            background-color:#0D9BF2;
            border:0;
            border-radius: 3px;
            margin-top:8px;
            margin-left:5px;
            cursor:pointer;
            margin-right:10px;
        }
    </style>
    <div id="main-container" >
        <div id="mapContainer"></div>
        <div id="tip">
            <input type="button" value="开始动画" onclick="startAnimation()"/>
            <input type="button" value="停止动画" onclick="stopAnimation()"/>
            <input type="hidden"  id="grabLng" value="${tracklist.grabLng!''}">
            <input type="hidden"  id="grabLat" value="${tracklist.grabLat!''}">
            <input type="hidden"  id="pickLng" value="${tracklist.pickLng!''}">
            <input type="hidden"  id="pickLat" value="${tracklist.pickLat!''}">
            <input type="hidden"  id="arriveLng" value="${tracklist.arriveLng!''}">
            <input type="hidden"  id="arriveLat" value="${tracklist.arriveLat!''}">
        </div>
    </div>

    <script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=b2efba1426e8e7815816b9b612d59e2e"></script>
    <script type="text/javascript">
        var grabLng = document.getElementById("grabLng").value
        var grabLat = document.getElementById("grabLat").value
        var pickLng = document.getElementById("pickLng").value
        var pickLat = document.getElementById("pickLat").value
        var arriveLng = document.getElementById("arriveLng").value
        var arriveLat = document.getElementById("arriveLat").value
        //初始化地图对象，加载地图
        var map = new AMap.Map("mapContainer",{
            resizeEnable: true,
            //二维地图显示视口
            view: new AMap.View2D({
                //地图中心点
                center:new AMap.LngLat(grabLng,grabLat),
                //地图显示的缩放级别
                zoom:17
            }),
            continuousZoomEnable:false
        });
        AMap.event.addListener(map,"complete",completeEventHandler);

        //地图图块加载完毕后执行函数
        function completeEventHandler(){
            marker = new AMap.Marker({
                map:map,
                //draggable:true, //是否可拖动
                position:new AMap.LngLat(grabLng,grabLat),//基点位置
                icon:"http://code.mapabc.com/images/car_03.png", //marker图标，直接传递地址url
                offset:new AMap.Pixel(-26,-13), //相对于基点的位置
                autoRotation:true
            });
            lineArr = new Array();
            lineArr.push(new AMap.LngLat(grabLng,grabLat));
            lineArr.push(new AMap.LngLat(pickLng,pickLat));
            lineArr.push(new AMap.LngLat(arriveLng,arriveLat));

            //绘制轨迹
            var polyline = new AMap.Polyline({
                map:map,
                path:lineArr,
                strokeColor:"#00A",//线颜色
                strokeOpacity:1,//线透明度
                strokeWeight:3,//线宽
                strokeStyle:"solid"//线样式
            });
            map.setFitView();
        }
        function startAnimation() {
            marker.moveAlong(lineArr,500);
        }
        function stopAnimation() {
            marker.stopMove();
        }
    </script>

	