<#include "audit_config.ftl">
<#include "../widgets/sidebar.ftl">
<div id="main-container">
    <div class="panel panel-default table-responsive">

        <div>
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="/settleWaybillAudit/listJiaoma">需人工审核订单列表</a></li>
            <li role="presentation"><a href="/settleWaybillAudit/rule/list?waybillType=1">美团订单审核规则</a></li>
            <li role="presentation" ><a href="/settleWaybillAudit/rule/list?waybillType=2">非美团订单审核规则</a></li>
          </ul>
          <div style="width: 100%;height: 2px;background: #58B7B1;"></div>
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="audit-list">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
            <#-- <th>操作</th>-->
                <th>可疑单ID</th>
                <th>运单编号</th>
                <th>运单金额</th>
                <th>创单时间</th>
                <th>待审核疑点</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list bmsettlewaybillauditlist as audit>
            <tr>
            <#--<td>
                <div class="btn-group hover-dropdown">
                    <button class="btn btn-xs btn-info dropdown-toggle" data-toggle="dropdown">操作<span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a target="_blank" href="/settleWaybillAudit/detail?id=${audit.id!''}">查看详情</a></li>
                        &lt;#&ndash;<li><a target="_blank" href="#">操作日志</a></li>&ndash;&gt;
                    </ul>
                </div>
            </td>-->
                <td><span style="text-decoration:underline;color:blue;"><a target="_blank" href="/settleWaybillAudit/detailJiaoma?id=${audit.id!''}">${audit.id!''}</a></span></td>
                <td>${audit.bm_waybill_id!''}</td>
                <td>${audit.money!''}</td>
                <td>${audit.ctime!''}</td>
                <td>${audit.reason!''}</td>
            </tr>
            </#list>
            </tbody>
        </table>
        </div>
        </div>
        </div>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/settleWaybillAudit/listJiaoma"/>
    </#if>
    </div>
</div>
<script>document.title = '可疑运单管理';</script>
