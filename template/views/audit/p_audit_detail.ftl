<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="pageName">waybill_detail</content>
<content tag="cssmain">/task_manage/dispatch.css</content>



<div id="main-container">

    <div id="nav">异常订单审核 > 订单详情</div><br />
    <div id="content">
        加载中
    </div>

    <div>
        <div class="panel panel-default" style="width: 100%; float: left;">
            <div class="panel-heading">
            <span class="panel-title">
                操作记录
            </span>
            </div>
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>操作</th>
                <th>修改内容</th>
                <th>操作人</th>
                <th>操作人ID</th>
                <th>操作时间</th>

            </tr>
            </thead>
            <tbody>
            <#list SettleWaybillAuditLogViewList as list>
            <tr>
               <td><#if 10 == list.status>审核
                <#elseif 20 == list.status>审核
                <#elseif 30 == list.status>审核
                <#elseif 1 == list.status>修改实收
                <#elseif 2 == list.status>修改实付
                <#else>
                </#if></td>
                <td>${list.comment!''}</td>
                <td>${list.auditor_name!''}</td>
                <td>${list.auditor_id!''}</td>
                <td>${list.audit_time!''}</td>
            </tr>
            </#list>
            </tbody>
        </table>
        </div>
    </div>

</div>
<script>
    $("#content").load("/partner/waybill/detail.ajax?audit=1&waybillId="+${waybillId});
</script>