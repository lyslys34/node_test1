<#include "audit_config.ftl">
<#include "../widgets/sidebar.ftl">

<link rel="stylesheet" href="${staticPath}/css/page/delivery.css" xmlns="http://www.w3.org/1999/html"/>
<style>
    input[type="radio"],
    input[type="checkbox"] {
        opacity:1;
        position: static;
        width: 13px;
        height: 13px;
    }
</style>
<div id="wrapper">
    <div id="main-container">
        <div class="padding-md">
            <div class="panel panel-default package-detail">
                <div class="panel-heading">
                    <span>基本信息</span>
                    <#--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                        <#--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                        <#--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                        <#--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                        <#--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                        <#--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                        <#--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                        <#--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                        <#--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                        <#--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                    <#--小票信息-->
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="col-md-5">
                            <table class="table table-striped">
                                <tr><th>运单ID：${waybillAuditView.bmWaybillView.id!''}</th></tr>
                                <tr><td>状态：<#if 0 == waybillAuditView.bmWaybillView.status>生成运单
                                <#elseif 20 == waybillAuditView.bmWaybillView.status>已接单
                                <#elseif 30 == waybillAuditView.bmWaybillView.status>已取餐
                                <#elseif 50 == waybillAuditView.bmWaybillView.status>已送达
                                <#elseif 99 == waybillAuditView.bmWaybillView.status>已取消
                                <#else>
                                </#if></td></tr>
                                <tr><td>下单时间：${waybillAuditView.createTime!''}</td></tr>
                                <tr><td>列为可疑运单时间：${waybillAuditView.auditCtime!''}</td></tr>
                                <tr><td>订单价格：${waybillAuditView.bmWaybillView.pkgValue!''}</td></tr>
                                <tr><td>支付方式：<#if waybillAuditView.bmWaybillView.payed>在线支付 
                                <#elseif false == waybillAuditView.bmWaybillView.payed>货到付款 
                                <#else> </#if></td></tr>
                                <tr><td>发货方：${waybillAuditView.bmWaybillView.senderName!''}</td></tr>
                                <tr><td>发货地址：${waybillAuditView.bmWaybillView.senderAddress!''}</td></tr>
                                <tr><td>发货方联系方式：${waybillAuditView.bmWaybillView.senderPhone!''}</td></tr>
                                <tr><td>收货人：${waybillAuditView.bmWaybillView.recipientName!''}</td></tr>
                                <tr><td>收货地址：${waybillAuditView.bmWaybillView.recipientAddress!''}</td></tr>
                                <tr><td>收货人联系方式：${waybillAuditView.bmWaybillView.recipientPhone!''}</td></tr>
                            </table>
                        </div>
                        <div class="col-md-7">
                            <#--<img src="${waybillAuditView.bmWaybillView.invoiceImgUrl!''}" height="410px">-->
                        </div>
                    </div>

                </div>
            </div>

            <div class="panel panel-default package-detail">
                <div class="panel-heading">
                    <span>配送信息</span>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <tr>
                                <td>配送员：${waybillAuditView.bmWaybillView.riderName!''}</td>
                                <td>
                                    <#--抢单时间：${waybillAuditView.grabTime!''}-->
                                </td>
                            </tr>
                            <tr>
                                <td>联系电话：${waybillAuditView.bmWaybillView.riderPhone!''}</td>
                                <td>取货时间：${waybillAuditView.pickTime!''}</td>
                            </tr>
                            <tr>
                                <td>配送时长：${waybillAuditView.useTime}分</td>
                                <td>送达时间：${waybillAuditView.arriveTime!''}</td>
                            </tr>
                            <tr>
                                <td>位移信息：${waybillAuditView.distance!''}</td>
                                <td>运动轨迹：<a target="_blank" href="/settleWaybillAudit/track?grabLng=${waybillAuditView.grabLng!''}&grabLat=${waybillAuditView.grabLat!''}&pickLng=${waybillAuditView.pickLng!''}&pickLat=${waybillAuditView.pickLat!''}&arriveLng=${waybillAuditView.arriveLng!''}&arriveLat=${waybillAuditView.arriveLat!''}">点击查看</a></td>

                            </tr>
                        </table>
                    </div>

                </div>
            </div>

            <div class="panel panel-default package-detail">
                <div class="panel-heading">
                    <span>待审核疑点</span>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                    <#list reasonlist as str>
                        <tr>
                            <td>●&nbsp;&nbsp;<td>
                            <td>${str!''}</td>
                            </br>
                        </tr>
                    </#list>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="panel-footer text-center">
    <a href="#auditDisagreeModal" role="button" data-toggle="modal" class="btn btn-danger">审核不通过</a>
    <a href="#auditAgreeModal" role="button" data-toggle="modal" class="btn btn-success">审核通过</a>
</div>
<form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post" action="/settleWaybillAudit/auditJiaoma?status=30&id=${waybillAuditView.bmSettleWaybillAuditView.id!}">
    <div class="modal fade" id="auditDisagreeModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>将要不通过审核，确认?</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>请填写审核不通过原因</label><div id="alert_error"></div>
                        <input type="checkbox" name="boxes" value="接单后未按时取货或者未准时完成配送">接单后未按时取货或者未准时完成配送<br>
                        <input type="checkbox" name="boxes" value="未按平台规定点击取货/送达按钮，如提前点击送达">未按平台规定点击取货/送达按钮，如提前点击送达<br>
                        <input type="checkbox" name="boxes" value="恶意接单，接单后不上门取货直接点击完成">恶意接单，接单后不上门取货直接点击完成<br>
                        <input type="checkbox" name="boxes" value="因服务态度、不良行为等导致客户投诉">因服务态度、不良行为等导致客户投诉<br>
                        <input type="checkbox" name="boxes" value="只接同一个商户的订单">只接同一个商户的订单<br>
                        <input type="checkbox" name="boxes" value="接虚假订单骗取补贴">接虚假订单骗取补贴<br>
                        <input type="checkbox" name="others" value="其它">其它
                        <textarea rows="6" class="form-control input-sm" id="audit_msg" name="reason" placeholder="审核不通过原因"></textarea>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" value="确认不通过" class="btn btn-danger btn-sm" />
                        <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">再想想</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post" action="/settleWaybillAudit/auditJiaoma?status=20&id=${waybillAuditView.bmSettleWaybillAuditView.id!}">
    <div class="modal fade" id="auditAgreeModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>将要通过审核，确认?</h4>
                    <div class="modal-footer">
                        <input type="submit" value="确认通过" class="btn btn-success btn-sm" />
                        <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true">再想想</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>



<script>document.title = '可疑运单明细';</script>
