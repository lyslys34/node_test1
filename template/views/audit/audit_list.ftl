<#include "audit_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/settle/dateSimple.js</content>
<content tag="cssmain">/statistics/daily_data.css</content>
<style type="text/css">
    #main-container {
        padding: 10px 20px;
    }
    .a{
        color:#428bca;
        cursor: pointer;
    }
    hr{
        margin: 0;
        padding: 0;
    }
    input[type="radio"], input[type="checkbox"] {
        opacity: 1;
    }
    .js_date_start, .js_date_end{
        width:90%;
        cursor: pointer;
    }
    .block{
        display: block;
    }
    #beginTimeStr,#endTimeStr{
        cursor: pointer;
    }
</style>
<div id="main-container">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#audit-list" aria-controls="audit-list" role="tab"
                                                  data-toggle="tab">需人工审核订单列表</a></li>
        <li role="presentation"><a href="/settleWaybillAudit/rule/list?waybillType=0">系统审核规则</a></li>
    </ul>
    <div style="width: 100%;height: 2px;background: #58B7B1;"></div>

    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/settleWaybillAudit/list" method="post" >
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">订单号</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="platformOrderId" id="platformOrderId" data-required="true" data-minlength="8" value="${queryParam.platformOrderId!""}"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">配送员姓名</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="riderName" id="riderName" data-required="true" data-minlength="8" value="${queryParam.riderName!""}"/>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">订单类型</label>
                            <select class="form-control input-sm" id="pkgType" name="pkgType">
                                <option value="-1" <#if queryParam.pkgType == -1> selected="selected" </#if>>全部</option>
                                <option value="1" <#if queryParam.pkgType == 1 > selected="selected" </#if>>普通</option>
                                <option value="2" <#if queryParam.pkgType == 2 > selected="selected" </#if>>代购</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">城市</label>
                            <select class="form-control input-sm" id="cityId" name="cityId">
                                <option value="-1" <#if queryParam.cityId == -1> selected="selected" </#if>>全部</option>
                                <#list cityViews as city>
                                    <option value="${city.city_id !''}" <#if city.city_id == queryParam.cityId > selected="selected" </#if> >${city.name !''}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label block">开始时间</label>
                            <input id="beginTimeStr" type="text" name="beginTimeStr"  readonly="readonly" value="<#if queryParam.beginTimeStr?exists>${queryParam.beginTimeStr}</#if>" class="form-control J-datepicker js_date_start">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label block">结束时间</label>
                            <input type="text" id="endTimeStr" name="endTimeStr" readonly="readonly"  value="<#if queryParam.endTimeStr?exists>${queryParam.endTimeStr}</#if>" class="form-control J-datepicker js_date_end">
                        </div>
                    </div>

                    <div class="col-md-3" style="padding-top:18px">
                        <input type="button" class="btn btn-success js_submitbtn" actionurl="/settleWaybillAudit/list" value="查询"/>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <div>

            <!-- Tab panes -->
            <div class="tab-content" style="margin-top: 10px">
                <div role="tabpanel" class="tab-pane active" id="audit-list">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>订单号</th>
                            <th>送达时间</th>
                            <th>配送员</th>
                            <th>配送员角色</th>
                            <th>订单金额(元)</th>
                            <th>订单类型</th>
                            <th>城市</th>
                            <th>审核状态</th>
                            <th>拍摄凭证</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <#--循环-->
                        <#if vos??>
                            <#list vos as audit>
                            <tr>
                                <td><span class="a waybillId" value="${audit.waybillId}">${audit.orderId!''}</span></td>
                                <td>${audit.arriveTime!''}</td>
                                <td>${audit.riderName!''}</td>
                                <td><#if audit.riderRole==2>兼职<#elseif audit.riderRole==1>全职</#if></td>
                                <td>￥${audit.pkgValue}</td>
                                <td><#if audit.pkgType==2>代购<#elseif audit.pkgType==1>普通<#else></#if></td>
                                <td>${audit.cityName!''}</td>
                                <td>${audit.auditStatusComment!''}</td>
                                <td>
                                    <#if audit.invoiceImgUrl??>
                                        <a data-toggle="tooltip" title="查看照片" data-placement="top" href="${audit.invoiceImgUrl!''}" target="_blank" class="a">
                                            查看
                                        </a>
                                    <#else>-
                                    </#if>
                                </td>
                                <td>
                                    <input type="button" class="btn btn-success btn-sm" value="处理"
                                           data-id="${audit.waybillId}"/>
                                </td>
                            </tr>
                            </#list>
                        </#if>
                        </tbody>
                    </table>
                <#import "../page/pager.ftl" as q>
                <#if recordCount??>
                    <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/settleWaybillAudit/list"/>
                </#if>
                </div>
                <div role="tabpanel" class="tab-pane" id="audit-rule">

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="waybillAuditForm" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 style="margin: 0" class="modal-title">审核处理 <small id="auditStatusComment">{auditStatusComment}</small></h4>
            </div>
            <div style="background-color: #f7f7f9; padding: 10px">
                <div>
                    <div class="col-md-4" style="margin-bottom: 5px;padding-left: 5px;font-size:13px"><b>存疑点</b></div>
                    <div class="col-md-offset-10"><a id="movementUrl" class="a" target="_blank">运动轨迹</a></div>
                </div>
                <div>
                    <ul id="reasons" style="margin: 8px 5px 0 5px">
                    </ul>
                </div>
            </div>
            <div class="modal-body" style="margin: 0;padding:0 15px;border-top: 1px solid #e5e5e5;">
                <form>
                    <input type="hidden" id="waybillId" name="waybillId" value="">
                    <div class="form-group"  id="isCheatDiv">
                        <h5>此单是否为正常配送订单？</h5>
                        <div class="radio">
                            <div>
                                <label>
                                    <input type="radio" name="isCheat" value="0" checked="true"/>是
                                </label>
                            </div>
                            <div>
                                <label>
                                    <input type="radio" name="isCheat" value="1" />否（判定为作弊单，不支付此单任何费用）
                                </label>
                            </div>
                            <div style="margin-left: 20px;width: 95%;">
                                <textarea class="form-control" rows="2" id="cheatReason" name="cheatReason" placeholder="选填，请填写判定为作弊原因"></textarea>
                            </div>
                        </div>
                    </div>
                <hr>
                    <fieldset>
                    <div class="form-group" id="abnormalDiv">
                        <input type="hidden" id="isAbnormal"  value="-1">
                        <h5 style="padding-top:0">此单是否获得运费＋平台奖励？</h5>
                        <div class="radio">
                            <p id="distance" style="color: #3C8DBC;">{distance}</p>
                            <div>
                                <label>
                                    <input type="radio" name="isAbnormal" value="0"/>是
                                </label>
                            </div>
                            <div>
                                <label>
                                    <input type="radio" name="isAbnormal" value="1" />否
                                </label>
                            </div>
                            <div style="margin-left: 20px;width: 95%;" class="span2Textarea">
                                <textarea class="form-control" rows="2" id="abnormalReason" name="abnormalReason" placeholder="必填，请填写判定违规驳回原因，点击下方文字快速选择原因"></textarea>
                                <div style="padding-top: 2px">
                                    <span class="bg-warning" style="margin: 3px;cursor: pointer;">未在取货地点点击“我已取货”按钮</span>
                                    <span class="bg-warning" style="margin: 3px;cursor: pointer;">未在送货地点点击“我已送达”按钮</span>
                                    <span class="bg-warning" style="margin: 3px;cursor: pointer;">配送时间超时</span><br />
                                </div>
                                <div style="padding-top: 2px">
                                    <span class="bg-warning" style="margin:0 3px;cursor: pointer;">预定单超时或提前20分钟送达</span>
                                    <span class="bg-warning" style="margin:0 3px;cursor: pointer;">即时订单取货时间超过15分钟</span>
                                    <span class="bg-warning" style="margin:0 3px;cursor: pointer;">商家/用户投诉</span>
                                </div>
                            </div>
                        </div>
                    </div>
                <hr>
                    <div class="form-group" id="prepayDiv">
                        <input type="hidden" id="isPrepayAbnormal" value="-1">
                        <h5>此单是否通过实收实付审核？ (<a data-toggle="tooltip" id="invoiceImgUrl" href="{invoiceImgUrl}" target="_blank" class="a">拍照凭证</a>)</h5>
                        <div class="radio">
                            <p id="waybillAmount" style="color: #3C8DBC;">{waybillAmount}
                            </p>
                            <div>
                                <label>
                                    <input type="radio" name="isPrepayAbnormal"value="0" />是
                                </label>
                            </div>
                            <div>
                                <label>
                                    <input type="radio" name="isPrepayAbnormal"value="1" />否
                                </label>
                            </div>
                            <textarea class="form-control" style="margin-left: 20px;width: 95%" rows="2" id="prepayAbnormalReason" name="prepayAbnormalReason" placeholder="选填，请填写驳回原因"></textarea>
                        </div>
                    </div>
                    </fieldset>
                </form>
            </div>

            <div class="modal-footer" style="text-align: center;">
                <button type="button" class="btn btn-success" id="btn_submit">提交结果</button>
                <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">取消</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="waybillDetailForm" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: medium none;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: -8px; margin-top: -10px;"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="detailModalBody" style="padding:0 15px;"></div>
        </div>
    </div>
</div>

    <script>document.title = '可疑运单管理';
    $('select').select2();

    $('.table').delegate("input[type=button]", "click", function () {
        var id = $(this).attr('data-id');
        showModal(id);
    });
    $('.table').delegate(".waybillId", "click", function () {
        var id = $(this).attr('value');
        showWaybillDetail(id);
    });
    $('.span2Textarea').delegate("span", "click", function () {
        $('.span2Textarea textarea').val($(this).html());
    });
    $("input[name='isCheat']").change(
        function () {
            var isCheat = $("input[name=isCheat]:checked").val();
            $('fieldset').attr('disabled', isCheat == 1);
    });
    $('#btn_submit').click(function(){
        var $isCheatRadio = $("input[name=isCheat]:checked");
        if ($isCheatRadio.length == 0 || $isCheatRadio.val() == 0) {
            if($('#isAbnormal').val()!=-1){
                var $isAbnormalRadio = $("input[name=isAbnormal]:checked");
                if($isAbnormalRadio.length == 0) {
                    alert('请审核此单是否为正常配送订单！');
                    return;
                }
                if($isAbnormalRadio.val()==1){
                    var $abnormalReason = $('#abnormalReason');
                    if($abnormalReason.val().length == 0){
                        $abnormalReason.parent().addClass('has-error');
                        alert('请填写判定为非正常配送订单的原因！');
                        return;
                    }
                    if($abnormalReason.val().length > 100){
                        $abnormalReason.parent().addClass('has-error');
                        alert('为非正常配送订单的原因不能超过100个字符！');
                        return;
                    }else{
                        $abnormalReason.parent().removeClass('has-error');
                    }
                }
            }
            if($('#isPrepayAbnormal').val()!=-1){
                var $isPrepayAbnormalRadio = $("input[name=isPrepayAbnormal]:checked");
                if($isPrepayAbnormalRadio.length==0){
                    alert('请审核此单是否通过实收实付！');
                    return;
                }
                if($isPrepayAbnormalRadio.val() == 1) {
                    var $prepayAbnormalReason = $('#prepayAbnormalReason');
                    if($prepayAbnormalReason.val().length > 100){
                        $prepayAbnormalReason.parent().addClass('has-error');
                        alert('实收实付审核不通过原因不能超过100个字符！');
                        return;
                    }else{
                        $prepayAbnormalReason.parent().removeClass('has-error');
                    }
                }
            }
        }else{
            var $cheatReason = $('#cheatReason');
            if($cheatReason.val().length > 100){
                $cheatReason.parent().addClass('has-error');
                alert('非正常配送订单原因不能超过100个字符！');
                return;
            }else{
                $cheatReason.parent().removeClass('has-error');
            }
        }
        $.post("/settleWaybillAudit/audit",
                $('#waybillAuditForm form').serialize(),
                function (result) {
                    if (result.success) {
                        window.location.reload();
                        return;
                    }
                    alert(result.message);
                });
    });

    function showModal(waybillId){
        if(waybillId){
            $.getJSON("/settleWaybillAudit/initWaybillAuditForm?waybillId=" + waybillId, function (result) {
                if(!result.success){
                    alert(result.message);
                    return;
                }
                $('#waybillId').val(result.vo.waybillId);
                $('#auditStatusComment').html('('+result.vo.auditStatusComment+')');
                $('#distance').html(result.vo.distance);
                $('#movementUrl').attr('href',result.vo.movementUrl)[0].style.display = result.vo.movementUrl?'block':'none';;
                var reasons='';
                for(var i=0;i<result.vo.reasons.length;i++){
                    if(result.vo.reasons[i]){
                        reasons+= '<li>'+result.vo.reasons[i]+'</li>';
                    }
                }
                $('#reasons').html(reasons);
                document.getElementById('isCheatDiv').style.display=result.vo.isCheat==-1?'none':'block';
                document.getElementById('abnormalDiv').style.display=result.vo.isAbnormal==-1?'none':'block';
                document.getElementById('prepayDiv').style.display=result.vo.isPrepayAbnormal==-1?'none':'block';
                $('#invoiceImgUrl').attr('href',result.vo.invoiceImgUrl);
                $('#isAbnormal').val(result.vo.isAbnormal);
                $('#isPrepayAbnormal').val(result.vo.isPrepayAbnormal);
                $('#waybillAmount').html('应收 '+result.vo.planChargeAmount+'元/实收 '+result.vo.actualChargeAmount+'元 ; 应付 '+result.vo.planPayAmount+'元/实付 '+result.vo.actualPayAmount+'元');

//                $('input[name=isCheat]').eq(0).attr('checked','checked');
//                $('input[name=isAbnormal]').eq(0).attr('checked','checked');
                $('input[name=isCheat]')[0].checked = true;
                $('input[name=isAbnormal]')[0].checked = true;
                $('input[name=isPrepayAbnormal]')[0].checked = true;
                $('textarea').val('');
                $('fieldset').attr('disabled', false);
                $('#waybillAuditForm').modal('show');
            })
        }
    };
    function showWaybillDetail(waybillId){
        var body = '<div id="detailCon">这是ID＝' + waybillId+ '的订单详情</div>';
        $.ajax({
            url: "/partner/waybill/detailV2.ajax?waybillId="+ waybillId,
            success: function(result){
                body = '<div>';
                body = body + result + '</div>';
                $('#detailModalBody').html(body);
                $('#waybillDetailForm').modal('show');
            },
            error:function(){
                alert('访问出错');
            }});
    };
    </script>
