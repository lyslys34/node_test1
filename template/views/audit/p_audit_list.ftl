<#include "audit_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">/settle/date.js</content>
<content tag="cssmain">/statistics/daily_data.css</content>

<style>
    input[type="radio"],
    input[type="checkbox"] {
        opacity:1;
        position: static;
        width: 13px;
        height: 13px;
    }
    .waybillid{
    color:#428bca;
    cursor: pointer;
}

</style>
<script>

    $(document).ready(function() {

        $('#jumpBtn').click(function(){
            window.location.reload();
        });
    
//        $('[data-placement="top"]').tooltip();
    });
    function export1(acti) {
        var form1 = document.getElementById("formValidate1");
        form1.action = acti;
        form1.submit();
    }
    function doChange2(waybillId) {
        var pay = $('#pay_'+waybillId).val();
        var charge = $('#charge_'+waybillId).val();
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/partner/pWaybillAudit/changeActualChargeAndPay",
            data: {
                id: waybillId,
                pay:pay,
                charge:charge
            },
            success : function(data){
                if(data.success){
                    alert("提交修改成功");
                    window.location.reload();
                }else {
                    alert(data.message);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });
    }
   /* function radioChange(radio) {
        if(radio.value == '3') {
            document.getElementById("start").readOnly = false;
            document.getElementById("end").readOnly = false;
        } else {
            document.getElementById("start").readOnly = true;
            document.getElementById("end").readOnly = true;
        }
    }*/

    function audit(id, status) {
        var msg = $('#audit_msg_'+id).val();
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/partner/pWaybillAudit/audit",
            data: {
                id: id,
                status: status,
                reason: msg
            },
            success : function(data){
                if(data.success){
                    $('#resultContent').html(data.successMsg);
                    $('#resultModal').modal('show');
                }else {
                    alert(data.success);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });
    }

</script>

<div id="main-container">
   <!-- <div id="nav"><br />&nbsp;&nbsp;异常订单审核 </div><br />-->
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="get" action="/partner/pWaybillAudit/list">
            <div class="panel-body">
                <div class="row">
                    <#--<div class="col-md-3">-->
                        <#--<div class="form-group">-->
                            <#--<label class="control-label">异常类型</label>-->
                            <#--<select class="form-control input-sm" id="type" name="type">-->
                                <#--<option value="-1"<#if type == -1> selected="selected" </#if>>全部</option>-->
                                <#--<option value="1"<#if type == 1> selected="selected" </#if>>应付实付不一致</option>-->
                                <#--<option value="2"<#if type == 2> selected="selected" </#if>>应收实收不一致</option>-->
                            <#--</select>-->
                        <#--</div>-->
                    <#--</div>-->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">审核状态</label>
                            <select class="form-control input-sm" id="status" name="status">
                                <option value="-1"<#if status == -1> selected="selected" </#if>>全部</option>
                                <option value="10"<#if status == 10> selected="selected" </#if>>待审核</option>
                                <option value="15"<#if status == 15> selected="selected" </#if>>待总部审核</option>
                                <option value="20"<#if status == 20> selected="selected" </#if>>已通过</option>
                                <option value="30"<#if status == 30> selected="selected" </#if>>已驳回</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div style="padding-left:15px;">
                        <div class="form-group form-inline">
                            日期：
                            <input type="radio" class="dateType" name="timeFlag" value="0" <#if 0 == timeFlag>checked="true" </#if>/> 今日
                            &nbsp;&nbsp;|&nbsp;&nbsp;<input type="radio" class="dateType" name="timeFlag" value="1" <#if 1 == timeFlag>checked="true" </#if>/> 昨日
                            &nbsp;&nbsp;|&nbsp;&nbsp;<input type="radio" class="dateType" name="timeFlag" value="2" <#if 2 == timeFlag>checked="true" </#if>/> 近三天
                            &nbsp;&nbsp;|&nbsp;&nbsp;<input type="radio" class="dateType" name="timeFlag" value="3" <#if 3 == timeFlag>checked="true" </#if>/> 自定义时间
                            <input id="start" type="text" style="cursor: pointer;margin-left: 5px;" name="beginTimeStr" readonly="readonly" value="<#if beginTimeStr?exists>${beginTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start"／>
                            至<input type="text" id="end" name="endTimeStr" style="cursor: pointer;" readonly="readonly"  value="<#if endTimeStr?exists>${endTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end"／>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-left">
                <button type="button" class="btn btn-success js_submitbtn" actionurl="/partner/pWaybillAudit/list">查询</button>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>序号</th>
                <th>订单号</th>
                <th>订单完成时间</th>
                <th>配送员姓名</th>
                <th>商家名称</th>
                <th>订单金额(元)</th>
                <th>应收用户(元)</th>
                <th>实收用户(元)</th>
                <th>应付商家(元)</th>
                <th>实付商家(元)</th>
                <#--<th>异常类型</th>-->
                <th>拍摄凭证</th>
                <th>审核状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list bmsettlewaybillauditlist as audit>
                <tr>
                    <td>${audit.bmWaybillAuditView.id}</td>
                    <td><span  class="waybillid" value="${audit.bmWaybillAuditView.waybillId!''}">${audit.platformOrderId!''}</span></td>
                    <td>${audit.timeStr!''}</td>
                    <td>${audit.riderName!''}</td>
                    <td>${audit.poiName!''}</td>
                    <td>${audit.pkgValue!''}</td>
                    <td>${audit.planChargeAmount!''}</td>
                    <td <#if audit.actualChargeAmount != audit.planChargeAmount>style="color:red;"</#if>>${audit.actualChargeAmount!''}</td>
                    <td>${audit.planPayAmount!''}</td>
                    <td <#if audit.actualPayAmount != audit.planPayAmount>style="color:red;"</#if>>${audit.actualPayAmount!''}</td>
                    <#--<td width="110px">${audit.bmWaybillAuditView.reason!''}</td>-->
                    <td>
                        <#if audit.invoiceImgUrl?exists>
                            <#if audit.invoiceImgUrl != ''>
                                <a data-toggle="tooltip" title="查看照片" data-placement="top" href="${audit.invoiceImgUrl}" target="_blank">
                                    <i class="fa fa-info-circle fa-lg opration-icon"></i>
                                </a>
                            </#if>
                            <#if audit.invoiceImgUrl == ''>无</#if>
                        <#else>
                            无
                        </#if>

                    </td>

                    <td><#if audit.bmWaybillAuditView.status == 10>待审核<#elseif audit.bmWaybillAuditView.status==15>待总部审核</#if><#if audit.bmWaybillAuditView.status == 20>已通过</#if><#if audit.bmWaybillAuditView.status == 30>已驳回</#if></td>
                    <td>
                        <#if audit.bmWaybillAuditView.status != 20 && audit.bmWaybillAuditView.status != 15>
                            <a title="修改实收实付" data-placement="top" href="#updateChargePay${audit.bmWaybillAuditView.id}" role="button" data-toggle="modal">
                                <i class="fa fa-edit fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                            <#--<a title="修改" data-placement="top" target="_blank" href="/partner/pWaybillAudit/detail?id=${audit.bmWaybillAuditView.id!''}">
                                <i class="fa fa-edit fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;-->
                            <#if audit.bmWaybillAuditView.status == 10>
                                <a title="通过" data-placement="top" href="#auditAgreeModal${audit.bmWaybillAuditView.id}" role="button" data-toggle="modal">
                                    <i class="fa fa-thumbs-o-up fa-lg opration-icon"></i>
                                </a>&nbsp;&nbsp;
                                <a title="拒绝" data-placement="top" href="#auditDisagreeModal${audit.bmWaybillAuditView.id}" role="button" data-toggle="modal">
                                    <i class="fa fa-thumbs-o-down fa-lg opration-icon"></i>
                                </a>
                            </#if>
                        </#if>
                    </td>
                </tr>

                <div class="modal fade" id="auditDisagreeModal${audit.bmWaybillAuditView.id}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>审核驳回</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>驳回原因：（驳回后，该订单全部垫付款将不支付骑手）</label><div id="alert_error"></div>
                                    <textarea rows="6" class="form-control input-sm" id="audit_msg_${audit.bmWaybillAuditView.id}" name="reason_${audit.bmWaybillAuditView.id}" placeholder="请填写驳回原因"></textarea>
                                </div>
                                <div class="modal-footer">
                                    <input type="button" onclick="audit(${audit.bmWaybillAuditView.id!}, 30)" value="确认驳回" class="btn btn-danger btn-sm" />
                                    <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">取消</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="auditAgreeModal${audit.bmWaybillAuditView.id}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>审核通过</h4>
                                <hr>
                                <label>审核通过后，将于次日以 ”实付商家 ${audit.actualPayAmount!''}元，实收用户 ${audit.actualChargeAmount!''}元“ 与骑手结算该订单；即支付骑手 <span style="color: #ff0000">${audit.actualPayAmount-audit.actualChargeAmount}</span> 元。
                                    <#if audit.actualPayAmount-audit.actualChargeAmount < 0><span style="color: #ff0000">注意：本单实收大于实付，将向骑手扣款。</span></#if>
                                </label>

                                <div class="modal-footer">
                                    <input type="button" onclick="audit(${audit.bmWaybillAuditView.id!}, 20)" value="确认通过" class="btn btn-danger btn-sm" />
                                    <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">取消</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="updateChargePay${audit.bmWaybillAuditView.id}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>修改实收实付</h4>
                                <hr>
                                <input type="hidden" id="waybillId" value="${audit.bmWaybillAuditView.id}">
                                <div>应付商家（元）<a>${audit.planPayAmount!''}</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    应收用户（元）<a>${audit.planChargeAmount!''}</a>
                                </br>
                                <div>实付商家（元）<input id="pay_${audit.bmWaybillAuditView.waybillId!''}" type ="text" name="pay_${audit.bmWaybillAuditView.waybillId!''}"  value="${audit.actualPayAmount!''}">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    实收用户（元）<input id="charge_${audit.bmWaybillAuditView.waybillId!''}" type ="text" name="charge_${audit.bmWaybillAuditView.waybillId!''}" value="${audit.actualChargeAmount!''}">
                                </div>

                                <br>
                                <div class="modal-footer">
                                    <input type="button" id="doChange2" class="btn btn-danger btn-sm" style="button" value="提交审核" onclick="doChange2(${audit.bmWaybillAuditView.waybillId!''})">
                                    <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">取消</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </#list>
            </tbody>
        </table>
        <div class="modal fade" id="resultModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <h4>操作结果</h4>
                        <hr>
                        <div class="form-group">
                            <label id="resultContent">请填写审核不通过原因</label>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" id="jumpBtn">知道了</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/pWaybillAudit/list"/>
    </#if>
    </div>
</div>
<script>document.title = '可疑运单管理';</script>
