<#include "audit_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/audit/audit_agent.js</content>
<content tag="cssmain">/statistics/daily_data.css</content>

<script type="text/javascript" src="${staticPath}/js/page/formatCheck/numberCheck.js"></script>

<style>
    input[type="radio"],
    input[type="checkbox"] {
        opacity:1;
        position: static;
        width: 13px;
        height: 13px;
    }
    .waybillid{
    color:#428bca;
    cursor: pointer;
}

</style>
<script>

    $(document).ready(function() {
    
        $('[data-placement="top"]').tooltip();

    });

    function audit(id, status) {
        var msg = $('#audit_msg_'+id).val();
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/settleWaybillAuditAgent/auditAgent",
            data: {
                id: id,
                status: status,
                reason: msg
            },
            success : function(data){
                if(data.success){
                    //alert("审核成功");
                    window.location.reload();
                }else {
                    alert(data.message);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });
    }

    function changePrice(id) {
        var changeType = 0;
        //未修改前的值
        var oldPay = $('#oldPay_' + id).val()
        var oldCharge = $('#oldCharge_' + id).val();
        //编辑后的值
        var pay = $('#pay_' + id).val();
        var charge = $('#charge_' + id).val();
        //比较确定修改的是哪个值
        if(oldPay != pay) {
            changeType = changeType | 1;
            if(!isMoneyFormat(pay)){
                $('#tip_' + id).html('请输入合法的最多两位小数的实付金额！');
                $('#tip_' + id).css('display', 'inline-block');
                return false;
            } else {
                var payFloat = parseFloat(pay);
                if(payFloat >= 100000) {
                    $('#tip_' + id).html('实付金额需要小于100000！');
                    $('#tip_' + id).css('display', 'inline-block');
                    return false;
                }
            }
        }
        if (oldCharge != charge) {
            changeType = changeType | 2;
            if(!isMoneyFormat(charge)){
                $('#tip_' + id).html('请输入合法的最多两位小数的实收金额！');
                $('#tip_' + id).css('display', 'inline-block');
                return false;
            } else {
                var payFloat = parseFloat(charge);
                if(payFloat >= 100000) {
                    $('#tip_' + id).html('实收金额需要小于100000！');
                    $('#tip_' + id).css('display', 'inline-block');
                    return false;
                }
            }
        }
        //没有任何修改
        if(changeType == 0) {
            $('#tip_' + id).html('请输入修改后的价格！');
            $('#tip_' + id).css('display', 'inline-block');
            return false;
        }
        $('#tip_' + id).css('display', 'none');
        $('#doChange2').attr('disabled', 'disabled');
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/settleWaybillAuditAgent/changeActualChargeAndPay",
            data: {
                id: id,
                pay:pay,
                charge:charge,
                changeType:changeType
            },
            success : function(data){
                if(data.success){
                    alert("提交修改成功");
                    window.location.reload();
                }else {
                    alert(data.message);
                    $('#doChange2').removeAttr('disabled');
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
                $('#doChange2').removeAttr('disabled');
            }
        });
    }

</script>

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="get" action="/settleWaybillAuditAgent/listAgent">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">审核状态</label>
                            <select class="form-control input-sm" id="status" name="status">
                                <option value="-1"<#if status == -1> selected="selected" </#if>>全部</option>
                                <option value="10"<#if status == 10> selected="selected" </#if>>待审核</option>
                                <option value="20"<#if status == 20> selected="selected" </#if>>已通过</option>
                                <option value="30"<#if status == 30> selected="selected" </#if>>已驳回</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div style="padding-left:15px;">
                        <div class="form-group form-inline">
                            日期：
                            <input type="radio" class="dateType" name="timeFlag" value="0" <#if 0 == timeFlag>checked="true" </#if>/> 今日
                            &nbsp;&nbsp;|&nbsp;&nbsp;<input type="radio" class="dateType" name="timeFlag" value="1" <#if 1 == timeFlag>checked="true" </#if>/> 昨日
                            &nbsp;&nbsp;|&nbsp;&nbsp;<input type="radio" class="dateType" name="timeFlag" value="2" <#if 2 == timeFlag>checked="true" </#if>/> 近三天
                            &nbsp;&nbsp;|&nbsp;&nbsp;<input type="radio" class="dateType" name="timeFlag" value="3" <#if 3 == timeFlag>checked="true" </#if>/> 自定义时间
                            <input id="start" type="text" style="cursor: pointer;margin-left: 5px;" name="beginTimeStr" readonly="readonly" value="<#if beginTimeStr?exists>${beginTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start"／>
                            至<input type="text" id="end" name="endTimeStr" style="cursor: pointer;" readonly="readonly"  value="<#if endTimeStr?exists>${endTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end"／>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-left">
                <button type="button" class="btn btn-success js_submitbtn" actionurl="/settleWaybillAuditAgent/listAgent">查询</button>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>序号</th>
                <th>订单号</th>
                <th>订单完成时间</th>
                <th>配送员姓名</th>
                <th>商家名称</th>
                <th>订单金额(元)</th>
                <th>应收用户(元)</th>
                <th>实收用户(元)</th>
                <th>应付商家(元)</th>
                <th>实付商家(元)</th>
                <th>拍摄凭证</th>
                <th>审核状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list bmsettlewaybillauditlist as audit>
                <tr>
                    <td>${audit.bmWaybillAuditView.id}</td>
                    <td><span  class="waybillid" value="${audit.bmWaybillAuditView.waybillId!''}">${audit.platformOrderId!''}</span></td>
                    <td>${audit.timeStr!''}</td>
                    <td>${audit.riderName!''}</td>
                    <td>${audit.poiName!''}</td>
                    <td>${audit.pkgValue!''}</td>
                    <td>${audit.planChargeAmount!''}</td>
                    <td <#if audit.actualChargeAmount != audit.planChargeAmount>style="color:red;"</#if>>${audit.actualChargeAmount!''}</td>
                    <td>${audit.planPayAmount!''}</td>
                    <td <#if audit.actualPayAmount != audit.planPayAmount>style="color:red;"</#if>>${audit.actualPayAmount!''}</td>
                    <td>
                        <#if audit.invoiceImgUrl?exists>
                            <#if audit.invoiceImgUrl != ''>
                            <a data-toggle="tooltip" title="查看照片" data-placement="top" href="${audit.invoiceImgUrl}" target="_blank">
                                <i class="fa fa-info-circle fa-lg opration-icon"></i>
                            </a>
                            </#if>
                            <#if audit.invoiceImgUrl == ''>无</#if>
                        <#else>
                            无
                        </#if>

                    </td>
                    <td><#if audit.bmWaybillAuditView.status == 10>待审核</#if><#if audit.bmWaybillAuditView.status == 20>已通过</#if><#if audit.bmWaybillAuditView.status == 30>已驳回</#if></td>
                    <td>
                        <#if audit.bmWaybillAuditView.status != 20>
                            <a title="修改实收实付" data-placement="top" href="#updateChargePay${audit.bmWaybillAuditView.id}" role="button" data-toggle="modal">
                                <i class="fa fa-edit fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                            <#if audit.bmWaybillAuditView.status == 10>
                                <a title="通过" data-placement="top" href="#auditAgreeModal${audit.bmWaybillAuditView.id}" role="button" data-toggle="modal">
                                    <i class="fa fa-thumbs-o-up fa-lg opration-icon"></i>
                                </a>&nbsp;&nbsp;
                                <a title="拒绝" data-placement="top" href="#auditDisagreeModal${audit.bmWaybillAuditView.id}" role="button" data-toggle="modal">
                                    <i class="fa fa-thumbs-o-down fa-lg opration-icon"></i>
                                </a>
                            </#if>
                        </#if>
                    </td>
                </tr>

                <!-- 不通过的对话框 -->
                <div class="modal fade" id="auditDisagreeModal${audit.bmWaybillAuditView.id}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>将要不通过审核，确认?</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>请填写审核不通过原因</label><div id="alert_error"></div>
                                    <textarea rows="6" class="form-control input-sm" id="audit_msg_${audit.bmWaybillAuditView.id}" name="reason_${audit.bmWaybillAuditView.id}" placeholder="审核不通过原因"></textarea>
                                </div>
                                <div class="modal-footer">
                                    <input type="button" onclick="audit(${audit.bmWaybillAuditView.id!}, 30)" value="确认不通过" class="btn btn-danger btn-sm" />
                                    <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">再想想</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 不通过的对话框END -->

                <!-- 通过审核的对话框 -->
                <div class="modal fade" id="auditAgreeModal${audit.bmWaybillAuditView.id}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>将要通过审核，确认?</h4>
                                <div class="modal-footer">
                                    <input type="button" onclick="audit(${audit.bmWaybillAuditView.id!}, 20)" value="确认通过" class="btn btn-success btn-sm" />
                                    <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true">再想想</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 通过审核的对话框END -->

                <!-- 修改实收实付的对话框 -->
                <div class="modal fade" id="updateChargePay${audit.bmWaybillAuditView.id}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>修改实收实付</h4>
                                <hr>
                                <input type="hidden" id="waybillId" value="${audit.bmWaybillAuditView.id}">
                                <div>
                                    <span>应收用户（元）<a>${audit.planChargeAmount!''}</a></span>
                                    <span style="margin-left: 170px;">应付商家（元）<a>${audit.planPayAmount!''}</a></span>
                                    </br>
                                    <div>
                                        实收用户（元）<input id="charge_${audit.bmWaybillAuditView.waybillId!''}" type ="text" value="${audit.actualChargeAmount!''}">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        实付商家（元）<input id="pay_${audit.bmWaybillAuditView.waybillId!''}" type ="text" value="${audit.actualPayAmount!''}">
                                        <input type="hidden" id="oldPay_${audit.bmWaybillAuditView.waybillId!''}" value="${audit.actualPayAmount!''}"/>
                                        <input type="hidden" id="oldCharge_${audit.bmWaybillAuditView.waybillId!''}" value="${audit.actualChargeAmount!''}"/>
                                    </div>
                                    <br>
                                    <div class="modal-footer">
                                        <span id="tip_${audit.bmWaybillAuditView.waybillId!''}" style="display: none;color:red;">请输入修改后的价格！</span>
                                        <input type="button" id="doChange2" class="btn btn-danger btn-sm" style="button" value="提交审核" onclick="changePrice(${audit.bmWaybillAuditView.waybillId!''})">
                                        <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">取消</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 修改实收实付的对话框END -->
            </#list>
            </tbody>
        </table>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/settleWaybillAuditAgent/listAgent"/>
    </#if>
    </div>
</div>
<script>document.title = '可疑运单管理';</script>
