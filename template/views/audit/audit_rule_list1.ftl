<#include "audit_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/audit/audit_rule_list1.js</content>


<style type="text/css">
    #main-container{
        padding: 10px 20px;
    }
    .modal-body .panel-body span{
        margin-right: 15px;
    }
    .edit-form {
        margin-bottom: 5px;
    }
    .panel {
        margin-bottom: 5px;
    }
    tr th{
        color: white;
    }
    .border-red{
        border: 1px solid red;
    }
    .rule-li{
        padding-left: 15px;
    }
    .city-table tr th{
        text-align:center;
    }
    .city-table tr td{
        text-align:center;
    }
    .breadcrumb{
        height:38px;
    }
</style>
<div id="main-container">
    <div class="panel panel-default table-responsive">
        
        <div>
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation"><a href="/settleWaybillAudit/listJiaoma">需人工审核订单列表</a></li>
            <li role="presentation" class="active"><a href="/settleWaybillAudit/rule/list?waybillType=1">美团订单审核规则</a></li>
            <li role="presentation" ><a href="/settleWaybillAudit/rule/list?waybillType=2">非美团订单审核规则</a></li>
          </ul>
                <div style="width: 100%;height: 2px;background: #58B7B1;"></div>
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="audit-list">
            <button class="btn btn-info edit-link" id="add-rule" style="margin: 5px 10px" data-toggle="modal" title="添加新规则" data-target="#edit-modal" href="javacript:void(0);" target="_blank"> 添加新规则</button>
            <table class="table table-striped" id="responsiveTable">
                <thead>
                <tr style="background-color: gray;">
                    <th>名称</th>
                    <th>描述</th>
                    <th width="15%">应用城市</th>
                    <th>GPS信息检测规则</th>
                    <th>时间信息检测规则</th>
                    <th>操作人</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                    <#list auditrulelist as auditRule>
                    <tr>
                        <td class="prop-id" style="display: none;">${auditRule.id!''}</td>
                        <td>
                        <span class="prop-name">${auditRule.name!''}</span>
                        <br>
                        <#if auditRule.isDefault == 1>
                            <span class="default-rule" style="display:none;">true</span>
                            <i class="fa fa-star fa-lg opration-icon"></i>(默认规则)
                        </#if>
                        </td>
                        <td class="prop-description">${auditRule.description!''}</td>
                        <td class="rule-city">
                            <#if auditRule.isDefault == 1>
                                所有城市
                            </#if>
                            <#list auditRule.cityOrderAuditRuleViewList as city>
                                ${city.cityName!''}
                                &nbsp&nbsp
                            </#list>
                            <span style="display: none;" class="city-info">
                                [<#list auditRule.cityOrderAuditRuleViewList as city>["${city.cityName!''}","${city.cityId!''}","${city.userName!''}","${city.utime!''}"],</#list>]
                            </span>
                        </td>
                        <td>取货位置与发货地点直线距离:
                        <br>
                        <li class="rule-li" <#if auditRule.pickDistanceRejectMax == -1 > style="display: none;" </#if>>><span class="rule-pick-distance-reject-max">${auditRule.pickDistanceRejectMax!''}</span>米,系统置为审核不通过</li>
                        <li class="rule-li" <#if auditRule.pickDistanceCheckMax == -1 > style="display: none;" </#if>>><span class="rule-pick-distance-check-max">${auditRule.pickDistanceCheckMax!''}</span>米,需人工审核</li>
                        送达位置与收货地点直线距离:
                        <br>
                        <li class="rule-li" <#if auditRule.sendDistanceRejectMax == -1 > style="display: none;" </#if>>><span class="rule-send-distance-reject-max">${auditRule.sendDistanceRejectMax!''}</span>米,系统置为审核不通过</li>
                        <li class="rule-li" <#if auditRule.sendDistanceCheckMax == -1 > style="display: none;" </#if>>><span class="rule-send-distance-check-max">${auditRule.sendDistanceCheckMax!''}</span>米,需人工审核</li>
                        </td>
                        <td>立即单:
                        <br>
                        <li class="rule-li" <#if auditRule.deliveryTimeRejectMax == -1 > style="display: none;" </#if>>配送时长><span class="rule-delivery-time-reject-max">${auditRule.deliveryTimeRejectMax!''}</span>分钟,系统置为审核不通过</li>
                        <li class="rule-li" <#if auditRule.deliveryTimeCheckMax == -1 > style="display: none;" </#if>>配送时长><span class="rule-delivery-time-check-max">${auditRule.deliveryTimeCheckMax!''}</span>分钟,需人工审核</li>
                        <li class="rule-li" <#if auditRule.deliveryTimeRejectMin == -1 > style="display: none;" </#if>>配送时长<<span class="rule-delivery-time-reject-min">${auditRule.deliveryTimeRejectMin!''}</span>分钟,系统置为审核不通过</li>
                        <li class="rule-li" <#if auditRule.deliveryTimeCheckMin == -1 > style="display: none;" </#if>>配送时长<<span class="rule-delivery-time-check-min">${auditRule.deliveryTimeCheckMin!''}</span>分钟,需人工审核</li>
                        预订单:
                        <br>
                        <li class="rule-li" <#if auditRule.bookTimeRejectMax == -1 > style="display: none;" </#if>>当到达时间与期望时间差距><span class="rule-book-time-reject-max">${auditRule.bookTimeRejectMax!''}</span>分钟,系统置为审核不通过</li>
                        <li class="rule-li" <#if auditRule.bookTimeCheckMax == -1 > style="display: none;" </#if>>当到达时间与期望时间差距><span class="rule-book-time-check-max">${auditRule.bookTimeCheckMax!''}</span>分钟,需人工审核</li>
                        </td>
                        <td>
                            ${auditRule.userName!''}
                            <br>
                            ${auditRule.utime!''}
                        </td>
                        <td>
                            <a data-toggle="modal" class="edit-link" title="编辑" data-target="#edit-modal" href="javacript:void(0);" target="_blank" >
                                <i class="fa fa-edit fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                            <#if auditRule.isDefault == 0>
                                <a data-toggle="tooltip" class="del-rule" title="删除" href="javacript:void(0);" data-placement="top">
                                    <i class="fa fa-trash-o fa-lg opration-icon"></i>
                                </a>&nbsp;&nbsp;
                            </#if>
                        </td>
                    </tr>
                    </#list>
                </tbody>
            </table>
            <#import "../page/pager.ftl" as q>
            <#if recordCount??>
                <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/settleWaybillAudit/rule/list?waybillType=1"/>
            </#if>
            <div id="edit-modal" class="modal fade" role="dialog">
              <div class="modal-dialog" style="width: 50%;">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">编辑系统审核规则</h4>
                  </div>
                  <div class="modal-body" style="background-color: rgb(247,247,247)">
                    <div class="panel">
                        <div class="panel-heading">名称及描述</div>
                        <div class="panel-body">
                            <input id="modal-prop-id" type="text" style="display: none;">
                            <div class="edit-form">
                                <span>名称:</span>
                                <input id="modal-prop-name" type="text" maxlength="24">
                            </div>
                            <div class="edit-form">
                                <span>描述:</span>
                                <input id="modal-prop-description" style="width: 80%;" type="text" maxlength="64"/>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading GPS">GPS信息检测<br/></div>
                        <div class="panel-body">
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" class="rule-checkbox" checked="false" style="opacity: 1;" value="" >
                                当取货位置与发货地点直线距离<strong>>&nbsp&nbsp</strong><input id="modal-rule-pick-distance-reject-max" style="width: 40px" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')"/>&nbsp&nbsp米时,系统将此单置为"审核不通过"
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" class="rule-checkbox" checked="false" style="opacity: 1;" value="">
                                当取货位置与发货地点直线距离<strong>>&nbsp&nbsp</strong><input id="modal-rule-pick-distance-check-max" style="width: 40px" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')"/>&nbsp&nbsp米时,需人工审核
                              </label>
                            </div>
                            <hr>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" class="rule-checkbox" checked="false" style="opacity: 1;" value="">
                                当送达位置与收货地点直线距离<strong>>&nbsp&nbsp</strong><input id="modal-rule-send-distance-reject-max" style="width: 40px" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')"/>&nbsp&nbsp米时,系统将此单置为"审核不通过"
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" class="rule-checkbox" checked="false" style="opacity: 1;" value="">
                                当送达位置与收货地点直线距离<strong>>&nbsp&nbsp</strong><input id="modal-rule-send-distance-check-max" style="width: 40px" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')"/>&nbsp&nbsp米时,需人工审核
                              </label>
                            </div>

                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading time">时间信息检测<br/></div>
                        <div class="panel-body">
                            <div class="panel">
                                <div class="panel-heading">立即单</div>
                                <div class="panel-body">
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" class="rule-checkbox" checked="false" style="opacity: 1;" value="">
                                        当配送时长<strong>>&nbsp&nbsp</strong><input id="modal-rule-delivery-time-reject-max" style="width: 40px" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')"/>&nbsp&nbsp分钟时,系统将此单置为"审核不通过"
                                      </label>
                                    </div>
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" class="rule-checkbox" checked="false" style="opacity: 1;" value="">
                                        当配送时长<strong>>&nbsp&nbsp</strong><input id="modal-rule-delivery-time-check-max" style="width: 40px" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')"/>&nbsp&nbsp分钟时,需人工审核
                                      </label>
                                    </div>
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" class="rule-checkbox" checked="false" style="opacity: 1;" value="">
                                        当配送时长<strong><&nbsp&nbsp</strong><input id="modal-rule-delivery-time-reject-min" style="width: 40px" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')"/>&nbsp&nbsp分钟时,系统将此单置为"审核不通过"
                                      </label>
                                    </div>
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" class="rule-checkbox" checked="false" style="opacity: 1;" value="">
                                        当配送时长<strong><&nbsp&nbsp</strong><input id="modal-rule-delivery-time-check-min" style="width: 40px" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')"/>&nbsp&nbsp分钟时,需人工审核
                                      </label>
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-heading">预订单</div>
                                <div class="panel-body">
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" class="rule-checkbox" checked="false" style="opacity: 1;" value="">
                                        当到达时间与期望时间差距<strong>>=&nbsp&nbsp</strong><input id="modal-rule-book-time-reject-max" style="width: 40px" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')"/>&nbsp&nbsp分钟时,系统将此单置为"审核不通过"
                                      </label>
                                    </div>
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" class="rule-checkbox" checked="false" style="opacity: 1;" value="">
                                        当到达时间与期望时间差距<strong>>=&nbsp&nbsp</strong><input id="modal-rule-book-time-check-max" style="width: 40px" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')"/>&nbsp&nbsp分钟时,需人工审核
                                      </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="city-panel-all">
                        <div class="panel-heading">应用城市</div>
                        <!-- <div class="panel-body"> -->
                            <div class="city-select-area panel-heading" style="margin-top: 5px; margin-bottom: 5px;padding-left: 20px; background-color: white;">
                                <select class="form-control input-sm" id="citys" name="name" style="width: 150px;margin-right:10px;">
                                    <option id="city_option" value="北京,110100" >北京</option>
                                </select>
                                <button class="btn btn-success" id="add-city">添加城市</button>
                            </div>
                            <table class="table city-table panel-heading">
                                <thead>
                                    <tr style="background-color: gray;">
                                        <th>城市名</th>
                                        <th>城市ID</th>
                                        <th>操作记录</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody id="city-panel">

                                </tbody>
                            </table>
                        <!-- </div> -->
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                    <button class="btn btn-success" id="update-btn">确定</button>
                  </div>
                </div>

              </div>
            </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="audit-rule">
              
            </div>
          </div>

        </div>

    </div>
</div>
<script>
document.title = '美团运单审核规则';
    $('select').select2();
</script>