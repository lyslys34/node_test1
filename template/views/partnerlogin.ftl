
<#include "base/home_config.ftl">
<content tag="pageName">login</content>


<style type="text/css">
.err-msg{
    font-size: 15px;
    color: #e75724;
    margin-bottom: 5px;
}

a {
  color: #428bca;
  text-decoration: none;
  font-size: 14px;
}
a:hover{

    color: #2a6496;
}

</style>
<div style="padding-top:100px">

    <div style="text-align: center;">
        <ul>
            <li>
               <#if error?? && (error?length > 0)>
                    
                     <label class="err-msg">
                         ${error !''}
                     </label>
                </#if>
            </li>

            <li>
                <a href="${loginUrl !''}">重新登录</a>&nbsp;&nbsp;
                <a href="/partner/login/oldLogin">旧版登录</a>
            </li>
        </ul>
    </div>

</div>






