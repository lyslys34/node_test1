<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">

<#if unAuthMsg?exists >
<div id="main-container">
    <div class="alert alert-danger" role="alert">${unAuthMsg}</div>
    <div><a class="btn btn-success" href="javascript:location.href = document.referrer;">返回上一页</a></div>
</div>
<#else>
<content tag="cssmain">/staypoi/staypoi.css</content>
<content tag="javascript">/staypoi/index.js</content>
<!-- main-container -->
<div id="main-container">
    <div class="main-header clearfix">
        <div class="btn-group city-org-wrap">
            <div class="city-wrap pull-left" id='city'>
                <select name="cityId" id="cityId" class="form-control input-sm">
                    <#if role?exists && (role=="JINGLI" || role=="ZHANZHANG") && cityLimit?exists && (cityLimit?size > 0) >
                        <#list cityLimit as cityView>
                        <option value='${cityView.cityId}'>${cityView.cityName}</option>
                        </#list>
                    <#else>
                        <option value="-1" data-cityId="-1" selected="selected">全部城市</option>
                    </#if>
                </select>
            </div>
            <div class="station-wrap pull-left" id='station'>
                <select name="areaId" id="areaId" class="form-control input-sm">
                    <#if role?exists && (role=="ZHANZHANG") && orgLimit?exists && (orgLimit?size > 0) >
                        <#list orgLimit as orgView>
                        <option value='${orgView.orgId}'>${orgView.orgName}</option>
                        </#list>
                    <#else>
                        <option value="-1" data-orgId="-1" selected="selected">全部站点</option>
                    </#if>
                </select>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="row operate">
            <div class="col-md-3">
                <button type="button" class="btn btn-primary btn-add-poi">新增驻店商家</button>
            </div>
            <div class="col-md-9">
                <div class="form-group form-inline pull-right">
                  <input type="text" class="form-control" name="poiId" placeholder="输入商家POI...">
                  <button type="button" class="btn btn-primary btn-search-poi">搜索驻店商家</button>
                </div>
                <div class="form-group form-inline pull-right">
                  <input type="text" class="form-control" name="riderName" placeholder="输入骑手姓名or手机号...">
                  <button type="button" class="btn btn-primary btn-search-rider">搜索驻店骑手</button>
                </div>
            </div>
        </div>
        <p class="row" style="color: #F00;text-align: right;padding-right: 25px;display:block">今日驻店信息凌晨0点后已确定，如需修改某日信息，需0点前进行</p>
    </div>
    <div class="section">
        <div class="search-list">
            <table class="table table-hover table-bordered">
                <thead>
                  <tr>
                    <th>商家名称</th>
                    <th>商家POI</th>
                    <th>所在城市</th>
                    <th>所属站点</th>
                    <th>本店全部驻店人员</th>
                    <th>今日驻店信息</th>
                    <th>操作</th>
                  </tr>
                </thead>
                <tbody>                  
                </tbody>
              </table>
              <div class="search-result-info"></div>
              <ul class="pagination pull-right"></ul>
            </div>
        </div>
    </div>
<div class="loading-wrap"><p></p></div>
</#if>

<script>
    document.title = '驻店管理';
    $('select').select2();
</script>
<script src='/static/js/lib/jquery.twbsPagination.min.js'></script>
