<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">

<link rel="stylesheet" href="/static/css/lib/bootstrap-datetimepicker.min.css">

<#if unAuthMsg?exists >
<div id="main-container">
    <div class="alert alert-danger" role="alert">${unAuthMsg}</div>
    <div><a class="btn btn-success" href="javascript:location.href = document.referrer;">返回上一页</a></div>
</div>
<#else>
<content tag="cssmain">/staypoi/staypoi.css</content>
<content tag="javascript">/staypoi/modify.js</content>
<!-- main-container -->
<div id="main-container">
    <script type="text/javascript">
        if(location.href.indexOf('poiId=')>0){
            document.getElementById('main-container').className = "modify";
        }
    </script>
    <h4 class="for-add">新增驻店商家</h4>
    <h4 class="for-modify">修改驻店商家</h4>
    <div class="section">
        <h5 class="title for-add">选择商家</h5>
        <h5 class="title for-modify">商家POI</h5>
        <div class="form-group form-inline for-add">
          <input type="text" class="form-control" name="poiId" placeholder="请输入商家POI...">
          <button type="button" class="btn btn-primary btn-search-poi">查询商家</button>
          <span class="error-info error-info-search"></span>
        </div>
        <div class="search-list search-list-poi minw500">
            <table class="table table-hover table-bordered">
                <thead>
                  <tr>
                    <th>商家名称</th>
                    <th>商家POI</th>
                    <th>所在城市</th>
                    <th>所属站点</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
        </div>
    </div>
    <div class="section">
        <h5 class="title">驻店时间</h5>
        <div class="form-group form-inline">
          <button type="button" class="btn btn-primary btn-add-time" data-toggle="modal">添加驻店时间</button>
        </div>
        <div class="search-list search-list-time">
            <table class="table table-hover table-bordered">
                <thead>
                  <tr>
                    <th>星期</th>
                    <th>时间</th>
                    <th>兼职人员</th>
                    <th>操作</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
        </div>
    </div>
    <div class="section operate-wrap">
        <div class="row operate">
            <div class="btn-wrap">
                <button type="button" class="btn btn-success btn-save">保存</button>
                <button type="button" class="btn btn-danger btn-cancel">取消</button>
            </div>
        </div>
        <p class="row result-msg"></p>
    </div>
</div>
<!-- Modal -->
<div class="modal fade timeModal" id="timeModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>
<div class="loading-wrap"><p></p></div>
</#if>

<script>
    document.title = '新增驻店商家';
    $('select').select2();
</script>