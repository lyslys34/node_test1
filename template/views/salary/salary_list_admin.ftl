<#include "salary_config.ftl">
<#include "../widgets/sidebar.ftl">

<style type="text/css">
    #main-container {
        padding: 0 20px;
    }
    td {
        white-space:nowrap;
    }
    th {
        white-space:nowrap;
    }
</style>
<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post"
              action="/salary/setting/listSalary">
            <div class="panel-body" >
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group form-inline ">
                            <label class="control-label">时间:</label>
                            <select class="form-control input-sm" id="year" name="year">
                                <option value="2015" <#if year == 2015> selected="selected" </#if>>2015</option>
                                <option value="2016" <#if year == 2016> selected="selected" </#if>>2016</option>
                                <option value="2017" <#if year == 2017> selected="selected" </#if>>2017</option>
                                <option value="2018" <#if year == 2018> selected="selected" </#if>>2018</option>
                                <option value="2019" <#if year == 2019> selected="selected" </#if>>2019</option>
                                <option value="2020" <#if year == 2020> selected="selected" </#if>>2020</option>
                            </select>
                            <select class="form-control input-sm" id="month" name="month">
                                <option value="1" <#if month == 1> selected="selected" </#if>>一月</option>
                                <option value="2" <#if month == 2> selected="selected" </#if>>二月</option>
                                <option value="3" <#if month == 3> selected="selected" </#if>>三月</option>
                                <option value="4" <#if month == 4> selected="selected" </#if>>四月</option>
                                <option value="5" <#if month == 5> selected="selected" </#if>>五月</option>
                                <option value="6" <#if month == 6> selected="selected" </#if>>六月</option>
                                <option value="7" <#if month == 7> selected="selected" </#if>>七月</option>
                                <option value="8" <#if month == 8> selected="selected" </#if>>八月</option>
                                <option value="9" <#if month == 9> selected="selected" </#if>>九月</option>
                                <option value="10" <#if month == 10> selected="selected" </#if>>十月</option>
                                <option value="11" <#if month == 11> selected="selected" </#if>>十一月</option>
                                <option value="12" <#if month == 12> selected="selected" </#if>>十二月</option>
                            </select>
                        </div>
                        <!-- /form-group -->
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-inline">
                            <label class="control-label">城市:</label>
                            <select class="form-control input-sm" id="cityId" name="cityId" >
                                <option value="0" >全部</option>
                            <#list cityList as city>
                                <option value="${city.city_id}" <#if city.city_id == cityId >
                                selected="selected"</#if>> ${city.name}</option>
                            </#list>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-inline">
                            <label class="control-label">站点:</label>
                            <select class="form-control input-sm" id="orgId" name="orgId">
                                <option value="0" <#if orgId == 0 > selected="selected" </#if> >全部</option>
                            <#list orgList as org>
                                <option value="${org.orgId !''}" <#if org.orgId == orgId >
                                        selected="selected" </#if> >${org.orgName !''}</option>
                            </#list>
                            </select>
                        </div>
                        <!-- /form-group -->
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-inline">
                            <label class="control-label">配送员姓名:</label>
                            <select class="form-control input-sm" id="bmUserId" name="bmUserId" data-minlengt="8">
                                <option value="0">全部</option>
                            <#list riderList as rider>
                                <option value="${rider.id}" <#if rider.id == bmUserId >
                                        selected="selected" </#if> >${rider.name}</option>
                            </#list>
                            </select>
                        <#--<input type="text" class="form-control input-sm parsley-validated" id="userName" name="userName" value="${userName!''}" data-required="true" data-minlength="8">-->
                        </div>
                        <!-- /form-group -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group form-inline">
                            <label class="control-label">劳务派遣公司:</label>
                            <select class="form-control input-sm" id="laborDispatchingCompany" name="laborDispatchingCompany" data-minlengt="6">
                                <option value="0">全部</option>
                                <option value="1" <#if laborDispatchingCompany==1 >selected="selected" </#if>>北京万古恒信科技有限公司</option>
                                <option value="2" <#if laborDispatchingCompany==2 >selected="selected" </#if>>上海正东人力资源有限公司</option>
                                <option value="3" <#if laborDispatchingCompany==3 >selected="selected" </#if>>北京永盛立业企业管理有限公司</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group form-inline">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-inline">
                            <label class="control-label">站点ID:</label>
                            <input type="text" class="form-control input-sm parsley-validated" id="orgId1"
                                   name="orgId1" <#if orgId1 != 0> value="${orgId1}" </#if> data-required="true" data-minlength="8">
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel-footer text-left">
                <!--<button type="submit" class="btn btn-success">查询</button>-->
                <button type="button" class="btn btn-success js_submitbtn" actionurl="/salary/setting/salarylistAdmin">查询</button>
                <button type="button" class="btn btn-success js_submitbtn" actionurl="/salary/setting/salarylistExcelAdmin">
                    导出EXCEL
                </button>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive" style="overflow: auto">
    <#if vos?? && (vos?size > 0)>
        <table class="table table-striped" id="">
            <thead>
            <tr>
                <th>骑手ID</th>
                <th>姓名</th>
                <th>身份证</th>
                <th>联系电话</th>
                <th>站点城市</th>
                <th>站点</th>
                <th>劳务派遣公司</th>
                <th>招聘渠道</th>
                <th>户口类型</th>
                <th>薪资月份</th>
                <th>税后薪酬</th>
                <th>总薪酬</th>
                <th>实际出勤(天)</th>
                <th>应出勤(天)</th>
                <th>订单总量</th>
                <th>夜间配送订单量</th>
                <th>大额配送订单量</th>
                <th>提成</th>
                <th>夜间配送补贴</th>
                <th>大额配送补贴</th>
                <th>活动补贴</th>
                <th>底薪</th>
                <th>工作补贴(住房、餐饮、车辆、充电、话费)</th>
                <th>福利公司缴纳</th>
                <th>福利个人缴纳</th>
                <th>个人扣税</th>
            </tr>
            </thead>
            <tbody>
                <#list vos as vo>
                <tr>
                    <td>${vo.bmUserId}</td>
                    <td>${vo.bmUserName!''}</td>
                    <td>${vo.cardNo!''}</td>
                    <td>${vo.mobile!''}</td>
                    <td>${vo.cityName!''}</td>
                    <td>${vo.bmOrgName!''}</td>
                    <td>
                        <#if vo.laborDispatchingCompany==1>北京万古恒信科技有限公司<#elseif vo.laborDispatchingCompany==2>上海正东人力资源有限公司<#elseif vo.laborDispatchingCompany==3>北京永盛立业企业管理有限公司</#if>
                    </td>
                    <td>
                        <#if vo.recruitChannel==1>直招<#elseif vo.recruitChannel==2>非直招</#if>
                    </td>
                    <td>
                        <#if vo.householdRegisterType==1>本地城镇<#elseif vo.householdRegisterType==2>本地农村
                        <#elseif vo.householdRegisterType==3>外地城镇<#elseif vo.householdRegisterType==4>外地农村</#if>
                    </td>
                    <td>${vo.year}年${vo.month}月</td>
                    <td>${vo.total}</td>
                    <td>${vo.total+vo.tax+vo.personalWelfare}</td>
                    <td>${vo.days}</td>
                    <td>${vo.full_days}</td>
                    <td>${vo.waybillNum}</td>
                    <td>${vo.nightWaybillNum}</td>
                    <td>${vo.largeWaybillNum}</td>
                    <td>￥${vo.totalCommission}</td>
                    <td>￥${vo.nightSubsidy}</td>
                    <td>￥${vo.largrWaybillSubsidy}</td>
                    <td>￥${vo.temperatureSubsidy}</td>
                    <td>￥${vo.basicSalary}</td>
                    <td>￥${vo.sumSubsidy}</td>
                    <td>￥${vo.companyWelfare}</td>
                    <td>￥${vo.personalWelfare}</td>
                    <td>￥${vo.tax}</td>
                </tr>
                </#list>
            </tbody>
        </table>
    </#if>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/salary/setting/salarylistAdmin"/>
    </#if>
    </div>
</div>
<script>document.title = '自建配送员薪酬报表';
$('select').select2();
$(document).delegate(".js_submitbtn", "click", function() {
    var url=$(this).attr("actionurl");
    $("#formValidate1").attr("action",url);
    $("#formValidate1").submit();
});
</script>
