<#include "salary_config.ftl">
<#include "../widgets/sidebar.ftl">
<#setting datetime_format="yyyy-MM-dd HH:mm:ss"/>

<!--<link rel="stylesheet" type="text/css" href="/static/css/lib/bootstrap.css" />
<script type="text/javascript" src="/static/js/lib/jquery.js"></script>-->


<style type="text/css">
    .form-horizontal .control-label {
        text-align: left;
    }

    input[type="radio"] {
        opacity: 1;
    }
</style>

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" data-validate="parsley" novalidate="" method="post" id="salarySettingForm"
              action="/salary/setting/save">

        <#if vo??><input type="hidden" value="${vo.id!}" id="id" name="id"/> </#if>
        <#if vo??><input type="hidden" value="${vo.cityId!}" id="cityId" name="cityId"/> </#if>
            <div class="alert alert-danger" role="alert">
                <a href="#" class="alert-link"><#if message??>${message}<#else>
                    友情提醒：每月1-${endEditableDay}日为考勤时间，不可修改薪酬设置；每月${endEditableDay+1}日至月末修改，将会影响当月的薪酬数据 !</#if></a>
            </div>
            <div class="panel-body" id="div_body">
                <div class="row form-group form-inline">
                    <div class="col-sm-4">
                        <label class="col-sm-3 control-label">城市名称:</label>
                        <select class="form-control input-sm" id="cityId" name="cityId"
                                <#if vo?? || !editable>disabled</#if>>
                        <#--<option value = "0">全部</option>-->
                        <#if cityViews?exists && (cityViews?size > 0)>
                            <#list cityViews as city>
                                <option value="${city.city_id}"
                                        <#if vo?exists && vo.cityId == city.city_id>selected</#if>>${city.name}</option>
                            </#list>
                        </#if>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label class="col-sm-3 control-label">底薪金额:</label>
                        <input type="text" class="form-control input-sm" name="basicSalary" id="basicSalary"
                               <#if !editable>disabled</#if>
                               placeholder="" value="<#if vo??>${vo.basicSalary!""}</#if>"/> 元/月
                    </div>
                </div>
                <div class="row form-group form-inline">
                    <div class="col-sm-4">
                        <label class="col-sm-3 control-label">住房补贴:</label>
                        <input type="text" class="form-control input-sm" name="houseSubsidy" id="houseSubsidy"
                               <#if !editable>disabled</#if>
                               placeholder="" value="<#if vo??>${vo.houseSubsidy!""}</#if>"/> 元/月
                    </div>
                    <div class="col-sm-4">
                        <label class="col-sm-3 control-label">餐费补助:</label>
                        <input type="text" class="form-control input-sm" name="mealSubsidy" id="mealSubsidy"
                               <#if !editable>disabled</#if>
                               placeholder="" value="<#if vo??>${vo.mealSubsidy!""}</#if>"/> 元/天
                    </div>
                </div>
                <div class="row form-group form-inline">
                    <div class="col-sm-4">
                        <label class="col-sm-3 control-label">流量补助:</label>
                        <input type="text" class="form-control input-sm" name="internetSubsidy" id="internetSubsidy"
                               <#if !editable>disabled</#if>
                               placeholder="" value="<#if vo??>${vo.internetSubsidy!""}</#if>"/> 元/月
                    </div>
                    <div class="col-sm-4">
                        <label class="col-sm-3 control-label">话费补助:</label>
                        <input type="text" class="form-control input-sm" name="phoneSubsidy" id="phoneSubsidy"
                               <#if !editable>disabled</#if>
                               placeholder="" value="<#if vo??>${vo.phoneSubsidy!""}</#if>"/> 元/月
                    </div>
                </div>
                <div class="row form-group form-inline">
                    <div class="col-sm-4">
                        <label class="col-sm-3 control-label">车辆补助:</label>
                        <input type="text" class="form-control input-sm" name="vehicleSubsidy" id="vehicleSubsidy"
                               <#if !editable>disabled</#if>
                               placeholder="" value="<#if vo??>${vo.vehicleSubsidy!""}</#if>"/> 元/月
                    </div>
                    <div class="col-sm-4">
                        <label class="col-sm-3 control-label">充电补助:</label>
                        <input type="text" class="form-control input-sm" name="chargingSubsidy" id="chargingSubsidy"
                               <#if !editable>disabled</#if>
                               placeholder="" value="<#if vo??>${vo.chargingSubsidy!""}</#if>"/> 元/月
                    </div>
                </div>
                <div class="row form-group form-inline">
                    <div class="col-sm-4">
                        <label class="col-sm-3 control-label">月基础订单:</label>
                        <input type="text" class="form-control input-sm" name="basicWaybill" id="basicWaybill"
                               <#if !editable>disabled</#if>
                               placeholder="" value="<#if vo??>${vo.basicWaybill!""}</#if>"/> 单
                    </div>
                    <div class="col-sm-4">
                        <label class="col-sm-3 control-label">订单提成:</label>
                        <input type="text" class="form-control input-sm" name="commission" id="commission"
                               <#if !editable>disabled</#if>
                               placeholder="" value="<#if vo??>${vo.commission!""}</#if>"/> 元/单
                    </div>
                </div>
            </div>

            <div class="panel-footer text-center">
                <button type="button" class="btn btn-success js_submitbtn" <#if !editable>disabled</#if>
                        onclick="submitSalarySetting()">保存
                </button>
            </div>
        </form>
    </div>

<#if opviews??>
    <h4>操作记录:</h4>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>操作人</th>
            <th>操作时间</th>
            <th>操作内容</th>
        </tr>
        </thead>
        <tbody>
        <#--循环-->
            <#list opviews as opview>
            <tr>
                <td>${opview.userName!""}</td>
            <#--<td>${opview.ctime!""}</td>-->
                <td><#if (opview.ctime>0)>${(opview.ctime *1000)?number?number_to_datetime}</#if></td>
                <td>${opview.content!""}</td>
            </tr>
            </#list>
        </tbody>
    </table>
</#if>
</div>
<script type="text/javascript">
    document.title = '编辑薪酬设置';
    $('#cityId').select2();
    function submitSalarySetting() {
        var has_error = false;
        $('#salarySettingForm').find(':text').each(function () {
            if (!this.value || isNaN(this.value)) {
                $(this).parent().addClass('has-error');
                has_error = true;
            } else {
                $(this).parent().removeClass('has-error');
            }
        })
        if (has_error) {
            return;
        }
        $.post("/salary/setting/save",
                $('#salarySettingForm').serialize(),
                function (result) {
                    alert(result.message);
                    if (result.success) {
                        window.location.href = "/salary/setting/list";
                    }
                });
    }
</script>
