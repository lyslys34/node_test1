<#include "../config.ftl">
<#include "../widgets/sidebar.ftl">
<link href="/static/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" rel="stylesheet">
<link href="/static/css/lib/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script type="text/javascript" src="/static/js/lib/bootstrap-datetimepicker.min.js" ></script>
<style type="text/css">
    #main-container {
        padding: 10px 20px;
    }
    input[type="radio"], input[type="checkbox"] {
        opacity: 1;
    }
</style>
<div id="main-container">
    <ul class="nav nav-tabs">
        <li role="presentation"><a href="/salary/setting/list">基本薪酬项</a></li>
        <li role="presentation" class="active"><a href="/salary/setting/list?page=extra_subsidy">活动补贴</a></li>
    </ul>

    <div class="panel panel-default table-responsive">
        <div class="panel-footer text-left">
            <button type="button" class="btn btn-success <#if !editable>disabled</#if>" data-toggle="modal" id="btn_create">新建活动
            </button>
        </div>
        <div class="alert alert-danger" role="alert">
            <a href="#" class="alert-link"><#if message??>${message}<#else>
                友情提醒：每月1-${endEditableDay}日为考勤时间，不可修改薪酬设置；每月${endEditableDay+1}日至月末修改，将会影响当月的薪酬数据 !</#if></a>
        </div>
        <table class="table table-striped" id="">
            <thead>
            <tr>
                <th>活动名称</th>
                <th>活动开始时间</th>
                <th>活动结束时间</th>
                <th>活动范围</th>
                <th>活动金额</th>
                <th>活动对象</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#list vos as vo>
            <tr>
                <td>${vo.subsidyName!""}</td>
                <td>${vo.sdate!""}</td>
                <td>${vo.edate!""}</td>
                <td>${vo.cityName!""}</td>
                <td>￥${vo.subsidyMoney!""}</td>
                <td>${vo.bmRoleName!""}</td>
                <td>
                    <a title="修改" data-placement="top" data-toggle="modal" data-id="${vo.id}" action="edit"
                       class="btn btn-xs <#if !editable>disabled</#if>">
                        <i class="fa fa-edit fa-lg operation-icon"></i>
                    </a>
                </td>
            </tr>
            </#list>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/salary/setting/list"/>
    </#if>
    </div>

    <div class="modal fade" role="dialog" id="div_form">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <#--<h4 class="modal-title">Modal title</h4>-->
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="extraSubsidyForm" data-validate="parsley" method="post">
                        <input type="hidden" id="id" name="id" value="0"/>
                        <input type="hidden" id="subsidyType" name="subsidyType" value="1"/>

                        <div class="panel-body" id="div_body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">活动名称:</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control input-sm" name="subsidyName"
                                           id="subsidyName"/>

                                    <p style="color:red;display: none">活动名称不可重复</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">活动开始时间:</label>

                                <div class="col-sm-8">
                                    <input type="text" id="sdate" name="sdate" style="cursor: pointer" data-date-format="yyyy-mm-dd"
                                           readonly="readonly"
                                           class="form-control input-sm date-picker J-datepicker"/>

                                    <p style="color:red;">高温补贴以月为单位计算，请选择生效月份的第一天</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">活动结束时间:</label>

                                <div class="col-sm-8">
                                    <input type="text" id="edate" name="edate" style="cursor: pointer" data-date-format="yyyy-mm-dd"
                                           readonly="readonly" class="form-control input-sm date-picker J-datepicker"/>

                                    <p style="color:red;">高温补贴以月为单位计算，请选择结束月份的最后一天</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">活动范围:</label>

                                <div class="col-sm-8">
                                    <select class="form-control input-sm select2" id="cityId" name="cityId" style="width:349px">
                                        <option value="0">请选择城市</option>
                                    <#if cityViews?exists && (cityViews?size > 0)>
                                        <#list cityViews as city>
                                            <option value="${city.city_id}">${city.name}</option>
                                        </#list>
                                    </#if>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">活动金额:</label>

                                <div class="col-sm-8">
                                    <input type="number" class="form-control input-sm" name="subsidyMoney"
                                           id="subsidyMoney"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">活动对象:</label>

                                <div class="col-sm-8">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="bmRoleCode" id="bmRoleCode" value="1001"
                                                   checked="true">配送员
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <button type="button" class="btn btn-success" <#if !editable>disabled</#if> id="btn_submit">保存
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    </form>
</div>
<script type="text/javascript" src="/static/js/lib/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script type="text/javascript">
    document.title = '活动补贴';
    $(".date-picker").datetimepicker({'autoclose': true,'minView':2});
    $('.select2').select2();
//    $('#cityId').select2('val',110100);
    document.getElementById('btn_submit').onclick=submitForm;
    document.getElementById('btn_create').onclick=function(){showModal()};

    $('.table').delegate("a[action]", "click", function() {
        var id=$(this).attr('data-id');
        showModal(id);
    });

    function showModal(data_id){
        $('#id').val(0);
        $('#subsidyName').val('').parent().parent().removeClass('has-error');
        $('#subsidyMoney').val('').parent().parent().removeClass('has-error');
        $('#sdate').val('').parent().parent().removeClass('has-error');
        $('#edate').val('').parent().parent().removeClass('has-error');
        $('#cityId').select2('val',0);
        $('#cityId').parent().parent().removeClass('has-error');
        if(!data_id){
            $('#div_form').modal('show');
        }else {

            $.getJSON("/salary/setting/extraSubsidy/" + data_id, function (result) {
                $('#div_form').modal('show');
                if(result.item){
                    $('#id').val(result.item.id);
                    $('#subsidyName').val(result.item.subsidyName);
                    $('#subsidyMoney').val(result.item.subsidyMoney);
                    $('#sdate').val(result.item.sdate);
                    $('#edate').val(result.item.edate);
                    $('#cityId').select2('val',result.item.cityId);
                }
            })
        }
    }

    function submitForm() {
        var has_error = false;
        $('#div_body').find('input').each(function () {
            if (!this.value) {
                $(this).parent().parent().addClass('has-error');
                has_error = true;
            } else {
                $(this).parent().parent().removeClass('has-error');
            }
            if (this.name == 'subsidyMoney' && (isNaN(this.value) || this.value <= 0)) {
                $(this).parent().parent().addClass('has-error');
                has_error = true;
            }
        });
        var cityId = $('#cityId').val();
        if (cityId == 0) {
            $("#cityId").parent().parent().addClass('has-error');
            has_error = true;
        }
        else {
            $("#cityId").parent().parent().removeClass('has-error');
        }
        var sdate = new Date(Date.parse($("#sdate").val().replace(/-/g,   "/")));
        var edate = new Date(Date.parse($("#edate").val().replace(/-/g,   "/")));
        if(sdate > edate) {
            has_error = true;
            alert("开始时间不能晚于结束时间");
        }
        if (has_error) {
            return;
        }
        $.post("/salary/setting/extraSubsidy/save",
                $('#extraSubsidyForm').serialize(),
                function (result) {
                    if (result.success) {
                        window.location.reload();
                    } else {
                        if (result.errorInput == 'subsidyName') {
                            $("#subsidyName").focus().next('p').show().parent().parent().addClass('has-error');
                        } else {
                            $("#" + result.errorInput).parent().parent().addClass('has-error');
                        }
                    }
                });
    }
</script>
