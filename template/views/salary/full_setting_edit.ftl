<#include "salary_config.ftl">
<#include "../widgets/sidebar.ftl">
<#setting datetime_format="yyyy-MM-dd HH:mm:ss"/>
<#assign welfareTypeNum = [0, 1, 2, 3, 4, 5, 6, 7, 8]>

<style type="text/css">
    table thead {
        background-color: #eee;
    }
    .table > tbody > tr > td{ vertical-align: middle; }
    .td-input{
        outline: none;background-color: #fff;border: 1px solid #fff;
    }
    .table-striped > tbody > tr:nth-child(odd) > td .td-input{ 
        background: #f9f9f9;border: 1px solid #f9f9f9;
    }
    .table-striped .input-active, #welfare .input-active{
        color: #691715; border: 1px solid #fc8675 !important;
    }
    #welfare .td-input{ 
        padding-left: 4px;padding-right: 4px;
        max-width: 60px;
    }
    .table > tbody > tr > td{
        padding-top: 4px;padding-bottom: 4px;
    }
</style>
<div id="main-container">
    <div id="nav" style="color:#757986;font-size:14px;"><br />结算中心 > 配送员薪酬设置 </div><br />
    <div>
        <input type="hidden" value="<#if editable==true>1<#else>0</#if>" id="editable" />

        <div class="alert alert-danger" role="alert">
            <a href="#" class="alert-link"><#if error??>${error}<#else>
                友情提醒：每月1-${endEditableDay}日为考勤时间，不可修改薪酬设置；每月${endEditableDay+1}日至月末修改，将会影响当月的薪酬数据 !</#if></a>
        </div>
        <div class="row" style="margin-bottom: 10px; padding-right:20px">

            <div class="col-md-4">
                <i class="glyphicon glyphicon-bookmark opration-icon"></i>
                <label class="control-label">骑手所属城市:</label>
                <select id="cityId" name="cityId" style="width: 200px">
                <#if cityViews?exists && (cityViews?size > 0)>
                    <#list cityViews as city>
                        <option value="${city.city_id}"
                                <#if vo?exists && vo.cityId == city.city_id>selected</#if>>${city.name}</option>
                    </#list>
                </#if>
                </select>
            </div>
            <div style="float:right;padding-right: 10px"><a class="btn btn-success" style="float: right;" href="/salary/setting/list">完成设置</a></div>
        </div>
    </div>
<#if vo??>
    <div class="panel panel-default table-responsive">
        <div class="table_title form-inline" style="margin-top: 10px">
            <div style="float: left"><h5><i class="glyphicon glyphicon-usd opration-icon"></i>底薪</h5></div>
            <div class="pull-right" style="padding-right: 10px">
                <button type="button" class="btn btn-success btn-sm pull-right table-btn" <#if editable==false>disabled</#if>>编辑</button>
            </div>
            <table class="table table-striped table-bordered" id="basicSalary">
                <thead>
                    <tr>
                        <td>项目名</td>
                        <td>单位</td>
                        <td>金额</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>底薪</td>
                        <td>元/月</td>
                        <td>￥<input type="text" class="td-input" value="${vo.basicSalary}" disabled="disabled" /></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table_title form-inline">
            <div style="float: left"><h5><i class="glyphicon glyphicon-usd opration-icon"></i>工作补贴</h5></div>
            <div style="float:right;padding-right: 10px"><button type="button" class="btn btn-success btn-sm pull-right table-btn" <#if editable==false>disabled</#if>>编辑</button></div>
            <table class="table table-striped table-bordered" id="jobSubsidy">
                <thead>
                    <tr>
                        <td>项目名</td>
                        <td>单位</td>
                        <td>金额</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>住房补贴</td>
                        <td>元/月</td>
                        <td>￥<input type="text" class="td-input" value="${vo.houseSubsidy}" disabled="disabled" /></td>
                    </tr>
                    <tr>
                        <td>餐饮补贴</td>
                        <td>元/月</td>
                        <td>￥<input type="text" class="td-input" value="${vo.mealSubsidy}" disabled="disabled" /></td>
                    </tr>
                    <tr>
                        <td>车辆补贴</td>
                        <td>元/月</td>
                        <td>￥<input type="text" class="td-input" value="${vo.vehicleSubsidy}" disabled="disabled" /></td>
                    </tr>
                    <tr>
                        <td>话费补贴</td>
                        <td>元/月</td>
                        <td>￥<input type="text" class="td-input" value="${vo.phoneSubsidy}" disabled="disabled" /></td>
                    </tr>
                    <tr>
                        <td>充电补贴</td>
                        <td>元/月</td>
                        <td>￥<input type="text" class="td-input" value="${vo.chargingSubsidy}" disabled="disabled" /></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table_title form-inline">
            <div style="float: left"><h5><i class="glyphicon glyphicon-usd opration-icon"></i>订单补贴</h5></div>
            <div style="float:right;padding-right: 10px"><button type="button" class="btn btn-success btn-sm pull-right table-btn" <#if editable==false>disabled</#if>>编辑</button></div>
            <table class="table table-striped table-bordered" id="orderSubsidy">
                <thead>
                    <tr>
                        <td>项目名</td>
                        <td>单位</td>
                        <td>金额</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>夜间补贴</td>
                        <td>元/单</td>
                        <td>￥<input type="text" class="td-input" value="${vo.nightSubsidy}" disabled="disabled" /></td>
                    </tr>
                    <tr>
                        <td>大额配送补贴</td>
                        <td>元/单</td>
                        <td>￥<input type="text" class="td-input" value="${vo.largeWaybillSubsidy}" disabled="disabled" /></td>
                    </tr>
                    <tr>
                        <td>大额配送补贴起始金额</td>
                        <td>元/单</td>
                        <td>￥<input type="text" class="td-input" value="${vo.largeWaybillMinAmount}" disabled="disabled" /></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table_title form-inline">
            <div style="float: left"><h5><i class="glyphicon glyphicon-usd opration-icon"></i>提成</h5></div>
            <div style="float:right;padding-right: 10px"><button type="button" class="btn btn-success btn-sm pull-right table-btn" <#if editable==false>disabled</#if>>编辑</button></div>
            <table class="table table-striped table-bordered" id="percentage">
                <thead>
                    <tr>
                        <td>订单区间</td>
                        <td>单位</td>
                        <td>金额</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>${firstLevel}</td>
                        <td>元/单</td>
                        <td>￥<input type="text" class="td-input" value="${vo.firstLevel}" disabled="disabled" /></td>
                    </tr>
                    <tr>
                        <td>${secondLevel}</td>
                        <td>元/单</td>
                        <td>￥<input type="text" class="td-input" value="${vo.secondLevel}" disabled="disabled" /></td>
                    </tr>
                    <tr>
                        <td>${thirdLevel}</td>
                        <td>元/单</td>
                        <td>￥<input type="text" class="td-input" value="${vo.thirdLevel}" disabled="disabled" /></td>
                    </tr>
                    <tr>
                        <td>${fourthLevel}</td>
                        <td>元/单</td>
                        <td>￥<input type="text" class="td-input" value="${vo.fourthLevel}" disabled="disabled" /></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="table_title form-inline">
            <div><h5><i class="glyphicon glyphicon-usd opration-icon"></i>福利</h5></div>
            <table class="table table-striped table-bordered" id="welfare">
                <thead>
                    <tr>
                        <th>户籍类型</th>
                        <th>缴费方</th>
                        <th>福利总计</th>
                        <th>养老保险</th>
                        <th>医疗保险</th>
                        <th>失业保险</th>
                        <th>工伤保险</th>
                        <th>生育保险</th>
                        <th>住院医疗</th>
                        <th>大病医疗</th>
                        <th>残保金</th>
                        <th>公积金</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td rowspan="2">本地城镇</td>
                    <td>公司缴纳 
                        <div class="hidden">
                            <input type="text" value="<#if vo.companyLocalTown??>${vo.companyLocalTown.id}<#else>0</#if>" class="hidden" />
                            <input type="text" value="1" class="hidden" />
                            <input type="text" value="1" class="hidden" />
                        </div>
                    </td>
                    <#if vo.companyLocalTown??>
                        <td>￥${vo.companyLocalTown.total}</td>
                        <td>￥<input type="text" class="td-input" value="${vo.companyLocalTown.pensionInsurance}" disabled="disabled" /></td>
                        <td>￥<input type="text" class="td-input" value="${vo.companyLocalTown.medicalInsurance}" disabled="disabled" /></td>
                        <td>￥<input type="text" class="td-input" value="${vo.companyLocalTown.joblessInsurance}" disabled="disabled" /></td>
                        <td>￥<input type="text" class="td-input" value="${vo.companyLocalTown.accidentInsurance}" disabled="disabled" /></td>
                        <td>￥<input type="text" class="td-input" value="${vo.companyLocalTown.maternityInsurance}" disabled="disabled" /></td>
                        <td>￥<input type="text" class="td-input" value="${vo.companyLocalTown.hospitalizationInsurance}" disabled="disabled" /></td>
                        <td>￥<input type="text" class="td-input" value="${vo.companyLocalTown.seriousIllnessInsurance}" disabled="disabled" /></td>
                        <td>￥<input type="text" class="td-input" value="${vo.companyLocalTown.disabilityInsurance}" disabled="disabled" /></td>
                        <td>￥<input type="text" class="td-input" value="${vo.companyLocalTown.housingFund}" disabled="disabled" /></td>
                    <#else>
                        <td>0</td>
                    <#list welfareTypeNum as vo>
                        <td><input type="text" class="td-input" value="0" disabled="disabled" /></td>
                    </#list>
                    </#if>
                    <td><button type="button" class="btn btn-success btn-sm welfare-btn" <#if editable==false>disabled</#if>>编辑</button></td>
                </tr>
                <tr>
                    <td>个人缴纳
                        <div class="hidden">
                            <input type="text" value="<#if vo.personalLocalTown??>${vo.personalLocalTown.id}<#else>0</#if>" class="hidden" />
                            <input type="text" value="1" class="hidden" />
                            <input type="text" value="2" class="hidden" />
                        </div>
                    </td>
                <#if vo.personalLocalTown??>
                    <td>￥${vo.personalLocalTown.total}</td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalTown.pensionInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalTown.medicalInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalTown.joblessInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalTown.accidentInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalTown.maternityInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalTown.hospitalizationInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalTown.seriousIllnessInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalTown.disabilityInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalTown.housingFund}" disabled="disabled" /></td>
                <#else>
                    <td>0</td>
                    <#list welfareTypeNum as vo>
                        <td><input type="text" class="td-input" value="0" disabled="disabled" /></td>
                    </#list>
                </#if>
                    <td><button type="button" class="btn btn-success btn-sm welfare-btn" <#if editable==false>disabled</#if>>编辑</button></td>
                </tr>
                <tr>
                    <td rowspan="2">本地农村</td>
                    <td>公司缴纳
                        <div class="hidden">
                            <input type="text" value="<#if vo.companyLocalCountry??>${vo.companyLocalCountry.id}<#else>0</#if>" class="hidden" />
                            <input type="text" value="2" class="hidden" />
                            <input type="text" value="1" class="hidden" />
                        </div>
                    </td>
                <#if vo.companyLocalCountry??>
                    <td>￥${vo.companyLocalCountry.total}</td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyLocalCountry.pensionInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyLocalCountry.medicalInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyLocalCountry.joblessInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyLocalCountry.accidentInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyLocalCountry.maternityInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyLocalCountry.hospitalizationInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyLocalCountry.seriousIllnessInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyLocalCountry.disabilityInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyLocalCountry.housingFund}" disabled="disabled" /></td>
                <#else>
                    <td>0</td>
                    <#list welfareTypeNum as vo>
                        <td><input type="text" class="td-input" value="0" disabled="disabled" /></td>
                    </#list>
                </#if>
                    <td><button type="button" class="btn btn-success btn-sm welfare-btn" <#if editable==false>disabled</#if>>编辑</button></td>
                </tr>
                <tr>
                    <td>个人缴纳
                        <div class="hidden">
                            <input type="text" value="<#if vo.personalLocalCountry??>${vo.personalLocalCountry.id}<#else>0</#if>" class="hidden" />
                            <input type="text" value="2" class="hidden" />
                            <input type="text" value="2" class="hidden" />
                        </div>
                    </td>
                <#if vo.personalLocalCountry??>
                    <td>￥${vo.personalLocalCountry.total}</td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalCountry.pensionInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalCountry.medicalInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalCountry.joblessInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalCountry.accidentInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalCountry.maternityInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalCountry.hospitalizationInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalCountry.seriousIllnessInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalCountry.disabilityInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalLocalCountry.housingFund}" disabled="disabled" /></td>
                <#else>
                    <td>0</td>
                    <#list welfareTypeNum as vo>
                        <td><input type="text" class="td-input" value="0" disabled="disabled" /></td>
                    </#list>
                </#if>
                    <td><button type="button" class="btn btn-success btn-sm welfare-btn" <#if editable==false>disabled</#if>>编辑</button></td>
                </tr>
                <tr>
                    <td rowspan="2">外地城镇</td>
                    <td>公司缴纳
                        <div class="hidden">
                            <input type="text" value="<#if vo.companyForeignTown??>${vo.companyForeignTown.id}<#else>0</#if>" class="hidden" />
                            <input type="text" value="3" class="hidden" />
                            <input type="text" value="1" class="hidden" />
                        </div>
                    </td>
                <#if vo.companyForeignTown??>
                    <td>￥${vo.companyForeignTown.total}</td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignTown.pensionInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignTown.medicalInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignTown.joblessInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignTown.accidentInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignTown.maternityInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignTown.hospitalizationInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignTown.seriousIllnessInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignTown.disabilityInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignTown.housingFund}" disabled="disabled" /></td>
                <#else>
                    <td>0</td>
                    <#list welfareTypeNum as vo>
                        <td><input type="text" class="td-input" value="0" disabled="disabled" /></td>
                    </#list>
                </#if>
                    <td><button type="button" class="btn btn-success btn-sm welfare-btn" <#if editable==false>disabled</#if>>编辑</button></td>
                </tr>
                <tr>
                    <td>个人缴纳
                        <div class="hidden">
                            <input type="text" value="<#if vo.personalForeignTown??>${vo.personalForeignTown.id}<#else>0</#if>" class="hidden" />
                            <input type="text" value="3" class="hidden" />
                            <input type="text" value="2" class="hidden" />
                        </div>
                    </td>
                <#if vo.personalForeignTown??>
                    <td>￥${vo.personalForeignTown.total}</td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignTown.pensionInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignTown.medicalInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignTown.joblessInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignTown.accidentInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignTown.maternityInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignTown.hospitalizationInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignTown.seriousIllnessInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignTown.disabilityInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignTown.housingFund}" disabled="disabled" /></td>
                <#else>
                    <td>0</td>
                    <#list welfareTypeNum as vo>
                        <td><input type="text" class="td-input" value="0" disabled="disabled" /></td>
                    </#list>
                </#if>
                    <td><button type="button" class="btn btn-success btn-sm welfare-btn" <#if editable==false>disabled</#if>>编辑</button></td>
                </tr>
                <tr>
                    <td rowspan="2">外地农村</td>
                    <td>公司缴纳
                        <div class="hidden">
                            <input type="text" value="<#if vo.companyForeignCountry??>${vo.companyForeignCountry.id}<#else>0</#if>" class="hidden" />
                            <input type="text" value="4" class="hidden" />
                            <input type="text" value="1" class="hidden" />
                        </div>
                    </td>
                <#if vo.companyForeignCountry??>
                    <td>￥${vo.companyForeignCountry.total}</td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignCountry.pensionInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignCountry.medicalInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignCountry.joblessInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignCountry.accidentInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignCountry.maternityInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignCountry.hospitalizationInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignCountry.seriousIllnessInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignCountry.disabilityInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.companyForeignCountry.housingFund}" disabled="disabled" /></td>
                <#else>
                    <td>0</td>
                    <#list welfareTypeNum as vo>
                        <td><input type="text" class="td-input" value="0" disabled="disabled" /></td>
                    </#list>
                </#if>
                    <td><button type="button" class="btn btn-success btn-sm welfare-btn" <#if editable==false>disabled</#if>>编辑</button></td>
                </tr>
                <tr>
                    <td>个人缴纳
                        <div class="hidden">
                            <input type="text" value="<#if vo.personalForeignCountry??>${vo.personalForeignCountry.id}<#else>0</#if>" class="hidden" />
                            <input type="text" value="4" class="hidden" />
                            <input type="text" value="2" class="hidden" />
                        </div>
                    </td>
                <#if vo.personalForeignCountry??>
                    <td>￥${vo.personalForeignCountry.total}</td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignCountry.pensionInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignCountry.medicalInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignCountry.joblessInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignCountry.accidentInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignCountry.maternityInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignCountry.hospitalizationInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignCountry.seriousIllnessInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignCountry.disabilityInsurance}" disabled="disabled" /></td>
                    <td>￥<input type="text" class="td-input" value="${vo.personalForeignCountry.housingFund}" disabled="disabled" /></td>
                <#else>
                    <td>0</td>
                    <#list welfareTypeNum as vo>
                        <td><input type="text" class="td-input" value="0" disabled="disabled" /></td>
                    </#list>
                </#if>
                    <td><button type="button" class="btn btn-success btn-sm welfare-btn" <#if editable==false>disabled</#if>>编辑</button></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</#if>
</div>

<script type="text/javascript">
    document.title = '编辑薪酬设置';
    $('select').select2();
    $("#cityId").change(function(event) {
        // window.location.assign( "/salary/setting/editFull/"+$(this).val() );
        // window.location = "/salary/setting/editFull/"+$(this).val();
        reloadUrl();
    });
    function reloadUrl() {
        window.location = "/salary/setting/editFull/"+$("#cityId").val();
    };
    $(".form-inline").delegate('button', "click", function(event) {//.table-btn
        var _this = $(this);
        if ( _this.hasClass('save') ) {// 执行保存的操作
            fnSave( _this );
        }else{// 执行编辑的操作
            fnEdit( _this );
        }
    });
    // 常用的编辑
    function fnEdit(node) {
        var table = node.parent().next();
        if ( node.hasClass('welfare-btn') ) {
            table = node.parents("table");
            var tr = node.parent().parent();

            tr.find('input').addClass('input-active');
            tr.find('input').removeAttr("disabled");
            tr.find('input:eq(0)').focus();
        }else{
            table.find('input').addClass('input-active');
            table.find('input').removeAttr("disabled");
            table.find('input:eq(0)').focus();
        }
        

        node.text("保存");
        node.addClass('save');
    };
    // 常用的保存
    function fnSave(node) {
        var table = node.parent().next();
        var tr = node.parent().parent();

        if ( node.hasClass('welfare-btn') ) {
            table = node.parents("table");
        };
        var tableId = table.attr("id");

        var data = {
            "cityId": $("#cityId").val()
        };
        switch( tableId ){
            case "basicSalary":
                url = '/salary/setting/saveBasicSalary';
                data.basicSalary = table.find('input:eq(0)').val();
                break;
            case "jobSubsidy":
                url = '/salary/setting/saveBaseSubidy';
                data.houseSubsidy = table.find('input:eq(0)').val();
                data.mealSubsidy = table.find('input:eq(1)').val();
                data.vehicleSubsidy = table.find('input:eq(2)').val();
                data.phoneSubsidy = table.find('input:eq(3)').val();
                data.chargingSubsidy = table.find('input:eq(4)').val();
                break;
            case "orderSubsidy":
                url = '/salary/setting/saveWaybillSubidy';
                data.nightSubsidy = table.find('input:eq(0)').val();
                data.largeWaybillSubsidy = table.find('input:eq(1)').val();
                data.largeWaybillMinAmount = table.find('input:eq(2)').val();
                break;
            case "percentage":
                url = '/salary/setting/saveCommission';
                data.firstLevelMoney = table.find('input:eq(0)').val();
                data.secondLevelMoney = table.find('input:eq(1)').val();
                data.thirdLevelMoney = table.find('input:eq(2)').val();
                data.fourthLevelMoney = table.find('input:eq(3)').val();
                break;
            case "welfare":
                url = '/salary/setting/saveWelfare';
                data.id = tr.find('input:eq(0)').val()
                data.householdRegisterType = tr.find('input:eq(1)').val()
                data.payerType = tr.find('input:eq(2)').val()
                data.pensionInsurance = tr.find('input:eq(3)').val();
                data.medicalInsurance = tr.find('input:eq(4)').val();
                data.joblessInsurance = tr.find('input:eq(5)').val();
                data.accidentInsurance = tr.find('input:eq(6)').val();
                data.maternityInsurance = tr.find('input:eq(7)').val();
                data.hospitalizationInsurance = tr.find('input:eq(8)').val();
                data.seriousIllnessInsurance = tr.find('input:eq(9)').val();
                data.disabilityInsurance = tr.find('input:eq(10)').val();
                data.housingFund = tr.find('input:eq(11)').val();
                break;
            default:
                alert("查无此按钮");
                return;
        };
        for(var item in data){
            if(data[item]===''){
                data[item] = 0;
            }else if(isNaN(data[item])){
                alert('请输入数字！');
                return;
            }
        }
        ajaxSave(data, url, function(){
            table.find('input').attr("disabled", "disabled").removeClass('input-active');
            node.text("编辑");
            node.removeClass('save');
        });
    };
    function  ajaxSave(sendData, url, callback) {
        $.ajax({
            url: url,
            type: 'POST',
            data: sendData,
            success: function(res) {
                if (res.success) {
                    if(callback && typeof(callback) == 'function'){ callback(); }
                    alert(res.message);
                    reloadUrl();
                }else{
                    alert(res.message);
                }
            },
            error: function() {
                alert("请检查网络！");
            }
        });
    }
</script>