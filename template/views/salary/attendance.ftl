<#include "salary_config.ftl">
<#include "../widgets/sidebar.ftl">

<#--<link rel="stylesheet" type="text/css" href="/static/css/lib/touchspin/jquery.bootstrap-touchspin.min.css" />-->

<script>
$(document).ready(function(){
//    $(".num").TouchSpin({
//        min: 0,
//        max: 999999999,
//        step: 0.1,
//        decimals: 1,
//        boostat: 5,
//        maxboostedstep: 10
//    });
	initSave();
});

function initSave() {
	$("#save_button").click(function() {
		var rows = $("#context tr");
		var data = new Array()
		var i = 0;
		$(rows).each(function(){
		    var id = $(this).find("input[type='hidden']").val();
		    var day = $(this).find("input[type='text']").val();
		    data[i++] = {"id":id,"day":day};
		});
		var jsonStr = JSON.stringify(data);
		
	    $.ajax({
	        dataType: 'json',
	        type : 'post',
	        url : "/partner/salary/saveAttendance",
	        data: {
	        	data: jsonStr
	        },
	        success : function(data){
	            if(data.success){
	            	alert("保存成功");
	            	window.location.reload();
	            }else {
	                alert(data.success);
	            }
	        },
	        error:function(XMLHttpRequest ,errMsg){
	            alert("网络连接失败");
	        }
	    });
	});    
}
</script>
<div id="main-container">
    <div class="alert alert-info" role="alert">
        ${tip}
    </div>
    <div class="panel panel-default table-responsive">
    	<table class="table table-striped" id="" >
            <thead>
            <tr>
                <th>骑手姓名</th>
                <th>一月</th>
                <th>二月</th>
                <th>三月</th>
                <th>四月</th>
                <th>五月</th>
                <th>六月</th>
                <th>七月</th>
                <th>八月</th>
                <th>九月</th>
                <th>十月</th>
                <th>十一月</th>
                <th>十二月</th>
            </tr>
            </thead>
            <tbody id = "context">
            <#--循环-->
            <#list riders as rider>
                <tr>
                    <th>${rider.userName}<input type="hidden" value="${rider.userId}"></th>
                    <#list rider.days as day>
	                <th>
						<#if day_index < curMonth-1><!--  本月之前的如果没有数据显示为－ -->
							<#if day_index == editMonth - 1 && canEdit == true>
                                <input type="text" class="num" value="<#if day=='' >0<#else>${day}</#if>">
							<#else>
								<#if day=='' >-<#else>${day}</#if>
							</#if>
						<#else><!--  本月及以后的不用显示－ -->
							${day}
						</#if>

	                </th>
	                </#list>
                </tr>
            </#list>
            </tbody>
        </table>
        
        <#import "../page/pager.ftl" as q>
	    <#if recordCount??>
	        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/salary/rider/attendance"/>
	    </#if>
    </div>
    <div class="panel-footer text-center" style="background-color: #f9f9f9;">
    	<input type="button" class="btn btn-success" id="save_button" value="保存"/>
	</div>
</div>
<script>document.title = '出勤天数';</script>
<#--<script type="text/javascript" src="/static/js/lib/touchspin/jquery.bootstrap-touchspin.min.js"></script>-->
