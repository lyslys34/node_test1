<#include "salary_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/settle/date.js</content>

<style type="text/css">
    #main-container {
        padding: 0 20px;
    }
</style>
<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post"
              action="/partner/salary/list">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group form-inline">
                            <label class="control-label">时间:</label>
                            <select class="form-control input-sm" id="year" name="year">
                                <option value="2015" <#if year == 2015> selected="selected" </#if>>2015</option>
                                <option value="2016" <#if year == 2016> selected="selected" </#if>>2016</option>
                                <option value="2017" <#if year == 2017> selected="selected" </#if>>2017</option>
                                <option value="2018" <#if year == 2018> selected="selected" </#if>>2018</option>
                                <option value="2019" <#if year == 2019> selected="selected" </#if>>2019</option>
                                <option value="2020" <#if year == 2020> selected="selected" </#if>>2020</option>
                            </select>
                            <select class="form-control input-sm" id="month" name="month">
                                <option value="1" <#if month == 1> selected="selected" </#if>>一月</option>
                                <option value="2" <#if month == 2> selected="selected" </#if>>二月</option>
                                <option value="3" <#if month == 3> selected="selected" </#if>>三月</option>
                                <option value="4" <#if month == 4> selected="selected" </#if>>四月</option>
                                <option value="5" <#if month == 5> selected="selected" </#if>>五月</option>
                                <option value="6" <#if month == 6> selected="selected" </#if>>六月</option>
                                <option value="7" <#if month == 7> selected="selected" </#if>>七月</option>
                                <option value="8" <#if month == 8> selected="selected" </#if>>八月</option>
                                <option value="9" <#if month == 9> selected="selected" </#if>>九月</option>
                                <option value="10" <#if month == 10> selected="selected" </#if>>十月</option>
                                <option value="11" <#if month == 11> selected="selected" </#if>>十一月</option>
                                <option value="12" <#if month == 12> selected="selected" </#if>>十二月</option>
                            </select>
                        </div>
                        <!-- /form-group -->
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-inline">
                            <label class="control-label">姓名:</label>
                            <select class="form-control input-sm" id="bmUserId" name="bmUserId" data-minlengt="8">
                                <option value="0">全部</option>
                            <#list riderList as rider>
                                <option value="${rider.id}" <#if rider.id == bmUserId >
                                        selected="selected" </#if> >${rider.name}</option>
                            </#list>
                            </select>
                        <#--<input type="text" class="form-control input-sm parsley-validated" id="userName" name="userName" value="${userName!''}" data-required="true" data-minlength="8">-->
                        </div>
                        <!-- /form-group -->
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-inline">
                            <label class="control-label">组织名称:</label>
                            <select class="form-control input-sm" id="orgId" name="orgId">
                                <option value="0" <#if orgId == 0 > selected="selected" </#if> >全部</option>
                            <#list orgList as org>
                                <option value="${org.orgId !''}" <#if org.orgId == orgId >
                                        selected="selected" </#if> >${org.orgName !''}</option>
                            </#list>
                            </select>
                        </div>
                        <!-- /form-group -->
                    </div>
                </div>
            </div>
            <div class="panel-footer text-left">
                <!--<button type="submit" class="btn btn-success">查询</button>-->
                <button type="button" class="btn btn-success js_submitbtn" actionurl="/partner/salary/list">查询</button>
                <button type="button" class="btn btn-success js_submitbtn" actionurl="/partner/salary/listExcel">
                    导出EXCEL
                </button>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
    <#if vos?? && (vos?size > 0)>
        <table class="table table-striped" id="">
            <thead>
            <tr>
                <th>姓名</th>
                <th>薪资金额</th>
                <th>底薪金额</th>
                <th>住房补贴</th>
                <th>餐费补助</th>
                <th>流量补助</th>
                <th>话费补助</th>
                <th>车辆补助</th>
                <th>充电补助</th>
                <th>高温补助</th>
                <th>提成金额</th>
                <th>实际出勤天数</th>
                <th>实际配送订单量</th>
                <th>所在组织</th>
            </tr>
            </thead>
            <tbody>
                <#list vos as vo>
                <tr>
                    <td>${vo.bmUserName}</td>
                    <td>￥${vo.total}</td>
                    <td>￥${vo.basicSalary}</td>
                    <td>￥${vo.houseSubsidy}</td>
                    <td>￥${vo.mealSubsidy}</td>
                    <td>￥${vo.internetSubsidy}</td>
                    <td>￥${vo.phoneSubsidy}</td>
                    <td>￥${vo.vehicleSubsidy}</td>
                    <td>￥${vo.chargingSubsidy}</td>
                    <td>￥${vo.temperatureSubsidy}</td>
                    <td>￥${vo.totalCommission}</td>
                    <td>${vo.days}</td>
                    <td>${vo.waybillNum}</td>
                    <td>${vo.bmOrgName!""}</td>
                </tr>
                </#list>
            </tbody>
        </table>
    </#if>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/salary/list"/>
    </#if>
    </div>
</div>
<script>document.title = '自建配送员薪酬报表';
$('select').select2();</script>
