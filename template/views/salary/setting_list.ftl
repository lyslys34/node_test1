<#include "salary_config.ftl">
<#include "../widgets/sidebar.ftl">

<#assign welfareNum = [0, 1, 2, 3, 4, 5, 6, 7]>
<#--<content tag="javascript">/settle/date.js</content>-->

<style type="text/css">
    #main-container {
        padding: 10px 20px;
    }
    table thead tr th{
        white-space: nowrap;
        text-align: center;        
        background-color: #f8f8f8;
    }
    .table-responsive{
        overflow-x: auto;
    }
    .table > tbody > tr > td{
        /*padding-top: 4px;padding-bottom: 4px;*/
    }
</style>
<div id="main-container">
    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="/salary/setting/list">基本薪酬项</a></li>
        <li role="presentation"><a href="/salary/setting/list?page=extra_subsidy">活动补贴</a></li>
    </ul>

    <div class="panel panel-default table-responsive">
        <div class="panel-footer text-left">
            <#--<a type="button" class="btn btn-success <#if !editable>disabled</#if>" href="/salary/setting/new">新建城市-->
            <#--</a>-->
        </div>
        <table class="table table-striped table-bordered">
            <thead>
            <tr style="border: 1px solid #d3d3d3;text-align: center">
                <th>大类</th>
                <th>底薪</th>
                <th colspan="5">工作补贴</th>
                <th colspan="2">订单补贴</th>
                <th colspan="4">提成</th>
                <th colspan="4">福利：公司缴纳</th>
                <th colspan="4">福利：个人缴纳</th>
                <th rowspan="2">操作</th>
            </tr>
            <tr style="border: 1px solid #d3d3d3;">
                <th>子类</th>
                <th>底薪(元/月)</th>
                <th>住房补贴(元/月)</th>
                <th>餐饮补贴(元/月)</th>
                <th>车辆补贴(元/月)</th>
                <th>话费补贴(元/月)</th>
                <th>充电补贴(元/月)</th>
                <th>夜间补贴(元/月)</th>
                <th>大额配送补贴(元/月)</th>
                <th>${firstLevel}</th>
                <th>${secondLevel}</th>
                <th>${thirdLevel}</th>
                <th>${fourthLevel}</th>
                <th>本地城镇(元/月)</th>
                <th>本地农村(元/月)</th>
                <th>外地城镇(元/月)</th>
                <th>外地农村(元/月)</th>
                <th>本地城镇(元/月)</th>
                <th>本地农村(元/月)</th>
                <th>外地城镇(元/月)</th>
                <th>外地农村(元/月)</th>
            </tr>
            </thead>
            <tbody>
            <#list vos as vo>
            <tr>
                <td>${vo.cityName!''}</td>
                <td>￥${vo.basicSalary}</td>
                <td>￥${vo.houseSubsidy}</td>
                <td>￥${vo.mealSubsidy}</td>
                <td>￥${vo.vehicleSubsidy}</td>
                <td>￥${vo.phoneSubsidy}</td>
                <td>￥${vo.chargingSubsidy}</td>
                <td>￥${vo.nightSubsidy}</td>
                <td>￥${vo.largeWaybillSubsidy}</td>
                <td>￥${vo.firstLevel}</td>
                <td>￥${vo.secondLevel}</td>
                <td>￥${vo.thirdLevel}</td>
                <td>￥${vo.fourthLevel}</td>
                <td>￥<#if vo.companyLocalTown??>${vo.companyLocalTown.total}<#else>0</#if></td>
                <td>￥<#if vo.companyLocalCountry??>${vo.companyLocalCountry.total}<#else>0</#if></td>
                <td>￥<#if vo.companyForeignTown??>${vo.companyForeignTown.total}<#else>0</#if></td>
                <td>￥<#if vo.companyForeignCountry??>${vo.companyForeignCountry.total}<#else>0</#if></td>
                <td>￥<#if vo.personalLocalTown??>${vo.personalLocalTown.total}<#else>0</#if></td>
                <td>￥<#if vo.personalLocalCountry??>${vo.personalLocalCountry.total}<#else>0</#if></td>
                <td>￥<#if vo.personalForeignTown??>${vo.personalForeignTown.total}<#else>0</#if></td>
                <td>￥<#if vo.personalForeignCountry??>${vo.personalForeignCountry.total}<#else>0</#if></td>
                <td>
                    <a data-toggle="tooltip" title="修改" data-placement="top" class="btn btn-xs"
                       href="/salary/setting/editFull/${vo.cityId}" >
                        <i class="fa fa-edit fa-lg operation-icon"></i>
                    </a>
                </td>
            </tr>
            </#list>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/salary/setting/list"/>
    </#if>
    </div>
</div>
<script>document.title = '自建配送员薪酬设置';
</script>
