<#-- 加载全局配置文件 -->
<#include "../config.ftl">

<title>phome管理</title>
<content tag="cssmain">/partner/phome.css</content>
<content tag="javascript">/partner/phome.js</content>
<#include "../widgets/sidebar.ftl">
<div id="main-container">

	<div class="home_head">
		<span id="home_name"></span>
		<span class="hidden">骑手(<span id="home_rider_count"></span>)</span>
		<span class="hidden">商家(<span id="home_poi_count"></span>)</span>
		<div class="pull-right select-right">
			<select id="selectOrg" class="input-sm" style="width:160px;padding:0;opacity:0"></select>
    </div>
  </div>

    <div class="row" style="margin-top:20px;">
      <div class="col-md-4 padding_lr_5">
        <div class="panel-stat3 double_row">
        <div class="left-msgs">
          <div class="home_panel_head">
            <span class="home_panel_title">今日待办事项</span>
          </div>
          <div class="home_panel_content">

            <div id="abnormal_waybill" class="hidden">
              <span>有<span class="abnormal_count">0</span>条异常订单</span>
              <a class="pull-right" href="">去处理&nbsp;&gt;</a>
            </div>
            <div id="notification">
              <div class="noti_head">
                  <span>有<span class="abnormal_count">0</span>条未读通知</span>
                <a class="pull-right" href="/msg/r/list">去阅读&nbsp;&gt;</a>
              </div>
            </div>
            <div id="auditMsg" class="hidden">
              <p class="section-title">审核消息</p>  
            </div>

          </div>
        </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="row">
          <div class="col-md-6 padding_lr_5">
            <div class="panel-stat3" id="dispatch_module">
              <div class="home_panel_head">
                <span class="home_panel_title">站点实时监控</span>
                <a class="home_panel_link hidden" href="http://dispatch.peisong.meituan.com/partner/dispatch/home">调度控制台&nbsp;&gt;</a>
              </div>
  
              <div class="home_panel_content">
              <div class="home_panel_left">
              
                <p class="home_line_info"><span class="pull-right">需手工派单</span></p>
                <p class="home_line_info"><span class="pull-right">新订单</span></p>
                <p class="home_line_info"><span class="pull-right">未完成订单</span></p>
                <p class="home_line_info"><span class="pull-right">骑手开工/收工/忙碌</span></p>
              </div>
              <div class="home_panel_right">
                <p class="home_line_info"><span id="dis_manul_count" class="pull-left home_count_span">0</span></p>
                <p class="home_line_info"><span id="dis_new_count" class="pull-left home_count_span">0</span></p>
                <p class="home_line_info"><span id="dis_notcom_count" class="pull-left home_count_span">0</span></p>
                <p class="home_line_info"><span id="dis_rider_counts" class="pull-left home_count_span">0/0/0</span></p>
              </div>
              </div>

            </div>
          </div>
          <div class="col-md-6 padding_lr_5">
            <div class="grey-panel panel-stat3">
                <div class="home_panel_head">
                  <span class="home_panel_title">日历</span>
                </div>
                <div id="weather"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 padding_lr_5 margin_top_10">
            <div class="panel-stat3" id="data_module">
              <div class="home_panel_head">
                <span class="home_panel_title">昨日运营情况</span>
                <a class="home_panel_link" target="_blank">运营指标监测&nbsp;&gt;</a>
              </div>
              <div class="home_panel_content" style="padding:20px 10px;">
              	    <div class="row">
              	    	<div id="riderEfficiency" class="border_bottom border_right home_data_height">
              	    		<p class="data_module_title">骑手人效</p>
              	    		<span class="home_data_count" data-toggle="tooltip" title="昨天的表现" data-placement="bottom">-</span>
              	    		<div class="home_data_rate home_data_left">
                          <p class="home_rate_num"><span>－</span><img class="hidden" src="/static/imgs/green-up.png" width="7px" height="16px"></p>
                          <span data-toggle="tooltip" title="昨天对比上月均值" data-placement="bottom">比上月</span>
              	    		</div>
              	    	</div>
              	    	<div id="completeWaybill" class="border_bottom home_data_height">
              	    		<p class="data_module_title">订单完成量</p>
              	    		<span class="home_data_count" data-toggle="tooltip" title="昨天的表现" data-placement="bottom">-</span>
              	    		<div class="home_data_rate">
                          <p class="home_rate_num"><span>－</span><img class="hidden" src="/static/imgs/green-up.png" width="7px" height="16px"></p>
                          <span data-toggle="tooltip" title="昨天对比上月均值" data-placement="bottom">比上月</span>
              	    		</div>
              	    	</div>
              	    </div>
              	    <div class="row">
              	    	<div id="incidentRatio" class="border_right home_data_height">
              	    		<p class="data_module_title">超时单率</p>
              	    		<span class="home_data_count" data-toggle="tooltip" title="昨天的表现" data-placement="bottom">-</span>
              	    		<div class="home_data_rate home_data_left">
                          <p class="home_rate_num"><span>－</span>
                          <img class="hidden" src="/static/imgs/green-up.png" width="7px" height="16px">
                          </p>
                          <span data-toggle="tooltip" title="昨天对比上月均值" data-placement="bottom">比上月</span>
              	    		</div>
              	    	</div>
              	    	<div id="completeRatio" class="home_data_height">
              	    		<p class="data_module_title">完成单率</p>
              	    		<span class="home_data_count" data-toggle="tooltip" title="昨天的表现" data-placement="bottom">-</span>
              	    		<div class="home_data_rate">
                          <p class="home_rate_num"><span>－</span><img class="hidden" src="/static/imgs/green-up.png" width="7px" height="16px"></p>
              	    			<span data-toggle="tooltip" title="昨天对比上月均值" data-placement="bottom">比上月</span>
              	    		</div>
              	    	</div>
              	    </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 padding_lr_5 margin_top_10">
            <div class="panel-stat3">
              <div class="home_panel_head">
                <span class="home_panel_title">常见问题自助</span>
              </div>
              <div class="home_panel_content">
              <div class="home_btn_group">
                <ul class="nav" role="tablist">
                    <li class="active">
                        <a href="#commonProblem" role="tab" data-toggle="tab" class="col-md-6 home_btn home_btn_left">常见问题</a>
                    </li>
                    <li>
                        <a href="#systemProblem" class="col-md-6 home_btn home_btn_right" role="tab" data-toggle="tab">系统操作指南</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="commonProblem">
                        <div class="problem_title">
                            <a class="" target="_blank" href="/faq/faqTypeAndInfo/1">全部</a>
                        </div>
                        <div class="problem_main">
                            <div class="problem_main_li">
                                <span>烽火台</span><a target="_blank" href="/faq/faqTypeAndInfo/1?faqType=895"> &nbsp;&gt; </a>
                            </div>
                            <div class="problem_main_li">
                                <span>骑手APP</span><a target="_blank" href="/faq/faqTypeAndInfo/1?faqType=897"> &nbsp;&gt; </a>
                            </div>
                            <div class="problem_main_li">
                                <span>调度相关</span><a target="_blank" href="/faq/faqTypeAndInfo/1?faqType=899"> &nbsp;&gt; </a>
                            </div>
                            <div class="problem_main_li">
                                <span>人员相关</span><a target="_blank" href="/faq/faqTypeAndInfo/1?faqType=901"> &nbsp;&gt; </a>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="systemProblem">
                      <div class="problem_title">
                            <a class="" target="_blank" href="/faq/faqTypeAndInfo/2">全部</a>
                        </div>
                        <div class="problem_main">
                            <div class="problem_main_li">
                                <span>骑手送餐流程</span><a target="_blank" href="/faq/faqTypeAndInfo/2?faqType=905"> &nbsp;&gt; </a>

                            </div>
                            <div class="problem_main_li">
                                 <span>烽火台调度</span><a target="_blank" href="/faq/faqTypeAndInfo/2?faqType=907"> &nbsp;&gt; </a>
                            </div>
                            <div class="problem_main_li">
                                <span>烽火台管理</span><a target="_blank" href="/faq/faqTypeAndInfo/2?faqType=908"> &nbsp;&gt; </a>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
	var home_riderCount = 0;
	<#if riderCount??>
    	home_riderCount = "${riderCount}";
	</#if>
	var home_poiCount = 0;
	<#if poiCount??>
    	home_poiCount = "${poiCount}";
	</#if>

	var home_orgList = {};
	<#if (orgList??) && (orgList?size > 0) >
    <#list orgList as orgView>

    	var org = {};
    	org.orgId = "${orgView.orgId}";
    	org.name = "${orgView.orgName}";
    	
    	home_orgList["${orgView.orgId}"] = org;
    </#list>
	</#if>
    var orgSelect = $('#selectOrg');
    var mySelect2 = $.fn.select2;
</script>
