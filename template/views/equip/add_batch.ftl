<#assign cssmain="/equip/add_batch.css">
<#include "../../decorators/header.inc">

<#include "config.ftl">
<#include "../widgets/sidebar.ftl">


<content tag="cssmain">/equip/add_batch.css</content>
<content tag="javascript">/equip/add_batch.js</content>


<#assign javascript="/equip/add_batch.js">



<style type="text/css">
    

</style>

<div id="main-container">
    <!-- 物资信息panel -->
    <div class="panel panel-default table-responsive">
        <div class="form-horizontal">
            <div class="panel panel-default" style="margin-bottom: 0;">
                <div class="panel-heading" style="background-color: #fafafa;">
                    <label class="panel-title" style="font-size:16px;">物资信息</label>
                </div>
                <div class="panel-body">
                    <div class="form-group col-md-7">
                        <label class="" style="font-size: 14px;">R端展示方案：</label>
                        <textarea id="showMethod" name="orgIds" class="form-control" rows="3"></textarea>
                    </div>
                    <div class="form-group col-md-7">
                        <label class=""  style="font-size: 14px;">领取期限：</label>
                        <div class="after-text">
                            <input type="text" class="form-control input-sm parsley-validated" id="deadline" name="userPhone" data-required="true" data-minlength="8">
                            <span>天</span>
                        </div>
                    </div>

                    <div class="form-group col-md-7 date-start-end">
                        <div class="date-container">
                            <label class="" style="font-size: 14px;">上线日期</label>
                            <div class="form-inline date-start-end">
                                <input type="text" name="ts" style="cursor: pointer;" value="<#if ts?exists>${ts}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start"/>
                            </div>
                        </div>
                        <span style="">~</span>
                        <div class="date-container">
                            <label class="" style="font-size: 14px;">下线日期</label>
                            <div class="form-inline date-start-end">
                                <input type="text" name="te" style="cursor: pointer;" value="<#if te?exists>${te}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 物资信息panel -->

    <!-- 申领条件panel -->
    <div class="panel panel-default table-responsive">
        <div class="form-horizontal">
            <div class="panel panel-default" style="margin-bottom: 0;">
                <div class="panel-heading" style="background-color: #fafafa;">
                    <label class="panel-title" style="font-size:16px;">申领条件</label>
                </div>
                <div class="panel-body">
                    <div class="form-group col-md-7">
                        <label class=""  style="font-size: 14px;">审核通过订单量（必填）</label>
                        <div class="after-text">
                            <input type="text" class="form-control input-sm parsley-validated" id="userPhone" name="userPhone" data-required="true" data-minlength="8">
                            <span>单</span>
                        </div>
                    </div>

                    <div class="form-group col-md-7 date-start-end">
                        <div class="date-container">
                            <label class="" style="font-size: 14px;">开始时间</label>
                            <div class="form-inline date-start-end">
                                <input type="text" name="ts" style="cursor: pointer;" placeholder="开始"
                                class="form-control input-sm date-picker J-datepicker js_date_end"/>
                            </div>
                        </div>
                        <span style="">~</span>
                        <div class="date-container">
                            <label class="" style="font-size: 14px;">截止时间</label>
                            <div class="form-inline date-start-end">
                                <input type="text" name="te" style="cursor: pointer;" placeholder="至今" class="form-control input-sm date-picker J-datepicker js_date_end"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-7">
                        <label class=""  style="font-size: 14px;">接单完成率</label>
                        <div class="after-text">
                            <input type="text" class="form-control input-sm parsley-validated" id="userPhone" name="userPhone" data-required="true" data-minlength="8">
                            <span>%</span>
                        </div>
                    </div>

                    <div class="form-group col-md-7">
                        <label class=""  style="font-size: 14px;">投诉订单数</label>
                        <div class="after-text">
                            <input type="text" class="form-control input-sm parsley-validated" id="userPhone" name="userPhone" data-required="true" data-minlength="8">
                            <span>单</span>
                        </div>
                    </div>

                    <div class="form-group col-md-7">
                        <label class=""  style="font-size: 14px;">综合评分</label>
                        <div class="after-text">
                            <input type="text" class="form-control input-sm parsley-validated" id="userPhone" name="userPhone" data-required="true" data-minlength="8">
                        </div>
                    </div>

                     <div class="form-group col-md-7" style="margin-top: 20px;">
                        <div class="col-md-6 pl0" style="line-height: 34px;">
                            <label class=""  style="font-size: 14px;">满足条件人数：</label>
                            <span id="satisfy-people">- -</span>
                        </div>
                        <div class="col-md-6 btn-container">
                            <button id="compute" class="btn btn-default">
                                计算
                            </button>
                            <button id="export" class="btn btn-success">
                                导出
                            </button>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 申领条件panel -->

    <!-- 物资分配panel -->
    <div class="panel panel-default table-responsive">
        <div class="form-horizontal">
            <div class="panel panel-default" style="margin-bottom: 0;">
                <div class="panel-heading" style="background-color: #fafafa;">
                    <label class="panel-title" style="font-size:16px;">物资分配</label>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="normal">
                            <div style="overflow-x:auto">
                                <table class="table table-striped" style="vertical-align: middle;">
                                    <thead>
                                        <tr>
                                            <th>城市</th>
                                            <th>领取地址</th>
                                            <th>数量</th>
                                            <th>联系人</th>
                                            <th>联系电话</th>
                                            <th>其他</th>
                                            <th class="text-center">操作</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody-grant-equip-list">
                                        
                                    </tbody>
                                </table>
                                <div class="pager-wrap col-md-12">
                                    <div class="pagination col-md-8" style="text-align: right;"></div>
                                </div>
                                <div>
                                    <button id="compute" class="btn btn-default"data-toggle="modal" data-target="#addStation">
                                        添加站点
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 物资分配panel -->

    <div>
        <button id="compute" class="btn btn-default">
            保存
        </button>
    </div>
</div>


<#-- 弹出确认层 -->
<div class="modal fade" id="addStation">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4>添加站点</h4>
            </div>
            <div class="modal-body">
                <div class="row clearfix" style="">
                    <label class="col-sm-2 control-label">城市</label>
                    <select class="input-sm" id="cityId" name="cityId" style="width: 58.33333333%;">
                    </select>
                </div>

                <div class="row clearfix" style="">
                    <label class="col-sm-2 control-label">地址</label>
                    <input type="text" class="col-sm-7 input-sm" id="add_address" value="${mobile!}">
                </div>

                <div class="row clearfix" style="">
                    <label class="col-sm-2 control-label">联系人</label>
                    <input type="text" class="col-sm-7 input-sm" id="add_contact" value="${mobile!}">
                </div>

                <div class="row clearfix" style="">
                    <label class="col-sm-2 control-label">电话</label>
                    <input type="text" class="col-sm-7 input-sm" id="add_phone" value="${mobile!}">
                </div>

                <div class="row clearfix" style="">
                    <label class="col-sm-2 control-label">数量</label>
                    <input type="text" class="col-sm-7 input-sm" id="add_amount" value="${mobile!}">
                </div>

                <div class="row clearfix" style="">
                    <label class="col-sm-2 control-label">备注</label>
                    <div class="col-sm-7" style="padding: 0;">
                        <textarea id="add_memo" class="form-control" rows="3"></textarea>
                    </div>
                    
                </div>
                <div class="modal-footer" style="text-align: center">
                    <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true" style="width:60px;">确定</button>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" style="width:60px;">取消</button>
                </div>
            </div>
        </div>
    </div>
</div>



<#include "../../decorators/footer.inc">