<#include "config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="cssmain">/equip/list_ver0d111a4.css</content>
<content tag="javascript">/equip/list_ver0d111a4.js</content>

<div id="main-container">
    <div class="panel panel-default">
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="<#if type == 1 >active</#if>" id="all-poi-type">
                    <a href="#normal" id="wait_for_fetch" aria-controls="normal" role="tab" data-toggle="tab">待领取</a>
                </li>
                <li role="presentation"  class="<#if type == 2 >active</#if>" id="proxy-poi-type">
                    <a href="#normal" id="fetched" aria-controls="normal" role="tab" data-toggle="tab">已领取</a>
                </li>
            </ul>
            <div style="width: 100%;height: 2px;background: #58B7B1;"></div>

            <div class="row" style="margin-top: 20px;padding-left: 0px;">
                <div class="form-horizontal" role="form">
                    <div class="col-md-12">
                        <div class="col-md-4" style="padding-left: 0px">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="s_mobile" value="${mobile!}" placeholder="请输入手机号">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" style="padding-left: 0px">
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-4" style="margin-left: 10px;">
                                    <button id="search" class="btn btn-default"
                                            style="width: 100px;background-color: #58B7B1;color: white;">搜索
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="normal">
                    <div style="overflow-x:auto">
                    <table class="table" style="vertical-align: middle;min-width:1000px;">
                        <thead>
                            <tr>
                                <th width="100px">姓名</th>
                                <th width="150px">手机号</th>
                                <th width="200px">身份证号</th>
                                <th width="100px">站点</th>
                                <th width="100px">申请装备</th>
                                <th width="150px">申请时间</th>
                                <#if type == 1 >
                                    <th  class="text-center" width="100px">操作</th>
                                <#elseif type == 2 >
                                    <th  width="300px">操作记录</th>
                                <#else>
                                    <th width="100px">未知状态</th>
                                </#if>
                            </tr>
                        </thead>
                        <tbody id="poi-table">
                            <#if page.equipApplyViews??>
                                <#list page.equipApplyViews as ap>
                                    <#assign ctime_stamp=ap.ctime*1000 />
                                    <#assign utime_stamp=ap.utime*1000 />
                                    <tr>
                                        <td>${ap.bmUserName !''}</td>
                                        <td>${ap.mobile !''}</td>
                                        <td>${ap.cardNo !''}</td>
                                        <td>${ap.stationName !''}</td>
                                        <td>${ap.equipName !''}</td>
                                        <td>${ctime_stamp?number_to_datetime}</td>
                                        <td>
                                            <#if type == 1 >
                                                <input rel="${ap.bmEquipApplyId!}" type="button" value="确认领取" style="background-color: #58B7B1;color:#FFFFFF" class="fetch btn js_submitbtn" />
                                            <#elseif type == 2 >
                                                操作人: ${ap.opUserName!}<br/>
                                                领取时间:${utime_stamp?number_to_datetime}
                                            <#else>
                                                未知状态
                                            </#if>
                                            
                                        </td>
                                    </tr>
                                </#list>
                            </#if>
                        </tbody>
                    </table>


                    </div>
                </div>
            </div>
            <#import "../page/pager.ftl" as q>
            <#if page.total??>
                <@q.pager pageNo=page.pageNum pageSize=page.pageSize recordCount=page.total toURL="/partner/equip/list?type=${type!}&mobile=${mobile!}"/>
            </#if>
            </ul>
        </div>
    </div>
</div>

<#-- 弹出确认层 -->
<div class="modal fade" id="showConfirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="showConfirmTitle" style="border-bottom: 1px solid #F5F5F5">提示</h4>
                <div class="row clearfix" style="height:70px;line-height:70px"> 
                    <div class="text-center col-md-12 column" style="font-size:12pt">
                        确定配送员已经领取装备？
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" value="确定" class="btn btn-info btn-sm" id="confirm" style="width:60px;"/>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" style="width:60px;">取消</button>
                </div>
            </div>
        </div>
    </div>
</div>

<form method="post" id="params-form" action='/partner/equip/list'>
    <input type="hidden" name="type" value="${type!}"/>
    <input type="hidden" name="mobile" value="${mobile!}"/>
    <input type="hidden" name="pageNum" value="${pageNum!}" >
    <input type="hidden" name="pageSize" value="${pageSize!}" >
</form>

<script>
    document.title = '装备发放';
</script>
