<#assign cssmain="/equip/add_material.css">
<#include "../../decorators/header.inc">

<#include "config.ftl">
<#include "../widgets/sidebar.ftl">


<content tag="cssmain">/equip/add_material.css</content>
<content tag="javascript">/equip/add_material.js</content>


<#assign javascript="/equip/add_material.js">



<style type="text/css">
    .row {
        margin: 15px 0;
    }


    .row label {
        line-height: 30px;
        font-size: 15px;
        padding-right: 0;
    }

    .upload-img {
        display: -webkit-box;
    }

    .upload-img .img-area {
        display: -webkit-box;
        -webkit-box-pack: center;
        -webkit-box-align:center;
        background-color: white;
        font-size: 16px;
    }

    .upload-img .img-action {
        margin-left: 20px;
        display: -webkit-box;
        -webkit-box-orient: vertical;
    }
</style>

<div id="main-container">
    <div class="container">
        <div class="row col-sm-10" style="">
        <label class="col-sm-2 control-label">物资名称</label>
        <input type="text" class="col-sm-7 input-sm" id="add_amount" value="${mobile!}">
    </div>

    <div class="row col-sm-10" style="">
        <label class="col-sm-2 control-label">物资介绍</label>
        <div class="col-sm-7" style="padding: 0;">
            <textarea id="add_memo" class="form-control" rows="3"></textarea>
        </div>
    </div>

     <div class="row col-sm-10" style="">
        <label class="col-sm-2 control-label">上传图片</label>
        <div class="col-sm-7 upload-img" style="padding: 0;">
            <div id="img-preview" style="width: 200px; height: 150px" class="img-area">
                暂未上传图片
            </div>
            <div class="img-action">
                <div style="margin-top: 10px; line-height: 16px;">
                    上传图片要求:<br>
                    1. 尺寸：720*490px<br>
                    2. 格式：jpg、png
                </div>
                <div style="position: absolute; bottom: 0;">
                    <button id="upload-img-btn" class="btn btn-default" style="width: 100px;">
                        上传
                    </button>
                    <input type="file" class="js_fileupload" name="imgFile" style="display: none"/>
                </div>
            </div>
        </div>
    </div>

    <div class="row col-sm-10">
        <div class="col-sm-2 ctn">
            <button id="save" class="btn btn-default" style="width: 100px;">
                保存
            </button>
        </div>
    </div>
    </div>
</div>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.fileupload.js"></script>
<#include "../../decorators/footer.inc">
