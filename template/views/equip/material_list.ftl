<#include "config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="cssmain">/equip/material_list.css</content>
<content tag="javascript">/equip/material_list.js</content>

<style>
    #main-container{
        padding: 10px 20px;
    }

    .table-action-bar {
        margin: 15px 0;
        padding-left: 0px;
        text-align: right;
    }

    .table-action-bar .btn {
        width: 100px;
        background-color: #58B7B1;
        color: white;
    }

    .amount {
        height: 32px;
        line-height: 32px;
        padding-left: 0;
    }

    .pagination {
        margin: 0;
    }

    .pagination > ul {
        margin: 0;
    }

</style>

<div id="main-container">
    <div class="panel panel-default">
        <div>
            <div class="table-action-bar">
                <a class="btn btn-default" href="/views/equip/add_material.ftl">
                    添加物资
                </a>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="normal">
                    <div style="overflow-x:auto">
                        <table class="table table-striped" style="vertical-align: middle;min-width:1000px;">
                            <thead>
                                <tr>
                                    <th>物资名称</th>
                                    <th>库存</th>
                                    <th>已领数量</th>
                                    <th>状态</th>
                                    <th>发放批次</th>
                                    <th style="text-align: center">操作</th>
                                </tr>
                            </thead>
                            <tbody id="tbody-grant-equip-list">
                                
                            </tbody>
                        </table>
                        <div class="pager-wrap col-md-12">
                            <div class="amount col-md-4">
                                总计：123条    
                            </div>
                            <ul class="pagination pagination-xs pull-right"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

<#-- 弹出确认层 -->
<div class="modal fade" id="showConfirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="showConfirmTitle" style="border-bottom: 1px solid #F5F5F5">提示</h4>
                <div class="row clearfix" style="height:70px;line-height:70px">
                    <div class="text-center col-md-12 column" style="font-size:12pt">
                        确定配送员已经领取装备？
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" value="确定" class="btn btn-info btn-sm" id="confirm" style="width:60px;"/>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" style="width:60px;">取消</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.title = '物资管理';
</script>

<script type='text/template' id="listTemplate">
  <tr>
    <td class="t_name">{name}</td>
    <td class="t_total">{total}</td>
    <td class="t_receive">{receive}</td>
    <td class="t_status">{status}</td>
    <td class="t_batch">{equipName}</td>
    <td class="t_operate text-center">
        <a data-toggle="tooltip" title="发放详情" data-placement="top" href="/views/equip/add_material.ftl?id={id}" target="_blank">
            <i class="fa fa-list-ul fa-lg opration-icon"></i>
        </a>&nbsp;&nbsp;
        <a data-toggle="tooltip" title="修改信息" data-placement="top" href="/views/equip/add_material.ftl?id={id}" target="_blank"
            style="display: {isShow}">
            <i class="fa fa-edit fa-lg opration-icon"></i>
        </a>&nbsp;&nbsp;
        <a data-toggle="tooltip" title="操作日志" data-placement="top" href="/views/equip/add_material.ftl?id={id}" target="_blank">
            <i class="fa fa-history fa-lg opration-icon"></i>
        </a>
    </td>
  </tr>
</script>

<script type="text/javascript" src="/static/js/lib/jqueryPlugs/jquery.twbsPagination.min.js"></script>
<script type="text/javascript" src="http://xs01.meituan.net/cdn/moment/2.10.6/moment.min.js"></script>