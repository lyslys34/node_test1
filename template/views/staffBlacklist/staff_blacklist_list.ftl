<#include "blacklist_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/staffBlacklist/blacklist.js</content>
<content tag="cssmain">/staffBlacklist/blacklist.css</content>
<div id="main-container">
    <h4 style="padding-left:15px;">黑名单系统</h4>
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate" data-validate="parsley" novalidate="" action="/rider/getBlacklistList" method="get" style="padding-bottom:0px;">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6" style="float:right;width:350px;padding-right:40px;" >
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="输入手机号搜索" name="phone" value="${phone!''}" id="phoneSearch">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" id="search">查询</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom:10px">
                            <input type="button" value="拉黑配送员" id="create" class="btn btn-danger js_submitbtn" style="float:left;margin-left:15px;"/>

                            <button type="submit" class="btn btn-default js_submitbtn" id="selectBlacklist" style="float:right;margin-right:40px;">筛选</button>
                            <div class="form-group" style="width:200px;float:right;margin-right:20px;">
                                <input type="text" class="form-control input-sm parsley-validated js_city_name" name="cityName" id="cityName" data-minlength="8" value="" placeholder="全部城市"/>
                                <input type="hidden" name="cityId" id="cityId" class="js_city_id" value="${cityId!""}"/>
                            </div>
                            <div class="form-group" style="float:right;margin-right:20px;">
                                <input type="text" name="beginTimeStr" style="cursor: pointer;width:auto" readonly="readonly" value="<#if beginTimeStr?exists>${beginTimeStr}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start" placeholder="全部日期" />
                            </div>
                </div>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <p style="margin:5px 20px;">共${recordCount!'0'}条记录</p>
        <#if staffBlacklists?exists && (staffBlacklists?size>0)>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>姓名</th>
                        <th>手机号</th>
                        <th>城市</th>
                        <th>拉黑原因</th>
                        <th>拉黑日期</th>
                        <th>拉黑时限</th>
                        <th>操作人</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody id="tbody-dispatcher-list">
                <#list staffBlacklists as b>
                    <tr>
                        <td>${b.bmUserName!''}</td>
                        <td>${b.phone!''}</td>
                        <td>${b.cityname!''}</td>
                        <td>${b.opDescription!''}</td>
                        <td>${b.ctime!''}</td>
                        <td><#if b.dayLimit==-1>永久拉黑<#else>${b.dayLimit!'0'}天</#if></td>
                        <td>${b.opUserName!''}</td>
                        <td>
                            <input type="button" value="移出黑名单"  class="btn btn-primary js_submitbtn del" style="padding:0px;margin:0px;width:120px;height:24px;background:#337ab7" delId="${b.bmStaffBlacklistId!''}" delUserId="${b.bmUserid!''}" bmName="${b.bmUserName!''}" bmPhone="${b.phone!''}"/>
                        </td>
                    </tr>
                </#list>
                </tbody>
            </table>
            <#import "../page/pager.ftl" as q>
            <#if recordCount??>
                <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/rider/getBlacklistList"/>
            </#if>
        </#if>

    </div>

    <#-- 弹出确认层 -->
    <div class="modal fade" id="showConfirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 id="showConfirmTitle"></h4>
                    <div class="modal-footer">
                        <input type="submit" value="确定" class="btn btn-info btn-sm" id="confirm" style="width:60px;"/>
                        <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" style="width:60px;">再想想</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--弹出返回提示层-->
    <div class="modal fade" id="showReturnRes">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="alert_msg" style="text-align:center">

                </div>
            </div>
        </div>
    </div>

    <#-- 拉黑配送员 -->
    <div id="create-container" class="hide">
        <div class="panel-heading">
            <span>拉黑配送员</span>
        </div>
        <input type="hidden" name="id" id="id" value="0" />
        <div id="edit-container">
            <div class="row" style="margin:10px 0px;;width:450px;">
                <div class="form-group" style="margin:5px 0px;">
                    <label  class="control-label" >搜索配送员</label>
                </div>
                <div class="col-lg-6" style="width:440px;margin-left:20px;" >
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="输入手机号搜索" name="phone" id="onlyPhoneSearch">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" id="selectPhone">查询</button>
                        </span>
                    </div>
                </div>
            </div>
            <!--展示用户的身份及拉黑信息，如果没有此人则显示showMsg，否则显示下面的showDiv -->
            <p style="color:red;margin-left:35px;" id="showMsg"></p>
            <div id="showDiv1" style="width:450px;height:auto;">
            </div>
            <!--显示拉黑状态的提示，如果处于拉黑状态，则显示，否则显示下面的div -->
            <p style="color:red" id="showStausMsg"></p>
            <div style="width:450px;height:auto; margin:10px 0px;" id="showDiv2">
            </div>
        </div>
    </div>
</div>

<script>
    document.title = '黑名单系统';
    $('select').select2();
</script>