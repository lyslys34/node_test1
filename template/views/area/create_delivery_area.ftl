<#include "area_config.ftl">
<#include "../widgets/sidebar.ftl">
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=820afe3932ebea15d73cd05b2a28e606"></script>

<link rel="stylesheet" type="text/css" href="${staticPath}/css/lib/touchspin/jquery.bootstrap-touchspin.min.css" />
<link rel="stylesheet" type="text/css" href="${staticPath}/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />
<content tag="cssmain">/area/create_delivery_area.css</content>

<script type="text/javascript" src="${staticPath}/js/lib/touchspin/jquery.bootstrap-touchspin.min.js"></script>


<script type="text/javascript">
    // TODO 先将页面的类型（主要是角色相关的权限类型）注入到页面
    window.pageType = ${type};
</script>

<content tag="javascript">/area/area_map.js</content>

<style type="text/css">
  #main-container {
    min-height: 1280px;
  }
</style>
<div id="main-container" >

    <#if vo??><h3>${vo.deliveryAreaName!''}(id:${vo.id!''})</h3></#if>


    <div class="panel-heading" <#if !vo??>style="display:none"</#if> <#if is_org_manager?exists && is_org_manager == 1> hidden="hidden" </#if>>
        <ul class="nav nav-tabs" id="commonMenu">
            <li  <#if tipName?? && tipName == 'baseArea'> class="active"</#if> value="baseArea"><a href="#baseArea">区域信息<span class="hidden">0</span></a></li>
            <#if !is_org_manager?exists || is_org_manager != 1>
                <li <#if tipName?? && tipName == 'areaOrg'> class="active"</#if> value="areaOrg"><a href="#areaOrg">站点列表<span class="hidden" >(0)</span></a></li>
                <li <#if tipName?? && tipName == 'areaPoi'> class="active"</#if> value="areaPoi"><a href="#areaPoi">服务商家列表<span class="hidden">(0)</span></a></li>
                <li <#if tipName?? && tipName == 'areaOPLog'> class="active"</#if> value="areaOPLog"><a href="#areaOPLog">操作记录<span class="hidden">(0)</span></a></li>
            </#if>
        </ul>
    </div>


    <div id="alert_error"></div>
    <div id="tips">
        <div id="baseArea" style="display:none">
            <div id="tip" class="fixed" >
                <input type="hidden" id="tipName" value="${tipName!''}">
                <input type="hidden" id="area" name="area" >
                <input type="hidden" id="shippingArea" name="shippingArea" >
                <input type="hidden" id="id" name="id" value="<#if vo??>${vo.id}</#if>">
                <input type="hidden" id="originArea" name="originArea" value="<#if vo??>${vo.deliveryAreaCoordinates!''}</#if>">
                <input type="hidden" id="initArea" name="initArea" value="<#if vo??>${vo.deliveryAreaCoordinates!''}</#if>">
                <input type="hidden" id="initShippingArea" name="initShippingArea" value="<#if vo??>${vo.shippingAreaCoordinates!''}</#if>">
                <input type="hidden" id="allowChange" value="${allowChange!''}">
                <input type="hidden" id="countBindBmOrg" value="<#if vo??>${vo.countBindBmOrg!''}</#if>">
                <input type="hidden" id="isOrgManager" value="<#if vo??>${is_org_manager!''}</#if>">
                <input type="hidden" id="isSelfCreateShippingArea" name="isSelfCreateShippingArea" value="<#if vo?? && vo.shippingAreaCoordinates?? &&  vo.shippingAreaCoordinates!= ''>0<#else>1</#if>">
                <input type="hidden" id="initDispatchStrategy" name="initDispatchStrategy" value="<#if initDispatchStrategyVo??>${initDispatchStrategyVo.id}</#if>">

                <#if vo??><#if vo.id??><#if vo.checked == 1><#elseif vo.checked==2><div class="panel-header header-tip">驳回原因：${vo.rejectReason!''}</div><#else></#if></#if><#else><div class="panel-header header-tip">新建配送区域后将由总部审核，通过后立即生效</div></#if>

                <div class="panel panel-default table-responsive">
                    <div class="form-horizontal">
                        <div class="panel panel-default" style="margin-bottom: 0;">
                            <div class="panel-heading" style="background-color: #fafafa;">
                                <label class="panel-title" style="font-size:16px;">基本信息</label>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="inputGroupName" class="col-sm-2 control-label" style="font-size: 14px; line-height: 18px;">区域名称</label>
                                    <div class="col-sm-3">
                                        <input style="margin-left:10px; height: 31px;" <#if is_org_manager?exists && is_org_manager == 1> readonly </#if> type="text" <#if allowChange?exists && allowChange != 0>readonly</#if> value="<#if vo??>${vo.deliveryAreaName!''}</#if>" class="form-control parsley-validated" id="areaName" name="areaName" data-required="true" maxlength=16>
                                    </div>

                                    <div class="" style="display:none;">
                                        <select <#if is_org_manager?exists && is_org_manager == 1> disabled='disabled' </#if> <#if allowChange?exists && allowChange != 0>disabled='disabled'</#if> <#if orgType??><#if orgType!=0><#if type??><#if type!=0>disabled='disabled'</#if></#if></#if></#if> <#if vo?? && (vo.countBindBmOrg>0) > disabled='disabled' </#if> class="form-control input-sm" id="type" name="type">
                                        <!-- <select class="form-control input-sm" id="type" name="type"> -->
                                            <option value="0" >请选择区域类型</option>
                                            <option <#if type??><#if type=1>selected</#if></#if> value="1" rel="<#if orgInitDispatchStrategy??>${orgInitDispatchStrategy['1']!''}</#if>">自建</option>
                                            <option <#if type??><#if type=2>selected</#if></#if> value="2"  rel="<#if orgInitDispatchStrategy??>${orgInitDispatchStrategy['2']!''}</#if>">加盟</option>
                                            <option <#if type??><#if type=4>selected</#if></#if> value="4"  rel="<#if orgInitDispatchStrategy??>${orgInitDispatchStrategy['4']!''}</#if>">众包</option>
                                            <option <#if type??><#if type=7>selected</#if></#if> value="7"  rel="<#if orgInitDispatchStrategy??>${orgInitDispatchStrategy['7']!''}</#if>">城市代理</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="area_type" class="form-group">
                                  <label for="inputGroupDesc" class="col-sm-2 control-label"  style="font-size: 14px;">区域类型</label>
                                  <div class="col-sm-10">
                                    <label class="radio-inline" style="margin-left:10px;">
                                      <input type="radio" name="inlineRadioOptions" value="1"> 自建
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="inlineRadioOptions" value="2"> 加盟
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="inlineRadioOptions" value="4"> 众包
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="inlineRadioOptions" value="7"> 城市代理
                                    </label>
                                  </div>
                                </div>

                                <div class="form-group" style="margin-bottom: 0;">
                                    <!--
                                    <label for="inputGroupName" class="col-sm-1 control-label">城市名称</label>
                                    -->
                                    <div class="col-sm-3" style="display: none;">
                                        <input <#if is_org_manager?exists && is_org_manager == 1> readonly </#if> type="text" <#if allowChange?exists && allowChange != 0>readonly</#if> value="<#if vo??>${vo.cityName!''}</#if>" class="form-control parsley-validated js_city_name" <#if vo?? && (vo.countBindBmOrg>0) > readonly </#if> id="cityName" name="cityName" data-required="true">
                                        <input type="hidden" id="cityId" name="cityId" class="js_city_id" value="<#if vo??>${vo.bmCityId!''}</#if>" />
                                    </div>
                                    <label for="inputGroupDesc" class="col-sm-2 control-label" style="font-size: 14px;">调度模式</label>
                                    <div  id="dispatch_mode" class="col-sm-10">
                                      <label class="radio-inline" style="margin-left:10px;">
                                        <input type="radio" name="inlineRadioOptionsD"  value="option1"> 推抢结合
                                      </label>
                                      <label class="radio-inline">
                                        <input type="radio" name="inlineRadioOptionsD"  value="option2"> 纯抢单
                                      </label>
                                      <label class="radio-inline">
                                        <input type="radio" name="inlineRadioOptionsD"  value="option3"> 人工分配
                                      </label>
                                      <label class="radio-inline">
                                        <input type="radio" name="inlineRadioOptionsD"  value="option3"> 众包调度
                                      </label>
                                    </div>
                                    <div class="col-sm-12" style="margin-top: 10px; opacity: 0.6;">
                                      <div class="col-sm-2"></div>
                                      <span><label style="margin-left:14px;" class="text-center">新绑定站点按此调度模式工作。绑定站点多于一个时，调度模式自动调整为“推抢结合”</label></span>
                                    </div>
                                    <div class="" style="display: none;">
                                        <select <#if allowChange?exists && allowChange != 0>disabled='disabled'</#if> class="form-control input-sm" id="dispatchStrategy" <#if vo?? && (vo.countBindBmOrg>1) >disabled='disabled'</#if> <#if vo?? && (vo.type != 2) && vo.type!=7 >disabled='disabled'</#if> name="dispatchStrategy">
                                            <option value="0" >请先选择区域类型</option>
                                            <#if initDispatchStrategyVo?? >
                                                <option value="${initDispatchStrategyVo.id !''}" selected>${initDispatchStrategyVo.name !''}</option>
                                            </#if>
                                        </select>
                                        <!-- <span><label class="text-center">此调度模式一期暂无法使用，请以站点调度模式为准</label></span>  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

                <input type="hidden" id="initType" value="${orgType!''}">

                <input type="hidden" id="intersect" name="intersect" value="${allowIntersect!''}" />
<!--
                    <div class="panel-footer text-center" style="background-color: #fff">
                        <div id="alert_error"></div>
                    </div> -->
                </div>

                    <div id="area-panel" class="panel panel-default table-responsive" style="margin-bottom: 64px;">
                        <div class="form-horizontal" >
                            <div class="panel panel-default">
                                <div class="panel-heading" style="background-color: #fafafa;">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="panel-title" style="font-size:16px;">配送区域<!-- <span><label class="text-center">(点击右侧按钮“画多边形”开始划，双击地图完成绘制)</label></span>--></label>
                                        </div>
                                        <div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="padding: 0 80px 0 20px;">
                              <div class="row row_user_custom">
                                  <div class = "col-md-4">
                                      <div class="input-group" id="searchResult" style="min-width: 304px;">

                                          <input <#if is_org_manager?exists && is_org_manager == 1> readonly </#if> type="text" <#if allowChange?exists && allowChange != 0>readonly</#if> value="<#if vo??>${vo.cityName!''}</#if>" class="form-control parsley-validated js_city_name" <#if vo?? && (vo.countBindBmOrg>0) > readonly </#if> id="cityName" name="cityName" data-required="true">
                                          <input type="hidden" id="cityId" name="cityId" class="js_city_id" value="<#if vo??>${vo.bmCityId!''}</#if>" />
                                          <div class="input-group-addon split-input">|</div>
                                          <input type="text" id="keyword" name="keyword" value=""  class="form-control"
                                           placeholder="输入目标地址搜索"/>
                                          <button id="searchPlace" type="button" class="form-control btn btn-default btn-sm">搜索</button>
                                          <div id="result1" style="display: none;" name="result1"></div>
                                      </div>
                                      <div id="result" class="modal fade">
                                          <div>选中节点坐标：</div><textarea style="margin-bottom:10px" class="form-control" rows="3" id="resultInfo"></textarea>
                                      </div>
                                  </div>

                                  <div id="explainTip" class="col-md-6">
                                    <div style="text-align:center">
                                      <label class="checkbox-inline" data-toggle="tooltip" data-placement="top">
                                        <input class="form-control area_type" type="checkbox" data-toggle="tooltip" data-placement="top" style="background-color: #ff6564;" checked value="-1"> 蜂窝
                                      </label>
                                      <label class="checkbox-inline" data-toggle="tooltip" data-placement="top">
                                        <input class="form-control area_type" type="checkbox" data-toggle="tooltip" data-placement="top"
                                        style="background-color: #f8c832;" checked value="1"> 自建
                                      </label>
                                      <label class="checkbox-inline" data-toggle="tooltip" data-placement="top">
                                        <input class="form-control area_type" type="checkbox" style="background-color: #06944b;" checked value="2"> 加盟
                                      </label>
                                      <label class="checkbox-inline" data-toggle="tooltip" data-placement="top">
                                        <input class="form-control area_type" type="checkbox" style="background-color: #b56fe7;" checked value="4"> 众包
                                      </label>
                                      <label class="checkbox-inline" data-toggle="tooltip" data-placement="top">
                                        <input class="form-control area_type" type="checkbox" style="background-color: #6c553c;" checked value="7"> 城市代理
                                      </label>
                                      <label class="checkbox-inline" data-toggle="tooltip" data-placement="top" style="margin-right:93px;">
                                        <input id="checkbox_show_reference_distance" class="form-control" type="checkbox" style="background-color: #888888;" > 参考范围
                                      </label>
                                      <input id="reference_distance" style="background:white;height:23px;" class="reference_distance" type="text" value="1.5">
                                      <label id="reference_distance_desc" data-toggle="tooltip" data-placement="top">
                                        公里
                                      </label>
                                      <span class="reference_desc">参考范围根据取件范围计算</span>
                                    </div>
                                  </div>

                                  <div id="areaLengthTip" class="col-md-2" style="display: none;">
                                      <div id="alert_area_length" class="label label-success"></div>
                                  </div>

                                  <div class="col-md-2" style="text-align: right;">
                                    <div>
                                        <#if !vo??><button id="B_init_edit_area" rel="0" type="button" class="btn btn-success btn-sm">开始绘制</button></#if>
                                        <button id="B_start_edit_area" rel="1" type="button" class="btn btn-danger btn-sm"
                                          <#if !vo??>style="display:none;"</#if>
                                          <#if allowChange?exists && allowChange != 0> style="display:none;"</#if>
                                          <#if is_org_manager?exists && is_org_manager == 1> style="display:none;"</#if>>
                                          分别编辑
                                        </button>

                                        <button id="B_refresh_edit_area" type="button" class="btn btn-default"
                                          <#if allowChange?exists && allowChange != 0> style="display:none;"</#if>
                                          <#if is_org_manager?exists && is_org_manager == 1> style="display:none;"</#if>
                                           btn-sm">
                                         重置
                                       </button>
                                    </div>
                                  </div>

                                  <div id="areaTypeCheckBox" class="col-md-3" style="display: none;">
                                      <div class="btn-group">
                                          <button type="button" class="btn btn-info btn-sm dropdown-toggle" style="display: ;"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">显示<span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                              <li><label class="checkbox-inline"><input type="checkbox" checked style="opacity:1;margin-top:2px" class="form-control area_type" value="1" checked>自建</label></li>
                                              <li><label class="checkbox-inline"><input type="checkbox" checked style="opacity:1;margin-top:2px" class="form-control area_type" value="2" checked>加盟</label></li>
                                              <li><label class="checkbox-inline"><input type="checkbox" checked style="opacity:1;margin-top:2px" class="form-control area_type" value="4" checked>众包</label></li>
                                              <li><label class="checkbox-inline"><input type="checkbox" checked style="opacity:1;margin-top:2px" class="form-control area_type" value="-1" checked>蜂窝</label></li>
                                              <li><label class="checkbox-inline"><input type="checkbox" checked style="opacity:1;margin-top:2px" class="form-control area_type" value="7" checked>城市代理</label></li>
                                          </ul>
                                          <#if !vo??><button id="B_init_edit_area" rel="0" type="button" class="btn btn-success btn-sm">新建取件范围</button></#if>
                                          <button id="B_start_edit_area" rel="1" type="button" class="btn btn-danger btn-sm" <#if !vo??>style="display:none;"</#if>>编辑送件范围</button>

                                          <button id="B_refresh_edit_area" type="button" class="btn btn-default btn-sm">重置</button>
                                          <#-- </#if> -->
                                          <!-- <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-animate" style="width: 102px;"><div class="bootstrap-switch-container" style="width: 150px; margin-left: 0px;"><span class="bootstrap-switch-handle-on bootstrap-switch-primary" style="width: 50px;">编辑送件范围</span><span class="bootstrap-switch-label" style="width: 50px;">&nbsp;</span><span class="bootstrap-switch-handle-off bootstrap-switch-default" style="width: 50px;">编辑取件范围</span><input type="checkbox" checked=""></div></div> -->
                                      </div>
                                  </div>
                              </div>

                              <div id="mapContainer">
                                <div id="map_draw_tip">
                                    <div id="resetZoomBtn" class="col-md-1">
                                    </div>
                                    <div class="col-md-11 areaExplainTip" >
                                        <span style="font-size:14px;">
                                          取件范围
                                          <span class="fetch-line"></span>
                                          已绘制：
                                          <span class="fetch-mile"></span>
                                          公里
                                          <span class="overrange-tip" style="display: none;">（建议8-12公里）</span>
                                          &nbsp;
                                          <span class="fetch-square-kilometre"></span>
                                          平方公里
                                        </span>
                                    </div>
                                    <div class="col-md-1 resetZoomBtnWidth">
                                    </div>
                                    <div class="col-md-11 areaExplainTip" >
                                        <span style="font-size:14px;">
                                          送件范围
                                          <span class="dispatch-line"></span>
                                          已绘制：
                                          <span class="dispatch-mile"></span>
                                          公里&nbsp;
                                          <span class="dispatch-square-kilometre"></span>
                                          平方公里
                                        </span>
                                    </div>
                                    <div id="reference_explain" hidden>
                                      <div class="col-md-1 resetZoomBtnWidth">
                                      </div>
                                      <div class="col-md-11 areaExplainTip" >
                                            <span style="font-size:14px;">
                                              参考范围
                                              <span class="reference-line"></span>
                                            </span>
                                      </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                         </div>
                    </div>
            </div>

        <div id="areaPoi" style="display:none">
    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
                <tr>
                    <th>商家POI</th>
                    <th>商家名称</th>
                    <th>商家地址</th>
                    <th>商家电话</th>
                    <th>商家负责BD</th>
                    <th>上下线</th>
                    <th>营业状态</th>
                </tr>
            </thead>
            <tbody id="merchantListBody">

            </tbody>
        </table>
    </div>
    <div id="areaPoiPager"></div>
    </div>

        <div id="areaOrg" style="display:none">
        <div class="panel panel-default table-responsive">
            <table class="table table-striped" id="responsiveTable">
                <thead>
                    <tr>
                        <th>站点ID</th>
                        <th>站点名称</th>
                        <th>站点类型</th>
                        <th>城市</th>
                        <th>加盟商名称</th>
                    </tr>
                </thead>
                <tbody id="areaOrgBody">

                </tbody>
            </table>
        </div>
        <div id="areaOrgPager"></div>
        </div>

        <div id="areaOPLog" style="display:none">

            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">操作类型</label>
                                <!--<div class="col-sm-2">-->
                                <select class="form-control input-sm" id="opType" name="opType">
                                    <option value="0" >全部</option>
                                    <#if opTypeList??>
                                    <#list opTypeList as opType >
                                    <option value="${opType.opId !''}" >${opType.comment !''}</option>
                                    </#list>
                                    </#if>
                                </select>
                                <!--</div>-->
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">变更内容（选填）</label>
                                <input type="text" class="form-control input-sm parsley-validated" name="opDesc" id="opDesc" data-minlength="8" value="" maxlength=16/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">操作人</label>
                                <input type="text" class="form-control input-sm parsley-validated" name="opUname" id="opUname" data-minlength="8" value="" maxlength=16/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">开始时间</label>
                                <input id="opStartTime" readonly type="text" name="opStartTime" style="cursor: pointer;"  value="" class="form-control input-sm date-picker J-datepicker">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">截止时间</label>
                                <input id="opEndTime" type="text" readonly name="opEndTime" style="cursor: pointer;"  value="" class="form-control input-sm date-picker J-datepicker">
                            </div>
                        </div>

                        <div class="col-md-1" style="padding-top:18px;">
                            <button id="opSearch" type="button" class="btn btn-success">查询</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default table-responsive">
                <table class="table table-striped" id="responsiveTable">
                    <thead>
                        <tr>
                            <th>序号</th>
                            <th>操作类型</th>
                            <th>变更内容</th>
                            <th>操作人</th>
                            <th>操作时间</th>
                        </tr>
                    </thead>
                    <tbody id="areaLogBody">

                    </tbody>
                </table>
            </div>
            <div id="areaOPLogPager"></div>
            </div>
    </div>

        <div class="modal fade" id="saveSuccessModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div id="alert_success_massage"></div>
                        <div class="text-center" >
                         <!-- <button type="button" class="btn btn-success" id="B_ok">确定</button> -->
                     </div>
                 </div>
             </div>

         </div>
     </div>

     <!-- 不再显示 -->
     <div class="modal fade" id="saveOverModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="alert_over_massage"></div>
                    <input type="hidden" id="save_id">
                    <input type="hidden" id="save_city_id">
                    <input type="hidden" id="save_area_name">
                    <input type="hidden" id="save_area">
                    <input type="hidden" id="save_type">
                    <input type="hidden" id="save_dispatchStrategy">
                    <input type="hidden" id="save_shipping_area">
                    <div style="color: red">建议将配送范围控制在8到12公里</div>
                    <div class="text-center" >
                     <button type="button" class="btn btn-success" id="B_continue_save">继续保存</button>
                     <button type="button" class="btn btn-success" id="B_cancel_save">取消</button>
                 </div>
             </div>
         </div>

     </div>
 </div>

 <div class="modal fade" id="saveOutAreaModal">
    <div class="modal-dialog" style="max-width: none;">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 style="color: red">下列商家被移出配送区域，请谨慎操作</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default table-responsive">
                    <table class="table table-striped" id="responsiveTable">
                        <thead>
                            <tr>
                                <th>商家POI</th>
                                <th>商家名称</th>
                            </tr>
                        </thead>
                        <tbody id="affectPoi">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="B_check_continue">确定</button>
                <button type="button" class="btn btn-normal" id="B_check_cancel">取消</button>
            </div>
        </div>

    </div>
</div>

<!-- 不再显示 -->
<div class="modal fade" id="editChangeCommentModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>请填写修改内容及原因</h4>
                <div id="alert_edit_change_comment_error"></div>
                <div class="form-group">
                    <textarea id="change_comment" style="height:150px;resize:none;" class="form-control " placeholder="请输入修改原因"></textarea>
                </div><br>
                <div class="modal-footer">
                    <button type="button" id="cancelChangeCommentSubmit" class="btn btn-success btn-sm">取消</button>
                    <button type="button" id="editChangeCommentSubmit" class="btn btn-danger btn-sm">提交</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="typeChangeIntersectModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4>区域类型更改确认</h4>
            </div>
            <div class="modal-body">
                当前范围与现存众包区域有重合，如需更改将重围当前已的范围，确定更改吗？
            </div>
            <div class="modal-footer">
                <button type="button" id="confirmTypeChangeIntersectRange" class="btn btn-success btn-sm">确定更改</button>
                <button type="button" id="cancelTypeChangeIntersectRange" class="btn btn-normal btn-sm">取消</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="commitIntersectModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>绘制范围重合提示</h4>
            </div>
            <div class="modal-body">
                  当前绘制####区域范围与现存%%%%区域有重合<br>
                  请修改后提交！
            </div>
            <div class="modal-footer">
                <button type="button" id="commitIntersectModalKnown" class="btn btn-success btn-sm">知道了</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="commitCityCheckModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>绘制范围重合提示</h4>
            </div>
            <div class="modal-body">
                城市名称未填写，请填写后提交！
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" id="commitCityCheckModalKnown" class="btn btn-success btn-sm">知道了</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="resetMapModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4>重置绘制区域确认提示</h4>
            </div>
            <div class="modal-body">
                  一旦重置当前绘制区域后不可恢复！
            </div>
            <div class="modal-footer">
                <button type="button" id="confirmResetMap" class="btn btn-success btn-sm ok">重置</button>
                <button type="button" id="cancelResetMap" class="btn btn-normal btn-sm cancel">取消</button>
            </div>
        </div>
    </div>
</div>


<div class="editFooter">
  <#if allowChange?exists && allowChange == 0>
    <#if !is_org_manager?exists || is_org_manager != 1 || type == 2 || type == 7>
      <button type="button" class="btn btn-danger left" id="B_add"><#if vo??>保存<#else>保存</#if></button>
    </#if>
    <#if !is_org_manager?exists || is_org_manager != 1>
      <a href="/delivery/list" class="btn btn-success left" id="B_cancel">返回</a>
    </#if>
  </#if>
</div>

<div class="tooltip top" role="tooltip">
  <div class="tooltip-arrow"></div>
  <div class="tooltip-inner">
    Some tooltip text!
  </div>
</div>

<input type="hidden" id = "roleCode" value="${roleCode !''}" />
<script>
document.title = '配送区域';
  // $('select').select2();
</script>
