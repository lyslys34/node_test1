<#include "area_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="cssmain">/area/area_explosion_process.css</content>
<content tag="javascript">/area/area_explosion_process.js</content>
<link rel="stylesheet" type="text/css" href="${staticPath}/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />
<div id="main-container">
    <ul  class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#areaSetting" aria-controls="areaSetting" role="tab" data-toggle="tab">区域配置</a></li>
        <li role="presentation"><a href="#thresholdSetting" aria-controls="thresholdSetting" role="tab" data-toggle="tab">阈值配置</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="areaSetting">
            <div class="panel panel-default mar-bottom-0">
                <div id="alert_error"></div>
                <div id="alert_bind_error"></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">城市名称</label>
                                <input type="text" class="form-control input-sm parsley-validated js_city_name" name="cityName" id="cityName" data-minlength="8" value="" />
                                <input type="hidden" name="cityId" id="cityId" class="js_city_id" value="${cityId!""}"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">配送区域名称</label>
                                <input type="text" class="form-control input-sm parsley-validated js_area_name" name="areaName" id="areaName" data-minlength="8" value="${name!""}"/>
                                <input type="hidden" name="areaId" id="areaId" class="js_area_id" value="${areaId!""}"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">区域类型</label>
                                <select class="form-control input-sm" id="type" name="type">
                                    <option value="-1">全部</option>
                                    <option value="0" >关闭爆单</option>
                                    <option value="1" >A</option>
                                    <option value="2" >B</option>
                                    <option value="3" >C</option>
                                    <option value="4" >D</option>
                                    <option value="5" >E</option>
                                    <option value="6" >F</option>
                                </select> 
                            </div>
                        </div>
                        <div class="col-md-1" style="padding-top:18px;">
                            <button id="search" class="btn btn-success">查询</button>
                        </div>
                        <div class="col-md-1" style="padding-top:18px;padding-left: 0;">
                            <button id="batchModify" class="btn btn-success">批量修改</button>
                        </div>
                        <div class="col-md-1" style="padding-top:18px;">
                            <button id="pressIdModify" class="btn btn-success">按id修改</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default table-responsive">
                <table class="table" id="responsiveTable">
                    <thead>
                    <tr>
                        <th><input type="checkbox" value="1" /></th>
                        <th>城市</th>
                        <th>区域</th>
                        <th>区域ID</th>
                        <th>策略名称</th>
                        <th>最近修改时间</th>
                        <th>策略修改人</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div id="areaPage"></div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="thresholdSetting">
            <div id="thresholdAlert"></div>
            <div class="panel panel-default table-responsive">
                <table class="table" id="thresholdTable">
                    <thead>
                    <tr>
                        <th></th>
                        <th>阈值1(延迟30分钟)</th>
                        <th>阈值2(下代购店活动)</th>
                        <th>阈值3(下其他店活动)</th>
                        <th>阈值4(延迟60分钟)</th>
                        <th>阈值5(关部分代购店)</th>
                        <th>阈值6(关全部代购店)</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="showBindRes">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="resMsg">
                </div>
            </div>
        </div>
    </div>      
</div>