<#include "area_config.ftl">
<#include "../widgets/sidebar.ftl">
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=820afe3932ebea15d73cd05b2a28e606"></script>

<link rel="stylesheet" type="text/css" href="/static/css/lib/touchspin/jquery.bootstrap-touchspin.min.css" />
<link rel="stylesheet" type="text/css" href="/static/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />
<content tag="cssmain">/area/audit_delivery_area.css</content>

<script type="text/javascript" src="${staticPath}/js/lib/touchspin/jquery.bootstrap-touchspin.min.js"></script>
<content tag="javascript">/area/area_audit_map.js</content>

<style type="text/css">
</style>
<div id="main-container" >

    <div class="row">
        <div class="col-lg-2">
            <button type="button" class="btn btn-danger left" id="B_reject">驳回请求</button>
            <a  class="btn btn-success left" id="B_checked">审核通过</a>
        </div>
    </div>

    <#if vo??><h3>${vo.deliveryAreaName!''}</h3></#if>

    
    <div class="panel-heading">
        <ul class="nav nav-tabs" id="commonMenu">
            <li value="baseArea"><a href="#baseArea">区域信息<span class="hidden">0</span></a></li>     
        </ul>
    </div>
    

    <div id="alert_error"></div>
    <div id="tips">
        <div id="baseArea">
            <div id="tip" class="fixed">
                <input type="hidden" id="area" name="area" >
                <input type="hidden" id="id" name="id" value="<#if auditVo??>${auditVo.id}</#if>">
                <input type="hidden" id="areaId" name="areaId" value="<#if vo??>${vo.id}</#if>">
                <input type="hidden" id="originArea" name="originArea" value="<#if vo??>${vo.deliveryAreaCoordinates!''}</#if>">
                <input type="hidden" id="auditArea" name="auditArea" value="<#if auditVo??>${auditVo.deliveryAreaCoordinates!''}</#if>">

                <div class="panel panel-default table-responsive">
                    <div class="form-horizontal">
                        <div class="panel panel-default">
                            <h3 style="padding-left:15px;">基本信息</h3>
                            <div class="panel-body">
                                <div class="form-group ">
                                    <label for="inputGroupName" class="col-sm-1 control-label">区域名称</label>
                                    <div class="col-sm-3">
                                        <input type="text" value="<#if vo??>${vo.deliveryAreaName!''}</#if>" class="form-control parsley-validated" id="areaName" name="areaName" data-required="true" readonly maxlength=16>
                                    </div>
                                    <label for="inputGroupDesc" readonly class="col-sm-1 control-label">区域类型</label>
                                    <div class="col-sm-3">
                                        <select disabled='disabled' class="form-control input-sm" id="type" name="type">
                                            <option value="0" >请选择区域类型</option>
                                            <option <#if vo??><#if vo.type=1>selected</#if></#if> value="1" >自建</option>
                                            <option <#if vo??><#if vo.type=2>selected</#if></#if> value="2" >加盟</option>
                                            <option <#if vo??><#if vo.type=4>selected</#if></#if> value="4" >众包</option>
                                        </select>  
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputGroupName" class="col-sm-1 control-label">城市名称</label>
                                    <div class="col-sm-3">
                                        <input type="text" readonly value="<#if vo??>${vo.cityName!''}</#if>" class="form-control parsley-validated " id="cityName" name="cityName" data-required="true">
                                        <input type="hidden" id="cityId" name="cityId" class="js_city_id" value="<#if vo??>${vo.bmCityId!''}</#if>" />
                                    </div>
                                    <label for="inputGroupDesc" class="col-sm-1 control-label">调度模式</label>
                                    <div class="col-sm-3">
                                        <select disabled='disabled' class="form-control input-sm" id="dispatchStrategy" name="dispatchStrategy">
                                            <option value="0" >请选择调度模式</option>
                                            <#if dispatchStrategy??>
                                            <#list dispatchStrategy as ds >
                                            <option value="${ds.code !''}" <#if vo?? && ds.code == vo.dispatchStrategy>selected</#if>>${ds.type !''}</option>
                                            </#list>
                                            </#if>
                                        </select>  
                                    </div>
                                    <div class="col-sm-3"></div>

                                </div>

                            </div>
                        </div>
                    </div>    
                </div>
            </div>

            <div class="panel panel-default table-responsive" >
                <div class="form-horizontal" >
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="panel-title" style="font-size:24px;">配送区域</label>
                                </div>
                            </div>
                        </div>    
                    </div>

                    <div class="row" <#if is_org_manager?exists && is_org_manager == 1> hidden="hidden" </#if>>

                        <div id="explainTip" class="col-md-2">
                            <div class="color_tip" style="background-color:#00FFFF;left:10px;"></div>
                            <span class="color_explain" style="left:65px;">已审核</span>
                            <div class="color_tip" style="background-color:#FF3333;left:110px;"></div>
                            <span class="color_explain" style="left:165px;">待审核</span>

                        </div>

                        <div id="areaLengthTip" class="col-md-3" >
                            <div id="alert_area_length"></div>
                        </div>

                    </div>
                    <div id="mapContainer"></div>

                </div>
            </div>
        </div>

        <div id="areaPoi">
            <div class="panel panel-default table-responsive">
                <div class="form-horizontal" >
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="panel-title" style="font-size:24px;">受影响商家</label>
                                </div>
                            </div>
                        </div>    
                    </div>
                    <table class="table table-striped" id="responsiveTable">
                        <thead>
                            <tr>
                                <th>商家名称</th>
                                <th>商家地址</th>
                                <th>商家联系方式</th>
                                <th>商家负责BD</th>
                                <th>变更类型</th>
                            </tr>
                        </thead>
                        <tbody id="merchantListBody">
                            <#if pois??>
                                <#list pois as poi>
                                <tr>
                                    <td>${poi.name!''}</td>
                                    <td>${poi.address!''}</td>
                                    <td>${poi.phone!''}</td>
                                    <td>${poi.ownerName!''}</td>
                                    <td>移出</td> 
                                </tr>
                                </#list>
                            </#if>

                        </tbody>
                    </table>
                </div>
                <div id="areaPoiPager"></div>
            </div>
        </div>

        <div class="modal fade" id="editRejectReasonModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 id="addModalTitle" class="text-center">驳回原因</h4>
                        <div id="alert_edit_reject_reason_error"></div>
                        <div class="form-group">
                            <textarea id="reject_reason" style="height:150px;resize:none;" class="form-control " placeholder="请输入驳回原因"></textarea>
                        </div><br>
                        <div class="modal-footer">
                            <button type="button" id="editRejectReasonSubmit" rel="2" class="btn btn-success btn-block">确定</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="saveInfoModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">

                        <h4>首次创建的配送区域， 审核通过后<span style="color:red">立即生效</span></h4>
                        <div class="text-right" >
                           <button type="button" class="btn btn-danger" rel="6" id="B_continue_save">确定</button>
                           <button type="button" class="btn btn-success" id="B_cancel_save">取消</button>
                       </div>
                   </div>
               </div>

           </div>
       </div>

        <div class="modal fade" id="saveSuccessModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div id="alert_success_massage"></div>
                        <div class="text-center" >
                           <!-- <button type="button" class="btn btn-success" id="B_ok">确定</button> -->
                       </div>
                   </div>
               </div>

           </div>
       </div>

       <script>
       document.title = '配送区域审核';
	// $('select').select2();
    </script>
