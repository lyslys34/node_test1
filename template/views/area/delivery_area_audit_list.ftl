<#include "area_config.ftl">
<#include "../widgets/sidebar.ftl">

<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=820afe3932ebea15d73cd05b2a28e606"></script>
<content tag="javascript">/area/area_audit_list.js</content>

<link rel="stylesheet" type="text/css" href="${staticPath}/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />
<content tag="cssmain">/area/area_audit_list.css</content>
<style type="text/css">

</style>

<div id="main-container">
    <div class="panel panel-default">
        <div id="alert_error"></div>
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post" action="/delivery/audit/list">
            <div class="panel-body">
                <div class="row">
                	<div class="col-md-3">
            			<div class="form-group">
                            <label class="control-label">区域名称</label>
                            <input type="text" class="form-control input-sm parsley-validated js_area_name" name="name" id="name" data-minlength="8" value="${name!""}"/>
                            <input type="hidden" name="id" id="id" class="js_area_id" value="${areaId!""}"/>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding-top:18px;">
                        <button id="search" type="submit" class="btn btn-success">查询</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table style="table-layout:fixed;word-break:break-all;" class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>创建时间</th>
                <th>提交人</th>
                <th>所属城市团队</th>
                <th>审核类型</th>
                <th>区域名称</th>
                <th>修改原因</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#if deliveryAreas??>
                <#list deliveryAreas as area>
                <tr>
                    <td>${area.updateTime!''}</td>
                    <td>${area.createUserName!''}</td>
                    <td>${area.parentCityOrg!''}</td>
                    <td>${area.checkedDesc!''}</td>
                    <td><a data-toggle="tooltip" title="审阅" data-placement="top" href="/delivery/audit/detail?id=${area.id}" target="_blank">${area.bmDeliveryAreaName!''}</a></td>
                    <td>${area.changeComment!''}</td>

                    <td>
                        <a data-toggle="tooltip" title="审阅" data-placement="top" href="/delivery/audit/detail?id=${area.id}" target="_blank">
                                <i class="fa fa-eye fa-lg opration-icon"></i>
                        </a>
                    </td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>

    <div style="float: left; padding: 10px; 15px"><span>共${recordCount!'0'}项</span></div>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/delivery/audit/list"/>
    </#if>
    </div>
</div>

<script>document.title = '配送区域审核';</script>

