<#include "area_config.ftl">
<#include "../widgets/sidebar.ftl">

<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=820afe3932ebea15d73cd05b2a28e606"></script>
<content tag="javascript">/area/area_list.js</content>

<link rel="stylesheet" type="text/css" href="${staticPath}/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />
<content tag="cssmain">/area/area_list.css</content>
<style type="text/css">

</style>

<div id="main-container">
    <div class="panel panel-default">
        <div id="alert_error"></div>
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post" action="/delivery/list">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">城市名称</label>
                            <input type="text" class="form-control input-sm parsley-validated js_city_name" name="cityName" id="cityName" data-minlength="8" value=""/>
                            <input type="hidden" name="cityId" id="cityId" class="js_city_id" value="${cityId!""}"/>
                        </div>
                    </div>
                	<div class="col-md-3">
            			<div class="form-group">
                            <label class="control-label">区域名称</label>
                            <input type="text" class="form-control input-sm parsley-validated js_area_name" name="name" id="name" data-minlength="8" value="${name!""}"/>
                            <input type="hidden" name="id" id="id" class="js_area_id" value="${areaId!""}"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">区域类型</label>
                            <select class="form-control input-sm" id="type" name="type">
                                <#if userType?? && userType == 0>
                                    <option value="0" >全部</option>
                                    <option <#if type??><#if type=1>selected</#if></#if> value="1" >自建</option>
                                    <option <#if type??><#if type=2>selected</#if></#if> value="2" >加盟</option>
                                    <option <#if type??><#if type=4>selected</#if></#if> value="4" >众包</option>
                                    <option <#if type??><#if type=7>selected</#if></#if> value="7" >城市代理</option>
                                <#else>
                                    <option selected value="${type!""}" >${typeName!""}</option>
                                </#if>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding-top:18px;">
                        <button id="search" type="submit" class="btn btn-success">查询</button>
                        <button id="showCityArea" type="button" class="btn btn-success">查看城市配送范围</button>
                        <a href="/delivery/cuArea" role="button" class="btn btn-danger" target="_blank">新建配送区域</a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>区域名称</th>
                <th>站点数量</th>
                <th>城市</th>
                <th>业务类型</th>
                <th>调度模式&nbsp;<span data-toggle="tooltip" data-placement="top" title="多站点时，配送区域调度模式只能选择“推抢结合”。"><i class="fa fa-lg fa-question-circle"></i></span></th>
                <th>营业时间&nbsp;<span data-toggle="tooltip" data-placement="top" title="配送区域的营业时间，是根据所绑定站点的最长营业时间确定的。如需调整，请调整该区域内站点营业时间。"><i class="fa fa-lg fa-question-circle"></i></span></th>
                <th>审核状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#if deliveryAreas??>
                <#list deliveryAreas as area>
                <tr>
                    <td><a title="修改信息" target="_blank" href="/delivery/cuArea?id=${area.id}" >${area.deliveryAreaName!''}</a></td>
                    <td>${area.countBindBmOrg!''}</td>
                    <td>${area.cityName!''}</td>
                    <td><#if area.type??><#if area.type == 1>自建<#elseif area.type=2>加盟<#elseif area.type=3>驻店三方<#elseif area.type==4>众包<#elseif area.type==5>角马<#elseif area.type==7>城市代理<#else>未知类型</#if></#if></td>
                    <td>${area.dispatchStrategyDesc!''}</td>
                    <td>${area.businessHours!''}</td>
                    <td>${area.checkedDesc!''}</td>
                    <#if area.bmOrgList??>
                        <#list area.bmOrgList as bmorg>
                        <input type="hidden" class="orgId" value="${bmorg.bmOrgId!''}"/>
                        <input type="hidden" class="orgName" value="${bmorg.bmOrgName!''}"/>
                        </#list>
                    </#if>
                    <td>
                        <#if area.checked??><#if area.checked ==1 || area.checked == 2 || area.checked == 6>
                                <a data-toggle="tooltip" title="修改信息" data-placement="top" href="/delivery/cuArea?id=${area.id}" target="_blank">
                                    <i class="fa fa-edit fa-lg opration-icon"></i>
                                </a>&nbsp;&nbsp;
                            </#if></#if>
                            <a data-toggle="tooltip" title="站点列表" data-placement="top" href="/delivery/cuArea?id=${area.id}&tipName=areaOrg" target="_blank">
                                <i class="fa fa-sitemap fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                            <a data-toggle="tooltip" title="商家列表" data-placement="top" href="/delivery/cuArea?id=${area.id}&tipName=areaPoi" target="_blank">
                                <i class="fa fa-list-ul fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                            <a data-toggle="tooltip" title="操作日志" data-placement="top" href="/delivery/cuArea?id=${area.id}&tipName=areaOPLog" target="_blank">
                                <i class="fa fa-history fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                            <#if area.checked??><#if area.checked ==1 || area.checked == 2 || area.checked == 6>
                                <a data-toggle="tooltip" title="删除" data-placement="top" class="delete_link" rel="${area.id}" data-name="${area.deliveryAreaName!''}">
                                    <i class="fa fa-trash-o fa-lg opration-icon"></i>
                                </a>
                            </#if></#if>
                    </td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>

     <div class="modal fade" id="bindOrgModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div id="alert_bind_error"></div>
                    <h4>绑定站点</h4>
                    <label>请输入站点名称：</label>
                    <input type="text" class="form-control input-sm js_bind_org_name" name="bindOrgName" id="bindOrgName" data-minlength="8" value=""/>
                    <input type="hidden" class="js_bind_org_id" name="bindOrgId" id="bindOrgId" value=""/>
                    <p style="color:red">注意：新绑定站点将会覆盖原有已绑定站点；如果输入为空，则解绑已有站点</p>
                    <div class="modal-footer">
                        <input type="submit" id="bindOrgSubmit" value="确认" class="btn btn-danger btn-sm" />
                        <button class="btn btn-success btn-sm" id="cancelBindOrg">取消</button>
                    </div>
                </div>
            </div>

        </div>
     </div>
    <div class="modal fade" id="showBindRes">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="resMsg">

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="showCityAreaModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <button id="resetZoomBtn" type="button" class="btn btn-default">视野</button>
                    <div id="mapContainer">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="unBindOrgModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>不绑定任何站点？</h4>
                    <div class="modal-footer">
                        <input type="submit" value="是" class="btn btn-danger btn-sm" id="unBindOrgSubmit"/>
                        <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">否</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editChangeCommentModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-horizontal">
                        <h4 style="border-bottom: 1px solid #e5e5e5; padding-bottom: 7px;">提示</h4>
                        <div class="form-group">
                            <label class="control-label col-sm-3" style="font-size: 16px;">确定要删除</label>
                            <div class="col-sm-5" style="top: 7px;">
                                <span id="delAreaName" style="font-size:16px; font-family: microsoft yahei,Arial,Helvetica,sans-serif; font-weight: 500;"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" style="font-size: 16px;">待删除区域名称</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control input-sm" id="delAreaNameInput" placeholder="请输入待删除区域名称" value=""/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-8" style="font-size: 16px; color: #ff00da;">温馨提示：区域删除后无法恢复，请谨慎操作</label>
                        </div>

                        <div class="modal-footer">
                            <button type="button" id="editChangeCommentSubmit" class="btn btn-danger btn-sm">确定</button>
                            <button type="button" id="cancelChangeCommentSubmit" class="btn btn-success btn-sm">取消</button>
                        </div>

                        <input id="delete_id" type="hidden" value=""/>
                        <input id="delAreaNameHidden" type="hidden" value=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="float: left; padding: 10px; 15px"><span>共${recordCount!'0'}项</span></div>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/delivery/list"/>
    </#if>
    </div>
</div>

<script>document.title = '配送区域管理';</script>
