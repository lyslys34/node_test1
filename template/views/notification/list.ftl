<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<#setting datetime_format="yyyy-MM-dd HH:mm:ss"/>


<div id="main-container">
    <#if (page)?exists && (page.noticeList)?exists && (page.noticeList?size > 0) >
    <table class="table">
        <thead>
            <tr>
                <td>通知标题</td>
                <td>通知内容</td>
                <td>发布时间</td>
                <td>操作</td>
            </tr>
        </thead>
        <tbody>
        <#list page.noticeList as notice>
            <tr <#if notice.hasRead==1>class="active"</#if>>
                <td><a title="${notice.title !''}"><#if ((notice.title !'')?length>10)>${(notice.title !'')?substring(0,10)}...<#else>${notice.title !''}</#if></a></td>
                <td><a title="${notice.content !''}"><#if ((notice.content !'')?length>10)>${(notice.content !'')?substring(0,10)}...<#else>${notice.content !''}</#if></a></td>
                <td><#if (notice.publishTime>0)>${(notice.publishTime *1000)?number?number_to_datetime}</#if></td>
            
				<td>
                    <a data-toggle="tooltip" title="查看详情" data-placement="top" href="<#if isPartner>/partner</#if>/notification/detail?id=${notice.id}">
                        <i class="fa fa-info-circle fa-lg opration-icon"></i>
                    </a>
                </td>
			</tr>
        </#list>
        </tbody>
    </table>
    <#if (page)?exists && (page.total)?exists && (page.pageSize)?exists && (page.pageNum)?exists && page.total gt 0 && page.pageSize gt 0 && page.pageNum gt 0  >
      <#if page.total??>
        <#import "/page/pager.ftl" as q>
        <#assign toURL = "/notification/list" />
        <#if isPartner><#assign toURL = "/partner/notification/list"></#if>
        <@q.pager pageNo=page.pageNum pageSize=page.pageSize recordCount=page.total toURL=toURL/>
      </#if>
    </#if>
   <#else>
    暂无数据！
   </#if>
</div>