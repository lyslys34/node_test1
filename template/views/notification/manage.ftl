<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<#include "../utils.ftl">
<#setting datetime_format="yyyy-MM-dd HH:mm:ss"/>

<content tag="javascript">/notification/manage.js</content>
<content tag="cssmain">/notification/manage.css</content>

<div id="main-container">
    <form id="fm" method="get" action="/notification/manage" class="form-inline">
        发布日期&nbsp;
        <input type="text" name="ts" style="cursor: pointer" readonly="readonly" value="<#if ts?exists>${ts}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start"/>
        至&nbsp;
        <input type="text" name="te" style="cursor: pointer" readonly="readonly" value="<#if te?exists>${te}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end"/>

        &nbsp;&nbsp;&nbsp;&nbsp;
        <input type="button" value="查询" class="btn btn-success js_submitbtn" id="search" />
        <input type="button" value="新建" id="create" class="btn btn-success js_submitbtn"/>
    </form>
    <#if (page)?exists && (page.noticeList)?exists && (page.noticeList?size > 0) >
    <table class="table">
        <thead>
        <tr>
            <td>通知标题</td>
            <td>通知内容</td>
            <td>范围</td>
            <td>发布时间</td>
            <td>定时撤下</td>
            <td>状态</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody>
            <#list page.noticeList as notice>
            <tr>
                <td><a title="${notice.title !''}"><#if ((notice.title !'')?length>10)>${(notice.title !'')?substring(0,10)}...<#else>${notice.title !''}</#if></a></td>
                <td><a title="${notice.content !''}"><#if ((notice.content !'')?length>10)>${(notice.content !'')?substring(0,10)}...<#else>${notice.content !''}</#if></a></td>
                <td>
                    <#if notice.area?index_of("0") == -1>
                        全部
                    <#else>
                        <#if notice.area?substring(0,1) == "1">直营
                        </#if>
                        <#if notice.area?substring(1,2) == "1">加盟
                        </#if>
                        <#if notice.area?substring(2,3) == "1">众包
                        </#if>
                    </#if>
                </td>
                <td><#if (notice.publishTime>0)>${(notice.publishTime *1000)?number?number_to_datetime}</#if></td>
                <td><#if (notice.offlineTime>0 && notice.status!=3)>${(notice.offlineTime *1000)?number?number_to_datetime}</#if></td>
                <td>
                    <#if notice.status == 1><span class="label badge label-disactive">未发布</span></#if>
                    <#if notice.status == 2><span class="label badge label-success">已发布</span></#if>
                    <#if notice.status == 3><span class="label badge label-warning">已下线</span></#if>
                </td>
				<td id="${notice.id}">
                        <a data-toggle="tooltip" title="详情" data-placement="top" href="javascript:;" name="detail">
                            <i class="fa fa-info-circle fa-lg opration-icon"></i>
                        </a>&nbsp;&nbsp;
                        <a data-toggle="tooltip" title="编辑" data-placement="top" href="javascript:;" name="edit">
                            <i class="fa fa-edit fa-lg opration-icon"></i>
                        </a>&nbsp;&nbsp;
                        <#if notice.status == 1>
                        <a data-toggle="tooltip" title="发布" data-placement="top" href="javascript:;" name="publish">
                            <i class="fa fa-arrow-up fa-lg opration-icon"></i>
                        </a>&nbsp;&nbsp;
                        </#if>
                        <#if notice.status == 2>
                        <a data-toggle="tooltip" title="下线" data-placement="top" href="javascript:;" name="offline">
                            <i class="fa fa-arrow-down fa-lg opration-icon"></i>
                        </a>&nbsp;&nbsp;
                        </#if>
                        <a data-toggle="tooltip" title="删除" data-placement="top" href="javascript:;" name="delete">
                            <i class="fa fa-trash-o fa-lg opration-icon"></i>
                        </a>
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
    <#if (page)?exists && (page.total)?exists && (page.pageSize)?exists && (page.pageNum)?exists && page.total gt 0 && page.pageSize gt 0 && page.pageNum gt 0  >
      <#if page.total??>
        <#import "/page/pager.ftl" as q>
        <@q.pager pageNo=page.pageNum pageSize=page.pageSize recordCount=page.total toURL="/notification/manage"/>
      </#if>
    </#if>
    <#else>
        暂无数据！
    </#if>

    <#-- 新建 -->
    <div id="create-container" class="hide">
        <form class="form-horizontal" id="noticeForm" action="/notification/save.ajax" type="post">
            <input type="hidden" name="id" id="id" value="0" />
            <input type="hidden" name="status" id="status" value="0" />
            <div id="edit-container">
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">通知标题</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="title" id="title" required=true placeholder="请输入标题">
                </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label checkbox"><input type="checkbox" id="setOfflineTime"> 定时下线 </label>
                  <div class="col-sm-10">
                    <input type="text" name="offlineTime" id="offlineTime" style="cursor: pointer" class="input-sm form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'%y-%M-%d %H:%m:%s'})" />
                  </div>
              </div>
              <div class="form-group">
                  <label for="area" class="col-sm-2 control-label">发布范围</label>
                    <div class="col-sm-10 form-inline" id="area">
                        <span class="checkbox checkbox-span">
                            <label>
                                <input type="checkbox" name="selfSupport" id="selfSupport" checked> 直营
                            </label>
                        </span>
                        <span class="checkbox checkbox-span">
                            <label>
                                <input type="checkbox" name="partner" id="partner" checked> 加盟
                            </label>
                        </span>
                        <span class="checkbox">
                            <label>
                                <input type="checkbox" name="crowdsourcing" id="crowdsourcing" checked> 众包
                            </label>
                        </span>
                    </div>
              </div>
              <div class="form-group">
                  <div class="col-sm-10">
                    <textarea id="content" name="content" class="form-control" style="width:450px" required=true rows=5 placeholder="请输入公告内容"></textarea>
                  </div>
              </div>

            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="/static/js/lib/My97DatePicker/WdatePicker.js"></script>
