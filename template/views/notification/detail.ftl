<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/notification/detail.js</content>
<content tag="cssmain">/notification/detail.css</content>

<div id="main-container">
<#if (notice)?exists >
    <h5>${notice.title!''}<h5>
    <h6>${notice.publishTime}</h6>
    <p id="content">${notice.content!''}</p>
<#else>
    暂无数据！
</#if>
</div>