<#include "jiaoma_config.ftl">
<#include "../widgets/sidebar.ftl">


<content tag="javascript">/jiaoMa/rider_poi_list.js</content>
<style type="text/css">
    #main-container{
    padding: 10px 20px;
    }
</style>
    <div id="main-container">
        <div class="panel-heading" style="background-color:#3c8dbc;color:#fff;font-size:16px;">
            <span>基本信息</span>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4" style="width:250px;">
                    <div class="form-group" style="width:250px;">
                        <label class="control-label">姓名:</label>
                        ${user.name!''}
                    </div><!-- /form-group -->
                </div>
                <div class="col-md-4" style="width:250px;">
                    <div class="form-group" style="width:250px;">
                        <label class="control-label">手机号:</label>
                        ${user.phone!''}
                    </div><!-- /form-group -->
                </div>
                <div class="col-md-4" style="width:250px;">
                    <div class="form-group" style="width:250px;">
                        <label class="control-label">职位:</label>
                        ${user.roleName!''}
                    </div><!-- /form-group -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-4" style="width:250px;">
                    <div class="form-group" style="width:250px;">
                        <label class="control-label">组织类型:</label>
                        ${user.orgTypeName!''}
                    </div><!-- /form-group -->
                </div>
                <div class="col-md-4" style="width:250px;">
                    <div class="form-group" style="width:250px;">
                        <label class="control-label">组织名称:</label>
                        ${user.orgName!''}
                    </div><!-- /form-group -->
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <input type="hidden" name="userId" value="${userId!'0'}"/>
            <input type="hidden" name="roleCode" value="${roleCode!'0'}"/>
            <input type="hidden" name="orgId" value="${orgId!'0'}"/>
            <input type="hidden" name="orgType" value="${orgType!'0'}"/>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group" style="margin:10px 10px; ">
                        <label class="control-label">绑定商家</label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <input class="btn btn-success" id="addPoi" value="添加" style="width:60px;height:30px;"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default table-responsive">

            <table class="table table-striped" id="showJiaoMaPoi">
                <thead>
                <tr>
                    <th>商家名称</th>
                    <th>商家ID</th>
                    <th>操作</th>
                </tr>
                </thead>
                <#if jiaoMaRiderPoiList?exists && (jiaoMaRiderPoiList?size>0) >
                    <tbody>
                    <#--循环-->
                        <#list jiaoMaRiderPoiList as item>
                            <tr id="${item.id!''}">
                                <td >${item.poiName!''}</td>
                                <td>${item.wmPoiId!''}</td>
                                <td><input value="解除绑定" class="btn btn-danger btn-sm unBindPoiSubmit" poiId="${item.wmPoiId!''}" roleCode="${item.roleCode!''}" style="width:80px;"/></td>
                            </tr>
                        </#list>
                    </tbody>
                </#if>
            </table>
            <div class="modal fade" id="bindOrgModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <label>请输入商家POI：</label>
                            <input type="text" class="form-control input-sm js_bind_org_name" name="wmPoiId" id="wmPoiId" data-minlength="8" value="" placeholder="商家POI"/>
                            <input type="hidden" class="js_bind_org_id" name="bindOrgId" id="bindOrgId" value=""/>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">取消</button>
                                <input type="submit" id="bindPoiSubmit" value="提交" class="btn btn-danger btn-sm" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <#--
                <#import "../page/pager.ftl" as q>
                    <#if recordCount??>
                        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/rider/list"/>
                    </#if>
            -->
        </div>
    </div>
    <script>
    document.title = '角马人员商家列表';
    $('select').select2();
</script>