<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">

<title>活动通知</title>

<div id="main-container" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10 h3">
                活动通知系统
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row" style="background-color:#FFFFFF">
            <div class="col-md-1"></div>
            <div class="col-md-10" style="border:1px solid #696969;line-height:40px">
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2 text-center">活动名称</div>
                    <div class="col-md-5">
                        <input id="name" name="activityName" type="text" class="form-control" placeholder="请输入活动名称"/>
                    </div>
                    <div class="col-md-5">（此内容将PUSH消息给众包配送员）</div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-center">推送图片</div>
                    <div class="col-md-2">
                        <input id="upload-input" name="image" type="file" style="display: none" />
                        <button id="upload-button" class="btn btn-primary btn-block">上传图片</button>
                    </div>
                    <div class="col-lg-6">
                        <img id="activity-image" src="">
                    </div>
                    <div class="col-md-8">（上传图片大小说明：建议尺寸 580x780px）</div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-center">URL</div>
                    <div class="col-md-5">
                        <input id="url" name="jumpUrl" type="text" class="form-control" placeholder="请输入URL"/>
                    </div>
                    <div class="col-md-5"></div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-center">推送组织</div>
                    <div class="col-md-5">
                        <textarea id="org_ids" name="orgIds" class="form-control" rows="3" placeholder="可批量添加组织ID，以逗号为分隔符"></textarea>
                    </div>
                    <div class="col-md-5"></div>
                </div>
                <div class="row" style="margin-top:10px;margin-bottom:10px">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <input id="push" type="button" value="开始推送" class="btn btn-danger btn-block" />
                    </div>
                    <div class="col-md-8"></div>
                </div>

                <form id="push-form" method="post" action="/activity/pushNotification">
                    <input type="hidden" name="imageUrl">
                </form>

            </div>

            <div class="col-md-1"></div>

        </div>

    </div>
</div>


<script type="text/javascript" src="${staticPath}/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="${staticPath}/js/lib/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="${staticPath}/js/lib/jQuery-File-Upload/js/jquery.fileupload.js"></script>

<script type="text/javascript" src="${staticPath}/js/page/activity/notification.js"></script>