<#include "base/home_config.ftl">

<content tag="javascript">/login.js</content>
<content tag="cssmain">/login.css</content>

<div style="padding-top:80px">

    <form method="post" action="/partner/login" id="login_form">
        <table style="width:400px; position:relative; left:50%; margin-left:-200px;">

            <#if error?? && (error?length > 0)>
                 <tr>
                     <td></td>
                     <td colspan="2">
                         <label class="err-msg">
                             ${error !''}
                         </label>
                     </td>
                 </tr>
            </#if>
            <tr class="hide error">
                 <td></td>
                 <td colspan="2">
                     <label class="err-msg">
                     </label>
                 </td>
             </tr>
            <tr>
                <td style="width:100px">手机号</td>
                <td style="width:100px" colspan="2">
                    <input name="mobile" id="mobile" class="form-control" value="${mobile?if_exists}"/>
                </td>
            </tr>

            <tr>
                <td>验证码</td>
                <td>
                    <input type="text" name="captcha" id="captcha" maxlength="50" class="form-control">
                </td>
                <td>
                    <input type="button" name="send-captcha" id="send-captcha" class="btn btn-default" value="获取验证码"/>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div style="text-align: center;">
                        <button type="submit" class="btn btn-default" style="width:100px;">登录</button>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div style="text-align: center">
                        <p>还没有账号？请联系您的美团接口人开通</a></p>
                    </div>
                </td>
            </tr>

        </table>
    </form>

</div>






