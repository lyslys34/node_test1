<#include "proxy_poi_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/proxyPoi/proxyPoi.js</content>
<content tag="cssmain">/proxyPoi/proxyPoi.css</content>

    <div id="main-container">

            <#--<input type="button" value="新增商家" id="create" class="btn btn-success js_submitbtn" style="margin-bottom:15px;margin-left:60px;"/>-->

        <div class="panel panel-default table-responsive" style="width:100%">
            <#if poiLists?? && (poiLists?size>0) >
            <table class="table table-striped" id="responsiveTable">
                <thead>
                <tr>
                    <th style="padding-left:60px;">商家名称</th>
                    <th>折扣</th>
                    <th>商家POI</th>
                    <th>商家电话</th>
                    <th>商家地址</th>
                    <#--<th style="padding-left:40px;">操作</th>-->
                </tr>
                </thead>
                <tbody>
                <#--循环-->

                        <#list poiLists as poi>
                            <tr>
                                <td style="padding-left:60px;">${poi.name!''}</td>
                                <td>${poi.discount!''}</td>
                                <td>${poi.wmPoiId!''}</td>
                                <td>${poi.phone!''}</td>
                                <td>${poi.address!''}</td>
                                <#--<td bmPoiId="${poi.id!'0'}" wmPoiId="${poi.wmPoiId!'0'}">-->
                                    <#--<span  data-toggle="tooltip" title="编辑" data-placement="top" class="js_operate_submit edit" wmPoiName="${poi.name!''}" style="margin-left:10px;cursor:pointer">-->
                                        <#--<i class="fa fa-edit fa-lg opration-icon"></i>-->
                                    <#--</span>-->
                                    <#--<span  data-toggle="tooltip" title="删除" data-placement="top" class="js_operate_submit del" style="margin-left:5px;cursor:pointer">-->
                                        <#--<i class="fa fa-trash-o fa-lg opration-icon"></i>-->
                                    <#--</span>-->
                                <#--</td>-->
                            </tr>
                        </#list>

                </tbody>
            </table>
            <#else>
                暂时没有商家信息！
            </#if>
        </div>
    </div>
    <#-- 新建 -->
        <div id="create-container" class="hide">
            <div class="panel-heading">
                <span id="showTitle"></span>
            </div>
            <form class="form-horizontal" id="proxyPoiForm" action="/proxyPoi/save.ajax" type="post">
                <input type="hidden" name="id" id="id" value="0" />
                <div id="edit-container">
                    <div class="row" >
                        <div class="form-group">
                            <label  class="control-label" >商家POI</label>
                            <input type="text"  class="form-control" name="wmPoiId" id="wmPoiId" required=true placeholder="请准确输入商家POI">
                        </div>
                        <div class="form-group" style="float:right">
                            <label class="control-label" style="width:200px text-align:left"> 商家名称 </label>
                            <p id="poiName" style="width:200px;padding-top:10px;"></p>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="form-group">
                            <label class="control-label">商家折扣</label>
                            <input type="text" class="form-control" name="discount" id="discount" required=true placeholder="如75折，填写75">

                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="hide" id="proxyPoiMap"></div>
    </div>
    <script>document.title = '代购商家列表';</script>