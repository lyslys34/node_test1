<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/riderstate/riderstate.js</content>
<content tag="cssmain">/riderstate/riderstate.css</content>
<content tag="pageName">waybilllist</content>

<div id="main-container">

<style>

</style>
<ul class="nav nav-tabs h50" style="height:50px">
<li class="active"  style="top:7px; border-bottom:solid 1px #ffffff;"><a href="javascript:void(0);">配送员状态</a></li>
   <#--<li value="30"><a href="/dispatch#30"> </a></li>-->
   <#--<li value="50"><a href="/dispatch#50"> </a></li>-->
</ul>
<br/>
<div id="oneoftheRiders">
	<h5><a href="/partner/riderstate/riderstate">全部配送员</a>&nbsp;>&nbsp;${user.name}今日已完成订单</h5>
</div>
<br />
<div id="Fenched">
	<#if list?exists>
		<h5>共${list.total}单&nbsp;已完成的订单(${.now})</h5>
	<#else>
		<h5>共0单&nbsp;已完成的订单(${.now})</h5>
	</#if>
</div>
<br />
<div id="packageInformation">
<table class="table">
    <thead>
   				<tr>
					<th><strong>序号</strong></th>
					<th><strong>订单号</strong></th>
					<th><strong>接单时间</strong></th>
					<th><strong>配送时长(分钟)</strong></th>
					<th><strong>订单金额(元)</strong></th>
					<th><strong>商家</strong></th>
					<th><strong>订单详情</strong></th>
				</tr>    
	</thead>
    <tbody id="tbody-dispatcher-list">
    			<#if list?exists && (list.waybillList?size > 0) >
    				<#list list.waybillList as w>
    				<#assign duringTime=((w.utime-w.ctime)/(60))?int>
    				<#setting datetime_format="HH:mm"/>
    					<tr>
    						<td>${w_index + 1}</td>
    						<td>${w.platformOrderId}</td>
    						<td>${(w.ctime*1000)?number?number_to_datetime}</td>
    						<td>${duringTime}</td>
    						<td>${w.pkgPrice}</td>
    						<td>${w.senderName}</td>
    						<td><a href="/partner/riderstate/waybilldetail?bid=${w.id}&uid=${user.id}&isFinish=true" target="_blank">查看详情></a></td>
    					</tr>
    				</#list>
    			</#if>
    </tbody>
</table>
</div>

<#if (list?exists && (list.waybillList?size > 0)) && page?exists >
    <#assign leftNum = page.leftNum>
    <#assign rightNum = page.rightNum>
   <#if rightNum gt leftNum>
		<ul class="pagination" style="float: right;">
		    <li <#if page.currentPage == 1>class="disabled"</#if>><a id = "prev" data-page = "${page.currentPage}" href=<#if page.currentPage == 1>"javascript:void(0);"<#else>"waybillList?pageNo=${page.currentPage-1}&pageSize=20"</#if>>&laquo;</a></li>
		    <#list leftNum..rightNum as p>
		        <li <#if p == page.currentPage>class="active"</#if>><a name = "pageCode" href="waybillList?pageNo=${p}&pageSize=20">${p}</a></li>
		    </#list>
		    <li <#if page.currentPage == page.totalPage>class="disabled"</#if>><a id= "next" data-page = "${page.currentPage}" href=<#if page.currentPage == page.totalPage>"javascript:void(0);"<#else>"waybillList?pageNo=${page.currentPage+1}&pageSize=20"</#if>>&raquo;</a></li>
		</ul>
	</#if>
</#if>

<div>
