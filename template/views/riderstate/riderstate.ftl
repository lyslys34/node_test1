<#include "config.ftl">

<content tag="javascript">/riderstate/riderstate.js</content>
<content tag="cssmain">/riderstate/riderstate.css</content>
<content tag="pageName">riderstate</content>

<div id="main-container">


<ul class="nav nav-tabs h50"  style="height:50px" >
<li class="active"  style="top:7px; border-bottom:solid 1px #ffffff;"><a href="javascript:void(0);">配送员状态</a></li>
   <#--<li value="30"><a href="/dispatch#30"> </a></li>-->
   <#--<li value="50"><a href="/dispatch#50"> </a></li>-->
</ul>
<br/>
<div >
	<div style="display:inline-block;">
		<#if view?exists >
		共${view.total}位工作状态的配送员
		<#else>
		共0位工作状态的配送员
	   </#if>
	</div>
	<div style="display:inline-block;float:right;font"> 
		<span style="width:30px;height:20px;background-color:green;margin-right:5px;margin-left:5px;">
			&nbsp;&nbsp;&nbsp;&nbsp;
		</span>
		0-30分钟订单      
		<span style="width:30px;height:20px;background-color:yellow;margin-right:5px;margin-left:5px;">
			&nbsp;&nbsp;&nbsp;&nbsp;
		</span>
		31-50分钟订单     
		<span style="width:30px;height:20px;background-color:red;margin-right:5px;margin-left:5px;">
			&nbsp;&nbsp;&nbsp;&nbsp;
		</span> 
		大于51分钟订单    
		&nbsp;&nbsp;&nbsp;&nbsp;35'该订单总持续分钟数（从接单算起）
	</div>
</div>
<table class="table">
    <thead>
    <tr class="thead">
        <td>姓名</td>
        <td style="width:250px;">已接单</td>
        <td style="width:250px;">已取货</td>
        <td>今日已完成</td>
        <td>当前位置(测试版)</td>
    </tr>
    </thead>
    <tbody id="tbody-dispatcher-list">
    <#assign lineNumber=0>
	<#setting datetime_format="HH:mm"/>
    <#if view?exists && (view.riders?size > 0) >
        <#list view.riders as d>
		<#assign lineNumber=lineNumber+1>
        <tr>
         <td>${d.rider.name}<#if d.rider.workStatus == 2>(忙碌)</#if></td>
 		 <td>
 		 	<#if d.infoMap["20"]?exists>
	 		 	<#assign grabedList=d.infoMap["20"]>
				<#list grabedList as grabed>
					<#assign duringTime=(grabed.duringTime/60)?int>
					<#if duringTime<=30>	
						<span class="duringtime30" style="" id="${grabed.id}"><span value="${grabed.id}" class="waybillid duringLink30">${duringTime}'</span></span>
					<#elseif duringTime<=50>
						<span class="duringtime3050" style="" id="${grabed.id}"><span value="${grabed.id}" class="waybillid duringLink3050">${duringTime}'</span></span>
					<#elseif duringTime<120> 
						<span class="duringtime50"  id="${grabed.id}"><span value="${grabed.id}" class="waybillid duringLink50">${duringTime}'</span></span>	
					<#else>
						<span class="duringtime50"  id="${grabed.id}"><span value="${grabed.id}" class="waybillid duringLink50" >${(duringTime/60)?int}时${duringTime%60}分</span></span>	
					</#if>	
				</#list>
			<#else>
				暂无任务
			</#if>
		 </td>
		 <td>
			<#if d.infoMap["30"]?exists>
			 	<#assign fetchedList=d.infoMap["30"]>
				<#list fetchedList as fetched>
					<#assign duringTime=((fetched.duringTime/60))?int>
					<#if duringTime<=30>	
						<span class="duringtime30"  id="${fetched.id}"><span value="${fetched.id}" class="waybillid duringLink30">${duringTime}'</span></span>
					<#elseif duringTime<=50>
						<span class="duringtime3050"  id="${fetched.id}"><span value="${fetched.id}" class="waybillid duringLink3050">${duringTime}'</span></span>
					<#elseif duringTime<120> 
						<span class="duringtime50"  id="${fetched.id}"><span value="${fetched.id}" class="waybillid duringLink50">${duringTime}'</span></span>	
					<#else>
						<span class="duringtime50"  id="${fetched.id}"><span value="${fetched.id}" class="waybillid duringLink50">${(duringTime/60)?int}时${duringTime%60}分</a></span>	
					</#if>
				</#list>
			<#else>
				暂无任务
			</#if>
		</td>
 		 <td>
			<#if d.totalMap["50"]?exists>
 		 	<#assign delivered=d.totalMap["50"]>	
 		 	<#if delivered==0>
				${delivered}
			<#else>
			 	<a href="/partner/riderstate/waybilllist?uid=${d.rider.id}">${delivered}</a>
			</#if>
			</#if>
		 </td>
		 <td>
		 	<#if d.lastLocation?exists>
			[${(d.lastLocation.updateTime*1000)?number?number_to_datetime}同步]${d.lastLocation.location}
			<#else>
			暂无位置信息
			</#if>
		 </td>     
        </tr>
        </#list>
    </#if>
	<#-- bugfix:BANMA-287 人数不够10行情况下显示10行数据-->
	<#if lineNumber <10>
	<#list lineNumber..10 as line>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</#list>
	</#if>
 </tbody>
</table>



<#if (view?exists && (view.riders?size > 0)) && page?exists >
    <#assign leftNum = page.leftNum>
    <#assign rightNum = page.rightNum>
    <#if rightNum gt leftNum>
		<ul class="pagination" style="float: right;">
		    <li <#if page.currentPage == 1>class="disabled"</#if>><a id = "prev" data-page = "${page.currentPage}" href=<#if page.currentPage == 1>"javascript:void(0);"<#else>"riderstate?pageNo=${page.currentPage-1}&pageSize=20"</#if>>&laquo;</a></li>
		    <#list leftNum..rightNum as p>
		        <li <#if p == page.currentPage>class="active"</#if>><a name = "pageCode" href="riderstate?pageNo=${p}&pageSize=20">${p}</a></li>
		    </#list>
		    <li <#if page.currentPage == page.totalPage>class="disabled"</#if>><a id= "next" data-page = "${page.currentPage}" href=<#if page.currentPage == page.totalPage>"javascript:void(0);"<#else>"riderstate?pageNo=${page.currentPage+1}&pageSize=20"</#if>>&raquo;</a></li>
		</ul>
	</#if>
</#if>

</div>

