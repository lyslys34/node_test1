<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">


<content tag="javascript">/riderstate/waybilldetail.js</content>
<content tag="cssmain">/riderstate/riderstate.css</content>
<content tag="pageName">waybilldetail</content>

<div id="main-container">

<ul class="nav nav-tabs h50"  style="height:50px">
<li class="active"  style="top:7px; border-bottom:solid 1px #ffffff;"><a href="javascript:void(0);">配送员状态</a></li>
   <#--<li value="30"><a href="/dispatch#30"> </a></li>-->
   <#--<li value="50"><a href="/dispatch#50"> </a></li>-->
</ul>
<br/>
<div>
	<a href="/partner/riderstate/riderstate">全部配送员</a>
	<#if isFinish>
	><a href="/partner/riderstate/waybilllist?uid=${user.id}">${user.name}的今日已完成订单</a>
	</#if>
	>订单详情
</div>
<div id="detailCon" bid='${bid}'></div>

</div>
