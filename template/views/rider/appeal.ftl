<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/rider/rider_appeal.js</content>
<content tag="cssmain">/rider/rider_attendance.css</content>

<div id="main-container">
	<div id="appealDetail"></div>
	<div id="confirmBox" class="popup">
		<p class="info"><i class="fa fa-info-circle"></i>提示</p>
		<div class="content"></div>
		<div class="btn_box">
			<button class="btn btn-sm btn-danger sure">确认</button>
			<button class="btn btn-sm btn-success cancel">取消</button>
		</div>
	</div>
	<div id="rejectBox" class="popup">
		<p class="info"><i class="fa fa-info-circle"></i>提示</p>
		<textarea id="reason" placeholder="请输入拒绝理由"></textarea>
		<div class="btn_box">
			<button class="btn btn-sm btn-danger sure">确认</button>
			<button class="btn btn-sm btn-success cancel">取消</button>
		</div>
	</div>
</div>

<script id="J_detail_tpl" type="text/tpl">
	<table id="detailTable">
		<tr>
			<td>骑手姓名:</td>
			<td>{{riderName}}</td>
		</tr>
		<tr>
			<td>考勤异常日期:</td>
			<td>{{attendanceDate}}</td>
		</tr>
		<tr>
			<td>申诉为:</td>
			<td>{{appealTo}}</td>
		</tr>
		<tr>
			<td>原因描述:</td>
			<td>{{appealReason}}</td>
		</tr>
		<tr>
			<td>证明:</td>
			<td><img src="{{appealProve}}"></td>
		</tr>
		<tr>
			<td>提交时间:</td>
			<td>{{appealTime}}</td>
		</tr>
		<tr>
			<td>修改时间:</td>
			<td>{{modifyTime}}</td>
		</tr>
		<tr>
			<td>骑手轨迹:</td>
			<td><a class="link" target="_blank" href="http://dispatch.peisong.meituan.com/partner/dispatch/riderTrack?riderPhone={{riderMobile}}&date={{attendanceDate}}">点击查看</a></td>
		</tr>
		<tr>
			<td>订单详情:</td>
			<td><a class="link" target="_blank" href="/partner/statistics/dailyData?riderName={{riderName}}&riderId={{riderId}}&ts={{attendanceDate}}&te={{attendanceDate}}">点击查看</a></td>
		</tr>
		<tr>
			<td>申诉记录:</td>
			<td>
				<table class="records table">
					<tr>
						<th class="r_item">姓名</th>
						<th class="r_item">处理时间</th>
						<th class="r_item">处理内容</th>
						<th class="r_item" width="50%">原因</th>
					</tr>
					{{each approvalRecords as record}}
					<tr>
						<td class="r_item">{{record.approvalBy}}</td>
						<td class="r_item">{{record.approvalTime}}</td>
						<td class="r_item">{{record.approvalResult}}</td>
						<td class="r_item">{{record.approvalReason}}</td>
					</tr>
					{{/each}}
				</table>
			</td>
		</tr>
		<tr>
			<td>等待处理:</td>
			<td>{{approvalingUserName}}</td>
		</tr>
	</table>
	{{if approvalShow}}
	<div class="approve_btns">
		<button class="btn btn-sm btn-success">通过</button>
		<button class="btn btn-sm btn-danger">驳回</button>
	</div>
	{{/if}}
</script>