<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/rider/rider_check.js</content>
<content tag="cssmain">/rider/rider_check.css</content>


    <div id="wrapper">

            <div id="main-container">
                <div class="padding-md">

                    <#if user.checked==4 && unCheckReasons?? && (unCheckReasons?size>0) >
                        <div class="panel panel-default package-detail" style="background-color:#FFFAFA">
                            <div class="row" style="margin-left: 15px;margin-top:10px;">
                                <strong style="font-size:16px;line-height:2">驳回原因:</strong>
                                <ul style="list-style-type:disc;margin-left: 15px;line-height:2">
                                    <#list unCheckReasons as reason>
                                        <li>${reason}</li>
                                    </#list>
                                </ul>
                            </div>
                        </div>
                    </#if>

                    <div class="panel panel-default package-detail">
                    	<input type="hidden" name="pageNo" id="pageNo" value="${pageNo !'1'}"/>
                        <div class="panel-heading clearfix">
                            <input type="hidden" value="${user.bmUserId!''}" name="userId">
                            <input type="hidden" value="${user.id!''}" name="id">
                            <div class="col-md-6">
                                <div class="row f-row">
                                    <span class="fl strong" style="font-size:17px"><strong>姓名：</strong></span>
                                    <span class="fl" style="font-size:17px">${user.name!''}</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row f-row">
                                    <span class="fl strong" style="font-size:17px"><strong>身份证号：</strong></span>
                                    <span class="fl" style="font-size:17px">${user.cardNo !''}</span>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="panel panel-default package-detail">

                        <div class="panel-body">
                            <div class="row_item">
                                <div class="row_tit">手持身份证照</div>
                                <div class="ibox" id="ibox1">
                                    <div class="ibox_tool">
                                        <i class="tool_btn fa fa-plus" data-type="plus"></i>
                                        <i class="tool_btn fa fa-minus" data-type="minus"></i>
                                        <i class="tool_btn fa fa-rotate-right" data-type="right"></i>
                                        <i class="tool_btn fa fa-rotate-left" data-type="left"></i>
                                        <i class="tool_btn fa fa-arrows-h" data-type="horiz"></i>
                                    </div>
                                    <div class="ibox_wrapper">
                                        <img class="photo" src="${user.faceUrl !''}" data-horiz="0" data-scale="1" data-rotate="0" draggable="false">
                                    </div>
                                </div>
                            </div>
                            <div class="row_item">
                                <div class="row_tit">身份证正面照</div>
                                <div class="ibox" id="ibox2">
                                    <div class="ibox_tool">
                                        <i class="tool_btn fa fa-plus" data-type="plus"></i>
                                        <i class="tool_btn fa fa-minus" data-type="minus"></i>
                                        <i class="tool_btn fa fa-rotate-right" data-type="right"></i>
                                        <i class="tool_btn fa fa-rotate-left" data-type="left"></i>
                                        <i class="tool_btn fa fa-arrows-h" data-type="horiz"></i>
                                    </div>
                                    <div class="ibox_wrapper">
                                        <img class="photo" src="${user.idCardUrl !''}" data-horiz="0" data-scale="1" data-rotate="0" draggable="false">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div align="right" style="padding-right:15%"><button id="disagree" class="btn btn-default" style="margin-right:30px;">驳回</button>
                        <button id="approve" class="btn btn-default" >通过</button>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="disaggresUser" >
                <div class="modal-dialog">
                    <div class="modal-content" style="width:400px;">
                        <div class="modal-header">
                            <h4>驳回原因</h4>
                        </div>
                        <div class="modal-body">
                            <div id="alert_bind_error"></div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remark" id="optionsRadios1" value="人员姓名、身份证号和图片信息不一致">
                                    人员姓名、身份证号和图片信息不一致
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remark" id="optionsRadios2" value="手持身份证照片不清晰">
                                    手持身份证照片不清晰
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remark" id="optionsRadios3" value="身份证照片不清晰" >
                                    身份证照片不清晰
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remark" id="optionsRadios4" value="未上传手持身份证照片" >
                                    未上传手持身份证照片
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remark" id="optionsRadios6" value="未上传身份证照片" >
                                    未上传身份证照片
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remark" id="optionsRadios7" value="两张照片位置颠倒" >
                                    两张照片位置颠倒
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remark" id="optionsRadios5" value="" >
                                    其它
                                </label>
                            </div>

                            <div id="showOtherDesc">

                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm" id="cancelCheck">取消</button>
                                <input type="submit" id="disaggreUserBtn1" value="确认" class="btn btn-danger btn-sm" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="aggresUserConfirm">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;&nbsp;提示
                        </div>
                        <div class="modal-body">
                            <h4 style="text-align:center">确认审核通过吗？</h4>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" id="aggreUserBtn1" value="确定" class="btn btn-danger btn-sm" />
                            <button class="btn btn-success btn-sm" id="cancel">取消</button>

                        </div>

                    </div>
                </div>
            </div>

        </div>
        <script>
            document.title = '身份证快照';
        </script>
