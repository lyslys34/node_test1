<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="cssmain">/rider/rider_save_new.css</content>

<div id="main-container">
    <div class="panel panel-default">
        <div id="alert_error"></div>

        <input type="hidden" name="id" value="${user.id}" id="id"/>
        <input type="hidden" name="originOrgId" id="originOrgId" value="${user.orgId ! '-1'}" />
        <input type="hidden" name="originOrgType" value="${user.orgType !'0'}" id="originOrgType"/>
        <input type="hidden" name="orgType" value="${user.orgType !'0'}" id="orgType"/>
        <input type="hidden" name="originLevelType" id="originLevelType" value="${user.levelType ! '-1'}" />
        <input type="hidden" id="orgIdTemp" value="${user.orgId ! '-1'}" />
        <input type="hidden" id="orgTypeTemp" value="${user.orgType !'0'}" />



        <#if user.checked==2 && user.unCheckReasons?exists && ( user.unCheckReasons?size > 0 ) >
            <div class="panel panel-default package-detail" style="background-color:#FFFAFA">
                <div class="row" style="margin-left: 15px;margin-top:10px;">
                    <strong style="font-size:16px;line-height:2">您的身份审核不通过</strong><br/>
                    <strong style="font-size:14px;line-height:2">认证审核不通过原因:</strong>
                    <ul style="list-style-type:disc;margin-left: 15px;line-height:2">
                        <#list user.unCheckReasons as reason>
                            <li>${reason}</li>
                        </#list>
                    </ul>
                    <p style="font-size:14px;line-height:2">请重新提交身份信息资料，完成身份认证</p>
                </div>
            </div>
        </#if>
        <#if user.headPortraitChecked==2>
            <div class="panel panel-default package-detail" style="background-color:#FFFAFA">
                <div class="row" style="margin-left: 15px;margin-top:10px;">
                    <strong style="font-size:16px;line-height:2">您的头像审核不通过</strong><br/>
                    <strong style="font-size:14px;line-height:2">审核不通过原因如下:</strong>
                    <ul style="list-style-type:disc;margin-left: 15px;line-height:2">
                            <li>${user.headPortraitUnCheckRaasons!''}</li>
                    </ul>
                    <p style="font-size:14px;line-height:2">请重新上传骑手头像，通过后，顾客下单即可看到骑手头像。</p>
                </div>
            </div>
        </#if>

        <#include "rider_save_common.ftl">

        <div class="panel-footer text-right">
            <button type="button" class="btn btn-success" onclick="javasctipt:history.go(-1);">返回</button>&nbsp;&nbsp;
            <button type="button" class="btn btn-success" id="updateUserSubmit">保存</button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <#-- <a href="/rider/list" class="btn btn-default">取消</a> -->
        </div>

    </div>
</div>

<input type = "hidden" id = "curOrgId" value="${curOrgId !''}">
<input type = "hidden" id = "role"  value = "${currentRoleCode !''}"/>

<script type="text/javascript" src="${staticPath}/js/page/utils.js?ver=${staticVersion !''}"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="/static/js/lib/cutout.js"></script>
<script type="text/javascript" src="${staticPath}/js/page/rider/rider_detail.js"></script>

<script>
    document.title = '编辑骑手信息';
    $('select').select2();
</script>
