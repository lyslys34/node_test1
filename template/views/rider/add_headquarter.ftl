<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/rider/add_headquarter.js</content>
<content tag="pageName">add_headquarter</content>

<div id="main-container">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2" style="width: 150px;">
                    <div class="form-group">
                        <label class="control-label">mis</label>
                        <input type="text" class="form-control input-sm parsley-validated" id="misId" name="misId" style="width:100px;">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">姓名</label>
                        <input type="text" class="form-control input-sm parsley-validated" id="name" name="name" value="">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">组织ID</label>
                        <input type="text" class="form-control input-sm parsley-validated" id="orgId" name="orgId" value="">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">手机号</label>
                        <input type="text" class="form-control input-sm parsley-validated" id="mobile" name="mobile" value="">
                    </div>
                </div>

            </div>
            <button id = "send" type="submit" class="btn btn-success" style="float: left;">添加</button>
        </div>
    </div>
</div>