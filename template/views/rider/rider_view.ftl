<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">
    <content tag="cssmain">/delivery.css</content>


<div id="main-container">
  <div class="panel panel-default">

      <input type="hidden" name="id" value="${user.id}"/>
      <div class="panel-heading" style="font-size:17px;">
          <span>人员基本信息</span>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-4" style="width:250px;">
            <div class="form-group" style="width:250px;">
              <label class="control-label">姓名</label>
                ${user.name!''}
            </div><!-- /form-group -->
          </div>
          <div class="col-md-4" style="width:250px;">
            <div class="form-group" style="width:250px;">
              <label class="control-label">手机号</label>
                ${user.phone!''}
            </div><!-- /form-group -->
          </div>
          <div class="col-md-4" style="width:250px;">
            <div class="form-group" style="width:250px;">
               <label class="control-label">职位</label>
                ${user.roleName!''}
               </div><!-- /form-group -->
          </div>
          <div class="col-md-4" style="width:250px;">
            <div class="form-group" style="width:250px;">
               <label class="control-label">审核状态</label>
                <#if user.checked==1>审核通过<#elseif user.checked ==2>驳回<#if user.unCheckReason?? && user.unCheckReason!='' >(${user.unCheckReason!'无'})</#if><#elseif user.checked ==-1>未提交审核<#else>待审核</#if>
            </div><!-- /form-group -->
          </div>
        </div>
        <div class="row">
            <div class="col-md-4" style="width:250px;">
                <div class="form-group" style="width:250px;">
                    <label class="control-label">城市</label>
                    ${user.cityName!''}
                </div><!-- /form-group -->
            </div>
            <div class="col-md-4" style="width:250px;">
                <div class="form-group" style="width:250px;">
                    <label class="control-label">组织类型</label>
                        ${user.orgTypeName!''}
                </div><!-- /form-group -->
            </div>
            <div class="col-md-4" style="width:250px;">
                <div class="form-group" style="width:250px;">
                    <label class="control-label">组织名称</label>
                    ${user.orgName!''}
                </div><!-- /form-group -->
            </div>
        </div>
          <div class="row">
              <div class="col-md-4" style="width:500px;">
                  <div class="form-group">
                      <label class="control-label">最后一次上线时间</label>
                      ${user.lastestOnLineTime!''}
                  </div><!-- /form-group -->
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">最后一次接单时间</label>
                      ${user.lastestGrabTime!''}
                  </div><!-- /form-group -->
              </div>
          </div>
      </div>
      </div>

      <#--<#if user.orgType==4&&user.roleCode==1001&&user.checked!=-1>-->
          <div class="panel panel-default package-detail">
              <div class="panel-heading" style="background-color:#3c8dbc;color:#fff;font-size:16px;">
                  <span>身份证信息</span>
              </div>
              <div class="panel-body">
                  <div class="row">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label">身份证号：</label>
                              ${user.cardNum!''}
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-4" style="width:50%;text-align:center;">
                          <div class="form-group" >
                              <label class="control-label" >手持身份证照</label>
                          </div>
                      </div>
                      <div class="col-md-4" style="width:50%;text-align:center;">
                          <div class="row f-row">
                              <label class="control-label" >身份证正面照</label>
                          </div>

                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-4" style="width:50%;text-align:center;">
                        <img src="${user.selfPicPath!''}" style="width:500px; height:650px;">
                      </div>
                      <div class="col-md-4" style="width:50%;text-align:center;">
                          <img src="${user.frontPicPath!''}" style="width:500px; height:650px;">
                      </div>

              </div>
          </div>
      <#--</#if>-->
          <#if user.staffOpLogViewList?exists && (user.staffOpLogViewList?size>0)>
              <div class="panel panel-default package-detail">
                  <div class="panel-heading" style="background-color:#3c8dbc;color:#fff;font-size:16px;">
                      <span>黑名单信息</span>
                  </div>
                  <div class="panel-body">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label">当前状态：<#if user.blacklistStatus==1>黑名单<#elseif user.blacklistStatus==0>非黑名单<#else>未知</#if></label>
                          </div>
                      </div>
                      <table class="table table-bordered" style="text-align:center">
                          <thead>
                          <tr>
                              <th style="text-align:center">序号</th>
                              <th style="text-align:center">操作</th>
                              <th style="text-align:center">操作原因</th>
                              <th style="text-align:center">操作人</th>
                              <th style="text-align:center">操作时间</th>
                          </tr>
                          </thead>
                          <tbody>
                              <#list user.staffOpLogViewList as opLog>
                                  <tr>
                                      <td>${(user.staffOpLogViewList?size)-opLog_index}</td>
                                      <td><#if opLog.opType==1>拉入黑名单<#elseif opLog.opType==2>移出黑名单<#else>未知</#if></td>
                                      <td>${opLog.description!'无'}</td>
                                      <td>${opLog.opUname!''}</td>
                                      <td>${opLog.ctime!''}</td>
                                  </tr>
                              </#list>
                          </tbody>
                      </table>
                  </div>
              <div>
          </#if>
      <div class="panel-footer text-right">
          <#--<button type="button" class="btn btn-success" onclick="javasctipt:history.go(-1);">返回</button>&nbsp;&nbsp;-->
          <#--<#if (roleCode?exists&& roleCode == 4001 && user.isDimission()) || user.accountSource != 1><#else><a href="/rider/update/${user.orgId!'-1'}/${user.id ! '0'}" class="btn btn-success">编辑</a></#if>-->
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a href="/rider/list" class="btn btn-default">返回</a>
        <#--<button type="button" class="btn btn-success" onclick="javasctipt:history.go(-1);">返回</button>&nbsp;&nbsp;-->
      </div>
  </div>
</div>
<script>document.title = '骑手信息';</script>