<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">

<style>
    input[type="radio"] {
        opacity: 0
    }
</style>
<div id="main-container">
    <div class="panel panel-default">
        <form method="post" class="no-margin" id="formValidate1" data-validate="parsley" novalidate=""
              action="/rider/doUpdate">
            <input type="hidden" name="id" value="${user.id}"/>
            <input type="hidden" name="originOrgId" id="originOrgId" value="${user.orgId ! '-1'}" />
            <input type="hidden" name="orgType" value="${user.orgType !'0'}" id="orgType"/>

            <#if user.unCheckReason?? && user.unCheckReason!='' >
                <div class="row">
                    <h4 style="color:red;margin-left:15px;">驳回原因：${user.unCheckReason!'无'}</h4>
                </div>
            </#if>

            <div class="panel-heading" style="background-color:#3c8dbc;color:#fff;font-size:16px;">
                <span>基本信息</span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">姓名</label>
                            <input type="text" class="form-control input-sm parsley-validated" id="userName"
                                   name="userName" value="${user.name!''}" data-required="true">
                        </div>
                        <!-- /form-group -->
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">手机号</label>
                            <input type="text" class="form-control input-sm parsley-validated" id="userPhone"
                                   name="userPhone" value="${user.phone!''}" data-required="true">
                        </div>
                        <!-- /form-group -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">性别</label>
                            <br/>
                            <label class="label-radio inline">
                                <input type="radio" name="gender" <#if !user.gender?exists || user.gender == 1 >checked</#if> value="1">
                                <span class="custom-radio"></span>
                                男
                            </label>
                            <label class="label-radio inline">
                                <input type="radio" name="gender" <#if user.gender?exists && user.gender == 2 >checked</#if> value="2">
                                <span class="custom-radio"></span>
                                女
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">员工类型</label>
                            <br/>
                            <label class="label-radio inline">
                                <input type="radio" name="jobType" <#if !user.jobType?exists || user.jobType == 1 >checked</#if> value="1">
                                <span class="custom-radio"></span>
                                全职
                            </label>
                            <label class="label-radio inline">
                                <input type="radio" name="jobType" <#if user.jobType?exists && user.jobType == 2 >checked</#if> value="2">
                                <span class="custom-radio"></span>
                                兼职
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">职位</label>
                            <select class="form-control input-sm" id="roleType" name="roleType">
                            <#if roleVoList?exists>
                                <#list roleVoList as role>
                                    <option value="${role.code !'0'}" <#if role.code == user.roleCode >
                                            selected="selected" </#if>>${role.name !''}</option>
                                </#list>

                            </#if>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">所在组织</label>
                            <select class="form-control input-sm" id="orgId" name="orgId">
                                <#if orgs?exists>
                                    <#list orgs as org>
                                        <option value="${org.orgId !''}" orgType="${org.type}" <#if org.orgId == user.orgId >
                                                selected="selected" </#if> >${org.orgName !''}</option>
                                    </#list>
                                </#if>
                            </select>
                        <#--<input type="text" class="form-control input-sm parsley-validated" readonly id="orgName" name="orgName" value="${user.orgName!''}" data-required="true" >-->
                        </div>
                        <!-- /form-group -->
                    </div>
                </div>

            </div>

            <div class="panel-heading" style="background-color:#3c8dbc;color:#fff;font-size:16px;">
                <span>身份证信息</span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="form-group">
                                <label class="control-label">身份证号(<span style="color:#ff0000;">请务必准确填写，否则影响配送费结算</span>)</label>
                                <input type="text" class="form-control input-sm parsley-validated" id="idcardNo"
                                       name="idcardNo" value="${user.cardNum !''}" data-required="true">
                            </div>
                        <#--<input type="button" class="btn btn-default" value="上传" id="fileupload_btn_1">-->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" style="width:50%">
                        <div class="form-group">
                            <label class="control-label">手持身份证照(<span style="color:#ff0000;">请确保身份证信息清晰、无遮挡，照片正向放置</span>)</label>

                            <div style=" width:500px; height:650px; border:1px solid #eeeeee;">
                                <img src="${user.selfPicPath !''}" style="width:500px; height:650px; padding:5px;"/>
                                <input type="button" value="点击上传" class="btn btn-default js_uploadbtn"
                                       style="position:absolute; z-index: 100; left:230px; top:550px;"/>
                                <input type="hidden" class="js_upload_back_url" name="faceUrl" id="js_faceUrl" value="${user.selfPicPath !''}"/>
                                <input type="file" class="js_fileupload" name="imgFile" style="display: none"/>
                            </div>
                        <#--<input type="button" class="btn btn-default" value="上传" id="fileupload_btn_1">-->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">身份证正面照(<span style="color:#ff0000;">请确保身份证信息清晰、无遮挡，照片正向放置</span>)</label>

                            <div style=" width:500px; height:650px; border:1px solid #eeeeee;">
                                <img src="${user.frontPicPath !''}" style="width:500px; height:650px; padding:5px;"/>
                                <input type="button" value="点击上传" class="btn btn-default js_uploadbtn"
                                       style="position:absolute; z-index: 100; left:230px; top:550px;"/>
                                <input type="hidden" class="js_upload_back_url" name="idcardUrl" id="js_cardUrl" value="${user.frontPicPath !''}"/>
                                <input type="file" class="js_fileupload" name="imgFile" style="display: none"/>
                            </div>
                        <#--<input type="button" class="btn btn-default" value="上传" id="fileupload_btn_1">-->
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-footer text-right">
            <#--<button type="button" class="btn btn-success" onclick="javasctipt:history.go(-1);">返回</button>&nbsp;&nbsp;-->
                <button type="button" class="btn btn-success" id="js_submit">保存</button>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="/rider/list" class="btn btn-default">取消</a>
            </div>
        </form>
    </div>
</div>

<input type = "hidden" id = "curOrgId" value="${curOrgId !''}">
<input type = "hidden" id = "role"  value = "${currentRoleCode !''}"/>

<script type="text/javascript" src="${staticPath}/js/page/utils.js?ver=${staticVersion !''}"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="${staticPath}/js/page/rider/rider_detail.js"></script>

<script>
    document.title = '编辑骑手信息';
</script>