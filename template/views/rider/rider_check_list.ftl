<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/rider/rider_check.js</content>
<content tag="cssmain">/rider/rider_check.css</content>


<div id="main-container">
    <div class="panel panel-default table-responsive" style="margin-top:10px;padding:10px 10px;">
    <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post"
          action="/rider/noChecklist">
        <input type="hidden" value="0" name="checked" >
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label class="control-label">姓名:</label>
                    <input type="text" class="form-control input-sm parsley-validated" id="userName"
                           name="userName" value="${userName !''}" data-required="true" data-minlength="8">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label class="control-label">手机号:</label>
                    <input type="text" class="form-control input-sm parsley-validated" id="userPhone"
                           name="userPhone" value="${userPhone !''}" data-required="true" data-minlength="8">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label class="control-label">组织名称</label>
                    <select class="form-control input-sm" id="orgId" name="orgId">
                        <option value="0">全部</option>
                    <#if orgMap?exists>
                        <#list orgMap?keys as k>
                            <option value="${k?string !'0'}"
                                    <#if RequestParameters.orgId?exists && RequestParameters.orgId == k>selected="selected" </#if> >${orgMap[k] !''}</option>
                        </#list>
                    </#if>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label class="control-label">城市</label>
                    <select class="form-control input-sm" id="cityId" name="cityId">
                        <option value="0">全部</option>
                    <#if cityMap?exists>
                        <#list cityMap?keys as k>
                            <option value="${k?string !'0'}"
                                    <#if RequestParameters.cityId?exists && RequestParameters.cityId == k>selected="selected" </#if> >${cityMap[k] !''}</option>
                        </#list>
                    </#if>
                    </select>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="padding-top:20px">
                    <button type="submit" class="btn btn-success">查询</button>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="padding-top:20px;">
                        <button type="button" class="btn btn-danger"><a href="/log/auditLog" style="color:white" target="_blank">查看操作记录</a></button>
                </div>
            </div>
        </div>
    </form>
    </div>
    <div class="btn btn-success" id="multi_pass">审核通过</div><span>&nbsp;&nbsp;已选择了<span id="se_num">0</span>人</span>
    <br>
    <br>
    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="checkTable">
            <thead>
            <tr>
                <th>
                    <input style="opacity: 1; margin-top:-14px; margin-left:2px;" type="checkbox" name="check-All" class="all_check_box" id="check-All" value="allcheck" />
                </th>
                <th style="text-align:center;">姓名</th>
                <th style="text-align:center;">手机号</th>
                <th style="text-align:center;">身份证号码</th>
                <th style="text-align:center;">组织名称</th>
                <th style="text-align:center;">组织类型</th>
                <th style="text-align:center;">身份证快照</th>
                <th style="text-align:center;">审核类型</th>
                <th style="text-align:center;">创建时间</th>
                <th style="text-align:center;">操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
                <#if userList??>
                    <#list userList as user>
                        <tr>
                            <td>
                                <input style="opacity: 1;" type="checkbox" name="check-user" class="user_check_box" value="nocheck" />
                            </td>
                            <td>${user.name!''}</td>
                            <td>${user.mobile!''}</td>
                            <td>${user.cardNo!''}</td>
                            <td>${user.orgName!''}</td>
                            <#--<td><#if user.orgType==1>自建<#elseif user.orgType==2>加盟<#elseif user.orgType==4>众包</#if></td>-->
                            <td>${user.orgTypeName!''}</td>
                            <td><a href="/rider/noCheckPicInfo/${user.bmUserId}?pageNo=${pageNo}" class="aStyle" target="_blank">查看</td>
                            <td><#if user.checked==0>首次提交<#elseif user.checked==3>审核通过后修改提交<#elseif user.checked==4>驳回后重新提交</#if></td>
                            <td>${user.utime!''}</td>
                            <td userId="${user.bmUserId!''}" recordId="${user.id!''}"><button class="btn btn-default disagree" >驳回</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-default approve" >通过</button></td>
                        </tr>
                    </#list>
                </#if>
            </tbody>
        </table>

        <div class="modal fade" id="disaggresUser" >
            <div class="modal-dialog">
                <div class="modal-content" style="width:400px;">
                    <div class="modal-header">
                        <h4>驳回原因</h4>
                    </div>
                    <div class="modal-body">
                        <div id="alert_bind_error"></div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remark" id="optionsRadios1" value="人员姓名、身份证号和图片信息不一致">
                                人员姓名、身份证号和图片信息不一致
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remark" id="optionsRadios2" value="手持身份证照片不清晰">
                                手持身份证照片不清晰
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remark" id="optionsRadios3" value="身份证照片不清晰" >
                                身份证照片不清晰
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remark" id="optionsRadios4" value="未上传手持身份证照片" >
                                未上传手持身份证照片
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remark" id="optionsRadios6" value="未上传身份证照片" >
                                未上传身份证照片
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remark" id="optionsRadios7" value="两张照片位置颠倒" >
                                两张照片位置颠倒
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remark" id="optionsRadios5" value="" >
                                其它
                            </label>
                        </div>

                        <div id="showOtherDesc">

                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm" id="cancelCheck">取消</button>
                            <input type="submit" id="disaggreUserBtn" value="确认" class="btn btn-danger btn-sm" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="aggresUserConfirm">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;&nbsp;提示
                    </div>
                    <div class="modal-body">
                        <h4 style="text-align:center">确认审核通过吗？</h4>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" id="aggreUserBtn" value="确定" class="btn btn-danger btn-sm" />
                        <button class="btn btn-success btn-sm" id="cancel">取消</button>

                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="multiAggresUserConfirm">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;&nbsp;提示
                    </div>
                    <div class="modal-body">
                        <h4 style="text-align:center">确认审核通过吗？</h4>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" id="multiAggreUserBtn" value="确定" class="btn btn-danger btn-sm" />
                        <button class="btn btn-success btn-sm" id="multiCancel">取消</button>

                    </div>

                </div>
            </div>
        </div>

        <div style="float: left; padding: 10px 15px;"><span>共${recordCount!'0'}项</span></div>
        <div style="float: right; margin-top: 10px;">
            <span>跳转到</span>
            <input type="text" id="pageNum" name="pageNum" value="" data-required="true" data-minlength="8" style="width:30px;">
            <span>页</span>
            <button type="button" class="btn btn-success btn-sm js_btn_jump button_width">跳转</button>
        </div>
        <#import "../page/pager.ftl" as q>
            <#if recordCount??>
                <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/rider/noChecklist"/>
            </#if>
    </div>
</div>
<script>
    document.title = '人员审核';
    $('select').select2();
</script>