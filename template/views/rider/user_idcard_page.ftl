<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">


<content tag="javascript">/rider/user_idcard_page.js</content>
<style type="text/css">
    #main-container{
        padding: 10px 20px;
    }
    .col-md-4{width: 260px;}
    a:hover { text-decoration: underline; font-weight: bold; color:#7FC7A6; }
</style>

<div id="main-container">
    <div class="panel panel-default">
    	<div id="alert_error">
    	<#if idCard?? && !rider??>
        <div class='alert alert-danger' role='alert'> 系统无此身份证号 </div>
        </#if>
    	</div>
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post"
              action="/rider/getUserByIdCard">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label">身份证号</label>
                            <input type="text" class="form-control input-sm parsley-validated" id="idCard"
                                   name="idCard" value="${idCard!''}" data-required="true" >
                        </div>
                    </div>
                    <div class="col-md-4">
                    	<div class="form-group" style="padding-top:18px">
                        <button type="submit"  class="btn btn-success">查询</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
            	<th>站点名称</th>
                <th>姓名</th>
                <th>手机号</th>
                <th>职位</th>
            </tr>
            </thead>
            <tbody>
                <#if rider??>
				<tr <@output_props obj = rider />>
				<td>${rider.orgName!''}</td>
                <td><a target="_blank" href="/rider/list?userPhone=${rider.account?url}&jobStatus=1">${rider.name!''}</a></td>
                <td>${rider.account!''}</td>
                <td>${rider.roleName!''}</td>
                </tr>
                </#if>
            </tbody>
        </table>
        

</div>
<script type="text/javascript" src="${staticPath}/js/page/utils.js?ver=${staticVersion !''}"></script>
<script>
    document.title = '身份证号查询';
    $('select').select2();
</script>