<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">

<style type="text/css">
    #main-container{
        padding: 10px 20px;
    }
</style>

<div id="main-container">
    <#if RequestParameters.f?exists && RequestParameters.f == 'headfranchisee'>
        <@headFranchiseeNavi org 3></@headFranchiseeNavi>
    <#else>
        <@joinOrgNavi org is_org_manager 3></@joinOrgNavi>
    </#if>
    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>姓名</th>
                <th>手机号</th>
                <th>性别</th>
                <th>所在组织</th>
                <th>职位</th>
                <th>员工类型</th>
                <th>员工状态</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#if riderList?exists>
                <#list riderList as rider>
                <tr <@output_props obj = rider />>
                    <td class="name"><a href="/rider/list?userName=${rider.name!''}&userPhone=${rider.phone}" target="_blank">${rider.name!''}</a></td>
                    <td>${rider.phone!''}</td>
                    <td>
                        <#if rider.gender?exists && rider.gender == 1>男
                        <#elseif rider.gender?exists && rider.gender == 2>女
                        </#if>
                    </td>
                    <td>${rider.orgName!''}</td>
                    <td>${rider.roleName!''}</td>
                    <td>
                        <#if rider.jobType?exists && rider.jobType == 1>全职
                        <#elseif rider.jobType?exists && rider.jobType == 2>兼职
                        </#if>
                    </td>
                    <td>
                        <#if rider.jobStatus?exists && rider.jobStatus==1>
                            在职
                        <#else>
                            离职
                        </#if>
                    </td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>
	    <#if recordCount?? && recordCount gt 0><div class="panel-footer clearfix pull-left">共${recordCount!'0'}条</div></#if>
	    
	    <#import "../page/pager.ftl" as q>
	    <#if recordCount??>
	        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/rider/franchiseeRiderList"/>
	    </#if>

    </div>
</div>
<script>
    document.title = '人员列表';
</script>