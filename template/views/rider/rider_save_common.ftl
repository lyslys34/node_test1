<style media="screen">
</style>


<div class="panel-heading" style="font-size:20px;">
    <span>人员基本信息</span><span style="font-size:14px;color:#FF6666;margin-left:60px;">新建人员需经总部审核通过后才生效。18：00前提交的申请，可当日完成审核。18：00后提交的申请，次日完成审核。</span>
</div>
<div class="panel-body">
    <div class="title">身份信息</div>
    <br/>
    <dl class="dl-horizontal">
        <dt><span class="star">*</span>姓名：</dt>
        <dd><input type="text" class="form-control input-sm parsley-validated" id="userName"
                   name="userName" <#if user?? && (user.name?exists)> value="${user.name!''}" </#if>  data-required="true"></dd>
    </dl>
    <dl class="dl-horizontal">
        <dt><span class="star">*</span>性别：</dt>
        <dd>
            <select class="form-control input-sm" id="gender" name="gender">
                <option value="1" <#if user?? && (user.gender?exists) && (user.gender==1)>
                        selected="selected" </#if>>男</option>
                <option value="2" <#if user?? && (user.gender?exists) && (user.gender==2)>
                        selected="selected" </#if>>女</option>
            </select>
        </dd>
    </dl>
    <dl class="dl-horizontal">
        <dt><span class="star">*</span>身份证号码：</dt>
        <dd>
            <input type="text" class="form-control input-sm parsley-validated" id="idcardNo"
                   name="idcardNo" <#if user?? && (user.cardNum?exists)> value="${user.cardNum !''}" </#if>  data-required="true">
        </dd>
    </dl>
    <dl class="dl-horizontal">
        <dt style="margin-top: 23px;"><span class="star">*</span>手持身份证照：</dt>
        <dd>
            <div style="width:1100px;">
                <div style=" width:400px; height:280px; border:1px solid #eeeeee; float:left; margin-top: 23px;">
                <#if user?? && (user.selfPicPath?exists)><img src="${user.selfPicPath !''}" style="width:400px; height:280px; padding:5px;position:absolute;"/>
                    <input type="button" value="点击上传" class="btn btn-default js_uploadbtn"
                           style="position:absolute; z-index: 100; margin-left:160px; margin-top:120px;"/>
                <#else>
                    <img src="#" style="width:400px; height:280px; padding:5px; display:none;position:absolute;"/>
                    <input type="button" value="点击上传" class="btn btn-default js_uploadbtn"
                           style="position:absolute; z-index: 100; margin-left:160px; margin-top:120px;"/>
                </#if>
                    <input type="hidden" class="js_upload_back_url" name="faceUrl" id="js_faceUrl" <#if  user?? && (user.selfPicPath?exists)> value="${user.selfPicPath !''}" </#if> />
                    <input type="hidden" class="js_upload_back_url_with_watermark" name="faceUrlWithWatermark" id="faceUrlWithWatermark" <#if user?? && (user.faceUrlWithWatermark?exists)> value="${user.faceUrlWithWatermark !''}" </#if>/>
                    <input type="file" class="js_fileupload" name="imgFile" style="display: none"/>
                </div>
                <div style="width:50px; height: 100px; float: left; margin-top:4px;"><span><br/>&nbsp;&nbsp;&nbsp;&nbsp;示例:</span></div>
                <div style="width:400px; height:300px; float:left; padding: 0px;">
                    <span style="margin-left: 65px;">此处为保护个人隐私对示例身份证信息做处理</span>
                    <img src="/static/imgs/face_pic_v2.jpg" style="width:400px; height:289px; padding:5px;" />
                </div>
                <div style="width:250px; float: left; padding-top: 20px;">
                    <p>1、必须为本人身份证</p>
                    <p>2、身份证上所有信息必须真实有效且清晰可见</p>
                    <p>3、手持证件人必须免冠且无遮挡</p>
                    <p>4、支持.jpg,.png,.gif格式不大于5M.</p>
                    <br/>
                    <br/>
                    <p style="color:#ff1ed8;">拍照小tip:伸长手臂,将身份证尽量靠近镜头,焦点对准身份证,就可以拍出清晰的照片哦~~</p>
                </div>
            </div>
        </dd>
    </dl>

    <dl class="dl-horizontal">
        <dt style="margin-top: 23px;"><span class="star">*</span>身份证正面：</dt>
        <dd>
            <div style="width:1100px;">
                <div style=" width:400px; height:280px; border:1px solid #eeeeee; float:left; margin-top: 23px;">
                <#if user?? && (user.frontPicPath?exists)><img src="${user.frontPicPath !''}" style="width:400px; height:280px; padding:5px;position:absolute;"/>
                    <input type="button" value="点击上传" class="btn btn-default js_uploadbtn text-center"
                           style="position:absolute; z-index: 100; margin-left:160px; margin-top:120px;"/>
                <#else>
                    <img src="#" style="width:400px; height:280px; padding:5px; display:none;position:absolute;"/>
                    <input type="button" value="点击上传" class="btn btn-default js_uploadbtn text-center"
                           style="position:absolute; z-index: 100; margin-left:160px; margin-top:120px;"/>
                </#if>
                    <input type="hidden" class="js_upload_back_url" name="idcardUrl" id="js_cardUrl" <#if user?? && (user.frontPicPath?exists)> value="${user.frontPicPath !''}" </#if> />
                    <input type="hidden" class="js_upload_back_url_with_watermark" name="idCardUrlWithWatermark" id="idCardUrlWithWatermark" <#if user?? && (user.idCardUrlWithWatermark?exists)> value="${user.idCardUrlWithWatermark !''}" </#if>/>
                    <input type="file" class="js_fileupload" name="imgFile" style="display: none"/>
                </div>
                <div style="width:50px; height: 100px; float: left; margin-top:20px;"><span>&nbsp;&nbsp;&nbsp;&nbsp;示例:</span></div>
                <div style="width:400px; height:300px; float:left; padding: 0px;">
                    <span style="margin-left: 65px;">此处为保护个人隐私对示例身份证信息做处理</span>
                    <img src="/static/imgs/id_card_pic_v2.jpg" style="width:400px; height:289px; padding:5px;" />
                </div>
                <div style="width:250px; float: left; padding-top: 20px;">
                    <p>1、证件上文字清晰,可识别</p>
                    <p>2、身份证四个边角完整露出</p>
                    <p>3、支持.jpg,.png,.gif格式不大于5M.</p>
                </div>
            </div>
        </dd>
    </dl>

    <div class="title">工作信息</div>
    <br/>
    <dl class="dl-horizontal">
        <dt><span class="star">*</span>手机号：</dt>
        <dd>
            <input type="text" class="form-control input-sm parsley-validated" id="userPhone"
                   name="userPhone" <#if user?? && (user.phone?exists)> value="${user.phone!''}" </#if> data-required="true">
        </dd>
    </dl>
    <dl class="dl-horizontal">
        <dt><span class="star">*</span>员工类型：</dt>
        <dd>
            <select class="form-control input-sm" id="jobType" name="jobType">
                <option value="1" <#if user?? && (user.jobType?exists) && (user.jobType==1)>
                        selected="selected" </#if>>全职</option>
                <option value="2" <#if user?? && (user.jobType?exists) && (user.jobType==2)>
                        selected="selected" </#if>>兼职</option>
            </select>
        </dd>
    </dl>
    <dl class="dl-horizontal">
        <dt><span class="star">*</span>职位：</dt>
        <dd>
            <div style="width:800px;">
                <div style="width:50%; display: inline-block; float: left;">
                    <select class="form-control input-sm" id="roleType" name="roleType" style="width:400px; display: inline-block;" >
                        <#if roleVoList?exists>
                            <#list roleVoList as role>
                                <option value="${role.code !'0'}" <#if user?? && (user.roleCode?exists) && role.code == user.roleCode >
                                        selected="selected" </#if>>${role.name !''}</option>
                            </#list>
                        </#if>
                    </select>
                </div>
                <div style="width:40%; display: inline-block; float: left; margin-left: 10px;">
                    <span data-toggle="tooltip" data-placement="right" data-html="true" data-original-title='<p align="left">1、组织负责人:拥有对烽火台的全部操作权限</p><p align="left">2、加盟站长:仅可查看"调度中心" "业务管理" "数据统计"</p><p align="left">3、财务人员:仅可查看"结算对账" "数据统计"</p><p align="left">4、配送员:使用骑手客户端登录</p><#if is_org_manager?exists && is_org_manager != 1><p align="left">5、角马商家负责人:角马的负责人</p><p align="left">6、自营配送主管:负责处理所管辖下级自营站的全部事务</p><p align="left">7、自营分部助理:协助配送主管完成日常管理工作</p><p align="left">8、自营站长助理:协助自营站长完成日常管理工作</p></#if>'><i class="fa fa-lg fa-question-circle"></i></span>
                </div>
            </div>
        </dd>
    </dl>
    <dl class="dl-horizontal">
        <dt><span class="star">*</span>组织名称：</dt>
        <dd>
            <select class="form-control input-sm inline" id="levelType" name="levelType" style="width:100px;margin-right:20px;">
            <#if levelTypeList?exists>
                <#list levelTypeList as levelType>
                    <option value="${levelType.code !''}">${levelType.name !''}</option>
                </#list>
            </#if>
            </select>

            <select class="form-control input-sm inline" id="orgId" name="orgId" style="width:296px;">
            <#if orgs?exists>
                <#list orgs as org>
                    <option value="${org.orgId !''}" orgType="${org.type}" levelType="${org.levelType!'0'}" <#if user?? && (user.orgId?exists) && (org.orgId == user.orgId) >
                            selected="selected" </#if> >${org.orgName !''}</option>
                </#list>
            </#if>
            </select>
        </dd>
    </dl>
    <dl class="dl-horizontal hide headPortrait">
        <dt style="margin-top: 6px;"><#--<span class="star">*</span>-->骑手头像：</dt>
        <dd>
            <div style="width:1100px;">
                <div>
                    <input id="J_upload_avatar" type="file" style="display: none;" />
                    <button id="J_upload_avatar_click" style="background-color: #0097ff; color: #fff; border-radius: 5px; border-width: 0px; padding: 4px 16px; font-size: 12px;">
                      点击上传
                    </button>
                    <span style="color:red;">可拖拽正方形右下角调整截取范围</sapn>
                </div>

                <div style="width:400px; height:443px; background-color:#fafafa; float:left; margin-top: 23px;">
                    <div id="result" onselectstart="return false;"></div>
                </div>
                <div style="width:50px; height: 100px; float: left; margin-top:20px;"><span>&nbsp;&nbsp;&nbsp;&nbsp;示例:</span></div>
                <div style="width:268px; height:448px; float:left; padding-top: 18px;">
                    <img src="/static/imgs/rider_avatar.jpg" style="width:268px; height:448px; padding:5px;" />
                </div>
                <div style="width:250px; float: left; padding-top: 20px;">
                    <p>1.必须穿工服</p>
                    <p>2.照片截取到肩膀，超过不予通过</p>
                    <p>3.头像照片和手持身份证照片是同一个人</p>
                    <p>4.照片背景颜色为白色</p>
                    <p>5.长发女骑手需要束成发髻，女骑手不可以化浓妆</p>
                    <p>6.戴头盔要带正并系好，不能歪带</p>
                    <p>7.不留胡须</p>
                    <p>8.着装整齐，衣领平整</p>
                    <p>9.头发不允许染成过于明显或夸张的颜色，长度不能到眼睛</p>
                    <p>10.支持.jpg,.png,.gif格式不大于5M</p>
                </div>
            </div>
        </dd>
    </dl>
    <dl class="dl-horizontal hide headPortrait" >
        <dt style="margin-top: 23px;">头像预览：</dt>
        <dd>
            <div style="width:1100px;">
                <div style="width:200px; height:200px; background-color:#fafafa; float:left; margin-top: 23px;margin-right: 200px;">
                  <img id="J_avatar_preview" src="<#if user??>${user.headPortraitUrl !''}</#if>" style="width:200px; height:200px; border-radius: 50%;" />
                </div>
                <div style="width:50px; height: 100px; float: left; margin-top:20px;"><span>&nbsp;&nbsp;&nbsp;&nbsp;示例:</span></div>
                <div style="width:200px; height:200px; float:left; padding-top: 18px;">
                    <img src="/static/imgs/rider_avatar_circle.png" style="width:200px; height:200px; padding:5px;" />
                </div>
            </div>
        </dd>
    </dl>
    <dl class="dl-horizontal" id ="entryDateDiv">
        <dt><span class="star">*</span>入职日期：</dt>
        <dd>
            <div style="width:800px;">
                <div style="width:50%; display: inline-block; float: left;">
                    <select class="form-control input-sm" id="entryDate" name="entryDate" style="width:400px; display: inline-block;" >
                        <#if entryDateList?exists>
                            <#list entryDateList as day>
                                <option value="${day !''}" <#if (entryDate?exists) && entryDate == day >
                                        selected="selected" </#if>>${day !''}</option>
                            </#list>
                        </#if>
                    </select>
                </div>
                <div style="width:40%; display: inline-block; float: left; margin-left: 10px;margin-top: 5px;">
                    <span style="color: red;" >拎包第一天的日期</span>
                </div>
            </div>
        </dd>
    </dl>
    <dl class="dl-horizontal otherThree" name="otherThree">
        <dt><span class="star">*</span>户口类型：</dt>
        <dd>
            <select class="form-control input-sm" id="houseHoldRegisterType" name="houseHoldRegisterType">
                <option value="0">请选择</option> 
                <option value="1" <#if user?? && (user.houseHoldRegisterType?exists) && (user.houseHoldRegisterType==1)>
                selected="selected" </#if>>本地城镇</option>
                <option value="2" <#if user?? && (user.houseHoldRegisterType?exists) && (user.houseHoldRegisterType==2)>
                selected="selected" </#if>>本地农村</option>
                <option value="3" <#if user?? && (user.houseHoldRegisterType?exists) && (user.houseHoldRegisterType==3)>
                selected="selected" </#if>>外地城镇</option>
                <option value="4" <#if user?? && (user.houseHoldRegisterType?exists) && (user.houseHoldRegisterType==4)>
                selected="selected" </#if>>外地农村</option>
            </select>
        </dd>
    </dl>
    <dl class="dl-horizontal otherThree" name="otherThree">
        <dt><span class="star">*</span>是否直招：</dt>
        <dd>
            <select class="form-control input-sm" id="directRecruit" name="directRecruit">
                <option value="0">请选择</option> 
                <option value="1" <#if user?? && (user.directRecruit?exists) && (user.directRecruit==1)>
                selected="selected" </#if>>是</option>
                <option value="2" <#if user?? && (user.directRecruit?exists) && (user.directRecruit==2)>
                selected="selected" </#if>>否</option>
            </select>
        </dd>
    </dl>
    <dl class="dl-horizontal otherThree" name="otherThree">
        <dt><span class="star">*</span>所属劳务派遣公司：</dt>
        <dd>
            <select class="form-control input-sm" id="laborDispatchingCompany" name="laborDispatchingCompany">
                <option value="0">请选择</option> 
                <option value="1" <#if user?? && (user.laborDispatchingCompany?exists) && (user.laborDispatchingCompany==1)>
                selected="selected" </#if>>北京万古恒信科技有限公司</option>
                <option value="2" <#if user?? && (user.laborDispatchingCompany?exists) && (user.laborDispatchingCompany==2)>
                selected="selected" </#if>>上海正东人力资源有限公司</option>
                <option value="3" <#if user?? && (user.laborDispatchingCompany?exists) && (user.laborDispatchingCompany==3)>
                                selected="selected" </#if>>北京永盛立业企业管理有限公司</option>
            </select>
        </dd>
    </dl>

    <dl class="dl-horizontal" id = "misDiv">
        <dt>mis账号：</dt>
        <dd>
            <input type="text" class="form-control input-sm parsley-validated" id="employeeId" name="" data-required="true" <#if user?? && user.employeeId?exists && user.employeeId gt 0 > value="${user.employeeName!''}(${user.misId!''})" </#if> >
            <div class="mis-search">
                <input style="display: none;" id="mis-id" name="employeeId" value="<#if user?? && user.employeeId?exists && user.employeeId gt 0>${user.employeeId!''}</#if>" >
                <ul class="mis-search-ul">
                </ul>
            </div>
        </dd>
    </dl>

</div>
 