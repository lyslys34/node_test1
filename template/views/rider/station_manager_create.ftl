<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="cssmain">/rider/rider_create.css</content>
<style>
    input[type="radio"] {
        opacity: 0
    }

    .mis-search-ul {
        /*position: absolute;*/
        border: 1px solid rgba(0,0,0,.15);
        /*min-width: 160px;*/
        width: 100%;
        background-color: white;
    }

    .mis-search {
        display: none;
        position: absolute;
        width: 100%;
        padding: 0 15px;
        height: 200px;
        left: 0;
        z-index: 200000;
    }

    .mis-search-ul li {
        margin: 0px 0px;
        padding: 5px 5px;
        border-bottom: 1px solid rgba(0,0,0,.15);
        cursor: pointer;
    }
    .mis-search-ul li:hover {
        background-color: rgba(0,0,0,.15);
    }
</style>
<div id="main-container">
    <#import "/organization/station_create_progress.ftl" as p>
    <#if orgVo?? && orgVo.levelType == 230><@p.progress length=3 step=2 /><#else><@p.progress length=2 step=2 /></#if>
    <h4>负责人信息</h4>
    <div class="panel panel-default">
        <form method="post" class="form-horizontal" id="formValidate1" data-validate="parsley" novalidate="" action="/rider/doCreate">
            <div class="form-group">
                <label class="control-label col-sm-2">负责人</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control input-sm parsley-validated" id="userName" name="name"
                       value="" data-required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">负责人联系电话</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control input-sm parsley-validated" id="userPhone" name="phone"
                       value="" data-required="true">
                </div>
            </div>
            <#if isSelfSupport?exists && isSelfSupport == 1>
                <div class="form-group">
                    <label class="control-label col-sm-2">组织负责人MIS账号</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control input-sm parsley-validated" id="employeeId" name="" data-required="true" >
                        <div class="mis-search">
                            <input style="display: none;" id="mis-id" name="employeeId" value="" >
                            <ul class="mis-search-ul">
                            </ul>
                        </div>
                    </div>
                </div>
            </#if>
            <div class="form-group">
                <label class="control-label col-sm-2">性别</label>
                <div class="col-sm-4">
                    <label class="label-radio inline">
                        <input type="radio" name="gender" checked value="1">
                        <span class="custom-radio"></span>
                        男
                    </label>
                    <label class="label-radio inline">
                        <input type="radio" name="gender" value="2">
                        <span class="custom-radio"></span>
                        女
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">员工类型</label>
                <div class="col-sm-4">
                    <label class="label-radio inline">
                        <input type="radio" name="jobType" checked value="1">
                        <span class="custom-radio"></span>
                        全职
                    </label>
                    <label class="label-radio inline">
                        <input type="radio" name="jobType" value="2">
                        <span class="custom-radio"></span>
                        兼职
                    </label>
                </div>
            </div>
            <input type="hidden" id="roleType" name="roleType" value="2001" />
            <input type="hidden" id="orgId" name="orgId" value="${orgId}" />
            <div class="form-group">
                <label class="control-label col-sm-2">身份证号</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control input-sm parsley-validated" id="idcardNo" name="idcardNo" value="" data-required="true" placeholder="请务必准确填写，否则影响配送费结算">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">手持身份证照</label>
                <div class="col-sm-4">
                    <input type="button" value="点击上传" class="btn btn-default js_uploadbtn" />
                    <input type="hidden" class="js_upload_back_url" id="js_faceUrl" name="faceUrl" value="" />
                    <input type="hidden" class="js_upload_back_url_with_watermark" name="faceUrlWithWatermark" id="faceUrlWithWatermark" value=""/>
                    <input type="file" class="js_fileupload" name="imgFile" style="display: none"/>
                    <img src="" width="300px" height="400px" style="padding-top:10px;display:none;" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">身份证正面照</label>
                <div class="col-sm-4">
                    <input type="button" value="点击上传" class="btn btn-default js_uploadbtn" />
                    <input type="hidden" class="js_upload_back_url" id="js_cardUrl" name="idcardUrl" value="" />
                    <input type="hidden" class="js_upload_back_url_with_watermark" name="idCardUrlWithWatermark" id="idCardUrlWithWatermark" value=""/>
                    <input type="file" class="js_fileupload" name="imgFile" style="display: none"/>
                    <img src="" width="300px" height="400px" style="padding-top:10px;display:none;" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                <#if orgVo?? && orgVo.levelType == 230>
                    <input type="hidden" value="/settle/orgsetting?orgId=${orgId}&f=sc" name="returnUrl" />
                    <button type="button" class="btn btn-danger" id="js_submit">保存并下一步</button>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="javascript:;" class="btn btn-success" id="skip">跳过</a>
                <#elseif orgVo?? && orgVo.levelType == 430>
                    <button type="button" class="btn btn-danger" id="js_submit">保存</button>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="javascript:;" class="btn btn-success" id="skip">取消</a>
                <#else>
                    <button type="button" class="btn btn-danger" id="js_submit">保存</button>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="/org/stations" class="btn btn-success">取消</a>
                </div>
                </#if>
            </div>
            <input type = "hidden" id = "curOrgId" value="${curOrgId !''}">
            <input type = "hidden" id = "role"  value = "${currentRoleCode !''}"/>
            <input type="hidden" id="orgType" name="orgType" value="${orgVo.orgType !''}" />
            <input type="hidden" id="isSelfSupport" name="isSelfSupport" value="${isSelfSupport !''}">
        </form>
    </div>
</div>
<@commonPop></@commonPop>
<script type="text/javascript" src="${staticPath}/js/page/utils.js?ver=${staticVersion !''}"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="${staticPath}/js/page/rider/station_manager_create.js"></script>

<script>
    document.title = '创建用户';
</script>