<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/rider/rider_head_portrait_check_list.js</content>
<content tag="cssmain">/rider/rider_head_portrait_check_list.css</content>


<div id="main-container">
    <div class="btn btn-success" id="multi_pass">审核通过</div><span>&nbsp;&nbsp;已选择了<span id="se_num">0</span>人</span>
    <br>
    <br>
    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="checkTable">
            <thead>
            <tr>
                <th style="text-align:center;">
                    <input style="opacity: 1; margin-top:-14px; margin-left:2px;" type="checkbox" name="check-All" class="all_check_box" id="check-All" value="allcheck" />
                </th>
                <th style="text-align:center;">骑手头像</th>
                <th style="text-align:center;">手持身份证照片</th>
                <th style="text-align:center;">骑手姓名</th>
                <th style="text-align:center;">站点名称</th>
                <th style="text-align:center;">手机号</th>
                <th style="text-align:center;">操作</th>
            </tr>
            </thead>
            <tbody valign="middle">
            <#--循环-->
                <#if riderReturnList??>
                    <#list riderReturnList.data as user>
                        <tr>
                            <td>
                                <input style="opacity: 1;" type="checkbox" name="check-user" class="user_check_box" value="nocheck" recordId="${user.id!''}" />
                            </td>
                            <td><img src="${user.headPortraitUrl!''}" width="200px" height="200px" style="border-radius: 50%;" class="smallImg riderHeadImg" /></td>
                            <td><img src="${user.selfPicPath!''}" width="200px" height="200px" class="smallImg" /></td>
                            <td>${user.name!''}</td>
                            <td>${user.orgName!''}</td>
                            <td>${user.mobile!''}</td>
                            <td recordId="${user.id!''}"><button class="btn btn-success approve" >通过</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-danger disagree" >驳回</button></td>
                        </tr>
                    </#list>
                </#if>
            </tbody>
        </table>

        <div class="modal fade" id="disaggresUser" >
            <div class="modal-dialog">
                <div class="modal-content" style="width:400px;">
                    <div class="modal-header">
                        <h4>提示</h4>
                    </div>
                    <div class="modal-body">
                        <textarea rows=8 placeholder="请输入驳回原因" style="width:370px;" id="comment"></textarea>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm" id="cancelCheck">取消</button>
                            <input type="submit" id="disaggreUserBtn" value="确认" class="btn btn-danger btn-sm" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="aggresUserConfirm">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;&nbsp;提示
                    </div>
                    <div class="modal-body">
                        <h4 style="text-align:center">确认审核通过吗？</h4>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" id="aggreUserBtn" value="确定" class="btn btn-danger btn-sm" />
                        <button class="btn btn-success btn-sm" id="cancel">取消</button>

                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="multiAggresUserConfirm">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;&nbsp;提示
                    </div>
                    <div class="modal-body">
                        <h4 style="text-align:center">确认审核通过吗？</h4>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" id="multiAggreUserBtn" value="确定" class="btn btn-danger btn-sm" />
                        <button class="btn btn-success btn-sm" id="multiCancel">取消</button>

                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="imgOrigin">
            <div class="modal-dialog" style="width:450px;">
            </div>
        </div>
        <div style="float: left; padding: 10px; 15px"><span>共<#if riderReturnList??>${riderReturnList.totalCount!'0'}<#else>0</#if>项</span></div>
        <#import "../page/pager.ftl" as q>
            <#if riderReturnList??>
                <@q.pager pageNo=riderReturnList.pageNo pageSize=riderReturnList.pageSize recordCount=riderReturnList.totalCount toURL="/rider/headPortraitCheckList"/>
            </#if>
    </div>
</div>
<script>
    document.title = '骑手头像审核';
    $('select').select2();
</script>