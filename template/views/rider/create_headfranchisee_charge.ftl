<#include "../headfranchisee/headfranchisee_config.ftl" />
<#include "../widgets/sidebar.ftl" />

<style>
    .cred {
        color: #b22222;
    }

    .lh30 {
        line-height: 30px;
    }

    input[type="radio"] {
        opacity: 0
    }

</style>
<div id="main-container">
<@createHeadFranchiseeStep 2 />
    <form method="post" class="form-horizontal" id="formValidate1" data-validate="parsley" novalidate="">

        <input type="hidden" id="orgId" name="orgId" value="${orgId !'0'}"

        <div class="panel panel-default">
            <div class="panel panel-heading">
                基本信息
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>姓名</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm" id="userName" name="name" data-required="true"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>负责人联系电话</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm" id="userPhone" name="phone" data-required="true"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>性别</label>

                <div class="col-sm-6">
                    <label class="label-radio inline">
                        <input type="radio" name="gender" checked value="1">
                        <span class="custom-radio"></span>
                        男
                    </label>
                    <label class="label-radio inline">
                        <input type="radio" name="gender" value="2">
                        <span class="custom-radio"></span>
                        女
                    </label>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>员工类型</label>

                <div class="col-sm-6">
                    <label class="label-radio inline">
                        <input type="radio" name="jobType" checked value="1">
                        <span class="custom-radio"></span>
                        全职
                    </label>
                    <label class="label-radio inline">
                        <input type="radio" name="jobType" value="2">
                        <span class="custom-radio"></span>
                        兼职
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>身份证号</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm" id="idcardNo" name="idcardNo" data-required="true">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>手持身份证照</label>

                <div class="col-sm-6 lh30">
                    <a href="javascript:void(0)" class="js_uploadbtn" style="display: inline-block;">上传附件</a>
                    <input type="file" name="imgFile" style="display:none" class="js_fileupload">
                    <input type="hidden" id="js_faceUrl" class="js_upload_back_url" name="faceUrl" />
                    <input type="hidden" class="js_upload_back_url_with_watermark" name="faceUrlWithWatermark" id="faceUrlWithWatermark" value=""/>
                    <img style="display:none; width:300px; height:400px;" class="js_img_click_open" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>身份证正面照</label>

                <div class="col-sm-6 lh30">
                    <a href="javascript:void(0)" class="js_uploadbtn" style="display: inline-block;">上传附件</a>
                    <input type="file" name="imgFile" style="display:none" class="js_fileupload">
                    <input type="hidden" id="js_cardUrl" class="js_upload_back_url" name="idcardUrl" />
                    <input type="hidden" class="js_upload_back_url_with_watermark" name="idCardUrlWithWatermark" id="idCardUrlWithWatermark" value=""/>
                    <img style="display:none; width:300px; height:400px;" class="js_img_click_open" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-10 text-center">
                    <input type="button" class="btn btn-danger" value="保存" id="js_save"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="button" class="btn btn-success" value="跳过" id="js_passed"/>
                </div>
            </div>
        </div>

    </form>
</div>
<@commonPop></@commonPop>
<script type="text/javascript" src="${staticPath}/js/page/utils.js?ver=${staticVersion !''}"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="${staticPath}/js/page/rider/create_headfranchisee_charge.js?ver=${staticVersion !''}"></script>

<script>
    document.title = '新建加盟商总负责人';
    // $('#orgType').select2();
    $('#orgCity').select2();
    $('#parentId').select2();
</script>