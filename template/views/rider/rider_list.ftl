<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">


<content tag="javascript">/rider/rider_list.js</content>
<content tag="cssmain">/rider/rider_list.css</content>
<style type="text/css">
    #main-container{
        padding: 10px 20px;
    }
    .col-md-4{width: 260px;}
</style>

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post"
              action="/rider/list">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">姓名</label>
                            <input type="text" class="form-control input-sm parsley-validated" id="userName"
                                   name="userName" value="${userName!''}" data-required="true" data-minlength="8">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">手机号</label>
                            <input type="text" class="form-control input-sm parsley-validated" id="userPhone"
                                   name="userPhone" value="${userPhone!''}" data-required="true" data-minlength="8">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">组织名</label>
                                <select class="form-control input-sm" id="orgId" name="orgId" style="width:229px;">
                                    <option value="0" <#if orgId == 0 > selected="selected" </#if> >全部</option>
                                    <#list orgs as org>
                                        <option value="${org.orgId !''}" <#if org.orgId == orgId >
                                                selected="selected" </#if> >${org.orgName !''}</option>
                                    </#list>
                                </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">审核状态</label>
                            <select class="form-control input-sm" id="checkType" name="checkType" style="width:229px;">
                                <option value="-1" <#if checkType == -1 > selected="selected" </#if> >全部</option>
                                <option value="0" <#if checkType == 0 > selected="selected" </#if>>待审核</option>
                                <option value="1" <#if checkType == 1 > selected="selected" </#if>>审核通过</option>
                                <option value="2" <#if checkType == 2 > selected="selected" </#if>>驳回</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <#if !is_org_manager?exists || is_org_manager != 1>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">职位</label>
                                <select class="form-control input-sm" id="roleType" name="roleType" style="width:229px;">
                                    <option value="0">全部</option>
                                    <#if roleVoList?exists>
                                        <#list roleVoList as role>
                                            <option value="${role.code !'0'}" <#if roleType == role.code > selected="selected" </#if>>${role.name !''}</option>
                                        </#list>
                                    </#if>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">组织类型</label>
                                <select class="form-control input-sm" id="orgType" name="orgType" style="width:229px;">
                                    <option value="0">全部</option>
                                <#list orgTypeList as type>
                                    <option value="${type.type !''}" <#if type.type == orgType >
                                            selected="selected" </#if> >${type.comment !''}</option>
                                </#list>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">城市名称</label>
                                <select class="form-control input-sm" id="cityId" name="cityId" style="width:229px;">
                                    <option value="0">全部</option>
                                <#list cityList as city>
                                    <option value="${city.city_id !''}" <#if city.city_id == cityId >
                                            selected="selected" </#if> > ${city.name!''}</option>
                                </#list>
                                </select>
                            </div>
                        </div>
                    </#if>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">员工状态</label>
                            <select class="form-control input-sm" id="jobStatus" name="jobStatus">
                                <option value="-1">全部</option>
                                <option value="1" <#if jobStatus?exists && jobStatus == 1 >
                                            selected="selected" </#if> >在职</option>
                                <option value="0" <#if jobStatus?exists && jobStatus == 0 >
                                            selected="selected" </#if> >离职</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="panel-footer text-left">
                总计${recordCount!'0'}条记录
                &nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-success">查询</button>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="/rider/create" class="btn btn-success">添加人员</a>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>姓名</th>
                <th>手机号</th>
                <th>性别</th>
                <th>城市</th>
                <th>所在组织</th>
                <th>组织类型</th>
                <th>职位</th>
                <th>员工类型</th>
                <th>审核状态</th>
                <th>员工状态</th>
                <th>头像状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list userList as user>
            <tr <@output_props obj = user />>
                <td class="name"><a target="_blank" href="/rider/detail/${user.orgId!'-1'}/${user.id}">${user.name!''}</a></td>
                <td>${user.phone!''}</td>
                <td>
                    <#if user.gender?exists && user.gender == 1>男
                    <#elseif user.gender?exists && user.gender == 2>女
                    </#if>
                </td>
                <td>${user.cityName!''}</td>
                <td>${user.orgName!''}</td>
                <td>${user.orgTypeName!''}</td>
                <td>${user.roleName!''}</td>
                <td>
                    <#if user.jobType?exists && user.jobType == 1>全职
                    <#elseif user.jobType?exists && user.jobType == 2>兼职
                    </#if>
                </td>
                <td><#if user.checked==0>待审核<#elseif user.checked==1>通过<#elseif user.checked==2>驳回</#if></td>
                <td><span class="js_item_jobStatus"><#if user.jobStatus==1>在职<#else>离职</#if></span></td>
                <#-- 用户头像审核状态  待审核、审核通过和审核驳回  如果不是自建和加盟的骑手，显示为空 -->
                <td>
                <#if user.roleCode == 1001 && (user.orgType == 1 || user.orgType == 2 || user.orgType == 7)>
                    <#if user.headPortraitChecked == 0><span class="label badge label-warning">待审核</span></#if>
                    <#if user.headPortraitChecked == 1><span class="label badge label-success">通过</span></#if>
                    <#if user.headPortraitChecked == 2><span class="label badge label-danger">驳回</span></#if>
                </#if>
                </td>
                <td>
                    <#if user.checked==0>
                        <a data-toggle="tooltip" title="详情" data-placement="top" href="/rider/detail/${user.orgId!'-1'}/${user.id}">
                            <i class="fa fa-info-circle fa-lg opration-icon"></i>
                        </a>&nbsp;&nbsp;
                    <#else>
                        <#if user.isDimission() || user.accountSource != 1>
                            <a data-toggle="tooltip" title="详情" data-placement="top" href="/rider/detail/${user.orgId!'-1'}/${user.id}">
                                <i class="fa fa-info-circle fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                        <#else>
                            <a data-toggle="tooltip" title="详情" data-placement="top" href="/rider/detail/${user.orgId!'-1'}/${user.id}">
                                <i class="fa fa-info-circle fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                            <a data-toggle="tooltip" title="修改" data-placement="top" href="/rider/update/${user.orgId!'-1'}/${user.id}">
                                <i class="fa fa-edit fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;

                            <#if user.orgTypeName?? && user.orgTypeName == "自建">
                                <#if currentRoleCode?exists && (currentRoleCode == 2001 || currentRoleCode == 2100 || currentRoleCode == 2101 || currentRoleCode == 2301 || currentRoleCode == 2302 || currentRoleCode == 2031)>
                                    <a data-toggle="tooltip" title="结算设置" data-placement="top" href="/partner/settle/ridersetting?userId=${user.id}">
                                        <i class="fa fa-credit-card fa-lg opration-icon"></i>
                                    </a>&nbsp;&nbsp;
                                <#else >
                                    <a data-toggle="tooltip" title="结算设置" data-placement="top" href="/settle/ridersetting?userId=${user.id}">
                                        <i class="fa fa-credit-card fa-lg opration-icon"></i>
                                    </a>&nbsp;&nbsp;

                                </#if>
                            </#if>
                            <#if user.orgType?? && user.orgType==5 >
                                <a data-toggle="tooltip" title="商家管理" data-placement="top" href="/rider/getJiaoMaRiderPoi?userId=${user.id}&roleCode=${user.roleCode}&orgId=${user.orgId}&orgType=${user.orgType}">
                                    <i class="fa fa-home fa-lg opration-icon"></i>
                                </a>&nbsp;&nbsp;
                            </#if>
                            <a data-toggle="tooltip" title="离职" data-placement="top"  href="javascript:void(0)" class="js_rider_delete">
                                <i class="fa fa-trash-o fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                        </#if>
                    </#if>
                    <#if is_org_manager?exists && is_org_manager!=1>
                    	<a data-toggle="tooltip" title="操作记录" data-placement="top" href="/log/riderOpLog?bmUserId=${user.id}" target="_blank">
	                        <i class="fa fa-history fa-lg opration-icon"></i>
	                    </a>&nbsp;&nbsp;
                    </#if>
                    
                </td>
            </tr>
            </#list>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/rider/list"/>
    </#if>
    </div>

    <div class="modal fade" id="dimissionModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-horizontal">
                        <h4 style="border-bottom: 1px solid #e5e5e5; padding-bottom: 7px;">提示</h4>

                        <div class="form-group">
                            <label class="control-label col-sm-3" style="font-size: 16px;">确定要离职</label>
                            <div class="col-sm-5" style="top: 7px;">
                                <span id="dimissionName" style="font-size:16px; font-family: microsoft yahei,Arial,Helvetica,sans-serif; font-weight: 500;"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" style="font-size: 16px;">离职骑手姓名</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control input-sm" id="dimissionNameInput" placeholder="请输入待离职的骑手姓名" value=""/>
                            </div>
                        </div>
                        
                        <div class="form-group" id="termDateDiv">
                            <label class="control-label col-sm-3" style="font-size: 16px;">离职日期</label>
                            <div class="col-sm-7">
                                <input type="text" id="termDate" name="termDate" style="cursor: pointer;display: inline;width: 50%;" maxlength="10"  readonly="readonly" value="${.now?string('yyyy-MM-dd')}" class="form-control input-sm date-picker J-datepicker "/>
                            	<span>(工作最后一天的日期)</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-8" style="font-size: 16px; color: #ff00da;">温馨提示：人员离职后无法恢复，请谨慎操作</label>
                        </div>

                        <div class="modal-footer">
                            <input type="button" id="confirmDimission" value="确认" class="btn btn-danger btn-sm" />
                            <button class="btn btn-success btn-sm" id="cancelDimission">取消</button>
                        </div>


                        <input type="hidden" id="dimissionNameHidden" value=""/>
                        <input type="hidden" id="dimissionUserId" value=""/>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<script>
    document.title = '人员管理';
    $('select').select2();
</script>