<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/rider/rider_attendance.js</content>
<content tag="cssmain">/rider/rider_attendance.css</content>
<script src="${staticPath}/js/lib/daterangepicker/moment.min.js"></script>
<script src="${staticPath}/js/lib/daterangepicker/daterangepicker-2.1.13.js"></script>

<div id="main-container">
	<#-- tab -->
	<div id="mtab" class="clearfix">
		<a class="mtab_item on" href="javascript:;" data-page="page1">考勤管理</a>
		<a class="mtab_item" href="javascript:;" data-page="page2">申诉处理</a>
	</div>
	<#-- page -->
	<div id="page1" class="page clearfix">
		<div class="tips">
			<input type="hidden" id="thisYear"><span id="thisMonth"></span>月应到<span id="shouldDays"></span>天<i id="changeShouldDays" class="fa fa-edit aop"></i>
			<small>本月实际出勤天数不小于应勤天数即可（不包括法定节假日）</small>
		</div>
		<div class="condition">
			<label class="aop">
				<select id="stationSelect">
					<option value="0" selected>全部</option>
				</select>
			</label>
			<label>姓名：<input type="text" class="input-sm" id="rname"></label>
			<label>电话：<input type="text" class="input-sm" id="rphone"></label>
			<button class="btn btn-success" id="searchBtn">查询</button>
		</div>
		<div class="weekpicker">
			<i class="fa fa-chevron-left week_arrow" data-type="prev"></i>
			<span id="weekString"></span>
			<i class="fa fa-chevron-right week_arrow" data-type="next"></i>
			<i class="fa fa-calendar"></i>
			<input type="text" id="weekPicker" readonly>
		</div>
		<#-- 考勤周列表 -->
		<div id="weekList"></div>
		<#-- 考勤月列表 -->
		<div id="monthList"></div>
		<#-- 月 选择 -->
		<div id="monthPickerBox">
			<div id="monthPickerSelectBox"></div>
			<div class="btn_box">
				<button class="btn btn-xs btn-success" id="submitMonthPick">确定</button>
				<button class="btn btn-xs btn-success" id="cancelMonthPick">取消</button>
			</div>
		</div>
		<#-- 编辑每月应到天数 -->
		<div id="editShouldDays">
			<p>修改应到天数</p>
			<div class="shouldday_select_box"></div>
			<label>
				应到天数：<input id="shouldDaysInput" type="text">天
			</label>
			<div class="btn_box">
				<button class="btn btn-xs btn-success" id="submitShouldDays">确定</button>
				<button class="btn btn-xs btn-success" id="cancelEditShouldDays">取消</button>
			</div>
		</div>
		<div id="bgWrapper"></div>
	</div>
	<div id="page2" class="page clearfix hide">
		<div class="condition">
			<label>
				<select id="statusSelect">
					<option value="1">待处理</option>
					<option value="2">已处理</option>
					<option value="0">全部</option>
				</select>
			</label>
			<label>工单号：<input type="text" class="input-sm" id="appealToken"></label>
			<label>骑手：<input type="text" class="input-sm" id="appealName"></label>
			<label>电话：<input type="text" class="input-sm" id="appealPhone"></label>
			<button class="btn btn-success" id="appealSearchBtn">查询</button>
		</div>
		<div id="appealList"></div>
	</div>
</div>
<script type="text/tpl" id="J_week_tpl">
	<table id="weekListTable">
		<thead>
			{{each thead as th}}
			<th>
				<p>{{th.data}}</p>
				<p>{{th.meta}}</p>
			</th>
			{{/each}}
		</thead>
		<tbody>
			{{each tbody as tr}}
			<tr>
				{{each tr as td}}
					{{if $index == 0}}
					<td class="rider_item" data-rid="{{td.meta}}">
					{{else}}
					<td>
					{{/if}}
						<p class="{{$index > 4 ? 'big' : ''}} {{$index == 0 ? 'rider_name' : ''}} {{td.data == '未出勤' ? 'red' : ''}}">{{td.data}}</p>
						{{if $index != 0}}
						<small>{{td.meta}}</small>
						{{/if}}
					</td>
				{{/each}}
			</tr>
			{{/each}}
		</tbody>
	</table>
	<div class="page_box clearfix">
		<div class="pull-right page_info">
			共<b>{{totalPage}}</b>页
			<input id="pageInput">
			<button class="btn btn-success btn-xs page_btn" data-page="n">跳转</button>
		</div>
		<ul class="pagination pagination-xs pull-right">
			<li><a class="page_btn" data-page="1" href="javascript:;">首页</a></li>
			<li><a class="page_btn" data-page="{{pageNum - 1}}" href="javascript:;">上一页</a></li>
			<li><a href="javascript:;">{{pageNum}}</a></li>
			<li><a class="page_btn" data-page="{{pageNum + 1}}" href="javascript:;">下一页</a></li>
			<li><a class="page_btn" data-page="{{totalPage}}" href="javascript:;">末页</a></li>
		</ul>
	</div>
</script>
<script type="text/tpl" id="J_month_tpl">
	<p>当月应出勤<span class="blue">{{expectDays}}</span>天, 实际出勤<span class="blue">{{actualDays}}</span>天</p>
	<div id="monthpicker">
		<i class="fa fa-chevron-left month_arrow" data-type="prev"></i>
		<span id="monthString" data-year="{{year}}" data-month="{{month}}">{{year}}年{{month}}月</span>
		<i class="fa fa-chevron-right month_arrow" data-type="next"></i>
		<i class="fa fa-calendar" id="monthPickerBtn"></i>
	</div>
	<i class="fa fa-times" id="closeMonthList"></i>
	<table id="monthListTable">
		<thead>
			<tr>
				<th>周一</th><th>周二</th><th>周三</th><th>周四</th><th>周五</th><th>周六</th><th>周日</th>
			</tr>
		</thead>
		<tbody>
			{{each monthList as week}}
			<tr>
				{{each week as day}}
				<td>
					<p class="day_num">{{day.dayofmonth}}</p>
					<h5 class="day_status" style="color: {{day.data == '未出勤' ? '#F44336' : '#2196F3'}}">{{day.data}}</h5>
					<p class="day_meta">{{day.meta}}</p>
					<small class="day_time">{{day.stime}}{{day.stime ? '-' : ''}}{{day.etime}}</small>
				</td>
				{{/each}}
			</tr>
			{{/each}}
		</tbody>
	</table>
</script>
<script type="text/tpl" id="J_appeal_tpl">
	<table id="appealListTable">
		<thead>
			{{each thead}}
			<th>{{$value}}</th>
			{{/each}}
		</thead>
		<tbody>
			{{each tbody as tr}}
			<tr>
				<td>{{tr.appealToken}}</td>
				<td>{{tr.appealTime}}</td>
				<td>{{tr.riderName}}</td>
				<td>{{tr.riderMobile}}</td>
				<td>{{tr.appealStatus}}</td>
				<td>{{tr.appealResult}}</td>
				<td><a class="link" href="/rider/attendance/appeal/partner?appealtoken={{tr.appealToken}}">查看</a></td>
			</tr>
			{{/each}}
		</tbody>
	</table>
	<div class="page_box clearfix">
		<div class="pull-right page_info">
			共<b>{{totalPage}}</b>页
			<input id="appealPageInput">
			<button class="btn btn-success btn-xs appeal_page_btn" data-page="n">跳转</button>
		</div>
		<ul class="pagination pagination-xs pull-right">
			<li><a class="appeal_page_btn" data-page="1" href="javascript:;">首页</a></li>
			<li><a class="appeal_page_btn" data-page="{{pageNum - 1}}" href="javascript:;">上一页</a></li>
			<li><a href="javascript:;">{{pageNum}}</a></li>
			<li><a class="appeal_page_btn" data-page="{{pageNum + 1}}" href="javascript:;">下一页</a></li>
			<li><a class="appeal_page_btn" data-page="{{totalPage}}" href="javascript:;">末页</a></li>
		</ul>
	</div>
</script>
<script>
	var $stationSelect = $('#stationSelect');
	window.aop = 'p';
</script>