<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="cssmain">/rider/rider_view.css</content>

<div id="main-container">

    <#if user.checked==2 && user.unCheckReasons?exists && ( user.unCheckReasons?size > 0 ) >
        <div class="panel panel-default package-detail" style="background-color:#FFFAFA">
            <div class="row" style="margin-left: 15px;margin-top:10px;">
                <strong style="font-size:16px;line-height:2">您的身份审核不通过</strong><br/>
                <strong style="font-size:14px;line-height:2">认证审核不通过原因:</strong>
                <ul style="list-style-type:disc;margin-left: 15px;line-height:2">
                    <#list user.unCheckReasons as reason>
                        <li>${reason}</li>
                    </#list>
                </ul>
                <p style="font-size:14px;line-height:2">请重新提交身份信息资料，完成身份认证</p>
            </div>
        </div>
    </#if>

    <#if user.headPortraitChecked==2 && user.roleCode == 1001>
        <div class="panel panel-default package-detail" style="background-color:#FFFAFA">
            <div class="row" style="margin-left: 15px;margin-top:10px;">
                <strong style="font-size:16px;line-height:2">您的头像审核不通过</strong><br/>
                <strong style="font-size:14px;line-height:2">审核不通过原因如下:</strong>
                <ul style="list-style-type:disc;margin-left: 15px;line-height:2">
                        <li>${user.headPortraitUnCheckRaasons!''}</li>
                </ul>
                <p style="font-size:14px;line-height:2">请重新上传骑手头像，通过后，顾客下单即可看到骑手头像。</p>
            </div>
        </div>
    </#if>

    <div class="panel panel-default">
        <div class="panel-heading" style="font-size:20px;">
            <span>人员基本信息<#if user.checked==2><span style="font-size:14px;color:#FF6666;margin-left:60px;">新建人员需经总部审核通过后才生效。18：00前提交的申请，可当日完成审核。18：00后提交的申请，次日完成审核。</span></#if>
            </span>
        </div>

        <div class="panel-body">
            <div class="title">身份信息<#if user.checked==1><span style="color:#33CCCC;margin-left:110px;">审核通过</span><#elseif user.checked==0><span style="color:#FF6633;margin-left:110px;">待审核</span><#elseif user.checked ==-1><span style="margin-left:110px;">未提交审核</span></#if></div>
            <br/>
            <dl class="dl-horizontal">
                <dt><span class="star">*</span>姓名：</dt>
                <dd>${user.name!''}</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt><span class="star">*</span>性别：</dt>
                <dd><#if user.gender==1>男<#else>女</#if></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt><span class="star">*</span>身份证号码：</dt>
                <dd>${user.cardNum!''}</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt><span class="star">*</span>手持身份证照：</dt>
                <dd><img src="${user.selfPicPath!''}" style="width:400px; height:280px;"></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt><span class="star">*</span>身份证正面：</dt>
                <dd><img src="${user.frontPicPath!''}" style="width:400px; height:280px;"></dd>
            </dl>
            <div class="title">工作信息</div>
            <br/>
            <dl class="dl-horizontal">
                <dt><span class="star">*</span>手机号：</dt>
                <dd>${user.phone!''}</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt><span class="star">*</span>员工类型：</dt>
                <dd><#if user.jobType==1>全职<#else>兼职</#if></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt><span class="star">*</span>职位：</dt>
                <dd>${user.roleName!''}</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt><span class="star">*</span>组织名称：</dt>
                <dd>${user.levelTypeName!''}&nbsp;&nbsp;${user.orgName!''}</dd>
            </dl>
            <#if user.roleCode == 1001 && user.headPortraitUrl?? && user.headPortraitUrl?length gt 0>
            <dl class="dl-horizontal">
                <dt>骑手头像：</dt>
                <dd><img src="${user.headPortraitUrl!''}" style="width:200px; height:200px;"></dd>
            </dl>
            </#if>
            <dl class="dl-horizontal">
                <dt><span class="star">*</span>最后一次上线时间：</dt>
                <dd>${user.lastestOnLineTime!''}</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt><span class="star">*</span>最后一次接单时间：</dt>
                <dd>${user.lastestGrabTime!''}</dd>
            </dl>

            <#if user.orgType?? && user.orgType==1 && user.roleCode?? && user.roleCode==1001>
            	<dl class="dl-horizontal">
                    <dt><span class="star">*</span>入职日期：</dt>
                    <dd>
                        ${entryDate!''}
                    </dd>
                </dl>
                <#if termDate??>
                <dl class="dl-horizontal">
                    <dt><span class="star">*</span>离职日期：</dt>
                    <dd>
                        ${termDate!''}
                    </dd>
                </dl>
                </#if>
                <dl class="dl-horizontal">
                    <dt><span class="star">*</span>户口类型：</dt>
                    <dd>
                        ${user.houseHoldRegisterTypeName!''}
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt><span class="star">*</span>是否直招：</dt>
                    <dd>
                        <#if user.directRecruit==1>是
                        <#elseif user.directRecruit==2>否
                        <#else>
                        </#if>
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt><span class="star">*</span>所属劳务派遣公司：</dt>
                    <dd>
                        ${user.laborDispatchingCompanyName!''}
                    </dd>
                </dl>
            </#if>

            <#if user.orgType?? && user.orgType==1 && riderIsOrgManager?? && riderIsOrgManager == 1>
                <dl class="dl-horizontal">
                    <dt><span class="star">*</span>mis账号：</dt>
                    <dd>${user.employeeName}(${user.misId!''})</dd>
                </dl>
            </#if>

        </div>
    </div>
</div>