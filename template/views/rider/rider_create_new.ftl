<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="cssmain">/rider/rider_save_new.css</content>



<div id="main-container">
    <div class="panel panel-default">
        <div id="alert_error"></div>
        <input type="hidden" name="originLevelType" id="originLevelType" value="-1" />
        <input type="hidden" id="orgIdTemp" value="-1" />
        <input type="hidden" id="orgTypeTemp" value="-1" />

        <#include "rider_save_common.ftl">

        <div class="panel-footer text-right">
            <button type="button" class="btn btn-success" onclick="javasctipt:history.go(-1);">返回</button>&nbsp;&nbsp;
            <button type="button" class="btn btn-success" id="createUserSubmit">保存</button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <#-- <a href="/rider/list" class="btn btn-default">取消</a> -->
        </div>

        <input type = "hidden" id = "curOrgId" value="${curOrgId !''}">
        <input type = "hidden" id = "role"  value = "${currentRoleCode !''}"/>
        <input type="hidden" name="orgType" value="0" id="orgType"/>

    </div>
</div>

<script type="text/javascript" src="${staticPath}/js/page/utils.js?ver=${staticVersion !''}"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="/static/js/lib/cutout.js"></script>
<script type="text/javascript" src="${staticPath}/js/page/rider/rider_detail.js"></script>

<script>
document.title = '创建用户';
    $('select').select2();
</script>
