<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="cssmain">/rider/rider_list.css</content>
<style type="text/css">
    #main-container{
        padding: 10px 20px;
    }
</style>

<div id="main-container">
    <#if RequestParameters.f?exists>
        <#if RequestParameters.f == 'zj_branch'>
            <@zjNav orgVo 2></@zjNav>
        </#if>
    </#if>
    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>姓名</th>
                <th>手机号</th>
                <th>性别</th>
                <th>所在组织</th>
                <th>职位</th>
                <th>员工类型</th>
                <th>员工状态</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list userList as user>
            <tr <@output_props obj = user />>
                <td class="name"><a target="_blank" href="/rider/list?userName=${user.name!''}&userPhone=${user.phone}">${user.name!''}</a></td>
                <td>${user.phone!''}</td>
                <td>
                    <#if user.gender?exists && user.gender == 1>男
                    <#elseif user.gender?exists && user.gender == 2>女
                    </#if>
                </td>
                <td>${user.orgName!''}</td>
                <td>${user.roleName!''}</td>
                <td>
                    <#if user.jobType?exists && user.jobType == 1>全职
                    <#elseif user.jobType?exists && user.jobType == 2>兼职
                    </#if>
                </td>
                <td>
                    <#if user.jobStatus?exists && user.jobStatus==1>
                        在职
                    <#else >
                        离职
                    </#if>
                </td>
            </tr>
            </#list>
            </tbody>
        </table>
    <#if recordCount?? && recordCount gt 0><div class="panel-footer clearfix pull-left">共${recordCount!'0'}条</div></#if>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/rider/riderListByOrgId"/>
    </#if>
    </div>
</div>
<script>
    document.title = '人员列表';
    $('select').select2();
</script>