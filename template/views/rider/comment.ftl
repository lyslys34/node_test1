<#include "rider_config.ftl">
<#include "../widgets/sidebar.ftl">

  <div class="panel panel-default table-responsive">
    <table class="table table-striped" id="responsiveTable">
      <thead>
        <tr>
          <th>订单ID</th>
          <th>评论内容</th>
          <th>打分</th>
        </tr>
      </thead>
      <tbody>
      <#--循环-->
      <#list list as user>
        <tr>
          <td>${user.orderId!''}</td>
          <td>${user.comment!''}</td>
          <td>${user.star!''}</td>
        </tr>
      </#list>
      </tbody>
    </table>
  </div>
</div>
<script>document.title = '评论';</script>