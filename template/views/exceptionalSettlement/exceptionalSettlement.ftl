<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/exceptionalSettlement/exceptionalSettlement.js</content>
<content tag="cssmain">/exceptionalSettlement/exceptionalSettlement.css</content>
<content tag="pageName">exceptionalSettlement</content>
<div id="main-container">
<div style="content_body">
    <!--<div class="navi_head">
        <div class="navi_title">异常订单结算</div>
        <div class="navi_bottom_line">&nbsp;</div>
    </div>-->
    <div class="condition_area">
        <form class="form-inline">
            &nbsp;&nbsp;异常类型：<select class="form-control input-sm" style="width: 200px;" id="exceptionalType">
            <option value="0" <#if exceptionalType == 0>selected</#if>>全部</option>
            <option value="2" <#if exceptionalType == 2>selected</#if>>已取消</option>
            <option value="1" <#if exceptionalType == 1>selected</#if>>实收实付不一致</option>
        </select>

            &nbsp;&nbsp;
            结算时间段 <input id="query-start-date" style="cursor: pointer"
                         class='form-control input-sm date-picker J-datepicker js_date_start' readonly="readonly"
                         value="<#if startDate?exists>${startDate}<#else>${.now?string('yyyy-MM-dd')}</#if>">
            至 <input id="query-end-date" style="cursor: pointer"
                     class="form-control input-sm date-picker J-datepicker js_date_start" readonly="readonly"
                     value="<#if endDate?exists>${endDate}<#else>${.now?string('yyyy-MM-dd')}</#if>">
            &nbsp;&nbsp;&nbsp;&nbsp;<input type="button" id="query-commit" value="查看"
                                           class="btn btn-success js_submitbtn"/>
        </form>
    </div>

<#if list?exists && (list?size > 0)>
<div class="result_area">
    <div class="table_title form-inline">
        <span><p>结算细目</p></span>
        <a name="excelReport" href="javascript:void(0);" class="btn btn-sm btn-default">导出表格</a>
    </div>
    <table class="table">
        <tr>
            <td>订单号</td>
            <td>订单完成时间</td>
            <td>商家名称</td>
            <td>商家类型</td>
            <td>订单类型</td>
            <td>订单金额</td>
            <td>应付商家</td>
            <td>应收用户</td>
            <td>实付</td>
            <td>实收</td>
            <td>订单状态</td>
            <td>异常类型</td>
            <td>拍摄凭证</td>
        </tr>
        <#list list as settlementDetail>
            <tr>
                <td>${settlementDetail.orderId !''}</td>
                <td>${settlementDetail.finishedTime !''}</td>
                <td>${settlementDetail.poiName !''}</td>
                <td>
                    <#if settlementDetail.poiType == 1>
                        代购
                    <#elseif settlementDetail.poiType == 2>
                        普通
                    </#if>
                </td>
                <td>
                    <#if settlementDetail.orderType == 0>
                        货到付款
                    <#elseif settlementDetail.orderType == 1>
                        在线支付
                    </#if>
                </td>
                <td>￥${settlementDetail.orderMoney}</td>
                <td>￥${settlementDetail.planPayAmount}</td>
                <td>￥${settlementDetail.planChargeAmount}</td>
                <td>￥${settlementDetail.actualPayAmount}</td>
                <td>￥${settlementDetail.actualChargeAmount}</td>
                <td>
                    <#switch settlementDetail.orderStatus>
                        <#case 1>已送达<#break>
                        <#case 2>已取消<#break>
                        <#case 3>送达后取消<#break>
                    </#switch>
                </td>
                <td>
                    <#switch settlementDetail.exceptionalType>
                        <#case 1>实收实付不一致<#break>
                        <#case 2>已取消<#break>
                        <#case 3>送达后取消<#break>
                    </#switch>
                </td>
                <td>
                    <#if settlementDetail.invoiceImgUrl??>
                        <a data-toggle="tooltip" title="查看照片" data-placement="top" href="${settlementDetail.invoiceImgUrl}" target="_blank">
                            <i class="fa fa-info-circle fa-lg opration-icon"></i>
                        </a>
                    <#else>
                        无
                    </#if>
                </td>
            </tr>
        </#list>
    </table>
<#else>
    <br/>
    无符合条件的数据
</#if>
</div>
</div>
<nav style="float:right;">
<#if (list?exists && (list?size > 0)) && page?exists >
    <#assign leftNum = page.leftNum>
    <#assign rightNum = page.rightNum>
    <ul class="pagination" style="float: right;">
        <li <#if page.currentPage == 1>class="disabled"</#if>><a id="prev" data-page="${page.currentPage}"
                                                                 href="javascript:void(0);">&laquo;</a></li>
        <#list leftNum..rightNum as p>
            <li <#if p == page.currentPage>class="active"</#if>><a name="pageCode" href="javascript:void(0);">${p}</a>
            </li>
        </#list>
        <li <#if page.currentPage == page.totalPage>class="disabled"</#if>><a id="next" data-page="${page.currentPage}"
                                                                              href="javascript:void(0);">&raquo;</a>
        </li>
    </ul>
</#if>
</nav>

</div>