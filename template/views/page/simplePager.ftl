<#-- 自定义的分页指令。
属性说明：
   pageNo      当前页号(int类型)
   pageSize    每页要显示的记录数(int类型)
   toURL       点击分页标签时要跳转到的目标URL(string类型)
   recordCount 总记录数(int类型)
 使用方式：
  <#if recordCount??>
    <#import "/pager.ftl" as q>
    <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="testpager.do"/>
  </#if>
 -->

<#macro pager pageNo pageSize toURL recordCount>
<#-- 定义局部变量pageCount保存总页数 -->
    <#assign pageCount=((recordCount + pageSize - 1) / pageSize)?int>
    <#if recordCount==0><#return/></#if>

<#-- 页号越界处理 -->
    <#if (pageNo > pageCount)>
        <#assign pageNo=pageCount>
    </#if>
    <#if (pageNo < 1)>
        <#assign pageNo=1>
    </#if>
<#-- 输出分页表单 -->
<div class="panel-footer clearfix">
    <ul class="pagination pagination-xs m-top-none pull-right">

    <#-- 上一页处理 -->
        <#if (pageNo == 1)>
            <li class="disabled"><a href="javascript:;">上一页</a></li>
        <#else>
            <li><a href="javascript:;" onclick="turnOverPage(${pageNo - 1})">上一页</a></li>
        </#if>
    <#-- 如果前面页数过多,显示... -->
        <#assign start=1>
        <#if (pageNo > 4)>
            <#assign start=(pageNo - 1)>
            <li><a href="javascript:;" onclick="turnOverPage(1)">1</a></li>
            <li><a href="javascript:;" onclick="turnOverPage(2)">2</a></li>
            <li><a href="javascript:;">...</a></li>
        </#if>
    <#-- 显示当前页号和它附近的页号 -->
        <#assign end=(pageNo + 1)>
        <#if (end > pageCount)>
            <#assign end=pageCount>
        </#if>
        <#list start..end as i>
            <#if (pageNo==i)>
                <li class="active" ><a href="javascript:;">${i}</a></li>
            <#else>
                <li><a href="javascript:;" onclick="turnOverPage(${i})">${i}</a></li>
            </#if>
        </#list>
    <#-- 如果后面页数过多,显示... -->
        <#if (end < pageCount - 2)>
            <li><a href="javascript:;">...</a></li>
        </#if>
        <#if (end < pageCount - 1)>
            <li><a href="javascript:;" onclick="turnOverPage(${pageCount - 1})">${pageCount-1}</a></li>
        </#if>
        <#if (end < pageCount)>
            <li><a href="javascript:;" onclick="turnOverPage(${pageCount})">${pageCount}</a></li>
        </#if>
    <#-- 下一页处理 -->
        <#if (pageNo == pageCount)>
            <li class="disabled"><a href="javascript:;">下一页</a></li>
        <#else>
            <li><a href="javascript:;" onclick="turnOverPage(${pageNo + 1})">下一页</a></li>
        </#if>

    </ul>
</div>
</#macro>