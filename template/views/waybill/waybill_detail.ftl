<content tag="staticPath">
    /static
</content>
<content tag="pageName">
    waybill_detail
</content>
<content tag="jsmain">
    finance/waybill_detail.js
</content>
<content tag="css">
    finance/waybill_detail.css
</content>


<script>
    function change1() {
        document.getElementById("oldMoney1").style.display="none";
        document.getElementById("money1").style.display="";
        document.getElementById("change1").style.display="none";
        document.getElementById("doChange1").style.display="";
    }

    function doChange1() {
        var id = $('#waybillId').val();
        var money = $('#money1').val();
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/partner/pWaybillAudit/changeActualPay",
            data: {
                id: id,
                money: money
            },
            success : function(data){
                if(data.success){
                    alert("提交修改成功，请到“异常订单审核”中进行审核");
                    window.location.reload();
                }else {
                    alert(data.message);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });

    }

    function change2() {
        document.getElementById("oldMoney2").style.display="none";
        document.getElementById("money2").style.display="";
        document.getElementById("change2").style.display="none";
        document.getElementById("doChange2").style.display="";
    }

    function doChange2() {
        var id = $('#waybillId').val();
        var money = $('#money2').val();
        $.ajax({
            dataType: 'json',
            type : 'post',
            url : "/partner/pWaybillAudit/changeActualCharge",
            data: {
                id: id,
                money: money
            },
            success : function(data){
                if(data.success){
                    alert("提交修改成功，请到“异常订单审核”中进行审核");
                    window.location.reload();
                }else {
                    alert(data.message);
                }
            },
            error:function(XMLHttpRequest ,errMsg){
                alert("网络连接失败");
            }
        });

    }

</script>

<#if detail?exists>

<input type="hidden" id="waybillId" value="${detail.id}">

<div class="panel panel-default">
    <div class="panel-heading">
        <span class="panel-title">
            ${detail.senderName !''}
        </span>
        <span>
            ${detail.senderAddress !''}
        </span>
    </div>
        <table class="table" style="width:100%;">
            <tr>
                <td colspan="1" rowspan="3" style="border-right: solid 1px #ddd;">
                    <p>
                        <#switch detail.status>
                            <#case 0>待接单<#break>
                            <#case 20>已接单<#break>
                            <#case 30>已取货<#break>
                            <#case 50>已送达<#break>
                            <#case 99>已取消<#break>
                            <#case 100>超时订单<#break>
                        </#switch>
                    </p>
                    <p>
                    ${detail.timeDifference !''}
                    </p>
                </td>
                <td colspan="2">
                    <p>
                        收货地址
                    </p>
                    <p>
                        ${detail.recipientAddress !''}
                    </p>
                </td>
                <td colspan="2">
                    <p>
                        收货人
                    </p>
                    <p>
                        <#if detail.recipientName?has_content>
                            ${detail.recipientName !''}
                        <#else>
                            未填写姓名
                        </#if>
                    </p>
                </td>
                <td colspan="2">
                    <p>
                        发单时间
                    </p>
                    <p>
                         ${detail.sendOrderTime !''}
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p>
                        订单号
                    </p>
                    <p>
                        ${detail.platformOrderId !''}
                    </p>
                </td>
                <td colspan="4">
                    <p>
                        配送员
                    </p>
                    <p>
                        <#if detail.status?exists && (detail.status == 0 || detail.status == 100)>
                            待定
                        <#else>
                            ${detail.riderName !''}
                        </#if>
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <p>商品数量</p>
                    <p>${detail.goodsCount}</p>
                </td>
                <td colspan="1">
                    <p>商品价格</p>
                    <p>￥${detail.goodsPrice?string('0.0')}</p>
                </td>
                <td colspan="1">
                    <p>应付价格</p>
                    <p style="color: red;">￥${detail.planPayAmount?string('0.0')}</p>
                </td>
                <td colspan="1">
                    <p>应收价格</p>
                    <p style="color: red">￥${detail.planChargeAmount?string('0.0')}</p>
                </td>
                <td colspan="1" width="150px">
                    <table style="border: hidden" >
                        <tr>
                            <td>
                                <p>实付价格</p>
                                <p style="color: red;">
                                    <span id="oldMoney1">￥${detail.actualPayAmount?string('0.0')}</span>
                                    <input id="money1" style="display: none;width: 50px;" type="text" value="${detail.actualPayAmount?string('0.0')}">
                                </p>
                            </td>
                            <#if audit?exists>
                                <#if audit == 1>
                                <td>
                                    &nbsp; &nbsp;<a id="change1" class="btn btn-success btn-sm" href="javascript:change1();">修改</a>
                                    <a  id="doChange1" class="btn btn-success btn-sm" style="display:none;" href="javascript:doChange1();">提交</a>
                                </td>
                            </#if>
                        </#if>
                        </tr>
                    </table>

                </td>
                <td colspan="1" width="150px">
                    <table style="border: hidden">
                        <tr>
                            <td>
                                <p>实收价格</p>
                                <p style="color: red">
                                    <span id="oldMoney2">￥${detail.actualChargeAmount?string('0.0')}</span>
                                    <input id="money2" style="display: none;width: 50px;" type="text" value="${detail.actualChargeAmount?string('0.0')}">
                                </p>
                            </td>
                            <#if audit?exists>
                                <#if audit == 1>
                                    <td>
                                        &nbsp; &nbsp;<a id="change2" class="btn btn-success btn-sm" href="javascript:change2();">修改</a>
                                        <a  id="doChange2" class="btn btn-success btn-sm" style="display:none;" href="javascript:doChange2();">提交</a>
                                    </td>
                                </#if>
                            </#if>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
</div>

<div>
    <div class="panel panel-default" style="width: 50%; display: inline; float: left;">
        <div class="panel-heading">
            <span class="panel-title">
                商品详情
            </span>
        </div>
        <table class="table">
            <tbody>
            <#if detail.goodsDetailList?exists>
                <#list detail.goodsDetailList as good>
                <tr>
                    <td width="34%">
                        ${good.name}
                    </td>
                    <td width="33%">
                        X${good.count}
                    </td>
                    <td width="33%">
                        ￥${good.price?string('0.0')}
                    </td>
                </tr>
                </#list>
            </#if>
            <tr class="active">
                <td>合计</td>
                <td>X${detail.goodsCount}</td>
                <td>￥${detail.goodsPrice?string('0.0')}</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="panel panel-default" style="width: 50%; display: inline; float:right;">
            <div class="panel-heading">
                <span class="panel-title">
                    活动详情
                </span>
            </div>
            <table class="table">
                <tbody>
                    <#if detail.activityDetail?exists>
                        <#assign map= detail.activityDetail>
                        <#assign keys= map?keys>
                        <#list keys as key>
                        <tr>
                            <td width="50%">
                            ${key}
                            </td>
                            <td width="50%">
                                ￥${map[key]}
                            </td>
                        </tr>
                        </#list>
                    </#if>
                <tr class="active">
                    <td>合计</td>
                    <td>￥${detail.activityTotalMoney?string('0.0')}</td>
                </tr>
                </tbody>
            </table>
    </div>
    <div style="clear:both"></div>

</div>


<div>
    <div class="panel panel-default" style="width: 100%; float: left;">
        <div class="panel-heading">
            <span class="panel-title">
                订单处理进度及用时
            </span>
        </div>
        <#if canceled?exists && canceled == 1>
            <#if statusAndTime?exists>
                <#assign size = statusAndTime?size>
                <table class="table status-circle">
                    <tr>
                        <#list statusAndTime as sat>
                            <td>
                                <div class="col-xs-6 col-sm-3 placeholder text-center" style=" width: 100%">
                                    <#if sat_index == size-1>
                                        <img width="50px;" height="50px;" src="/static/imgs/cancel.png" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                                    <#else>
                                        <img width="50px;" height="50px;" src="/static/imgs/green.png" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                                    </#if>

                                    <span class="text-muted"><#noescape>${sat}</#noescape></span>
                            </td>
                            <#if sat_index != size - 1>
                                <td style="text-align: center;">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            用时<br/>${timeCostList[sat_index]}分钟
                                        </div>
                                    </div>
                                </td>
                            </#if>
                        </#list>
                    </tr>
                </table>
            </#if>
        <#else>
            <#if axisList?exists>
                <#assign size = timeCostList?size>
                <table class="table status-circle" style="table-layout:fixed;">
                    <tr>
                        <#list axisList as axis>
                            <#if axis.isGreen == 1>
                                <td>
                                    <div class="col-xs-6 col-sm-3 placeholder text-center" style=" width: 100%">
                                        <img width="50px;" height="50px;" src="/static/imgs/green.png" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                                        <span class="text-muted"><#noescape>${axis.statusAndTimeHtml}</#noescape></span>
                                </td>
                                <#if axis.status != 50>
                                    <td style="text-align: center;">
                                        <div class="panel panel-default">
                                            <#if axis_index == size - 1 && detail.status != 50>
                                                    <div class="panel-body" style="color:#e78f08">
                                                        已进行<br/>${timeCostList[axis_index]}分钟
                                                    </div>
                                            <#else>
                                                    <div class="panel-body">
                                                        用时<br/>${timeCostList[axis_index]}分钟
                                                    </div>
                                            </#if>
                                        </div>
                                    </td>
                                </#if>
                            <#elseif axis.isGreen == 0>
                                <td>
                                    <div class="col-xs-6 col-sm-3 placeholder text-center" style=" width: 100%">
                                        <img width="50px;" height="50px;" src="/static/imgs/gray.png" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                                        <span class="text-muted">${axis.statusAndTimeHtml}</span>
                                    </div>
                                </td>
                            </#if>
                        </#list>
                    </tr>
                </table>
            </#if>
        </#if>
    </div>
        
        <div style="clear:both"></div>
</div>

<#else>
没有查询到相应的订单信息
</#if>
