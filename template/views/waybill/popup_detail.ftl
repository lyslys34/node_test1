
<style type="text/css">
    .btn-bg{
        background:#00abe4;
        color: #FFFFFF;
    }
    .panel{
        margin-bottom: 5px;
    }
    .panel-heading{
        padding:5px 2px;
    }
    .order-operate>tbody>tr:nth-child(even),.order-operate>thead>tr{
        background: #f9f9f9;
    }
    .order-operate>thead>tr{
        color: inherit;
    }

    .bill-detail>tbody>tr:nth-child(even),.bill-detail>thead>tr{
        background: #f9f9f9;
    }
    .bill-detail>thead>tr{
        color: inherit;
    }
    .line-pay, .pay-type{
        color: #fff; padding: 3px 5px;border-radius: 3px;
    }
    .line-pay{
        background: #25ADEE;margin-left: 5px;
    }
    .pay-type{
        background: #f8c833;
    }
</style>
<#if detail?exists>

<div class="panel panel-default">
    <div class="panel-heading">
        <span class="" style="font-size: 14px;padding-top: 10px;padding-left: 5px;">
            &nbsp;<b>#${detail.poiSeq !''}</b>
            &nbsp;订单号:&nbsp;&nbsp;&nbsp;${detail.platformOrderId !''}&nbsp;&nbsp;
            <a href="http://dispatch.peisong.meituan.com/playback/home?tab=3&id=${detail.platformOrderId !''}" target="_blank">
                <button type="button" class="btn btn-default btn-bg">查看调度情况</button>
            </a>

        </span>


        <span class="pop_wanttime_span">
            【
            <#if detail.isPrebook?exists && detail.isPrebook != 0>
                    预订单
                    <#else>即时单
              </#if>】期望送达:&nbsp;&nbsp;&nbsp;${detail.deliveredFormatTime ! ''}
        </span>
    </div>

    <div  style="border-bottom: solid 1px #ddd; position:relative;">
        <div class="" style="width: 50%; display: inline; float: left; padding-top:10px">
            <div>
                <span style="font-size:14px; padding-left:20px;">商家信息</span>
            </div>
            <div style="padding-left:30px; padding-top:10px;">
                <p>商家名称：${detail.senderName !''}
                    <#if detail.pkgType?exists && detail.pkgType == 2>
                        <span class="pay-type">代购</span>
                    </#if>
                    <#if detail.logisticsOfflinePayOrderRecord?exists && detail.logisticsOfflinePayOrderRecord == 1>
                        <span class="line-pay">线下结算</span>
                    </#if>
                </p>
                <p>商家地址：${detail.senderAddress !''}</p>
                <p>商家电话：${detail.senderPhone !''}</p>
                <p>取货应付：<span style="color: red">￥${detail.planPayAmount?string('0.00')}</span>&nbsp;&nbsp;&nbsp;
                    实付：<span style="color: #3C8DBC">￥${detail.actualPayAmount?string('0.00')}</span></p>
            </div>
        </div>

        <div class="" style="width: 50%; display: inline; float:right; padding-top:10px">
            <div>
                <span style="font-size:14px;padding-left:20px;">顾客信息</span>
            </div>
            <div style="padding-left:30px; padding-top:10px;">
                <p>顾客地址：${detail.recipientAddress !''}</p>
                <p>顾客电话：<span style="color: #3C8DBC">${detail.recipientPhone !''}</span></p>
                <p>顾客姓名：${detail.recipientName !''}</p>
                <p>送达应收：
                    <span style="color:red">
                        <#if detail.isPayed?exists && detail.isPayed == 0>
                            ￥${detail.planChargeAmount?string('0.00')}
                        <#else>
                            ￥0.00(已在线支付)
                        </#if>
                    </span>&nbsp;&nbsp;&nbsp;

                    实收：<span style="color: #3C8DBC">￥${detail.actualChargeAmount?string('0.00')}</span></p>
            </div>
        </div>

        <div style="position:absolute; width:2px; background-color:#ddd; left:50%; height:100%"></div>
        <div style="clear:both"></div>
    </div>



</div>
<div  class="panel panel-default" style="border-bottom: solid 1px #ddd;">
    <div class="" style="width: 100%; display: inline; float: left; padding-top:2px">
        <div class="panel-heading">
            <span style="font-size:14px; padding-left:5px;">订单明细</span>
        </div>

        <div style="padding-left: 20px;padding-top: 10px;">

            <#if detail.remark?exists && detail.remark != ''>
                    <p>备注：${detail.remark !''}</p>
                <#else>
                    <p style="color:#c8c8c8">备注：无</p>
            </#if>
            <#if detail.invoiceTitle?exists && detail.invoiceTitle != ''>
                    <p>开票信息：${detail.invoiceTitle}</p>
                <#else>
                    <p style="color:#c8c8c8">开票信息：无</p>
            </#if>

        </div>
        <div style="width:100%; padding-left: 20px; padding-bottom: 20px;">
            <table class="table bill-detail" border="1" bordercolor="#ddd" width="100%" >
                <thead>
                <tr>
                    <th>商品名称</th>
                    <th>数量</th>
                    <th>单价（元）</th>
                    <th>应收顾客（元）</th>
                </tr>
                </thead>
                <tbody>
                    <#if detail.goodsDetailList?exists>
                        <#list detail.goodsDetailList as goods>
                            <tr>
                                <td>${goods.name !''}</td>
                                <td>${goods.count !'-'}</td>
                                <td>${goods.price?string('0.00') !''}</td>
                                <#function mul x y>
                                    <#return x*y>
                                </#function>
                                <td>${mul(goods.count,goods.price)?string('0.00')}</td>
                            </tr>

                        </#list>
                    </#if>
                    <tr>
                        <td>活动减免</td>
                        <td>-</td>
                        <td>-</td>
                        <#function sub x y>
                            <#return x-y>
                        </#function>
                        <td>${sub(detail.pkgValue,detail.pkgPrice)?string('0.00')}</td>
                    </tr>
                    <#if detail.boxPriceTotal??>
                        <tr>
                            <td>餐盒费</td>
                            <td>-</td>
                            <td>-</td>
                            <td>${detail.boxPriceTotal?string('0.00') !''}</td>
                        </tr>
                    </#if>
                    <tr>
                        <td>配送费</td>
                        <td>-</td>
                        <td>-</td>
                        <td>${detail.deliveryFee?string('0.00') !''}</td>
                    </tr>
                    <tr>
                        <td>合计</td>
                        <td>
                            <#assign sum1 = 0>
                                <#if detail.goodsDetailList?exists>
                                    <#list detail.goodsDetailList as goods>
                                        <#if goods.count?exists>
                                            <#assign sum1 = sum1 + goods.count>
                                        </#if>
                                    </#list>
                                    ${sum1}
                                </#if>
                        </td>
                        <td>-</td>
                        <td>
                            ${detail.pkgPrice?string('0.00') !''}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


    <div style="clear:both"></div>

</div>

<div class="panel panel-default">
    <div class="" style="width: 100%; display: inline; float: left; padding-top:2px">
        <div class="panel-heading">
            <span style="font-size:14px; padding-left:5px;">骑手名称：${detail.riderNameWithoutPhone !''}</span>
            <span style="font-size:14px; padding-left:20px;">电话：${detail.riderPhone !''}</span>
            <span style="font-size:14px; padding-left:20px;">站点：${detail.orgName !''}</span>
        </div>
        <#if canceled?exists && canceled == 1>
            <#if statusAndTime?exists>
                <#assign size = statusAndTime?size>
                <table class="table status-circle" style="table-layout:fixed;">
                    <tr>
                        <#list statusAndTime as sat>
                            <td>
                                <div class="col-xs-6 col-sm-3 placeholder text-center" style=" width: 100%">
                                    <#if sat_index == size-1>
                                        <img width="50px;" height="50px;" src="/static/imgs/cancel.png" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                                    <#else>
                                        <img width="50px;" height="50px;" src="/static/imgs/green.png" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                                    </#if>

                                    <span class="text-muted"><#noescape>${sat}</#noescape></span>
                            </td>
                            <#if sat_index != size - 1>
                                <td style="text-align: center;">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <br/>
                                            <#if timeCostList[sat_index]<60>
                                                ${timeCostList[sat_index]}秒
                                            <#elseif timeCostList[sat_index]<3600>
                                                ${(timeCostList[sat_index]/60)?int}分
                                                <#if timeCostList[sat_index]%60 != 0>
                                                    ${timeCostList[sat_index]%60}秒
                                                </#if>
                                            <#elseif timeCostList[sat_index]<86400>
                                                ${(timeCostList[sat_index]/3600)?int}时
                                                <#if timeCostList[sat_index]%3600 != 0>
                                                    ${timeCostList[sat_index]%3600/60}分
                                                </#if>
                                                <#if timeCostList[sat_index]%3600/60 != 0>
                                                    ${timeCostList[sat_index]%3600%60}秒
                                                </#if>
                                            <#else>
                                                ${(timeCostList[sat_index]/86400)?int}天
                                                <#if timeCostList[sat_index]%86400 != 0>
                                                    ${(timeCostList[sat_index]%86400/3600)?int}时
                                                </#if>
                                                <#if timeCostList[sat_index]%86400 != 0>
                                                    ${timeCostList[sat_index]%86400%3600/60}分
                                                </#if>
                                                <#if timeCostList[sat_index]%86400/60 != 0>
                                                    ${timeCostList[sat_index]%86400%3600%60}秒
                                                </#if>
                                            </#if>
                                        </div>
                                    </div>
                                </td>
                            </#if>
                        </#list>
                    </tr>
                </table>
            </#if>
        <#else>
            <#if axisList?exists>
                <#assign size = timeCostList?size>
                <table class="table status-circle" style="table-layout:fixed;">
                    <tr>
                        <#list axisList as axis>
                            <#if axis.isGreen == 1>
                                <td>
                                    <div class="col-xs-6 col-sm-3 placeholder text-center" style=" width: 100%">
                                        <img width="50px;" height="50px;" src="/static/imgs/green.png" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                                        <span class="text-muted"><#noescape>${axis.statusAndTimeHtml}</#noescape></span>
                                </td>
                                <#if axis.status != 50>
                                    <td style="text-align: center;">
                                        <div class="panel panel-default">
                                            <#if axis_index == size - 1 && detail.status != 50>
                                                <div class="panel-body" style="color:#e78f08">
                                                    <br/>
                                                    <#if timeCostList[axis_index]<60>
                                                        ${timeCostList[axis_index]}秒
                                                    <#elseif timeCostList[axis_index]<3600>
                                                        ${(timeCostList[axis_index]/60)?int}分
                                                        <#if timeCostList[axis_index]%60 != 0>
                                                            ${timeCostList[axis_index]%60}秒
                                                        </#if>
                                                    <#elseif timeCostList[axis_index]<86400>
                                                        ${(timeCostList[axis_index]/3600)?int}时
                                                        <#if timeCostList[axis_index]%3600 != 0>
                                                            ${timeCostList[axis_index]%3600/60}分
                                                        </#if>
                                                        <#if timeCostList[axis_index]%3600/60 != 0>
                                                            ${timeCostList[axis_index]%3600%60}秒
                                                        </#if>
                                                    <#else>
                                                        ${(timeCostList[axis_index]/86400)?int}天
                                                        <#if timeCostList[axis_index]%86400 != 0>
                                                            ${(timeCostList[axis_index]%86400/3600)?int}时
                                                        </#if>
                                                        <#if timeCostList[axis_index]%86400 != 0>
                                                            ${timeCostList[axis_index]%86400%3600/60}分
                                                        </#if>
                                                        <#if timeCostList[axis_index]%86400/60 != 0>
                                                            ${timeCostList[axis_index]%86400%3600%60}秒
                                                        </#if>
                                                    </#if>
                                                </div>
                                            <#else>
                                                <div class="panel-body">
                                                    <br/>
                                                    <#if timeCostList[axis_index]<60>
                                                        ${timeCostList[axis_index]}秒
                                                    <#elseif timeCostList[axis_index]<3600>
                                                        ${(timeCostList[axis_index]/60)?int}分
                                                        <#if timeCostList[axis_index]%60 != 0>
                                                            ${timeCostList[axis_index]%60}秒
                                                        </#if>
                                                    <#elseif timeCostList[axis_index]<86400>
                                                        ${(timeCostList[axis_index]/3600)?int}时
                                                        <#if timeCostList[axis_index]%3600 != 0>
                                                            ${timeCostList[axis_index]%3600/60}分
                                                        </#if>
                                                        <#if timeCostList[axis_index]%3600/60 != 0>
                                                            ${timeCostList[axis_index]%3600%60}秒
                                                        </#if>
                                                    <#else>
                                                        ${(timeCostList[axis_index]/86400)?int}天
                                                        <#if timeCostList[axis_index]%86400 != 0>
                                                            ${(timeCostList[axis_index]%86400/3600)?int}时
                                                        </#if>
                                                        <#if timeCostList[axis_index]%86400 != 0>
                                                            ${timeCostList[axis_index]%86400%3600/60}分
                                                        </#if>
                                                        <#if timeCostList[axis_index]%86400/60 != 0>
                                                            ${timeCostList[axis_index]%86400%3600%60}秒
                                                        </#if>
                                                    </#if>
                                                </div>
                                            </#if>
                                        </div>
                                    </td>
                                </#if>
                            <#elseif axis.isGreen == 0>
                                <td>
                                    <div class="col-xs-6 col-sm-3 placeholder text-center" style=" width: 100%">
                                        <img width="50px;" height="50px;" src="/static/imgs/gray.png" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                                        <span class="text-muted">${axis.statusAndTimeHtml}</span>
                                    </div>
                                </td>
                            </#if>
                        </#list>
                    </tr>
                </table>
            </#if>
        </#if>
    </div>


    <div style="clear:both"></div>
</div>

<div class="panel panel-default" style="padding-left: 5px;">
    <div class="panel-heading">
        <span style="font-size: 14px;">
            配送评分
        </span>
    </div>
    <div style="padding-left: 20px;">
        <div style="padding-top: 5px;padding-bottom: 5px;">

            <#if feedback.deliveryCommentScore?exists && feedback.deliveryCommentScore != 0>
                <#assign x = feedback.deliveryCommentScore>
                <#list 1..x as i>
                    <i class="fa fa-star fa-lg" style="color:#ffc609; letter-spacing: -3px;"></i>
                </#list>
                <#assign y = 5-feedback.deliveryCommentScore>
                <#if y != 0 >
                    <#list 1..y as i>
                        <i class="fa fa-star fa-lg" style="color:#c8c8c8; letter-spacing: -3px;"></i>
                    </#list>
                </#if>
                &nbsp;<span style="color: #c8c8c8;font-size: 14px;">${feedback.deliveryCommentScore}分</span>
            <#else>
                <span>未评分</span>
            </#if>

        </div>
        <p>${feedback.cleanComment !'未评价'}</p>
    </div>
</div>


<div>
    <div class="panel panel-default" style="width: 100%; float: left;">
        <div class="panel-heading" style="padding-left: 5px">
            <span class="panel-title" style="font-size:14px;">
                订单操作记录
            </span>
        </div>

        <div style="width: 100%;padding: 5px 5px 10px 5px" >
            <table class="table order-operate" border="1" bordercolor="#ddd">
                <thead>
                    <tr>
                        <td>操作</td>
                        <td>内容</td>
                        <td>操作人[手机号]</td>
                        <td>操作时间</td>
                    </tr>
                </thead>
                <tbody>
                    <#if bmWaybillFlowDetailViews?exists>
                        <#list bmWaybillFlowDetailViews as bills>
                            <tr>
                                <td>
                                    ${bills.operate !''}
                                </td>
                                <td>
                                    ${bills.note !''}
                                </td>
                                <td>
                                    ${bills.operaterName !''}
                                    <#if bills.operaterPhone?exists && bills.operaterPhone !=''>
                                        [${bills.operaterPhone}]
                                        <#else>
                                        &nbsp;
                                    </#if>
                                </td>
                                <td>
                                    ${bills.date !''}
                                </td>
                            </tr>
                        </#list>
                    </#if>
                </tbody>
            </table>
        </div>

    </div>
        
        <div style="clear:both"></div>
</div>

<#else>
没有查询到相应的订单信息
</#if>
