<#include "grade_common_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="cssmain">/grade/grade_franchisee.css</content>
<content tag="javascript">/grade/franchisee_platform_order.js</content>

<div id="main-container">
	<div class="panel panel-default table-responsive" style="margin-top:10px;padding:10px 10px;">		
		<ul class="nav nav-tabs">		    
		    <li role="presentation"><a href="/grade/franchiseeGrade">加盟站评级列表</a></li>
		    <li role="presentation" class="active"><a href="/grade/franchiseePlatformOrder">加盟站订单列表</a></li>
		    <li role="presentation"><a href="/grade/config2">评级方案配置</a></li>
		    <li role="presentation"><a href="/grade/city">关联城市</a></li>
		</ul>
		<br>
		<form class="no-margin" id="form" data-validate="parsley" novalidate="" method="post"
			  action="/grade/franchiseePlatformOrder">
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label class="control-label">加盟站：</label>
						<select class="form-control input-sm" id="orgId" name="orgId">
                        <option value="0">全部</option>
                        <#if orgList?exists>
                            <#list orgList as orgView>
                                <option value="${orgView.orgId !0}"
                                    <#if orgView.orgId == orgId>selected="selected" </#if> >${orgView.orgName !''}</option>
                            </#list>
                        </#if>
                        </select>
					</div>
				</div>
				<div class="col-md-3 month-col">
					<div class="form-group">
						<label class="control-label">月份：</label>
						<div class="form-group form-inline">
							<input type="text" readonly
                                   class="form-control input-sm date-picker J-datepicker"
                                   name="timeStr"
                                   value="<#if timeStr?exists>${timeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>"/>
						</div>
					</div>
				</div>
				<div class="col-md-1 query-col">
                    <div class="form-group" style="padding-top:18px">
                        <button type="submit" class="btn btn-success js_submit_btn">查询</button>
                    </div>
                </div>
                <#if author??>
	                <div class="col-md-2 platform-col">
	                	<div class="form-group" style="padding-top:18px">
	                		<button type="button" class="btn btn-success" id="upload-exception-order-btn">异常订单录入</button>
	                	</div>
	                </div>
	                <div class="col-md-2 platform-col">
	                	<div class="form-group" style="padding-top:18px">
	                		<button type="button" class="btn btn-success" id="upload-complaint-order-btn">客诉订单录入</button>
	                	</div>
	                </div>
	                <div class="col-md-2 platform-col-1">
	                	<div class="form-group" style="padding-top:18px">
	                		<button type="button" class="btn btn-success" id="upload-undelived-order-btn">未送达点已送达订单录入</button>
	                	</div>
	                </div>
	                <div class="col-md-2">
	                	<div class="form-group" style="padding-top:18px">
	                		<button type="button" class="btn btn-success" id="upload-org-blacklist-btn">加盟站黑名单录入</button>
	                	</div>
	                </div>
                </#if>
                <div class="col-md-2">
                	<div class="form-group" style="padding-top:18px">
                		<button type="button" class="btn btn-danger js_export_btn" style="color:white">下载</button>
                	</div>
                </div>
			</div>
		</form>
	</div>
	<div>
		<p>注意：参与加盟站评级的订单为加盟站管理组去除异常订单（恶劣天气等其他异常情况订单不计入考核）后的订单。</p>	
		<div class="panel panel-default table-responsive">
			<table class="table table-striped" id="checkTable">
				<thead>
					<tr>
						<th style="text-align:center;">订单号</th>
						<th style="text-align:center;">是否完成</th>
						<th style="text-align:center;">未完成处罚金额</th>
						<th style="text-align:center;">是否超时</th>
						<th style="text-align:center;">超时处罚金额</th>
						<th style="text-align:center;">原邮资</th>
						<th style="text-align:center;">是否客诉</th>
						<th style="text-align:center;">客诉处罚金额</th>
						<th style="text-align:center;">是否未送达点已送达</th>
						<th style="text-align:center;">未送达点已送达处罚金额</th>
						<th style="text-align:center;">是否连击</th>
						<th style="text-align:center;">连击处罚金额</th>
						<th style="text-align:center;">邮资惩罚金额</th>
					</tr>
				</thead>
				<tbody>
					<#if platformOrderList??>
						<#list platformOrderList as platformOrder>
							<tr>
								<td style="text-align:center;">${platformOrder.order_id !''}</td>
								<td style="text-align:center;"><#if platformOrder.is_finished == 1>是<#elseif platformOrder.is_finished == 0>否</#if></td>
								<td style="text-align:center;">${platformOrder.unfinish_punish_amt !0}</td>
								<td style="text-align:center;"><#if platformOrder.is_overtime == 1>是<#elseif platformOrder.is_overtime == 0>否</#if></td>
								<td style="text-align:center;">${platformOrder.overtime_punish_amt !0}</td>
								<td style="text-align:center;">${platformOrder.origin_postage !0}</td>
								<td style="text-align:center;"><#if platformOrder.is_complain == 1>是<#elseif platformOrder.is_complain == 0>否</#if></td>
								<td style="text-align:center;">${platformOrder.complain_punish_amt !0}</td>
								<td style="text-align:center;"><#if platformOrder.is_click_early == 1>是<#elseif platformOrder.is_click_early == 0>否</#if></td>
								<td style="text-align:center;">${platformOrder.click_early_punish_amt !0}</td>
								<td style="text-align:center;"><#if platformOrder.is_batter == 1>是<#elseif platformOrder.is_batter == 0>否</#if></td>
								<td style="text-align:center;">${platformOrder.batter_punish_amt !0}</td>
								<td style="text-align:center;">${platformOrder.postage_amt !0}</td>
							</tr>
						</#list>
					</#if>
				</tbody>
			</table>
			<div style="float: left; padding: 10px; 15px"><span>共${totalNum!'0'}项</span></div>
		    <#import "../page/pager.ftl" as q>
		        <#if totalNum??>
		            <@q.pager pageNo=pageNo pageSize=pageSize recordCount=totalNum toURL="/grade/franchiseePlatformOrder"/>
		        </#if>
		</div>
	</div>
	<div id="upload-file-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">上传excel文件</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group ">
								<form id="upload-file-form" enctype="multipart/form-data">
                                     <input type="file" name="file" id="upload-file-input" />
                                     <input id="upload-file-type-input" type="text" name="fileType" style="display:none" value="1"/>
                                </form>
                                <br/>
                                <textarea id="failed-record-text" rows="5" cols="35" style="resize: none;display:none"></textarea>
                                <br/>
                                <button type="button" class="btn btn-default" id="upload-file-btn">上传</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="button" class="btn btn-default" data-dismiss="modal" id="upload-file-cancel-btn">取消</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
</div>
	<div id="down_temp"></div>
<script>
	document.title = '加盟站订单评级列表';
	$('select').select2();
	var orgId = "${orgId !0}";
	var timeStr = "${timeStr !'0'}";
</script>
