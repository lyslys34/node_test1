<#include "grade_common_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="cssmain">/grade/grade_franchisee.css</content>
<content tag="javascript">/grade/franchisee_grade.js</content>

<div id="main-container">
	<div class="panel panel-default table-responsive" style="margin-top:10px;padding:10px 10px;">
		<ul class="nav nav-tabs">		    
		    <li role="presentation" class="active"><a href="/grade/franchiseeGrade">加盟站评级列表</a></li>
		    <li role="presentation"><a href="/grade/franchiseePlatformOrder">加盟站订单列表</a></li>
		     <li role="presentation"><a href="/grade/config2">评级方案配置</a></li>
		    <li role="presentation"><a href="/grade/city">关联城市</a></li>
		</ul>
		<br>
		<form class="no-margin" id="form" data-validate="parsley" novalidate="" method="post"
			  action="/grade/franchiseeGrade">
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label class="control-label">加盟站：</label>
						<select class="form-control input-sm" id="orgId" name="orgId">
                        <option value="0">全部</option>
                        <#if orgList?exists>
                            <#list orgList as orgView>
                                <option value="${orgView.orgId !0}"
                                    <#if orgView.orgId == orgId>selected="selected" </#if> >${orgView.orgName !''}</option>
                            </#list>
                        </#if>
                        </select>
					</div>
				</div>
				<div class="col-md-3 month-col">
					<div class="form-group">
						<label class="control-label">月份：</label>
						<div class="form-group form-inline">
							<input type="text" readonly
                                   class="form-control input-sm date-picker J-datepicker js_date_start"
                                   name="timeStr"
                                   value="<#if timeStr?exists>${timeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>"/>
						</div>
					</div>
				</div>
				<div class="col-md-1">
                    <div class="form-group" style="padding-top:18px">
                        <button type="submit" class="btn btn-success js_submit_btn">查询</button>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group" style="padding-top:18px">
                        <button type="button" class="btn btn-danger js_export_btn" style="color:white">下载</button>
                    </div>
                </div>
			</div>
		</form>
	</div>
	<div>
		<p>注意：参与加盟站评级的订单为加盟站管理组去除异常订单（恶劣天气等其他异常情况订单不计入考核）后的订单。</p>		
		<div class="panel panel-default table-responsive">
			<table class="table table-striped" id="checkTable">
				<thead>
					<tr>
						<th style="text-align:center;">加盟站</th>
						<th style="text-align:center;">总订单数</th>
						<th style="text-align:center;">未完成订单数</th>
						<th style="text-align:center;">订单完成率</th>
						<th style="text-align:center;">未完成惩罚单数</th>
						<th style="text-align:center;">未完成惩罚金额</th>
						<th style="text-align:center;">超时订单数</th>
						<th style="text-align:center;">订单超时率</th>
						<th style="text-align:center;">超时惩罚单</th>
						<th style="text-align:center;">超时惩罚金额</th>
						<th style="text-align:center;">客诉数量</th>
						<th style="text-align:center;">客诉处罚金额</th>
						<th style="text-align:center;">未送达点已送达数量</th>
						<th style="text-align:center;">未送达点已送达处罚金额</th>
						<th style="text-align:center;">连击数量</th>
						<th style="text-align:center;">连击处罚金额</th>
						<th style="text-align:center;">级别评定</th>
						<th style="text-align:center;">邮资惩罚总额</th>
					</tr>
				</thead>
				<tbody>
					<#if gradeViewList??>
						<#list gradeViewList as gradeView>
							<tr>
								<td style="text-align:center;">${gradeView.org_name !''}</td>
								<td style="text-align:center;">${gradeView.waybill_cnt !0}</td>
								<td style="text-align:center;">${gradeView.waybill_undelivered_cnt !0}</td>
								<td style="text-align:center;">${gradeView.waybill_delivered_ratio !0}%</td>
								<td style="text-align:center;">${gradeView.waybill_undelivered_punish_cnt !0}</td>
								<td style="text-align:center;">${gradeView.waybill_undelivered_punish_amt !0}</td>
								<td style="text-align:center;">${gradeView.waybill_overtime_cnt !0}</td>
								<td style="text-align:center;">${gradeView.waybill_overtime_ratio !0}%</td>
								<td style="text-align:center;">${gradeView.waybill_overtime_punish_cnt !0}</td>
								<td style="text-align:center;">${gradeView.waybill_overtime_punish_amt !0}</td>
								<td style="text-align:center;">${gradeView.waybill_complaint_cnt !0}</td>
								<td style="text-align:center;">${gradeView.waybill_complaint_amt !0}</td>
								<td style="text-align:center;">${gradeView.waybill_click_early_cnt !0}</td>
								<td style="text-align:center;">${gradeView.waybill_click_early_punish_amt !0}</td>
								<td style="text-align:center;">${gradeView.waybill_batter_cnt !0}</td>
								<td style="text-align:center;">${gradeView.waybill_batter_punish_amt !0}</td>
								<td style="text-align:center;"><#if gradeView.level == 1>及格<#elseif gradeView.level == 0>不及格</#if></td>
								<td style="text-align:center;">${gradeView.postage_amt !0}</td>
							</tr>
						</#list>
					</#if>
				</tbody>
			</table>
			<div style="float: left; padding: 10px; 15px"><span>共${totalNum!'0'}项</span></div>
		    <#import "../page/pager.ftl" as q>
		        <#if totalNum??>
		            <@q.pager pageNo=pageNo pageSize=pageSize recordCount=totalNum toURL="/grade/franchiseeGrade"/>
		        </#if>
		</div>
	</div>
</div>
<script>
	document.title = '加盟站评级列表';
	$('select').select2();
	var orgId = "${orgId !0}";
	var timeStr = "${timeStr !'0'}";
</script>