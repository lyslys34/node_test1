<#include "grade_common_config.ftl">
<#include "../widgets/sidebar.ftl">
<script>var pageType = "searchRule";</script>
<link href="/static/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" rel="stylesheet" />
<style>
.ui-datepicker-trigger{display:none}
</style>
<content tag="javascript">/grade/grade_search.js</content>

<div id="main-container">
	<div class="panel panel-default table-responsive" style="margin-top:10px;padding:10px 10px;">
		<ul class="nav nav-tabs">		    
		    <li role="presentation" class="active"><a href="/grade/franchiseeGrade">加盟站评级列表</a></li>
		    <li role="presentation"><a href="/grade/franchiseePlatformOrder">加盟站订单列表</a></li>
		     <li role="presentation"><a href="/grade/config">评级方案配置</a></li>
		    <li role="presentation"><a href="/grade/city">关联城市</a></li>
		</ul>
		<br>
		
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label class="control-label">加盟站：</label>
						<select class="form-control input-sm" id="orgId" name="orgId" aria-hidden="true">
                        <option value="0">全部</option>
                        </select>

					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label class="control-label">月份：</label>
						<div class="form-group form-inline">
							<input type="text" readonly="" class="form-control input-sm date-picker J-datepicker" id="timeStr" value="2015-10">
						</div>
					</div>
				</div>
				<div class="col-md-8">
                    <div class="form-group" style="padding-top:14px">
                        <button type="submit" class="btn btn-success" id="btnSearch">查询</button>
                        <button type="button" class="btn btn-danger" id="btnDownload">下载</button>
                    </div>
                </div>
               
			</div>
		
	</div>
	<div>
		<p>注意：参与加盟站评级的订单为加盟站管理组去除异常订单（恶劣天气等其他异常情况订单不计入考核）后的订单。</p>		
		<div class="panel panel-default table-responsive">
			<div style="overflow-y: hidden; position: relative; border: 0px; width: 100%;">
			<table class="table table-striped" style="width:2400px">
				<thead>
					
				</thead>
				<tbody id="list">
				</tbody>
			</table>
			</div>
			<div style="float: left; padding: 10px; 15px"><span id="countLable"></span></div>
	        <div class="panel-footer clearfix">
	            <ul id="pager" class="pagination pagination-xs m-top-none pull-right"><li><a data-page="1">上一页</a></li><li class="active"><a data-page="1">1</a></li><li><a data-page="2">下一页</a></li></ul>
	        </div>
		</div>
	</div>
</div>