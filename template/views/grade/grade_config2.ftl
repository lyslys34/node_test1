<#include "grade_common_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/grade/grade_config2.js</content>
<content tag="cssmain">/grade/grade_config2.css</content>


<div id="main-container">
	<div class="panel panel-default table-responsive" style="margin-top:10px;padding:10px 10px;">
		<ul class="nav nav-tabs">
		     <li role="presentation"><a href="/grade/franchiseeGrade">加盟站评级列表</a></li>
		    <li role="presentation"><a href="/grade/franchiseePlatformOrder">加盟站订单列表</a></li>
		     <li role="presentation" class="active"><a href="/grade/config">评级方案配置</a></li>
		    <li role="presentation"><a href="/grade/city">关联城市</a></li>
		</ul>
		<form>
		 <div class="panel-body">
            <div class="row" id="btnCreate">
                <div class="col-md-12">
                   <div class="btn-group">
                    	<button type="button" class="btn btn-primary">+ 新建评级方案</button>
                	</div>
               </div>
            </div>
            <div class ="editDiv">
            	<div class="row">
                   <b>方案名称：</b>
                   <input id="ruleName" class="input-sm"/>
                </div>
                <div class="row editRow">
                	<div class="editTitle">
	                	<h1>奖励档位</h1>
	                	<div class="btn-group">
	                    	<button type="button" id="btnAward" class="btn btn-info">+ 新建奖励档位</button>
	                	</div>
	                	<div>说明：第一档位为最高档位，奖励最高。</div>
                	</div>
                	<table class="table table-striped">
			            <thead>
			            <tr>
			                <th>档位</th>
			                <th>完成率</th>
			                <th>准时率</th>
			                <th>超时率</th>
			                <th>瑕疵率</th>
			                <th>奖励金额</th>
			                <th>操作</th>
			            </tr>
			            </thead>
			            <tbody id="rewardsRuleList">
			            </tbody>
			        </table>
                </div>
                <div class="row editRow punishRuleList">
                	<div class="editTitle">
                	<h1>惩罚档位</h1>
                	</div>
                	<div class="col-md-6">
                	<div class="col-md-12 statusDisable ruleList" id="finishRuleList">
                		<div class="editSubTitle">
	                		<h2>完成率</h2>
	                		<label><input type="checkbox" data-name="finishRate"/>启用该指标</label>
	                		<div class="btn-group">
	                			<button type="button" data-type="finishRuleList" class="btn btn-sm btn-info btnCreate">+ 新建</button>
	                			<button type="button" data-type="finishRuleList" class="btn btn-sm btn-success btnEdit">编辑</button>
	                		</div>
	                		<h3>容忍度：<i class="per"></i>%</h3>
                		</div>
                		<table class="table table-striped">
			            <thead>
			            <tr>
			                <th>档位</th>
			                <th>完成率</th>
			                <th>惩罚金额</th>
			                <th>操作</th>
			            </tr>
			            </thead>
			            <tbody></tbody>
			        </table>
                	</div>
					

					<div class="col-md-12 statusDisable ruleList" id="flawRateRuleList">
                		<div class="editSubTitle">
	                		<h2>瑕疵率</h2>
	                		<label><input type="checkbox" data-name="flawRate"/>启用该指标</label>
	                		<div class="btn-group">
	                			<button type="button" data-type="flawRateRuleList" class="btn btn-sm btn-info btnCreate">+ 新建</button>
	                			<button type="button" data-type="flawRateRuleList" class="btn btn-sm btn-success btnEdit">编辑</button>
	                		</div>
	                		<h3>容忍度：<i class="per"></i>%</h3>
                		</div>
                		<table class="table table-striped">
			            <thead>
			            <tr>
			                <th>档位</th>
			                <th>瑕疵率</th>
			                <th>惩罚金额</th>
			                <th>操作</th>
			            </tr>
			            </thead>
			            <tbody>
			            </tbody>
			        </table>
                	</div>
					</div>
					<div class="col-md-6">
					<div class="col-md-12 statusDisable ruleList" id="onTimeRuleList">
						<div class="editSubTitle">
	                		<h2>准时率</h2>
	                		<label><input type="checkbox" data-name="onTimeRate"/>启用该指标</label>
	                		<div class="btn-group">
	                			<button type="button" data-type="onTimeRuleList" class="btn btn-sm btn-info btnCreate">+ 新建</button>
	                			<button type="button" data-type="onTimeRuleList" class="btn btn-sm btn-success btnEdit">编辑</button>
	                		</div>
	                		<h3>容忍度：<i class="per"></i>%</h3>
                		</div>
                		<table class="table table-striped">
			            <thead>
			            <tr>
			                <th>档位</th>
			                <th>准时率</th>
			                <th>惩罚金额</th>
			                <th>操作</th>
			            </tr>
			            </thead>
			            <tbody>
			            </tbody>
			        </table>

					</div>
					<div class="col-md-12 statusDisable ruleList" id="timeoutRateList">
						<div class="editSubTitle">
	                		<h2>超时率</h2>
	                		<label><input type="checkbox" data-name="timeoutRate"/>启用该指标</label>
	                		<div class="btn-group">
	                			<button type="button" data-type="timeoutRateList" class="btn btn-sm btn-info btnCreate">+ 新建</button>
	                			<button type="button" data-type="timeoutRateList" class="btn btn-sm btn-success btnEdit">编辑</button>
	                		</div>
	                		<h3>容忍度：<i class="per"></i>%</h3>
                		</div>
                		<table class="table table-striped">
			            <thead>
			            <tr>
			                <th>档位</th>
			                <th>超时率</th>
			                <th>惩罚金额</th>
			                <th>操作</th>
			            </tr>
			            </thead>
			            <tbody>
				            
			            </tbody>
			        </table>

					</div>
				</div>
                </div>

                <div class="row editRow permanentRule">
                	<div class="editTitle">
                	<h1>固定惩罚指标</h1>
                	</div>
                	<table class="table table-striped">
			            <thead>
			            <tr>
			                <th>指标</th>
			                <th>生效</th>
			                <th>扣罚金额</th>
			                
			            </tr>
			            </thead>
			            <tbody>
				            <tr>
					            <td>客诉</td>
					            <td><input id="complaintStatus" type="checkbox"/></td>
					            <td>
					            	<label class="radio-inline"><input value="1" name="complaintRadio" type="radio"/></label>
		                            <span class="fa fa-minus">&nbsp;</span><input id="complaintMoney" min="0" type="number" class="input-sm min-input number" value="">元/单&nbsp;&nbsp;
		                        	<label class="radio-inline"><input value="0" name="complaintRadio" type="radio"/></label>
		                            <span class="fa fa-minus">&nbsp;</span><input id="complaintPercentage" min="0" type="number" class="input-sm min-input number" value="">%邮资/单
		                        </td>
				            </tr>
				            <tr>
					            <td>提前点完成</td>
					            <td><input id="finishedAheadStatus" type="checkbox"/></td>
					            <td>
					            	<label class="radio-inline"><input value="1" name="finishedAheadRadio" type="radio"/></label>
		                            <span class="fa fa-minus">&nbsp;</span><input id="finishedAheadMoney" min="0" type="number" class="input-sm min-input number" value="">元/单&nbsp;&nbsp;
		                        	<label class="radio-inline"><input value="0" name="finishedAheadRadio" type="radio"/></label>
		                            <span class="fa fa-minus">&nbsp;</span><input id="finishedAheadPercentage" min="0" type="number" class="input-sm min-input number" value="">%邮资/单
		                        </td>
				            </tr>
				            <tr>
					            <td>连击</td>
					            <td><input id="doubleHitStatus" type="checkbox"/></td>
					            <td>
					            	<label class="radio-inline"><input value="1" name="doubleHitRadio" type="radio"/></label>
		                            <span class="fa fa-minus">&nbsp;</span><input id="doubleHitMoney" min="0" type="number" class="input-sm min-input number" value="">元/单&nbsp;&nbsp;
		                        	<label class="radio-inline"><input value="0" name="doubleHitRadio" type="radio"/></label>
		                            <span class="fa fa-minus">&nbsp;</span><input id="doubleHitPercentage" min="0" type="number" class="input-sm min-input number" value="">%邮资/单
		                        </td>
				            </tr>

				            <tr>
					            <td>一二星评价</td>
					            <td><input id="poorRateStatus" type="checkbox"/></td>
					            <td>
					            	<label class="radio-inline"><input value="1" name="poorRateRadio" value="1" type="radio"/></label>
		                            <span class="fa fa-minus">&nbsp;</span><input min="0" id="poorRateMoney" type="number" class="input-sm min-input number" value="">元/单&nbsp;&nbsp;
		                        	<label class="radio-inline"><input value="0" name="poorRateRadio" value="0" type="radio"/></label>
		                            <span class="fa fa-minus">&nbsp;</span><input min="0" type="number" id="poorRatePercentage" class="input-sm min-input number" value="">%邮资/单
		                        </td>
				            </tr>
				            <tr>
					            <td colspan="2">连击时间范围</td>
					            <td><input id="doubleHitSeconds" min="0" type="number" class="input-sm min-input number" value="">秒
		                        </td>
				            </tr>
			            </tbody>
			        </table>
                </div>

                <div class="row editFooter">
			    	<button type="button" class="btn btn-primary" id="btnSaveAll">保存评级方案</button>
			    	<button type="button" class="btn btn-info" id="btnCancelEdit">取消</button>
			    </div>
                

            </div>
        </div>
    </div>

</form>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="tableList">
            <thead>
            <tr>
                <th>方案名称</th>
                <th>已应用城市</th>
                <th>创建时间</th>
                <th>上次修改时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody id="list">
	            <tr>
		            <td colspan="6">加载中...</td>
	            </tr>
            </tbody>
        </table>
        <div style="float: left; padding: 10px; 15px"><span id="countLable"></span></div>
        <div class="panel-footer clearfix">
            <ul id="pager" class="pagination pagination-xs m-top-none pull-right"><li><a data-page="1">上一页</a></li><li class="active"><a data-page="1">1</a></li><li><a data-page="2">下一页</a></li></ul>
        </div>

    </div>



</div>

<div class="modal fade" id="awardDialog">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
           <h4 class="modal-title"></h4></div>
          <div class="modal-body">
          		<div class="row editDialog">
						<div class="col-md-6"><label class="checkbox-inline"><input checked="checked" class="finishRateStatus" type="checkbox"/>完成率</label><input class="input-sm finishRate number" min="0" max="100" type="number"/><i>%</i></div>
						<div class="col-md-6"><label class="checkbox-inline"><input checked="checked" class="flawRateStatus" type="checkbox"/>瑕疵率</label><input class="input-sm flawRate number" type="number" min="0" max="100"/><i>%</i></div>
						<div class="col-md-6"><label class="checkbox-inline"><input checked="checked" class="onTimeRateStatus" type="checkbox"/>准时率</label><input class="input-sm onTimeRate number" type="number" min="0" max="100"/><i>%</i></div>
						<div class="col-md-6"><label class="checkbox-inline"><input checked="checked" class="timeoutRateStatus" type="checkbox"/>超时率</label><input class="input-sm timeoutRate number" type="number" min="0" max="100"/><i>%</i></div>
						<div class="col-md-12">奖励金额：
							<label class="radio-inline"><input checked="checked" value="1" name="rewardsMoneyRadio" type="radio"></label>
							<input type="number" class="input-sm rewardsMoney number" min="0" value="">元/单&nbsp;&nbsp;
					        <label class="radio-inline"><input value="0" name="rewardsMoneyRadio" type="radio"></label>
					        <input type="number" class="input-sm rewardsPercentage number" min="0" value="">%邮资/单</div>
				</div>
          </div>
           <div class="modal-footer">
           		<span class="notice" style="float:left"></span>
           		<button type="button" class="btn btn-success submit">确定</button>
           		<button type="button" class="btn btn-default cancel">取消</button>
           </div>
       </div>
   </div>
</div>


<div class="modal fade" id="punishDialog">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
           <h4 class="modal-title">惩罚档位设置</h4></div>
          <div class="modal-body">
          		<div class="row editDialog">
					
				</div>
          </div>
           <div class="modal-footer">
           		<span class="notice" style="float:left"></span>
           		<button type="button" class="btn btn-success submit">确定</button>
           		<button type="button" class="btn btn-default cancel">取消</button>
           </div>
       </div>
   </div>
</div>

<div class="modal" id="confirmDialog">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
           <h4 class="modal-title">确认操作</h4></div>
          <div class="modal-body">
          		<div class="row"><div class="col-md-12 msg"></div></div>
          </div>
           <div class="modal-footer">
           		<button type="button" class="btn btn-success submit">确定</button>
           		<button type="button" class="btn btn-default cancel">取消</button>
           </div>
       </div>
   </div>
</div>


<div class="modal" id="cityDialog">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
           <h4 class="modal-title">请填写城市ID</h4></div>
          <div class="modal-body">
          		<div class="row">
          			<textarea placeholder="请填写城市ID，每行一个" class="center-block" cols="60" rows="10"></textarea>
          		</div>
          </div>
           <div class="modal-footer">
           		<button type="button" class="btn btn-success submit">确定</button>
           		<button type="button" class="btn btn-default cancel">取消</button>
           </div>
       </div>
   </div>
</div>
 
 <div class="modal" id="alertDialog">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
           <h4 class="modal-title">提示</h4></div>
          <div class="modal-body">
               <div>
                     <div class="msg center-block"></div>
                </div>
          </div>
           <div class="modal-footer">
                <button type="button" class="btn btn-success cancel center-block">知道了</button>
           </div>
       </div>
   </div>
</div>

<script>
    document.title = '加盟商评级方案配置';
</script>