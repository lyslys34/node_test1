<#include "grade_common_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/grade/grade_search.js</content>
<script>
var pageType = "searchOrder";
var importAuth = ${isAuthed};
</script>
<link href="/static/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" rel="stylesheet" />
<style>
.ui-datepicker-trigger{display:none}
a{cursor:pointer}
</style>
<content tag="javascript">/grade/grade_search.js</content>

<div id="main-container">
	<div class="panel panel-default table-responsive" style="margin-top:10px;padding:10px 10px;">
		<ul class="nav nav-tabs">		    
		    <li role="presentation"><a href="/grade/franchiseeGrade">加盟站评级列表</a></li>
		    <li role="presentation" class="active"><a href="/grade/franchiseePlatformOrder">加盟站订单列表</a></li>
		     <li role="presentation"><a href="/grade/config">评级方案配置</a></li>
		    <li role="presentation"><a href="/grade/city">关联城市</a></li>
		</ul>
		<br>
		
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label class="control-label">加盟站：</label>
						<select class="form-control input-sm" id="orgId" name="orgId" aria-hidden="true">
                        <option value="0">全部</option>
                        </select>

					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label class="control-label">月份：</label>
						<div class="form-group form-inline">
							<input type="text" readonly="" class="form-control input-sm date-picker J-datepicker" id="timeStr" value="2015-10">
						</div>
					</div>
				</div>
				<div style="float:left">
					<div class="form-group" style="padding-top:14px">
                        <button type="submit" class="btn btn-success" id="btnSearch">查询</button>
                        <button type="button" class="btn btn-danger" id="btnDownload">下载</button>
                        
                    </div>
				</div>
				<div class="col-md-2" id="importDiv" style="display:none">
                    <div class="btn-group" style="padding-top:14px">
                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    导入 <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu">
						    <li><a data-type="1">异常订单</a></li>
						    <li><a data-type="3">客诉订单</a></li>
						    <li><a data-type="2">未送达点已送达订单</a></li>
						    <li role="separator" class="divider"></li>
						    <li><a data-type="4">加盟站黑名单</a></li>
						  </ul>
                    </div>
                </div>
               
			</div>
		
	</div>
	<div>
		<p>注意：参与加盟站评级的订单为加盟站管理组去除异常订单（恶劣天气等其他异常情况订单不计入考核）后的订单。</p>		
		<div class="panel panel-default table-responsive">
			<div style="overflow-y: hidden; position: relative; border: 0px; width: 100%;">
			<table class="table table-striped" style="width:2400px">
				<thead>
					
				</thead>
				<tbody id="list">
				</tbody>
			</table>
			</div>
			<div style="float: left; padding: 10px; 15px"><span id="countLable"></span></div>
	        <div class="panel-footer clearfix">
	            <ul id="pager" class="pagination pagination-xs m-top-none pull-right"><li><a data-page="1">上一页</a></li><li class="active"><a data-page="1">1</a></li><li><a data-page="2">下一页</a></li></ul>
	        </div>
		</div>
	</div>
</div>

<div id="upload-file-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group ">
							<form id="upload-file-form" enctype="multipart/form-data">
                                 <input type="file" name="file" id="upload-file-input" title="请选择要上传的EXCEL文件" />
                                 <input id="upload-file-type-input" type="hidden" name="fileType" value="1"/>
                            </form>
                            <br/>
                            <textarea id="failed-record-text" rows="5" cols="60" style="display:none"></textarea>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="upload-file-btn">上传</button>
                <button type="button" class="btn btn-default"  data-dismiss="modal" id="upload-file-cancel-btn">取消</button>
           </div>
        </div>
   </div>
</div>