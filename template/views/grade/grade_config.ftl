<#include "grade_common_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/grade/grade_config.js</content>
<style>
    .form-control {
        display: inline-block;
    }

    .min-input {
        width: 80px;
    }

    .tc {
        text-align: center;
    }

    .tl {
        text-align: left;
    }

    input[type="radio"], input[type="checkbox"] {
        /*opacity: 0;*/
        display: none;
    }
</style>
<div id="main-container">
	<div class="panel panel-default table-responsive" style="margin-top:10px;padding:10px 10px;">
		<ul class="nav nav-tabs">		    
		    <li role="presentation"><a href="/grade/franchiseeGrade">加盟站评级列表</a></li>
		    <li role="presentation"><a href="/grade/franchiseePlatformOrder">加盟站订单列表</a></li>
		    <li role="presentation" class="active"><a href="/grade/config">加盟站配置</a></li>
		</ul>
		<div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-bordered table-condensed table-hover table-striped">
                <tr>
                    <th width="20%" class="tc">指标</th>
                    <th width="10%" class="tc">是否生效</th>
                    <th width="20%" class="tc">扣罚金额</th>
                    <th width="50%" class="tc">公式说明</th>
                </tr>
            <#if configList?exists>
                <#list configList as config>
                    <tr data-key="${config.key ! 0}" class="js_item">

                        <td class="tc">${config.name ! ''}</td>
                        <td class="tc">
                            <label class="label-checkbox">
                            	<#if config.key!=6&&config.key!=7&&config.key!=8>
                                <input type="checkbox" class="chk-row js_status" <#if config.status==1>checked</#if>>
                                <span class="custom-checkbox"></span>
                                </#if>
                            </label>
                        </td>
                        <td>
                            <input type="text" class="form-control input-sm min-input js_value"
                                   value="${config.value ! 0}"/>
                        ${config.unit ! ''}
                        </td>
                        <td>${config.formula!''}</td>
                    </tr>
                </#list>

            <#else>
                <tr>
                    <td colspan="4">没有配置</td>
                </tr>
            </#if>
            </table>
        </div>
        <div class="panel-footer text-right">
            <button type="button" class="btn btn-success" id="js_submit">保存</button>
        </div>
    </div>
	</div>
</div>
<script>
    document.title = '加盟商评级配置';
</script>