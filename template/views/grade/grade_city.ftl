<#include "grade_common_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/grade/grade_city.js</content>
<style type="text/css">a{cursor: pointer;}</style>

<div id="main-container">
    <div class="panel panel-default table-responsive" style="margin-top:10px;padding:10px 10px;">
        <ul class="nav nav-tabs">
             <li role="presentation"><a href="/grade/franchiseeGrade">加盟站评级列表</a></li>
            <li role="presentation"><a href="/grade/franchiseePlatformOrder">加盟站订单列表</a></li>
             <li role="presentation"><a href="/grade/config">评级方案配置</a></li>
            <li role="presentation" class="active"><a href="/grade/city">关联城市</a></li>
        </ul>
        <div class="panel-body">
            
        
            <div class="row col-md-12" style="line-height:60px;">
                <span>方案：</span>
               
                <select class="input-sm" id="solutions">
                       <option value="-1">未关联方案</option>
                    </select>
                <button class="btn btn-info btn-sm" id="btnDown">下载</button>
            </div>
            <div class="row col-md-12">
                <span>城市ID：</span>

                <input class="input-sm" id="citySearch"/>
               
                <button class="btn btn-primary btn-sm" id="btnSearch">查询</button>
                
            </div>
        </div>

        <div class="panel col-md-8" style="margin-top:10px">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>城市ID</th>
                    <th>城市</th>
                    <th>方案</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="list">
                </tbody>
            </table>
            <div style="float: left; padding: 10px; 15px"><span id="countLable"></span></div>
            <div class="panel-footer clearfix">
                <ul id="pager" class="pagination pagination-xs m-top-none pull-right"><li><a data-page="1">上一页</a></li><li class="active"><a data-page="1">1</a></li><li><a data-page="2">下一页</a></li></ul>
            </div>
        </div>

        <div class="row" ></div>

    </div>



</div>


<div class="modal" id="confirmDialog">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
           <h4 class="modal-title">确认操作</h4></div>
          <div class="modal-body">
                <div class="row"><div class="col-md-12 msg"></div></div>
          </div>
           <div class="modal-footer">
                <button type="button" class="btn btn-success submit">确定</button>
                <button type="button" class="btn btn-default cancel">取消</button>
           </div>
       </div>
   </div>
</div>

<div class="modal" id="ruleDialog">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
           <h4 class="modal-title">编辑方案</h4></div>
          <div class="modal-body">
               <div class="row" style="padding-left:20px">
                     <select class="input-sm">
                        <option value="0">请选择方案</option>
                    </select>
                </div>
          </div>
           <div class="modal-footer">
                <button type="button" class="btn btn-success submit">确定</button>
                <button type="button" class="btn btn-default cancel">取消</button>
           </div>
       </div>
   </div>
</div>

<div class="modal" id="alertDialog">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
           <h4 class="modal-title">提示</h4></div>
          <div class="modal-body">
               <div>
                     <div class="msg center-block"></div>
                </div>
          </div>
           <div class="modal-footer">
                <button type="button" class="btn btn-success cancel center-block">知道了</button>
           </div>
       </div>
   </div>
</div>

<script>
    document.title = '评级方案关联城市';

    

</script>