<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="cssmain">/white_poi/white_poi.css</content>
<content tag="javascript">/white_poi/white_poi.js</content>
<script src="/static/js/lib/jquery.tmpl.js"></script>
<div id="main-container">
    <table class='table table-striped mt20' id='whiteListTable'>
        <thead>
            <th>类别</th>
            <th>白名单编号</th>
            <th>白名单列表</th>
            <th>备注</th>
            <th>操作记录</th>
        </thead>
        <tbody>
    
        </tbody>
    </table>
    <div class='t-right'>
        <input type="button" value="添加" class="btn btn-success add" id="add">
    </div>
</div>

<div class='ajax-loading' id='ajaxLoading'>
    <div class='ajax-loading-msg'>
        <img src='/static/imgs/loading.gif'>
        数据处理中...
    </div>
</div>

<script type='text/template' id='tbodyTmpl'>
    {{each(i,r) data}}
         <tr data-id={{= r.type}}>
            <td>
                <span data-type="type" data-id="{{= r.type}}">{{= r.description}}</span>
                <a data-toggle="tooltip" title="修改" data-placement="top" href="javascript:;" name="detail">
                    <i class="fa fa-edit fa-lg opration-icon"></i>
                </a>
            </td>
            <td>{{= r.type}}</td>
            <td>
                <div class='whitelist-wrap'>
                    {{= r.whiteIds.join(',')}}
                </div>
                <div class='op-wrap'>
                    <input type="button" value="+" class="add-num-btn">
                    <input type="button" value="－" class="del-num-btn">
                </div>
            </td>
            <td>
                <span data-type="description" data-id="{{= r.type}}">{{= r.note}}</span>
                <a data-toggle="tooltip" title="修改" data-placement="top" href="javascript:;" name="detail">
                    <i class="fa fa-edit fa-lg opration-icon"></i>
                </a>
            </td>
            <td>
                <a href='white_poi_config?id={{= r.type}}' class='t-link' target='_blank'>查看</a>
            </td>
        </tr>
    {{/each}}
</script>
<script type='text/template' id='whiteAddTmpl'>
    <tr>
        <td>
            <input type='text' placeholder='给白名单取个名字吧'/>
        </td>
        <td>
            <input type='text' placeholder='编号切勿与已有重复'/>
        </td>
        <td>
            <textarea placeholder='请用英文逗号隔开'></textarea>
        </td>
        <td>
            <input type='text' placeholder='适用范围&注意事项'/>
        </td>
        <td>
            <input type="button" value="确定" class="btn-success add-item-comfirm btn">
            <input type="button" value="取消" class="btn-danger add-item-cancel btn">
        </td>
    </tr>
</script>
<script type='text/template' id='addTmpl'>
    <div class='add-wrap'>
        <input type='text' class='add-input form-control' placeholder='请用英文逗号隔开'>
        <input type="button" value="确定" class="btn-success add-comfirm btn">
        <input type="button" value="取消" class="btn-danger add-cancel btn">
    </div>
</script>

<script type='text/template' id='delTmpl'>
    <div class='del-wrap'>
        <input type='text' class='del-input form-control' placeholder='请用英文逗号隔开'>
        <input type="button" value="确定" class="btn-success del-comfirm btn">
        <input type="button" value="取消" class="btn-danger del-cancel btn">
    </div>
</script>


