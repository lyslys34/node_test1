<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="cssmain">/white_poi/white_poi.css</content>
<content tag="javascript">/white_poi/white_poi_config.js</content>
<script src="/static/js/lib/jquery.tmpl.js"></script>
<div id="main-container">
    <table class='table table-striped mt20' id='config_table'>
        <thead>
            <th>操作类型</th>
            <th style='width:800px'>变更内容</th>
            <th>操作人</th>
            <th>操作时间</th>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<script type='text/template' id='tbodyTmpl'>
    {{if data.length}}
        {{each(i,r) data}}
            <tr>
                {{if r.opType==1}}
                <td>新增白名单类别</td>
                {{else r.opType==2}}
                <td>增加白名单内容</td>
                {{else}}
                <td>删除白名单内容</td>
                {{/if}}
                {{if r.opType==1}}
                <td></td>
                {{else}}
                <td>{{= r.content}}</td>
                {{/if}}
                <td>{{= r.opName}}</td>
                <td>{{= r.opTime}}</td>
            </tr>
        {{/each}}
    {{else}}
    <tr>
        <td colspan='5'>暂无数据</td>
    </tr>
    {{/if}}
</script>