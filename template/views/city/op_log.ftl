<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">

<div id="main-container">
    <br/>
    <br/>
    <div class="panel panel-default table-responsive">
        <div class="">
            <table class="table table-striped" id="responsiveTable">
                <thead>
                <tr>
                    <th class="col-md-1">操作人</th>
                    <th class="col-md-9">变更内容</th>
                    <th class="col-md-2">操作时间</th>
                </tr>
                </thead>
                <tbody id="city-info">
                    <#list opLogList as item>
                        <tr>
                            <td>${item.userName}</td>
                            <td>${item.content}</td>
                            <td>${item.ctime}</td>
                        </tr>
                    </#list>
                <#--循环-->
                </tbody>
            </table>
        </div>
                <span>总共有${recordCount}条记录<>
                <#import "../page/pager.ftl" as q>
                <#if recordCount??>
                    <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/settleWaybillAudit/listJiaoma"/>
                </#if>
    </div>

</div>
<script>
    document.title = '城市管理';
</script>
