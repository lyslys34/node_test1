<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="cssmain">/city/city_list.css</content>
<content tag="javascript">/city/city_list.js</content>

<div id="main-container">
    <div class="panel panel-default table-responsive">
        <div class="">
            <div id="city-search" class="row">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label class="control-label">城市名称</label>
                                    <input type="text" id="city-name"></input>
                                </div>
                                <div class="col-md-2">
                                    <label class="control-label">城市ID</label>
                                    <input type="text" id="city-id"></input>
                                </div>
                                <div class="col-md-2">
                                    <label class="control-label">城市级别</label>
                                    <br>
                                    <select id="city-level" class="input-sm" style="max-height: 22px; min-width: 100px;">
                                        <option value="0" selected="selected">全部</option>
                                        <option value="1" >1</option>
                                        <option value="2" >2</option>
                                        <option value="3" >3</option>
                                        <option value="4" >4</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <button id="query-city" class="btn btn-success">查询</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <label>批量设置:</label>
                <button id="batch-set-postage"  class="btn btn-xs btn-success">邮资策略</button>
                <button id="batch-set-night-postage"  class="btn btn-xs btn-success">夜间配送费</button>
                <button id="batch-set-winter-postage"  class="btn btn-xs btn-success">冬季补贴</button>
                <span id="chose-number-text"></span>
            </div>
            <br/>
            <table class="table table-striped" id="responsiveTable">
                <thead>
                <tr>
                    <th><input type="checkbox" id="choose-all" style="opacity: 1;position:relative"></th>
                    <th>城市ID</th>
                    <th>城市名称</th>
                    <th>城市级别</th>
                    <th>财务拼音</th>
                    <th>所属片区</th>
                    <th>邮资策略</th>
                    <th>配送费</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="city-info">
                <#--循环-->
                </tbody>
            </table>
            <div>
                <span id="record-count"></span>
                <ul class="pagination pagination-xs pull-right" id="pager" style="margin-bottom: 10px;">
                </ul>
            </div>

            <div id="batch-set-postage-modal" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">修改</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">城市级别</span>
                                <select class="form-control" name="level" id="city-level-select">
                                    <option value="1" selected="selected">1级</option>
                                    <option value="2" selected="selected">2级</option>
                                    <option value="3" selected="selected">3级</option>
                                    <option value="4" selected="selected">4级</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                       <div class="col-md-8">
                        <div class="input-group">
                            <span class="input-group-addon">定价方案</span>
                            <div class="radio-list" id="postage-strategy-list" style="border: 1px solid #ccc; padding-left: 10px;">
                            </div>
                        </div>
                       </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="batch-set-postage-btn">确定</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="batch-set-postage-cancel-btn">取消</button>
                  </div>

                </div>

              </div>
            </div>
            <div id="batch-set-night-postage-modal" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content" style="width:700px;">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">修改</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                       <div class="col-md-4">
                        <div class="input-group ">
                            <span class="input-group-addon">夜间配送费</span>
                            <a class="add-time pull-right" id="add-more-night-postage" style="padding-top:8px;">
                                <i class="fa fa-plus fa-lg opration-icon"></i>
                            </a>
                        </div>
                       </div>
                        <div id="night-postage" class="col-md-12">
                        </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="batch-set-night-postage-btn">确定</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="batch-set-night-postage-cancel-btn">取消</button>
                  </div>

                </div>
              </div>
            </div>
            <div id="batch-set-winter-postage-modal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">修改</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group ">
                                        <span class="input-group-addon">冬季配送</span>
                                    </div>
                                </div>
                                <div id="winter-postage" class="col-md-12">
                                    <div class="row night" style="margin-top: 15px;margin-right:5px;width:600px;">
                                        <div class="col-md-2" style="text-align: center; margin-top: 5px;width:90px;">冬季配送费</div>
                                        <div class="col-md-2">
                                            <input type="text"  class="form-control input-sm winter-postage"  style="width:75px;">
                                        </div>
                                        <div class="col-md-2" style="text-align: center; margin-top: 5px;width:90px;">冬季补贴</div>
                                        <div class="col-md-2">
                                            <input type="text"  class="form-control input-sm winter-postageSubsidy"  style="width:75px;">
                                        </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" id="batch-set-winter-postage-btn">确定</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="batch-set-winter-postage-cancel-btn">取消</button>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.title = '城市管理';
</script>
