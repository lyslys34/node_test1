<#include "../base/home_config.ftl">
      <#include "../widgets/sidebar.ftl">
      <content tag="cssmain">/city/city_update.css</content>
      <script type="text/javascript" src="${staticPath}/js/page/city/city_update.js"></script>
      <div id="main-container" <@output_props obj = cityInfo />>
          <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div id="logmsg" style="display: none;"></div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon" style="width:100px;">城市名称</span>
                            <input class="form-control" id="name" name="name" value="${cityInfo.name!''}"  disabled style="width:350px;"></input>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon" style="width:100px;">城市ID</span>
                            <input class="form-control" id="cityId" name="cityId" value="${cityInfo.cityId!''}"  disabled style="width:350px;"></input>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6" style="display:none">
                        <div class="input-group">
                            <span class="input-group-addon" style="width:100px;">邮资</span>
                            <input class="form-control" id="postage" name="postage" value="${cityInfo.postage!''}" disabled style="width:350px;"></input>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon" style="width:100px;">城市级别</span>
                            <select class="form-control" name="level" id="level"  style="width:350px;">
                            <#list 1..4 as t>
                              ${t}
                              <#global select="">
                              <#if cityInfo.level == t>
                                <#global select='selected="selected"'>
                              </#if>
                              <option value="${t}" ${select}>${t}</option>
                            </#list>
                            </select>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon" style="width:100px;">片区ID</span>
                            <select class="form-control" name="regionId" id="regionId" style="width:350px;">
                              <#list regionList as region>
                              <#global select="">
                              <#if cityInfo.regionId == region.id>
                                <#global select='selected="selected"'>
                              </#if>
                                <option value="${region.id!'0'}" ${select}>${region.name!''}</option>
                              </#list>
                            </select>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6" style="display:none">
                        <div class="input-group">
                             <span class="input-group-addon" style="width:100px;">财务拼音</span>
                             <input class="form-control" id="caiwuPinyin" name="caiwuPinyin" value="${cityInfo.caiwuPinyin!''}"  disabled style="width:350px;"></input>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon" style="width:100px;">定价方案</span>
                          <div class="radio-list" style="border: 1px solid #ccc; padding-left: 10px;width:350px;">
                            <#list postageStra?keys as pkey> 
                                <#assign plist = postageStra[pkey]>
                                <#global display = "style='display: none;'">
                                <#global checked = "">
                                <#global cityLevel = cityInfo.level>
                                <#if btnText == '添加'>
                                  <#global cityLevel = 3>
                                </#if>
                                  <#if cityLevel == pkey?number>
                                    <#global display = ''>
                                  </#if>
                                  <#list plist as p>
                                  <#global checked = "">
                                  <#if cityInfo.postageStrategyId == p.code?number>
                                    <#global checked = "checked='checked'">
                                  </#if>
                                    <div class="radio radio-${pkey!''}" ${display!''}>
                                      <label>
                                        <input type="radio" ${checked!''} style="opacity: 1" name="postageStrategyRadio" id="postageStrategyId" value="${p.code!''}">
                                        ${p.strategy!''}
                                      </label>
                                    </div>
                                  </#list>
                             </#list>
                          </div>
                        </div>
                    </div>
                </div>
                <br/>
              <div class="col-md-8">
                  <div class="row">
                      <div class="input-group">
                          <span class="input-group-addon" style="width:100px;">冬季补贴</span>
                          <span></span>
                      </div>
                  </div>
                  <div id="new-postage-list" style="margin-bottom: 10px;" class="row">

                  </div>
              </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="input-group">
                            <span class="input-group-addon" style="width:100px;">夜间配送费</span>
                            <a class="add-time" id="add-more-night-postage" style="padding-top:20px;width:350px;">
                                <i class="fa fa-plus fa-lg opration-icon" style="padding-top:8px;padding-left:15px;"></i>
                            </a>
                        </div>
                    </div>
                    <div id="night-postage-list" class="row">
                    </div>
                </div>
                <br/>
                <br/>
                <div class="col-md-8">
                    <div class="row">
                        <div class="input-group">
                            <br/>
                            <button class="btn btn-md btn-success" id="city_update" style="width:100px;">${btnText!'修改'}</button>
                        </div>
                    </div>
                </div>
          </div>
          <div class="row" style="display:none">
            <input id="night-postage-hide" value="${cityInfo.nightPostage!''}"></input>
            <input id="new-postage-hide" value="${cityInfo.newPostage!''}"></input>
          </div>
      </div>
      <script>
          document.title = '城市信息修改';
      </script>
