<#include "exp_org_config.ftl">
<#include "../widgets/sidebar.ftl">

<div id="main-container">
  <div class="panel panel-default">
    <form method="post" class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/partner/new">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">组织名称</label>
              <input type="text" class="form-control input-sm parsley-validated" id="orgName" name="orgName" data-required="true" >
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">组织类型</label>
                <select class="form-control input-sm inline" id="orgType" name="orgType">
                    <option value="1">自建</option>
                    <option value="2" selected>加盟</option>
                    <option value="3">驻店三方</option>
                    <option value="4">众包</option>
                </select>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">组织负责人</label>
                    <input type="text" class="form-control input-sm parsley-validated" id="orgAdmin" name="orgAdmin" data-required="true" >
                </div><!-- /form-group -->
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">登录手机号</label>
                    <input type="text" class="form-control input-sm parsley-validated" id="adminNo" name="adminNo" data-required="true" >
                </div>
            </div>
        </div>
          <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">组织城市</label>
                      <#--input type="text" class="form-control input-sm parsley-validated js_city_name" name="cityName" id="cityName" data-minlength="8" value=""/-->
                  	  <#--input type="hidden" name="orgCity" id="orgCity" class="js_city_id" value="${orgCity!""}"/-->
                  	  <select class="form-control input-sm" id="orgCity" name="orgCity">
                  	  	<option value="0" selected="selected">全部</option>
                  	  	<#list cityList as city >
                  	  	    <option value="${city.city_id !''}" >${city.name !''}</option>
                  	  	</#list>
                  	  </select>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">组织简介</label>
          			  <textarea rows="6" class="form-control input-sm" id="orgDes" name="orgDes"></textarea>
                  </div>
              </div>
          </div>
      </div>
      <div class="panel-footer text-right">
        <#--<button type="button" class="btn btn-success" onclick="javasctipt:history.go(-1);">返回</button>&nbsp;&nbsp;-->
        <button type="submit" class="btn btn-success">保存</button>
      </div>
    </form>
  </div>
</div>
<script>
	document.title = '新建合作伙伴';
	$('select').select2();
</script>