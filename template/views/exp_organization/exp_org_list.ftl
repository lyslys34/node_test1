<#include "exp_org_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/audit/org_list.js</content>

<link rel="stylesheet" type="text/css" href="${staticPath}/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />

<style type="text/css">
.ui-autocomplete{
    z-index: 2000;
}
#seprator{

    position: absolute;
    top: 0;
    width: 1px;
    background-color: #7B7474;
    height: 100%;
    right: 33%;
    display: none;
}
@media (min-width: 992px){
    #seprator{
        display: block;
}
}

</style>

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post" action="/partner/list">
            <div class="panel-body">
            <div id="seprator"></div>
                <div class="row">
                	<div class="col-md-4">
            			<div class="form-group">
                            <label class="control-label">组织名称</label>
                            <input type="text" class="form-control input-sm parsley-validated js_org_name" name="orgName" id="orgName" data-required="true" data-minlength="8" value="${searchOrgName!""}"/>
                            <input type="hidden" class="js_org_id" name="orgId" id="orgId" value="${orgId!""}"/>
	                    </div>
	          		</div>
                    <div class="col-md-4">
        				<div class="form-group">
                            <label class="control-label">组织类型</label>
                            <select class="form-control input-sm" id="orgType" name="orgType">
                                <option value="0"<#if searchOrgType == "0"> selected="selected" </#if>>全部</option>
                                <option value="1"<#if searchOrgType == "1"> selected="selected" </#if>>自建</option>
                                <option value="2"<#if searchOrgType == "2"> selected="selected" </#if>>加盟</option>
                                <option value="3"<#if searchOrgType == "3"> selected="selected" </#if>>驻店三方</option>
                                <option value="4"<#if searchOrgType == "4"> selected="selected" </#if>>众包</option>
                            </select>
                        </div>
	          		</div>
                    <div class="col-md-4" style="padding-top:18px;padding-bottom:15px;">
                        <button type="submit" class="btn btn-success btn-block">查询</button>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">启用状态</label>
                            <select class="form-control input-sm" id="status" name="status">
                                <option value="-1"<#if searchStatus == -1> selected="selected" </#if>>全部</option>
                                <option value="1"<#if searchStatus == 1> selected="selected" </#if>>启用</option>
                                <option value="0"<#if searchStatus == 0> selected="selected" </#if>>禁用</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">所在城市</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="orgCity" id="orgCity" data-minlength="4" disabled/>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding-top:18px;padding-bottom:15px;">
                        <a href="/partner/create" role="button" class="btn btn-danger btn-block">新建</a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>组织名称</th>
                <th>组织类型</th>
                <th>所在城市</th>
                <th>启用状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
                <#list result.data.data as org>
                <tr>
                    <td><a href="/org/list?parent=${org.orgId}">${org.orgName!''}</a></td>
                    <td>
                        <#if 1 == org.orgType>自建
                        <#elseif 2 == org.orgType>加盟
                        <#elseif 3 == org.orgType>驻店三方
                        <#elseif 4 == org.orgType>众包
                        <#else>
                        </#if>
                    </td>
                    <td></td>
                    <td>
                        <#if org.orgChecked == 0>
                            <span class="label label-danger">禁用</span>
                        <#elseif org.orgChecked == 1>
                            <span class="label label-success">启用</span>
                        </#if>
                    </td>
					<td>
		                <#--<li class="dropdown hidden-xs">-->
	                    <#--<a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
	                    <#--请选择-->
	                    <#--</a>-->
	                    <#--<ul class="dropdown-menu dropdown-org">-->
	                    <#--<li>-->
	                    <#--<a href="/org/merchantlist?orgid=${org.orgId}">服务商列表</a>-->
	                    <#--<a style="color:#adb8c0">编辑</a>-->
	                    <#--<a href="#">关联人员</a>-->
	                    <#--&lt;#&ndash;<a href="#">添加服务商</a>&ndash;&gt;-->
	                    <#--<a href="#">操作日志</a>-->
	                    <#--</li>-->
	                    <#--</ul>-->
	                    <#--</li>-->
                        <div class="btn-group hover-dropdown">
                            <button class="btn btn-xs btn-info dropdown-toggle" data-toggle="dropdown">操作<span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <#--<li><a href="#">详情</a></li>-->
                                <#--<li><a href="#">重置密码</a></li>-->
                                <#--<li><a href="#">删除</a></li>-->
                            </ul>
                        </div>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/list"/>
    </#if>


    <#--<div class="panel-footer clearfix">-->
    <#--<ul class="pagination pagination-xs m-top-none pull-right">-->
    <#--<li class="disabled"><a href="#">Previous</a></li>-->
    <#--<li class="active"><a href="#">1</a></li>-->
    <#--<li><a href="#">2</a></li>-->
    <#--<li><a href="#">3</a></li>-->
    <#--<li><a href="#">4</a></li>-->
    <#--<li><a href="#">5</a></li>-->
    <#--<li><a href="#">Next</a></li>-->
    <#--</ul>-->
    <#--</div>-->
    </div>
</div>
<script>document.title = '合作伙伴管理';</script>
