<#include "headfranchisee_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/headfranchisee/franchisee_list.js</content>

<style>
    .cgreen { color: green; }
    .cred { color: red; }
    .fsize17 { font-size: 17px;}
    .hover_underline:hover{ text-decoration: underline; }
</style>

<div id="main-container">
    <@headFranchiseeNavi org 2></@headFranchiseeNavi>

    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post"
              action="/headfranchisee/franchiseelist">
            <div class="panel-body">
                <div class="row">
                    <input type="hidden" id="parentId" name="parentId" value="${org.orgId!''}">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">加盟商名称：</label>
                            <input type="text" class="form-control input-sm check_data" name="orgName" placeholder="请输入汉字搜索"
                                   value="${RequestParameters.orgName !''}"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">加盟商ID：</label>
                            <input type="text" class="form-control input-sm check_num" name="orgId" placeholder="请准确输入加盟商ID"
                                   value="${RequestParameters.orgId !''}"/>
                        </div>
                    </div>
                    <div class="panel-footer text-left">
                        <button type="submit" class="btn btn-success">查询</button>
                        <button id="addFranchiseeBtn" type="button" class="btn btn-danger">关联子加盟商</button>
                    </div>
                    
                </div>
            </div>
            
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th style="width: 20%;">加盟商名称</th>
                <th style="width: 10%;">加盟商ID</th>
                <th style="width: 20%;">渠道经理</th>
                <th style="width: 10%;">合作站点数</th>
                <th style="width: 10%;">在职人员数</th>
                <th style="width: 10%;">操作</th>
            </tr>
            </thead>
            <tbody>
            <#if result?? && result.data??>
                <#list result.data as o>
                <tr>
                    <td>
                        <p><a href="/org/join/orgList?orgId=${o.orgId !'0'}" class="hover_underline">
                        <span style="font-weight: bold;">${o.orgName !''}</span></p>
                    </td>
                    <td>
                        <p>${o.orgId ! '0'}</p>
                    </td>
                    <td>
                        <#if o.parentUserList?exists >
                            <#list o.parentUserList as u>
                                <p>${u.name!''}</p>
                            </#list>
                        </#if>
                    </td>
                    <td>
                        ${o.childOrgNum ! '0'}
                    </td>
                    <td>
                        ${o.userNum !'0'}
                    </td>
                    <td>
                        <a data-toggle="tooltip" title="解绑子加盟商" data-placement="top" class="unbindfranchisee_link" rel="${o.orgId!''}"> <i class="fa fa-link fa-lg opration-icon"></i></a>
                    </td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>
        <div style="float: left; padding: 10px; 15px"><span>共${(result.totalCount)!'0'}项</span></div>
    <#import "../page/pager.ftl" as q>
    <#if result??>
        <@q.pager pageNo=result.pageNo pageSize=result.pageSize recordCount=result.totalCount toURL="/headfranchisee/franchiseelist"/>
    </#if>

    </div>

    <div class="modal fade" id="showSuccessMsg">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="resMsg">
                    
                </div>
                <div class="modal-footer">
                    <input type="submit" id="successSubmit" value="确定" class="btn btn-success " />
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addFranchiseeModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="text-center">关联子加盟商到${org.orgName!''}</h4>
                    <div id="alert_add_franchisee_error"></div>
                    <div class="form-group">
                        <textarea id="franchisee_ids" style="height:200px" class="form-control " placeholder="可批量关联子加盟商ID到此总账号下，每个ID一行" ></textarea>
                    </div><br>
                     <div class="modal-footer">
                        <input type="submit" id="addFranchiseeSubmit" value="确定" class="btn btn-success " />
                        <button type="button" id="cancelAddFranchiseeBtn" class="btn btn-danger " >取消</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="unBindTipsModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" id="unbindCloseButton" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 style="border-bottom: 1px solid #e5e5e5; padding-bottom: 5px;">
                        <i class = "fa fa-info-circle fa-lg opration-icon"></i>&nbsp;提示
                    </h4>
                    <p id="unbind_massage" style = "margin: 20px auto;width: 68%;font: initial;">
                        确定解绑子加盟商？
                    </p>
                    <input type="hidden" id="unBindHeadFranchiseeId" value="0"/>
                    <input type="hidden" id="unBindFranchiseeId" value="0"/>
                    <div class="modal-footer" style="border-top: none">
                        <input type="submit" id="unBindResubmit" value="确认" class="btn btn-danger btn-sm" />
                        <button class="btn btn-success btn-sm" id="cancelunBind">取消</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="showUnbindRes">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="unBindResMsg">
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="${staticPath}/js/page/organization/join_list.js"></script>

<script>
    document.title = '总加盟商管理';
</script>
