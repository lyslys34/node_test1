<#include "headfranchisee_config.ftl">
<#include "../widgets/sidebar.ftl">

<style>
    .cgreen { color: green; }
    .cred { color: red; }
    .fsize17 { font-size: 17px;}
    .hover_underline:hover{ text-decoration: underline; }
</style>

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post"
              action="/headfranchisee/list">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">城市</label>
                            <select class="form-control input-sm" id="cityId" name="cityId">
                                <option value="0">全部</option>
                                <#if cityList?exists>
                                    <#list cityList as c>
                                        <option value="${c.city_id !'0'}"
                                                <#if RequestParameters.cityId?exists && RequestParameters.cityId == c.city_id?string>selected="selected" </#if> >${c.name !''}</option>
                                    </#list>
                                </#if>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">总账号名称</label>
                            <input type="text" class="form-control input-sm" name="orgName" placeholder="请输入汉字搜索"
                                   value="${RequestParameters.orgName !''}"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">总账号ID</label>
                            <input type="text" class="form-control input-sm check_num" name="orgId" placeholder="请准确输入加盟商总账号ID"
                                   value="${RequestParameters.orgId !''}"/>
                        </div>
                    </div>

                    <div class="panel-footer text-left">
                        <button type="submit" class="btn btn-info">搜索</button>
                        <a type="submit" class="btn btn-danger" href="/headfranchisee/goCreate">新建加盟商总账号</a>
                    </div>
                </div>
            </div>
            
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th style="width: 20%;">加盟商信息</th>
                <th style="width: 10%;">证照信息</th>
                <th style="width: 20%;">操作</th>
            </tr>
            </thead>
            <tbody>
            <#if result?? && result.data??>
                <#list result.data as o>
                <tr>
                    <td>
                        <p><a href="/headfranchisee/goUpdate?orgId=${o.orgId !'0'}" class="hover_underline">
                            <span style="font-weight: bold;">${o.orgName !''}</span>&nbsp;&nbsp;&nbsp;
                            (id:${o.orgId ! '0'}) <br/>
                        </a></p>

                        <p>城市：${o.cityName !''} </p>
                        <p>合作子加盟商数量：${o.childJmPartnerNum ! '0'}</p>
                        <p>合作站点数量：${o.childOrgNum ! '0'}</p>
                        <p>在职员工总数：${o.userNum !'0'}</p>
                    </td>
                    <td>
                        <p><#if o.contractPicUrl?exists && o.contractPicUrl != ''><i class="fa fa-check-circle fsize17 cgreen"></i><#else><i class="fa fa-times-circle fsize17 cred"></i></#if>合同照片</p>
                        <p><#if o.businessLicensePicUrl?exists && o.businessLicensePicUrl != ''><i class="fa fa-check-circle fsize17 cgreen"></i><#else><i class="fa fa-times-circle fsize17 cred"></i></#if>营业执照照片</p>
                        <p><#if o.foodLicenseNumber?exists && o.foodLicenseNumber != ''><i class="fa fa-check-circle fsize17 cgreen"></i><#else><i class="fa fa-times-circle fsize17 cred"></i></#if>食品流通许可证 </p>
                    </td>
                    <td>                   
                        <a data-toggle="tooltip" title="修改信息" data-placement="top" href="/headfranchisee/goUpdate?orgId=${o.orgId !'0'}" target="_blank">
                            <i class="fa fa-edit fa-lg opration-icon"></i>&nbsp;&nbsp;
                        </a>
                        <a data-toggle="tooltip" title="子加盟商列表" data-placement="top" href="/headfranchisee/franchiseelist?parentId=${o.orgId!'0'}&f=join_org" target="_blank">
                            <i class="fa fa-list-ul fa-lg opration-icon"></i>&nbsp;&nbsp;
                        </a>
                        <a data-toggle="tooltip" title="人员列表" data-placement="top" href="/rider/listByOrgId?orgId=${o.orgId!'0'}&f=headfranchisee" target="_blank">
                            <i class="fa fa-list-ol fa-lg opration-icon"></i>&nbsp;&nbsp;
                        </a>
                        <a data-toggle="tooltip" title="操作记录" data-placement="top" href="/log/staff?orgId=${o.orgId!'0'}&f=headfranchisee" target="_blank">
                            <i class="fa fa-history fa-lg opration-icon"></i>&nbsp;&nbsp;
                        </a>
                    </td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>
        <div style="float: left; padding: 10px; 15px"><span>共${(result.totalCount)!'0'}项</span></div>
    <#import "../page/pager.ftl" as q>
    <#if result??>
        <@q.pager pageNo=result.pageNo pageSize=result.pageSize recordCount=result.totalCount toURL="/headfranchisee/list"/>
    </#if>

    </div>
</div>
<script type="text/javascript" src="${staticPath}/js/page/headfranchisee/headfranchisee_create.js"></script>

<script>
    document.title = '总加盟商列表';
    $("#cityId").select2();
    $(".check_num").keyup(function(){
        var value = $(this).val();
        $(this).val(value.replace(/[^\d]/g,''));
    });
</script>
