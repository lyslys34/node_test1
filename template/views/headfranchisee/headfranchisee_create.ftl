<#include "headfranchisee_config.ftl">
<#include "../widgets/sidebar.ftl">

<style>
    .cred { color: #b22222; }
    .lh30 { line-height: 30px; }
    input[type="radio"] {
        opacity: 0
    }
</style>
<div id="main-container">
    <#--<div class="cred" style="font-size:17px; font-weight: bold; margin:5px 0px;">新建加盟商后将由总部审核才能生效</div>-->
<@createHeadFranchiseeStep 1 />
    <form method="post" class="form-horizontal" id="formValidate1" data-validate="parsley" novalidate="" >
        <div class="panel panel-default">
            <div class="panel panel-heading">
                基本信息
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>总账号名称</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm" id="orgName" name="orgName" data-required="true" maxlength="25" placeholder="请输入总账号名称">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>所在城市</label>

                <div class="col-sm-6">
                    <select class="form-control input-sm" id="orgCity" name="orgCity">
                    <#if cityList?exists>
                        <#list cityList as city >
                            <option value="${city.city_id !''}">${city.name !''}</option>
                        </#list>
                    </#if>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>模式</label>

                <div class="col-sm-6">
                    <select class="form-control input-sm" id="isProxy" name="isProxy">
                        <option value="0">加盟</option>
                        <option value="1">代理</option>

                    </select>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>紧急联系人</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm parsley-validated" id="emergencyContact"
                           name="emergencyContact" data-required="true" maxlength="20" placeholder="请输入负责人姓名">
                    <span class="help-block cred">(仅供紧急联系使用，此账号无法登录烽火台)</span>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>紧急联系人电话</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm parsley-validated" id="emergencyPhone"
                           name="emergencyPhone" data-required="true" maxlength="11" placeholder="请输入负责人联系方式">
                    <span class="help-block cred">(仅供紧急联系使用，此账号无法登录烽火台)</span>
                </div>

            </div>

            <div class="panel panel-heading">
                调研信息
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>团队规模</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm parsley-validated" id="teamSize"
                           name="teamSize" data-required="true" maxlength="9">
                    <span class="help-block">单位(人)</span>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>从业时长</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm parsley-validated" id="workingTime"
                           name="workingTime" data-required="true" maxlength="9">
                    <span class="help-block">单位(月)</span>

                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">从业经验</label>

                <div class="col-sm-6 lh30">
                    <label class="label-radio inline">
                        <input type="radio" name="experienceType" value="1">
                        <span class="custom-radio"></span>
                        独立经营
                    </label>
                    <label class="label-radio inline">
                        <input type="radio" name="experienceType" value="2">
                        <span class="custom-radio"></span>
                        与其他平台合作
                    </label>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>骑手商业意外险</label>

                <div class="col-sm-6 lh30">
                    <label class="label-radio inline">
                        <input type="radio" name="hasInsurance" value="1">
                        <span class="custom-radio"></span>
                        有
                    </label>
                    <label class="label-radio inline">
                        <input type="radio" name="hasInsurance" checked value="0">
                        <span class="custom-radio"></span>
                        无
                    </label>
                </div>

            </div>

            <div class="panel panel-heading">
                资质信息
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">食品流通许可证编号</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm parsley-validated" id="foodLicenseNumber"
                           name="foodLicenseNumber" data-required="true" maxlength="100">
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">上传食品流通许可证</label>

                <div class="col-sm-6 lh30">
                    <a href="javascript:void(0)" class="js_uploadbtn" style="display: inline-block;">上传附件</a>
                    <input type="file" name="imgFile" style="display:none" class="js_fileupload">
                    <input type="hidden" id="foodLicenseFile" class="js_upload_back_url" name="foodLicenseFile" />
                    <img style="display:none; width:300px; height:400px;" class="js_img_click_open" />
                </div>

            </div>

            <div class="form-group">
                <div class="col-sm-10 text-center">
                    <input type="button" class="btn btn-danger" value="创建" id="js_save_and_next" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="/headfranchisee/list" class="btn btn-success" >取消</a>
                </div>
            </div>
        </div>
    </form>
</div>
<@commonPop></@commonPop>
<script type="text/javascript" src="${staticPath}/js/page/utils.js?ver=${staticVersion !''}"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="${staticPath}/js/page/headfranchisee/headfranchisee_create.js?ver=${staticVersion !''}"></script>
<script>
    document.title = '新建加盟商总账号';
</script>