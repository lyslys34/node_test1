<#macro station_nav active orgOperations orgVo>
<ul class="nav nav-tabs">
  <#if orgOperations??>
  <#list orgOperations as op>
    <li role="presentation" <#if active == op.order>class="active"</#if>><a href="${op.url}${orgVo.orgId}">${op.name}</a></li>
  </#list>
  </#if>
</ul>
</#macro>
