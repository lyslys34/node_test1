<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">

<link rel="stylesheet" type="text/css" href="/static/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />

<style type="text/css">
    .ui-autocomplete{
        z-index: 2000;
    }
    a[name='aname']:hover {color:#7FC7A6}
</style>

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="get" action="/org/partner/franchiseeList">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">所在城市</label>
                            <select class="form-control input-sm" id="cityId" name="cityId">
                                <option value = "0">全部</option>
                            <#if cityList?exists && (cityList?size > 0)>
                                <#list cityList as city>
                                    <option value="${city.city_id}" <#if cityId?exists && cityId == city.city_id>selected</#if>>${city.name}</option>
                                </#list>
                            </#if>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">加盟商</label>
                            <select class="form-control input-sm" id="orgId" name="orgId">
                                <option value="0">全部</option>
                            <#list orgs as org>
                                <option value="${org.orgId}" <#if orgId?exists && orgId == org.orgId>selected</#if>>${org.orgName}</option>
                            </#list>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3" style="padding-top:18px">
                        <button type="submit" class="btn btn-success">查询</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
            </div>
            <input type = "hidden" name="source" value="submit"/>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>加盟商名称</th>
                <th>合作状态</th>
                <th>审核状态</th>
                <th>城市</th>
                <th>合作站点数</th>
                <th>组织人员数</th>
                <th>负责人</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->

            <#list result.data.data as org>
            <tr>
                <td>${org.orgName!''}</td>
                <td>
                    <#if org.status==1>合作
                    <#elseif org.status==0>不合作
                    </#if>
                </td>
                <td>
                    <#if org.orgChecked == 1>审核通过
                    <#elseif org.orgChecked == 0>待审核
                    <#elseif org.orgChecked == 2>驳回
                    </#if>
                </td>
                <td>${org.cityName!''}</td>
                <td>${org.cooperativeSubOrgCount!0}</td>
                <td>${org.riderCount!0}</td>
                <td>${org.orgManager!''}</td>
                <td>
                    <#if org.levelType == 220>
                        <#if org.status==1 && org.orgChecked != 0>
                            <a data-toggle="tooltip" title="修改信息" data-placement="top"
                               href="/org/join/updateOrg?orgId=${org.orgId}" target="_blank">
                                <i class="fa fa-edit fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                        </#if>
                        <a data-toggle="tooltip" title="人员列表" data-placement="top"
                           href="/rider/listByOrgId?orgId=${org.orgId}&f=join_org" target="_blank">
                            <i class="fa fa-list-ol fa-lg opration-icon"></i>
                        </a>&nbsp;&nbsp;
                        <a data-toggle="tooltip" title="站点列表" data-placement="top"
                           href="/org/franchiseeStationList?orgId=${org.orgId}" target="_blank">
                            <i class="fa fa-list-ul fa-lg opration-icon"></i>&nbsp;&nbsp;
                        </a>
                    <#else>
                        <#if org.orgOperations??>
                            <#list org.orgOperations as op>
                                <a data-toggle="tooltip" title="${op.name}" data-placement="top"
                                   href="${op.url}${org.orgId}" target="_blank">
                                    <i class="fa ${op.icon} fa-lg opration-icon"></i>
                                </a>&nbsp;&nbsp;
                            </#list>
                        </#if>
                    </#if>
                </td>
            </tr>
            </#list>
            </tbody>
        </table>
        <div style="float: left; padding: 10px; 15px"><span>共${recordCount!'0'}项</span></div>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/org/partner/franchiseeList"/>
    </#if>

    </div>
</div>
<script>
    document.title = '加盟商管理';
    $('#cityId').select2();
    $('#orgId').select2();
</script>
