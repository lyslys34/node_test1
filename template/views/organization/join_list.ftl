<#include "join_config.ftl">
<#include "../widgets/sidebar.ftl">

<style>
    .cgreen { color: green; }
    .cred { color: red; }
    .fsize17 { font-size: 17px;}
    .hover_underline:hover{ text-decoration: underline; }
</style>

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post"
              action="/org/join/orgList">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">城市</label>
                            <select class="form-control input-sm" id="cityId" name="cityId">
                                <option value="0">全部</option>
                                <#if cityList?exists>
                                    <#list cityList as c>
                                        <option value="${c.city_id !'0'}"
                                                <#if RequestParameters.cityId?exists && RequestParameters.cityId == c.city_id?string>selected="selected" </#if> >${c.name !''}</option>
                                    </#list>
                                </#if>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">加盟商名称</label>
                            <input type="text" class="form-control input-sm" name="orgName" placeholder="请输入汉字搜索"
                                   value="${RequestParameters.orgName !''}"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">加盟商ID</label>
                            <input type="text" class="form-control input-sm" name="orgId" placeholder="请准确输入加盟商ID"
                                   value="${RequestParameters.orgId !''}"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">审核状态</label>
                            <select class="form-control input-sm" id="checked" name="checked">
                                <option value="-1">全部</option>
                                <option value="1" <#if RequestParameters.checked?exists && RequestParameters.checked == '1'>selected="selected" </#if>>审核通过</option>
                                <option value="0" <#if RequestParameters.checked?exists && RequestParameters.checked == '0'>selected="selected" </#if>>待审核</option>
                                <option value="2" <#if RequestParameters.checked?exists && RequestParameters.checked == '2'>selected="selected" </#if>>驳回</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">合作状态</label>
                            <select class="form-control input-sm" id="status" name="status">
                                <option value="-1">全部</option>
                                <option value="1" <#if status?exists && status == 1>selected="selected" </#if>>合作</option>
                                <option value="0" <#if status?exists && status == 0>selected="selected" </#if>>不合作</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-left">
                <button type="submit" class="btn btn-success">查询</button>
                <a type="submit" class="btn btn-warning" href="/org/join/createOrg">新建加盟商</a>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th style="width: 20%;">加盟商信息</th>
                <th style="width: 10%;">证照信息</th>
                <th style="width: 20%;">城市团队/渠道经理</th>
                <th style="width: 10%;">审核状态</th>
                <th style="width: 10%;">合作状态</th>
                <th style="width: 20%;">操作</th>
            </tr>
            </thead>
            <tbody>
            <#if result?? && result.data??>
                <#list result.data as o>
                <tr>
                    <td>
                        <#if o.checked?exists && o.checked == 0>
                            <p><span style="font-weight: bold;">${o.orgName !''}</span>&nbsp;&nbsp;&nbsp;
                            (id:${o.orgId ! '0'}) </p>
                        <#else>
                            <p><a href="/org/join/updateOrg?orgId=${o.orgId !'0'}" class="hover_underline">
                                <span style="font-weight: bold;">${o.orgName !''}</span>&nbsp;&nbsp;&nbsp;
                                (id:${o.orgId ! '0'}) <br/>
                            </a></p>
                        </#if>

                        <p>城市：${o.cityName !''} </p>
                        <p>合作站点数量：${o.childOrgNum ! '0'}</p>
                        <p>在职员工总数：${o.userNum !'0'}</p>
                        <p>模式：<#if o.isProxy?exists && o.isProxy == 1>代理<#else>加盟</#if> </p>
                    </td>
                    <td>
                        <p><#if o.contractPicUrl?exists && o.contractPicUrl != ''><i class="fa fa-check-circle fsize17 cgreen"></i><#else><i class="fa fa-times-circle fsize17 cred"></i></#if>合同照片</p>
                        <p><#if o.businessLicensePicUrl?exists && o.businessLicensePicUrl != ''><i class="fa fa-check-circle fsize17 cgreen"></i><#else><i class="fa fa-times-circle fsize17 cred"></i></#if>营业执照照片</p>
                        <p><#if o.foodLicenseNumber?exists && o.foodLicenseNumber != ''><i class="fa fa-check-circle fsize17 cgreen"></i><#else><i class="fa fa-times-circle fsize17 cred"></i></#if>食品流通许可证 </p>
                    </td>
                    <td>
                        <p>${o.parentOrgName !''} </p>
                        <#if o.parentUserList?exists >
                            <#list o.parentUserList as u>
                                <p>${u.name!''}</p>
                            </#list>
                        </#if>
                    </td>
                    <td>
                        <#if o.checked?exists && o.checked == 0>
                            待审核
                        <#elseif o.checked?exists && o.checked == 1>
                            审核通过
                        <#elseif o.checked?exists && o.checked == 2>
                            驳回
                        </#if>
                    </td>
                    <td>
                        <#if o.status?exists && o.status == 0>
                             不合作
                        <#elseif o.status?exists && o.status == 1>
                             合作
                        </#if>
                    </td>
                    <td>
                        <#if o.checked?exists && o.checked == 0>
                            <a data-toggle="tooltip" title="结算设置" data-placement="top" href="/settle/orgsetting?orgId=${o.orgId!'0'}&f=join_org" target="_blank">
                                <i class="fa fa-credit-card fa-lg opration-icon"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="tooltip" title="人员列表" data-placement="top" href="/rider/listByOrgId?orgId=${o.orgId!'0'}&f=join_org" target="_blank">
                                <i class="fa fa-list-ol fa-lg opration-icon"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="tooltip" title="站点列表" data-placement="top" href="/org/franchiseeStationList?orgId=${o.orgId!'0'}" target="_blank">
                                <i class="fa fa-list-ul fa-lg opration-icon"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="tooltip" title="操作记录" data-placement="top" href="/log/staff?orgId=${o.orgId!'0'}&f=join_org" target="_blank">
                                <i class="fa fa-history fa-lg opration-icon"></i>&nbsp;&nbsp;
                            </a>
                        <#else>
                            <a data-toggle="tooltip" title="修改信息" data-placement="top" href="/org/join/updateOrg?orgId=${o.orgId !'0'}" target="_blank">
                                <i class="fa fa-edit fa-lg opration-icon"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="tooltip" title="结算设置" data-placement="top" href="/settle/orgsetting?orgId=${o.orgId!'0'}&f=join_org" target="_blank">
                                <i class="fa fa-credit-card fa-lg opration-icon"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="tooltip" title="人员列表" data-placement="top" href="/rider/listByOrgId?orgId=${o.orgId!'0'}&f=join_org" target="_blank">
                                <i class="fa fa-list-ol fa-lg opration-icon"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="tooltip" title="站点列表" data-placement="top" href="/org/franchiseeStationList?orgId=${o.orgId!'0'}" target="_blank">
                                <i class="fa fa-list-ul fa-lg opration-icon"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="tooltip" title="操作记录" data-placement="top" href="/log/staff?orgId=${o.orgId!'0'}&f=join_org" target="_blank">
                                <i class="fa fa-history fa-lg opration-icon"></i>&nbsp;&nbsp;
                            </a>
                        </#if>
                    </td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>
        <div style="float: left; padding: 10px; 15px"><span>共${(result.totalCount)!'0'}项</span></div>
    <#import "../page/pager.ftl" as q>
    <#if result??>
        <@q.pager pageNo=result.pageNo pageSize=result.pageSize recordCount=result.totalCount toURL="/org/join/orgList"/>
    </#if>

    </div>
</div>
<script type="text/javascript" src="${staticPath}/js/page/organization/join_list.js"></script>

<script>
    document.title = '加盟商管理';
</script>
