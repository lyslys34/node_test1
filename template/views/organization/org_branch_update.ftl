<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">
<style>
    .cred { color: #b22222; }
    .lh30 { line-height: 30px; }
    .tl {
        text-align: left !important;
    }
    input[type="radio"] {
        opacity: 0
    }
    #main-container{
        padding: 10px 20px;
    }

</style>
<div id="main-container">
    <#if RequestParameters.f?exists>
        <#if RequestParameters.f == 'zj_branch'>
            <@zjNav org 1></@zjNav>
        </#if>
    </#if>
    <form method="post" class="form-horizontal" id="formValidate1" data-validate="parsley" novalidate="" >
        <input type="hidden" id="orgId" value="${org.orgId!'0'}" />
        <div class="panel panel-default">
            <div class="panel panel-heading">
                基本信息
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>自营分部名称</label>

                <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" id="orgName" name="orgName" data-required="true" maxlength="25" value="${org.orgName!''}">
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>自营片区</label>

                <div class="col-sm-4">
                    <select class="form-control input-sm" id="parentId" name="parentId">
                        <option class="other_level_org" value="0">请选择上级组织</option>
                    <#if orgs?exists>
                        <#list orgs as o>
                            <option class="other_level_org" value="${o.orgId !''}" <#if org?exists && o.orgId==org.parentId >selected="selected" </#if> >${o.orgName !''}</option>
                        </#list>
                    </#if>
                    </select>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>所在城市</label>

                <div class="col-sm-4">
                    <select class="form-control input-sm" id="orgCity" name="orgCity">
                    <#if cityList?exists>
                        <#list cityList as city >
                            <option value="${city.city_id !''}" <#if org?exists && org.cityId==city.city_id >selected="selected" </#if> >${city.name !''}</option>
                        </#list>
                    </#if>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>分部联系电话</label>

                <div class="col-sm-4">
                    <input type="text" class="form-control input-sm parsley-validated" id="emergencyPhone"
                           name="emergencyPhone" data-required="true" maxlength="18" value="${org.emergencyPhone!''}">
                    <span class="help-block"></span>
                </div>
                <div class="tl col-sm-4 control-label" >(仅供紧急联系使用，此账号无法登录烽火台)
                </div>

            </div>

            <div class="form-group">
                <div class="col-sm-8 text-center">
                    <input type="button" class="btn btn-danger" value="提交" id="js_save" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="button" class="btn btn-success" value="取消" id="js_cancel" />
                </div>
            </div>
        </div>
    </form>
</div>
<@commonPop></@commonPop>
<script type="text/javascript" src="${staticPath}/js/page/utils.js?ver=${staticVersion !''}"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="${staticPath}/js/page/organization/org_branch_update.js"></script>
<script>
    document.title = '修改自营分部';
</script>