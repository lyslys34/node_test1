<#macro progress length step>
<link href="/static/css/lib/bootstrap-nav-wizard.css" rel="stylesheet">
<style type="text/css">
    .nav-wizard li{
        width:${90/length}%;
    }
</style>
<ul class="nav nav-wizard">
    <li <#if step==1>class="active"</#if>><a>step1:新建站点</a></li>
    <li <#if step==2>class="active"</#if>><a>step2:新建负责人</a></li>
    <#if length == 3><li id="settleProgress" <#if step==3>class="active"</#if>><a>step3:结算设置</a></li></#if>
</ul>
</#macro>