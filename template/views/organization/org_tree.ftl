
<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="pageName">org_tree</content>
<content tag="javascript">/organization/org_tree.js</content>
<content tag="cssmain">/organization/org_tree.css</content>

<div id="main-container">
    <div class="container-fluid wrap-box">
      <div class="row">
        <div class="col-md-3 rbnopadding yauto">
              <div class="control-group mt20" >
                  <div class="controls">
                      <input type="text" placeholder="搜索组织架构或负责人" id="J-searchAuthTree" class="m-wrap">
                  </div>
              </div>
              <div class="mt20">
                <ul class="ztree" id="treeMenu" curr-node-name="${name!}">
                    组织树形菜单加载中...
                </ul>
              </div>
        </div>
        <div class="col-md-9 cityTree">
            <div id="J-btn-group" class="ml20">
                <h1 id="J-nodeName" class="vmiddle">配送总部</h1>
                <a href="javascript:;" class="btn btn-success hidden" id="J-addNode">新增下级结构</a>
                <a href="javascript:;" class="btn btn-danger hidden" id="J-revise">修改</a>
                <a href="javascript:;" class="btn btn-success hidden" id="J-move">转移</a>
            </div>
            <div class="inner-box1 mt20">
              <div class="tree ml20" id="J-tree">
                   获取组织架构中...
              </div>
            </div>

        </div>
      </div>
    </div>
</div>
<script>
    document.title = '组织架构管理';
</script>
