<#macro nav navs>
<ul class="nav nav-tabs">
    <#list navs as nav>
    <li role="presentation" <#if nav?? && nav.active>class="active"</#if>><a href="${nav.url!''}">${nav.name!''}</a></li>
    </#list>
</ul>
</#macro>