<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">

<link rel="stylesheet" type="text/css" href="/static/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />

<style type="text/css">
    #main-container{
        padding: 10px 20px;
    }
    a[name='aname']:hover {color:#7FC7A6}
</style>

<div id="main-container">
    <@zjNav org 3></@zjNav>
    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>站点名称</th>
                <th>站点ID</th>
                <th>合作状态</th>
                <th>服务商家数</th>
                <th>在职人员数</th>
                <th>负责人</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->

            <#list result.data.data as org>
                <tr>
                    <td><a style="color: blue;" href="/org/update/${org.orgId!'0'}">${org.orgName!''}</a></td>
                    <td>${org.orgId!'0'}</td>
                    <td>
                        <#if org.status==1>合作
                        <#elseif org.status==0>不合作
                        </#if>
                    </td>
                    <td>${org.poiCount!0}</td>
                    <td>${org.riderCount!0}</td>
                    <td>${org.orgManager!''}</td>
                </tr>
            </#list>

            </tbody>
        </table>
        <div style="float: left; padding: 10px; 15px"><span>共${recordCount!'0'}项</span></div>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/org/zjStations"/>
    </#if>

    </div>
</div>
<script>
    document.title = '站点管理';
    $('#cityId').select2();
    $('#orgId').select2();
</script>
