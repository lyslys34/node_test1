<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/audit/org_list.js</content>

<link rel="stylesheet" type="text/css" href="/static/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />

<style type="text/css">
.ui-autocomplete{
    z-index: 2000;
}
</style>

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post" action="/auditOrg/list">
            <div class="panel-body">
                <div class="row">
                	<div class="col-md-3">
            			<div class="form-group">
                            <label class="control-label">组织名称</label>
                            <input type="text" class="form-control input-sm parsley-validated js_org_name" name="orgName" id="orgName" data-required="true" data-minlength="8" value="${orgName!""}"/>
                            <input type="hidden" class="js_org_id" name="orgId" id="orgId" value="${orgId!""}"/>
	                    </div>
	          		</div>
                    <div class="col-md-3" style="padding-top:18px">
                        <button type="submit" class="btn btn-success">查询</button>&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>创建时间</th>
                <th>提交人</th>
                <th>组织名称</th>
                <th>组织层级</th>
                <th>城市</th>
                <th>上级组织</th>
                <th>城市团队</th>
                <th>审核类型</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#if audits?exists && (audits?size > 0)>
                <#list audits as audit>
                    <tr>
                        <td>${audit.ctime!''}</td>
                        <td>${audit.createOrgUserName!''}</td>
                        <td>${audit.orgName!''}</td>
                        <td>
                            <#if audit.levelType == 220>
                                加盟商
                            <#elseif audit.levelType == 230>
                                加盟站
                            </#if>
                        </td>
                        <td>${audit.cityName!''}</td>
                        <td>${audit.parentOrgName!''}</td>
                        <td>${audit.cityOrgName!''}</td>
                        <td>
                            <#if audit.checked == 0>
                                首次提交
                            <#elseif audit.checked == 3>
                                驳回后重新提交
                            </#if>
                        </td>
                        <td  style="padding-top: 2px;"><a href="/auditOrg/detail?orgAuditId=${audit.id}" class="btn btn-success">审阅</a></td>
                    </tr>
                </#list>
            </#if>
            </tbody>
        </table>
        <div style="float: left; padding: 10px; 15px"><span>共${recordCount!'0'}项</span></div>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/auditOrg/list"/>
    </#if>

    </div>
</div>
<script>document.title = '合作伙伴审核';</script>

