<#include "join_config.ftl">
<#include "../widgets/sidebar.ftl">
<style>
    .cred {
        color: #b22222;
    }

    .lh30 {
        line-height: 30px;
    }

    input[type="radio"] {
        opacity: 0
    }

</style>
<div id="main-container">
<#if org?exists >
    <@joinOrgNavi org is_org_manager 1></@joinOrgNavi>

    <form method="post" class="form-horizontal" id="formValidate1" data-validate="parsley" novalidate="">
        <input type="hidden" value="${org.orgId!'0'}" id="orgId" name="orgId"/>
        <input type="hidden" value="${org.orgType !'2'}" id="orgType" name="orgType"/>
        <input type="hidden" value="${org.levelType !'220'}" id="levelType" name="levelType"/>

        <div class="panel panel-default">
            <div class="panel panel-heading">
                基本信息
            </div>
            <#if org.orgChecked?exists && org.orgChecked != 0 && org.status==1>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>加盟商名称</label>

                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm" id="orgName" name="orgName"
                               data-required="true" value="${org.orgName !''}" maxlength="25" <#if is_org_manager?exists && is_org_manager == 1>readonly</#if>>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>所属加盟城市</label>

                    <div class="col-sm-6">
                        <#if is_org_manager?exists && is_org_manager == 1>
                            <input type="hidden" name="parentId" id="parentId" value="${org.parentId}" />
                        </#if>
                        <select class="form-control input-sm" id="parentId" name="parentId" <#if is_org_manager?exists && is_org_manager == 1>disabled</#if>>
                            <option class="other_level_org" value="0">请选择上级组织</option>

                            <#if orgs?exists>
                                <#list orgs as o>
                                    <option class="other_level_org" value="${o.orgId !''}"
                                            <#if org.parentId==o.orgId>selected="selected" </#if> >${o.orgName !''}</option>
                                </#list>
                            </#if>
                        </select>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>所在城市</label>

                    <div class="col-sm-6">
                        <#if is_org_manager?exists && is_org_manager == 1>
                            <#-- <input type="hidden" name="orgCity" id="orgCity" value="${org.cityId}" />-->
                        </#if>
                        <select class="form-control input-sm" id="orgCity" name="orgCity" <#if is_org_manager?exists && is_org_manager == 1>disabled</#if>>
                            <#if cityList?exists>
                                <#list cityList as city >
                                    <option value="${city.city_id !''}"
                                            <#if org.cityId==city.city_id >selected="selected" </#if> >${city.name !''}</option>
                                </#list>
                            </#if>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>模式</label>

                    <div class="col-sm-6">
                        <#if is_org_manager?exists && is_org_manager == 1>
                            <input type="hidden" name="isProxy" value="${org.isProxy}" />
                        </#if>
                        <select class="form-control input-sm" id="isProxy" name="isProxy" <#if is_org_manager?exists && is_org_manager == 1>disabled</#if>>
                            <option value="0" <#if org.isProxy?exists && org.isProxy == 0>selected="selected" </#if> >加盟
                            </option>
                            <option value="1" <#if org.isProxy?exists && org.isProxy == 1>selected="selected" </#if> >代理
                            </option>
                        </select>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>紧急联系人</label>

                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm parsley-validated" id="emergencyContact"
                               name="emergencyContact" data-required="true" value="${org.emergencyContact!''}"
                               maxlength="20">
                        <span class="help-block cred">(仅供紧急联系使用，此账号无法登录烽火台)</span>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>紧急联系人电话</label>

                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm parsley-validated" id="emergencyPhone"
                               name="emergencyPhone" data-required="true" value="${org.emergencyPhone !''}"
                               maxlength="11">
                        <span class="help-block cred">(仅供紧急联系使用，此账号无法登录烽火台)</span>
                    </div>

                </div>

                <div class="panel panel-heading">
                    调研信息
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>团队规模</label>

                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm parsley-validated" id="teamSize"
                               name="teamSize" data-required="true" value="${org.teamSize !'0'}" maxlength="9" <#if is_org_manager?exists && is_org_manager == 1>readonly</#if>>
                        <span class="help-block">单位(人)</span>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>从业时长</label>

                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm parsley-validated" id="workingTime"
                               name="workingTime" data-required="true" value="${org.workingTime !''}" maxlength="9" <#if is_org_manager?exists && is_org_manager == 1>readonly</#if>>
                        <span class="help-block">单位(月)</span>

                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">从业经验</label>

                    <div class="col-sm-6 lh30">
                        <label class="label-radio inline">
                            <input type="radio" name="experienceType"
                                   <#if org.experienceType==1 >checked="checked" </#if> value="1" <#if is_org_manager?exists && is_org_manager == 1>disabled</#if>>
                            <span class="custom-radio"></span>
                            独立经营
                        </label>
                        <label class="label-radio inline">
                            <input type="radio" name="experienceType"
                                   <#if org.experienceType==2 >checked="checked" </#if> value="2" <#if is_org_manager?exists && is_org_manager == 1>disabled</#if>>
                            <span class="custom-radio"></span>
                            与其他平台合作
                        </label>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>骑手商业意外险</label>

                    <div class="col-sm-6 lh30">
                        <label class="label-radio inline">
                            <input type="radio" name="hasInsurance" <#if org.hasInsurance==1 >checked="checked" </#if>
                                   value="1" <#if is_org_manager?exists && is_org_manager == 1>disabled</#if>>
                            <span class="custom-radio"></span>
                            有
                        </label>
                        <label class="label-radio inline">
                            <input type="radio" name="hasInsurance" <#if org.hasInsurance==0 >checked="checked" </#if>
                                   value="0" <#if is_org_manager?exists && is_org_manager == 1>disabled</#if>>
                            <span class="custom-radio"></span>
                            无
                        </label>
                    </div>

                </div>

                <div class="panel panel-heading">
                    资质信息
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">食品流通许可证编号</label>

                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm parsley-validated" id="foodLicenseNumber"
                               name="foodLicenseNumber" data-required="true" value="${org.foodLicenseNumber !''}"
                               maxlength="100">
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">上传食品流通许可证</label>

                    <div class="col-sm-6 lh30">
                        <#if org.foodLicenseFile?exists && org.foodLicenseFile != ''>
                            <a href="javascript:void(0)" class="js_uploadbtn" style="display: inline-block;">重新上传</a>
                            <input type="file" name="imgFile" style="display:none" class="js_fileupload">
                            <input type="hidden" id="foodLicenseFile" class="js_upload_back_url" name="foodLicenseFile"
                                   value="${org.foodLicenseFile!''}"/>
                            <img style="display:block;width:300px; height:400px;" src="${org.foodLicenseFile !''}"
                                 class="js_img_click_open"/>
                        <#else>
                            <a href="javascript:void(0)" class="js_uploadbtn" style="display: inline-block;">上传附件</a>
                            <input type="file" name="imgFile" style="display:none" class="js_fileupload">
                            <input type="hidden" id="foodLicenseFile" class="js_upload_back_url"
                                   name="foodLicenseFile"/>
                            <img style="display:none; width:300px; height:400px;" class="js_img_click_open"/>
                        </#if>
                    </div>

                </div>
            <#else>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>加盟商名称</label>

                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm" disabled id="orgName" name="orgName"
                               data-required="true" value="${org.orgName !''}" maxlength="25">
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>所属加盟城市</label>

                    <div class="col-sm-6">
                        <select class="form-control input-sm" id="parentId" name="parentId" disabled>
                            <option class="other_level_org" value="0">请选择上级组织</option>

                            <#if orgs?exists>
                                <#list orgs as o>
                                    <option class="other_level_org" value="${o.orgId !''}"
                                            <#if org.parentId==o.orgId>selected="selected" </#if> >${o.orgName !''}</option>
                                </#list>
                            </#if>
                        </select>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>所在城市</label>

                    <div class="col-sm-6">
                        <select class="form-control input-sm" id="orgCity" name="orgCity" disabled>
                            <#if cityList?exists>
                                <#list cityList as city >
                                    <option value="${city.city_id !''}"
                                            <#if org.cityId==city.city_id >selected="selected" </#if> >${city.name !''}</option>
                                </#list>
                            </#if>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>模式</label>

                    <div class="col-sm-6">
                        <select class="form-control input-sm" id="isProxy" name="isProxy" disabled>
                            <option value="0" <#if org.isProxy?exists && org.isProxy == 0>selected="selected" </#if> >加盟
                            </option>
                            <option value="1" <#if org.isProxy?exists && org.isProxy == 1>selected="selected" </#if> >代理
                            </option>
                        </select>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>紧急联系人</label>

                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm parsley-validated" id="emergencyContact"
                               name="emergencyContact" data-required="true" value="${org.emergencyContact!''}"
                               maxlength="20" disabled>
                        <span class="help-block cred">(仅供紧急联系使用，此账号无法登录烽火台)</span>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>紧急联系人电话</label>

                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm parsley-validated" id="emergencyPhone"
                               name="emergencyPhone" data-required="true" value="${org.emergencyPhone !''}"
                               maxlength="11" disabled>
                        <span class="help-block cred">(仅供紧急联系使用，此账号无法登录烽火台)</span>
                    </div>

                </div>

                <div class="panel panel-heading">
                    调研信息
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>团队规模</label>

                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm parsley-validated" id="teamSize"
                               name="teamSize" data-required="true" value="${org.teamSize !'0'}" maxlength="9" disabled>
                        <span class="help-block">单位(人)</span>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>从业时长</label>

                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm parsley-validated" id="workingTime"
                               name="workingTime" data-required="true" value="${org.workingTime !''}" maxlength="9" disabled>
                        <span class="help-block">单位(月)</span>

                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">从业经验</label>

                    <div class="col-sm-6 lh30">
                        <label class="label-radio inline">
                            <input type="radio" name="experienceType"
                                   <#if org.experienceType==1 >checked="checked" </#if> value="1" disabled>
                            <span class="custom-radio"></span>
                            独立经营
                        </label>
                        <label class="label-radio inline">
                            <input type="radio" name="experienceType"
                                   <#if org.experienceType==2 >checked="checked" </#if> value="2" disabled>
                            <span class="custom-radio"></span>
                            与其他平台合作
                        </label>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><span class="cred">*</span>骑手商业意外险</label>

                    <div class="col-sm-6 lh30">
                        <label class="label-radio inline">
                            <input type="radio" name="hasInsurance" <#if org.hasInsurance==1 >checked="checked" </#if>
                                   value="1" disabled>
                            <span class="custom-radio"></span>
                            有
                        </label>
                        <label class="label-radio inline">
                            <input type="radio" name="hasInsurance" <#if org.hasInsurance==0 >checked="checked" </#if>
                                   value="0" disabled>
                            <span class="custom-radio"></span>
                            无
                        </label>
                    </div>

                </div>

                <div class="panel panel-heading">
                    资质信息
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">食品流通许可证编号</label>

                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm parsley-validated" id="foodLicenseNumber"
                               name="foodLicenseNumber" data-required="true" value="${org.foodLicenseNumber !''}"
                               maxlength="100" disabled>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">上传食品流通许可证</label>

                    <div class="col-sm-6 lh30">
                        <#if org.foodLicenseFile?exists && org.foodLicenseFile != ''>
                            <img style="display:block;width:300px; height:400px;" src="${org.foodLicenseFile !''}"
                                 class="js_img_click_open"/>
                        <#else>
                            <img style="display:none; width:300px; height:400px;" class="js_img_click_open"/>
                        </#if>
                    </div>

                </div>
            </#if>

            <div class="form-group">
                <div class="col-sm-10 text-center">
                    <#if org.orgChecked?exists && org.orgChecked != 0>
                        <input type="button" class="btn btn-danger" value="保存" id="js_save"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </#if>
                    <a href="<#if is_org_manager?exists && is_org_manager == 1>/org/partner/franchiseeList<#else>/org/join/orgList</#if>" class="btn btn-success" id="js_cancel">返回</a>
                </div>
            </div>
        </div>
    </form>
<#else>
    加盟商不存在
</#if>
</div>
<@commonPop></@commonPop>
<script type="text/javascript" src="${staticPath}/js/page/utils.js?ver=${staticVersion !''}"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="${staticPath}/js/page/organization/join_update.js"></script>

<script>
    document.title = '加盟商信息';
    var isOrgManager = false;
    <#if is_org_manager?exists && is_org_manager == 1>
        isOrgManager = true;
    </#if>
</script>