<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">
<link rel="stylesheet" type="text/css" href="/static/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />
<style type="text/css">
    a:hover { text-decoration: underline; font-weight: bold; color:#7FC7A6; }
</style>
<content tag="javascript">/organization/stations.js</content>
<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="get" action="/org/deliveryDivisionList">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">城市</label>
                            <select class="form-control input-sm" id="cityId" name="cityId">
                                <option value = "-1">全部</option>
                                <#if cityList?exists && (cityList?size > 0)>
                                    <#list cityList as city>
                                        <option value="${city.city_id}" <#if RequestParameters.cityId ?? &&RequestParameters.cityId == city.city_id?string>selected</#if>>${city.name}</option>
                                    </#list>
                                </#if>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">自营配送分部名称</label>
                            <select class="form-control input-sm" id="orgId" name="orgId">
                                <option value = "-1">全部</option>
                            <#--<option value="0"<#if RequestParameters.orgId?? && RequestParameters.orgId == "-1"> selected="selected" </#if>>全部</option>-->
                                <#if subOrgViewList?exists && (subOrgViewList?size > 0)>
                                    <#list subOrgViewList as org>
                                        <option value="${org.orgId!'0'}" <#if RequestParameters.orgId?? && RequestParameters.orgId == org.orgId?string >selected="selected" </#if>>${org.orgName} </option>
                                    </#list>
                                </#if>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">自营分部ID</label>
                                <input type="text" class="form-control input-sm" name="divisionOrgId" value="${RequestParameters.divisionOrgId!''}" />
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3" style="padding-top:16px">
                        <button type="submit" class="btn btn-success">查询</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="/org/branch/createOrg" role="button" data-toggle="modal" class="btn btn-success">新建分部</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
                <tr>
                    <th>自营分部信息</th>
                    <th>自营片区/配送经理</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
            <#if page?? && page.orgStationVoList??>
                <#list page.orgStationVoList as org>
                    <#assign orgStation = org.orgStation>
                <tr data-orgname="${orgStation.orgName !''}">
                    <td>
                        <p><a href="/org/branch/updateOrg?orgId=${orgStation.orgId}&f=zj_branch" target="_blank">${orgStation.orgName}</a> &nbsp;&nbsp;&nbsp;&nbsp;(id:${orgStation.orgId})</p>
                        <p>城市：${orgStation.cityName}</p>
                        <p>合作站点数量：${orgStation.cooperativeSubOrgCount!0}</p>
                        <p>在职员工总数：${orgStation.riderCount!0}</p>
                    </td>
                    <#assign parent = org.parent>
                    <td>
                        <p>自营片区：${parent.orgName}</p>
                        <#if parent.managerNameList?exists && (parent.managerNameList?size > 0)>
                            <p>自营配送经理：</p>
                                <#list parent.managerNameList as managerName>
                                        <p>${managerName!''}</p>
                                </#list>
                        </#if>

                        <#if parent.managerAssistNameList?exists && (parent.managerAssistNameList?size > 0)>
                            <p>自营配送经理助理：</p>
                                <#list parent.managerAssistNameList as managerAssistName>
                                        <p>${managerAssistName!''}</p>
                                </#list>
                        </#if>
                    </td>
                    <td>
                        <a data-toggle="tooltip" title="" data-placement="top" href="/org/branch/updateOrg?orgId=${orgStation.orgId}&f=zj_branch" target="_blank" data-original-title="修改信息">
                            <i class="fa fa-edit fa-lg opration-icon"></i>
                        </a>
                        &nbsp;&nbsp;
                        <a data-toggle="tooltip" title="" data-placement="top" href="/rider/listByOrgId?orgId=${orgStation.orgId}&f=zj_branch" target="_blank" data-original-title="人员列表">
                            <i class="fa fa-list-ol fa-lg opration-icon"></i>
                        </a>
                        &nbsp;&nbsp;
                        <a data-toggle="tooltip" title="" data-placement="top" href="/org/zjStations?parentId=${orgStation.orgId}" target="_blank" data-original-title="站点列表">
                            <i class="fa fa-list-ul fa-lg opration-icon"></i>
                        </a>
                        &nbsp;&nbsp;
                        <a data-toggle="tooltip" title="" data-placement="top" href="/log/staff?orgId=${orgStation.orgId}&f=zj_branch" target="_blank" data-original-title="操作记录">
                            <i class="fa fa-history fa-lg opration-icon"></i>
                        </a>
                    </td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>

        <#if (page)?exists && (page.total)?exists && (page.pageSize)?exists && (page.pageNum)?exists && page.total gt 0 && page.pageSize gt 0 && page.pageNum gt 0  >
            <div style="float: left; padding: 10px; 15px"><span>共${page.total!'0'}项</span></div>
            <#if page.total??>
                <#import "/page/pager.ftl" as q>
                <@q.pager pageNo=page.pageNum pageSize=page.pageSize recordCount=page.total toURL="/org/deliveryDivisionList"/>
            </#if>
        </#if>

    </div>

</div>
<script>
    document.title = '自营配送分部管理';
    $('#cityId').select2();
    $('#orgId').select2();
</script>