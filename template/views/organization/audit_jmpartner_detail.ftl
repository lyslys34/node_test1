<#include "join_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/organization/audit_jmpartner_detail.js</content>
<style>
    .cred { color: #b22222; }
    .lh30 { line-height: 30px; }
    input[type="radio"] {
        opacity: 0
    }
</style>

<div id="main-container">

    <a href="#auditOrgDisagreeModal" role="button" data-toggle="modal" class="btn btn-danger">驳回请求</a>
    <a href="#auditOrgAgreeModal" role="button" data-toggle="modal" class="btn btn-success">审核通过</a>

    <h4>${org.orgName !''}</h4>

    <br/>

    <form class="no-margin" id="formValidate2" data-validate="parsley" novalidate="" method="post" action="/audit/auditOrg?orgAuditId=${org.id!}&checked=1">
        <div class="modal fade" id="auditOrgAgreeModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>审核通过后，加盟商将与美团配送达成合作关系</h4>
                        <div class="modal-footer">
                            <input type="submit" value="确认通过" class="btn btn-success btn-sm" />
                            <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true">再想想</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form class="no-margin" id="formValidate3" data-validate="parsley" novalidate="" method="post" action="/audit/auditOrg?orgAuditId=${org.id!}&checked=2">
        <div class="modal fade" id="auditOrgDisagreeModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>${org.orgName!""}将要驳回申请，确认?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>请填写驳回原因(必填)</label><div id="alert_error"></div>
                            <textarea rows="6" class="form-control input-sm" id="comment" name="comment" placeholder="驳回原因"></textarea>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" id="reject" value="确认驳回" class="btn btn-danger btn-sm" />
                            <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">再想想</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form method="post" class="form-horizontal" id="formValidate1" data-validate="parsley" novalidate="" >

        <div class="panel panel-default">

            <div class="panel panel-heading">
                基本信息
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>加盟商名称</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm" id="orgName" name="orgName" data-required="true" disabled="disabled" value="${org.orgName !''}">
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>所属加盟城市</label>

                <div class="col-sm-6">
                    <select class="form-control input-sm" id="parentId" name="parentId" disabled="disabled">
                        <option class="other_level_org" value="${org.parentId !''}">${org.parentOrgName !''}</option>
                    </select>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>所在城市</label>

                <div class="col-sm-6">
                    <select class="form-control input-sm" id="orgCity" name="orgCity" disabled="disabled">
                        <option value="">${org.cityName !''}</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>模式</label>

                <div class="col-sm-6">
                    <select class="form-control input-sm" id="orgType" name="orgType" disabled="disabled">
                        <#if org.isProxy == 1><option value="">代理</option></#if>
                        <#if org.isProxy == 0><option value="">加盟</option></#if>
                    </select>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>紧急联系人</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm parsley-validated" id="emergencyContact"
                           name="emergencyContact" data-required="true" disabled="disabled" value="${org.emergencyContact !''}" >
                    <span class="help-block cred">(仅供紧急联系使用，此账号无法登陆烽火台)</span>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>紧急联系人电话</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm parsley-validated" id="emergencyPhone"
                           name="emergencyPhone" data-required="true" disabled="disabled" value="${org.emergencyPhone !''}">
                    <span class="help-block cred">(仅供紧急联系使用，此账号无法登陆烽火台)</span>
                </div>

            </div>

            <div class="panel panel-heading">
                调研信息
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>团队规模</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm parsley-validated" id="teamSize"
                           name="teamSize" data-required="true" disabled="disabled" value="${org.teamSize}">
                    <span class="help-block">单位(人)</span>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>从业时长</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm parsley-validated" id="workingTime"
                           name="workingTime" data-required="true" disabled="disabled" value="${org.workingTime}">
                    <span class="help-block">单位(月)</span>

                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">从业经验</label>

                <div class="col-sm-6 lh30">
                    <label class="label-radio inline">
                        <input type="radio" name="experienceType" value="1"  disabled="disabled" <#if org.experienceType == 1>checked="checked" </#if>>
                        <span class="custom-radio"></span>
                        独立经营
                    </label>
                    <label class="label-radio inline">
                        <input type="radio" name="experienceType" value="2"  disabled="disabled" <#if org.experienceType == 2>checked="checked" </#if>>
                        <span class="custom-radio"></span>
                        与其他平台合作
                    </label>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>骑手商业意外险</label>

                <div class="col-sm-6 lh30">
                    <label class="label-radio inline">
                        <input type="radio" name="hasInsurance"  disabled="disabled" value="1" <#if org.hasInsurance == 1>checked="checked"</#if>>
                        <span class="custom-radio"></span>
                        有
                    </label>
                    <label class="label-radio inline">
                        <input type="radio" name="hasInsurance"  disabled="disabled" value="2" <#if org.hasInsurance == 0>checked="checked"</#if>>
                        <span class="custom-radio"></span>
                        无
                    </label>
                </div>

            </div>

            <div class="panel panel-heading">
                资质信息
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">食品流通许可证编号</label>

                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm parsley-validated" id="foodLicenseNumber"
                           name="foodLicenseNumber" data-required="true" disabled="disabled" value="${org.foodLicenseNumber !''}">
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">上传食品流通许可证</label>

                <div class="col-sm-6 lh30">
                    <#if org.foodLicenseFile?has_content ><img style=" width:300px; height:400px;" src="${org.foodLicenseFile !''}" /></#if>
                </div>

            </div>
        </div>
    </form>

</div>

<script type="text/javascript" src="${staticPath}/js/page/utils.js?ver=${staticVersion !''}"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
<script>
    document.title = '合作伙伴审核';
</script>