<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<script type="text/javascript" src="${staticPath}/js/page/organization/merchant.js?ver=2"></script>

<div id="main-container">
    <h4><#if orgVo?exists>${orgVo.orgName !''}(ID:${orgVo.orgId!''})</#if></h4>
    <#import "/organization/station_nav.ftl" as n>
        <@n.station_nav active=4 orgOperations=orgVo.orgOperations orgVo=orgVo />
    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>商家名</th>
                <th>商家POI</th>
                <th>配送方式</th>
                <th>商家电话</th>
                <th>商家地址</th>
                <th>上下线</th>
                <th>营业状态</th>
            </tr>
            </thead>
            <tbody>
            <#if result??>
            <#list result.data.data as merchant>
                <tr>
                    <td>${merchant.name}</td>
                    <td>${merchant.id}</td>
                    <td>
                    	<#if merchant.logistics??>
	                    	<#list merchant.logistics as logistic>
	                    		<#if logistic_index gt 0 >
	                    			<br/>
	                    		</#if>
	                    		${logistic.topType}:${logistic.subType}
	                    	</#list>
	                   	<#else>
	                   		没取出来
                    	</#if>
                    </td>
                    <td>${merchant.phone}</td>
                    <td>${merchant.address}</td>
                    <td><#if merchant.valid??><#if merchant.valid==1>上线<#elseif merchant.valid == 0>下线</#if></#if></td>
                    <td><#if merchant.state??><#if merchant.state==1>营业<#elseif merchant.state == 3>休息</#if></#if></td>
                </tr>
            </#list>
            </#if>
            </tbody>
        </table>
    <#if recordCount?? && recordCount gt 0><div class="panel-footer clearfix pull-left">共${recordCount!'0'}条</div></#if>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/org/merchantlist_station"/>
    </#if>
    </div>
</div>
<script>document.title = '服务商家列表';</script>
