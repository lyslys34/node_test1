<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/organization/station_update.js</content>
<#if readonly?exists && readonly == 0>
<div id="main-container">
    <#if auditOrganizationVo?? && auditOrganizationVo.comment??><h4 style="color:red;">${auditOrganizationVo.comment!''}</h4></#if>
    <h4><#if orgVo?exists>${orgVo.orgName !''}(ID:${orgVo.orgId!''})</#if></h4>
    <#import "/organization/station_nav.ftl" as n>
    <@n.station_nav active=1 orgOperations=orgVo.orgOperations orgVo=orgVo />
    <div class="panel panel-default">
        <form method="post" class="form-horizontal" id="formValidate1" data-validate="parsley" novalidate="" action="/org<#if is_org_manager?exists && is_org_manager == 1>/partner</#if>/doUpdate">
            <input type="hidden" value="${orgVo.orgType}" name="orgType" id="orgType" />
            <input type="hidden" name="orgId" value="${orgVo.orgId}" />
            <input type="hidden" name="levelType" value="${orgVo.levelType}" id="levelType" />
            <div class="form-group">
                <label class="control-label col-sm-2">站点名称</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control input-sm parsley-validated" <#if (is_org_manager?exists && is_org_manager == 1) || orgVo.orgChecked == 0>readonly</#if> id="orgName" name="orgName" data-required="true" value="<#if orgVo?exists>${orgVo.orgName !''}</#if>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2"><#if orgVo.levelType == 330>自营分部<#elseif  orgVo.levelType == 430>众包城市组<#elseif orgVo.levelType == 770>城市代理城市组<#else>加盟商</#if></label>
                <div class="col-sm-5">
                    <#if is_org_manager?exists && is_org_manager == 1>
                        <input type="hidden" name="parentId" value="${orgVo.parentId}" />
                        <input type="text" class="form-control input-sm parsley-validated" value="${parentOrgVo.orgName !''}" disabled />
                    <#else >
                        <select class="form-control input-sm" id="parentId" name="parentId" <#if (is_org_manager?exists && is_org_manager == 1) || orgVo.orgChecked == 0>disabled = "disabled" </#if>>
                            <option class="other_level_org" value="0">请选择</option>
                            <#list orgs as org>
                                <option class="other_level_org" <#if org.orgId == orgVo.parentId > selected="selected" </#if> value="${org.orgId !''}"  >${org.orgName !''}</option>
                            </#list>
                        </select>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">所在城市</label>
                <div class="col-sm-5">
                    <#if (is_org_manager?exists && is_org_manager == 1) || orgVo.hasBindDeliveryArea>
                        <input type="hidden" name="orgCity" value="${orgVo.cityId}" />
                    </#if>

                    <select class="form-control input-sm" id="orgCity" name="orgCity"  <#if orgVo.hasBindDeliveryArea> disabled = "disabled" </#if> <#if (is_org_manager?exists && is_org_manager == 1) || orgVo.orgChecked == 0>disabled = "disabled" </#if>>
                        <#list cityList as city >
                            <option value="${city.city_id !''}" <#if city.city_id == orgVo.cityId > selected="selected" </#if> >${city.name !''}</option>
                        </#list>
                    </select>
                </div>
            </div>
            <#if orgVo.levelType == 230>
                <div class="form-group">
                    <label class="col-sm-2 control-label">模式</label>
                    <div class="col-sm-5">
                        <#if (is_org_manager?exists && is_org_manager == 1) || orgVo.orgChecked == 0>
                            <input type="hidden" name="isProxy" value="${orgVo.isProxy}" />
                        </#if>
                        <select class="form-control input-sm" id="isProxy" name="isProxy" <#if (is_org_manager?exists && is_org_manager == 1) || orgVo.orgChecked == 0>disabled</#if>>
                            <option value="0" <#if orgVo.isProxy?exists && orgVo.isProxy == 0>selected="selected" </#if> >加盟
                            </option>
                            <option value="1" <#if orgVo.isProxy?exists && orgVo.isProxy == 1>selected="selected" </#if> >代理
                            </option>
                        </select>
                    </div>
                </div>
            </#if>
            <div class="form-group">
                <label class="control-label col-sm-2">站点联系电话</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control input-sm parsley-validated" id="phone" name="phone" data-required="true" value="<#if orgVo?exists>${orgVo.phone !''}</#if>" <#if orgVo.orgChecked == 0>readonly</#if>>（仅供紧急联系使用，此账号无法登录烽火台）
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">营业时间</label>
                <div class="col-sm-5">
                    <#if orgVo.orgBusinessHourses??>
                        <#list orgVo.orgBusinessHourses as bh>
                            <div class="businessHourses">
                                <input type="text" class="form-control input-sm parsley-validated" name="businessHoursBegin" <#if (is_org_manager?exists && is_org_manager == 1) || orgVo.orgChecked == 0 || (bh?? && bh_index > 0)>readonly</#if> data-required="true" style="width:40%;display:inline" value="${bh.beginTime}">&nbsp;&nbsp;至&nbsp;&nbsp;
                                <input type="text" class="form-control input-sm parsley-validated" name="businessHoursEnd" <#if (is_org_manager?exists && is_org_manager == 1) || orgVo.orgChecked == 0 || (bh?? && bh_has_next)>readonly</#if> data-required="true" style="width:40%;display:inline" value="${bh.endTime}">
                                <#if !((is_org_manager?exists && is_org_manager == 1) || orgVo.orgChecked == 0)>
                                    <#if bh?? && bh_has_next>
                                        <a data-toggle="tooltip" title="" data-placement="top" target="_blank" id="add" href="javascript:;">
                                            <i class="fa fa-plus fa-lg"></i>
                                        </a>
                                        <p>（请按时间顺序填写，如9:00-23:00）</p>
                                    <#elseif bh?? && bh_index==0>
                                        <a data-toggle="tooltip" title="" data-placement="top" target="_blank" id="add" href="javascript:;">
                                            <i class="fa fa-plus fa-lg opration-icon"></i>
                                        </a>
                                        <p>（请按时间顺序填写，如9:00-23:00）</p>
                                    <#else>
                                        <a data-toggle="tooltip" title="" data-placement="top" target="_blank" id="delete" href="javascript:;">
                                            <i class="fa fa-minus fa-lg opration-icon"></i>
                                        </a>
                                        <p>（第二个时段为夜宵专用，只允许设置0:00开始）</p>
                                    </#if>
                                </#if>
                            </div>
                        </#list>
                        <#if orgVo.orgBusinessHourses?size lt 2>
                            <div class="businessHourses hide">
                                <input type="text" class="form-control input-sm parsley-validated" name="businessHoursBegin" data-required="true" style="width:40%;display:inline" >&nbsp;&nbsp;至&nbsp;&nbsp;
                                <input type="text" class="form-control input-sm parsley-validated" name="businessHoursEnd" data-required="true" style="width:40%;display:inline" >
                                <a data-toggle="tooltip" title="" data-placement="top" target="_blank"  id="delete" href="javascript:;">
                                    <i class="fa fa-minus fa-lg opration-icon"></i>
                                </a>
                                <p>（第二个时段为夜宵专用，只允许设置0:00开始）</p>
                            </div>
                        </#if>
                    <#else>

                        <#if !((is_org_manager?exists && is_org_manager == 1)|| orgVo.orgChecked == 0)>
                        <div class="businessHourses">
                            <input type="text" class="form-control input-sm parsley-validated" name="businessHoursBegin" data-required="true" style="width:40%;display:inline" >&nbsp;&nbsp;至&nbsp;&nbsp;
                            <input type="text" class="form-control input-sm parsley-validated" name="businessHoursEnd" data-required="true" style="width:40%;display:inline" >
                            <a data-toggle="tooltip" title="" data-placement="top" target="_blank"  id="add" href="javascript:;">
                                <i class="fa fa-plus fa-lg opration-icon"></i>
                            </a>
                            <p>（请按时间顺序填写，如9:00-23:00）</p>
                        <div>

                        </#if>
                    </#if>
                    <div style="color: red;">修改站点营业时间，系统会同步修改配送区域内商家的营业时间，请慎重调整</div>
                </div>
                </div>
                    <input type="hidden" value="" name="businessHourses" id="businessHourses" />
                    <input type="hidden" value="/org/<#if is_org_manager?exists && is_org_manager == 1>partner/</#if>stations" name="returnUrl" />
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <#if orgVo.orgChecked?exists && orgVo.orgChecked != 0 && orgVo.status==1><button type="submit" class="btn btn-danger" id="submit">保存</button></#if>
                            <#if is_org_manager?exists && is_org_manager == 1>
                                <a href="/org/partner/stations" class="btn btn-success">返回</a>
                            <#else>
                                <a href="/org/stations" class="btn btn-success">返回</a>
                            </#if>
                        </div>
                    </div>
        </form>
    </div>
</div>
<#else >
<div id="main-container">
    <#if auditOrganizationVo?? && auditOrganizationVo.comment??><h4
            style="color:red;">${auditOrganizationVo.comment!''}</h4></#if>
    <h4><#if orgVo?exists>${orgVo.orgName !''}(ID:${orgVo.orgId!''})</#if></h4>
    <#import "/organization/station_nav.ftl" as n>
    <@n.station_nav active=1 orgOperations=orgVo.orgOperations orgVo=orgVo />
    <div class="panel panel-default">
        <form method="post" class="form-horizontal" id="formValidate1" data-validate="parsley" novalidate=""
              action="/org<#if is_org_manager?exists && is_org_manager == 1>/partner</#if>/doUpdate">
            <input type="hidden" value="${orgVo.orgType}" name="orgType" id="orgType"/>
            <input type="hidden" name="orgId" value="${orgVo.orgId}"/>
            <input type="hidden" name="levelType" value="${orgVo.levelType}" id="levelType"/>

            <div class="form-group">
                <label class="control-label col-sm-2">站点名称</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control input-sm parsley-validated" disabled
                           <#if (is_org_manager?exists && is_org_manager == 1) || orgVo.orgChecked == 0>readonly</#if>
                           id="orgName" name="orgName" data-required="true"
                           value="<#if orgVo?exists>${orgVo.orgName !''}</#if>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2"><#if orgVo.levelType == 330>自营分部<#elseif  orgVo.levelType == 430>
                    众包城市组<#elseif orgVo.levelType == 770>城市代理城市组<#else>加盟商</#if></label>

                <div class="col-sm-5">
                    <#if is_org_manager?exists && is_org_manager == 1>
                        <input type="hidden" name="parentId" value="${orgVo.parentId}"/>
                        <input type="text" class="form-control input-sm parsley-validated" value="${parentOrgVo.orgName !''}" disabled />
                    <#else >
                        <input type="text" class="form-control input-sm parsley-validated" readonly data-required="true" value="${parentOrgVo.orgName !''}">
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">所在城市</label>
                <div class="col-sm-5">
                    <#if (is_org_manager?exists && is_org_manager == 1) || orgVo.hasBindDeliveryArea >
                        <input type="hidden" name="orgCity" value="${orgVo.cityId}"/>
                    </#if>
                    <select class="form-control input-sm" id="orgCity" name="orgCity" <#if orgVo.hasBindDeliveryArea> disabled = "disabled" </#if> disabled>
                        <#list cityList as city >
                            <option value="${city.city_id !''}" <#if city.city_id == orgVo.cityId >
                                    selected="selected" </#if> >${city.name !''}</option>
                        </#list>
                    </select>
                </div>
            </div>
            <#if orgVo.levelType == 230>
                <div class="form-group">
                    <label class="col-sm-2 control-label">模式</label>

                    <div class="col-sm-5">
                        <#if (is_org_manager?exists && is_org_manager == 1) || orgVo.orgChecked == 0>
                            <input type="hidden" name="isProxy" value="${orgVo.isProxy}"/>
                        </#if>
                        <select class="form-control input-sm" id="isProxy" name="isProxy" disabled >
                            <option value="0"
                                    <#if orgVo.isProxy?exists && orgVo.isProxy == 0>selected="selected" </#if> >加盟
                            </option>
                            <option value="1"
                                    <#if orgVo.isProxy?exists && orgVo.isProxy == 1>selected="selected" </#if> >代理
                            </option>
                        </select>
                    </div>
                </div>
            </#if>
            <div class="form-group">
                <label class="control-label col-sm-2">站点联系电话</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control input-sm parsley-validated" id="phone" name="phone" disabled
                           data-required="true" value="<#if orgVo?exists>${orgVo.phone !''}</#if>"
                           <#if orgVo.orgChecked == 0>readonly</#if>>（仅供紧急联系使用，此账号无法登录烽火台）
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">营业时间</label>

                <div class="col-sm-5">
                    <#if orgVo.orgBusinessHourses??>
                        <#list orgVo.orgBusinessHourses as bh>
                            <div class="businessHourses">
                                <input type="text" class="form-control input-sm parsley-validated" disabled
                                       name="businessHoursBegin"
                                       <#if (is_org_manager?exists && is_org_manager == 1) || orgVo.orgChecked == 0 || (bh?? && bh_index > 0)>readonly</#if>
                                       data-required="true" style="width:40%;display:inline" value="${bh.beginTime}">&nbsp;&nbsp;至&nbsp;&nbsp;
                                <input type="text" class="form-control input-sm parsley-validated" disabled
                                       name="businessHoursEnd"
                                       <#if (is_org_manager?exists && is_org_manager == 1) || orgVo.orgChecked == 0 || (bh?? && bh_has_next)>readonly</#if>
                                       data-required="true" style="width:40%;display:inline" value="${bh.endTime}">
                            </div>
                        </#list>
                    </#if>
                    <div style="color: red;">修改站点营业时间，系统会同步修改配送区域内商家的营业时间，请慎重调整</div>

                </div>
                </div>
                    <input type="hidden" value="" name="businessHourses" id="businessHourses"/>
                    <input type="hidden"
                           value="/org/<#if is_org_manager?exists && is_org_manager == 1>partner/</#if>stations"
                           name="returnUrl"/>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <#if is_org_manager?exists && is_org_manager == 1>
                                <a href="/org/partner/stations" class="btn btn-success">返回</a>
                            <#else>
                                <a href="/org/stations" class="btn btn-success">返回</a>
                            </#if>

                        </div>
                    </div>
        </form>
    </div>
</div>
</#if>
<script type="text/javascript">
	document.title = '编辑站点';
    $('#orgCity').select2();
    $('#parentId').select2();
</script>