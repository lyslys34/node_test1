<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/organization/org_create.js</content>

<div id="main-container">
  <div class="panel panel-default">
    <form method="post" class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/org/doCreate">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">组织名称</label>
              <input type="text" class="form-control input-sm parsley-validated" id="orgName" name="orgName" data-required="true" >
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">组织类型</label>
                <select class="form-control input-sm" id="orgType" name="orgType">
                    <#list orgTypeList as type>
                        <option value="${type.type !''}">${type.comment !''}</option>
                    </#list>
                </select>
            </div>
          </div>
          <div id="div_level_type" hidden class="col-md-4">
            <div class="form-group">
              <label class="control-label">组织层级</label>
                <select class="form-control input-sm" id="levelType" name="levelType">
                        <!-- <option value="0">请选择组织层级</option> -->
                        <option class="type_proxy" value="220">加盟商</option>
                        <option class="type_proxy" value="230">加盟站</option>
                        <option class="type_self" value="330">自营站</option>
                </select>
            </div>
          </div>
        </div>

          <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">组织城市</label>
                      <#--input type="text" class="form-control input-sm parsley-validated js_city_name" name="cityName" id="cityName" data-minlength="8" value=""/-->
                      <#--input type="hidden" name="orgCity" id="orgCity" class="js_city_id" value="${orgCity!""}"/-->
                      <select class="form-control input-sm" id="orgCity" name="orgCity">
                  	  	<#list cityList as city >
                  	  	    <option value="${city.city_id !''}" >${city.name !''}</option>
                  	  	</#list>
                  	  </select>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">上级组织</label>
                      <select class="form-control input-sm" id="parentId" name="parentId">
                          <option class="other_level_org" value="0">请选择上级组织</option>
                          <#list orgs as org>
                              <option class="other_level_org" value="${org.orgId !''}"  >${org.orgName !''}</option>
                          </#list> 
                      </select>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">组织负责人</label>
                      <input type="text" class="form-control input-sm parsley-validated" id="orgAdmin" name="orgAdmin" data-required="true" >
                  </div><!-- /form-group -->
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">手机号</label>
                      <input type="text" class="form-control input-sm parsley-validated" id="adminNo" name="adminNo" data-required="true" >
                  </div>
              </div>
          </div>

          <div class="row" id = "extraInfo">
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">是否和美团配送结算邮资</label>
                      <select class="form-control input-sm" id="isProxy" name="isProxy">
                          <option value="0">是</option>
                          <option value="1">否</option>
                      </select>
                 </div>
              </div>

              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">组织简介</label>
                      <textarea rows="6" class="form-control input-sm" id="orgIntroduction" name="orgIntroduction"></textarea>
                  </div>
              </div>

          </div>

      </div>
      <div class="panel-footer text-right">
        <button type="submit" class="btn btn-success">保存</button>
      </div>
    </form>
  </div>
</div>
<script>
	document.title = '新建组织';
	// $('#orgType').select2();
	$('#orgCity').select2();
	$('#parentId').select2();
</script>