<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/organization/audit_jmstation_detail.js</content>
<link href="/static/css/lib/bootstrap-nav-wizard.css" rel="stylesheet">
<style>
    input[type="radio"] {
        opacity: 0
    }
</style>
<div id="main-container">

    <a href="#auditOrgDisagreeModal" role="button" data-toggle="modal" class="btn btn-danger">驳回请求</a>
    <a href="#auditOrgAgreeModal" role="button" data-toggle="modal" class="btn btn-success">审核通过</a>

    <h4>${org.orgName !''}</h4>

    <br/>

    <form class="no-margin" id="formValidate2" data-validate="parsley" novalidate="" method="post" action="/audit/auditOrg?orgAuditId=${org.id!}&checked=1">
        <div class="modal fade" id="auditOrgAgreeModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>审核通过后，站点将与美团配送达成合作关系</h4>
                        <div class="modal-footer">
                            <input type="submit" value="确认通过" class="btn btn-success btn-sm" />
                            <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true">再想想</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form class="no-margin" id="formValidate3" data-validate="parsley" novalidate="" method="post" action="/audit/auditOrg?orgAuditId=${org.id!}&checked=2">
        <div class="modal fade" id="auditOrgDisagreeModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>${org.orgName!""}将要驳回申请，确认?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>请填写驳回原因(必填)</label><div id="alert_error"></div>
                            <textarea rows="6" class="form-control input-sm" id="comment" name="comment" placeholder="驳回原因"></textarea>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" id="reject" value="确认驳回" class="btn btn-danger btn-sm" />
                            <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">再想想</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>


    <#--<h4>基本信息</h4>-->
    <div class="panel panel-default">
        <form method="post" class="form-horizontal" id="formValidate1" data-validate="parsley" novalidate="" action="/org/doCreate">
            <input type="hidden" value="" name="orgType" id="orgType" />
            <div class="form-group">
                <label class="control-label col-sm-2">站点类型</label>
                <div class="col-sm-5">
                    <select class="form-control input-sm" id="levelType" name="levelType" disabled="disabled">
                        <#if org.levelType == 230><option value="">加盟站</option></#if>
                        <#if org.levelType == 330><option value="">自营站</option></#if>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">站点名称</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control input-sm parsley-validated"  disabled="disabled" id="orgName" name="orgName" value="${org.orgName !''}" data-required="true" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" id="parentLabel">
                    <#if org.levelType == 230>加盟商</#if>
                    <#if org.levelType == 330>自营城市组</#if>
                </label>
                <div class="col-sm-5">
                    <select class="form-control input-sm" id="parentId" name="parentId"  disabled="disabled">
                        <option class="other_level_org" value="">${org.parentOrgName !''}</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">所在城市</label>
                <div class="col-sm-5">
                    <select class="form-control input-sm" id="orgCity" name="orgCity"  disabled="disabled">
                        <option value="">${org.cityName !''}</option>
                    </select>
                </div>
            </div>
            <#if org.levelType == 230>
            <div class="form-group">
                <label class="col-sm-2 control-label">模式</label>
                <div class="col-sm-5">
                    <select class="form-control input-sm" id="isProxy" name="isProxy" disabled="disabled">
                        <option value="0" <#if org.isProxy?exists && org.isProxy == 0>selected="selected" </#if> >加盟
                        </option>
                        <option value="1" <#if org.isProxy?exists && org.isProxy == 1>selected="selected" </#if> >代理
                        </option>
                    </select>
                </div>
            </div>
            </#if>
            <div class="form-group">
                <label class="control-label col-sm-2">站点联系电话</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control input-sm parsley-validated" id="phone" name="phone" data-required="true" value="${org.emergencyPhone !''}" disabled="disabled">（仅供紧急联系使用，此账号无法登录烽火台）
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">营业时间</label>
                <div class="col-sm-5">
                    <div class="businessHourses">
                        <#if org.orgBusinessHourses?exists && (org.orgBusinessHourses?size > 0)>
                            <#list org.orgBusinessHourses as businessHour>
                                <input type="text" class="input-sm parsley-validated" name="businessHoursBegin" data-required="true" value="${businessHour.beginTime !''}" style="width:40%" disabled="disabled">&nbsp;&nbsp;至&nbsp;&nbsp;
                                <input type="text" class="input-sm parsley-validated" name="businessHoursEnd" data-required="true" value="${businessHour.endTime !''}" style="width:40%" disabled="disabled">
                                <a data-toggle="tooltip" title="" data-placement="top" target="_blank"  name="modifyBusinessHours" href="javascript:;">
                                    <i class="fa fa-plus fa-lg opration-icon"></i>
                                </a>
                                <div>
                                    （请按时间顺序填写，如9:00-23:00）
                                </div>
                            </#list>
                        </#if>
                    </div>
        </form>
    </div>
</div>
<script>
    document.title = '合作伙伴审核';
</script>