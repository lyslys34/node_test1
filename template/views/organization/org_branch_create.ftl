<#include "join_config.ftl">
<#include "../widgets/sidebar.ftl">
<style>
    .cred { color: #b22222; }
    .lh30 { line-height: 30px; }
    .tl {
        text-align: left !important;
    }
    input[type="radio"] {
        opacity: 0
    }

</style>
<div id="main-container">
    <form method="post" class="form-horizontal" id="formValidate1" data-validate="parsley" novalidate="" >
        <div class="panel panel-default">
            <div class="panel panel-heading">
                基本信息
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>自营分部名称</label>

                <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" id="orgName" name="orgName" data-required="true" maxlength="25">
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>自营片区</label>

                <div class="col-sm-4">
                    <select class="form-control input-sm" id="parentId" name="parentId">
                        <option class="other_level_org" value="0">请选择上级组织</option>
                    <#if orgs?exists>
                        <#list orgs as org>
                            <option class="other_level_org" value="${org.orgId !''}">${org.orgName !''}</option>
                        </#list>
                    </#if>
                    </select>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>所在城市</label>

                <div class="col-sm-4">
                    <select class="form-control input-sm" id="orgCity" name="orgCity">
                    <#if cityList?exists>
                        <#list cityList as city >
                            <option value="${city.city_id !''}">${city.name !''}</option>
                        </#list>
                    </#if>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><span class="cred">*</span>分部联系电话</label>

                <div class="col-sm-4">
                    <input type="text" class="form-control input-sm parsley-validated" id="emergencyPhone"
                           name="emergencyPhone" data-required="true" maxlength="18">
                    <span class="help-block"></span>
                </div>
                <div class="tl col-sm-4 control-label" >(仅供紧急联系使用，此账号无法登录烽火台)
                </div>

            </div>

            <div class="form-group">
                <div class="col-sm-8 text-center">
                    <input type="button" class="btn btn-danger" value="提交" id="js_save" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="button" class="btn btn-success" value="取消" id="js_cancel" />
                </div>
            </div>
        </div>
    </form>
</div>
<@commonPop></@commonPop>
<script type="text/javascript" src="${staticPath}/js/page/utils.js?ver=${staticVersion !''}"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/static/js/lib/jQuery-File-Upload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="${staticPath}/js/page/organization/org_branch_create.js"></script>
<script>
    document.title = '新建自营分部';
</script>