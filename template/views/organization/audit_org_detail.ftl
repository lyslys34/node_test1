<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/organization/audit_org_detail.js</content>

<style>
    input[type="radio"] {
        opacity: 0
    }
</style>


<div id="main-container">
  <form method="post" class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/myorg/editorgaudit?orgAuditId=${audit.id!}">
  <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">组织名称</label>
              <input type="text" value="${audit.orgName!""}" <#if editable == false > disabled </#if> class="form-control input-sm parsley-validated" id="orgName" name="orgName" data-required="true" >
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">组织类型</label>
                <select class="form-control input-sm inline" disabled >
                    <option value="1" <#if audit.orgType == 1> selected="selected" </#if>>自建</option>
                    <option value="2" <#if audit.orgType == 2> selected="selected" </#if>>加盟</option>
                    <option value="3" <#if audit.orgType == 3> selected="selected" </#if>>驻店三方</option>
                    <option value="4" <#if audit.orgType == 4> selected="selected" </#if>>众包</option>
                </select>
                <input type="hidden" value="${audit.orgType}" id="orgType" name="orgType">
            </div>
          </div>
          <div id="div_level_type" <#if audit.orgType gt 2 >hidden</#if> class="col-md-4">
            <div class="form-group">
              <label class="control-label">组织层级</label>
                <select class="form-control input-sm" disabled id="levelType" name="levelType">
                        <option value="${audit.levelType!''}" selected="selected" >${levelTypeName!''}</option>
                </select>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">组织城市</label>
                    <#if editable == true>
                        <select name="orgCity" class="form-control input-sm select2">
                            <option value="-1" <#if audit.orgCity < 1>selected="selected"</#if> >选择城市</option>
                            <#if cityList?exists && (cityList?size >0)>
                                <#list cityList as c>
                                    <option value="${c.city_id ! -1}" <#if audit.orgCity == c.city_id>selected="selected"</#if> >${c.name ! ''}</option>
                                </#list>
                            </#if>
                        </select>
                    <#else>
                        <input type="text" value="${audit.cityName!""}" <#if editable == false > disabled </#if> class="form-control input-sm parsley-validated" id="cityName" name="cityName" data-required="true" >
                    </#if>
                </div>
            </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">上级组织</label>
                        <select class="form-control input-sm" id="parentId" name="parentId" <#if editable == false > disabled </#if>>
                            <option value='0'>请选择上级组织</option>
                            <#if orgs?exists && (orgs?size > 0)>
                                <#list orgs as org>
                                    <option value="${org.orgId !''}" <#if org.orgId == audit.parentId > selected="selected" </#if> >${org.orgName !''}</option>
                                </#list>
                            </#if>
                        </select>
                    </div>
                </div>
        </div>


          <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">组织负责人</label>
                      <input type="text" value="${audit.userName!""}" <#if editable == false > disabled </#if> class="form-control input-sm parsley-validated" id="orgAdmin" name="orgAdmin" data-required="true" >
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">登录手机号</label>
                      <input type="text" value="${audit.userMobile!""}" <#if editable == false > disabled </#if> class="form-control input-sm parsley-validated" id="adminNo" name="adminNo" data-required="true" >
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">是否和美团配送结算邮资</label>
                      <select class="form-control input-sm" id="isProxy" name="isProxy" <#if editable == false > disabled </#if>>
                          <option value="0" <#if audit?exists && audit.isProxy == 0>selected="selected"</#if>>是</option>
                          <option value="1" <#if audit?exists && audit.isProxy == 1>selected="selected"</#if>>否</option>
                      </select>
                  </div>
              </div>

              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">组织简介</label>
          			  <textarea rows="6" <#if editable == false > disabled </#if> class="form-control input-sm" id="orgDes" name="orgDes">${audit.orgDes!""}</textarea>
                  </div>
              </div>
          </div>
          <#if audit.comment??><div class="alert alert-danger" role="alert">驳回原因:${audit.comment!""}</div></#if>
      </div>
      <#if editable == true >
      <div class="panel-footer text-center">
        <button type="submit" class="btn btn-success">保存</button>
      </div>
      </#if>
      </form>
      <#if checked == true >
      <div class="panel-footer text-center">
        <a href="#auditOrgAgreeModal" role="button" data-toggle="modal" class="btn btn-success">审核通过</a>
        <a href="#auditOrgDisagreeModal" role="button" data-toggle="modal" class="btn btn-danger">驳回</a>
      </div>
      </#if>
      
      <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post" action="/audit/auditOrg?orgAuditId=${audit.id!}&checked=2">
      	<div class="modal fade" id="auditOrgDisagreeModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>${audit.orgName!""}将要驳回申请，确认?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>请填写驳回原因(必填)</label><div id="alert_error"></div>
                            <textarea rows="6" class="form-control input-sm" id="comment" name="comment" placeholder="驳回原因"></textarea>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" value="确认驳回" class="btn btn-danger btn-sm" />
                            <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">再想想</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </form>
        
     <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post" action="/audit/auditOrg?orgAuditId=${audit.id!}&checked=1">
        <div class="modal fade" id="auditOrgAgreeModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>${audit.orgName!""}将要通过审核，确认?</h4>
                        <div class="modal-footer">
                            <input type="submit" value="确认通过" class="btn btn-success btn-sm" />
                            <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true">再想想</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </form>
     
  </div>
</div>
<#if editable == true >
	<script>document.title = '审核信息修改'; $('.select2').select2(); $('#parentId').select2();</script>
<#elseif checked == true >
	<script>document.title = '组织审核';</script>
<#else>
	<script>document.title = '组织详情';</script>
</#if>

