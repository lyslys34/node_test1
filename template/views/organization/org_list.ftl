<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/organization/org_list.js</content>

<link rel="stylesheet" type="text/css" href="/static/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />

<style type="text/css">
.ui-autocomplete{
    z-index: 2000;
}
a[name='aname']:hover {color:#7FC7A6}
</style>

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post" action="/org/list?">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">所在城市</label>
                            <select class="form-control input-sm" id="cityId" name="cityId">
                                <option value = "0">全部</option>
                                <#if cityList?exists && (cityList?size > 0)>
                                    <#list cityList as city>
                                        <option value="${city.city_id}" <#if cityId?exists && cityId == city.city_id>selected</#if>>${city.name}</option>
                                    </#list>
                                </#if>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">组织类型</label>
                            <select class="form-control input-sm" id="orgType" name="orgType">
                                <option value="0"<#if searchOrgType?exists && searchOrgType == 0> selected="selected" </#if>>全部</option>
                                <#if orgTypeList?exists>
                                    <#list orgTypeList as ot>
                                        <option value="${ot.type !'0'}" <#if searchOrgType?exists && searchOrgType == ot.type> selected="selected" </#if>>${ot.comment !''}</option>
                                    </#list>
                                </#if>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">组织名称</label>
                            <select class="form-control input-sm" id="orgId" name="orgId">
                                <option value="0">全部</option>
                                <#list orgs as org>
                                    <option value="${org.orgId}" <#if orgId?exists && orgId == org.orgId>selected</#if>>${org.orgName}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3" style="padding-top:18px">
                        <button type="submit" class="btn btn-success">查询</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
            </div>
            <input type = "hidden" name="source" value="submit"/>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <div id="breadcrumb">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="/org/list"> 全部</a></li>
                <#if parent??>
                    <#list parent as org>
                        <li class="active"><a href="/org/list?parent=${org.orgId}">${org.orgName}</a></li>
                    </#list>
                </#if>
            </ul>
        </div><!-- /breadcrumb-->

        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>组织名称</th>
                <th>组织ID</th>
                <th>所在城市</th>
                <th>组织类型</th>
                <th>组织层级</th>
                <#--<th>组织负责人数</th>-->
                <#--<th>组织电话</th>-->
                <th>组织人员数</th>
                <th>服务商家数</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->

            <#list result.data.data as org>
                <tr>
                    <#if org.isLeaf()>
                        <td>${org.orgName}</td>
                    <#else>
                        <td><a name="aname" style="" href="/org/list?parent=${org.orgId}">${org.orgName}</a></td>
                    </#if>
                    <td>${org.orgId!''}</td>
                    <td>${org.cityName!''}</td>
                    <td>
                        <#if 1 == org.orgType>自建
                        <#elseif 2 == org.orgType>加盟
                        <#elseif 3 == org.orgType>驻店三方
                        <#elseif 4 == org.orgType>众包
                        <#elseif 5 == org.orgType>角马
                        <#else>
                        </#if>
                    </td>
                    <td>${org.levelTypeName!''}</td>
                    <#--<td>${org.leaderCount!0}</td>-->
                    <#--<td>${org.orgPhone!''}</td>-->
                    <td><a href="/rider/list?orgId=${org.orgId}" target="_blank">${org.riderCount!0}</a></td>
                    <td><a href="/org/merchantlist?orgId=${org.orgId}" target="_blank">${org.poiCount!0}</a></td>
                    <td>
                        <#if org.orgType!=0>
                            <#if ownOrgIds?exists && ownOrgIds?seq_contains(org.orgId)&&(!(canOperateOrg?exists))>
                                <#if org.hasBindDeliveryArea>
                                    <a data-toggle="tooltip" title="解绑配送区域" data-placement="top" class="unbindArea_link" rel="${org.orgId}" >
                                        <i class="fa fa-link fa-lg opration-icon"></i>
                                    </a>&nbsp;&nbsp;
                                <#else>
                                    <a data-toggle="tooltip" title="绑定配送区域" data-placement="top" class="bindArea_link" rel="${org.orgId}" >
                                        <i class="fa fa-link fa-lg opration-icon"></i>
                                    </a>&nbsp;&nbsp;
                                </#if>
                                <a data-toggle="tooltip" title="人员列表" data-placement="top" href="/rider/list?orgId=${org.orgId}" target="_blank">
                                    <i class="fa fa-list-ol fa-lg opration-icon"></i>
                                </a>&nbsp;&nbsp;
                                <a data-toggle="tooltip" title="操作记录" data-placement="top" href="/log/staff?orgId=${org.orgId}" target="_blank">
                                    <i class="fa fa-history fa-lg opration-icon"></i>
                                </a>&nbsp;&nbsp;
                            <#else>
                                <#if canOperateOrg?exists && canOperateOrg==1>
                                    <#if org.hasBindDeliveryArea>
                                        <a data-toggle="tooltip" title="解绑配送区域" data-placement="top" class="unbindArea_link" rel="${org.orgId}" >
                                            <i class="fa fa-link fa-lg opration-icon"></i>
                                        </a>&nbsp;&nbsp;
                                        <#else>
                                            <a data-toggle="tooltip" title="绑定配送区域" data-placement="top" class="bindArea_link" rel="${org.orgId}" >
                                                <i class="fa fa-link fa-lg opration-icon"></i>
                                            </a>&nbsp;&nbsp;
                                    </#if>
                                    <a data-toggle="tooltip" title="服务商列表" data-placement="top" href="/org/merchantlist?orgId=${org.orgId}" target="_blank">
                                        <i class="fa fa-list-ul fa-lg opration-icon"></i>
                                    </a>&nbsp;&nbsp;
                                <#else>
                                    <#if org.hasBindDeliveryArea>
                                        <a data-toggle="tooltip" title="解绑配送区域" data-placement="top" class="unbindArea_link" rel="${org.orgId}" >
                                            <i class="fa fa-link fa-lg opration-icon"></i>
                                        </a>&nbsp;&nbsp;
                                        <#else>
                                            <a data-toggle="tooltip" title="绑定配送区域" data-placement="top" class="bindArea_link" rel="${org.orgId}" >
                                                <i class="fa fa-link fa-lg opration-icon"></i>
                                            </a>&nbsp;&nbsp;
                                    </#if>
                                    <a data-toggle="tooltip" title="服务商列表" data-placement="top" href="/org/merchantlist?orgId=${org.orgId}" target="_blank">
                                        <i class="fa fa-list-ul fa-lg opration-icon"></i>
                                    </a>&nbsp;&nbsp;

                                    <a data-toggle="tooltip" title="编辑" data-placement="top" href="/org/update/${org.orgId}" target="_blank">
                                        <i class="fa fa-edit fa-lg opration-icon"></i>
                                    </a>&nbsp;&nbsp;
                                    <a data-toggle="tooltip" title="人员列表" data-placement="top" href="/rider/list?orgId=${org.orgId}" target="_blank">
                                        <i class="fa fa-list-ol fa-lg opration-icon"></i>
                                    </a>&nbsp;&nbsp;
                                    <#if org.orgType == 2 && org.isProxy == 0>
                                        <a data-toggle="tooltip" title="结算设置" data-placement="top" href="/settle/orgsetting?orgId=${org.orgId}" target="_blank">
                                            <i class="fa fa-credit-card fa-lg opration-icon"></i>
                                        </a>&nbsp;&nbsp;
                                    </#if>
                                    <a data-toggle="tooltip" title="操作记录" data-placement="top" href="/log/staff?orgId=${org.orgId}" target="_blank">
                                        <i class="fa fa-history fa-lg opration-icon"></i>
                                    </a>&nbsp;&nbsp;
                                </#if>
                                <#if org.hasBindDeliveryArea>
                                    <a data-toggle="tooltip" title="配送区域" data-placement="top" href="/delivery/orgArea?orgId=${org.orgId}" target="_blank">
                                        <i class="fa fa-truck fa-lg opration-icon"></i>
                                    </a>
                                </#if>
                            </#if>
                        </#if>

                    </td>
                </tr>
            </#list>

            </tbody>
        </table>
        <div style="float: left; padding: 10px; 15px"><span>共${recordCount!'0'}项</span></div>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/org/list"/>
    </#if>
        
    </div>

    <!--<div class="modal fade" id="unBindDeliveryAreaModal">-->
        <!--<div class="modal-dialog">-->
            <!--<div class="modal-content">-->
                <!--<div class="modal-body">-->
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                    <!--<div id="alert_un_bind_area_error"></div>-->
                    <!--<h4>解绑配送区域</h4>-->
                    <!--<label>请输入配送区域名称：</label>-->
                    <!--<input type="text" class="form-control input-sm js_un_bind_area_name" name="unbindAreaName" id="unbindAreaName" data-minlength="8" value=""/>-->
                    <!--<input type="hidden" class="js_un_bind_area_id" name="unbindAreaId" id="unbindAreaId" value=""/>-->
                    <!--&lt;!&ndash;<p style="color:red">注意：新绑定站点将会覆盖原有已绑定站点；如果输入为空，则解绑已有站点</p>&ndash;&gt;-->
                    <!--<input type="hidden" name="unbindOrgId" id="unbindOrgId"/>-->
                    <!--<input type="hidden" id="isCheckedUnbind" value="0"/>-->
                    <!--<div class="modal-footer">-->
                        <!--<input type="submit" id="unbindAreaSubmit" value="确认" class="btn btn-danger btn-sm" />-->
                        <!--<button class="btn btn-success btn-sm" id="cancelUnbindArea">取消</button>-->
                    <!--</div>-->
                <!--</div>-->
            <!--</div>-->

        <!--</div>-->
    <!--</div>-->

    <div class="modal fade" id="bindDeliveryAreaModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" id ="bindAreaCloseButton" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div id="alert_bind_area_error"></div>
                    <h4 style="border-bottom: 1px solid #e5e5e5; padding-bottom: 7px;">选择待绑定的配送区域</h4>
                    <label>城市</label>
                    <input type="text" class="form-control input-sm js_bind_city_name" name="bindCityName" id="bindCityName" data-minlength="8" value=""/>
                    <input type="hidden" class="js_bind_city_id" name="bindCityId" id="bindCityId" value=""/>
                    <label>请输入配送区域名称：</label>
                    <input type="text" class="form-control input-sm js_bind_area_name" name="bindAreaName" id="bindAreaName" data-minlength="8" value=""/>
                    <input type="hidden" class="js_bind_area_id" name="bindAreaId" id="bindAreaId" value=""/>
                    <div>
                        <p id="modelChangeTips" style="color: red;width: 44%;margin: 20px auto;line-height: 22px;display:none;">
                            此配送区域已绑定其他站点，如继续绑定，则全部站点调度模式改为“推抢结合”，次日生效。
                        </p>
                    </div>
                    <input type="hidden" name="bindOrgId" id="bindOrgId"/>
                    <input type="hidden" id="isChecked" value="0"/>
                    <div class="modal-footer">
                        <input type="submit" id="bindAreaSubmit" value="确认" class="btn btn-danger btn-sm" />
                        <button class="btn btn-success btn-sm" id="cancelBindArea">取消</button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="showBindAreaRes">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="resMsg">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="showUnbindAreaRes">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="unBindResMsg">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="unBindAreaTipsModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 style="border-bottom: 1px solid #e5e5e5; padding-bottom: 5px;">
                        <i class = "fa fa-info-circle fa-lg opration-icon"></i>&nbsp;提示
                    </h4>
                    <p  style = "margin: 20px auto;width: 68%;font: initial;">
                        解绑后，此配送区域无其他绑定站点，商家将被全部置休，确定此操作请点击“确定”完成解绑功能。
                    </p>
                    <input type="hidden" id="unBindOrgId" value="0"/>
                    <input type="hidden" id="unBindAreaId" value="0"/>
                    <div class="modal-footer" style="border-top: none">
                        <input type="submit" id="unBindAreaResubmit" value="确认" class="btn btn-danger btn-sm" />
                        <button class="btn btn-success btn-sm" id="cancelunBindArea">取消</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
    document.title = '组织管理';
    $('#cityId').select2();
    $('#orgId').select2();
</script>
