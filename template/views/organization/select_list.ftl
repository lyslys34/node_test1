<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<script type="text/javascript" src="${staticPath}/js/page/organization/merchant.js"></script>

<div id="main-container">
<#-- <div class="panel panel-default">
     <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="">

         <div class="panel-footer text-right">
         <#--<button type="submit" class="btn btn-success">查询</button>
                <a href="#addPartnerModal" role="button" data-toggle="modal" class="btn btn-success">添加商家</a>
            </div>
        </form>

    </div>
 -->
    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>商家名</th>
                <th>商家POI</th>
                <th>商家电话</th>
                <th>商家地址</th>
                <th>合作状态</th>
            </tr>
            </thead>
            <tbody>
            <#if result??>
                <#list result.data.data as merchant>
                <tr>

                    <td>${merchant.name}</td>
                    <td>${merchant.id}</td>
                    <td>${merchant.phone}</td>
                    <td>${merchant.address}</td>
                    <td><span class="label label-success">合作中</span></td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#--<#if recordCount??>-->
        <#--<@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/org/merchantlist"/>-->
    <#--</#if>-->
       <#-- <div class="modal fade" id="addPartnerModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>新建商家</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>请输入商家POI</label><div id="alert_error"></div>
                            <input type="text" class="form-control input-sm" placeholder="商家POI" id="merchant" name="merchant">
                            <input type="hidden" id="orgId" name="orgId" value="${orgId}">
                        </div><!-- /form-group
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">取消</button>
                            <input type="button" value="提交" class="btn btn-danger btn-sm" id="B_addMer"/>
                        </div>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog
        </div>
       -->

    </div>
</div>
<script>document.title = '代购商家列表';</script>
