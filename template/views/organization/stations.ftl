<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">
<link rel="stylesheet" type="text/css" href="/static/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />
<style type="text/css">
    a:hover { text-decoration: underline; font-weight: bold; color:#7FC7A6; }
    .ui-autocomplete{
        z-index: 2000;
    }
</style>
<content tag="javascript">/organization/stations.js</content>
<div id="main-container">
    <div id="alert_error"></div>
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="get" action="/org/stations">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">城市</label>
                            <select class="form-control input-sm" id="cityId" name="cityId">
                                <option value = "0">全部</option>
                                <#if cityList?exists && (cityList?size > 0)>
                                    <#list cityList as city>
                                        <option value="${city.city_id}" <#if RequestParameters.cityId ?? &&RequestParameters.cityId == city.city_id?string>selected</#if>>${city.name}</option>
                                    </#list>
                                </#if>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">站点类型</label>
                            <select class="form-control input-sm" id="orgType" name="orgType">
                                <option value="0"<#if RequestParameters.orgType?? && RequestParameters.orgType == "0"> selected="selected" </#if>>全部</option>
                                <#if visbelLevelTypes?exists>
                                    <#list visbelLevelTypes?keys as key>
                                        <option <#if RequestParameters.orgType?? && RequestParameters.orgType == key >selected="selected" </#if> value=${key}>
                                            ${visbelLevelTypes[key]}
                                        </option>
                                    </#list>
                                </#if>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">审核状态</label>
                            <select class="form-control input-sm" id="checked" name="checked">
                                <option value="-1" <#if RequestParameters.checked?? && RequestParameters.checked == '-1'>selected</#if>>全部</option>
                                <option value="0" <#if RequestParameters.checked?? && RequestParameters.checked == '0'>selected</#if>>待审核</option>
                                <option value="1" <#if RequestParameters.checked?? && RequestParameters.checked == '1'>selected</#if>>审核通过</option>
                                <option value="2" <#if RequestParameters.checked?? && RequestParameters.checked == '2'>selected</#if>>驳回</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">站点名称</label>
                            <select class="form-control input-sm" id="orgId" name="orgId">
                                <option value="0">全部</option>
                                <#if orgList??>
                                <#list orgList as org>
                                    <option value="${org.orgId}" <#if RequestParameters.orgId?? && RequestParameters.orgId == org.orgId?string>selected</#if>>${org.orgName}</option>
                                </#list>
                                </#if>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">站点ID</label>
                            <input type="text" class="form-control input-sm" name="orgId2" value="${RequestParameters.orgId2!''}" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">合作状态</label>
                            <select class="form-control input-sm" id="org_status" name="status">
                                <option value="-1">全部</option>
                                <option value="1" <#if status?? && status == 1>selected</#if>>合作</option>
                                <option value="0" <#if status?? && status == 0>selected</#if>>不合作</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3" style="padding-top:18px">
                        <button type="submit" class="btn btn-success">查询</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="/org/createStation" role="button" data-toggle="modal" class="btn btn-success">新建站点</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
            </div>
            <input type = "hidden" name="source" value="submit"/>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>站点信息</th>
                <th>上级组织</th>
                <th>城市团队/经理</th>
                <th>审核状态</th>
                <th>状态设置</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#if page?? && page.orgStationVoList??>
            <#list page.orgStationVoList as org>
            <#assign orgStation = org.orgStation>
            <tr data-orgname="${orgStation.orgName !''}">
                <td>
                    <p><#if orgStation.orgChecked != 0><a href="/org/update/${orgStation.orgId}" target="_blank">${orgStation.orgName}</a><#else>${orgStation.orgName}</#if> &nbsp;&nbsp;&nbsp;&nbsp;(id:${orgStation.orgId})</p>
                    <p>城市：${orgStation.cityName}</p>
                    <p>在职员工总数：${orgStation.riderCount!0}人</p>
                    <p>服务商家数：${orgStation.poiCount!0}</p>
                </td>
                <#assign parent = org.parent>
                <td>
                    <p>${parent.orgName}</p>
                    <p>类型：
                        <#if 1 == parent.orgType>自建
                        <#elseif 2 == parent.orgType>加盟
                        <#elseif 3 == parent.orgType>驻店三方
                        <#elseif 4 == parent.orgType>众包
                        <#elseif 5 == parent.orgType>角马
                        <#elseif 7 == parent.orgType>城市代理
                        <#else>
                        </#if>
                    </p>
                    <p>
                        <#if 2 == parent.orgType>
                            <#if 1 == parent.isProxy>模式：代理<#else>模式：加盟</#if>
                        </#if>
                    </p>
                </td>
                <td>
                    <p>${org.cityOrgName!''}</p>
                    <#if org?? && org.cityManagerNames??>
                        <p><#if org.cityManagerRoleName??> ${org.cityManagerRoleName!''}:</#if></p>
                        <#list org.cityManagerNames as cityManagerName>
                            <p>${cityManagerName!''}</p>
                        </#list>
                    </#if>

                    <#if org?? && org.areaManagerNames??>
                        <p><#if org.areaManagerRoleName??> ${org.areaManagerRoleName!''}:</#if></p>
                        <#list org.areaManagerNames as areaManagerName>
                            <p>${areaManagerName!''}</p>
                        </#list>
                    </#if>
                </td>
                <td>
                    <#if orgStation.orgChecked==0>待审核</#if>
                    <#if orgStation.orgChecked==1>审核通过</#if>
                    <#if orgStation.orgChecked==2>驳回</#if>
                </td>
                <td>
                    <p>合作状态:</p>
                    <#if orgStation.status==1>合作
                    <#elseif orgStation.status==0>
                        不合作
                    </#if>
                </td>
                <td>
                    <#if orgStation.orgChecked==1 && orgStation.isInOff == 0 && orgStation.status == 1>
                        <#if orgStation.hasBindDeliveryArea>
                            <a data-toggle="tooltip" title="解绑配送区域" data-placement="top" class="unbindArea_link"
                               rel="${orgStation.orgId!''}">
                                <i class="fa fa-link fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                        <#else>
                            <a data-toggle="tooltip" title="绑定配送区域" data-placement="top" class="bindArea_link"
                               rel="${orgStation.orgId!''},${orgStation.cityId!''},${orgStation.cityName!''},${orgStation.orgType!''}">
                                <i class="fa fa-link fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                        </#if>
                    </#if>
                    <#if orgStation.orgOperations??>
                        <#list orgStation.orgOperations as op>
                            <a data-toggle="tooltip" title="${op.name}" data-placement="top"
                               href="${op.url}${orgStation.orgId}" target="_blank">
                                <i class="fa ${op.icon} fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                        </#list>
                    </#if>
                    <#if orgStation.status==1 && 2 == orgStation.orgType && orgStation.isInOff == 0 && orgStation.status == 1>
                        <input type="hidden" class="js_station_id" value="${orgStation.orgId !0}"/>
                        <a data-toggle="tooltip" title="设置不合作" data-placement="top" href="javascript:void(0)"
                           class="js_btn_status">
                            <i class="fa fa-trash-o fa-lg opration-icon"></i>
                        </a>
                    </#if>
                </td>
            </tr>
            </#list>
            </#if>
            </tbody>
        </table>
        <#if (page)?exists && (page.total)?exists && (page.pageSize)?exists && (page.pageNum)?exists && page.total gt 0 && page.pageSize gt 0 && page.pageNum gt 0  >
            <div style="float: left; padding: 10px; 15px"><span>共${page.total!'0'}项</span></div>
          <#if page.total??>
            <#import "/page/pager.ftl" as q>
            <@q.pager pageNo=page.pageNum pageSize=page.pageSize recordCount=page.total toURL="/org/stations"/>
          </#if>
        </#if>
    </div>

    <div class="modal fade" id="bindDeliveryAreaModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" id ="bindAreaCloseButton" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div id="alert_bind_area_error"></div>
                    <h4 style="border-bottom: 1px solid #e5e5e5; padding-bottom: 7px;">选择待绑定的配送区域</h4>
                    <label>城市</label>
                    <input type="text" readonly class="form-control input-sm js_bind_city_name" name="bindCityName" id="bindCityName" data-minlength="8" value=""/>
                    <input type="hidden" class="js_bind_city_id" name="bindCityId" id="bindCityId" value=""/>
                    <input type="hidden" class="js_bind_org_type" name="bindOrgType" id="bindOrgType" value=""/>
                    <label>请输入配送区域名称</label>
                    <input type="text" class="form-control input-sm js_bind_area_name" name="bindAreaName" id="bindAreaName" data-minlength="8" value=""/>
                    <input type="hidden" class="js_bind_area_id" name="bindAreaId" id="bindAreaId" value=""/>
                    <div>
                        <p id="modelChangeOneTips" style="color: red;width: 44%;margin: 20px auto;line-height: 22px;display:none;">
                            此配送区域已绑定其他站点，如继续绑定，则当前站点次日才可绑定成功，且全部站点调度模式改为“推抢结合”，次日生效。
                        </p>
                        <p id="modelChangeMoreTips" style="color: red;width: 44%;margin: 20px auto;line-height: 22px;display:none;">
                            此配送区域已绑定其他站点，如继续绑定，则全部站点调度模式改为“推抢结合”，次日生效。
                        </p>
                    </div>
                    <input type="hidden" name="bindOrgId" id="bindOrgId"/>
                    <input type="hidden" id="isChecked" value="0"/>
                    <div class="modal-footer">
                        <input type="submit" id="bindAreaSubmit" value="确认" class="btn btn-danger btn-sm" />
                        <button class="btn btn-success btn-sm" id="cancelBindArea">取消</button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="showBindAreaRes">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="resMsg">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="showUnbindAreaRes">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="unBindResMsg">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="unBindAreaTipsModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 style="border-bottom: 1px solid #e5e5e5; padding-bottom: 5px;">
                        <i class = "fa fa-info-circle fa-lg opration-icon"></i>&nbsp;提示
                    </h4>
                    <p id="unbind_one_massage" hidden style = "margin: 20px auto;width: 68%;font: initial;">
                        解绑后，此配送区域无其他绑定站点，商家将被全部置休，确定此操作请点击“确定”完成解绑功能。
                    </p>
                    <p id="unbind_more_massage" hidden style = "margin: 20px auto;width: 68%;font: initial;">
                        确定解绑区域？
                    </p>
                    <input type="hidden" id="unBindOrgId" value="0"/>
                    <input type="hidden" id="unBindAreaId" value="0"/>
                    <div class="modal-footer" style="border-top: none">
                        <input type="submit" id="unBindAreaResubmit" value="确认" class="btn btn-danger btn-sm" />
                        <button class="btn btn-success btn-sm" id="cancelunBindArea">取消</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<@commonPop></@commonPop>
<script>
    document.title = '站点管理';
    $('#cityId').select2();
    $('#orgId').select2();
</script>