<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">

<style type="text/css">
    #main-container{
        padding: 10px 20px;
    }
</style>

<div id="main-container">
    <@joinOrgNavi org is_org_manager 4 ></@joinOrgNavi>
    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>加盟站名称</th>
                <th>加盟站ID</th>
                <th>合作状态</th>
                <th>审核状态</th>
                <th>负责人</th>
                <th>站点人员数</th>
                <th>服务商家数</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#if page?exists && page.data?exists>
                <#list page.data as station>
                <tr <@output_props obj = station />>
                    <td>
                        <#if is_org_manager?exists && is_org_manager == 1>
                            ${station.orgName!''}
                        <#else>
                            <a href="/org/stations?orgId=${station.orgId}&status=${station.status !'-1'}" target="_blank" style="color:blue">${station.orgName!''}</a>
                        </#if>
                    </td>
                    <td>${station.orgId!''}</td>
                    <td>
                        <#if station.status == 1>合作
                        <#elseif station.status==0>不合作
                        </#if>
                    </td>
                    <td>
                        <#if station.orgChecked == 1>审核通过
                        <#elseif station.orgChecked == 0>待审核
                        <#elseif station.orgChecked == 2>驳回
                        </#if>
                    </td>
                    <td>${station.orgManager!''}</td>
                    <td>${station.riderCount!'0'}</td>
                    <td>${station.poiCount!'0'}</td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>
    <#if page?exists && page.pageSize gt 0>
        <div style="float: left; padding: 10px; 15px"><span>共${(page.totalCount)!'0'}项</span></div>
        <#import "../page/pager.ftl" as q>
        <@q.pager pageNo=page.pageNo pageSize=page.pageSize recordCount=page.totalCount toURL="/org/franchiseeStationList"/>
    </#if>
    </div>   
</div>
<script>
    document.title = '站点列表';
</script>