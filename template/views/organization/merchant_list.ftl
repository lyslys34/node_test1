<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<script type="text/javascript" src="${staticPath}/js/page/organization/merchant.js?ver=2"></script>

<div id="main-container">
    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>商家名</th>
                <th>商家POI</th>
                <th>配送方式</th>
                <th>商家电话</th>
                <th>商家地址</th>
                <th>合作状态</th>
                <#if is_org_manager?exists && is_org_manager == 1>
                <#else>
                    <th>操作</th>
                </#if>
            </tr>
            </thead>
            <tbody>
            <#if result??>
            <#list result.data.data as merchant>
                <tr>
                    <td>${merchant.name}</td>
                    <td>${merchant.id}</td>
                    <td>
                    	<#if merchant.logistics??>
	                    	<#list merchant.logistics as logistic>
	                    		<#if logistic_index gt 0 >
	                    			<br/>
	                    		</#if>
	                    		${logistic.topType}:${logistic.subType}
	                    	</#list>
	                   	<#else>
	                   		没取出来
                    	</#if>
                    </td>
                    <td>${merchant.phone}</td>
                    <td>${merchant.address}</td>
                    <td><span class="label label-success">合作中</span></td>
                <#if is_org_manager?exists && is_org_manager == 1>
                <#else>
                    <td>
                    <#--<li class="dropdown hidden-xs">-->
                    <#--<a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
                    <#--请选择-->
                    <#--</a>-->
                    <#--<ul class="dropdown-menu dropdown-merchant">-->
                    <#--<li>-->
                    <#--<a href="/org/merchantremove?merchantId=${merchant.id}&orgId=${orgId}">删除</a>-->
                    <#--<a href="#">操作日志</a>-->
                    <#--</li>-->
                    <#--</ul>-->
                    <#--</li>-->
                                <a data-toggle="tooltip" title="操作日志" data-placement="top" href="#">
                                    <i class="fa fa-file-o fa-lg opration-icon"></i>
                                </a>
                    </td>
                </#if>

                </tr>
            </#list>
            </#if>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/org/merchantlist"/>
    </#if>
        <div class="modal fade" id="addPartnerModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>新建商家</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>请输入商家POI</label><div id="alert_error"></div>
                            <input type="text" class="form-control input-sm" placeholder="商家POI" id="merchant" name="merchant">
                            <input type="hidden" id="orgId" name="orgId" value="${orgId}">
                        </div><!-- /form-group -->
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">取消</button>
                            <input type="button" value="提交" class="btn btn-danger btn-sm" id="B_addMer"/>
                        </div>
                    </div>
                    <#--<div class="modal-footer">-->
                        <#--<button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">取消</button>-->
                        <#--<a href="/org/merchantadd?" class="btn btn-danger btn-sm">提交</a>-->
                    <#--</div>-->
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <#--<div class="panel-footer clearfix">-->
    <#--<ul class="pagination pagination-xs m-top-none pull-right">-->
    <#--<li class="disabled"><a href="#">Previous</a></li>-->
    <#--<li class="active"><a href="#">1</a></li>-->
    <#--<li><a href="#">2</a></li>-->
    <#--<li><a href="#">3</a></li>-->
    <#--<li><a href="#">4</a></li>-->
    <#--<li><a href="#">5</a></li>-->
    <#--<li><a href="#">Next</a></li>-->
    <#--</ul>-->
    <#--</div>-->
    </div>
</div>
<script>document.title = '服务商家列表';</script>
