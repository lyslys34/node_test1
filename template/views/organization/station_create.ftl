<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/organization/station_create.js</content>
<div id="main-container">
    <h2 style="color:red" id="head-message">新建加盟站后将由总部审核才能生效</h2>
    <#import "station_create_progress.ftl" as p>
    <@p.progress length=2 step=1 />
    <h4>基本信息</h4>
    <div class="panel panel-default">
        <form method="post" class="form-horizontal" id="formValidate1" data-validate="parsley" novalidate="" action="/org/doCreate">
            <input type="hidden" value="" name="orgType" id="orgType" />
            <div class="form-group">
                <label class="control-label col-sm-2">站点类型</label>
                <div class="col-sm-5">
                    <select class="form-control input-sm" id="levelType" name="levelType">
                        <#if visbelLevelTypes?exists>
                            <#list visbelLevelTypes?keys as key>
                                <option value=${key}>
                                    ${visbelLevelTypes[key]}
                                </option>
                            </#list>
                        </#if>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">站点名称</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control input-sm parsley-validated" id="orgName" name="orgName" data-required="true" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" id="parentLabel">自营分部</label>
                <div class="col-sm-5">
                    <select class="form-control input-sm" id="parentId" name="parentId">

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">所在城市</label>
                <div class="col-sm-5">
                    <select class="form-control input-sm" id="orgCity" name="orgCity">
                        <#list cityList as city >
                        <option value="${city.city_id !''}" >${city.name !''}</option>
                        </#list>
                    </select>
                </div>
            </div>
            <div class="form-group" id="isProxyDiv">
                <label class="col-sm-2 control-label">模式</label>
                <div class="col-sm-5">
                    <select class="form-control input-sm" id="isProxy" name="isProxy">
                        <option value="0">加盟</option>
                        <option value="1">代理</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">站点联系电话</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control input-sm parsley-validated" id="phone" name="phone" data-required="true" >（仅供紧急联系使用，此账号无法登录烽火台）
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">营业时间</label>
                <div class="col-sm-5">
                    <div class="businessHourses">
                        <input type="text" class="input-sm parsley-validated" name="businessHoursBegin" data-required="true" style="width:40%" >&nbsp;&nbsp;至&nbsp;&nbsp;
                        <input type="text" class="input-sm parsley-validated" name="businessHoursEnd" data-required="true" style="width:40%" >
                        <a data-toggle="tooltip" title="" data-placement="top" target="_blank"  id="add" href="javascript:;">
                            <i class="fa fa-plus fa-lg opration-icon"></i>
                        </a>
                        <p>（请按时间顺序填写，如9:00-23:00）</p>
                    </div>

                    <div class="businessHourses hide">
                        <input type="text" class="input-sm parsley-validated" name="businessHoursBegin" data-required="true" style="width:40%" >&nbsp;&nbsp;至&nbsp;&nbsp;
                        <input type="text" class="input-sm parsley-validated" name="businessHoursEnd" data-required="true" style="width:40%" >
                        <a data-toggle="tooltip" title="" data-placement="top" target="_blank"  id="delete" href="javascript:;">
                            <i class="fa fa-minus fa-lg opration-icon"></i>
                        </a>
                        <p>（第二个时段为夜宵专用，只允许设置0:00开始）</p>
                    </div>
                </div>
            </div>
            <input type="hidden" value="" name="businessHourses" id="businessHourses" />
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="button" class="btn btn-danger" id="submit">保存并下一步</button>
                    <input type="submit" id="realSubmit" class="hide" />
                    <a href="/org/stations" class="btn btn-success">取消</a>
                </div>
            </div>
        </form>
    </div>
</div>
<@commonPop></@commonPop>
<script type="text/javascript" src="${staticPath}/js/page/utils.js?ver=${staticVersion !''}"></script>
<script>
    document.title = '新建站点';
    javascript:window.history.forward(1);
    var $parentId = $('#parentId');
    // $('#orgType').select2();
    $('#orgCity').select2();
    $('#parentId').select2();

</script>