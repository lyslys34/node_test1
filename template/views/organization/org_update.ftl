<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/organization/org_update.js</content>

<div id="main-container">
  <div class="panel panel-default">
    <form method="post" class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/org/doUpdate">
        <input type="hidden" name="orgId" value="${orgId}"/>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">组织名称</label>
              <input type="text" class="form-control input-sm parsley-validated" id="orgName" name="orgName" value="<#if orgVo?exists>${orgVo.orgName !''}</#if>" data-required="true" >
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">组织类型</label>
                <#--<select class="form-control input-sm" id="orgType" name="orgType"  <#if is_org_manager?exists && is_org_manager == 1>disabled = "disabled" >-->

                <select class="form-control input-sm" id="orgType" name="orgType" disabled = "disabled" >
                    <#list orgTypeList as type>
                        <option value="${type.type !''}" <#if type.type == orgVo.orgType > selected="selected" </#if>>${type.comment !''}</option>
                    </#list>
                  
                    <#if orgVo?exists && orgVo.orgType==0><#if (orgVo.levelType==200||orgVo.levelType==210)><option value="0" selected="selected">加盟</option><#elseif (orgVo.levelType==310)><option value="0" selected="selected">自建</option></#if></#if>

                </select>
            <#if is_org_manager?exists && is_org_manager == 1><input type="hidden" name="orgType" value="${orgVo.orgType !''}"/></#if>
            </div>
          </div>

          <div id="div_level_type" hidden class="col-md-4">
            <div class="form-group">
              <label class="control-label">组织层级</label>
                <select class="form-control input-sm" id="levelType" <#if orgVo?exists><#if orgVo.levelType?exists><#if orgVo.levelType != 0>disabled = "disabled"</#if></#if></#if> name="levelType">
                      <option value="${orgVo.levelType!''}" selected="selected" >${levelTypeName!''}</option>
                </select>
            </div>
          </div>
        </div>

        <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                  <label class="control-label">组织城市</label>
                  <#--input type="text" class="form-control input-sm parsley-validated js_city_name" name="cityName" id="cityName" data-minlength="8" value=""/-->
                  <#--input type="hidden" name="orgCity" id="orgCity" class="js_city_id" value="${orgCity!""}"/-->
                  <select class="form-control input-sm" id="orgCity" name="orgCity" <#if is_org_manager?exists && is_org_manager == 1>disabled = "disabled" </#if>>
                  	  <#--<option value="0" <#if 0 == orgVo.cityId> selected="selected" </#if> >全部</option>-->
                  	  <#list cityList as city >
                  	  	  <option value="${city.city_id !''}" <#if city.city_id == orgVo.cityId > selected="selected" </#if> >${city.name !''}</option>
                  	  </#list>
                  </select>
              <#if is_org_manager?exists && is_org_manager == 1><input type="hidden" name="orgCity" value="${orgVo.cityId !''}"/></#if>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                  <label class="control-label">上级组织</label>
                  <select class="form-control input-sm" id="parentId" name="parentId"  <#if is_org_manager?exists && is_org_manager == 1>disabled = "disabled" </#if>>
                      <option class="other_level_org" value="0" >请选择组织</option>
                      <#list orgs as org>
                          <option class="other_level_org" value="${org.orgId !''}" <#if org.orgId == orgVo.parentId > selected="selected" </#if> >${org.orgName !''}</option>
                      </#list>
                  </select>
                  <#if is_org_manager?exists && is_org_manager == 1><input type="hidden" name="parentId" value="${orgVo.parentId !''}"/></#if>
              </div>
        	</div>
        </div>

          <div class = "row">
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">是否和美团配送结算邮资</label>
                      <select class="form-control input-sm" id="isProxy" name="isProxy" <#if is_org_manager?exists && is_org_manager == 1>disabled = "disabled" </#if>>
                          <option value="0" <#if orgVo?exists && orgVo.isProxy == 0>selected="selected"</#if>>是</option>
                          <option value="1" <#if orgVo?exists && orgVo.isProxy == 1>selected="selected"</#if>>否</option>
                      </select>
                      <#if is_org_manager?exists && is_org_manager == 1><input type="hidden" name="isProxy" value="${orgVo.isProxy !''}"/></#if>
                  </div>
              </div>
          </div>
      </div>
      <div class="panel-footer text-right">
        <#--<button type="button" class="btn btn-success" onclick="javasctipt:history.go(-1);">返回</button>&nbsp;&nbsp;-->
        <button type="submit" class="btn btn-success">保存</button>
      </div>
    </form>
  </div>
</div>
<script type="text/javascript">
	document.title = '编辑组织';
    $('#orgCity').select2();
    $('#parentId').select2();
</script>