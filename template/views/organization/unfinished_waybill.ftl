<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">
<div id="main-container">
    <div name='titleName' id='titleName' style="font-size: 14px; padding: 10px;"></div>
    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
            <#-- <th>操作</th>-->
                <th>组织名称</th>
                <#--<th>组织电话</th>-->
                <th>人均单量(订单数/骑手数)</th>
                <th>未完成订单数</th>
                <th>非收工状态骑手数</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list unfinishedList as unfinished>
            <tr>
                <td>${unfinished.orgName!''}</td>
                <td>${unfinished.averageWaybill!''}</td>
                <td>${unfinished.unfinishedWaybillNum!''}</td>
                <td>${unfinished.riderNum!''}</td>
            </tr>
            </#list>
            </tbody>
        </table>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/org/unfinishedlist"/>
    </#if>
    </div>
</div>
<script>document.title = '未完成运单管理';</script>
<script>
    function myrefresh()
    {
        window.location.reload();
    }
    var myDate = new Date();
    var year = myDate.getFullYear();
    var month = myDate.getMonth() + 1;
    var day = myDate.getDate();
    document.getElementById("titleName").innerText = "每个组织下当前人均单量  (日期: " + year +"年"+ month +"月"+ day + "日)";
    setInterval(myrefresh,60000); //指定1秒刷新一次
</script>