<#include "org_config.ftl">
<#include "../widgets/sidebar.ftl">

<style media="screen">
    .mis-search-ul {
        /*position: absolute;*/
        border: 1px solid rgba(0, 0, 0, .15);
        /*min-width: 160px;*/
        background-color: white;
        max-height: 200px;
        overflow: auto;
    }

    .mis-search {
        display: none;
        position: absolute;
        max-height: 200px;
        width: 300px;
        z-index: 10000;
    }

    .mis-search-ul li {
        margin: 0px 0px;
        padding: 5px 5px;
        border-bottom: 1px solid rgba(0, 0, 0, .15);
        cursor: pointer;
    }

    .mis-search-ul li:hover {
        background-color: rgba(0, 0, 0, .15);
    }

    .tl {
        text-align: left !important;
    }

    .tr {
        text-align: right !important;
    }

    .form-control {
        display: inline-block;
    }

    .dtitle {
        display: inline-block;
        width: 180px;
        padding-right: 5px;
    }

    .ddata {
        display: inline-block;
    }

    .w200 {
        width: 200px;
    }

    .w120 {
        width: 120px;
    }

    .w300 {
        width: 300px;
    }

    .user-add, .user-remove {
        cursor: pointer;
        font-size: 17px;
        width:24px;
        text-align: center;
    }

    .user-add:hover, .user-remove:hover {
        color: #888888;
    }

    .dtitle_s {
        display: inline-block;
    }

    .ddata_s {
        display: inline-block;
    }

    dd, dt {
        line-height: 24px;
    }

    .vertical-top {
        vertical-align: top;
    }

    .user-item {
        margin: 5px 0px;
    }
</style>
<content tag="javascript">/organization/org_tree_create.js</content>


<div id="main-container">
    <div class="panel panel-default">
        <form method="post" class="no-margin" id="formValidate" data-validate="parsley" novalidate="">
            <input type="hidden" id="parentId" name="parentId" value="${parentId}"/>
            <input type="hidden" id="parentType" name="parentType" value="${parentType}"/>
            <input type="hidden" id="parentLevelType" name="parentLevelType" value="${parentLevelType}"/>

            <div class="panel-body">

                <div class="form-group" id="parentOrgDiv">
                    <dt class="dtitle tr">
                        <label class="control-label">上级组织：</label>
                    </dt>
                    <dd class="ddata">
                        <input type="text" class="form-control input-sm parsley-validated w200" id="parentName"
                               name="parentName" data-required="true" disabled="disabled" value="${parentName}">
                    </dd>
                </div>

                <div class="form-group">
                    <dt class="dtitle tr">
                        <label class="control-label">${levelTypeDes!'组织'}名称：</label>
                    </dt>
                    <dd class="ddata">
                        <input type="text" class="form-control input-sm parsley-validated w200" id="orgName"
                               name="orgName" data-required="true" maxlength="30">
                    </dd>
                </div>

                <div class="form-group">
                    <dt class="dtitle tr">
                        <label class="control-label" class="dtitle tr">组织城市：</label>
                    </dt>
                    <dd class="ddata">
                        <select class="form-control input-sm w200" id="cityId" name="cityId">
                            <option value="0">全部</option>
                        <#if cityList?exists && (cityList?size > 0)>
                            <#list cityList as city >
                                <option value="${city.city_id}"
                                        <#if cityId?exists && cityId == city.city_id>selected</#if>>${city.name}</option>
                            </#list>
                        </#if>
                        </select>
                    </dd>
                </div>
            <#if roles?exists>
                <#list roles as r>
                    <div class="form-group js_role_list">
                        <input type="hidden" class="js_role_code" value="${r.code!0}"/>
                        <dt class="dtitle tr">
                            <label class="control-label">${r.name!''}：</label>
                        </dt>
                        <dd class="ddata vertical-top">
                            <div>
                                <div class="user-add js_add_user"><i class="fa fa-plus"></i></div>
                            </div>
                        </dd>
                    </div>
                </#list>
            </#if>


            </div>
            <div class="panel-footer text-right">
                <a href="javascript:;" id="js_save" class="btn btn-success"><i  class="js_loading fa fa-refresh fa-spin hidden"></i>保存</a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="/orgtree" class="btn btn-default">取消</a>
            </div>

        </form>
    </div>
    <div id="js_user_item_html" style="display: none;">
        <div class="user-item js_user_item">
            <span class="dtitle_s">mis账号:</span>
            <span class="ddata_s">
                <input type="text" class="form-control input-sm js_mis_box w300"/>
                <div class="mis-search">
                    <input type="hidden" class="js_employee_id" name="employeeId" value="${employeeId!''}">
                    <input type="hidden" class="js_mis_id" name="misId" value="${employeeId!''}">
                    <input type="hidden" class="js_user_name" name="misName" value="${employeeId!''}">
                    <ul class="mis-search-ul">
                    </ul>
                </div>
            </span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="dtitle_s">手机号:</span>
            <span class="ddata_s">
                <input type="text" class="form-control input-sm js_phone_box w120" maxlength="11"/>
            </span>&nbsp;&nbsp;&nbsp;
            <span class="user-remove js_user_remove">
                <i class="fa fa-minus"></i>
            </span>
        </div>
    </div>
</div>
<@commonPop></@commonPop>
<script>
    document.title = '新建组织';
    $('#cityId').select2();
</script>
