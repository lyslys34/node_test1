<#-- 截取字符串 -->
<#macro substring str count>
  <#if str?length != 0>
    <#assign name = str>
    <#assign end = (!name?? || name?length lt count)?string((name?length-1)!0, count-1)>
    ${(name[0..end?number])}
    <#if name?length gt count>...</#if>
  </#if>
</#macro>

<#function getPassDomain>
  <#if __environment__?? && __environment__=='test'>
    <#return 'passport.test.meituan.com'>
  <#else>
    <#return 'passport.meituan.com'>
  </#if>
</#function>

<#function getLoginUrl continue>
  <#local settokenUrl = __host__ + '/account/settoken'>
  <#if continue?? && continue?length gt 0>
    <#local settokenUrl = settokenUrl + '?continue=' + continue?url>
  </#if>
  <#return 'https://'+getPassDomain()+'/account/unitivelogin?service=waimai&continue='+settokenUrl?url>
</#function>

<#function getRegisterUrl continue>
  <#local settokenUrl = __host__ + '/account/settoken'>
  <#if continue?? && continue?length gt 0>
    <#local settokenUrl = settokenUrl + '?continue=' + continue?url>
  </#if>
  <#return 'https://'+getPassDomain()+'/account/unitivesignup?service=waimai&continue='+settokenUrl?url>
</#function>

<#function getLogoutUrl continue>
  <#local settokenUrl = __host__ + '/account/logout'>
  <#if continue?? && continue?length gt 0>
    <#local settokenUrl = settokenUrl + '?continue=' + continue?url>
  </#if>
  <#return 'https://'+getPassDomain()+'/account/unitivelogout?service=waimai&continue='+settokenUrl?url>
</#function>

<#-- 这只是一个简易的方法，应该用java实现一个效率更高的方法 -->
<#macro jsonEncode obj>
  <#if !obj??>null
  <#elseif obj?is_boolean>${obj?string}
  <#elseif obj?is_string>"${obj}"
  <#elseif obj?is_number>${obj}
  <#elseif obj?is_enumerable>[<#list obj as o><@jsonEncode o /><#if o_has_next>,</#if></#list>]
  <#elseif obj?is_hash>{<#list obj?keys as k>"${k}":<@jsonEncode obj[k]/><#if k_has_next>,</#if></#list>}
  <#else>${obj?string}
  </#if>
</#macro>

<#function arrayJoin arr separate=','>
  <#assign result = ''>
  <#if arr?? && arr?size gt 0>
    <#list arr as item>
      <#if item_has_next>
        <#assign result = result + item?string + separate>
      <#else>
        <#assign result = result + item?string>
      </#if>
    </#list>
  </#if>
  <#return result>
</#function>


<#-- partner使用 -->


<#macro output_props obj>
    <#assign keys = obj?keys>
    <#list keys as key>
        <#if key?length gt 3 && key[0..2] == "get" && key != "getClass">
            <#assign pName = key[3..key?length-1]?uncap_first>
            <#if obj[pName]?exists><#assign pValue = obj[pName]> <#if pValue?is_date>data-${pName}="${pValue?date}"<#else>data-${pName}="${pValue?string}"</#if></#if>
        </#if>
    </#list>
</#macro>

<#macro output_status status>
    <#switch status>
        <#case 0>等待抢单<#break>
        <#case 1>取餐中<#break>
        <#case 2>配送完成<#break>
        <#case 3>分拣中<#break>
    </#switch>
</#macro>

<#macro output_mode mode>
    <#switch mode>
        <#case 1>每单固定抽成<#break>
        <#case 2>每单比例分成<#break>
        <#case 3>菜品价格折扣<#break>
    </#switch>
</#macro>

<#macro output_query>

    <#if pois?exists>
    &nbsp;&nbsp;商家名称：<select id="poiId">
        <#if deleteAll?exists && deleteAll == 1>
        <#else>
            <option value="0">所有商家</option>
        </#if>
            <#list pois as poi>
                <option value="${poi.poiId}" <#if poiId?exists && poiId == poi.poiId>selected</#if>>${poi.poiName}</option>
            </#list>
        </select>
    </#if>

    <#if dispatchers?exists>
    &nbsp;&nbsp;姓名：<select id="query-dispatchers">
        <#if deleteAll?exists && deleteAll == 1>
        <#else>
                <option value="0">全部</option>
        </#if>
        <#list dispatchers as dispatcher>
            <option value="${dispatcher.dispatcherId}" <#if dispatcherId?exists && dispatcherId == dispatcher.dispatcherId>selected</#if>>${dispatcher.name}</option>
        </#list>
    </select>
    </#if>

    <#if dispatchType?exists>
    &nbsp;&nbsp;任务：<select id="query-dispatch-type" class="query-dispatch-type query-select" data-value="${dispatchType}">
            <option value="0">全部</option>
            <option value="1">从商家到分拣中心/楼口接餐人</option>  <!-- 取餐员 -->
            <option value="2">分配单量</option> <!-- 分拣员 -->
            <option value="3">从分拣中心到用户</option> <!-- 送餐员 -->
        </select>
    </#if>

    &nbsp;&nbsp;
    日期：<input id="query-start-date" class='date-picker J-datepicker' readonly="readonly" value="<#if startDate?exists>${startDate?date}<#else>${.now?string('yyyy-MM-dd')}</#if>">
    至 <input id="query-end-date" class="date-picker J-datepicker" readonly="readonly" value="<#if endDate?exists>${endDate?date}<#else>${.now?string('yyyy-MM-dd')}</#if>">

    <#if timePart?exists>
    &nbsp;&nbsp;
    <select id="query-time-part" class="query-time-part query-select" data-value="${timePart}">
        <option value="0">全天订单</option>
        <option value="1">午市订单</option>
        <option value="2">晚市订单</option>
    </select>
    </#if>

    &nbsp;&nbsp;

    <button id="query-commit" class="btn btn-default">查询</button>
</#macro>