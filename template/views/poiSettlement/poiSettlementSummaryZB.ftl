<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="staticPath">/static</content>
<content tag="javascript">/poiSettlement/poiSettlementSummaryZB.js</content>
<content tag="cssmain">/poiSettlement/poiSettlementSummary.css</content>
<content tag="pageName">poiSettlementSummary</content>
<style>
    .a hover{
        color: #58B7B1;
        text-decoration: underline;
    }
</style>
<div id="main-container">
    <form class="no-margin form-inline" id="formValidate1" data-validate="parsley" novalidate="" method="post">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>起始时间:</label>
                            <input type="text" name="startDate" style="cursor: pointer;"  readonly="readonly" value="<#if startDate?exists>${startDate}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <#if pois?exists>
                        <div class="form-group">
                            <label>商家名称:</label>
                            <select class="form-control input-sm" style="width: 200px;" name="poiName">
                                <option value="0">所有商家</option>
                                <#list pois as poi>
                                    <option value="${poi.poiId}" <#if poiName?exists && poiName == poi.poiId>selected</#if>>${poi.poiName}</option>
                                </#list>
                            </select>
                        </div>
                        </#if>
                    </div>
                    <div class="col-sm-3" style="width: 22%">
                        <div class="form-group">
                            <label>商家类型:</label>
                            <select name="poiType" style="width: 100px;height: 25px;">
                                <option value="0" <#if !poiType?exists || poiType == -1>selected="selected"</#if> >全部</option>
                                <option value="1" <#if poiType?exists && poiType == 1 >selected="selected"</#if> >代购</option>
                                <option value="2" <#if poiType?exists && poiType == 2 >selected="selected"</#if> >普通</option>
                            </select>
                        </div>
                    </div>
                    <div style="float:right;padding-right: 15px">
                        <button type="button" class="btn btn-success js_submitbtn" style="width:90px" actionurl="/partner/poiSettlement/zb">查询</button>
                    </div>
                </div>
                <div class="row" style="margin-top: 5px">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>结束时间:</label>
                            <input type="text" name="endDate" style="cursor: pointer;" readonly="readonly"  value="<#if endDate?exists>${endDate}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label style="margin-left: 13px">商家ID:</label>
                            <input type="text" style="width: 200px;" name="poiId" value="${poiId}" maxlength="20" class="form-control input-sm">
                        </div>
                    </div>
                    <#--<div style="float:right;">-->
                        <#--<a name="detail" style="margin-right:20px;" target="_blank"-->
                           <#--href="<#if orgType==2>/partner/settlementStrategy/agent<#elseif orgType==1>/partner/settlementStrategy/self</#if>">-->
                            <#--<label style="padding: 8px 0px;color:#4ac598;cursor:pointer;">结算规则</label>-->
                            <#--<i class="fa fa-question-circle fa-lg opration-icon"></i>-->
                        <#--</a>-->
                    <#--</div>-->
                </div>
            </div>
        </div>
        <div class="row" style="margin-bottom: 10px; padding-right:20px">
            <div class="col-md-4"><h5><i class="glyphicon glyphicon-bookmark opration-icon"></i><#if orgName??>${orgName}</#if> 商家结算报表</h5></div>
            <div style="float:right;padding-right: 10px"><button type="button" class="btn btn-success js_submitbtn" style="float: right;width:90px" actionurl="/partner/poiSettlement/zb/export">导出EXCEL</button></div>
        </div>
    </form>

    <div class="panel panel-default table-responsive">
        <#--<div class="result_area">-->
        <#if allPoiSummary?exists>
            <div class="table_title form-inline">
                <div class="col-md-4"><h5><i class="glyphicon glyphicon-list opration-icon"></i> 查询结果</h5></div>
                <table class="table table-striped">
                    <tr>
                        <td>数据项</td>
                        <td>订单总量</td>
                        <td>订单总额</td>
                        <td>商品原价总额</td>
                        <td>配送费总额</td>
                        <td>总代付额</td>
                    </tr>
                    <tr>
                        <td>总计</td>
                        <td>${allPoiSummary.totalOrderQuantity}</td>
                        <td>￥${allPoiSummary.totalOrderMoney?string('0.0')}</td>
                        <td>￥${allPoiSummary.totalGoodsMoney?string('0.0')}</td>
                        <td>￥${allPoiSummary.totalShippingFee?string('0.0')}</td>
                        <td>￥${allPoiSummary.totalPayAmount?string('0.0')}</td>
                    </tr>
                </table>
            </div>
        </#if>

            <div class="table_title form-inline">
                <#--<div class="col-md-4"><h5><i class="glyphicon glyphicon-list opration-icon"></i> 结算明细</h5></div>-->
            </div>
            <table class="table table-striped">
                <tr>
                    <td>商家名称</td>
                    <td>商家ID</td>
                    <td>商家类型</td>
                    <td>订单总数</td>
                    <td>运费总额</td>
                    <td>订单总金额</td>
                    <td>菜品总原价</td>
                    <#--<td>菜品折扣</td>-->
                    <td>应付总额</td>
                    <td>实付总额</td>
                    <td>应付实付总差额</td>
                    <td>详情</td>
                </tr>
        <#if list?exists && (list?size > 0)>
                <#list list as settlementDetail>
                    <tr>
                        <td>${settlementDetail.name !''}</td>
                        <td>${settlementDetail.poiId !''}</td>
                        <td>
                            <#if settlementDetail.type == 1>
                                代购
                            <#elseif settlementDetail.type == 2>
                                普通
                            </#if>
                        </td>
                        <td>${settlementDetail.orderQuantity}</td>
                        <td>￥${settlementDetail.shippingFee?string('0.0')}</td>
                        <td>￥${settlementDetail.orderMoney?string('0.0')}</td>
                        <td>￥${settlementDetail.goodsMoney?string('0.0')}</td>
                        <#--<td>￥${settlementDetail.discountMoney}</td>-->
                        <td>￥${settlementDetail.planPayAmount}</td>
                        <td>￥${settlementDetail.actualPayAmount}</td>
                        <td>￥${settlementDetail.planPayAmount-settlementDetail.actualPayAmount}</td>
                        <td>
                            <a href="/partner/poiSettlement/zb?poiName=${settlementDetail.poiId!''}&startDate=${startDate}&endDate=${endDate}&detail=1" class="a">
                                <i class="fa fa-info-circle fa-lg opration-icon"></i>
                            </a>
                        </td>
                    </tr>
                </#list>
        </#if>
            </table>

        <#--</div>-->

        <#import "../page/pager.ftl" as q>
        <#if recordCount??>
            <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/poiSettlement/zb"/>
        </#if>
    </div>
</div>
<script>
	document.title = '商家结算报表';
	$('select').select2();
</script>
