<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/poiSettlement/poiSettlementSummary.js</content>
<content tag="cssmain">/poiSettlement/poiSettlementSummary.css</content>
<content tag="pageName">poiSettlementSummary</content>
<div id="main-container">
    <div style="content_body">
        <!--<div class="navi_head">
            <div class="navi_title">商家结算报表</div>
            <div class="navi_bottom_line">&nbsp;</div>
        </div>-->
        <div class="condition_area">
            <form class="form-inline">
                <#if pois?exists>
                    &nbsp;&nbsp;商家名称：<select class="form-control input-sm" style="width: 200px;" id="poiId">
                    <option value="0">所有商家</option>
                    <#list pois as poi>
                        <option value="${poi.poiId}" <#if poiId?exists && poiId == poi.poiId>selected</#if>>${poi.poiName}</option>
                    </#list>
                </select>
                </#if>

                &nbsp;&nbsp;
                结算时间段 <input id="query-start-date" style="cursor: pointer" class='form-control input-sm date-picker J-datepicker js_date_start' readonly="readonly" value="<#if startDate?exists>${startDate}<#else>${.now?string('yyyy-MM-dd')}</#if>">
                至 <input id="query-end-date" style="cursor: pointer" class="form-control input-sm date-picker J-datepicker js_date_start" readonly="readonly" value="<#if endDate?exists>${endDate}<#else>${.now?string('yyyy-MM-dd')}</#if>">
                <br/>
                <br/>
                <#--&nbsp;&nbsp;商家类型：<select class="form-control input-sm" style="width: 200px;" id = "poiType">-->
                                        <#--<option value="0" <#if poiType == 0>selected</#if>>全部</option>-->
                                        <#--<option value="1" <#if poiType == 1>selected</#if>>代购</option>-->
                                        <#--<option value="2" <#if poiType == 2>selected</#if>>普通</option>-->
                                    <#--</select>-->
                    <input type="hidden" style="display: none;" id="poiType" value="0"/>
                &nbsp;&nbsp;&nbsp;&nbsp;<input type = "button" id="query-commit" value="查看" class="btn btn-success js_submitbtn"/>
                <a  name="excelReport"  href="javascript:void(0);" class="btn btn-success js_submitbtn">导出表格</a>
                <a name="detail" style="margin-right:20px;float:right;" target="_blank"
                   href="<#if orgType==2>/partner/settlementStrategy/agent<#elseif orgType==1>/partner/settlementStrategy/self</#if>">
                    <label style="padding: 8px 0px;color:#4ac598;cursor:pointer;">结算规则</label>
                    <i class="fa fa-question-circle fa-lg opration-icon"></i>
                </a>
            </form>
        </div>
    <#if allPoiSummary?exists>
        <div class="result_area">
    <div class="table_title form-inline">
        <span><p>查询结果</p></span>
        <table class="table">
            <tr>
                <td>数据项</td>
                <td>订单总量</td>
                <td>订单总额</td>
                <td>商品原价总额</td>
                <td>配送费总额</td>
                <td>总代付额</td>
            </tr>
            <tr>
                <td>总计</td>
                <td>${allPoiSummary.totalOrderQuantity}</td>
                <td>￥${allPoiSummary.totalOrderMoney?string('0.0')}</td>
                <td>￥${allPoiSummary.totalGoodsMoney?string('0.0')}</td>
                <td>￥${allPoiSummary.totalShippingFee?string('0.0')}</td>
                <td>￥${allPoiSummary.totalPayAmount?string('0.0')}</td>
            </tr>
            </table>
    </div>
    </#if>
    <#if list?exists && (list?size > 0)>
            <div class="table_title form-inline">
                <span><p>结算明细</p></span>
            </div>
            <table class="table">
                <tr>
                    <td>商家ID</td>
                    <td>商家名称</td>
                    <td>商家类型</td>
                    <td>订单量</td>
                    <td>订单金额</td>
                    <td>商品原价</td>
                    <td>配送费</td>
                    <td>代付金额</td>
                    <td>账单详情</td>
                </tr>
                <#list list as settlementDetail>
                    <tr>
                        <td>${settlementDetail.poiId !''}</td>
                        <td>${settlementDetail.name !''}</td>
                        <td>
                            <#if settlementDetail.type == 2>
                                代购
                            <#elseif settlementDetail.type == 1>
                                普通
                            </#if>
                        </td>
                        <td>${settlementDetail.orderQuantity}</td>
                        <td>￥${settlementDetail.orderMoney?string('0.0')}</td>
                        <td>￥${settlementDetail.goodsMoney?string('0.0')}</td>
                        <td>￥${settlementDetail.shippingFee?string('0.0')}</td>
                        <td>￥${settlementDetail.advance?string('0.0')}</td>
                        <td>
                        <a data-toggle="tooltip" title="详情" data-placement="top" name="poiDetail" href ="javascript:void(0);" data-poiId = "${settlementDetail.poiId !''}">
                            <i class="fa fa-info-circle fa-lg opration-icon"></i>
                        </a>
                        </td>
                    </tr>
                </#list>
            </table>
    </#if>
        </div>

        <nav style="float:right;">
            <#if (list?exists && (list?size > 0)) && page?exists >
                <#assign leftNum = page.leftNum>
                <#assign rightNum = page.rightNum>
                <ul class="pagination" style="float: right;">
                    <li <#if page.currentPage == 1>class="disabled"</#if>><a id = "prev" data-page = "${page.currentPage}" href="javascript:void(0);">&laquo;</a></li>
                    <#list leftNum..rightNum as p>
                        <li <#if p == page.currentPage>class="active"</#if>><a name = "pageCode" href="javascript:void(0);">${p}</a></li>
                    </#list>
                    <li <#if page.currentPage == page.totalPage>class="disabled"</#if>><a id= "next" data-page = "${page.currentPage}" href="javascript:void(0);">&raquo;</a></li>
                </ul>
            </#if>
        </nav>

    </div>
</div>
<script>
	document.title = '商家名称';
	$('select').select2();
</script>
