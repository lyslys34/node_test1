<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="staticPath">/static</content>
<#include "poiSettlementUtils.ftl" >
<content tag="pageName">poiSettlementDetail</content>
<content tag="javascript">/poiSettlement/poiSettlementSummaryZB.js</content>
<content tag="cssmain">/poiSettlement/poiSettlementSummary.css</content>


<div id="main-container">
    <div class="navi_head">
        <div class="navi_title">商家结算报表</div>
        <div class="navi_bottom_line">&nbsp;</div>
    </div>
    <form class="no-margin form-inline" id="formValidate1" data-validate="parsley" novalidate="" method="post">
        <input type="hidden" name="detail" value="<#if detail??>${detail}<#else>0</#if>">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>起始时间:</label>
                            <input type="text" name="startDate" style="cursor: pointer;"  readonly="readonly" value="<#if startDate?exists>${startDate}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start">
                        </div>
                    </div>
                    <div class="col-md-4">
                    <#if pois?exists>
                        <div class="form-group">
                            <label>商家名称:</label>
                            <select class="form-control input-sm" style="width: 200px;" name="poiName">
                                <option value="0">所有商家</option>
                                <#list pois as poi>
                                    <option value="${poi.poiId}" <#if poiName?exists && poiName == poi.poiId>selected</#if>>${poi.poiName}</option>
                                </#list>
                            </select>
                        </div>
                    </#if>
                    </div>
                    <div style="float:right;padding-right: 15px">
                        <button type="button" class="btn btn-success js_submitbtn" style="width:90px" actionurl="/partner/poiSettlement/zb">查询</button>
                    </div>
                </div>
                <div class="row" style="margin-top: 5px">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>结束时间:</label>
                            <input type="text" name="endDate" style="cursor: pointer;" readonly="readonly"  value="<#if endDate?exists>${endDate}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label style="margin-left: 13px">商家ID:</label>
                            <input type="text" style="width: 200px;" name="poiId" value="${poiId}" maxlength="20" class="form-control input-sm">
                        </div>
                    </div>
                    <#--<div style="float:right;">-->
                        <#--<a name="detail" style="margin-right:20px;" target="_blank"-->
                           <#--href="<#if orgType==2>/partner/settlementStrategy/agent<#elseif orgType==1>/partner/settlementStrategy/self</#if>">-->
                            <#--<label style="padding: 8px 0px;color:#4ac598;cursor:pointer;">结算规则</label>-->
                            <#--<i class="fa fa-question-circle fa-lg opration-icon"></i>-->
                        <#--</a>-->
                    <#--</div>-->
                </div>
            </div>
        </div>
        <div class="row" style="margin-bottom: 10px; padding-right:20px">
            <div class="col-md-4"><h5><i class="glyphicon glyphicon-bookmark opration-icon"></i><#if orgName??>${orgName}</#if> 站点商家结算报表</h5></div>
            <div style="float:right;padding-right: 10px"><button type="button" class="btn btn-success js_submitbtn" style="float: right;width:90px" actionurl="/partner/poiSettlement/zb/export">导出EXCEL</button></div>
        </div>
    </form>

    <div class="panel panel-default table-responsive">
    <#--<div class="result_area">-->
    <#if summary?exists>
        <div class="table_title form-inline">
            <#--<div class="col-md-4"><h5><i class="glyphicon glyphicon-list opration-icon"></i> 查询结果</h5></div>-->
            <table class="table table-striped">
                <tr>
                    <td>商家名称</td>
                    <td>商家ID</td>
                    <td>商家类型</td>
                    <td>订单总数</td>
                    <td>运费总额</td>
                    <td>订单总金额</td>
                    <td>订单总原价</td>
                    <td>应付总额</td>
                    <td>实付总额</td>
                    <td>应付实付总额</td>
                </tr>
                <tr>
                    <td>${summary.name !''}</td>
                    <td>${summary.poiId !''}</td>
                    <td>
                        <#if summary.type == 1>
                            代购
                        <#elseif summary.type == 2>
                            普通
                        </#if>
                    </td>
                    <td>${summary.orderQuantity}</td>
                    <td>￥${summary.shippingFee?string('0.0')}</td>
                    <td>￥${summary.orderMoney?string('0.0')}</td>
                    <td>￥${summary.goodsMoney?string('0.0')}</td>
                    <td>￥${summary.discountMoney}</td>
                    <td>￥${summary.planPayAmount}</td>
                    <td>￥${summary.actualPayAmount}</td>
                    <td>￥${summary.planPayAmount-summary.actualPayAmount}</td>
                </tr>
            </table>
        </div>
    </#if>
        <div class="table_title form-inline">
            <#--<div class="col-md-4"><h5><i class="glyphicon glyphicon-list opration-icon"></i> 结算明细</h5></div>-->
        </div>
        <table class="table table-striped">
            <tr>
                <td>商家</td>
                <td>商家ID</td>
                <td>订单编号</td>
                <td>订单完成时间</td>
                <td>订单金额</td>
                <td>菜品原价</td>
                <td>运费</td>
                <td>取餐应付</td>
                <td>取餐实付</td>
                <td>应付实付差额</td>
                <td>线下垫款</td>
                <td>商家类型</td>
                <td>订单类型</td>
                <td>配送员</td>
                <td>配送员ID</td>
            </tr>
    <#if list?exists && (list?size > 0)>
            <#list list as settlementDetail>
                <tr>
                    <td>${settlementDetail.poiName !''}</td>
                    <td>${settlementDetail.poiId !''}</td>
                    <td>${settlementDetail.orderId !''}</td>
                    <td>${settlementDetail.finishTime !''}</td>
                    <td>￥${settlementDetail.orderMoney?string('0.0')}</td>
                    <td>￥${settlementDetail.goodsMoney?string('0.0')}</td>
                    <td>￥${settlementDetail.shippingFee?string('0.0')}</td>
                    <td>￥${settlementDetail.pay?string('0.0')}</td>
                    <td>￥${settlementDetail.actualPayAmount?string('0.0')}</td>
                    <td>￥${settlementDetail.pay-settlementDetail.actualPayAmount}</td>
                    <td><#if settlementDetail.logisticsOfflinePayOrderRecord == 1>是<#else>否</#if></td>
                    <td>
                        <#if settlementDetail.pkgType == 2>代购<#else>普通</#if>
                    </td>
                    <td>
                        <#if settlementDetail.orderType == 0>
                            货到付款
                        <#elseif settlementDetail.orderType == 1>
                            在线支付
                        </#if>
                    </td>
                    <td>${settlementDetail.riderName !''}</td>
                    <td>${settlementDetail.riderId !''}</td>
                </tr>
            </#list>
    </#if>
        </table>

    <#--</div>-->

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/poiSettlement/zb"/>
    </#if>
    </div>
</div>
<script>
	document.title = '商家结算报表详情';
	$('select').select2();
</script>
