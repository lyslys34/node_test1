<#macro output_poi_settlement_query>
    <#if pois?exists>
    &nbsp;&nbsp;商家名称：<select class="form-control input-sm" id="poiId">
        <option value="0">所有商家</option>
    <#list pois as poi>
    <option value="${poi.poiId}" <#if poiId?exists && poiId == poi.poiId>selected</#if>>${poi.poiName}</option>
    </#list>
</select>
</#if>

    &nbsp;&nbsp;
    日期：<input id="query-start-date" style="cursor: pointer" class='form-control input-sm date-picker J-datepicker' readonly="readonly" value="<#if startDate?exists>${startDate?date}<#else>${.now?string('yyyy-MM-dd')}</#if>">
    至 <input id="query-end-date" style="cursor: pointer" class="form-control input-sm date-picker J-datepicker" readonly="readonly" value="<#if endDate?exists>${endDate?date}<#else>${.now?string('yyyy-MM-dd')}</#if>">
    <br/>
    <br/>
    &nbsp;&nbsp;商家类型：<select id = "poiType">
                <option value="0" <#if poiType == 0>selected</#if>>全部</option>
                <option value="1" <#if poiType == 1>selected</#if>>代购</option>
                <option value="2" <#if poiType == 2>selected</#if>>普通</option>
            </select>
&nbsp;&nbsp;&nbsp;&nbsp;<input type = "button" id="query-commit" class="btn btn-success">查看</button>
</#macro>
