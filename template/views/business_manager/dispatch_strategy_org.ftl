<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/business_manager/dispatch_strategy_org.js</content>
<link rel="stylesheet" type="text/css" href="${staticPath}/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />

<style type="text/css">
.ui-autocomplete{
    z-index: 2000;
}
#main-container{
    padding: 10px 20px;
}
</style>

<div id="main-container">
    <div class="panel panel-default">
            <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="get" action="/business/orgList">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">所在城市</label>
                                <select class="form-control input-sm" id="cityId" name="cityId">
                                    <option value = "0">全部</option>
                                    <#if cityList?exists && (cityList?size > 0)>
                                        <#list cityList as city>
                                            <option value="${city.city_id}" <#if cityId?exists && cityId == city.city_id>selected</#if>>${city.name}</option>
                                        </#list>
                                    </#if>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">组织类型</label>
                                <select class="form-control input-sm" id="orgType" name="orgType">
                                    <#if is_org_manager?exists && is_org_manager == 1>
                                        <option value="0"<#if searchOrgType?exists && searchOrgType == 0> selected="selected" </#if>>全部</option>
                                        <option value="2"<#if searchOrgType?exists && searchOrgType == 2> selected="selected" </#if>>加盟</option>
                                    <#else>
                                        <option value="0"<#if searchOrgType?exists && searchOrgType == 0> selected="selected" </#if>>全部</option>
                                        <option value="1"<#if searchOrgType?exists && searchOrgType == 1> selected="selected" </#if>>自建</option>
                                        <option value="2"<#if searchOrgType?exists && searchOrgType == 2> selected="selected" </#if>>加盟</option>
                                        <option value="3"<#if searchOrgType?exists && searchOrgType == 3> selected="selected" </#if>>驻店三方</option>
                                        <option value="4"<#if searchOrgType?exists && searchOrgType == 4> selected="selected" </#if>>众包</option>
                                    </#if>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">组织名称</label>
                                <select class="form-control input-sm" id="orgId" name="orgId">
                                    <option value="0">全部</option>
                                    <#list orgs as org>
                                        <option value="${org.orgId}" <#if orgId?exists && orgId == org.orgId>selected</#if>>${org.orgName}</option>
                                    </#list>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3" style="padding-top:18px;">
                            <button type="submit" class="btn btn-success">查询</button>
                        </div>
                    </div>
                </div>
                <input type = "hidden" name="source" value="submit"/>
            </form>
        </div>

        <div class="panel panel-default table-responsive">
            <div id="breadcrumb">
                <ul class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="/business/orgList"> 全部</a></li>
                    <#if parent??>
                        <#list parent as org>
                            <li class="active"><a href="/business/orgList?parent=${org.orgId}">${org.orgName}</a></li>
                        </#list>
                    </#if>
                </ul>
            </div><!-- /breadcrumb-->
    <table class="table table-striped" id="responsiveTable">
        <thead>
        <tr>
            <th>组织名称</th>
            <th>组织ID</th>
            <th>城市</th>
            <th>组织类型</th>
            <th>组织层级</th>
            <th>调度模式</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <#--循环-->
        <#if parent??>
        <#list result.data.data as org>
            <tr>
                <#if org.isLeaf()>
                    <td>${org.orgName}</td>
                <#else>
                    <td><a name="aname" style="" href="/business/orgList?parent=${org.orgId}">${org.orgName}</a></td>
                </#if>
                <td>${org.orgId!''}</td>
                <td>${org.cityName!''}</td>
                <td>
                    <#if 1 == org.orgType>自建
                    <#elseif 2 == org.orgType>加盟
                    <#elseif 3 == org.orgType>驻店三方
                    <#elseif 4 == org.orgType>众包
                    <#else>
                    </#if>
                </td>
                <td>${org.levelTypeName!''}</td>
                <td>${org.dispatchStrategyName!'其他'}</td>
                <td><#if 230 == org.levelType>
                <a  style="cursor:pointer" data-toggle="tooltip" title="修改" data-placement="top" name="edit" data="{'id':${org.orgId},'dispStrgy':${org.dispatchStrategy}}">
                    <i class="fa fa-edit fa-lg opration-icon"></i>
                </a>
                </#if>
                </td>
            </tr>
        </#list>
        </#if>
        </tbody>
    </table>
    <div style="float: left; padding: 10px; 15px"><span>共${recordCount!'0'}项</span></div>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/business/orgList"/>
    </#if>
</div>
<div class="modal fade" id="edit">
    <div class="modal-dialog">
        <div class="modal-content" style="width:300px;margin:100px;">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="text-center">修改调度模式</h4>
                <div id="editErorInfo" class="alter alert-danger"></div>
                <div style="padding: 20px 0 50px 0;">
                <div class="form-group">
                    <label class="col-sm-5 control-label">当前调度模式</label>
                    <div class="col-sm-5">
                        <label id="dispStrgyText"></label>
                        <input type="hidden" id="editOrgId">
                        <input type="hidden" id="editDispStrgy">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label"> 修改为 </label>
                    <div class="col-sm-5">
                        <select id="dispStrgySelect">
                            <#list dispatchStrategys as dispatchStrategy>
                                <option value="${dispatchStrategy.code}">${dispatchStrategy.type}</option>
                            </#list>
                        </select>
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" id="save" value="提交" class="btn btn-danger btn-block" />
                </div>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="showRes">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" id="resMsg">

            </div>
        </div>
    </div>
</div>
<script>
    document.title = '组织管理';
    $('#cityId').select2();
    $('#orgId').select2();
</script>