<#setting url_escaping_charset="UTF-8">
<#setting number_format="0.##" />

<#-- 前端页面配置文件 start -->
<#include "./widgets/utils.ftl" />

<#-- 静态文件所在的路径 -->
<#assign staticPath = "/static" >

<#include "version.ftl" />

