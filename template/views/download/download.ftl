<#include "config.ftl">
<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">


<div id="main-container">
    <div style="text-align: center; margin-top:100px;">
        <#if appVersion?exists>
            <div style="margin:0 auto;">
                <div style="font-size: 16px; color:#333; font-weight:bold;">扫码下载美团骑手客户端</div>
                <div style="color: #9d9d9d; font-size:12px;">*仅支持安卓3.0 & iOS7.0及以上机型，且手机需具备GPS定位功能</div>
                <img src="${appVersion.qrcodeUrl ! ''}" width="300" height="300" >
                <div style="color: #9d9d9d; font-size:12px;">或访问以下地址下载：</div>
                <#if appVersion.appType==1>
                    <div style="color: #9d9d9d; font-size:12px;">peisong.meituan.com/zhongbao</div>
                <#else>
                    <div style="color: #9d9d9d; font-size:12px;">peisong.meituan.com/qishou</div>
                </#if>
            </div>
        <#else>
        </#if>
    </div>
</div>

<style type="text/css">
    #main-container{
        min-height: 0;
    }
    #wrapper{
        min-height: 0;
    }
</style>