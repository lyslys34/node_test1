<!DOCTYPE HTML>
<html>
<body  >
    <#assign pageVersion = "1a2e6022" />
    <title>下载美团骑手客户端</title>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <link type="text/css" rel="stylesheet" href="/static/css/page/download/handle_download.css?ver=${pageVersion}" />

    <div <#if RequestParameters.appType=='1'> class="zb"<#elseif RequestParameters.appType=='5'> class="jm"<#else> class="zj"</#if>>
    <#if clientType?exists && clientType==1> <!-- weixin -->
        <div class="wxbody w100 tc">
            <#if osType==2> <!-- ios -->
                <#if RequestParameters.appType=='1'>
                    <img src="/static/imgs/download/explain_zb_ios.png?ver=${pageVersion}" class="explainimg">
                <#elseif RequestParameters.appType=='5'>
                    <img src="/static/imgs/download/explain_jm_ios.png?ver=${pageVersion}" class="explainimg">
                <#else>
                    <img src="/static/imgs/download/explain_ios.png?ver=${pageVersion}" class="explainimg">
                </#if>
            <#else>
                <#if RequestParameters.appType=='1'>
                    <img src="/static/imgs/download/explain_zb_android.png?ver=${pageVersion}" class="explainimg">
                <#elseif RequestParameters.appType=='5'>
                    <img src="/static/imgs/download/explain_jm_android.png?ver=${pageVersion}" class="explainimg">
                <#else>
                    <img src="/static/imgs/download/explain_android.png?ver=${pageVersion}" class="explainimg">
                </#if>
            </#if>
        </div>
    <#else>

        <#if err?exists>
            <script type="text/javascript">
            alert("出现异常");
            </script>
        <#else>
            <#if RequestParameters.appType=='1'> <!-- 众包 -->
                <div  id="container" class="container_zb">
                    <div id="dldiv" class="w100 tc dldiv">
                        <#if app?exists>
                            <#if osType==1>
                                <a href="${app.downloadUrl}" class="dlbtn_zb">立即下载</a>
                            <#elseif osType==2>
                                <a href="${app.downloadUrl}" class="dlbtn_zb">立即下载</a>
                            </#if>
                        <#else>
                            <#if osType==2>
                                <a href="javascript:void(0)" class="dlbtn_zb disabled">iPhone版即将上线</a>
                            </#if>
                        </#if>
                    </div>
                </div>
            <#elseif RequestParameters.appType=='5'> <!-- 角马 -->
                <div  id="container" class="container_jm">
                    <div id="dldiv" class="w100 tc dldiv">
                        <#if app?exists>
                            <#if osType==1>
                                <a href="${app.downloadUrl}" class="dlbtn_jm">立即下载</a>
                            <#elseif osType==2>
                                <a href="${app.downloadUrl}" class="dlbtn_jm">立即下载</a>
                            </#if>
                        <#else>
                            <#if osType==2>
                                <a href="javascript:void(0)" class="dlbtn_jm disabled">iPhone版即将上线</a>
                            </#if>
                        </#if>
                    </div>
                </div>
            <#else>
                <div id="container" class="container_zj">
                    <div id="dldiv" class="w100 tc dldiv">
                        <#if app?exists>
                            <#if osType==1>
                                <a href="${app.downloadUrl}" class="dlbtn">立即下载</a>
                            <#elseif osType==2>
                                <a href="${app.downloadUrl}" class="dlbtn">立即下载</a>
                            <#else>
                            </#if>
                        <#else>
                            <#if osType==2>
                                <a href="javascript:void(0)" class="dlbtn disabled">iPhone版即将上线</a>
                            </#if>
                        </#if>
                    </div>
                </div>
            </#if>
        </#if>

    </#if>
    </div>

    <script type="text/javascript" src="/static/js/page/download/handle_download.js?ver=${pageVersion}"></script>
</body>
</html>


