<#include "../config.ftl">
<#include "../widgets/sidebar.ftl">
<title>操作日志</title>

<content tag="cssmain">/log/staff_log.css</content>
<content tag="javascript">/log/staff_log.js</content>

<div id="main-container">
    <h4><#if orgVo?exists>${orgVo.orgName !''}(ID:${orgVo.orgId!''})</#if></h4>
    <#import "/organization/station_nav.ftl" as n>
        <@n.station_nav active=5 orgOperations=orgVo.orgOperations orgVo=orgVo />
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post"
              action="/log/staff">
            <input type="hidden" name="bmUserId" value="${RequestParameters.bmUserId !'0'}">
            <input type="hidden" name="orgId" value="${RequestParameters.orgId !'0'}">

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">操作类型</label>
                            <select class="form-control input-sm" id="js_opType" name="opType">
                                <option value="0">全部</option>
                            <#if opTypeMap?exists>
                                <#list opTypeMap?keys as k>
                                    <option value="${k?string !'0'}"
                                            <#if RequestParameters.opType?exists && RequestParameters.opType == k>selected="selected" </#if> >${opTypeMap[k] !''}</option>
                                </#list>
                            </#if>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">变更内容</label>
                            <input type="text" class="form-control input-sm" name="keyWords" placeholder="请输入要包含的关键字"
                                   value="${RequestParameters.keyWords !''}"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">操作人</label>
                            <select class="form-control input-sm" id="js_opUserId" name="opUserId">
                                <option value="0">全部</option>
                            <#if opUsers??>
                                <#list opUsers as u>
                                    <option value="${u.userId !0}"
                                            <#if RequestParameters.opUserId?exists && RequestParameters.opUserId == u.userId?string >selected="selected" </#if> >${u.userName !''}</option>
                                </#list>
                            </#if>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-5 ">
                        <div class="form-group ">
                            <label class="control-label">操作时间</label>

                            <div class="form-group form-inline">
                                <input type="text" readonly
                                       class="form-control input-sm date-picker J-datepicker js_date_start"
                                       name="timeStartStr"
                                       value="<#if timeStartStr?exists>${timeStartStr}<#else>${.now?string('yyyy-MM-dd')}</#if>"/>
                                <span style="padding:0 5px; line-height:30px;">~</span>
                                <input type="text" readonly
                                       class="form-control input-sm date-picker J-datepicker js_date_end"
                                       name="timeEndStr"
                                       value="<#if timeEndStr?exists>${timeEndStr}<#else>${.now?string('yyyy-MM-dd')}</#if>"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-left">
                <button type="submit" class="btn btn-success">查询</button>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>序号</th>
                <th>操作类型</th>
                <th>变更内容</th>
                <th>操作人</th>
                <th>操作时间</th>
            </tr>
            </thead>
            <tbody>
            <#if logsViewPage?? && logsViewPage.bmOperationLogViewList??>
                <#list logsViewPage.bmOperationLogViewList as log>
                <tr>
                    <td>${log.id!''}</td>
                    <td>${opTypeMap[log.opType?string !'0'] ! ''}</td>
                    <td><#noescape>${log.description ! ''}</#noescape></td>
                    <td>${log.opUname ! ''}</td>
                    <td>
                        <#if log.ctime?exists && log.ctime gt 0>${(log.ctime*1000)?number_to_datetime}</#if>
                    </td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>
        <div style="float: left; padding: 10px; 15px"><span>共${logsViewPage.total!'0'}项</span></div>
    <#import "../page/pager.ftl" as q>
    <#if logsViewPage??>
        <@q.pager pageNo=logsViewPage.pageNum pageSize=logsViewPage.pageSize recordCount=logsViewPage.total toURL="/log/staff"/>
    </#if>

    </div>
</div>
<@commonPop></@commonPop>
<script>
    document.title = '操作记录';
    var opTypeSelect = $("#js_opType");
    var opUserSelect = $("#js_opUserId");
    var mySelect2 = $.fn.select2;
</script>
