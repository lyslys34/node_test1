<#include "../config.ftl">
<#include "../widgets/sidebar.ftl">
<title>人员审核操作日志</title>

<content tag="cssmain">/log/rider_audit_log.css</content>
<content tag="javascript">/log/rider_audit_log.js</content>

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post"
              action="/log/auditLog">
            <div class="panel-body">
                <div class="row">                   
                    <div class="col-md-5 ">
                        <div class="form-group ">
                            <label class="control-label">审核日期</label>
                            <div class="form-group form-inline">
                                <input type="text" readonly
                                       class="form-control input-sm date-picker J-datepicker js_date_start"
                                       name="startTimeStr"
                                       value="<#if startTimeStr?exists>${startTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>"/>
                                <span style="padding:0 5px; line-height:30px;">至</span>
                                <input type="text" readonly
                                       class="form-control input-sm date-picker J-datepicker js_date_end"
                                       name="endTimeStr"
                                       value="<#if endTimeStr?exists>${endTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>"/>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-2">
                    	<div class="form-group">
                    		<label class="control-label">操作人</label>
                    		<input type="text" class="form-control input-sm" name="opUname" value=""/>
                    	</div>
                    </div>
                    <div class="panel-footer text-left" style="padding-top:20px">
		                <input type="button" class="btn btn-success js_submit_btn" value="查询"/>
		            </div>
                </div>
            </div>            
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>审核时间</th>
                <th>操作人</th>
                <th>审核结果</th>
                <th>原因</th>
                <th>配送员</th>
                <th>配送员手机号</th>
            </tr>
            </thead>
            <tbody>
            <#if logsViewPage?? && logsViewPage.bmRiderAuditLogViewList??>
                <#list logsViewPage.bmRiderAuditLogViewList as log>
                <tr>
                    <td>
                    	<#if log.ctime?exists && log.ctime gt 0>${(log.ctime*1000)?number_to_datetime}</#if>
                    </td>
                    <td>${log.opUname !''}</td>
                    <td>${log.opTypeName !''}</td>
                    <td><#noescape>${log.description ! ''}</#noescape></td>
                    <td>${log.rider ! ''}</td>
                    <td>${log.phone !''}</td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>
        <div style="float: left; padding: 10px; 15px"><span>共${logsViewPage.total!'0'}项</span></div>
    <#import "../page/pager.ftl" as q>
    <#if logsViewPage??>
        <@q.pager pageNo=logsViewPage.pageNum pageSize=logsViewPage.pageSize recordCount=logsViewPage.total toURL="/log/auditLog"/>
    </#if>

    </div>
</div>
<script>
    document.title = '审核操作记录';
</script>
