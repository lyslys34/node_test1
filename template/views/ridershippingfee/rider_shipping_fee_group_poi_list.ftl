<#include "config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/ridershippingfee/rider_shipping_fee_group_poi_list.js</content>

</style>

<div id="main-container">
    <div class="panel panel-default">
        <div id="alert_error"></div>
        <!-- <input id="id" name="id" value="${id!""}"> -->
        <input id="id" type="hidden" name="id" value="<#if groupVO??>${groupVO.id!''}</#if>">
        <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                            <span style="font-size:25px;"><#if groupVO??>${groupVO.name!''}</#if></span>
                            <button id="addPoi" type="submit" style="margin-top:-12px" rel="add" class="btn btn-default">添加商家</button>
                            <button id="editGroup" type="submit" style="margin-top:-12px" rel="add" class="btn btn-default">编辑</button>                        
                    </div>
               </div>
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <button id="deletePoi" class="btn btn-danger">删除</button>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>
                    <input class="selectAll" type="checkbox" style="opacity:1;position:relative">
                </th>
                <th>商家名</th>
                <th>商家ID</th>
                <th>插入时间</th>
                <th>操作人</th>
            </tr>
            </thead>
            <tbody>
<!--                 <tr>
                    <td><input class="selectItem" type="checkbox" style="opacity:1;position:relative"></td>
                    <td>test1</td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                </tr> -->
            <#--循环-->
            <#if riderShippingFeeGroupPoi??>
                <#list riderShippingFeeGroupPoi as shippingfeepoi>
                <tr>
                    <td><input class="selectItem" type="checkbox" style="opacity:1;position:relative" value="${shippingfeepoi.wmPoiId!''}"></td>
                    <td>${shippingfeepoi.wmPoiName!''}</td>
                    <td>${shippingfeepoi.wmPoiId!''}</td>
                    <td>${(shippingfeepoi.ctime*1000)?number?number_to_datetime!''}</td>
                    <td>${shippingfeepoi.createUserName!''}</td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>
        

     <div class="modal fade" id="editGroupModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="text-center">编辑组</h4>
                    <div id="alert_edit_group_error"></div>
                    <input type="hidden" id="groupId" value="<#if groupVO??>${groupVO.id!''}</#if>"/>
                    <div class="input-group">
                        <span class="input-group-addon">组名</span>
                        <input type="text" class="form-control" name="groupname" id="groupname" data-minlength="8" value="<#if groupVO??>${groupVO.name!''}</#if>"/>
                    </div><br>

                    <div class="input-group">
                        <span class="input-group-addon">配送费</span>
                        <input type="text" class="form-control input-sm" name="shippingFee" id="shippingFee" data-minlength="8" value="<#if groupVO??>${groupVO.shippingFee!''}</#if>"/>
                    </div><br>

                    <div class="input-group">
                        <span class="input-group-addon">支付方式</span>
                        <span class="input-group-addon" style="background-color:#fff;"><input class="payment" type="radio" style="opacity:1;position:relative" name="paymenttype" id="onlinepayment" value="1" <#if groupVO.paymentType??><#if groupVO.paymentType = 1>checked</#if></#if>>线上支付</span>
                        <span class="input-group-addon" style="background-color:#fff;"><input class="payment" type="radio" style="opacity:1;position:relative" name="paymenttype" id="offlinepayment" value="2" <#if groupVO.paymentType??><#if groupVO.paymentType = 2>checked</#if></#if>>线下支付</span>
                    </div><br>
                    
                    <div class="modal-footer">
                        <input type="submit" id="editGroupSubmit" value="提交" class="btn btn-danger btn-block" />
                    </div>
                </div>
            </div>

        </div>
     </div>
    <div class="modal fade" id="showSuccessMsg">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="resMsg">
                    
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addPoiModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="text-center">添加商家</h4>
                    <div id="alert_add_poi_group_error"></div>
                    <div class="form-group">
                        <textarea id="poi_ids" style="height:200px" class="form-control " ></textarea>
                    </div><br>
                     <div class="modal-footer">
                        <input type="submit" id="addPoiSubmit" value="提交" class="btn btn-danger btn-block" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/ridershippingfee/groupdetail?id=<#if groupVO??>${groupVO.id!''}</#if>"/>
    </#if>
    </div>
</div>

<script>document.title = '配送费用';</script>

