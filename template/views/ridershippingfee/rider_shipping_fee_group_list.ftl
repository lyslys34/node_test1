<#include "config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/ridershippingfee/rider_shipping_fee_group_list.js</content>

</style>

<div id="main-container">
    <div class="panel panel-default">
        <div id="alert_error"></div>
        <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                            <span style="font-size:25px;">运费配置</span>
                    </div>
               </div>
        </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <form style="display:inline" class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post" action="/ridershippingfee/grouplist">
                            <label class="control-label">组名称：</label>
                            <input type="text" class=" input-sm parsley-validated" name="groupName" id="groupName" data-minlength="8" value=""/>&nbsp;&nbsp;
                            <button id="search" type="submit" class="btn btn-success">查询</button>&nbsp;&nbsp;
                        </form>
                        
                        <button id="addGroup" rel="add" class="btn btn-default">添加组</button>
                    </div>
                </div>
            </div>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th></th>
                <th>组名</th>
                <th>商家数</th>
                <th>运费</th>
                <th>支付方式</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <!-- <tr>
                    <td></td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                    <td>
                        <button class="btn btn-xs btn-info manageSubsidy" data-toggle="dropdown" rel="1">管理</button>  
                    </td>
                </tr> -->
            <#--循环-->
            <#if riderShippingFeeGroup??>
                <#list riderShippingFeeGroup as group>
                <tr>
                    <td></td>
                    <td>${group.name!''}</td>
                    <td>${group.poiCount!''}</td>
                    <td>${group.shippingFee!''}</td>       
                    <td><#if group.paymentType??><#if group.paymentType = 1>线上支付<#elseif group.paymentType = 2>线下支付<#else>未知支付方式</#if></#if></td>              
                    <td>
                        <a data-toggle="tooltip" title="管理" data-placement="top" class="manageShippingFee" rel="${group.id!''}" style="cursor:pointer">
                            <i class="fa fa-cog fa-lg opration-icon"></i>
                        </a>  
                    </td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>
        

     <div class="modal fade" id="addGroupModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="text-center">添加组</h4>
                    <div id="alert_add_group_error"></div>
                    <div class="input-group">
                        <span class="input-group-addon">组名</span>
                        <input type="text" class="form-control" name="group_name" id="group_name" data-minlength="8" value=""/>
                    </div><br>

                    <div class="input-group">
                        <span class="input-group-addon">配送费</span>
                        <input type="text" class="form-control" name="shippingFee" id="shippingFee" data-minlength="8" value=""/>
                    </div><br>

                    <div class="input-group">
                        <span class="input-group-addon">支付方式</span>
                        <span class="input-group-addon" style="background-color:#fff;"><input class="payment" type="radio" style="opacity:1;position:relative" name="paymenttype" id="onlinepayment" value="1" checked>线上支付</span>
                        <span class="input-group-addon" style="background-color:#fff;"><input class="payment" type="radio" style="opacity:1;position:relative" name="paymenttype" id="offlinepayment" value="2">线下支付</span>
                    </div><br>
                    <div class="modal-footer">
                        <input type="submit" id="addGroupSubmit" value="提交" class="btn btn-danger btn-block" />
                    </div>
                </div>
            </div>

        </div>
     </div>
    <div class="modal fade" id="showAddGroupRes">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="resMsg">
                    
                </div>
            </div>
        </div>
    </div>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/ridershippingfee/grouplist"/>
    </#if>
    </div>
</div>

<script>document.title = '运费配置';</script>

