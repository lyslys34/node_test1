<#include "../config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="pageName">riderefficiency</content>
<content tag="javascript">/statistics/rider_efficiency.js</content>
<content tag="cssmain">/statistics/rider_efficiency.css</content>

<div id="main-container">
    <div class="content_body">
        <div class="condition_area">
            <input type="button" data="30" class="btn btn-sm btn-default js_time_btn <#if RequestParameters.t?exists && RequestParameters.t == '30'>active</#if>"  value="最近一月" /> &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="button" data="7" class="btn btn-sm btn-default js_time_btn <#if !RequestParameters.t?exists || (RequestParameters.t?exists && RequestParameters.t == '7')>active</#if>" value="最近一周" /> &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="button" data="3" class="btn btn-sm btn-default js_time_btn <#if RequestParameters.t?exists && RequestParameters.t == '3'>active</#if>" value="最近三天" />
            <input type="hidden" id="js_time_type" value="${RequestParameters.t!'7'}" />
        <#if orgs?exists>
            &nbsp;&nbsp;组织名称：<select class="form-control input-sm" style="width: 200px; display: inline-block;" id="orgId">
            <#list orgs as org>
                <option value="${org.orgId}" <#if orgId?exists && orgId == org.orgId>selected</#if>>${org.orgName}</option>
            </#list>
        </select>
        </#if>
        </div>


        <div>
            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            <div class="line"></div>
            <div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            <div class="line"></div>
            <div id="js_table" class="table_area">

            </div>
            <div class="export_area">
                <a href="efficiencyExport?t=${RequestParameters.t!'7'}" target="_blank" class="btn btn-default" id="exportData">导出</a>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    var postAndChart;
    var dataProcessing;
    $('#orgId').select2();
    $('#orgId').on('change', function(){
        postAndChart();
    });
</script>
