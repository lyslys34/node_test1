<#include "../config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/statistics/date.js</content>
<content tag="cssmain">/statistics/daily_data.css</content>
<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/statistics/dailyData" method="get" >
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">日期(格式：2015-04-26)：</label>
                            <input type="text" name="ts" style="cursor: pointer;" readonly="readonly" value="<#if ts?exists>${ts}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">组织id</label>
                            <select class="form-control input-sm" id="orgId" name="orgId">
                                <#list orgs as org>
                                <option value="${org.orgId !''}" <#if org.orgId == orgId > selected="selected" </#if> >${org.orgName !''}</option>
                                </#list>
                            </select></div>
                    </div>
                    <div class="col-md-3" style="padding-top:18px;">
                        <button type="submit" class="btn btn-success">查询</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="/statistics/excelexport?ts=${ts!''}&orgId=${orgId!''}"><button type="button" class="btn btn-success" >导出EXCEL</button></a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>配送日期</th>
                <th>订单显示ID</th>
                <th width="100">商家名称</th>
                <th>支付方式</th>
                <th>订单原价</th>
                <th>付商家款</th>
                <#--<th>商家返利</th>-->
                <th>配送费</th>
                <th>配送时长</th>
                <th>配送员</th>
                <th>配送员手机号</th>
                <th>是否预订单</th>
                <th width="80">预订单时间差距(分钟)</th>
                <th>收货人电话</th>
                <th width="100">收货人地址</th>
                <th>状态</th>
            </tr>
            </thead>
        <#if (page.dailyDataViewList)?exists && (page.dailyDataViewList?size > 0) >
            <tbody id="tbody-dispatcher-list">
                <#list page.dailyDataViewList as d>
                <tr class="dispatcher">
                    <td class="">${d.date !''}</td>
                    <td class="">${d.orderViewId !''}</td>
                    <td class="">${d.poiName !''}</td>
                    <td class="">${d.payType !''}</td>
                    <td class="">${d.originPrice !''}</td>
                    <td class="">${d.offordToPoiAmount !''}</td>
                    <#--<td class="">${d.backToPoiAmount !''}</td>-->
                    <td class="">${d.deliveryFee !''}</td>
                    <td class="">${d.waybillTimeDis !''}</td>
                    <td class="">${d.riderName !''}</td>
                    <td class="">${d.riderPhone !''}</td>
                    <td class="">${d.isOrderCommet !''}</td>
                    <td class="">${d.orderTimeDis !''}</td>
                    <td class="">${d.recipientPhone !''}</td>
                    <td class="">${d.receiveAddress !''}</td>
                    <td class="">
                        <#if d.status?exists && d.status == 50 ><span style="color:#259b24">${d.statusCommet !''}</span>
                        <#elseif d.status?exists && d.status lt 50> <span style="">${d.statusCommet !''}</span>
                        <#elseif d.status?exists && d.status == 99> <span style="color: #9d9d9d">${d.statusCommet !''}</span>
                        <#else> <span style="color:#d58512">${d.statusCommet !''}</span>
                        </#if>
                    </td>
                </tr>

                </#list>
            </tbody>
        </#if>
        </table>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/statistics/dailyData"/>
    </#if>
    </div>
</div>
<script>
    document.title = '配送订单表';
    $('select').select2();
</script>
