<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/statistics/business_data_chart.js</content>
<content tag="cssmain">/statistics/business_data_chart.css</content>
<content tag="pageName">business_data_chart</content>

<input id="category" type = "hidden" value = "${dateStr !''}"/>
<input id="tradingVolume" type = "hidden" value = "${tradingVolume !''}"/>
<input id="orderCount" type = "hidden" value = "${orderCount !''}"/>
<input id="averageOrder" type = "hidden" value = "${averageOrder !''}"/>
<input id="poiCount" type = "hidden" value = "${poiCount !''}"/>
<input id="tradingVolumePerPerson" type = "hidden" value = "${tradingVolumePerPerson !''}"/>
<input id="orderPerPerson" type = "hidden" value = "${orderPerPerson !''}"/>
<div id="main-container">
<div class="content_body">
    <div class="condition_area">
        <input name="timeBtn" type="button" data-type="3" class="btn btn-default js_time_btn <#if timeType == 3>active</#if>" value="最近一月" /> &nbsp;&nbsp;&nbsp;&nbsp;
        <input name="timeBtn" type="button" data-type="2" class="btn btn-default js_time_btn <#if timeType == 2>active</#if>" value="最近一周" /> &nbsp;&nbsp;&nbsp;&nbsp;
        <input name="timeBtn" type="button" data-type="1" class="btn btn-default js_time_btn <#if timeType == 1>active</#if>"  value="最近三天" /> &nbsp;&nbsp;&nbsp;&nbsp;
    <#if orgs?exists>
        &nbsp;&nbsp;组织名称：<select class="form-control input-sm" style="width: 200px; display: inline-block;" id="orgId">
        <#list orgs as org>
            <option value="${org.orgId}" <#if orgId?exists && orgId == org.orgId>selected</#if>>${org.orgName}</option>
        </#list>
    </select>
    </#if>

    <#if pois?exists>
        &nbsp;&nbsp;商家名称：<select class="form-control input-sm" style="width: 200px; display: inline-block;" id="poiId">
        <option value="0">所有商家</option>
        <#list pois as poi>
            <option value="${poi.poiId}" <#if poiId?exists && poiId == poi.poiId>selected</#if>>${poi.poiName}</option>
        </#list>
    </select>
    </#if>
    </div>
    <#--<div>-->
        <div id="container" style="width: 100%; height: 400px; "></div>
        <div id="averageContainer" style="width: 100%; height: 400px; "></div>
        <#if list?exists && (list?size > 0)>
            <table class="table">
                <tr>
                    <th>日期</th>
                    <th>订单总数</th>
                    <th>交易额总数</th>
                    <th>订单均价</th>
                    <th>交易商家总数</th>
                    <th>活跃骑手人均交易额(元)</th>
                    <th>活跃骑手人均订单数</th>
                </tr>
                <#list list as ele>
                    <#assign lsize = list?size>
                    <tr>
                        <td>${ele.date !''}</td>
                        <td>${ele.orderTotal}</td>
                        <td>${ele.tradingVolume}</td>
                        <#if ele_index == lsize - 1><td>--</td> <#else><td>${ele.orderAveragePrice}</td></#if>
                        <td>${ele.poiCount}</td>
                        <td>${ele.tradingVolumePerPerson}</td>
                        <td>${ele.orderCountPerPerson}</td>
                    </tr>
                </#list>
            </table>
        </#if>
    <#--</div>-->


    <div class="export_area">
        <a href="javascript:void(0);"  class="btn btn-default" id="excelReport">导出</a>
    </div>

</div>


</div>

<script type="text/javascript">
    document.title = '商家名称';
    $('#poiId').select2();
    $('#orgId').select2();
    $('#poiId').on('change', function(){
        var timeType = 1;
        $('input[name=timeBtn]').each(function(){
            if ($(this).hasClass('active')) {
                timeType = $(this).attr('data-type');
            }
        });
        window.location.href = "/partner/businessDataChart?timeType="+ timeType + "&poiId=" + $('#poiId option:selected').val() + "&orgId=" + $('#orgId option:selected').val();
    });

    $('#orgId').on('change', function(){
        var timeType = 1;
        $('input[name=timeBtn]').each(function(){
            if ($(this).hasClass('active')) {
                timeType = $(this).attr('data-type');
            }
        });

        window.location.href = "/partner/businessDataChart?timeType="+ timeType + "&poiId=0" +  "&orgId=" + $('#orgId option:selected').val();
    });
</script>