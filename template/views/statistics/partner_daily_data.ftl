<#include "../config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/statistics/partner_daily_data.js</content>
<content tag="cssmain">/statistics/partner_daily_data.css</content>


<div id="main-container">

    <div class="content_body">
        <div class="temp"></div>
    <#if error?exists>
        <div class="alert small alert-warning alert-dismissible" style="<#if error?exists>display:block;<#else>display:none</#if>" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        ${error !''}
        </div>
    </#if>

    <#if tip?exists>
        <p style="font-size: 16; padding:20px 100px; color: #d58512">${tip !''}</p>
    <#else >
        <form id="fm" class="form-inline" method="get" action="dailyData">
            <input type="hidden" name="pageNo" value="" class="js_page_num">
            <#-- 上方搜索选项 -->
            <div class="condition_area">
                    <div class="form_row">
                        <div class="row_item">
                            <span class="input_title">组织：</span>
                            <div>
                                <select name="orgId" class="form-control input-sm js_orgId">
                                    <#if orgList?exists>
                                        <option value="-1" <#if !(RequestParameters.isOrder?exists) || RequestParameters.isOrder == '-1'>selected="selected"</#if> >全部</option>
                                        <#list orgList as o>
                                            <option value="${o.orgId !'-1'}" <#if RequestParameters.orgId?exists && RequestParameters.orgId == o.orgId?string>selected="selected"</#if> >${o.orgName ! ''}</option>
                                        </#list>
                                    </#if>
                                    <#-- 可能是错误代码，我先注释掉了@liupd -->
                                    <#-- <option value="" -->
                                </select>
                            </div>
                        </div>
                        <div class="row_item">
                            <span class="input_title">配送员：</span>
                            <div>
                                <#--<#if isOrgManager?? && isOrgManager>-->
                                <#--<select name="riderId" class="form-control input-sm js_rider_id">-->
                                    <#--<option value="0" <#if !RequestParameters.riderId?exists || RequestParameters.riderId == ''>selected="selected" </#if>>全部</option>-->
                                    <#--<#if userList?exists>-->
                                        <#--<#list userList as u>-->
                                            <#--<option value="${u.id !''}" <#if RequestParameters.riderId?exists && RequestParameters.riderId == u.id?string>selected="selected" </#if> >${u.name} (${u.mobile})</option>-->
                                        <#--</#list>-->
                                    <#--</#if>-->
                                <#--</select>-->
                                <#--<#else>-->
                                <input type="text" name="riderName" autocomplete="off" class="form-control input-sm js_rider_name input_short" id="riderIdInput" value="${(RequestParameters.riderName)!''}" placeholder="请输入骑手姓名或手机号" />
                                <input type="hidden" name="riderId" class="js_rider_id" value="${(RequestParameters.riderId)!'0'}" />
                                <i id="riderLoading" class="fa fa-refresh fa-spin hidden"></i>
                                <#--</#if>-->
                            </div>
                        </div>

                        <div class="row_item">
                            <span class="input_title">商家名称：</span>
                            <div>
                                <#--<#if isOrgManager?? && isOrgManager>-->
                                <#--<select name="platformPoiId" class="form-control input-sm js_platformPoiId" style="width:200px;">-->
                                    <#--<option value="" <#if !RequestParameters.platformPoiId?exists || RequestParameters.platformPoiId == ''>selected="selected" </#if>>全部</option>-->
                                    <#--<#if poilist?exists>-->
                                        <#--<#list poilist as p>-->
                                            <#--<option value="${p.poiId !''}" <#if RequestParameters.platformPoiId?exists && RequestParameters.platformPoiId == p.poiId>selected="selected" </#if> >${p.poiName}</option>-->
                                        <#--</#list>-->
                                    <#--</#if>-->
                                <#--</select>-->
                                <#--<#else>-->
                                <input type="text" name="poiName" autocomplete="off" class="form-control input-sm js_poi_name input_short" id="poiName" value="${(RequestParameters.poiName)!''}" placeholder="请输入商家名称或商家ID" />
                                <input type="hidden" name="platformPoiId" class="js_platformPoiId" value="${(RequestParameters.platformPoiId)!''}" />
                                <i id="poiNameLoading" class="fa fa-refresh fa-spin hidden"></i>
                                <#--</#if>-->
                                <input type="hidden" name="recipientPhone" value="" maxlength="12" class="form-control input-sm js_phone input_short" />
                            </div>
                        </div>
                    </div>
                    <div class="form_row">
                        <div class="row_item">
                            <span class="input_title">支付方式：</span>
                            <div>
                                <select name="payType" class="form-control input-sm js_payType">
                                    <option value="-1" <#if !RequestParameters.payType?exists || RequestParameters.payType == '-1'>selected="selected"</#if> >全部</option>
                                    <option value="0" <#if RequestParameters.payType?exists && RequestParameters.payType == '0'>selected="selected"</#if> >货到付款</option>
                                    <option value="1" <#if RequestParameters.payType?exists && RequestParameters.payType == '1'>selected="selected"</#if> >在线支付</option>
                                </select>
                            </div>
                        </div>
                        <div class="row_item">
                            <span class="input_title">是否预订单：</span>
                            <div>
                                <select name="isOrder" class="form-control input-sm js_isOrder">
                                    <option value="-1" <#if !RequestParameters.isOrder?exists || RequestParameters.isOrder == '-1'>selected="selected"</#if> >全部</option>
                                    <option value="1" <#if RequestParameters.isOrder?exists && RequestParameters.isOrder == '1'>selected="selected"</#if> >是</option>
                                    <option value="0" <#if RequestParameters.isOrder?exists && RequestParameters.isOrder == '0'>selected="selected"</#if> >否</option>
                                </select>
                            </div>
                        </div>
                        <div class="row_item">
                            <span class="input_title">是否线下结算：</span>
                            <div>
                                <select name="pkgType" class="form-control input-sm js_pkgType">
                                    <option value="-1" <#if !RequestParameters.pkgType?exists || RequestParameters.pkgType == '-1'>selected="selected"</#if> >全部</option>
                                    <option value="1" <#if RequestParameters.pkgType?exists && RequestParameters.pkgType == '1'>selected="selected"</#if> >是</option>
                                    <option value="0" <#if RequestParameters.pkgType?exists && RequestParameters.pkgType == '0'>selected="selected"</#if> >否</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="form_row">
                        <div class="row_item">
                            <span class="input_title">订单号：</span>
                            <div>
                                <input type="text" name="platformOrderId" value="${(RequestParameters.platformOrderId)!''}" maxlength="35" class="form-control input-sm js_order_id input_short" />
                            </div>
                        </div>
                        <div class="row_item">
                            <span class="input_title">运单状态：</span>
                            <div>
                                <select name="status" class="form-control input-sm js_status">
                                    <option value="-1" <#if !RequestParameters.status?exists || RequestParameters.status == '-1'>selected="selected"</#if> >全部</option>
                                    <#list packageStatuses?keys?sort as key>
                                    <option value="${key}" <#if RequestParameters.status?exists && RequestParameters.status == key>selected="selected"</#if> >${packageStatuses[key]}</option>
                                    </#list>
                                </select>
                            </div>
                        </div>
                        <div class="row_item">
                            <span class="input_title">顾客配送评分：</span>
                            <div>
                                <select name="commentScore" class="form-control input-sm js_commentScore">
                                    <option value="-1"  <#if RequestParameters.commentScore?exists && RequestParameters.commentScore == '-1'>selected="selected"</#if>>全部</option>
                                    <option value="1" <#if RequestParameters.commentScore?exists && RequestParameters.commentScore == '1'>selected="selected"</#if>>1分</option>
                                    <option value="2" <#if RequestParameters.commentScore?exists && RequestParameters.commentScore == '2'>selected="selected"</#if>>2分</option>
                                    <option value="3" <#if RequestParameters.commentScore?exists && RequestParameters.commentScore == '3'>selected="selected"</#if>>3分</option>
                                    <option value="4" <#if RequestParameters.commentScore?exists && RequestParameters.commentScore == '4'>selected="selected"</#if>>4分</option>
                                    <option value="5" <#if RequestParameters.commentScore?exists && RequestParameters.commentScore == '5'>selected="selected"</#if>>5分</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form_row">
                        <div class="row_item">
                            <span class="input_title">开始日期：</span>
                            <div class="date_box">
                                <input type="text" name="ts" value="<#if ts?exists>${ts}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start" maxlength="10"  readonly="readonly"  style="width: 60%; cursor: pointer;">
                            </div>
                        </div>
                        <div class="row_item">
                            <span class="input_title">结束日期：</span>
                            <div class="date_box">
                                <input type="text" name="te" value="<#if te?exists>${te}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end" maxlength="10"  readonly="readonly"  style="width: 60%; cursor: pointer;">
                            </div>
                        </div>
                        <#--<#if is_org_manager?? && is_org_manager==0>-->
                            <#--<div class="row_item">-->
                                <#--<span class="input_title">商家配送评分：</span>-->
                                <#--<div>-->
                                    <#--<select name="merchantCommentScore" class="form-control input-sm js_merchantCommentScore">-->
                                        <#--<option value="-1"  <#if RequestParameters.merchantCommentScore?exists && RequestParameters.merchantCommentScore == '-1'>selected="selected"</#if>>全部</option>-->
                                        <#--<option value="1" <#if RequestParameters.merchantCommentScore?exists && RequestParameters.merchantCommentScore == '1'>selected="selected"</#if>>1分</option>-->
                                        <#--<option value="2" <#if RequestParameters.merchantCommentScore?exists && RequestParameters.merchantCommentScore == '2'>selected="selected"</#if>>2分</option>-->
                                        <#--<option value="3" <#if RequestParameters.merchantCommentScore?exists && RequestParameters.merchantCommentScore == '3'>selected="selected"</#if>>3分</option>-->
                                        <#--<option value="4" <#if RequestParameters.merchantCommentScore?exists && RequestParameters.merchantCommentScore == '4'>selected="selected"</#if>>4分</option>-->
                                        <#--<option value="5" <#if RequestParameters.merchantCommentScore?exists && RequestParameters.merchantCommentScore == '5'>selected="selected"</#if>>5分</option>-->
                                    <#--</select>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</#if>-->

                    </div>
                    <div class="form_row">
                        <div class="button_box">
                            <a type="button" class="btn btn-success js_submit_btn">
                                <i id="submitLoading" class="fa fa-refresh fa-spin hidden"></i>
                                查询
                            </a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="button" value="导出" class="btn btn-success js_export_btn" />
                        </div>
                    </div>
                <#-- </table> -->
            </div>

            <#-- <div class="line"></div> -->
            <#-- 下方搜索结果列表 -->
            <table class="table result_list">
                <thead>
                <tr>
                    <th>订单号</th>
                    <th style="max-width:60px;">商家<br/>流水号</th>
                    <th>商家名称</th>
                    <th>城市</th>
                    <th>骑手</th>
                    <th>站点</th>
                    <th>商圈</th>
                    <th style="max-width:40px;">支付方式</th>
                    <th>预订单</th>
                    <th style="max-width:40px;">线下结算</th>
                    <th>配送时效</th>
                    <th style="min-width:90px;">顾客配送评分</th>
                    <#if is_org_manager?? && is_org_manager==0>
                        <th style="min-width:90px;">商家配送评分</th>
                    </#if>

                    <th>运单状态</th>
                    <th>订单金额</th>
                    <th>付商家款</th>
                    <th>收用户款</th>
                    <th>配送费</th>
                    <th>下单时间</th>
                    <th style="min-width:90px;">期望送达时间</th>
                    <#--<th>预订单时间差距(分钟)</th>
                    <th>收货人电话</th>-->

                </tr>
                </thead>
                <#if (page.dailyDataViewList)?exists && (page.dailyDataViewList?size > 0) >
                    <tbody id="tbody-dispatcher-list">
                        <#list page.dailyDataViewList as d>
                        <tr <@output_props obj = d /> class="dispatcher">
                            <td class=""><span class="waybillid blue" value="${d.waybillid!''}">${d.platformOrderId !''}</span></td>
                            <td>${d.poiSeq!''}</td>
                            <td style="width:100px;">
                            <#if is_org_manager?? && is_org_manager==1>
                                <a class="blue poiLink" value="${d.poiId!''}" target="_blank" href="http://waimai.meituan.com/restaurant/${d.poiId!''}">
                            <#else>
                                <a class="blue poiLink" value="${d.poiId!''}" target="_blank" href="http://bd.waimai.sankuai.com/honeycomb/wmpoi/poi/r/find?senior=1&p_poi_id=${d.poiId!''}">
                            </#if>${d.poiName !''}</a></td>
                            <td>${d.cityName!''}</td>
                            <td class=""><a title="${d.riderPhone!''}">${d.riderName !''}</a></td>
                            <td style="width:100px;">${d.orgName !''}</td>
                            <td style="width:100px;">${d.deliveryAreaName !''}</td>
                            <td class="">${d.payType !''}</td>
                            <td class="">${d.isOrderCommet !''}</td>
                            <td>
                                <#if d.pkgType?exists && d.pkgType == 1>是<#else>否</#if>
                            </td>
                            <td class="">${d.waybillTimeDis !''}</td>
                            <td class="">
                                <#if d.commentScore?exists && d.commentScore gt 0>
                                    <#list 1..d.commentScore as i>
                                        <i class="fa fa-star fa-lg" style="color:#ffc609; letter-spacing: -3px;"></i>
                                    </#list>
                                <#else>
                                    未评分
                                </#if>
                            </td>
                            <#if is_org_manager?? && is_org_manager==0>
                                <td class="">
                                    <#if d.merchantCommentScore?exists && d.merchantCommentScore gt 0>
                                        <#list 1..d.merchantCommentScore as i>
                                            <i class="fa fa-star fa-lg" style="color:#ffc609; letter-spacing: -3px;"></i>
                                        </#list>
                                    <#else>
                                        未评分
                                    </#if>
                                </td>
                            </#if>
                            <td class="">
                                <#if d.status?exists && d.status == 50 ><span style="color:#259b24">${d.statusCommet !''}</span>
                                <#elseif d.status?exists && d.status lt 50> <span style="">${d.statusCommet !''}</span>
                                <#elseif d.status?exists && d.status == 99> <span style="color: #9d9d9d">${d.statusCommet !''}</span>
                                <#else> <span style="color:#d58512">${d.statusCommet !''}</span>
                                </#if>
                            </td>
                            <td class="">${d.pkgPrice !''}</td>
                            <td class="">${d.offordToPoiAmount !''}</td>
                            <td class="">${d.planChargeAmount !''}</td>
                            <td>${d.deliveryFee}</td>
                            <td class="">${d.platformOrderTime !''}</td>
                            <td>${d.deliveredTime !''}</td>
                            <#--<td class="">${d.orderTimeDis !''}</td>
                            <td class="">${d.recipientPhone !''}</td>-->

                        </tr>

                        </#list>
                    </tbody>
                    <#if (page.total)?exists && (page.pageSize)?exists && (page.pageNo)?exists && page.total gt 0 && page.pageSize gt 0 && page.pageNo gt 0  >
                        <tfoot class="result_tfoot">
                        <tr>
                            <#assign pageCount = (page.total/page.pageSize)?ceiling   />
                            <#assign size = 6 />
                            <td colspan="14" style="text-align:center">

                                <a href="javascript:void(0)" value="1" class="btn btn-sm btn-default js_page_btn">首页</a>
                                <#if page.pageNo gt size+1><span >...</span></#if>

                                <#if page.pageNo lte size>
                                    <#if pageCount - page.pageNo gt size>
                                        <#list 1..page.pageNo+size as i >
                                            <a href="javascript:void(0)" <#if page.pageNo == i>disabled="disabled"</#if> value="#{i}" class="btn btn-sm btn-default js_page_btn">#{i}</a>
                                        </#list>
                                    </#if>
                                    <#if pageCount - page.pageNo lte size>
                                        <#list 1..pageCount as i >
                                            <a href="javascript:void(0)" <#if page.pageNo == i>disabled="disabled"</#if> value="#{i}" class="btn btn-sm btn-default js_page_btn">#{i}</a>
                                        </#list>
                                    </#if>
                                </#if>
                                <#if page.pageNo gt size>
                                    <#if pageCount - page.pageNo gt size>
                                        <#list page.pageNo-size..page.pageNo+size as i >
                                            <a href="javascript:void(0)" <#if page.pageNo == i>disabled="disabled"</#if> value="#{i}" class="btn btn-sm btn-default js_page_btn">#{i}</a>
                                        </#list>
                                    </#if>
                                    <#if pageCount - page.pageNo lte size>
                                        <#list page.pageNo-size..pageCount as i >
                                            <a href="javascript:void(0)" <#if page.pageNo == i>disabled="disabled"</#if> value="#{i}" class="btn btn-sm btn-default js_page_btn">#{i}</a>
                                        </#list>
                                    </#if>
                                </#if>

                                <#if pageCount - page.pageNo gt size><span >...</span></#if>
                                <a href="javascript:void(0)" value="#{pageCount}" class="btn btn-sm btn-default js_page_btn">尾页</a>&nbsp;&nbsp;&nbsp;
                                <span style="display: inline-block; line-height: 28px;">共#{page.total !'0'}条</span>
                            </td>
                        </tr>

                        </tfoot>
                    </#if>
                </#if>
            </table>

        </form>
    </#if>

    </div>
</div>
<script type="text/javascript" >
    document.title = '配送订单表';
	$('select').select2({width: '60%'});
	var userId  = "${userId!''}";
</script>
