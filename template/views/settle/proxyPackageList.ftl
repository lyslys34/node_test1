<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">

<script>
    function export1(acti) {
        var form1 = document.getElementById("formValidate1");
        form1.action = acti;
        form1.submit();
 //       alert(1);
    }
</script>

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/settle/proxyOrderList" method="post" >
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">下单起始时间：</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="beginTimeStr" id="beginTimeStr" data-required="true" data-minlength="8" value="${beginTimeStr!''}"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">下单截止时间：</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="endTimeStr" id="endTimeStr" data-required="true" data-minlength="8" value="${endTimeStr!''}"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">配送方</label>
                            <select class="form-control input-sm" id="orgId" name="orgId">
                                <option value="0" <#if orgId == 0 > selected="selected" </#if> >全部</option>
                            <#list orgs as org>
                                <option value="${org.orgId !''}" <#if org.orgId == orgId > selected="selected" </#if> >${org.orgName !''}</option>
                            </#list>
                            </select>
                        </div>
                    </div>
                    <#--<div class="col-md-4">-->
                        <#--<div class="form-group">-->
                            <#--<label class="control-label">渠道经理</label>-->
                            <#--<input type="text" class="form-control input-sm parsley-validated" name="manager" id="manager" data-required="true" data-minlength="8" value="${manager!''}"/>-->
                        <#--</div>-->
                    <#--</div>-->
                </div>
            </div>
            <div class="panel-footer text-left">
                <button type="button" class="btn btn-success" onclick="javascript:export1('/settle/proxyOrderList')">查询</button>
                <button type="button" class="btn btn-success" onclick="javascript:export1('/settle/excelexport')">导出EXCEL</button>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>订单显示ID</th>
                <th>商家ID</th>
                <th>商家名称</th>
                <th>订单类型</th>
                <th>下单时间</th>
                <th>配送方ID</th>
                <th>配送方名称</th>
                <#--<th>渠道经理</th>-->
                <th>订单原价（元）</th>
                <th>订单价格（元）</th>
                <th>配送费金额（元）</th>
                <th>邮资金额（元）</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list packages as package>
                <tr>
                    <td>${package.platformOrderId!''}</td>
                    <td>${package.platformPoiId!''}</td>
                    <td>${package.platformPoiName!''}</td>
                    <td>在线支付</td>
                    <td>${package.ctimeStr!''}</td>
                    <td>${package.orgId!''}</td>
                    <td>${package.name!''}</td>
                    <#--<td>${package.manager!''}</td>-->

                    <td>${package.pkgValue!''}</td>
                    <td>${package.pkgPrice!''}</td>
                    <td>${package.deliveryFee!''}</td>
                    <td>${package.postage!''}</td>
                </tr>
            </#list>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/settle/proxyOrderList"/>
    </#if>

    </div>
</div>
<script>document.title = '代理订单列表';</script>
