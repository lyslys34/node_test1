<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">

<!-- Color box -->
<link href="/static/css/lib/colorbox/colorbox.css" rel="stylesheet">
<script src='/static/js/lib/jquery.colorbox.min.js'></script>

<script>
	$(function()	{
		//Colorbox 
		$('.gallery-zoom').colorbox({
			maxWidth:'100%',
			width:'1024px'
		});
	});
</script>

<div id="main-container" >
    <div style="margin-top:20px;margin-left:20px;margin-right:20px">
        <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <#if pictures??>
                                <#list pictures as pic>
                                    <a class="gallery-zoom" rel='gal' href="${pic}">
                                        <img src="${pic}" style="width:1024px;max-height: 1024px;margin-top:40px;">
                                    </a>
                                </#list>
                            </#if>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<script>document.title = '合同浏览';</script>
