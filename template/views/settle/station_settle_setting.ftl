<#include "settle_setting_config.ftl">
<#include "../widgets/sidebar.ftl">

<!--<link rel="stylesheet" type="text/css" href="/static/css/lib/bootstrap.css" />
<script type="text/javascript" src="/static/js/lib/jquery.js"></script>-->
<content tag="javascript">/payment/station_settle_setting.js</content>
<link rel="stylesheet" type="text/css" href="/static/css/lib/touchspin/jquery.bootstrap-touchspin.min.css" />
<link href="/static/css/lib/bootstrap-nav-wizard.css" rel="stylesheet">
<script>
$(document).ready(function(){
	//初始化数字控件
	$("#postage").TouchSpin({
        min: 0,
        max: 999999999,
        step: 0.01,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '￥'
    });

    $("#minMoney").TouchSpin({
        min: 0,
        max: 999999999,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '￥'
    });
});
</script>

<style type="text/css">
.form-horizontal .control-label {
    text-align: left;
}

input[type="radio"] {
  opacity: 1;
}
</style>

<div id="main-container">
    <h4><#if org?exists>${org.orgName !''}(ID:${org.orgId!''})</#if></h4>
    <#if create?? && create>
    	<#import "/organization/station_create_progress.ftl" as p>
		<@p.progress length=3 step=3 />
    <#else><#import "/organization/station_nav.ftl" as n>
    <@n.station_nav active=2 orgOperations=org.orgOperations orgVo=org /></#if>

	<div id="info" style="margin:30px 100px;padding:20px;">
		<input type = "hidden" value="${type!""}" id="type" >
	    <form id="data_form" class="form-horizontal" enctype="multipart/form-data" action="<#if partner?? && partner >/partner/settle/saveSetting<#else>/settle/saveSetting</#if>" method="post">
	    <!--<#if type==1 ><p style="color:red;">选填项不填或填写错误将会导致无法打款成功，请慎重填写</p></#if> -->
		  <div class="form-group">
		    <!-- <label class="col-sm-2 control-label"><span style="color:red;">*</span><#if type == 1 >组织ID:<#else>骑手ID:</#if></label> -->
		    <div class="col-sm-4">
		      <!--<input type="text" class="form-control" id="" placeholder="" disabled value="${openAccountId}" >-->
		      <input type="hidden" name="openAccountId" value="${openAccountId}" >
		      <input type="hidden" name="type" value="${type}" >
		    </div>
		  </div>
		  <div class="form-group hide">
		    <label class="col-sm-2 control-label"><#if type == 1 >组织名称:<#else>骑手姓名:</#if></label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" id="" placeholder="" disabled value="${name}">
		      <input type="hidden" name="openAccountName" value="${name}">
		    </div>
		  </div>
		  <#if type == 1 >
		  <!--<div class="form-group">
		    <label class="col-sm-2 control-label"><span style="color:red;">*</span>邮资（元）:</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control parsley-validated" id="postage" name="postage" value="<#if vo??>${vo.postage!""}</#if>">
		    </div>
		  </div>-->
		  <div class="form-group">
			  <label class="col-sm-2 control-label">合同编号：</label>
			  <div class="col-sm-4">
                  <input type="text" class="form-control" name="contractCode" id="contractCode" placeholder="" value="<#if vo??>${vo.contractCode!""}</#if>">
              </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-2 control-label">上传合同照片:</label>
		    <div class="col-sm-4">
				<input type="file" class="form-control" id="contractFiles" name="contractFiles" multiple="multiple">
		    </div>
		  </div>
		  <#if contractPics??>
		  <div class="row">
		  	<#list contractPics as contractPic>
		  	<div class="col-xs-6 col-md-3">
		    	<a href="#" class="thumbnail">
		      	<img src="${contractPic}" style="width:300px;height:300px;">
		    	</a>
		  	</div>
		  	</#list>
		  </div>
	      </#if>
	      <input type = "hidden" value="<#if originContractPics??>${originContractPics!""}</#if>" id="originContractPic" name="originContractPic">
		  </#if>

		<#if type == 1 >
			<div class="form-group">
				<label class="col-sm-2 control-label">营业执照编号：</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" name="licenceCode" id="licenceCode" placeholder="" value="<#if vo??>${vo.licenceCode!""}</#if>">
				</div>
			</div>
		</#if>

		  <div class="form-group">
		    <label class="col-sm-2 control-label"><#if type == 1 >上传营业执照照片:<#else>上传身份证照片:</#if></label>
		    <div class="col-sm-4">
				<input type="file" class="form-control" id="file" name="file" multiple="multiple">
		    </div>
		    <input type = "hidden" value="<#if originPic??>${originPic!""}</#if>" name="originPic" id="originPic">
		  </div>
		   <#if pics??>
		  <div class="row">
		  	<#list pics as pic>
		  	<div class="col-xs-6 col-md-3">
		    	<a href="#" class="thumbnail">
		      	<img src="${pic}" style="width:300px;height:300px;">
		    	</a>
		  	</div>
		  	</#list>
		  </div>
	      </#if>
		  <!--<div class="form-group">
		    <div class="col-sm-2 control-label"></div>
		    <div class="col-sm-1">
				<button type="button" id="B_upload" class="btn btn-success" >上传</button>
		    </div>
		    <div class="col-sm-3">
		    	<p class="help-block">点击按钮上传照片</p>
    		</div>
		  </div> -->
		  <div class="form-group">
		    <label class="col-sm-2 control-label">结算周期:</label>
		    <div class="col-sm-4">
		        <label class="radio-inline">
				  <input disabled type="radio" value="1" <#if type == 2 > checked </#if>> 一天
				</label>
		        <label class="radio-inline">
				  <input disabled type="radio" value="3" > 三天
				</label>
				<label class="radio-inline">
				  <input disabled type="radio" value="7" > 一周
				</label>
				<label class="radio-inline">
				  <input disabled type="radio" value="14" <#if type == 1 > checked </#if>> 两周
			    </label>
			    <input type="hidden" name="cycle" value="<#if type == 1 >14<#else>1</#if>">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-2 control-label">最低结算金额（元）:</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control parsley-validated" name="minMoney" id="minMoney" placeholder="" value="<#if vo??>${vo.minMoney!""}</#if>">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-2 control-label">公私:</label>
		    <div class="col-sm-2">
		      	<label class="radio-inline">
				  <input type="radio" name="cardType" id="" value="1" <#if vo?? && vo.cardType == 1> checked </#if> <#if vo??><#else><#if type == 1>checked</#if></#if>> 对公
				</label>
				<label class="radio-inline">
				  <input type="radio" name="cardType" id="" value="2" <#if vo?? && vo.cardType == 2> checked </#if> <#if vo??><#else><#if type == 2>checked</#if></#if>> 对私
				</label>
		    </div>
		    <div class="col-sm-3">
		    	<p class="help-block">请选择结算方银行卡结算性质</p>
    		</div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-2 control-label">银行卡所在地:</label>
		    <div class="col-sm-4">
		      	<select class="form-control input-sm inline" id="province" name="province">
		      		<option value ="0" selected="selected"></option>
            	</select>
            	<input type = "hidden" value="<#if vo??>${vo.provinceCode!""}</#if>" id="provinceInit">
		    </div>
		    <div class="col-sm-4">
		      	<select class="form-control input-sm inline" id="city" name="city">
            		<option value="0" selected="selected" ></option>
            	</select>
            	<input type = "hidden" value="<#if vo??>${vo.cityCode!""}</#if>" id="cityInit">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-2 control-label">开户行:</label>
		    <div class="col-sm-4">
		      	<select class="form-control input-sm inline" id="bank" name="bank">
                	<option value="0" selected="selected" ></option>
            	</select>
            	<input type = "hidden" value="<#if vo??>${vo.bankCode!""}</#if>" id="bankInit">
		    </div>
		    <div class="col-sm-4">
		    	<select class="form-control input-sm inline" id="branchId" name="branchId">
                	<option value="0" selected="selected" ></option>
            	</select>
            	<input type = "hidden" value="<#if vo??>${vo.branchId!""}</#if>" id="bankbranchInit">
                <input type="hidden" id="bankSub" name="bankSub" value="${(vo.bankSub)!""}">

            	<!--<input type="text" maxlength="20" class="form-control input-sm js_branch_name" name="bankSub" value="${(vo.bankSub)!""}"/>
                <input type="hidden" name="branchId" class="js_branch_id" value="${(vo.branchId)!'-1'}">-->
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-2 control-label">开户名:</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="cardName" id="cardName" placeholder="" value="<#if vo??>${vo.cardName!""}</#if>">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-2 control-label">银行卡号:</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="cardNo" id="cardNo" placeholder="" value="<#if vo??>${vo.cardNo!""}</#if>">
		    </div>
		  </div>

		  <div class="panel-footer text-center" style="background-color: #f9f9f9;">
	        <input type="button" class="btn btn-success" id="save_button" value="保存"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	        <#if create?? && create>
	        	<a  class="btn btn-success" href="javascript:;" id="skip">跳过</a>
	        <#else>
	        	<a  class="btn btn-success" href="/org/stations">取消</a>
	        </#if>
	      </div>

		</form>

		<#if records??>
		<h1>操作历史:</h1>
		<table class="table table-striped" id="">
            <thead>
            <tr>
                <th>操作时间</th>
                <th>操作人</th>
                <th>操作内容</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list records as record>
                <tr>
                    <td>${record.ctime!""}</td>
                    <td>${record.admin_name!""}</td>
                    <td>${record.content!""}</td>
                </tr>
            </#list>
            </tbody>
        </table>
        </#if>
	</div>
</div>
<script>document.title = '结算设置';</script>
<script type="text/javascript" src="${staticPath}/js/page/payment/payment.js"></script>
<script type="text/javascript" src="/static/js/lib/touchspin/jquery.bootstrap-touchspin.min.js"></script>
