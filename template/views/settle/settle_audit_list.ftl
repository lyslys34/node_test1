<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">

<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu.js"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.position.js"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete.js"></script>

<link href="/static/css/lib/autocomplement/jquery.ui.all.css" rel="stylesheet">
<!-- Color box -->
<link href="/static/css/lib/colorbox/colorbox.css" rel="stylesheet">

<script src='/static/js/lib/jquery.colorbox.min.js'></script>

<script type="text/javascript" src="${staticPath}/js/page/settle/settle_audit.js?ver=3"></script>

<style type="text/css">

input[type="checkbox"] {
  opacity: 1;
}
</style>

<script>
	$(function()	{
		//Colorbox 
		<#list vos as vo>
		$('.gallery-zoom${vo.id}').colorbox({
			maxWidth:'90%',
			width:'800px'
		});
		</#list>

        //组织名称联想搜索
        $('#openAccountName').autocomplete({
            source:function(request, response){
                $.ajax({
                    url: "/settleAudit/nameList",
                    dataType: "json",
                    data: {
                        keyWords: request.term
                    },
                    success: function(data) {
                        response($.map(data, function(item) {
                            return { label: item, value: item }
                        }));
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 200
        });

        //绑定回车事件
        $('#comment').keydown(function(e){
            if(e.keyCode==13){
                $("#refuseBtn").click();
            }
        });
	});
</script>

<div id="main-container" >

<div style="margin-top:20px;margin-left:20px;margin-right:20px">
    <!--<div id="nav">&nbsp;&nbsp;财务审核 </div><br />-->
	<ul class="nav nav-tabs">
	  <li role="presentation" <#if auditStatus == "0">class="active"</#if>><a href="/settleAudit/page?auditStatus=0">待审核</a></li>
	  <li role="presentation" <#if auditStatus == "1,3">class="active"</#if>><a href="/settleAudit/page?auditStatus=1,3">审核通过</a></li>
	  <li role="presentation" <#if auditStatus == "2">class="active"</#if>><a href="/settleAudit/page?auditStatus=2">审核拒绝</a></li>
	</ul>

    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/settleAudit/page?auditStatus=${auditStatus}" method="post" >
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">账单ID</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="settleBillId" id="settleBillId" data-required="true" data-minlength="8" value="${riderId!""}"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">组织名称</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="openAccountName" id="openAccountName" data-required="true" data-minlength="8" value="${openAccountName!""}"/>
                        </div>
                    </div>
                    <#if auditStatus == "1,3">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">付款状态</label>
                            <select class="form-control input-sm" id="status" name="status">
                                <option value="" <#if status?? > selected="selected" </#if>>全部</option>
                                <option value="10" <#if status?? && status == "10" > selected="selected" </#if>>待付款</option>
                                <option value="20" <#if status?? && status == "20" > selected="selected" </#if>>已付款</option>
                                <option value="90" <#if status?? && status == "90" > selected="selected" </#if>>付款失败</option>
                            </select>
                        </div>
                    </div>
                    </#if>
                    <div class="col-md-3" style="padding-top:18px">
                        <button type="submit" class="btn btn-success">查询</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">

        <#import "../page/pager.ftl" as q>
        <#if recordCount??>
            <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/settleAudit/page?auditStatus=${auditStatus}"/>
        </#if>

        <table class="table table-striped" id="">
            <thead>
            <tr>
            	<#if auditStatus == "0" || auditStatus == "2">
               <!-- <th>
					<label style="margin-right:5px;">全选</label><input type="checkbox" value="">
				</th>-->
                <th style="padding-right: 0px;width:5%;">
                    <input type="checkbox" value="" style="position: relative;">
                    <label style="margin: 0px;">全选</label>
                </th>
				</#if>
                <th class="col-sm-1">账单ID</th>
                <th>结算周期</th>
                <th class="col-sm-1">组织名称</th>
                <th class="col-sm-1">组织ID</th>
                <th class="col-sm-1">账单类型</th>
                <th class="col-sm-1">任务建立时间</th>
                <th class="col-sm-1">账单金额</th>
                <th>银行信息</th>
                <th class="col-sm-1">合同信息</th>
                <#if auditStatus == "1,3"><th class="col-sm-1">付款状态</th></#if>
                <#if auditStatus == "2"><th class="col-sm-1">拒绝人</th><th class="col-sm-1">拒绝理由</th></#if>
                <th class="col-sm-1">操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list vos as vo>
                <tr>
                	<#if auditStatus == "0" || auditStatus == "2">
                    <td><input type="checkbox" value="${vo.id}"></td>
                    </#if>
                    <td><a style="color:blue" href="/settleAudit/billDetail?settleBillId=${vo.id}" target="_blank">${vo.id}</a></td>
                    <td>${vo.billTime!""}</td>
                    <td>${vo.openAccountName!""}</td>
                    <td>${vo.openAccountId!""}</td>
                    <td>
                        <#if vo.moneyType?? && vo.moneyType!="">${vo.moneyType!""}<#else>邮资</#if>
                    </td>
                    <td>${vo.ctime!""}</td>
                    <td>${vo.money!""}</td>
                    <td><p>银行卡号:${vo.cardNo!""}</p><p>开户行:${vo.bankSub!""}(${vo.bankName!""})</p><p>开户名:${vo.cardName!""}</p>对公对私:<#if vo.cardType?? ><#if vo.cardType ==1>对公<#else>对私</#if></#if></td>
                    <td>
                    	<#--<#list vo.contractPicUrl as pic>-->
                    		<#--<a style="color:blue" class="gallery-zoom${vo.id}" rel="${vo.id}" href="${pic}"><#if pic_index == 0 >合同</#if></a>-->
                    	<#--</#list>-->
                        <a style="color:blue" target="_blank" href="/settleAudit/contractPicture?openAccountId=${vo.openAccountId}">合同</a>
                    </td>
                    <#if auditStatus == "1,3"><th>${vo.payStatus!""}</th></#if>
                    <#if auditStatus == "2"><th>${vo.auditorName!""}</th><th>${vo.comment!""}</th></#if>
                    <td>
                                <a data-toggle="tooltip" title="详情" data-placement="top" href="/settleAudit/billDetail?settleBillId=${vo.id}" target="_blank" >
                                    <i class="fa fa-info-circle fa-lg opration-icon"></i>
                                </a>&nbsp;&nbsp;
                                <#if auditStatus == "0">
                                <a title="同意" data-placement="top" href="#auditAgreeModal" role="button" data-toggle="modal" class="agree_link" rel="${vo.id}" >
                                    <i class="fa fa-thumbs-o-up fa-lg opration-icon"></i>
                                </a>&nbsp;&nbsp;
                                <a title="拒绝" data-placement="top" href="#auditDisagreeModal" role="button" data-toggle="modal" class="disagree_link" rel="${vo.id}">
                                    <i class="fa fa-thumbs-o-down fa-lg opration-icon"></i>
                                </a>&nbsp;&nbsp;
                                <#elseif auditStatus == "1,3">
	                                <#if vo.payStatus == "生成未打款" >
	                            		<!-- <li><a href="#">拒绝付款</a></li> -->
		                            	<#if vo.auditStatus == 1 >
		                            	<a data-toggle="tooltip" title="暂停付款" data-placement="top" href="/settleAudit/suspend?billId=${vo.id}">
                                            <i class="fa fa-pause fa-lg opration-icon"></i>
                                        </a>&nbsp;&nbsp;
		                            	</#if>
		                                <#if vo.auditStatus == 3 >
		                            	<a data-toggle="tooltip" title="恢复付款" data-placement="top" href="/settleAudit/recover?billId=${vo.id}">
                                            <i class="fa fa-money fa-lg opration-icon"></i>
                                        </a>&nbsp;&nbsp;
		                            	</#if>
	                            	</#if>
	                            	<#if vo.payStatus == "打款失败" >
	                            	<a data-toggle="tooltip" title="重新付款" data-placement="top" href="/settleAudit/pay?billId=${vo.id}">
                                        <i class="fa fa-money fa-lg opration-icon"></i>
                                    </a>&nbsp;&nbsp;
	                            	</#if>
                                <#elseif auditStatus == "2">
                                <a title="审核通过" data-placement="top" href="#auditAgreeModal" role="button" data-toggle="modal" class="agree_link" rel="${vo.id}" >
                                    <i class="fa fa-thumbs-o-up fa-lg opration-icon"></i>
                                </a>
                            	</#if>
                    </td>
                </tr>
            </#list>
            </tbody>
        </table>

    <#import "../page/simplePager.ftl" as simpleP>
    <#if recordCount??>
        <@simpleP.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/settleAudit/page?auditStatus=${auditStatus}"/>
    </#if>
    
    <#if auditStatus == "0" || auditStatus == "2">
    <div class="panel-footer text-left">
    	<div class="row">
    		<div class="col-md-2">
	    		<span>已经选择了<span id="selectNum">0</span>个任务</span>
	    	</div>
	    	<div class="col-md-3" id="batchDiv">
	    		<a href="#auditAgreeModal" role="button" data-toggle="modal" class="btn btn-success" id="agree_button">同意</a>
                <#if auditStatus == "0">
	    		    <a href="#auditDisagreeModal" role="button" data-toggle="modal" class="btn btn-danger" id="disagree_button">拒绝</a>
                </#if>
	        </div>
        </div>
    </div>
    </#if>
    
    <form class="no-margin" id="disagreeForm" data-validate="parsley" novalidate="" method="post" action="">
      	<div class="modal fade" id="auditDisagreeModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>确认该账单审核被拒绝?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>请填写拒绝原因(必填)</label><div id="alert_error"></div>
                            <textarea rows="6" class="form-control input-sm" id="comment" name="comment" placeholder="拒绝原因"></textarea>
                        </div>
                        <div class="modal-footer">
                            <input type="button" id="refuseBtn" value="拒绝" class="btn btn-danger btn-sm" />
                            <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">取消</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
        
    <form class="no-margin" id="agreeForm" data-validate="parsley" novalidate="" method="post" action="">
        <div class="modal fade" id="auditAgreeModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>确认该账单审核通过?</h4>
                        <div class="modal-footer">
                            <input type="button" id = "commit" value="同意" class="btn btn-success btn-sm" />
                            <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true">取消</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- 消息通知框 START -->
    <div class="modal fade" id="infoModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>消息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <h5 id="infoMsg"></h5>
                    </div>
                    <div class="modal-footer" style="margin-bottom: 0px;">
                        <input type="button" id="confirmBtn" value="确定" class="btn btn-success btn-sm" style="float:right;margin-right: 50px;"/><!-- 成功时的确认按钮 -->
                        <button id="remainBtn" class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true">确定</button><!-- 失败时的确认按钮 -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 消息通知框 END -->

    </div>
</div>
</div>
<script>document.title = '财务审核';</script>
