<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/partner/settle/deliveryManager" method="post" >
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">站点ID</label>
                            <#--<input type="text" class="form-control input-sm parsley-validated" name="id" id="id" data-required="true" data-minlength="8" value="${id!""}"/>-->
                            <select class="form-control input-sm" id="id" name="id" data-minlengt="8">
                                <option value="">全部</option>
                                <#list orgIds as singleOrg>
                                    <option value="${singleOrg !''}" <#if id?? && singleOrg == id > selected="selected" </#if> >${singleOrg !''}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">站点名称</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="name" id="name" data-required="true" data-minlength="8" value="${name!""}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-left">
                <button type="submit" class="btn btn-success">查询</button>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
    	<h4 style="padding-left:10px">打款数据汇总</h4>
    	<table class="table table-striped" id="" >
            <thead>
            <tr>
                <th>站点总数(个)</th>
                <th>未结算账单总数(个)</th>
                <th>未结算金额(元)</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>${num!""}</td>
                    <td>${count!""}</td>
                    <td>${sum!""}</td>
                </tr>
            </tbody>
        </table>
        
        <h4 style="margin-top:30px;padding-left:10px">打款数据细目</h4>
        <table class="table table-striped" id="">
            <thead>
            <tr>
                <th>站点ID</th>
                <th>站点名称</th>
                <th>未结算账单总数(个)</th>
                <th>未结算金额(元)</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list riders as rider>
                <tr>
                    <td>${rider.riderId!""}</td>
                    <td>${rider.riderName!""}</td>
                    <td>${rider.settleCount!""}</td>
                    <td>${rider.settleSumMoney!"-"}</td>
                    <td>
                        <a data-toggle="tooltip" title="站点详情" data-placement="top" href="/partner/settle/selfRider?orgId=${rider.riderId!""}" target="_blank" >
                            <i class="fa fa-info-circle fa-lg opration-icon"></i>
                        </a>
                    </td>
                </tr>
            </#list>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/settle/deliveryManager"/>
    </#if>

    </div>
</div>
<script>document.title = '自建打款进度';$('select').select2();</script>
