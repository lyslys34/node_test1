<#include "settle_setting_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="pageName">settlementStrategy</content>

<style type="text/css">
	.content{
        font-size: 13px;
		padding-left: 20px;
		margin-top: 20px;
	}
	h4, h5{
		margin-top: 30px;
        padding-left: 10px;
        font-size: 16px;
	}
    thead{
        background-color: #d3d3d3;
    }
</style>

<div id="main-container">

    <#if orgType == 1>
        <h3>结算规则简述-自建版</h3>
    <#elseif orgType == 2>
        <h3>结算规则简述-加盟版</h3>
    </#if>
	<p></p>

    <h4>一、名词解释</h4>
    <div class="content">
        <p>线下垫款：骑手在取餐时，需使用现金取餐。</p>
        <p>代购：商家自己不接单，需骑手到店后现场等待商家做餐。</p>
    </div>

    <h4>二、影响范围</h4>
    <div class="content">
        <p>是否线下垫款：（以与商家签署合同为准）</p>
        <p>1.影响是否骑手需支付现金给商家。</p>
        <p>2.影响是否会与加盟商结算垫付款。</p>
        <p>3.影响美团App上，订单应付金额是否为0。</p>
    </div>
    <div class="content">
        <p>是否代购：</p>
        <p>影响是否商家会使用美团系统接单，即商家是否会提前备餐。</p>
    </div>

	<!-- 自建 -->
	<#if orgType == 1>
        <h4>三、结算规则</h4>
        <div class="content">
            <p>1.整体结算：货到付款不作任何结算。</p>
            <p>2.垫付款：骑手预先垫付给商家的款；即线下垫款+在线支付时结算。</p>
        </div>
        <div class="content">
            <p>图示说明：</p>
            <table class="table table-bordered" style="width: 40%;">
                <thead class="active">
                    <th>与商家结算方式</th>
                    <th>商家类型</th>
                    <th>用户支付方式</th>
                    <th>与骑手结算</th>
                </thead>
                <tbody>
                    <tr>
                        <td rowspan="4">线下垫款</td>
                        <td rowspan="2">普通</td>
                        <td>在线支付</td>
                        <td>垫付款</td>
                    </tr>
                    <tr>
                        <td>货到付款</td>
                        <td>无</td>
                    </tr>
                    <tr>
                        <td rowspan="2">代购</td>
                        <td>在线支付</td>
                        <td>垫付款</td>
                    </tr>
                    <tr>
                        <td>货到付款</td>
                        <td>无</td>
                    </tr>
                </tbody>
            </table>
        </div>
	<!-- 加盟商 -->
	<#elseif orgType == 2>
        <h4>三、结算规则</h4>
        <div class="content">
            <p>1.整体结算：货到付款不作任何结算。</p>
            <p>2.垫付款：加盟商预先垫付给商家的款；即线下结算+在线支付时结算。</p>
            <p>3.邮资：加盟商可获得的酬劳。即普通折扣+在线支付时结算。</p>
        </div>
        <div class="content">
            <p>图示说明：</p>
			<table class="table table-bordered" style="width: 40%;">
				<thead class="active">
					<th>与商家结算方式</th>
					<th>商家类型</th>
					<th>用户支付方式</th>
					<th>与加盟商结算</th>
				</thead>
				<tbody>
					<tr>
                        <td rowspan="2">线上结算</td>
                        <td rowspan="2">普通</td>
                        <td>在线支付</td>
                        <td>邮资</td>
					</tr>
                    <tr>
                        <td>货到付款</td>
                        <td>无</td>
                    </tr>
                    <tr>
                        <td rowspan="4">线下垫款</td>
                        <td rowspan="2">普通</td>
                        <td>在线支付</td>
                        <td>邮资+垫付款</td>
                    </tr>
                    <tr>
                        <td>货到付款</td>
                        <td>无</td>
                    </tr>
                    <tr>
                        <td rowspan="2">代购</td>
                        <td>在线支付</td>
                        <td>垫付款</td>
                    </tr>
                    <tr>
                        <td>货到付款</td>
                        <td>无</td>
                    </tr>
				</tbody>
			</table>
        </div>
	</#if>

</div>