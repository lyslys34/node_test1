<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">

<style type="text/css">
    .gray{
        color:#428bca;
    }
    .red{
        color:#eb4924;
    }
    table th, table td{
        text-align: center;
    }
</style>

<script>
$(document).ready(function(){
});
</script>

<div id="main-container">
    <div class="panel panel-default table-responsive">
        <div>
            <ul class="breadcrumb">
                <li style="color:#757986;font-size:14px;">结算中心<span class="divider"></span></li>
                <li style="color:#757986;font-size:14px;">加盟商打款进度<span class="divider"></span></li>
                <li style="color:#757986;font-size:14px;">加盟商账单列表<span class="divider"></span></li>
                <li style="color:#757986;font-size:14px;">账单详情<span class="active"></span></li>
            </ul>
        </div>

        <div style="display: inline-block;">
            <h4 style="padding-left:10px;">账单详情（ID:${settleBillId!''}）</h4>
        </div>

        <div style="display: inline-block;float: right;padding-top: 20px;">
            <a href="exportDeductionPaymentDetail?settleBillId=${settleBillId!''}" target="_blank" class="btn btn-sm btn-default pull-right" style="margin-right:20px;float:right;">导出表格</a>
            <a name="detail" style="margin-right:20px;" target="_blank"
               href="<#if orgType==2>/partner/settlementStrategy/agent<#elseif orgType==1>/partner/settlementStrategy/self</#if>">
                <label style="padding: 8px 0px;color:#4ac598;cursor:pointer;">结算规则</label>
                <i class="fa fa-question-circle fa-lg opration-icon"></i>
            </a>
        </div>

        <!-- Nav tabs Start-->
        <#if orgType == 2>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="/partner/settle/paymentDetail?settleBillId=${settleBillId!''}">正常订单</a></li>
                <#if moneyType?? && moneyType!=10>
                <#else>
                    <li role="presentation" class="active"><a href="/partner/settle/paymentDeductionDetail?settleBillId=${settleBillId!''}">异常订单</a></li>
                </#if>
            </ul>
            <div style="width: 100%;height: 2px;background: #58B7B1;"></div>
        </#if>
        <!-- Nav tabs End-->

    	<table class="table table-striped" id="" >
            <thead>
            <tr>
                <th>订单送达/取消时间</th>
                <th>订单状态</th>
                <th>订单价格(元)</th>
                <th>菜品原价(不含配送费)</th>

                <th>应邮资金额</th>
                <th>实际邮资金额</th>
                <th class="red">处罚总额</th>

                <th class="gray">未完成处罚金额</th>
                <th class="gray">超时处罚金额</th>
                <th class="gray">客诉处罚金额</th>
                <th class="gray">提前点送达处罚金额</th>
                <th class="gray">连击处罚金额</th>

                <th>商家名称</th>
                <th>订单ID</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list billDetailVos as vo>
                <tr>
                    <td>${vo.utime!""}</td>
                    <td><#if vo.orderStatus == 99>已取消<#elseif vo.orderStatus == 50>已完成<#else>-</#if></td>
                    <td>${vo.price!""}</td>
                    <td>${vo.pkgValue!""}</td>

                    <#--<th>${vo.money!""}</th>-->
                    <td>${vo.shippingFeeOrigin!""}</td>
                    <td>${vo.actualShippingFee!""}</td>
                    <td class="red"><#if vo.deductionTotalFee!=0>${-vo.deductionTotalFee}<#else>0</#if></td>

                    <td class="gray"><#if vo.unfinishedMoney!=0>${-vo.unfinishedMoney}<#else>0</#if></td>
                    <td class="gray"><#if vo.overtimeMoney!=0>${-vo.overtimeMoney}<#else>0</#if></td>
                    <td class="gray"><#if vo.complainMoney!=0>${-vo.complainMoney}<#else>0</#if></td>
                    <td class="gray"><#if vo.clickEarlyMoney!=0>${-vo.clickEarlyMoney}<#else>0</#if></td>
                    <td class="gray"><#if vo.batterMoney!=0>${-vo.batterMoney}<#else>0</#if></td>

                    <td>${vo.poiName!""}</td>
                    <td>${vo.platformOrderId!""}</td>
                </tr>
            </#list>
            </tbody>
        </table>
        
        <#import "../page/pager.ftl" as q>
	    <#if recordCount??>
	        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/settle/paymentDeductionDetail?settleBillId=${settleBillId}"/>
	    </#if>
        
    </div>
</div>
<script>document.title = '账单详情';</script>
