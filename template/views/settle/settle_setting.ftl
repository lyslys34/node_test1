<#include "settle_setting_config.ftl">
<#include "../widgets/sidebar.ftl">

<!--<link rel="stylesheet" type="text/css" href="/static/css/lib/bootstrap.css" />
<script type="text/javascript" src="/static/js/lib/jquery.js"></script>-->
<script type="text/javascript" src="/static/js/lib/jquery.form.js"></script>

<script>
    var from = '${RequestParameters.f !''}';
    $(document).ready(function () {
        //初始化数字控件
        $("#postage").TouchSpin({
            min: 0,
            max: 999999999,
            step: 0.01,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '￥'
        });

        $("#minMoney").TouchSpin({
            min: 0,
            max: 999999999,
            step: 1,
            decimals: 0,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '￥'
        });
        $('#myModal').modal({
            show: false,
            keyboard: true
        });

        $('#reloadBtn').click(function(){
            window.location.reload();
        });

        var formDisable = $('#formDisable').val();
        if( formDisable == 'true'){
            $('input,select').attr("disabled","disabled");
        }else{
            var cardVerifyStatus = <#if vo??>${vo.cardVerifyStatus}<#else>undefined</#if>;
            var cardVerifyMsg = <#if vo??>'${vo.cardVerifyMsg!''}'<#else>''</#if>;
            cardVerifyMsg=cardVerifyMsg.substring(cardVerifyMsg.indexOf('|')+1);
            var statusToMsg = {'-1':'系统异常，请重新提交校验','0':cardVerifyMsg,'1':'√ 银行卡校验通过','2':'银行卡校验中','3':cardVerifyMsg};
            if(cardVerifyStatus!=undefined){
                var msg = statusToMsg[cardVerifyStatus];
                $('#cardNoWarning').html(msg);
                if(cardVerifyStatus>0){
                    $('#cardNoWarning').show().addClass('alert-success');
                }else if(msg){
                    $('#cardNoWarning').show().addClass('alert-danger');
                }
            }
            var bankInfoEditDisabled = <#if vo??&&vo.cardVerifyStatus==2>true<#else>false</#if>;
            if(bankInfoEditDisabled){
//                $('.bank-info select').attr("disabled","disabled");
                $('.bank-info :text').attr("readonly","true");
                $('.bank-info :radio').attr("readonly","true");
            }
        }

        $("#js_passed").click(function() {
            var content = "后期可使用\"结算设置\"功能填写";
            _showPop({
                type:1,
                content: "<div style='font-size:16px; font-weight:bold; padding:0px 20px;'>" + content + "</div>",
                ok: {
                    display: true,
                    name: "知道了",
                    callback:function(){
                        if(from=='sc'){
                            window.location.href="/org/stations";
                        }
                        if('join_org_charge' == from){
                            window.location.href="/org/join/orgList";
                        }
                    }
                }
            });
        })
    });
</script>

<style type="text/css">
    .form-horizontal .control-label {
        text-align: left;
    }

    input[type="radio"] {
        opacity: 1;
    }

    .form-group .warning, .form-group .warningPic, .form-group .warningSelect {
        padding: 7px;
        margin-bottom: 0;
    }

    .form-group .hiding {
        display: none;
    }

    #myModal{
        top: 30%;
        left: 35%;
        background-color: #fff;
        border-radius: 10px;
        width: 400px;
        border: 1px solid rgba(0,0,0,0.3);
        box-shadow: 0 3px 7px rgba(0,0,0,0.3);
        color: #333;
        bottom: inherit;
    }

    #myModal .modal-header{
        padding: 0 10px;
    }

    #myModal h3{
        margin-top: 10px;
    }

    #myModal .modal-body{
        font-size: 14px;
        padding-left: 40px;
    }

    #myModal p{
        margin: 5px;
    }

    #myModal .modal-footer{
        background-color: #f5f5f5;
        padding: 10px 25px 12px;
    }
</style>

<div id="main-container">
    <#if RequestParameters.f?exists && RequestParameters.f == 'join_org'>
        <@joinOrgNavi org is_org_manager 2></@joinOrgNavi>
    </#if>
    <#if RequestParameters.f?exists && RequestParameters.f == 'join_org_charge'>
        <@joinCreateOrgStep 3 />
    </#if>
    <#if RequestParameters.f?exists && RequestParameters.f == 'sc'>
        <h4><#if org?exists>${org.orgName !''}(ID:${org.orgId!''})</#if></h4>
        <#import "/organization/station_create_progress.ftl" as p>
        <@p.progress length=3 step=3 />
    </#if>
    <#if RequestParameters.f?exists && RequestParameters.f == 'su'>
        <h4><#if org?exists>${org.orgName !''}(ID:${org.orgId!''})</#if></h4>
        <#import "/organization/station_nav.ftl" as n>
        <@n.station_nav active=2 orgOperations=org.orgOperations orgVo=org />
    </#if>

    <div id="info" style="margin:30px 100px;padding:20px;">
        <input type = "hidden" value="${type!""}" id="type" >
        <input type="hidden" value="<#if formDisable??>true<#else>false</#if>" id="formDisable" >
        <#if formDisable?? && type==1>
            <div class=" alert alert-warning" role="alert" id="">您有未打款的账单，暂时无法修改结算设置！</div>
        </#if>
        <form id="data_form" class="form-horizontal" enctype="multipart/form-data"
              action="<#if partner?? && partner >/partner/settle/saveSetting<#else>/settle/saveSetting</#if>"
              method="post">
            <!--<#if type==1 ><p style="color:red;">选填项不填或填写错误将会导致无法打款成功，请慎重填写</p></#if> -->
            <div class="form-group">
                <!-- <label class="col-sm-2 control-label"><span style="color:red;">*</span><#if type == 1 >组织ID:<#else>骑手ID:</#if></label> -->
                <div class="col-sm-4">
                    <!--<input type="text" class="form-control" id="" placeholder="" disabled value="${openAccountId}" >-->
                    <input type="hidden" name="openAccountId" value="${openAccountId}">
                    <input type="hidden" name="type" value="${type}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"><#if type == 1 >组织名称:<#else>骑手姓名:</#if></label>

                <div class="col-sm-4">
                    <input type="text" class="form-control" id="" placeholder="" disabled value="${name}">
                    <input type="hidden" name="openAccountName" value="${name}">
                </div>
            </div>
        <#if type == 1 >
            <!--<div class="form-group">
		    <label class="col-sm-2 control-label"><span style="color:red;">*</span>邮资（元）:</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control parsley-validated" id="postage" name="postage" value="<#if vo??>${vo.postage!""}</#if>">
		    </div>
		  </div>-->
            <div class="form-group">
                <label class="col-sm-2 control-label">合同编号：</label>

                <div class="col-sm-4">
                    <input type="text" class="form-control" name="contractCode" id="contractCode" placeholder=""
                           value="<#if vo??>${vo.contractCode!""}</#if>">
                </div>
                <div class="col-sm-4 alert alert-danger warning hiding" role="alert" id="contractCodeWarning"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">上传合同照片:</label>

                <div class="col-sm-4">
                    <input type="file" class="form-control" id="contractFiles" name="contractFiles" multiple="multiple">
                </div>
                <div class="col-sm-4 alert alert-danger warningPic hiding" role="alert" id="contractFilesWarning">
                    请上传合同照片
                </div>
            </div>
            <#if contractPics??>
                <div class="row">
                    <#list contractPics as contractPic>
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="${contractPic}" style="width:300px;height:300px;">
                            </a>
                        </div>
                    </#list>
                </div>
            </#if>
            <input type="hidden" value="<#if originContractPics??>${originContractPics!""}</#if>" id="originContractPic"
                   name="originContractPic">
        </#if>

        <#if type == 1 >
            <div class="form-group">
                <label class="col-sm-2 control-label">营业执照编号：</label>

                <div class="col-sm-4">
                    <input type="text" class="form-control" name="licenceCode" id="licenceCode" placeholder=""
                           value="<#if vo??>${vo.licenceCode!""}</#if>">
                </div>
                <div class="col-sm-4 alert alert-danger warning hiding" role="alert" id="licenceCodeWarning"></div>
            </div>
        </#if>

            <div class="form-group">
                <label class="col-sm-2 control-label"><#if type == 1 >上传营业执照照片:<#else>上传身份证照片:</#if></label>
                <#if formDisable?? && formDisable>
                <#else>
                    <#if type == 1>
                        <div class="col-sm-4">
                            <input type="file" class="form-control" id="file" name="file" multiple="multiple">
                        </div>
                    <#else>
                        <span>请到[配送员管理]中修改骑手身份证信息</span>
                    </#if>
                </#if>
                <div class="col-sm-4 alert alert-danger warningPic hide" role="alert"
                     id="originPicWarning"><#if type == 1 >请上传营业执照照片<#else>请上传身份证照片</#if></div>
                <input type="hidden" value="<#if originPic??>${originPic!""}</#if>" name="originPic" id="originPic">
            </div>
        <#if pics??>
            <div class="row">
                <#list pics as pic>
                    <div class="col-xs-6 col-md-3">
                        <a href="#" class="thumbnail">
                            <img src="${pic}" style="width:300px;height:300px;">
                        </a>
                    </div>
                </#list>
            </div>
        </#if>
            <!--<div class="form-group">
              <div class="col-sm-2 control-label"></div>
              <div class="col-sm-1">
                  <button type="button" id="B_upload" class="btn btn-success" >上传</button>
              </div>
              <div class="col-sm-3">
                  <p class="help-block">点击按钮上传照片</p>
              </div>
            </div> -->
            <div class="form-group">
                <label class="col-sm-2 control-label">结算周期:</label>

                <div class="col-sm-4">
                    <label class="radio-inline">
                        <input disabled type="radio" value="1" <#if type == 2 > checked </#if>> 一天
                    </label>
                    <label class="radio-inline">
                        <input disabled type="radio" value="3"> 三天
                    </label>
                    <label class="radio-inline">
                        <input disabled type="radio" value="7"> 一周
                    </label>
                    <label class="radio-inline">
                        <input disabled type="radio" value="14" <#if type == 1 > checked </#if>> 两周
                    </label>
                    <input type="hidden" name="cycle" value="<#if type == 1 >14<#else>1</#if>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">最低结算金额（元）:</label>

                <div class="col-sm-4">
                    <input type="text" class="form-control parsley-validated" name="minMoney" id="minMoney"
                           placeholder="" value="<#if vo??>${vo.minMoney!""}</#if>">
                </div>
                <div class="col-sm-4 alert alert-danger warning hiding" role="alert" id="minMoneyWarning"></div>
            </div>
            <div class="form-group bank-info">
                <label class="col-sm-2 control-label">公私:</label>

                <div class="col-sm-2">
                    <label class="radio-inline">
                        <input type="radio" name="cardType" id="" value="1" <#if vo?? && vo.cardType == 1>
                               checked </#if> <#if vo??><#else><#if type == 1>checked</#if></#if>> 对公
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="cardType" id="" value="2" <#if vo?? && vo.cardType == 2>
                               checked </#if> <#if vo??><#else><#if type == 2>checked</#if></#if>> 对私
                    </label>
                </div>
                <#if formDisable?? && formDisable>
                <#else>
                    <div class="col-sm-3">
                        <p class="help-block">请选择结算方银行卡结算性质</p>
                    </div>
                </#if>
            </div>
            <div class="form-group bank-info">
                <label class="col-sm-2 control-label">银行卡所在地:</label>

                <div class="col-sm-4">
                    <select class="form-control input-sm inline" id="province" name="province">
                        <option value="0" selected="selected"></option>
                    </select>
                    <input type="hidden" value="<#if vo??>${vo.provinceCode!""}</#if>" id="provinceInit">
                </div>
                <div class="col-sm-4">
                    <select class="form-control input-sm inline" id="city" name="city">
                        <option value="0" selected="selected"></option>
                    </select>
                    <input type="hidden" value="<#if vo??>${vo.cityCode!""}</#if>" id="cityInit">
                </div>
                <div class="col-sm-2 alert alert-danger warningSelect hiding" role="alert" id="locationWarning"></div>
            </div>
            <div class="form-group bank-info">
                <label class="col-sm-2 control-label">开户行:</label>

                <div class="col-sm-4">
                    <select class="form-control input-sm inline" id="bank" name="bank">
                        <option value="0" selected="selected"></option>
                    </select>
                    <input type="hidden" value="<#if vo??>${vo.bankCode!""}</#if>" id="bankInit">
                </div>
                <div class="col-sm-4">
                    <select class="form-control input-sm inline" id="branchId" name="branchId">
                        <option value="0" selected="selected"></option>
                    </select>
                    <input type="hidden" value="<#if vo??>${vo.branchId!""}</#if>" id="bankbranchInit">
                    <input type="hidden" id="bankSub" name="bankSub" value="${(vo.bankSub)!""}">

                    <!--<input type="text" maxlength="20" class="form-control input-sm js_branch_name" name="bankSub" value="${(vo.bankSub)!""}"/>
                <input type="hidden" name="branchId" class="js_branch_id" value="${(vo.branchId)!'-1'}">-->
                </div>
                <div class="col-sm-2 alert alert-danger warningSelect hiding" role="alert" id="bankWarning"></div>
            </div>
            <div class="form-group bank-info">
                <label class="col-sm-2 control-label">开户名:</label>

                <div class="col-sm-4">
                    <input type="text" class="form-control" name="cardName" id="cardName" placeholder=""
                           value="<#if vo??>${vo.cardName!""}</#if>">
                </div>
                <div class="col-sm-4 alert alert-danger hiding" role="alert" id="cardNameWarning"></div>
            </div>
            <div class="form-group bank-info">
                <label class="col-sm-2 control-label">银行卡号:</label>

                <div class="col-sm-4">
                    <input type="text" class="form-control" name="cardNo" id="cardNo" placeholder=""
                           value="<#if vo??>${vo.cardNo!""}</#if>">
                </div>
                <div class="col-sm-4 alert hiding" role="alert" id="cardNoWarning"></div>
            </div>

            <#if formDisable?? && formDisable>
                <div class="panel-footer text-center" style="background-color: #f9f9f9;"></div>
            <#else>
                <div class="panel-footer text-center" style="background-color: #f9f9f9;">
                    <input type="button" class="btn btn-success" id="save_button" value="保存"/>
                    <#if RequestParameters.f?exists && (RequestParameters.f == 'join_org_charge' || RequestParameters.f='sc')>
                        <input type="button" class="btn btn-success" value="跳过" id="js_passed"/>
                    </#if>
                </div>
            </#if>

        </form>

    <#if records??>
        <h1>操作历史:</h1>
        <table class="table table-striped" id="">
            <thead>
            <tr>
                <th>操作时间</th>
                <th>操作人</th>
                <th>操作内容</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
                <#list records as record>
                <tr>
                    <td>${record.ctime!""}</td>
                    <td>${record.admin_name!""}</td>
                    <td>${record.content!""}</td>
                </tr>
                </#list>
            </tbody>
        </table>
    </#if>
    </div>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>消息</h3>
        </div>
        <div class="modal-body">
            <p>保存修改成功！</p>
        </div>
        <div class="modal-footer">
            <a id="failBtn" class="btn btn-success">确定</a>
            <a id="reloadBtn" class="btn btn-success">确定</a>
        </div>

    </div>
</div>
<@commonPop></@commonPop>

<script>document.title = '结算设置';</script>

<script type="text/javascript" src="/static/js/lib/touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script type="text/javascript" src="${staticPath}/js/page/payment/payment.js?_t=${type!""}"></script>
