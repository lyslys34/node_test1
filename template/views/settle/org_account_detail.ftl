<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/settle/date.js</content>
<content tag="cssmain">/statistics/daily_data.css</content>
<link rel="stylesheet" type="text/css" href="/static/css/lib/touchspin/jquery.bootstrap-touchspin.min.css" />
<style>
    input[type="radio"],
    input[type="checkbox"] {
        opacity:1;
        position: static;
        width: 13px;
        height: 13px;
    }
</style>

<div id="main-container">
    <div id="nav"><br />&nbsp;&nbsp;站点对账 > 站点订单详情 </div><br />
    <form class="no-margin form-inline" id="formValidate1" data-validate="parsley" novalidate="" method="post" action="/partner/settle/detail">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div style="float:left;width:700px">
                        <div class="form-group">
                            &nbsp; &nbsp; &nbsp;日期：
                            <input type="radio" class="dateType" name="timeFlag" value="0" <#if 0 == timeFlag>checked="true" </#if>/> 今日
                            &nbsp;&nbsp;|&nbsp;&nbsp;<input type="radio" class="dateType"  name="timeFlag" value="1" <#if 1 == timeFlag>checked="true" </#if>/> 昨日
                            &nbsp;&nbsp;|&nbsp;&nbsp;<input type="radio" class="dateType"  name="timeFlag" value="2" <#if 2 == timeFlag>checked="true" </#if>/> 近三天
                            &nbsp;&nbsp;|&nbsp;&nbsp;<input type="radio" class="dateType"  name="timeFlag" value="3" <#if 3 == timeFlag>checked="true" </#if>/> 自定义时间
                            <input id="start" type="text" name="beginTimeStr" style="cursor: pointer;"  readonly="readonly" value="<#if beginTimeStr?exists>${beginTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start">至<input type="text" id="end" name="endTimeStr" style="cursor: pointer;" readonly="readonly"  value="<#if endTimeStr?exists>${endTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end">
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 10px">
                    <div style="float:left;margin-left:20px">
                        <div class="form-group">
                            <label>骑手姓名:</label>
                            <select class="form-control" id="riderId" name="riderId" style="width: 150px;height: 25px;">
                                <option value="-1">全部</option>
                            <#list riderList as rider>
                                <option value="${rider.id !''}" <#if rider.id?exists && rider.id == riderIdView > selected="selected" </#if> >${rider.name !''}</option>
                            </#list>
                            </select>
                        </div>
                    </div>
                    <div style="float:left;margin-left:20px">
                        <div class="form-group">
                            <label>订单ID:</label>
                            <input id="orgId" type="hidden" name="orgId" value="${orgId!""}">
                            <input id="waybillId" type="text" name="waybillId" style="width: 150px" value="${waybillIdView}" maxlength="20" class="form-control input-sm js_order_id">
                        </div>
                    </div>
                    <div style="float:left;margin-left:20px">
                        <div class="form-group">
                            <label>订单类型:</label>
                            <select class="form-control" id="waybillType" name="waybillType" style="width:150px;height: 25px;">
                                <option value="-1" <#if !waybillTypeView?exists || waybillTypeView == -1>selected="selected"</#if> >全部</option>
                                <option value="0" <#if waybillTypeView?exists && waybillTypeView == 0>selected="selected"</#if> >货到付款</option>
                                <option value="1" <#if waybillTypeView?exists && waybillTypeView == 1>selected="selected"</#if> >在线支付</option>
                            </select>
                        </div>
                    </div>
                    <div style="float:left;margin-left:20px">
                        <div class="form-group">
                            <label style="width: 60px;">商家类型:</label>
                            <select class="form-control" id="poiType" name="poiType" style="width: 150px;height: 25px;">
                                <option value="-1" <#if !poiTypeView?exists || poiTypeView == -1>selected="selected"</#if> >全部</option>
                                <option value="2" <#if poiTypeView?exists && poiTypeView == 2 >selected="selected"</#if> >代购</option>
                                <option value="1" <#if poiTypeView?exists && poiTypeView == 1 >selected="selected"</#if> >普通</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-left">
                <button type="button" class="btn btn-success js_submitbtn" actionurl="/partner/settle/detail" >查询</button>
                <button type="button" class="btn btn-success js_submitbtn" actionurl="/partner/settle/detailExcel">导出EXCEL</button>
                <a name="detail" style="margin-right:20px;float:right;" target="_blank"
                   href="<#if orgType==2>/partner/settlementStrategy/agent<#elseif orgType==1>/partner/settlementStrategy/self</#if>">
                    <label style="padding: 8px 0px;color:#4ac598;cursor:pointer;">结算规则</label>
                    <i class="fa fa-question-circle fa-lg opration-icon"></i>
                </a>
            </div>
        </div>
    </form>


    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>订单显示ID</th>
                <th>订单已送达时间</th>
                <th>商家名称</th>
                <th>商家类型</th>
                <th>线下垫款</th>
                <th>订单类型</th>
                <th>骑手姓名</th>
                <th>订单价格(元)</th>
                <th>菜品原价(元)</th>
                <th>邮资(元)</th>
                <th>垫付餐费(元)</th>
                <th>取餐实付(元)</th>
                <th>送餐实收(元)</th>
                <th>取餐应付(元)</th>
                <th>送餐应收(元)</th>
                <th>应收实收差额(元)</th>
                <th>应付实付差额(元)</th>
            <#if orgType == 2>
                <th>应上交金额(元)</th>
            </#if>
                <th>备注</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
          <#if list ??>
            <#list list as view>
            <tr>
                <#if view_index == 0>
                    <td colspan="7" align="center">合计</td>
                <#else>
                    <td>${view.orderViewId!''}</td>
                    <td>${view.arriveTime!''}</td>
                    <td>${view.poiName!''}</td>
                    <td><#if view.poiType == 1 >普通</#if><#if view.poiType == 2 >代购</#if></td>
                    <td><#if view.logistics_offline_pay_order_record == 1 >是<#else>否</#if></td>
                    <td><#if view.isPayed == 0 >货到付款</#if><#if view.isPayed == 1 >在线支付</#if></td>
                    <td>${view.rider_name!''}</td>
                </#if>

                <td>${view.pkgValue}</td>
                <td>${view.pkgValue2}</td>
                <td>${view.postage}</td>
                <td>${view.dianfuFee}</td>
                <td>${view.actual_pay_amount}</td>
                <td>${view.actual_charge_amount}</td>
                <td>${view.plan_pay_amount}</td>
                <td>${view.plan_charge_amount}</td>
                <td>${view.plan_actual_charge}</td>
                <td>${view.plan_actual_pay}</td>
                <#if orgType == 2>
                    <td>${view.plan_shangjiao_fee}</td>
                </#if>
                <td>${view.comment!''}</td>
            </tr>
            </#list>
          </#if>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/settle/detail"/>
    </#if>
    </div>
</div>
<script>
    document.title = '站点订单详情';
    $('select').select2();
</script>