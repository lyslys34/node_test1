<#include "../widgets/sidebar.ftl">
<#include "settle_config.ftl">
<div id="main-container">
    <div class="panel-heading" style="font-size:20px;">
            <span>账单任务(账单ID:${vo.bmSettleBillId!''})
            </span>
    </div>
    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>结算对象：</dt>
            <dd>${vo.openAccountName!''}</dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>所在地：</dt>
            <dd>${vo.cityName!''}</dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>开户行：</dt>
            <dd>${vo.bank!''}(开户支行:${vo.bankBranchName!''})</dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>账号: </dt>
            <dd>${vo.cardNo!''}</dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>对公对私: </dt>
            <dd><#if vo.acctype==1>对公<#elseif vo.acctype==2>对私</#if></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>提现金额: </dt>
            <dd>${vo.money?string(",##0.00#")}</dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>付款渠道: </dt>
            <dd>${vo.fixChannelName!''}</dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>支付状态: </dt>
            <dd><#if vo.status=1>成功<#elseif vo.status=2>失败<#elseif vo.status=5>提现中</#if></dd>
        </dl>
        <#if vo.status=3>
            <dl class="dl-horizontal">
                <dt>失败原因: </dt>
                <dd>${vo.failMsg}</dd>
            </dl>
        </#if>
        <dl class="dl-horizontal">
            <dt>付款流水平台号: </dt>
            <dd>${vo.paymentOutNo!''}</dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>发起提现时间: </dt>
            <dd>${vo.startTime!''}</dd>
        </dl>
        <#if vo.status=2>
        <dl class="dl-horizontal">
            <dt>提现成功时间: </dt>
            <dd>${vo.endTime!''}</dd>
        </dl>
        <#elseif vo.status=3>
        <dl class="dl-horizontal">
            <dt>提现失败时间: </dt>
            <dd>${vo.endTime!''}</dd>
        </dl>
        </#if>
    </div>

</div>
<script>
    function close(){
        alert();
        window.opener = window;
        window.close();
        alert(2);
    }
</script>