<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/settle/date.js</content>
<content tag="cssmain">/statistics/daily_data.css</content>

<style>
    input[type="radio"],
    input[type="checkbox"] {
        opacity:1;
        position: static;
        width: 13px;
        height: 13px;
    }
    #main-container{
      padding: 0 20px;
    }
    .a{
        color: #428bca;
        cursor: pointer;
    }
</style>
<style>
    .xlsText{mso-number-format:"\@";}
    .xls2Decimal{mso-number-format:"0\.00"}
</style>

<div id="main-container">
  <div class="panel panel-default">
    <form class="no-margin form-inline" id="formValidate1" data-validate="parsley" novalidate="" method="post" action="/partner/settle/${action}">
        <input type="hidden" name="orgParentId" value="${orgParentId}">
      <div class="panel-body">
        <div class="row">
          <div style="float: left;margin-left:10px">
            <div class="form-group">
                日期：
                <input type="radio" class="dateType" name="timeFlag" value="0" <#if 0 == timeFlag>checked="true" </#if>/> 今日
                &nbsp;&nbsp;|&nbsp;&nbsp;<input type="radio" class="dateType"  name="timeFlag" value="1" <#if 1 == timeFlag>checked="true" </#if>/> 昨日
                &nbsp;&nbsp;|&nbsp;&nbsp;<input type="radio" class="dateType"  name="timeFlag" value="2" <#if 2 == timeFlag>checked="true" </#if>/> 近三天
                &nbsp;&nbsp;|&nbsp;&nbsp;<input type="radio" class="dateType"  name="timeFlag" value="3" <#if 3 == timeFlag>checked="true" </#if>/> 自定义时间
                <input id="start" type="text" name="beginTimeStr" style="cursor: pointer;"  readonly="readonly" value="<#if beginTimeStr?exists>${beginTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start">至<input type="text" id="end" name="endTimeStr" style="cursor: pointer;" readonly="readonly"  value="<#if endTimeStr?exists>${endTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end">
            </div>
          </div>

          <div style="float: left;margin-left:15px">
            <div class="form-group">
                <label>站点:</label>
                <select class="form-control" id="orgId" name="orgId" style="width: 200px;height: 25px;">
                    <option value="0" <#if orgId == 0 > selected="selected" </#if> >全部</option>
                    <#list orgList as org>
                        <option value="${org.orgId}" <#if org.orgId == orgId > selected="selected" </#if> >${org.orgName !''}</option>
                    </#list>
                </select>
            </div>
          </div>
        </div>
      </div>
        <div class="panel-footer text-left">
            <button type="button" class="btn btn-success js_submitbtn" actionurl="/partner/settle/${action}" >查询</button>&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-success js_submitbtn" actionurl="/partner/settle/orgAccountEx?orgLevelType=${orgLevelType}">导出EXCEL</button>
            <a style="margin-right:20px;float:right;" target="_blank"
               href="<#if strategyType==2>/partner/settlementStrategy/agent<#elseif strategyType==1>/partner/settlementStrategy/self</#if>">
                <label style="padding: 8px 0px;color:#4ac598;cursor:pointer;">结算规则</label>
                <i class="fa fa-question-circle fa-lg opration-icon"></i>
            </a>
        </div>
    </form>
  </div>
  
  <div class="panel panel-default table-responsive">
    <table class="table table-striped" id="responsiveTable">
      <thead>
        <tr>
          <th>站点名称</th>
          <th>订单数量(个)</th>
          <th>实际交易额(元)</th>
          <th>菜品原价(元)</th>
          <th>邮资(元)</th>
          <th>垫付餐费(元)</th>
          <th>取餐实付(元)</th>
          <th>送餐实收(元)</th>
          <th>取餐应付(元)</th>
          <th>送餐应收(元)</th>
            <#if orgType == 2>
                <th>应上交金额(元)</th>
            </#if>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      <#--循环-->
    <#if list?exists>
      <#list list as view>
        <tr>
            <td>
                <#if view_index == 0>
                    合计
                <#else>
                    <a class="a" target="_blank" href="/partner/settle/detail?orgId=${view.orgId}&timeFlag=${timeFlag}&beginTimeStr=${beginTimeStr}&endTimeStr=${endTimeStr}">
                    ${view.orgName}
                    </a>
                </#if>
            </td>
            <td>${view.orderCount}</td>
            <td class="xls2Decimal">${view.pkgValue?if_exists?string.number}</td>
            <td class="xls2Decimal">${view.pkgValue2?if_exists?string.number}</td>
            <td class="xls2Decimal">${view.postage?if_exists?string.number}</td>
            <td class="xls2Decimal">${view.dianfuFee?if_exists?string.number}</td>
            <td class="xls2Decimal">${view.actual_pay_amount?if_exists?string.number}</td>
            <td class="xls2Decimal">${view.actual_charge_amount?if_exists?string.number}</td>
            <td class="xls2Decimal">${view.plan_pay_amount?if_exists?string.number}</td>
            <td class="xls2Decimal">${view.plan_charge_amount?if_exists?string.number}</td>
          <#if orgType == 2>
            <td class="xls2Decimal">${view.plan_shangjiao_fee?if_exists?string.number}</td>
          </#if>
            <td>
                <#if view_index == 0>

                <#else>
                    <a data-toggle="tooltip" title="详情" data-placement="top" target="_blank" href="/partner/settle/detail?orgId=${view.orgId}&timeFlag=${timeFlag}&beginTimeStr=${beginTimeStr}&endTimeStr=${endTimeStr}">
                      <i class="fa fa-info-circle fa-lg opration-icon"></i>
                    </a>
                </#if>
            </td>
        </tr>
      </#list>
    </#if>
      </tbody>
    </table>

  <#import "../page/pager.ftl" as q>
  <#if recordCount??>
    <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/settle/${action}"/>
  </#if>
  </div>
</div>
<script>
    document.title = '${title}';
    $('select').select2();
</script>