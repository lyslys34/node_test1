<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">

<div id="main-container">
	<div id="nav"><br />  众包配送员打款>账单详情 </div><br />
    <div class="panel panel-default table-responsive">
    <h3 style="padding-left:10px">账单概览</h3>
    	<table class="table table-striped" id="" >
            <thead>
            <tr>
                <th>账单ID</th>
                <th>结算对象名称</th>
                <th>结算对象ID</th>
                <th>结算周期</th>
                <th>任务创建时间</th>
                <th>结算订单数</th>
                <th>结算金额(元)</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <th>${bill.id!""}</th>
	                <th>${bill.openAccountName!""}</th>
	                <th>${bill.openAccountId!""}</th>
	                <th>${bill.billTime!""}</th>
	                <th>${bill.ctime!""}</th>
	                <th>${count!0}</th>
	                <th>${bill.money!""}</th>
                </tr>
            </tbody>
        </table>
        
    	<h3 style="padding-left:10px">账单详情</h3>
    	<table class="table table-striped" id="" >
            <thead>
            <tr>
                <th>订单号</th>
                <th>订单完成时间</th>
                <th>商家名称</th>
                <th>订单类型</th>
                <th>订单金额(元)</th>
                <th>结算金额(元)</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list billDetailVos as vo>
                <tr>
                    <th>${vo.platformOrderId!""}</th>
                    <th>${vo.utime!""}</th>
                    <th>${vo.poiName!""}</th>
                    <th>${vo.payType!""}</th>
                    <th>${vo.price!""}</th>
                    <th>${vo.money!""}</th>
                </tr>
            </#list>
            </tbody>
        </table>
        
        <#import "../page/pager.ftl" as q>
	    <#if recordCount??>
	        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/publicSettleAudit/billDetail?settleBillId=${settleBillId}"/>
	    </#if>
        
    </div>
</div>
<script>document.title = '账单详情';</script>
