<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/settle/dateSimple.js</content>
<content tag="cssmain">/settle/withdraw_list.css</content>


<div id="main-container" >
    <div style="margin-top:20px;margin-left:20px;margin-right:20px">
        <div class="panel panel-default">
            <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/settleAccountInout/withdrawList" method="post" >
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">提现申请时间</label>
                                <div>
                                    <input id="start" type="text" name="beginTimeStr" style="cursor: pointer;"  value="<#if beginTimeStr?exists>${beginTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start">
                                    至<input type="text" id="end" name="endTimeStr" style="cursor: pointer;"  value="<#if endTimeStr?exists>${endTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">提现人</label>
                                <select class="form-control input-sm" id="openAccountName" name="openAccountName">
                                    <option value="" <#if openAccountName?? > <#else>selected="selected" </#if> >全部</option>
                                    <#if nameList??>
                                        <#list nameList as name>
                                            <#if name??>
                                                <option value="${name !''}" <#if openAccountName?? && name == openAccountName > selected="selected" </#if> >${name !''}</option>
                                            </#if>
                                        </#list>
                                    </#if>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">提现状态</label>
                                <select class="form-control input-sm" id="status" name="status">
                                    <option value="-1" <#if status?? && status == -1> selected="selected" </#if>>全部</option>
                                    <option value="1" <#if status?? && status == 1 > selected="selected" </#if>>银行已处理</option>
                                    <option value="2" <#if status?? && status == 2 > selected="selected" </#if>>提现失败</option>
                                    <option value="5" <#if status?? && status == 5 > selected="selected" </#if>>提现中</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2" style="padding-top:18px">
                            <button type="button" class="btn btn-success js_submitbtn" actionurl="/settleAccountInout/withdrawList" >查询</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="panel panel-default table-responsive">
            <table class="table table-striped" id="responsiveTable">
                <thead>
                    <tr>
                        <th></th>
                        <th>提现ID</th>
                        <th>提现人</th>
                        <th>提现金额</th>
                        <th>提现状态</th>
                        <th>提现失败原因</th>
                        <th>提现申请时间</th>
                        <th>收款人</th>
                        <th>收款银行</th>
                        <th>收款银行卡号</th>
                        <th>付款平台流水号</th>
                    </tr>
                </thead>
                <tbody>
                <#--循环-->
                    <#if page?? && page.bmSettleAccountInoutDetailViewList??>
                    <#list page.bmSettleAccountInoutDetailViewList as record>
                        <tr>
                            <td></td>
                            <td>${record.id}</td>
                            <td>${record.openAccountName!''}</td>
                            <td>￥${record.money!''}</td>
                            <td>${record.status}

                            </td>
                            <td>${record.failMsg!'-'}</td>
                            <td>${record.ctime}</td>
                            <td>${record.cardName!''}</td>
                            <td>${record.bankSub!''}</td>
                            <td>${record.cardNo!''}</td>
                            <td>${record.paymentOutNo!''}</td>
                        </tr>
                    </#list>
                    </#if>
                </tbody>
            </table>
        </div>

        <#import "../page/pager.ftl" as q>
        <#if recordCount??>
            <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/settleAccountInout/withdrawList"/>
        </#if>
    </div>
</div>
<script>document.title = '提现记录';$('select').select2();</script>
