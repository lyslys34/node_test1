<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/settle/dateSimple.js</content>
<content tag="cssmain">/settle/withdraw_list.css</content>

<div id="main-container" >
    <div class="panel panel-default">
        <div class="panel" style="padding-top:10px;height:40px;margin-bottom:3px">
            <span style="font-size: 15px; margin-left: 5px;">配送账户信息</span>
        </div>
        <div class="row" style="height: 20px;">
            <div class="col-md-3" style="margin-left: 10px;border-right:1px solid #eee;">
                <label style="font-size: 17px;margin-top: 5px;" >当前余额:${money?string(",##0.00#")}</label>
            </div>
            <div class="col-md-3">
                <label class="control-label">昨日支出:${yesIncome?string(",##0.00#")}</label>
            </div>
            <div class="col-md-3">
                <label class="control-label">本月累计支出:${monthIncome?string(",##0.00#")}</label>
            </div>
         </div>
         <div class="row" style="height: 20px;">
            <div class="col-md-3">
                <label class="control-label">昨日收入:${yesExpense?string(",##0.00#")}</label>
            </div>
            <div class="col-md-3">
                <label class="control-label">本月累计收入:${monthExpense?string(",##0.00#")}</label>
            </div>
         </div>
    </div>
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/settleAccountInout/monitor" method="post" >
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">查询时间从</label>
                        <input id="start" type="text" name="beginTimeStr" style="cursor: pointer;"  value="<#if beginTimeStr?exists>${beginTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start">
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">查询时间至</label>
                        <input type="text" id="end" name="endTimeStr" style="cursor: pointer;"  value="<#if endTimeStr?exists>${endTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end">
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-success js_submitbtn" actionurl="/monitor/list" >查询</button>
                    </div>
                </div>

            </div>
        </form>
        <div class="panel panel-default table-responsive">
            <table class="table table-striped" id="responsiveTable">
                <thead>
                <tr>
                    <th>任务ID</th>
                    <th>时间</th>
                    <th>收支类型</th>
                    <th>结算金额</th>
                    <th>账户余额</th>
                    <th>状态</th>
                    <th>备注</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <#--循环-->
                <#if page?? && page.bmSettleAccountFlowView??>
                    <#list page.bmSettleAccountFlowView as record>
                    <tr>
                        <td>${record.outID}</td>
                        <td>${record.ctime!''}</td>
                        <td><#if record.accFlowType==0>入账<#elseif record.accFlowType==1>出账</#if></td>
                        <#if record.accFlowType==0> <td style="color:green;">+${record.money?string(",##0.00#")}</td>
                        <#elseif record.accFlowType==1> <td style="color:red;">-${record.money?string(",##0.00#")}</td>
                        </#if>

                        <td>${record.balance?string(",##0.00#")}</td>
                        <td>成功</td>
                        <td>${record.comment!''}</td>
                        <td>
                            <#if record.accFlowType==1 && record.orgType!=4 && record.orgType!=5>
                            <a style="color:#428bca;cursor: pointer;" target="_blank" href="/monitor/detail/${record.outID!''}"  >
                                查看 </a>
                            </#if>
                        </td>
                    </tr>
                    </#list>
                </#if>
                </tbody>
            </table>
       </div>
    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/monitor/list"/>
    </#if>
    </div>
 </div>

