<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<#include "settle_config.ftl" >
<content tag="pageName">dispatcher_static_list</content>
<content tag="javascript">/finance/rider_statistic.js</content>
<content tag="cssmain">/finance/rider_statistic.css</content>
<style>
    .a hover{
        color: #58B7B1;
        text-decoration: underline;
    }
</style>
<div id="main-container">
    <div class="navi_head">
        <div class="navi_title">众包配送员结算报表</div>
        <div class="navi_bottom_line">&nbsp;</div>
    </div>
    <div class="alert small alert-warning alert-dismissible" style="<#if error?exists>display:block;<#else>display:none</#if>" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    ${error !''}
    </div>
    <form class="no-margin form-inline" id="fm" data-validate="parsley" novalidate="" method="post" action="/partner/settle/riderStatistics">
        <input type="hidden" name="detail" value="<#if detail??>${detail}<#else>0</#if>">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>起始时间:</label>
                            <input type="text" name="ts" style="cursor: pointer;"  readonly="readonly" value="<#if ts?exists>${ts}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start">
                        </div>
                    </div>
                    <div class="col-md-4">
                    <div class="form-group">
                        <label>配送员名称:</label>
                        <select name="riderName" class="form-control input-sm" style="width: 200px;">
                            <option <#if !RequestParameters.bmid?? || (RequestParameters.bmid == '-1')>selected="selected" </#if> value="-1">全部</option>
                            <#if userlist?exists >
                                <#list userlist as u >
                                    <option <#if RequestParameters.riderName ?exists && RequestParameters.riderName == (u.id?c)>selected="selected" </#if> value="${u.id !'-1'}">${u.name !''}</option>
                                </#list>
                            </#if>
                        </select>
                    </div>
                    </div>
                    <#--<div class="col-sm-3" style="width: 22%">-->
                        <#--<div class="form-group">-->
                            <#--<label>配送员类型:</label>-->
                            <#--<select name="jobType" style="width: 100px;height: 25px;">-->
                                <#--<option value="-1" <#if !jobType?exists || jobType == -1>selected="selected"</#if> >全部</option>-->
                                <#--<option value="2" <#if jobType?exists && jobType == 2 >selected="selected"</#if> >兼职</option>-->
                                <#--<option value="1" <#if jobType?exists && jobType == 1 >selected="selected"</#if> >全职</option>-->
                            <#--</select>-->
                        <#--</div>-->
                    <#--</div>-->
                    <div style="float:right;padding-right: 15px">
                        <button type="button" class="btn btn-success js_submitbtn" style="width:90px" actionurl="/partner/settle/riderStatistics">查询</button>
                    </div>
                </div>
                <div class="row" style="margin-top: 5px">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>结束时间:</label>
                            <input type="text" name="te" style="cursor: pointer;" readonly="readonly"  value="<#if te?exists>${te}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label style="margin-left: 13px">配送员ID:</label>
                            <input type="text" style="width: 200px;" name="riderId" value="<#if RequestParameters.riderId ?exists>${RequestParameters.riderId}</#if>" maxlength="20" class="form-control input-sm">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-bottom: 10px; padding-right:20px">
            <div class="col-md-4"><h5><i class="glyphicon glyphicon-bookmark opration-icon"></i><#if orgName??>${orgName}</#if> 配送员结算报表</h5></div>
            <div style="float:right;padding-right: 10px"><button type="button" class="btn btn-success js_submitbtn" style="float: right;width:90px" actionurl="/partner/settle/riderStatisticsExcel">导出EXCEL</button></div>
        </div>
    </form>
    <div class="panel panel-default table-responsive" style="overflow-x: auto">
        <#if allpage?exists>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>配送员</th>
                    <th>配送员ID</th>
                    <th>配送员类型</th>
                    <th>订单总数</th>
                    <th>运费总额</th>
                    <th>补贴总额</th>
                    <th>审核不通过总数</th>
                    <th>订单总额</th>
                    <th>垫付餐费</th>
                    <th>应付总额</th>
                    <th>实付总额</th>
                    <th>应付实付总差额</th>
                    <th>应收总额</th>
                    <th>实收总额</th>
                    <th>应收实收总差额</th>
                    <th>详情</th>
                </tr>
                </thead>
                <#if (allpage.zbRiderStatisticViewList)?exists>
                    <#list allpage.zbRiderStatisticViewList as a>
                        <tr>
                            <td>${a.bmUserName !''}</td>
                            <td>${a.bmUserId!''}</td>
                            <td><#if a.jobType == 1 >全职</#if><#if a.jobType == 2 >兼职</#if></td>
                            <td>${a.finishPkgCount !''}</td>
                            <td>￥${a.allPostage !''}</td>
                            <td>￥${a.allBuTie !''}</td>
                            <td>${a.auditRejectCount!''}</td>
                            <td>￥${a.allValue !''}</td>
                            <td>￥${a.allPrepay !''}</td>
                            <td>￥${a.planPayAmount !''}</td>
                            <td>￥${a.actualPayAmount !''}</td>
                            <td>￥${a.planPayAmount-a.actualPayAmount}</td>
                            <td>￥${a.planChargeAmount !''}</td>
                            <td>￥${a.actualChargeAmount !''}</td>
                            <td>￥${a.planChargeAmount-a.actualChargeAmount}</td>
                            <td>
                                <a href="riderStatistics?ts=${Request.ts!''}&te=${Request.te !''}&riderName=${a.bmUserId !''}&detail=1" target="_blank" class="a">
                                    <i class="fa fa-info-circle fa-lg opration-icon"></i>
                                </a>
                            </td>
                        </tr>
                    </#list>
                </#if>
            </table>
        <#elseif singlepage?exists>
            <#--<table class="table">-->
                <#--<tr>-->
                    <#--<th>配送员姓名</th>-->
                    <#--<th>配送员ID</th>-->
                    <#--<th>完成订单数</th>-->
                    <#--<th>订单总额(元)</th>-->
                    <#--<th>补贴总额(元)</th>-->
                    <#--<th>运费总额(元)</th>-->
                    <#--<th>垫付款总额(元)</th>-->
                <#--</tr>-->
                <#--<#if (singlepage.zbRiderStatisticView)?exists>-->
                    <#--<tr>-->
                        <#--<td>${singlepage.zbRiderStatisticView.bmUserName!''}</td>-->
                        <#--<td>${singlepage.zbRiderStatisticView.bmUserId!''}</td>-->
                        <#--<td>${singlepage.zbRiderStatisticView.finishPkgCount !''}</td>-->
                        <#--<td>￥${singlepage.zbRiderStatisticView.allValue!''}</td>-->
                        <#--<td>￥${singlepage.zbRiderStatisticView.allBuTie !''}</td>-->
                        <#--<td>￥${singlepage.zbRiderStatisticView.allPostage !''}</td>-->
                        <#--<td>￥${singlepage.zbRiderStatisticView.allPrepay !''}</td>-->
                    <#--</tr>-->
                <#--</#if>-->
            <#--</table>-->

            <#--<div class="table_title form-inline">-->
                <#--&lt;#&ndash;<span><p>结算细目</p></span>&ndash;&gt;-->
                <#--<a href="riderStatisticsExcel?bmid=${RequestParameters.bmid!''}&ts=${Request.ts!''}&te=${Request.te!''}" target="_blank" class="btn btn-sm btn-default">导出表格</a>-->
            <#--</div>-->
            <#--<div></div>-->
            <table class="table table-striped">
                <tr>
                    <th>配送员</th>
                    <th>配送员ID</th>
                    <th>订单编号</th>
                    <th>订单完成时间</th>
                    <th>配送员类型</th>
                    <th>商家名称</th>
                    <th>订单类型</th>
                    <th>线下垫款</th>
                    <th>运费</th>
                    <th>补贴</th>
                    <th>订单金额</th>
                    <th>菜品原价</th>
                    <th>垫付餐费</th>
                    <td>取餐应付</td>
                    <td>取餐实付</td>
                    <td>应付实付差额</td>
                    <td>送餐应收</td>
                    <td>送餐实收</td>
                    <td>应收实收差额</td>
                    <td>审核结果</td>
                </tr>

                <#if (singlepage.zbRiderDetailViewList)?exists>
                    <#list  singlepage.zbRiderDetailViewList as a>
                        <tr>
                            <td>${a.riderName}</td>
                            <td>${a.riderId}</td>
                            <td>${a.orderViewId !''}</td>
                            <td>${a.finishTimeStr !''}</td>
                            <td><#if a.jobType == 1 >全职</#if><#if a.jobType == 2>兼职</#if></td>
                            <td>${a.poiName !''}</td>
                            <td><#if a.orderType == 1>在线支付</#if><#if a.orderType == 0>货到付款</#if></td>
                            <td><#if a.logisticsOffline == 1 >是<#else>否</#if></td>
                            <td>￥${a.postageValue !''}</td>
                            <td>￥${a.butieValue !''}</td>
                            <td>￥${a.pkgValue !''}</td>
                            <td>￥${a.goodsValue !''}</td>
                            <td>￥${a.prepayValue !''}</td>
                            <td>￥${a.planPayAmount !''}</td>
                            <td>￥${a.actualPayAmount !''}</td>
                            <td>￥${a.planPayAmount-a.actualPayAmount}</td>
                            <td>￥${a.planChargeAmount !''}</td>
                            <td>￥${a.actualChargeAmount !''}</td>
                            <td>￥${a.planChargeAmount-a.actualChargeAmount}</td>
                            <td>${a.auditStatus !''}</td>
                        </tr>
                    </#list>
                </#if>
            </table>
        </#if>
        <#import "../page/pager.ftl" as q>
        <#if recordCount??>
            <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/settle/riderStatistics"/>
        </#if>
        <#--<div style="text-align: right;">-->
            <#--<#noescape>${pagehtml!''}</#noescape>-->
        <#--</div>-->
    </div>
</div>
<script>document.title = '众包配送员报表';
$('select').select2();</script>