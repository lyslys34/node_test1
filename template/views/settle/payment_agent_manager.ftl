<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/partner/settle/agentManager" method="post" >
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">加盟商ID</label>
                            <#--<input type="text" class="form-control input-sm parsley-validated" name="id" id="id" data-required="true" data-minlength="8" value="${id!""}"/>-->
                            <select class="form-control input-sm" id="id" name="id" data-minlengt="8">
                                <option value="">全部</option>
                                <#list orgIds as singleOrg>
                                    <option value="${singleOrg !''}" <#if id?? && singleOrg == id > selected="selected" </#if> >${singleOrg !''}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">加盟商名称</label>
                            <select class="form-control input-sm" id="name" name="name" data-minlengt="8">
                                <option value="">全部</option>
                                <#list orgNames as singleOrg>
                                    <option value="${singleOrg !''}" <#if name?? && singleOrg == name > selected="selected" </#if> >${singleOrg !''}</option>
                                </#list>
                            </select>
                            <!--<input type="text" class="form-control input-sm parsley-validated" name="name" id="name" data-required="true" data-minlength="8" value="${name!""}"/>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-left">
                <button type="submit" class="btn btn-success">查询</button>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <#if failMessage??>
            <h4 style="padding-left:10px">${failMessage}</h4>
        <#else>
            <h4 style="padding-left:10px">打款数据汇总</h4>
            <table class="table table-striped" id="" >
                <thead>
                <tr>
                    <th>加盟商总数(个)</th>
                    <th>未结算账单总数(个)</th>
                    <th>未结算金额(元)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>${num!""}</td>
                    <td>${count!""}</td>
                    <td>${sum!""}</td>
                </tr>
                </tbody>
            </table>

            <h4 style="margin-top:30px;padding-left:10px">打款数据细目</h4>
            <table class="table table-striped" id="">
                <thead>
                <tr>
                    <th>加盟商ID</th>
                    <th>加盟商名称</th>
                    <th>未结算账单总数(个)</th>
                    <th>未结算金额(元)</th>
                    <th>最新打款成功时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <#--循环-->
                    <#if riders??>
                        <#list riders as rider>
                        <tr>
                            <td>${rider.riderId!""}</td>
                            <td>${rider.riderName!""}</td>
                            <td>${rider.settleCount!""}</td>
                            <td>${rider.settleSumMoney!"-"}</td>
                            <td>${rider.lastPayedSuccessTime!"-"}</td>
                            <td>
                                <a data-toggle="tooltip" title="账单详情" data-placement="top" href="/partner/settle/agentOrg?orgId=${rider.riderId!""}" target="_blank" >
                                    <i class="fa fa-info-circle fa-lg opration-icon"></i>
                                </a>
                            </td>
                        </tr>
                        </#list>
                    </#if>
                </tbody>
            </table>
        </#if>

        <#import "../page/pager.ftl" as q>
        <#if recordCount??>
            <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/settle/agentManager"/>
        </#if>

    </div>
</div>
<script>document.title = '加盟商打款进度';$('select').select2();</script>
