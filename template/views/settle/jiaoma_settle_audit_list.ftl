<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">

<script type="text/javascript" src="${staticPath}/js/page/settle/jiaoma_settle_audit.js"></script>

<style type="text/css">

input[type="checkbox"] {
  opacity: 1;
}
</style>

<div id="main-container" >
<div style="margin-top:20px;margin-left:20px;margin-right:20px">
    <div id="nav">&nbsp;&nbsp;角马配送员打款 </div><br />
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/publicSettleAudit/jiaomaPage" method="post" >
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">账单ID</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="settleBillId" id="settleBillId" data-required="true" data-minlength="8" value="${riderId!""}"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">结算对象名称</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="openAccountName" id="openAccountName" data-required="true" data-minlength="8" value="${openAccountName!""}"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">付款状态</label>
                            <select class="form-control input-sm" id="status" name="status">
                                <option value="" <#if status?? > selected="selected" </#if>>全部</option>
                                <option value="10" <#if status?? && status == "10" > selected="selected" </#if>>待付款</option>
                                <option value="20" <#if status?? && status == "20" > selected="selected" </#if>>已付款</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-left">
                <button type="submit" class="btn btn-success">查询</button>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">

        <table class="table table-striped" id="">
            <thead>
            <tr>
                <th>
                    <label style="margin-right:5px;">全选</label><input type="checkbox" value="">
                </th>
                <th>账单ID</th>
                <th>结算周期</th>
                <th>结算对象名称</th>
                <th>结算对象ID</th>
                <th>任务建立时间</th>
                <th>入账金额</th>
                <th>银行信息</th>
            	<th>付款状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list vos as vo>
            <tr>
                <td><input type="checkbox" value="${vo.id}" <#if vo.payStatus != "生成未打款">disabled</#if>></td>
                <td>${vo.id}</td>
                <td>${vo.billTime!""}</td>
                <td>${vo.openAccountName!""}</td>
                <td>${vo.openAccountId!""}</td>
                <td>${vo.ctime!""}</td>
                <td>${vo.money!""}</td>
                <td><p>银行卡号:${vo.cardNo!""}</p><p>开户行:${vo.bankSub!""}</p>对公对私:<#if vo.cardType?? ><#if vo.cardType ==1>对公<#else>对私</#if></#if></td>
                <td>${vo.payStatus!""}</td>
                <td>
                    <div class="btn-group hover-dropdown">
                        <button class="btn btn-xs btn-info dropdown-toggle" data-toggle="dropdown">操作<span class="caret"></span></button>
                        <ul class="dropdown-menu">
                        	<li><a href="/publicSettleAudit/jiaomaBillDetail?settleBillId=${vo.id}" target="_blank">查看详情</a></li>
                            <#if vo.payStatus == "生成未打款">
                            <li><a href="#payModal" role="button" data-toggle="modal" class="pay_link" rel="${vo.id}" >打款</a></li>
                        	</#if>
                        </ul>
                    </div>
                </td>
            </tr>
            </#list>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/publicSettleAudit/jiaomaPage"/>
    </#if>
    
    <div class="panel-footer text-left">
    	<div class="row">
    		<div class="col-md-2">
	    		<span>已经选择了<span id="selectNum">0</span>个任务</span>
	    	</div>
	    	<div class="col-md-3">
	    		<a href="#payModal" role="button" data-toggle="modal" class="btn btn-success" id="pay_button">批量打款</a>
	        </div>
        </div>
    </div>


     <form class="no-margin" id="payForm" data-validate="parsley" novalidate="" method="post" action="">
        <div class="modal fade" id="payModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>将要同意打款，确认?</h4>
                        <div class="modal-footer">
                            <input type="submit" value="同意" class="btn btn-success btn-sm" />
                            <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true">取消</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </form>

    </div>
</div>
</div>
<script>document.title = '角马打款';</script>
