<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<#include "settle_config.ftl" >
<content tag="pageName">dispatcher_static_list</content>
<content tag="javascript">/finance/rider_statistic.js</content>
<content tag="cssmain">/finance/rider_statistic.css</content>
<div id="main-container">
    <div css="content_body">
        <div class="navi_head">
            <div class="navi_title">众包配送员结算报表</div>
            <div class="navi_bottom_line">&nbsp;</div>
        </div>
        <div class="condition_area">
            <div class="alert small alert-warning alert-dismissible" style="<#if error?exists>display:block;<#else>display:none</#if>" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                ${error !''}
            </div>
            <form id="fm" method="get" action="riderStatistics" class="form-inline">
                配送员名称&nbsp;
                <select name="bmid" class="form-control input-sm" style="max-width:100px;">
                    <option <#if !RequestParameters.bmid?? || (RequestParameters.bmid == '-1')>selected="selected" </#if> value="-1">全部</option>
                    <#if userlist?exists >
                        <#list userlist as u >
                            <option <#if RequestParameters.bmid ?exists && RequestParameters.bmid == (u.id?c)>selected="selected" </#if> value="${u.id !'-1'}">${u.name !''}</option>
                        </#list>
                    </#if>
                </select>
                &nbsp;&nbsp;&nbsp;&nbsp;
                结算时间段&nbsp;
                <input type="text" name="ts" style="cursor: pointer" readonly="readonly" value="<#if ts?exists>${ts}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start"/>
                至&nbsp;
                <input type="text" name="te" style="cursor: pointer" readonly="readonly" value="<#if te?exists>${te}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end"/>

                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="button" value="查看" class="btn btn-success js_submitbtn"/>
            </form>

        </div>

        <div class="result_area">
            <p>查询结果</p>
            <#if allpage?exists>
                <table class="table">
                    <tr>
                        <th>数据项</th>
                        <th>配送员数</th>
                        <th>订单总量</th>
                        <th>订单总额(元)</th>
                        <th>补贴总额(元)</th>
                        <th>运费总额(元)</th>
                        <th>垫付款总额(元)</th>
                    </tr>
                    <#if (allpage.zbRiderStatisticView)?exists>
                        <tr>
                            <td>总计</td>
                            <td>${allpage.zbRiderStatisticView.userCountTotal !''}</td>
                            <td>${allpage.zbRiderStatisticView.finishPkgCount !''}</td>
                            <td>￥${allpage.zbRiderStatisticView.allValue !''}</td>
                            <td>￥${allpage.zbRiderStatisticView.allBuTie !''}</td>
                            <td>￥${allpage.zbRiderStatisticView.allPostage !''}</td>
                            <td>￥${allpage.zbRiderStatisticView.allPrepay !''}</td>
                        </tr>
                    </#if>
                </table>

                <div class="table_title form-inline">
                    <span><p>结算细目</p></span>
                    <a href="riderStatisticsExcel?bmid=${RequestParameters.bmid!''}&ts=${Request.ts!''}&te=${Request.te!''}" target="_blank" class="btn btn-sm btn-default">导出表格</a>
                </div>
                <div></div>
                <table class="table">
                    <tr>
                        <th>序号</th>
                        <th>配送员姓名</th>
                        <th>配送员ID</th>
                        <th>完成订单数</th>
                        <th>订单总额(元)</th>
                        <th>补贴总额(元)</th>
                        <th>运费总额(元)</th>
                        <th>垫付款总额(元)</th>
                        <th>款项明细</th>
                    </tr>

                    <#if (allpage.zbRiderStatisticViewList)?exists>
                        <#list  allpage.zbRiderStatisticViewList as a>
                            <tr>
                                <td>${a_index+1}</td>
                                <td>${a.bmUserName !''}</td>
                                <td>${a.bmUserId!''}</td>
                                <td>${a.finishPkgCount !''}</td>
                                <td>￥${a.allValue !''}</td>
                                <td>￥${a.allBuTie !''}</td>
                                <td>￥${a.allPostage !''}</td>
                                <td>￥${a.allPrepay !''}</td>
                                <td><a href="riderStatistics?ts=${Request.ts!''}&te=${Request.te !''}&bmid=${a.bmUserId !''}" target="_blank" class="mainc">查看详情&gt;&gt;</a></td>
                            </tr>

                        </#list>
                    </#if>
                </table>
            <#elseif singlepage?exists>
                <table class="table">
                    <tr>
                        <th>配送员姓名</th>
                        <th>配送员ID</th>
                        <th>完成订单数</th>
                        <th>订单总额(元)</th>
                        <th>补贴总额(元)</th>
                        <th>运费总额(元)</th>
                        <th>垫付款总额(元)</th>
                    </tr>
                    <#if (singlepage.zbRiderStatisticView)?exists>
                        <tr>
                            <td>${singlepage.zbRiderStatisticView.bmUserName!''}</td>
                            <td>${singlepage.zbRiderStatisticView.bmUserId!''}</td>
                            <td>${singlepage.zbRiderStatisticView.finishPkgCount !''}</td>
                            <td>￥${singlepage.zbRiderStatisticView.allValue!''}</td>
                            <td>￥${singlepage.zbRiderStatisticView.allBuTie !''}</td>
                            <td>￥${singlepage.zbRiderStatisticView.allPostage !''}</td>
                            <td>￥${singlepage.zbRiderStatisticView.allPrepay !''}</td>
                        </tr>
                    </#if>
                </table>

                <div class="table_title form-inline">
                    <span><p>结算细目</p></span>
                    <a href="riderStatisticsExcel?bmid=${RequestParameters.bmid!''}&ts=${Request.ts!''}&te=${Request.te!''}" target="_blank" class="btn btn-sm btn-default">导出表格</a>
                </div>
                <div></div>
                <table class="table">
                    <tr>
                        <th>序号</th>
                        <th>订单号</th>
                        <th>订单完成时间</th>
                        <th>商家名称</th>
                        <th>商家结算类型</th>
                        <th>线下垫款</th>
                        <th>订单类型</th>
                        <th>订单金额</th>
                        <th>补贴金额</th>
                        <th>运费金额</th>
                        <th>垫付款金额</th>
                    </tr>

                    <#if (singlepage.zbRiderDetailViewList)?exists>
                        <#list  singlepage.zbRiderDetailViewList as a>
                            <tr>
                                <td>${a_index+1}</td>
                                <td>${a.orderViewId !''}</td>
                                <td>${a.finishTimeStr !''}</td>
                                <td>${a.poiName !''}</td>
                                <td><#if a.isSettle == 1>线上支付<#elseif a.isSettle == 0>线下支付<#else>-</#if></td>
                                <td><#if a.logisticsOffline == 1 >是<#else>否</#if></td>
                                <td><#if a.orderType == 1>在线支付</#if><#if a.orderType == 0>货到付款</#if></td>
                                <td>￥${a.pkgValue !''}</td>
                                <td>￥${a.butieValue !''}</td>
                                <td>￥${a.postageValue !''}</td>
                                <td>￥${a.prepayValue !''}</td>
                            </tr>

                        </#list>

                    </#if>
                </table>
            </#if>
            <div style="text-align: right;">
                <#noescape>${pagehtml!''}</#noescape>
            </div>


        </div>



    </div>
</div>
<script>document.title = '众包配送员报表';
$('select').select2();</script>