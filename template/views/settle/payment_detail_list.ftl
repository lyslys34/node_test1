<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">

<style type="text/css">
    .tipWin{
        width: 180px;
        position: absolute;
        left: 170px;
        top: 90px;
        display: none;
    }

    .hoverTip{
        cursor: pointer;
    }
</style>

<script>
$(document).ready(function(){
    $('.hoverTip').mouseover(function(){
        $('.tipWin').fadeIn();
    });

    $('.hoverTip').mouseout(function(){
        $('.tipWin').fadeOut();
    });
});
</script>

<div id="main-container">
    <div class="panel panel-default table-responsive">
        <div>
            <ul class="breadcrumb">
                <li style="color:#757986;font-size:14px;">结算中心<span class="divider"></span></li>
                <li style="color:#757986;font-size:14px;">打款进度<span class="divider"></span></li>
                <li style="color:#757986;font-size:14px;">打款进度账单列表<span class="divider"></span></li>
                <li style="color:#757986;font-size:14px;">账单详情<span class="active"></span></li>
            </ul>
        </div>

        <div style="display: inline-block;">
            <h4 style="padding-left:10px;">账单详情（ID:${settleBillId!''}）</h4>
        </div>

        <div style="display: inline-block;float: right;padding-top: 20px;">
            <a href="exportPaymentDetail?settleBillId=${settleBillId!''}" target="_blank" class="btn btn-sm btn-default pull-right" style="margin-right:20px;float:right;">导出表格</a>
            <a name="detail" style="margin-right:20px;" target="_blank"
               href="<#if orgType==2>/partner/settlementStrategy/agent<#elseif orgType==1>/partner/settlementStrategy/self</#if>">
                <label style="padding: 8px 0px;color:#4ac598;cursor:pointer;">结算规则</label>
                <i class="fa fa-question-circle fa-lg opration-icon"></i>
            </a>
        </div>

        <!-- Nav tabs Start-->
        <#if orgType == 2>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="/partner/settle/paymentDetail?settleBillId=${settleBillId!''}">正常订单</a></li>
                <#if moneyType?? && moneyType!=10>
                <#else>
                    <li role="presentation"><a href="/partner/settle/paymentDeductionDetail?settleBillId=${settleBillId!''}">异常订单</a></li>
                </#if>
            </ul>
            <div style="width: 100%;height: 2px;background: #58B7B1;"></div>
        </#if>
        <!-- Nav tabs End-->

    	<table class="table table-striped" id="" >
            <thead>
            <tr>
                <th>订单已送达时间</th>
                <#if orgType == 2>
                    <#if moneyType == 10><th>邮资金额(元)</th>
                    <#else><th class="hoverTip">垫付餐费(元)</th><div class="alert alert-warning tipWin" role="alert">垫付餐费=实付商家-实收用户</div>
                        <th>实付商家(元)</th><th>实收用户(元)</th>
                    </#if>
                <#else>
                    <th class="hoverTip">垫付餐费(元)</th><div class="alert alert-warning tipWin" role="alert">垫付餐费=实付商家-实收用户</div>
                    <th>实付商家(元)</th><th>实收用户(元)</th>
                </#if>
                <th>订单价格(元)</th>
                <th>菜品原价(不含配送费)</th>
                <#if orgType == 2 && moneyType == 10>
                <th>补贴(元)</th>
                </#if>
                <th>商家名称</th>
                <#if orgType == 2 && moneyType == 10>
                    <th>骑手名称</th>
                </#if>
                <th>商家类型</th>
                <th>订单类型</th>
                <th>订单ID</th>
                <th>备注</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list billDetailVos as vo>
                <tr>
                    <th>${vo.utime!""}</th>
                    <th>${vo.money!""}</th>
                    <#if orgType == 2 && moneyType == 10><#else>
                        <th>${vo.actualPay!""}</th>
                        <th>${vo.actualCharge!""}</th>
                    </#if>
                    <th>${vo.price!""}</th>
                    <th>${vo.pkgValue!""}</th>
                    <#if orgType == 2 && moneyType == 10>
                    <th>${vo.subsidy!""}</th>
                    </#if>
                    <th>${vo.poiName!""}</th>
                    <#if orgType == 2 && moneyType == 10>
                    <th>${vo.riderName!""}</th>
                    </#if>
                    <th>${vo.poiType!""}</th>
                    <th>${vo.payType!""}</th>
                    <th>${vo.platformOrderId!""}</th>
                    <th>${vo.showName!""}</th>
                </tr>
            </#list>
            </tbody>
        </table>
        
        <#import "../page/pager.ftl" as q>
	    <#if recordCount??>
	        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/settle/paymentDetail?settleBillId=${settleBillId}"/>
	    </#if>
        
    </div>
</div>
<script>document.title = '账单详情';</script>
