<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">

<script type="text/javascript" >
$(document).ready(function(){
 	var settleBillId;
	init();
    $('[data-placement="top"]').tooltip(); 
});
function init() {
	$(".reWithdrawal_link").click(function() {
		settleBillId = $(this).attr('rel');
	});

	$("#commit").click(function() {
        $("#commit").attr("disabled","disabled");
        $.ajax({
	        dataType: 'json',
	        type : 'post',
	        url : "/partner/settle/reWithdrawal",
	        data: {
				settleBillId:settleBillId
			},
	        success : function(data){
	        	var returnMsg;
	            if(data.success){
	            	returnMsg = "重新提现成功";
	            }else {
	            	returnMsg = data.message;
	            }
            	$("#cancel").click();
	            $("#returnMsg").text(returnMsg);
            	$('#resultModal').modal('show')
	        },
	        error:function(XMLHttpRequest ,errMsg){
	            alert("网络连接失败");
	        }
	     });
	});
}
</script>

<div id="main-container">
    <div class="panel panel-default table-responsive">
        <div>
            <ul class="breadcrumb">
                <li style="color:#757986;font-size:14px;">结算中心<span class="divider"></span></li>
                <li style="color:#757986;font-size:14px;">打款进度<span class="divider"></span></li>
                <li style="color:#757986;font-size:14px;">打款进度账单列表<span class="active"></span></li>
            </ul>
        </div>

        <#--<div style="display: inline-block;">-->
            <#--<h3 style="padding-left:10px;">加盟商（${settleBillId!''}）打款进度</h3>-->
        <#--</div>-->

    	<h3 style="padding-left:10px">未结算账单</h3>
    	<table class="table table-striped" id="" >
            <thead>
            <tr>
                <#--由progress进入，则只有一个账户；由agentOrg进入，则过渡期有多个账户-->
                <#if orgName??>
                    <th>组织ID</th>
                    <th>组织名</th>
                <#elseif orgId??>
                    <th>组织ID</th>
                </#if>
                <th>账单结算时间</th>
                <th>打款状态</th>
                <th>打款失败原因</th>
                <th>账单金额(元)</th>
                <th>账单类型</th>
                <th>账单ID</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list notPayedBills as notPayedBill>
                <tr>
                    <#--由progress进入，则只有一个账户；由agentOrg进入，则过渡期有多个账户-->
                    <#if orgName??>
                        <th>${notPayedBill.orgId!""}</th>
                        <th>${notPayedBill.orgName!""}</th>
                    <#elseif orgId??>
                        <th>${orgId!""}</th>
                    </#if>
                    <td>${notPayedBill.billTime!""}</td>
                    <td>${notPayedBill.statusStr!""}</td>
                    <td>${notPayedBill.outMsg!""}</td>
                    <td>${notPayedBill.money!""}</td>
                    <td>
                        <#if notPayedBill.moneyType?? && notPayedBill.moneyType!="">
                            ${notPayedBill.moneyType!""}
                        <#else>
                            <#if billType == 1>垫付款<#elseif billType == 2>邮资</#if>
                        </#if>
                    </td>
                    <td>${notPayedBill.id!""}</td>
                    <td>
                        <a data-toggle="tooltip" title="详情" data-placement="top" target="_blank" href="/partner/settle/paymentDetail?settleBillId=${notPayedBill.id!""}">
                            <i class="fa fa-info-circle fa-lg opration-icon"></i>
                        </a>
                        <#if notPayedBill.statusStr == "打款失败" >
                        <a title="重新提现" data-placement="top" href="#reWithdrawalModal" role="button" data-toggle="modal" class="reWithdrawal_link" rel="${notPayedBill.id!""}">
                            <i class="fa fa-money fa-lg opration-icon"></i>
                        </a>
                        </#if>
					</td>
                </tr>
            </#list>
            </tbody>
        </table>
        
        <h3 style="margin-top:30px; padding-left:10px">已结算账单</h3>
        <table class="table table-striped" id="">
            <thead>
            <tr>
                <#if orgName??>
                    <th>组织名</th>
                </#if>
                <th>账单结算时间</th>
                <th>打款状态</th>
                <th>账单金额(元)</th>
                <th>账单类型</th>
                <th>账单ID</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list payedBills as payedBill>
                <tr>
                    <#if orgName??>
                        <th>${payedBill.orgName!""}</th>
                    </#if>
                    <td>${payedBill.billTime!""}</td>
                    <td>${payedBill.statusStr!""}</td>
                    <td>${payedBill.money!""}</td>
                    <td>
                        <#if payedBill.moneyType?? && payedBill.moneyType!="">
                        ${payedBill.moneyType!""}
                        <#else>
                            <#if billType == 1>垫付款<#elseif billType == 2>邮资</#if>
                        </#if>
                    </td>
                    <td>${payedBill.id!""}</td>
                    <td>
                        <a data-toggle="tooltip" title="详情" data-placement="top" target="_blank" href="/partner/settle/paymentDetail?settleBillId=${payedBill.id!""}">
                            <i class="fa fa-info-circle fa-lg opration-icon"></i>
                        </a>
                    </td>
                </tr>
            </#list>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <#if settleAccountId??>
            <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/settle/progress?settleAccountId=${settleAccountId}"/>
        <#elseif orgId??>
            <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/settle/agentOrg?orgId=${orgId}"/>
        <#else>
            <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/settle/agentOrg"/>
        </#if>
    </#if>
    
    <form class="no-margin" data-validate="parsley" novalidate="" method="post" action="">
        <div class="modal fade" id="reWithdrawalModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>将要重新提现，确认?</h4>
                        <div class="modal-footer">
                            <input type="button" id="commit" value="确认" class="btn btn-success btn-sm" />
                            <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" id="cancel">取消</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </form>
     
     <form class="no-margin" id="reWithdrawalForm" data-validate="parsley" novalidate="" method="post" action="">
        <div class="modal fade" id="resultModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 id="returnMsg"></h4>
                        <div class="modal-footer">
                            <input type="button" onclick="javascript:location.reload();" value="确认" class="btn btn-success btn-sm" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </form>

    </div>
</div>
<script>document.title = '打款进度';</script>
