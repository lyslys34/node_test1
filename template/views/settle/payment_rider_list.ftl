<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">

<div id="main-container">
    <div class="panel panel-default">
        <div>
            <ul class="breadcrumb">
                <li style="color:#757986;font-size:14px;">结算中心<span class="divider"></span></li>
                <li style="color:#757986;font-size:14px;">打款进度<span class="divider"></span></li>
                <li style="color:#757986;font-size:14px;">打款进度账单列表<span class="divider"></span></li>
            </ul>
        </div>

        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/partner/settle/selfRider" method="post" >
            <div class="panel-body">
                <div class="row">
                    <!--<div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">骑手ID</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="id" id="id" data-required="true" data-minlength="8" value="${id!""}"/>
                        </div>
                    </div>-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">骑手姓名</label>
                            <select class="form-control input-sm" id="name" name="name">
                                <option value=" ">全部</option>
                                <#list riderNames as singleName>
                                    <option value="${singleName !''}" <#if name?? && singleName == name > selected="selected" </#if> >${singleName !''}</option>
                                </#list>
                            </select>
                            <!--<input type="text" class="form-control input-sm parsley-validated" name="name" id="name" data-required="true" data-minlength="8" value="${name!""}"/>-->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">骑手手机号</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="mobile" id="mobile" data-required="true" data-minlength="8" value="${mobile!""}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-left">
                <button type="submit" class="btn btn-success">查询</button>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
    	<h4 style="padding-left:10px">打款数据汇总</h4>
    	<table class="table table-striped" id="" >
            <thead>
            <tr>
                <th>骑手总数(个)</th>
                <th>未结算账单总数(个)</th>
                <th>未结算金额(元)</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>${num!""}</td>
                    <td>${count!""}</td>
                    <td>${sum!"-"}</td>
                </tr>
            </tbody>
        </table>
        
        <h4 style="margin-top:30px;padding-left:10px">打款数据细目</h4>
        <table class="table table-striped" id="">
            <thead>
            <tr>
                <th>骑手姓名</th>
                <th>骑手手机号</th>
                <th>未结算账单总数(个)</th>
                <th>未结算金额(元)</th>
                <th>最新打款成功时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list riders as rider>
                <tr>
                    <td>${rider.riderName!""}</td>
                    <td>${rider.mobile!""}</td>
                    <td>${rider.settleCount!""}</td>
                    <td>${rider.settleSumMoney!"-"}</td>
                    <td>${rider.lastPayedSuccessTime!"-"}</td>
                    <td>
                        <a data-toggle="tooltip" title="账单详情" data-placement="top" target="_blank" href="/partner/settle/progress?settleAccountId=${rider.settleAccountId!""}">
                            <i class="fa fa-info-circle fa-lg opration-icon"></i>
                        </a>
                    </td>
                </tr>
            </#list>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/settle/selfRider"/>
    </#if>

    </div>
</div>
<script>document.title = '打款进度';$('select').select2();</script>
