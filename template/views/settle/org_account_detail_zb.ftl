<#include "settle_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/settle/date.js</content>
<content tag="cssmain">/statistics/daily_data.css</content>
<link rel="stylesheet" type="text/css" href="/static/css/lib/touchspin/jquery.bootstrap-touchspin.min.css" />
<style>
    input[type="radio"],
    input[type="checkbox"] {
        opacity:1;
        position: static;
        width: 13px;
        height: 13px;
    }
</style>

<div id="main-container">
    <div id="nav" style="color:#757986;font-size:14px;"><br />站点对账 > 站点订单详情 </div><br />
    <form class="no-margin form-inline" id="formValidate1" data-validate="parsley" novalidate="" method="post" action="/partner/settle/detail">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>起始时间:</label>
                            <input id="start" type="text" name="beginTimeStr" style="cursor: pointer;"  readonly="readonly" value="<#if beginTimeStr?exists>${beginTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start">
                        </div>
                    </div>
                    <div class="col-sm-3" style="width: 20%">
                        <div class="form-group">
                            <label>订单类型:</label>
                            <select id="waybillType" name="waybillType" style="width: 100px;">
                                <option value="-1" <#if !waybillTypeView?exists || waybillTypeView == -1>selected="selected"</#if> >全部</option>
                                <option value="0" <#if waybillTypeView?exists && waybillTypeView == 0>selected="selected"</#if> >货到付款</option>
                                <option value="1" <#if waybillTypeView?exists && waybillTypeView == 1>selected="selected"</#if> >在线支付</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3" style="width: 22%">
                        <div class="form-group">
                            <label>配送员:</label>
                            <select id="riderId" name="riderId" style="width: 150px;">
                                <option value="-1">全部</option>
                                <#list riderList as rider>
                                    <option value="${rider.id !''}" <#if rider.id?exists && rider.id == riderIdView > selected="selected" </#if> >${rider.name !''}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3" style="width: 22%">
                        <div class="form-group">
                            <label>订单ID:</label>
                            <input id="orgId" type="hidden" name="orgId" value="${orgId!""}">
                            <input type="text" name="waybillId" value="${waybillIdView}" maxlength="20" class="form-control input-sm js_order_id">
                        </div>
                    </div>
                    <div style="float:right;padding-right: 15px">
                        <button type="button" class="btn btn-success js_submitbtn" actionurl="/partner/settle/detailZb" style="width:90px">查询</button>
                    </div>
                </div>
                <div class="row" style="margin-top: 5px">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>结束时间:</label>
                            <input type="text" id="end" name="endTimeStr" style="cursor: pointer;" readonly="readonly"  value="<#if endTimeStr?exists>${endTimeStr}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>商家类型:</label>
                            <select id="poiType" name="poiType" style="width: 100px;height: 25px;">
                                <option value="-1" <#if !poiTypeView?exists || poiTypeView == -1>selected="selected"</#if> >全部</option>
                                <option value="2" <#if poiTypeView?exists && poiTypeView == 2 >selected="selected"</#if> >代购</option>
                                <option value="1" <#if poiTypeView?exists && poiTypeView == 1 >selected="selected"</#if> >普通</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-bottom: 10px; padding-right:20px">
            <div class="col-md-4"><h5><i class="glyphicon glyphicon-bookmark opration-icon"></i><#if orgName??>${orgName}</#if> 站点对账详情</h5></div>
            <div style="float: right;padding-right: 10px"><button type="button" class="btn btn-success js_submitbtn" style="float: right;width:90px" actionurl="/partner/settle/detailExcelZb">导出EXCEL</button></div>
        </div>
    </form>

    <div class="panel panel-default table-responsive" style="overflow-x: auto">
        <table class="table table-striped" id="responsiveTable">
            <thead>
                <tr>
                    <th>订单编号</th>
                    <th>订单完成时间</th>
                    <th>配送员</th>
                    <th>配送员ID</th>
                    <th>商家名称</th>
                    <th>商家ID</th>
                    <th>运费</th>
                    <th>补贴</th>
                    <th>应收实收差额</th>
                    <th>应付实付差额</th>
                    <th>审核结果</th>
                    <th>配送员类型</th>
                    <th>商家类型</th>
                    <th>订单类型</th>
                    <th>订单金额</th>
                    <th>菜品原价</th>
                    <th>配送费</th>
                    <th>垫付餐费</th>
                    <th>取餐应付</th>
                    <th>取餐实付</th>
                    <th>送餐应收</th>
                    <th>送餐实收</th>
                </tr>
            </thead>
            <tbody>
                <#--循环-->
              <#if list ??>
                <#list list as view>
                <tr>
                    <td>${view.orderViewId!''}</td>
                    <td>${view.arriveTime!''}</td>
                    <td>${view.rider_name!''}</td>
                    <td>${view.riderId}</td>
                    <td>${view.poiName!''}</td>
                    <td>${view.poiId!''}</td>
                    <td>${view.deliveryFee}</td>
                    <td>${view.subsidy}</td>
                    <td>${view.plan_actual_charge}</td>
                    <td>${view.plan_actual_pay}</td>
                    <td>${view.auditStatus!''}</td>
                    <#--<td>${view.auditComment}</td>-->
                    <td><#if view.jobType == 1 >全职</#if><#if view.jobType == 2 >兼职</#if></td>
                    <td><#if view.poiType == 1 >普通</#if><#if view.poiType == 2 >代购</#if></td>
                    <td><#if view.isPayed == 0 >货到付款</#if><#if view.isPayed == 1 >在线支付</#if></td>
                    <td>${view.pkgValue}</td>
                    <td>${view.pkgValue2}</td>
                    <td>${view.postage}</td>
                    <td>${view.dianfuFee}</td>
                    <td>${view.plan_pay_amount}</td>
                    <td>${view.actual_pay_amount}</td>
                    <td>${view.actual_charge_amount}</td>
                    <td>${view.plan_charge_amount}</td>
                </tr>
                </#list>
              </#if>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/partner/settle/detailZb"/>
    </#if>
    </div>
</div>
<script>
    document.title = '站点订单详情';
    $('select').select2();
</script>