<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/pushMsg/push_msg.js</content>
<content tag="pageName">push_msg</content>

<div id="main-container">
    <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2" style="width: 150px;">
                        <div class="form-group">
                            <label class="control-label">组织ID</label>
                            <input type="text" class="form-control input-sm parsley-validated" id="orgId" name="orgId" style="width:100px;">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">消息标题</label>
                            <input type="text" class="form-control input-sm parsley-validated" id="title" value="">
                        </div>
                    </div>
                </div>
                <button id = "send" type="submit" class="btn btn-success" style="float: left;">发送消息</button>
            </div>
    </div>
</div>