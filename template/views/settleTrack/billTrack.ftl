<#include "settle_track_config.ftl">
<#include "../widgets/sidebar.ftl">

<div id="main-container">
    <div class="panel panel-default table-responsive">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post"
              action="/settleTrack/bill">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">账单ID</label>
                            <input type="text" class="form-control input-sm parsley-validated"
                                   name="billId" value="${billId!''}" data-required="true" data-minlength="8">
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-left">
                <button type="submit" class="btn btn-success">查询</button>
            </div>
        </form>
    	<table class="table table-striped" id="" >
            <thead>
            <tr>
                <th>时间</th>
                <th>日志</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list vos as vo>
                <tr>
                    <th>${vo.time!""}</th>
                    <th>${vo.comment!""}</th>
                </tr>
            </#list>
            </tbody>
        </table>
        
    </div>
</div>
<script>document.title = '运单结算回放';</script>
