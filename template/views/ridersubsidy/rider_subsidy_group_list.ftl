<#include "config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/ridersubsidy/rider_subsidy_group_list.js</content>

</style>

<div id="main-container">
    <div class="panel panel-default">
        <div id="alert_error"></div>
        <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                            <span style="font-size:25px;">配送员补贴</span>
                            <button id="addGroup" type="submit" style="margin-top:-12px" rel="add" class="btn btn-default">添加组</button>
                        
                        
                    </div>
               </div>
        </div>
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post" action="/ridersubsidy/grouplist">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                       
                            <label class="control-label">组名称：</label>
                            <input type="text" class=" input-sm parsley-validated" name="groupName" id="groupName" data-minlength="8" value=""/>
                            <button id="search" type="submit" class="btn btn-success">查询</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th></th>
                <th>组名</th>
                <th>美团每单补贴金额</th>
                <th>其他平台每单补贴金额</th>
                <th>APP显示文案</th>
                <th>补贴开始时间</th>
                <th>补贴结束时间</th>
                <th>活动状态</th>
                <th>组织类型</th>
                <th>操作人</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
                <!-- <tr>
                    <td></td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                    <td>
                        <button class="btn btn-xs btn-info manageSubsidy" data-toggle="dropdown" rel="1">管理</button>  
                    </td>
                </tr> -->
            <#--循环-->
            <#if riderSubsidyGroup??>
                <#list riderSubsidyGroup as subsidy>
                <tr>
                    <th></th>
                    <td>${subsidy.name!''}</td>
                    <td>${subsidy.mtSubsidy!''}</td>
                    <td>${subsidy.otherSubsidy!''}</td>
                    <td>${subsidy.subsidyDesc!''}</td>
                    <td>${subsidy.beginTime!''}</td>
                    <td>${subsidy.endTime!''}</td>
                    <td>${subsidy.statusDesc!''}</td>
                    <td>${subsidy.bmOrgDesc!''}补贴</td>
                    <td>${subsidy.createUserName!''}</td>
                    <td>
                        <a data-toggle="tooltip" title="管理" data-placement="top" class="manageSubsidy" rel="${subsidy.id!''}" style="cursor:pointer">
                            <i class="fa fa-cog fa-lg opration-icon"></i>
                        </a>  
                    </td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/ridersubsidy/grouplist"/>
    </#if>
    </div>
</div>
    <div class="modal fade" id="addGroupModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div style="color: red"><h4 class='text-center text-danger'>选择补贴针对的组织类型</h4></div>
                    <p>
                        <a class="btn btn-success btn-block" href="/ridersubsidy/groupdetail?orgType=4">众包活动补贴</a>     
                    </p>
                    <p>
                        <a class="btn btn-success btn-block" href="/ridersubsidy/groupdetail?orgType=5">角马活动补贴</a>
                    </p>
                </div>
            </div>

        </div>
    </div>
<script>document.title = '配送员补贴';</script>

