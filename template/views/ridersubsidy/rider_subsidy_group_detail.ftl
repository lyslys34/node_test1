<#include "config.ftl">
<#include "../widgets/sidebar.ftl">
<link rel="stylesheet" type="text/css" href="/static/css/lib/bootstrap-datetimepicker.min.css" />
<content tag="javascript">/ridersubsidy/rider_subsidy_group_detail.js</content>
<style type="text/css">
.datetimepicker table tr td span.old {
    color: #777777;
}
.datetimepicker table tr td span.disabled, .datetimepicker table tr td span.disabled:hover {
    color: #999;
}
</style>

<div id="main-container">
    <div class="panel panel-default">
        <div id="alert_error"></div>
        <input id="id" type="hidden" name="id" value="<#if groupVO??>${groupVO.groupId!''}</#if>">
        <input id="org_type" type="hidden" name="org_type" value="${orgType!''}">
        <input id="status" type="hidden" name="status" value="<#if groupVO??>${groupVO.status!''}</#if>">
        <input id="status_desc" type="hidden" name="status_desc" value="<#if groupVO??>${groupVO.statusDesc!''}</#if>">
        <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                            <span style="font-size:25px;"><#if orgType??><#if orgType=4>众包<#elseif orgType=5>角马</#if></#if>活动补贴</span>
                            <button id="pauseGroup" type="submit" style="margin-top:-12px" class="btn btn-warning">暂停活动</button>
                            <button id="revokeGroup" type="submit" style="margin-top:-12px" class="btn btn-warning">撤销活动</button>
                            <button id="restartGroup" type="submit" style="margin-top:-12px" class="btn btn-warning">重新开始</button>
                        
                    </div>
               </div>
        </div>
    </div>

    <div class="panel panel-default table-responsive">
        <div class="form-horizontal">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">活动主题与文案</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="inputGroupName" class="col-sm-2 control-label">组名</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="group_name" placeholder="请填写组名" value="<#if groupVO??>${groupVO.groupName!''}</#if>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputGroupDesc" class="col-sm-2 control-label">APP显示文案</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="group_desc" placeholder="请填写活动显示文案" value="<#if groupVO??>${groupVO.groupDesc!''}</#if>">
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">设置可获取补贴的运单条件</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="inputStartTime" class="col-sm-2 control-label">活动活动开始时间</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control form_datetime" id="group_start_time" readonly placeholder="请选择开始时间" value="<#if groupVO??>${groupVO.beginTime!''}</#if>" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:00',minDate:'%y-%M-%d %H:%m:%s',maxDate:'2035-12-31 23:00:00'})">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEndTime" class="col-sm-2 control-label">活动结束时间</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control form_datetime" id="group_end_time" readonly placeholder="请选择结束时间" value="<#if groupVO??>${groupVO.endTime!''}</#if>" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:00',minDate:'%y-%M-%d %H:%m:%s',maxDate:'2035-12-31 23:00:00'})">
                            <p style="color:red">可设置的最大时间为：2035-12-31 23:00</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputGroupDesc" class="col-sm-2 control-label">运单对应订单类</label>
                        <div class="col-sm-8">
                            <label class="checkbox-inline">
                                <input type="checkbox" <#if !groupVO??>checked</#if> style="opacity:1;margin-top:2px" class="form-control group_order_type" id="group_st_order_type" value="10" <#if groupVO??><#if groupVO.mtType??><#if groupVO.mtType=1>checked</#if></#if></#if>>美团平台订单
                            </label>
                            <#if orgType??><#if orgType=5>
                            <label class="checkbox-inline">
                                <input type="checkbox"  style="opacity:1;margin-top:2px" class="form-control group_order_type" id="group_other_order_type" value="20" <#if groupVO??><#if groupVO.otherType??><#if groupVO.otherType=1>checked</#if></#if></#if>>其他平台订单
                            </label>
                            </#if></#if>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputGroupDesc" class="col-sm-2 control-label">参与活动运单范围</label>
                        <div class="col-sm-8">
                            <label class="radio-inline">
                                <input type="radio" checked name="group_type" style="opacity:1;margin-top:2px" class="form-control" id="group_st_order" value="1" <#if groupVO??><#if groupVO.groupDimension??><#if groupVO.groupDimension=1>checked</#if></#if></#if>>按城市维度
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="group_type" style="opacity:1;margin-top:2px" class="form-control" id="group_other_order" value="3" <#if groupVO??><#if groupVO.groupDimension??><#if groupVO.groupDimension=3>checked</#if></#if></#if>>按商家维度
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="btnAddDimension" class="col-sm-2 control-label">添加参与活动的运单范围</label>
                        <div class="col-sm-4">
                            <button type="button" id="addDimension" class="btn btn-sm btn-default">添加<#if groupVO??><#if groupVO.groupDimension??><#if groupVO.groupDimension=1>城市<#elseif groupVO.groupDimension=3>商家<#else>城市</#if></#if></#if></button>
                            
                        </div>
                    </div>
                    <div id="show_add_ids" hidden class="form-group">
                        <label for="btnAddDimension" class="col-sm-2 control-label">已添加id</label>
                        <div class="col-sm-4">
                            <textarea id="type_ids" readonly class="form-control "></textarea>
                        </div>
                    </div>

                    <div>
                        <#--循环-->
                        <#if riderSubsidyGroupPoi??>
                            <button type="button" id="deleteDimension" class="btn btn-sm btn-danger">删除<#if groupVO??><#if groupVO.groupDimension??><#if groupVO.groupDimension=1>城市<#elseif groupVO.groupDimension=3>商家<#else>城市</#if></#if></#if></button>
                                <table class="table table-striped" id="responsiveTable">
                                    <thead>
                                        <tr>
                                            <th>
                                                <input class="selectAll" type="checkbox" style="opacity:1;position:relative">
                                            </th>
                                            <#if groupVO??><#if groupVO.groupDimension??><#if groupVO.groupDimension=1>
                                            <th>城市名</th>
                                            <th>城市ID</th>
                                            <#elseif groupVO.groupDimension=3>
                                            <th>商家名</th>
                                            <th>商家ID</th>
                                            <#else>
                                            <th>未知信息</th>
                                            <th>未知信息</th>
                                            </#if></#if></#if>
                                            <th>插入时间</th>
                                            <th>操作人</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <#list riderSubsidyGroupPoi as subsidypoi>
                                        <tr>
                                            <td><input class="selectItem" type="checkbox" style="opacity:1;position:relative" value="${subsidypoi.dimensionId!''}"></td>
                                            <td>${subsidypoi.name!''}</td>
                                            <td>${subsidypoi.dimensionId!''}</td>
                                            <td>${(subsidypoi.ctime*1000)?number?number_to_datetime!''}</td>
                                            <td>${subsidypoi.createUserName!''}</td>
                                        </tr>
                                       </#list>
                                    </tbody>
                                </table>
                                <div>
                                    <#import "../page/pager.ftl" as q>
                                    <#if recordCount??>
                                        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/ridersubsidy/groupdetail"/>
                                    </#if>
                                </div>
                            
                        </#if>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">设置补贴规则</h3>
                </div>
                <div class="panel-body">
                    <div id="group_st_order_dimension" class="panel panel-default">
                        <div class="panel-heading">
                            <h5 class="panel-title">美团平台订单补贴规则</h5>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="inputGroupName" class="col-sm-3 control-label">若此订单不属于订单包，每单补贴金额</label>
                                <div class="col-sm-2 input-group">
                                    <input type="text" class="form-control input_number" id="mt_money" placeholder="" value="<#if groupVO??>${groupVO.mtMoney!''}</#if>">
                                    <div class="input-group-addon">元/单</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputGroupName" class="col-sm-3 control-label">若此订单属于订单包，每单补贴金额</label>
                                <div class="col-sm-2 input-group">
                                    <input type="text" class="form-control input_number" id="orderPackageMoney" placeholder="" value="<#if groupVO??>${groupVO.orderPackageMoney!''}</#if>">
                                    <div class="input-group-addon">元/单</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputGroupName" class="col-sm-3 control-label">每人每天补贴单数上限</label>
                                <div class="col-sm-2 input-group">
                                    <input type="text" class="form-control input_number" id="mt_order_ceil" placeholder="" value="<#if groupVO??>${groupVO.mtOrderCeil!''}</#if>">
                                    <div class="input-group-addon">单</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <#if orgType??><#if orgType=5>
                    <div id="group_other_order_dimension" class="panel panel-default">
                        <div class="panel-heading">
                            <h5 class="panel-title">非美团平台订单补贴规则</h5>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="inputGroupName" class="col-sm-3 control-label">每单补贴金额</label>
                                <div class="col-sm-2 input-group">
                                    <input type="text" class="form-control input_number" id="other_money" placeholder="" value="<#if groupVO??>${groupVO.otherMoney!''}</#if>">
                                    <div class="input-group-addon">元/单</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputGroupName" class="col-sm-3 control-label">每人每天补贴单数上限</label>
                                <div class="col-sm-2 input-group">
                                    <input type="text" class="form-control input_number" id="other_order_ceil" placeholder="" value="<#if groupVO??>${groupVO.otherOrderCeil!''}</#if>">
                                    <div class="input-group-addon">单</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </#if></#if>
                </div>
            </div>
        </div>
        <div style="text-align: center">
            <button id="addInfoBtn" type="button" class="btn btn-lg btn-warning">提交</button>
        </div>
    </div>

    <div class="modal fade" id="addDimensionModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 id="addModalTitle" class="text-center">添加参与活动运单所属城市</h4>
                    <div id="alert_add_dimension_error"></div>
                    <div class="form-group">
                        <textarea id="dimension_ids" style="height:200px" class="form-control " placeholder="请输入id，用英文逗号分割"></textarea>
                    </div><br>
                     <div class="modal-footer">
                        <input type="button" id="addDimensionSubmit" value="确定" class="btn btn-success btn-block" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="changeGroupStatusModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="alert_status_massage"></div>
                    <input type="hidden" id="old_status">
                    <input type="hidden" id="new_status">
                    <div style="color: red"></div>
                    <div class="text-center" >
                        <button type="button" class="btn btn-success" id="B_cancel_change">取消</button>
                        <button type="button" class="btn btn-success" id="B_continue_change">确定</button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="saveGroupModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">

                    <div style="color: red"><h4 class='text-center text-dange'>确定提交活动信息？</h4></div>
                    <div class="text-center" >
                        <button type="button" class="btn btn-success" id="B_cancel_save">取消</button>
                        <button type="button" class="btn btn-success" id="B_continue_save">确定</button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="showSuccessMsg">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="resMsg">
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script>document.title = '配送员补贴';</script>
<script type="text/javascript" src="/static/js/lib/My97DatePicker/WdatePicker.js"></script>

