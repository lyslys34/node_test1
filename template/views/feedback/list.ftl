<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<#setting datetime_format="yyyy-MM-dd HH:mm:ss"/>
<#setting date_format="yyyy-MM-dd"/>

<content tag="javascript">/feedback/list.js</content>
<content tag="cssmain">/feedback/list.css</content>
    
<div id="main-container">
    <form class="header-form" id="fm" method="get" action="/feedback/list">
        <div class="row">
            <div class="col-md-3">
            <div class="form-group">
                <label for="beginTime">起始时间</label>
                <input type="text" name="beginTime" style="cursor: pointer;" readonly="readonly" value="<#if (beginTime>0)>${(beginTime *1000)?number?number_to_date}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start"/>
            </div>
            </div>
            <div class="col-md-3">
            <div class="form-group">
                <label for="endTime">结束时间</label>
                <input type="text" name="endTime" style="cursor: pointer;" readonly="readonly" value="<#if (endTime>0)>${(endTime *1000)?number?number_to_date}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start"/>
            </div>
            </div>
            <div class="col-md-3">
            <div class="form-group">
                <label for="org">组织类型</label>
                <select id="orgType" name="orgType" class="form-control input-sm">
                    <option value="-1">全部</option>
                    <#if orgTypes?exists>
                    <#list orgTypes as orgType>
                        <option <#if RequestParameters?? && RequestParameters.orgType?? && RequestParameters.orgType?string == orgType.type?string>selected="selected" </#if>value=${orgType.type}>
                            ${orgType.comment}
                        </option>
                    </#list>
                    </#if>
                </select>
            </div>
            </div>
            <div class="col-md-3">
            <div class="form-group">
                <label for="org">组织</label>
                <select id="org" name="org" class="form-control input-sm">
                    <option <#if !org?? || (org == -1)>selected="selected" </#if> value="-1">全部</option>
                    <#if orgs?exists>
                    <#list orgs?keys as key>
                        <option <#if org ?exists && org?string == key>selected="selected" </#if>value=${key}>
                            ${orgs[key]}
                        </option>
                    </#list>
                    </#if>
                </select>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
            <div class="form-group">
                <label for="type">反馈类型</label>
                <select id="type" name="type" class="form-control input-sm">
                    <option <#if !type?? || type==-1 >selected="selected"</#if> value="-1">全部</option>
                    <option <#if type ?exists && type == 0 >selected="selected" </#if> value="0">其他</option>
                    <option <#if type ?exists && type == 1 >selected="selected" </#if> value="1">投诉</option>
                    <option <#if type ?exists && type == 2 >selected="selected" </#if> value="2">建议</option>
                    <option <#if type ?exists && type == 3 >selected="selected" </#if> value="3">求助</option>
                </select>
            </div>
            </div>
            <div class="col-md-3">
            <div class="form-group">
                <label for="questionType">问题分类</label>
                <select id="questionType" name="questionType" class="form-control input-sm">
                    <option <#if !questionType?? || questionType==-1 >selected="selected"</#if> value="-1">全部</option>
                    <#if questionTypes?exists>
                    <#list questionTypes?keys?sort?reverse as key>
                        <option <#if questionType ?exists && questionType?string == key >selected="selected" </#if> value=${key}>
                            ${questionTypes[key]}
                        </option>
                    </#list>
                    </#if>
                </select>
            </div>
            </div>
            <div class="col-md-3">
            <div class="form-group">
                <label for="role">角色</label>
                <select id="role" name="roleId" class="form-control input-sm">
                    <option <#if !roleId?? || (roleId == -1)>selected="selected" </#if> value="-1">全部</option>
                    <#if roleList?exists >
                        <#list roleList as r >
                            <option <#if roleId ?exists && roleId == r.id>selected="selected" </#if> value="${r.id !'-1'}" code="${r.code !'-1'}">${r.name !''}</option>
                        </#list>
                    </#if>
                </select>
            </div>
            </div>
            <div class="col-md-3">
            <div class="form-group">
                <label for="replied">是否回复</label>
                <select id="repied" name="replied" class="form-control input-sm">
                    <option <#if !replied?? || (replied == -1)>selected="selected" </#if> value="-1">全部</option>
                    <option <#if replied?? && (replied == 0)>selected="selected" </#if> value="0">未回复</option>
                    <option <#if replied?? && (replied == 1)>selected="selected" </#if> value="1">已回复</option>
                </select>
            </div>
            </div>
            <div class="col-md-3 hide col-md-offset-3" id="complainPoi">
            <div class="form-group">
                <select id="subQuestionCode" name="subQuestionCode" class="form-control input-sm" style="width:48%;display:inline;">
                    <option value="-1">投诉内容</option>
                    <#if subQuestionTypes?exists>
                    <#list subQuestionTypes?keys as key>
                        <option <#if RequestParameters?? && RequestParameters.subQuestionCode?? && RequestParameters.subQuestionCode?string == key>selected="selected"</#if> value=${key}>
                            ${subQuestionTypes[key]}
                        </option>
                    </#list>
                    </#if>
                </select>
                <input type="text" name="poiId" value="<#if RequestParameters??>${RequestParameters.poiId!''}</#if>" id="poiId" class="input-sm" style="width:45%;float:right;" placeholder="商家ID" />
            </div>
            </div>
            <div class="col-md-3 hide " id="osType">
            <div class="form-group">
                <select id="osType" name="osType" class="form-control input-sm">
                    <option value="-1">手机系统</option>
                    <option value="1" <#if RequestParameters?? && RequestParameters.osType?? && RequestParameters.osType=="1">selected</#if>>Android</option>
                    <option value="2" <#if RequestParameters?? && RequestParameters.osType?? && RequestParameters.osType=="2">selected</#if>>iOS</option>
                </select>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-md-offset-10">
            <div class="form-group searchItem">
                <input type="submit" value="查 询" class="btn btn-success js_submitbtn" id="search" style="width:80px"/>
                <input type="button" value="导 出" class="btn btn-success js_submitbtn" id="export" style="width:80px"/>
            </div>
            </div>
        </div>
    </form>

    <hr/>
    <#if (page)?exists && (page.feedbackList)?exists && (page.feedbackList?size > 0) >
    <table class="table table-bordered table-striped" style="background-color:#fff">
        <thead>
            <tr style="">
                <th width="35%">反馈信息</th>
                <th width="20%">反馈人</th>
                <th width="35%">回复信息</th>
                <th width="10%">操作</th>
            </tr>
        </thead>
        <tbody>
            <#list page.feedbackList as fb>
            <tr>
                <td>
                    <p style="color:red;">反馈内容：<#if fb.complaintPoiId gt 0>[${fb.complaintPoiId}]
                    <#if fb.bmFeedbackTagViews??>
                        <#list fb.bmFeedbackTagViews as tag>
                            [${subQuestionTypes[tag.code?string]!''}]
                        </#list>
                    </#if><br></#if>${fb.content !''}</p>
                    <p>反馈来源：${fb.appVersion!''}<#if fb.osType == 1>(Android)</#if><#if fb.osType == 2>(iOS)</#if>(${fb.deviceType!''})</p>
                    <p>反馈类型：
                        <#if fb.type == 0 >其他</#if>
                        <#if fb.type == 2 >建议</#if>
                        <#if fb.type == 1 >投诉</#if>
                        <#if fb.type == 3 >求助</#if>
                    </p>
                    <p>问题类型：
                       ${questionTypes[fb.questionTypeCode?string]!''}
                    </p>
                </td>
                <td>
                    <p>反馈人：${fb.createUserName !''}(${fb.createUserPhone !''})</p>
                    <p>组织：${fb.createUserOrgName !''}
                        <#if dispatchStrategyTypes??>
                            <#list dispatchStrategyTypes as dispatchStrategyType>
                                <#if dispatchStrategyType.code == fb.createUserOrgDispatchStrategy>
                                    [${dispatchStrategyType.type!''}]
                                </#if>
                            </#list>
                        </#if>
                    </p>
                    <#--<p>角色：
                    <#if fb.createUserRole == 0>配送员<#else>
                    <#if roleList?exists >
                        <#list roleList as r >
                            <#if fb.createUserRole == r.id>${r.name !''}</#if>
                        </#list>
                    </#if>
                    </#if>
                    </p> -->
                    <p>组织类型：
                        <#if orgTypes?exists>
                            <#list orgTypes as orgType>
                                <#if fb.createUserOrgType == orgType.type>${orgType.comment!''}</#if>
                            </#list>
                        </#if>
                    </p>
                    <p>反馈时间：${(fb.ctime *1000)?number?number_to_datetime}</p>
                </td>
                <td>
                    <#if fb.bmReplyToFeedbackView?exists>
                    <p style="color:red;">回复内容：${fb.bmReplyToFeedbackView.content}</p>
                    <p>备注：${fb.bmReplyToFeedbackView.remark}</p>
                    <p>回复人：${fb.bmReplyToFeedbackView.replyerName}</p>
                    <p>回复时间：${(fb.bmReplyToFeedbackView.ctime*1000)?number?number_to_datetime}</p>
                    </#if>
                </td>
                <td style="vertical-align:middle;text-align:center;">
                    <#if fb.replied==1><span class="label label-disactive">已回复</span><#else><a class="btn btn-success" name="reply" id=${fb.id} feedbackerId=${fb.createUserId}>回复</a></#if>
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
    <#if (page)?exists && (page.total)?exists && (page.pageSize)?exists && (page.pageNum)?exists && page.total gt 0 && page.pageSize gt 0 && page.pageNum gt 0  >
      <#if page.total??>
        <#import "/page/pager.ftl" as q>
        <@q.pager pageNo=page.pageNum pageSize=page.pageSize recordCount=page.total toURL="/feedback/list"/>
      </#if>
    </#if>
   </#if>
   <#if !((page)?exists && (page.feedbackList)?exists && (page.feedbackList?size > 0)) >
    <div style="clear:both;">暂无数据！</div>
   </#if>

   <div id="replyPanel" class="hide">
       <form class="form-horizontal" action="/feedback/create_reply" method="post">
       <input type="hidden" name="feedbackId" id="feedbackId" >
       <input type="hidden" name="feedbackerId" id="feedbackerId">
       <input type="hidden" name="returnUrl" id="returnUrl" />
         <div class="form-group">
           <label for="content" class="col-sm-2 control-label">回复</label>
           <div class="col-sm-10">
             <textarea class="form-control" id="content" name="content" rows="3"></textarea>
           </div>
         </div>
         <div class="form-group">
            <label for="remark" class="col-sm-2 control-label">备注</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="remark" name="remark" />
            </div>
         </div>
       <form>
   </div>
</div>
<script>
    $("#org").select2();
    $("#replier").select2();
</script>
