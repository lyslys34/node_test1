<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<#setting datetime_format="yyyy-MM-dd HH:mm:ss"/>

<content tag="cssmain">/feedback/addAndList.css</content>
<content tag="javascript">/feedback/addAndList.js</content>

<div id="main-container">
    <div class="top-panel">您好，欢迎您给我们的配送后台提供使用意见和建议！其他咨询和页面使用问题请联系您的渠道经理直接沟通处理，谢谢</div>
    <form id="fm" method="post" action="/feedback/create" class="form-horizontal">
        <div class="form-group">
            <label for="type" class="col-sm-2 control-label" required="true">反馈类型</label>
            <div class="col-sm-10" id="type">
                <label class="radio-inline">
                  <input type="radio" name="type" value=2> 建议
                </label>
                <label class="radio-inline">
                  <input type="radio" name="type" value=1> 投诉
                </label>
                <label class="radio-inline">
                  <input type="radio" name="type" value=0> 其他
                </label>
            </div>
        </div>

        <div class="form-group">
            <label for="questionType" class="col-sm-2 control-label" required="true">问题分类</label>
            <div class="col-sm-10">
                <select id="questionType" name="questionType" class="form-control input-sm inline" style="width:160px">
                    <option value=-1>请选择问题分类</option>
                    <#if questionTypes?exists>
                    <#list questionTypes?keys?sort?reverse as key>
                        <option value=${key} <#if key=="100">class="complaints"</#if> <#if RequestParameters.questionType ?exists && RequestParameters.questionType?string == key >selected="selected" </#if>>
                            ${questionTypes[key]}
                        </option>
                    </#list>
                    </#if>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="content" class="col-sm-2 control-label" required="true">内容</label>
            <div class="col-sm-10">
                <textarea name="content" id="content" class="form-control" rows="7" style="width:500px;" placeholder="感谢您给我们提出建议。您的感受和建议一旦在此发表，即表示您同意我们可无偿使用您的建议和感受来优化我们的产品和服务。

关于产品使用本身的问题或者咨询，请联系您的业务人员/渠道经理直接反馈，此页面仅收集意见和建议，谢谢"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-success">提交</button>
            </div>
        </div>
    </form>
    反馈记录
    <#if (page)?exists && (page.feedbackList)?exists && (page.feedbackList?size > 0) >
    <table class="table" style="border-top: solid 1px;">
        <!--<thead>
            <tr style="">
                <th width="10%"></th>
                <th width="10%"></th>
                <th width="25%"></th>
                <th width="25%"></th>
            </tr>
        </thead>-->
        <tbody>
            <#list page.feedbackList as fb>
            <tr>
                <td max-width="20%">反馈时间：${(fb.ctime *1000)?number?number_to_datetime}</td>
                <td width="20%">类型：
                    <#if fb.type == 0 ><span class="label badge label-disactive">其他</span></#if>
                    <#if fb.type == 2 ><span class="label badge label-success">建议</span></#if>
                    <#if fb.type == 1 ><span class="label badge label-warning">投诉</span></#if>
                </td>
                <td>问题分类：${fb.questionTypeName !''}</td>
            </tr>
            <tr>
                <td colspan=5>反馈内容：${fb.content !''}</td>
            </tr>
            <tr style="border-bottom:1px solid #ccc">
                <td colspan=5 style="color:red">
                <#if fb.bmReplyToFeedbackView?exists>
                回复：${fb.bmReplyToFeedbackView.content !''}
                </#if>
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
    <#if (page)?exists && (page.total)?exists && (page.pageSize)?exists && (page.pageNum)?exists && page.total gt 0 && page.pageSize gt 0 && page.pageNum gt 0  >
      <#if page.total??>
        <#import "/page/pager.ftl" as q>
        <@q.pager pageNo=page.pageNum pageSize=page.pageSize recordCount=page.total toURL="/feedback/addAndList"/>
      </#if>
    </#if>
   </#if>
   <#if !((page)?exists && (page.feedbackList)?exists && (page.feedbackList?size > 0)) >
    暂无数据！
   </#if>
</div>