<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">

<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=820afe3932ebea15d73cd05b2a28e606"></script>
<content tag="pageName">骑手轨迹</content>
<content tag="javascript">/task_manage/riderTrack.js</content>
<content tag="cssmain">/task_manage/riderTrack.css</content>

<div id="background" class="divbackground" style="display: none; "></div>
<div id="progressBar" class="progressBar" style="display: none; ">数据加载中，请稍候...</div>


<div id="main-container">
	<div class="condition_box">
		<div class="cond_item cond_item_short">
			<select id="orgSelect">
				<option value="-1">未指定站点</option>
				<#if (orgViewList??) && (orgViewList?size > 0) >
					<#list orgViewList as org>
						<option value="${org.baseData.id}">${org.baseData.name}</option>
					</#list>
				</#if>
			</select>
		</div>
		<div class="cond_item rider_input_item">
			<input type="text" id="riderInput" class="input-sm form-control" placeholder="请输入骑手手机号">
		</div>
		<div class="cond_item rider_select_item">
			<select id="riderSelect" placeholder="hehe">
				<option value="-1">未选择骑手</option>
			</select>
		</div>
		<div class="cond_item cond_item_short">
			<input type="text" id="datepickerInput" value="<#if ts?exists>${ts}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="input-sm date-picker J-datepicker" maxlength="10"  readonly="readonly"  style="width: 120px; cursor: pointer">
		</div>
		<div class="cond_item order_input_item">
			<input type="text" id="orderInput" class="input-sm form-control" placeholder="请输入订单号">
		</div>
		<div class="cond_item order_select_item">
			<select id="orderSelect">
				<option value="-1">请选择订单号</option>
			</select>
		</div>
		<button class="btn btn-default rider-btn" id="searchBtn">查询</button>
	</div>

	<!-- 滑动获取时间 -->
	<div class="time-chooce">
    	<input id="jSliderTime" type="slider" name="area" value="0;1440" />
    </div>

	<table class="table table-bordered table-rider-track">
		<tbody id="riderListBody"></tbody>
	</table>

	<div class="rider-track-map" id="mapContainer">
	    <div id="rider-track-map"></div>
    </div>

</div>
<script>
	// 声明一些dom变量，出于兼容性考虑。
	window.$orgSelect   = $('#orgSelect');
	window.$riderSelect = $('#riderSelect');
	window.$riderInput  = $('#riderInput');
	window.$orderSelect = $('#orderSelect');
	window.$orderInput  = $('#orderInput');
</script>