<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="pageName">newdispatch</content>
<content tag="javascript">/task_manage/newdispatch.js</content>
<content tag="cssmain">/task_manage/newdispatch.css</content>

<script type="text/javascript">

	var orgRoleCode = '2001';
	<#if orgRoleCode??>
    	orgRoleCode = "${orgRoleCode}";
	</#if>

	var orgRoleType = '1';
	<#if orgRoleType??>
    	orgRoleType = "${orgRoleType}";
	</#if>

	var allOrgList = {};
	<#if (orgViewList??) && (orgViewList?size > 0) >
    <#list orgViewList as orgView>

    	var org = {};
    	org.orgid = "${orgView.baseData.id}";
    	org.name = "${orgView.baseData.name}";
    	org.dispatchStrategyDes = "${orgView.baseData.dispatchStrategyDes}";
    	org.cityId = "${orgView.baseData.cityId}";
    	org.areaId = "${orgView.baseData.deliveryAreaId}";

    	allOrgList["${orgView.baseData.id}"] = org;
    </#list>
	</#if>

	var deliveryAreaList = {};
	<#if (DeliveryAreaList??) && (DeliveryAreaList?size > 0) >
    <#list DeliveryAreaList as deliveryArea>

    	var area = {};
    	area.id = "${deliveryArea.bmDeliveryAreaId}";
    	area.cityId = "${deliveryArea.bmCityId}";
		area.orgId = "${deliveryArea.bmOrgId}";
		area.name = '-';
		<#if deliveryArea.deliveryAreaName??>
			area.name = "${deliveryArea.deliveryAreaName}";
		</#if>
		deliveryAreaList[area.id] = area;
    </#list>
	</#if>


	var changeOrgList = {};
	<#if (changeOrgIds??) && (changeOrgIds?size > 0)>
		<#list changeOrgIds as changeOrg>
			var orgId={};
			orgId.orgId = "${changeOrg.orgId}";
			orgId.orgName = "${changeOrg.orgName}";
			orgId.changeTime  = "${changeOrg.changeTime}"
			changeOrgList["${changeOrg.orgId}"] = orgId;
		</#list>
	</#if>

	var selCity;
	var mySelect2;
	var selectStatus;
	$(document).ready(function(){
		selCity = $('#selectCity');
		mySelect2 = $.fn.select2;
		selectStatus = $("#selectStatus");
	});
</script>

<div id="main-container">
	<div class="tooltip-frame">
		<table class="table table-bordered" id="riderWaybillList">
            <thead>
	            <tr>
	            	<th>状态（已用时）</th>
	            	<th>时间</th>
	            	<th>订单路程</th>
	            </tr>
            </thead>
            <tbody id="riderWaybillListBody">
            </tbody>
		</table>
		<p>
			<button class="btn btn-default disabled hidden btn-left-8px" id="alreadyTheRider">订单已指派给此骑手</button>
			<#-- <button class="btn btn-default btn-left-12px" id="btn-assign">派单给此骑手</button> -->
			<span id="rider-mobile" class="rider-mobile-style blueSpan"></span>
			<#-- <span id="tip-riderinfo" style=" padding-top:17px" class="greySpan">状态：在岗</span> -->
            <span id="rider-outTime" class="btn-left-12px"></span>
            <a target="_blank" id="riderMap">
            	<#-- <span class="blueSpan btn-right-12px pull-right" >骑手位置</span> -->
                <span class="riderMap blueSpan btn-right-12px pull-right" style="padding: 0 2px;">骑手位置</span>
			</a>
		</p>
	</div>

	<div class="qa-datail" id="qaInfoDetail">
		<h5 style="font-size:12px;">各状态下“超时”定义规则：</h5>
		<ul>
			<li>新订单：剩余时间≤20分钟</li>
			<li>派单未确认：剩余时间≤20分钟</li>
			<li>已接单：剩余时间≤15分钟</li>
			<li>已取货：剩余时间≤10分钟</li>
			<li>剩余时间：期望时间-当前时间</li>
		</ul>
	</div>

	<div class="filter-data">
		<div class="filter-data-ul">
			<div class="filter-data-li filter-li-first">
				<img id="sound-icon" class="sound-icon-style paddTop5px" src="/static/imgs/sound-close.gif" />
			</div>
			<div class="filter-data-li" style="padding-left: 5px;"><!-- 城市 -->
					<select class="input-sm" id="selectCity">
						<#if (cityList??) && (cityList?size > 0) >
	                		<#list cityList as city>
								<#if city_index == 0 >
		            				<option selected="selected" value="${city.city_id}">${city.name}</option>
		            			<#else>
		            				<option value="${city.city_id}">${city.name}</option>
								</#if>
		            		</#list>
						</#if>
					</select>
			</div>
			<div class="filter-border-right"></div>
			<div class="filter-data-li"><!-- 商圈 -->
				<#if (orgRoleType??) && (orgRoleType == 1) >
					<select id="selectArea" class="input-sm" multiple="multiple" style="opacity:0">
				</select>
				<#else>
					<select id="selectArea" class="input-sm" style="opacity:0">
					</select>
				</#if>
			</div>
			<div class="filter-border-right"></div>
			<div class="filter-data-li li-border-right"><!-- 站点 -->
				<select id="selectOrg" class="input-sm" multiple="multiple"></select>
				<a href="/partner/dispatch/dispatchStrategy" target="_blank" style="display: block;float: right;margin-top: 6px;">
					<span data-toggle="tooltip" title="调度策略说明" data-placement="top">
						<#-- <i class="fa fa-lg  fa-exclamation-circle greySpan"></i> -->
						<img src="/static/imgs/msg-info.png" alt="说明" style="opacity: 0.5;" />
					</span>
				</a>
			</div>
			<div class="filter-data-li">
			<!-- 日期 -->
				<label for="selectDay" class="paddTop5px pull-left">
					<#-- <i class="fa fa-lg fa-calendar filter-color-icon"></i> -->
					<img src="/static/imgs/week-date-icon.gif" alt="日期" />
				</label>
				<select id="selectDay" class="input-sm hide">
		         	<#-- <option selected="selected" value="0">今天</option> -->
		        </select>
		        <div class="dropdown rider-input-select pull-left" style="margin-left: -8px;">
			  		<button class="btn btn-default dropdown-toggle" id="btnClickDate" type="button" data-toggle="dropdown" aria-expanded="false">
			    		今天<span class="caret btn-left-4px"></span>
			  		</button>
			  		<ul class="dropdown-menu self-dropmenu" aria-labelledby="dropdownMenu1" style="width:158px;margin-left:-36px;margin-top: 6px;"></ul>
				</div>
			</div>
			<div class="filter-border-right" style="margin-left:12px;"></div>
			<div class="filter-data-li filter-li-top15" id="showRiderOnMap" href="javascript:" ><!-- 骑手地图 -->
				<img src="/static/imgs/rider-map-icon.gif" alt="骑手地图" />
				<span>骑手地图</span>
			</div>
			<div class="filter-border-right"></div>
			<div class="filter-data-li pull-left" id="refreshIcon">
				<b><img src="/static/imgs/refresh.png" alt="刷新按钮" />&nbsp;<i class="filter-color-icon">刷新</i></b>
			</div>
		</div>
	</div>

	<div id="riderListDiv">
		<div id="riderListHead">
			<select id="selectStatus" class="input-sm rider-input-select hide">
				<option value="0">全部骑手</option>
				<option selected="selected" value="1">在岗</option>
                <option value="2">忙碌</option>
                <option value="3">离岗</option>
            </select>
            <div class="dropdown rider-input-select pull-left">
			  	<button class="btn btn-default dropdown-toggle inputBorder" id="btnClickStatus" type="button" data-toggle="dropdown" aria-expanded="false">
			    	在岗（2）<span class="caret btn-left-4px"></span>
			  	</button>
			  	<ul class="dropdown-menu self-dropmenu" aria-labelledby="dropdownMenu1" style="text-align: left;"></ul>
			</div>

            <i id="clearSearchRiderName" class="glyphicon glyphicon-remove-circle"></i>
			<input type="text" id="getSearchRiderName" placeholder="定位骑手" class="padding-left10px inputBorder" />
		</div>
		<div class="riderlist-table">
			<table class="table table-bordered table-striped-w" id="riderListTable">
	            <thead>
	            <tr>
	                <th style="width:76px;text-align: center;">骑手</th>
	                <th style="cursor: pointer;width:50px;padding-right:4px">
	                	送/总
	                	<img src="/static/imgs/fa-caret-i.png" alt="排序" />
	                </th>
	                <th style="cursor: pointer;width:50px;">
	                距离
	                	<img src="/static/imgs/fa-caret-i.png" alt="排序" />
	                </th>
	            </tr>
	            </thead>
				<tbody id="riderListBody">
	            </tbody>
			</table>
		</div>
	</div>

	<div id="wayBillListDiv" class="unselectable">
		<div >
			<input type="text" class="input-get-busy padding-left10px inputBorder" id="busyMessage" placeholder="输入商家/订单序号" />
            <i class="glyphicon glyphicon-remove-circle" id="clearBusyName" style="right: 20px;cursor: pointer;"></i>

			<input type="text" class="input-get-rider padding-left10px inputBorder" id="getRiderName" placeholder="输入骑手姓名" />
            <i class="glyphicon glyphicon-remove-circle" id="clearRiderName" style="right: 20px;cursor: pointer;"></i>

			<#-- <input type="checkbox" id="filterOutTime" class="out-time-check" /> -->
			<label for="filterOutTime" class="out-time-label qa-icon">
				超时订单<img src="/static/imgs/msg-info.png" alt="" style="margin-left: 4px;opacity: 0.5;margin-top: -3px;" alt="说明" />
				<#-- <i class="fa fa-lg  fa-exclamation-circle greySpan" style="margin-left: 4px;"></i> -->
			</label>

			<button id="btn-refresh" class="btn btn-default">搜索</button>
		</div>
		<ul class="nav nav-tabs" id="waybillStatue">

	      <#if (unfetchView??) && unfetchView == 1>
	      	<li value="10"><a href="#10">新订单<span class="hidden" id="assginedCount">(0)</span><br><span>(超时0)</span></a></li>
	      </#if>
	      <li value="-10"><a href="#-10">需手工派单<span class="hidden" id="manualCount">0</span><br><span>(超时0)</span></a></li>
	      <li class="active" value="15"><a href="#15">派单未确认<span class="hidden" id="notConfirm">(0)</span><br><span>(超时0)</span></a></li>
	      <li value="-5"><a href="#-5">待确认退单<span class="hidden" id="sendbackCount">(0)</span><br/><span>&nbsp;</span></a></li>
	      <li value="20"><a href="#20">已接单<span class="hidden" id="catchedCount">(0)</span><br><span>(超时0)</span></a></li>
	      <li value="30"><a href="#30">已取货<span class="hidden" id="onWayCount">(0)</span><br><span>(超时0)</span></a></li>

		  <li value="100"><a href="#100">未完成<span class="hidden" id="notComplete">(0)</span><br><span>(超时0)</span></a></li>
	      <li value="50"><a href="#50">已送达<span class="hidden" id="arrivedCount">(0)</span><br>&nbsp;</a></li>
	      <li value="99"><a href="#99">已取消 <span class="hidden" id="canceledCount">(0)</span><br>&nbsp;</a></li>
		  <li value="110"><a href="#110">全部<span class="hidden" id="allCount">(0)</span><br>&nbsp;</a></li>
	    </ul>
	    <div class="loading">加载中</div>

	    <div id="content" class="hide">
        <div id="waybilllist"></div>
        <div id="footer"></div>
    	</div>
	</div>

	<audio id="chatAudio">
		<source src="/static/imgs/newWaybill.mp3?ver=2" type="audio/mpeg">
		<source src="/static/imgs/newWaybill.ogg?ver=2" type="audio/ogg">
	</audio>
	<div id="loading_back" class="modal-backdrop hidden"></div>
	<div id="detail_loading" class="hidden unselectable">
	<div style="height:45%"></div>
		<div class="text-center">
			<i class="fa fa-refresh fa-spin fa-4x opration-icon"></i>
		</div>
	</div>
</div>
