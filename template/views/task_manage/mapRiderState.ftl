
<#include "../config.ftl">

<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=820afe3932ebea15d73cd05b2a28e606"></script>
<content tag="pageName">mapRiderState</content>
<content tag="javascript">/task_manage/mapRiderState.js</content>
<content tag="cssmain">/task_manage/mapRiderState.css</content>

<script type="text/javascript">
    document.title = "骑手地图";
	var allOrgList = {};
	<#if (orgViewList??) && (orgViewList?size > 0) >
    <#list orgViewList as orgView>

    	var org = {};
    	org.orgid = "${orgView.baseData.id}";
    	org.name = "${orgView.baseData.name}";
    	org.dispatchStrategyDes = "${orgView.baseData.dispatchStrategyDes}";
    	org.cityId = "${orgView.baseData.cityId}";

    	allOrgList["${orgView.baseData.id}"] = org;
    </#list>
	</#if>

	var selCity;
	var mySelect2;
	$(document).ready(function(){
		selCity = $('#selectCity');
		mySelect2 = $.fn.select2;
	});
</script>

<div id="main-container" style="margin:0 0;">
	<div id="map-topbar" style="white-space:nowrap;">
	
		<ul id="org-info">

			<select class="input-sm" id="selectCity">
			<!-- 城市 -->
				<#if (cityList??) && (cityList?size > 0) >
                <#list cityList as city>
					<#if city_index == 0 >
	                	<option selected="selected" value="${city.city_id}">${city.name}</option>
	                <#else>
	                	<option value="${city.city_id}">${city.name}</option>
					</#if>
	            </#list>
				</#if>
			</select>
			<select id="selectOrg" class="input-sm" multiple="multiple" style="max-width:150px;padding:0 0; max-height:30px;opacity:0">
            </select>

			<select id="selectRider" class="input-sm" multiple="multiple" style="max-width:100px; max-height:30px;padding:0 0; opacity:0">
		    </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<span class="map-icon-ul">
				<span>
					<img src="/static/imgs/rider-recommend1.png">
					<input type="checkbox" checked="checked" class="status-check" value="1" />
					&nbsp;在岗<span id="online-count"></span>人 
				</span>&nbsp;&nbsp;&nbsp;
				<span>
					<img src="/static/imgs/rider-notRecommend1.png">
					<input type="checkbox"  checked="checked" class="status-check" value="2" />
					&nbsp;忙碌<span id="busy-count"></span>人
				</span>&nbsp;&nbsp;&nbsp;
				<span id="offline-rider">
					<img src="/static/imgs/rider-noScore1.png">
					离岗<span id="offline-count"></span>人&nbsp;&nbsp;&nbsp;点击查看<div id="offlineDiv" style="background-color: #3a3a3a;color: #FFFFFF;"></div>
				</span>&nbsp;&nbsp;&nbsp;
				<span>
					<input type="checkbox" class="status-check-online" value="3" />
					&nbsp;只看掉线<span id="drop-count"></span>
				</span>&nbsp;&nbsp;&nbsp;
			</span>
	    	
			<button id="btn-refresh" class="btn btn-default" style="padding-top:4px; padding-bottom:4px;">刷新</button>&nbsp;&nbsp;&nbsp;&nbsp;
			<span style="color:white;font-size: 14px;padding: 10px 5px 0 0;color:#ffff00">掉线：超时5分钟未上报定位信息</span>
			
		</ul>
	</div>

	<div style="top:50px; position:absolute; min-height:800px;width:100%">
            
	    <div style="border:solid 1px; margin-top:0px; min-height:700px;" id="mapContainer">
	        
	    </div>
    </div>
	<div id="confirmWindow" class="confirm-window">
		<#-- 弹窗的 -->
	</div>
</div>