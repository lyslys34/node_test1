<#include "../config.ftl">
<script>
	document.title = "地图派单";

	var wayBill = {};//订单的信息
	<#if (bmWaybillView??) && (bmWaybillView?size > 0) >
		wayBill.id = "${bmWaybillView.id}";//订单号
		wayBill.senderName = "${bmWaybillView.senderName}";//取的(商家名)
		wayBill.senderLng = "${bmWaybillView.senderLng}";//取的(商家)经纬度
		wayBill.senderLat = "${bmWaybillView.senderLat}";
		wayBill.orgName = "${bmWaybillView.orgName}";//站点名
		wayBill.bmDeliveryAreaName = "${bmWaybillView.bmDeliveryAreaName}";//绑定区域
		wayBill.deliveredTime = "${bmWaybillView.deliveredTime}";//期望时间
		wayBill.recipientName = "${bmWaybillView.recipientAddress}";//商家名
		wayBill.recipientLng = "${bmWaybillView.recipientLng}";//送达经纬度
		wayBill.recipientLat = "${bmWaybillView.recipientLat}";
		wayBill.isPrebook = "${bmWaybillView.isPrebook}";//“预” 或者 “取“
		wayBill.pkgPrice = "${bmWaybillView.pkgPrice}";//订单价格
		wayBill.status = "${bmWaybillView.status}";//订单状态
		wayBill.riderId = "${bmWaybillView.riderId}";
		wayBill.poiSeq = "${bmWaybillView.poiSeq}";
	</#if>
</script>
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=820afe3932ebea15d73cd05b2a28e606"></script>
<content tag="pageName">mapDispatchWaybill</content>
<content tag="javascript">/task_manage/mapDispatchWaybill.js</content>
<content tag="cssmain">/task_manage/mapDispatchWaybill.css</content>
<div id="main-container">
	<div class="map-topbar">
		<div class="rider-busy-main">
			<input type="checkbox" name="riderBusy" id="riderBusy" checked="checked" />
			<label for="riderBusy">过滤"忙碌"骑手(<#if (busyCount??) >${busyCount}</#if>人)</label>
		</div>
		<div class="select-rider-num">
			<select name="riderNum" id="riderNum" class="input-sm rider-select">
				<#if (allCount??) >
					<#if allCount<=5 >
						<option value=${allCount}>全部${allCount}个</option>
					<#elseif allCount<=10 >
						<option value="5">最近5个</option>
						<option value=${allCount}>全部${allCount}个</option>
					<#elseif allCount<=15 >
						<option value="5">最近5个</option>
						<option value="10">最近10个</option>
						<option value=${allCount}>全部${allCount}个</option>
					<#elseif allCount<=20 >
						<option value="5">最近5个</option>
						<option value="10">最近10个</option>
						<option value="15">最近15个</option>
						<option value=${allCount}>全部${allCount}个</option>
					<#else>	
						<option value="5">最近5个</option>
						<option value="10">最近10个</option>
						<option value="15">最近15个</option>
						<option value="20">最近20个</option>
						<option value=${allCount}>全部${allCount}个</option>
					</#if>
				</#if>
			</select>
		</div>
		<div class="list-msg">
			
		</div>
		<button class="btn btn-reset" id="btnRefresh">刷新</button>
	</div>
	<div class="map-main" id="mapContainer"></div>
</div>