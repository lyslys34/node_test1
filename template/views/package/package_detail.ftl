<#include "package_config.ftl">
<#include "../widgets/sidebar.ftl">

<link rel="stylesheet" href="${staticPath}/css/page/delivery.css" />

<div id="wrapper">
    <!--#include virtual="/static/demos/components/top-nav.html"-->
    <!--#include virtual="/static/demos/components/sidebar.html"-->

    <div id="main-container">
        <div class="padding-md">
            <h3 class="headline m-top-md">包裹&nbsp;#${detail.data.pkgViewId!''}<span class="line"></span></h3>

            <div class="panel panel-default package-detail">
                <div class="panel-heading clearfix">
                    <span class="fl name">${merchantVo.name!''}</span>
                    <span class="fl phone">${merchantVo.phone!''}</span>
                    <span class="fr address">${merchantVo.address!''}</span>
                </div>
                <#list detail.data.wayBillVoList as waybill>
                <div class="panel-body">
                    <div class="col-md-3 col-sm-3">
                        <div class="row left-row">运单</div>
                        <div class="row left-row">${waybill.waybillViewId!''}</div>
                        <div class="row left-row">
                            <span class="label label-success">${waybill.status!''}</span>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row f-row">
                                    <span class="fl strong">配送站：</span>
                                    <span class="fl">${organizationVo.orgName!''}</span>
                                </div>
                                <div class="row f-row">
                                    <span class="fl strong">骑手：</span>
                                    <span class="fl">${waybill.riderName!''}</span>
                                </div>
                                <div class="row f-row">
                                    <span class="fl strong">骑手电话：</span>
                                    <span class="fl">${waybill.riderPhone!''}</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row f-row">
                                    <span class="fl strong">收货地址：</span>
                                    <span class="fl">${waybill.recipientAddress!''}</span>
                                </div>
                                <div class="row f-row">
                                    <span class="fl strong">收货姓名：</span>
                                    <span class="fl">${waybill.recipientName!''}</span>
                                </div>
                                <div class="row f-row">
                                    <span class="fl strong">收货电话：</span>
                                    <span class="fl">${waybill.recipientPhone!''}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>商品数量</th>
                                    <th>商品价格</th>
                                    <th>应付价格</th>
                                    <th>应收价格</th>
                                    <th>实付价格</th>
                                    <th>实收价格</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>${totalNum!''}</td>
                                    <td>${detail.data.packagePrice!''}</td>
                                    <td>${waybill.planPayAmount!''}</td>
                                    <td>${waybill.planChargeAmount!''}</td>
                                    <td>${waybill.actualPayAmount!''}</td>
                                    <td>${waybill.actualChargeAmount!''}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </#list>
            </div>
            <div class="panel panel-default package-detail">
                <div class="panel-heading">
                    <span>包裹明细</span>
                </div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>商品名</th>
                                <th>商品个数</th>
                                <th>商品价格</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#list productDetails as productDetail>
                            <tr>
                                <td>${productDetail.productName!''}</td>
                                <td>${productDetail.count!''}</td>
                                <td>${productDetail.price!''}</td>
                            </tr>
                            </#list>
                            <tr>
                                <td>合计</td>
                                <td>${totalNum!''}</td>
                                <td>${totalPrice!''}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <td>活动减免</td>
                                <td>${detail.data.packageValue - detail.data.packagePrice}</td>
                            </tr>
                            <tr>
                                <td>配送费</td>
                                <td>${detail.data.shippingFee!''}</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div>
                <div class="panel panel-default package-detail">
                    <div class="panel-heading" style="width: 50%; float: left;">
                        <span>流转时效</span>
                    </div>
                    <div class="panel-heading" style="width: 50%; float: left;">
                        <span>配送评分</span>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>流转状态</th>
                                        <th>时间</th>
                                        <th>信息</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <#list detail.data.trackVoList as trackVo>
                                    <tr>
                                        <td>${trackVo.status!''}</td>
                                        <td>${trackVo.utime!''}</td>
                                      <td>${trackVo.notes!''}</td>
                                    </tr>
                                    </#list>
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-md-6">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr>
                                        <td width="23%">配送评分</td>
                                        <td width="77%">配送评价</td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <#if detail.data.commentScore?exists &&  detail.data.commentScore gt 0>
                                            <#list 1..detail.data.commentScore as i>
                                                <i class="fa fa-star fa-lg" style="color:#ffc609; letter-spacing: -3px;"></i>
                                            </#list>
                                        <#else>
                                            未评分
                                        </#if>
                                        </td>
                                        <td><#if detail.data.comment?exists &&  detail.data.comment != ""> ${detail.data.comment} <#else> 未评价 </#if></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
<#--
                <div class="panel panel-default package-detail" style="width:40%;">
                    <div class="panel-heading">
                        <span>流转时效</span>
                    </div>
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-6">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr>
                                        <td width="23%">配送评分</td>
                                        <td width="77%">配送评价</td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <#if detail.data.commentScore?exists &&  detail.data.commentScore gt 0>
                                            <#list 1..detail.data.commentScore as i>
                                                <i class="fa fa-star fa-lg" style="color:#ffc609; letter-spacing: -3px;"></i>
                                            </#list>
                                        <#else>
                                            未评分
                                        </#if>
                                        </td>
                                        <td><#if detail.data.comment?exists &&  detail.data.comment != ""> ${detail.data.comment} <#else> 未评价 </#if></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>
<script>document.title = '包裹详情';</script>
