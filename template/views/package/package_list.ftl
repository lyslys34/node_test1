<#include "package_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/audit/package_city_list.js</content>

<link rel="stylesheet" type="text/css" href="/static/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />

<style type="text/css">
.ui-autocomplete{
    z-index: 2000;
}
#main-container{
    padding: 10px 20px;
}
</style>

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" action="/package/list" method="post" >
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">包裹号</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="packageViewID" id="packageViewID" data-required="true" data-minlength="8" value="${packageViewID!''}"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">订单展示ID</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="platformOrderId" id="platformOrderId" data-required="true" data-minlength="8" value="${platformOrderId!''}"/>
                        </div>
                    </div>
                    <#--<div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">收件手机号</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="recipientPhone" id="recipientPhone" data-required="true" data-minlength="8" value="${recipientPhone!''}"/>
                        </div>
                    </div>
                    -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">包裹状态</label>
                            <select class="form-control input-sm" id="packageState" name="packageState">
                                <option value="" <#if packageState == -1 > selected="selected" </#if>>全部</option>
                                <option value="0" <#if packageState == 0 > selected="selected" </#if>>待调度</option>
                                <option value="10" <#if packageState == 10 > selected="selected" </#if>>未接单</option>
                                <option value="20" <#if packageState == 20 > selected="selected" </#if>>已接单</option>
                                <option value="30" <#if packageState == 30 > selected="selected" </#if>>已取货</option>
                                <option value="50" <#if packageState == 50 > selected="selected" </#if>>已送达</option>
                                <option value="90" <#if packageState == 90 > selected="selected" </#if>>待取消</option>
                                <option value="99" <#if packageState == 99 > selected="selected" </#if>>已取消</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">商家POI</label>
                            <input type="text" class="form-control input-sm parsley-validated" name="merchantPOI" id="merchantPOI" data-required="true" data-minlength="8" value="${merchantPOI!""}"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">城市</label>
                            <input type="text" class="form-control input-sm parsley-validated js_city_name" name="orgCity" id="orgCity" data-required="true" data-minlength="8" value="${orgCity!""}"/>
                            <input type="hidden" class="js_city_id" name="cityId" id="cityId" value="${cityId!""}"/>
                        </div>
                    </div>
                    <#--<div class="col-md-3">-->
                        <#--<div class="form-group">-->
                            <#--<label class="control-label">日期</label>-->
                            <#--<input type="text" class="input-sm parsley-validated" name="date" id="date" data-required="true" data-minlength="8" value="${date!""}"/>-->
                        <#--</div>-->
                    <#--</div>-->

                    <div class="col-md-4" style="padding-top:18px">
                        <button type="submit" class="btn btn-success btn-block">查询</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>包裹号</th>
                <th>包裹状态</th>
                <th>订单展示ID</th>
                <#--<th>运单号</th>-->
                <th>收件地址</th>
                <th>骑手手机号</th>
                <th>骑手姓名</th>
                <th>商家POI</th>
                <th>商家推单时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
            <#list packages as package>
                <tr> 
                    <td>${package.pkgViewId!''}</td>
                    <td>${package.packageState!''}</td>
                    <#--<td>${package.waybillViewId!''}</td>-->
					<td>${package.platformOrderId!''}</td>
                    <td>${package.address!''}</td>
                    <td>${package.riderPhone!''}</td>
                    <td>${package.riderName!''}</td>
                    <td>${package.merchantPOI!''}</td>
                    <td>${package.ctime!''}</td>
					<td>
                        <a data-toggle="tooltip" title="查看详情" data-placement="top" target="_blank" href="/package/detail?packageID=${package.packageId!''}">
                            <i class="fa fa-info-circle fa-lg opration-icon"></i>
                        </a>
                    </td>
                </tr>
            </#list>
            </tbody>
        </table>

    <#import "../page/pager.ftl" as q>
    <#if recordCount??>
        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/package/list"/>
    </#if>

    </div>
</div>
<script>document.title = '包裹管理';</script>
