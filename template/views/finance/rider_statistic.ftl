<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<#include "config.ftl" >
<content tag="pageName">dispatcher_static_list</content>
<content tag="javascript">/finance/rider_statistic.js</content>
<content tag="cssmain">/finance/rider_statistic.css</content>
<div id="main-container">
    <div css="content_body">
        <!--<div class="navi_head">
            <div class="navi_title">配送员结算报表</div>
            <div class="navi_bottom_line">&nbsp;</div>
        </div>-->
        <div class="condition_area">
            <div class="alert small alert-warning alert-dismissible" style="<#if error?exists>display:block;<#else>display:none</#if>" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                ${error !''}
            </div>
            <form id="fm" method="get" action="riderStatistics" class="form-inline">
                配送员名称&nbsp;
                <select name="bmid" class="form-control input-sm" style="max-width:100px;">
                    <option <#if !RequestParameters.bmid?? || (RequestParameters.bmid == '-1')>selected="selected" </#if> value="-1">全部</option>
                    <#if userlist?exists >
                        <#list userlist as u >
                            <option <#if RequestParameters.bmid ?exists && RequestParameters.bmid == (u.id?c)>selected="selected" </#if> value="${u.id !'-1'}">${u.name !''}</option>
                        </#list>
                    </#if>
                </select>
                &nbsp;&nbsp;&nbsp;&nbsp;
                结算时间段&nbsp;
                <input type="text" name="ts" style="cursor: pointer" readonly="readonly" value="<#if ts?exists>${ts}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_start"/>
                至&nbsp;
                <input type="text" name="te" style="cursor: pointer" readonly="readonly" value="<#if te?exists>${te}<#else>${.now?string('yyyy-MM-dd')}</#if>" class="form-control input-sm date-picker J-datepicker js_date_end"/>

                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="button" value="查看" class="btn btn-success js_submitbtn"/>
                <a href="exportRiderStatistics?bmid=${RequestParameters.bmid!''}&ts=${Request.ts!''}&te=${Request.te!''}" target="_blank" class="btn btn-success js_submitbtn">导出表格</a>
                <a name="detail" style="margin-right:20px;float:right;" target="_blank"
                   href="<#if orgType==2>/partner/settlementStrategy/agent<#elseif orgType==1>/partner/settlementStrategy/self</#if>">
                    <label style="padding: 8px 0px;color:#4ac598;cursor:pointer;">结算规则</label>
                    <i class="fa fa-question-circle fa-lg opration-icon"></i>
                </a>
            </form>

        </div>

        <div class="result_area">
            <p>查询结果</p>
            <#if allpage?exists>
                <table class="table">
                    <tr>
                        <th>数据项</th>
                        <th>完成订单数</th>
                        <th>代付商家</th>
                        <th>代收用户</th>
                        <th>实付商家</th>
                        <th>实收用户</th>
                        <th>应上交\应补贴</th>
                        <th>配送费</th>
                    </tr>
                    <#if (allpage.totalAllRiderStatisticView)?exists>
                        <tr>
                            <td>总计</td>
                            <td>${allpage.totalAllRiderStatisticView.finishPkgCount !''}</td>
                            <td>￥${allpage.totalAllRiderStatisticView.allPay !''}</td>
                            <td>￥${allpage.totalAllRiderStatisticView.allCharge !''}</td>
                            <td>￥${allpage.totalAllRiderStatisticView.allActualPay !''}</td>
                            <td>￥${allpage.totalAllRiderStatisticView.allActualCharge !''}</td>
                            <td>
                                <#if allpage.totalAllRiderStatisticView.payChargeDis gt 0>应补贴￥${allpage.totalAllRiderStatisticView.payChargeDis !''}
                                <#elseif allpage.totalAllRiderStatisticView.payChargeDis lt 0>应上交￥${allpage.totalAllRiderStatisticView.payChargeDis*-1 !''}
                                <#else> ￥${allpage.totalAllRiderStatisticView.payChargeDis !''}
                                </#if>
                            </td>
                            <td>￥${allpage.totalAllRiderStatisticView.deliveryFee !''}</td>
                        </tr>
                    </#if>
                </table>

                <div class="table_title form-inline">
                    <span><p>结算细目</p></span>
                </div>
                <div></div>
                <table class="table">
                    <tr>
                        <th>姓名</th>
                        <th>完成订单数</th>
                        <th>代付商家</th>
                        <th>代收用户</th>
                        <th>实付商家</th>
                        <th>实收用户</th>
                        <th>应上交\应补贴</th>
                        <th>配送费</th>
                        <th>款项明细</th>
                    </tr>

                    <#if (allpage.allRiderStatisticViewList)?exists>
                        <#list  allpage.allRiderStatisticViewList as a>
                            <tr>
                                <td>${a.bmUserName !''}</td>
                                <td>${a.finishPkgCount !''}</td>
                                <td>￥${a.allPay !''}</td>
                                <td>￥${a.allCharge !''}</td>
                                <td>￥${a.allActualPay !''}</td>
                                <td>￥${a.allActualCharge !''}</td>
                                <td>
                                    <#if a.payChargeDis gt 0>应补贴￥${a.payChargeDis !''}
                                    <#elseif a.payChargeDis lt 0>应上交￥${a.payChargeDis*-1 !''}
                                    <#else> ￥${a.payChargeDis !''}
                                    </#if>
                                </td>
                                <td>￥${a.deliveryFee !''}</td>
                                <td>
                                <a data-toggle="tooltip" title="详情" data-placement="top" href="riderStatistics?ts=${Request.ts!''}&te=${Request.te !''}&bmid=${a.bmUserId !''}" target="_blank" class="mainc">
                                    <i class="fa fa-info-circle fa-lg opration-icon"></i>
                                </a>
                                </td>

                            </tr>

                        </#list>

                    </#if>


                </table>
            <#elseif singlepage?exists>
                <table class="table">
                    <tr>
                        <th>数据项</th>
                        <th>完成订单数</th>
                        <th>代付商家</th>
                        <th>代收用户</th>
                        <th>实付商家</th>
                        <th>实收用户</th>
                        <th>应上交\应补贴</th>
                        <th>订单总金额</th>
                        <th>配送费</th>
                    </tr>
                    <#if (singlepage.totalRiderStatisticView)?exists>
                        <tr>
                            <td>总计</td>
                            <td>${singlepage.totalRiderStatisticView.finishPkgCount !''}</td>
                            <td>￥${singlepage.totalRiderStatisticView.allPay !''}</td>
                            <td>￥${singlepage.totalRiderStatisticView.allCharge !''}</td>
                            <td>￥${singlepage.totalRiderStatisticView.allActualPay !''}</td>
                            <td>￥${singlepage.totalRiderStatisticView.allActualCharge !''}</td>
                            <td>
                                <#if singlepage.totalRiderStatisticView.payChargeDis gt 0>应补贴￥${singlepage.totalRiderStatisticView.payChargeDis !''}
                                <#elseif singlepage.totalRiderStatisticView.payChargeDis lt 0>应上交￥${singlepage.totalRiderStatisticView.payChargeDis*-1 !''}
                                <#else> ￥${singlepage.totalRiderStatisticView.payChargeDis !''}
                                </#if>
                            </td>
                            <td>￥${singlepage.totalRiderStatisticView.allValue !''}</td>
                            <td>￥${singlepage.totalRiderStatisticView.deliveryFee !''}</td>
                        </tr>
                    </#if>
                </table>

                <div class="table_title form-inline">
                    <span><p>结算细目</p></span>
                    <a href="exportRiderStatistics?bmid=${RequestParameters.bmid!''}&ts=${Request.ts!''}&te=${Request.te!''}" target="_blank" class="btn btn-sm btn-default">导出表格</a>
                </div>
                <div></div>
                <table class="table">
                    <tr>
                        <th>订单号</th>
                        <th>订单完成时间</th>
                        <th>商家名称</th>
                        <th>商家类型</th>
                        <th>订单类型</th>
                        <th>代付商家</th>
                        <th>代收用户</th>
                        <th>实付商家</th>
                        <th>实收用户</th>
                        <th>订单金额</th>
                        <th>配送费</th>
                    </tr>

                    <#if (singlepage.riderStatisticViewList)?exists>
                        <#list  singlepage.riderStatisticViewList as a>
                            <tr>
                                <td>${a.platformOrderId !''}</td>
                                <td>${a.finishedTime !''}</td>
                                <td>${a.poiName !''}</td>
                                <td>${a.poiTypeText !''}</td>
                                <td>${a.payTypeText !''}</td>
                                <td>￥${a.payAmount !''}</td>
                                <td>￥${a.chargeAmount !''}</td>
                                <td>￥${a.allActualPay !''}</td>
                                <td>￥${a.allActualCharge !''}</td>
                                <td>￥${a.money !''}</td>
                                <td>￥${a.deliveryFee !''}</td>
                            </tr>

                        </#list>

                    </#if>
                </table>
            </#if>
            <div style="text-align: right;">
                <#noescape>${pagehtml!''}</#noescape>
            </div>


        </div>



    </div>
</div>
<script>
    document.title = '配送员结算报表';
    $('select').select2();
</script>