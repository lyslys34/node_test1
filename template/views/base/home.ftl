<#include "home_config.ftl">
<#include "../widgets/sidebar.ftl">

<div id="main-container">
  <div class="padding-md">
    <div class="row">
      <div class="col-md-11">
        <h3 class="headline m-top-md">欢迎来到美团配送烽火台！<span class="line"></span></h3>
      </div>
    </div>
    <div><h4>新的一天祝你有个好心情！</h4></div>
  </div>
</div>

<style type="text/css">
    #main-container{
        min-height: 0;
    }
    #wrapper{
        min-height: 0;
    }
</style>