<#-- 加载全局配置文件 -->
<#include "../config.ftl">
<#include "../widgets/sidebar.ftl">

<title>phome管理</title>
<content tag="cssmain">/base/manager.css</content>
<content tag="javascript">/base/manager.js</content>
<script src='/static/js/lib/highcharts-4.1.5/highcharts.js'></script>
<#assign monitorRootUrlUtil= "com.sankuai.meituan.banma.admin.util.ftl.MonitorRootUrl"?new()>
<div id="main-container" class="clearfix">
	<div class="group_banner clearfix">
		<#if (orgList??) && (orgList?size > 0) >
			<h4 id="orgName" class="fl dark">${orgList[0].orgName}</h4>
		</#if>
		<div class="fr select_box">
			<select id="orgSelect" class="fr">
				<#if (orgList??) && (orgList?size > 0) >
					<#list orgList as orgView>
						<option value="${orgView.orgId}">${orgView.orgName}</option>
					</#list>
				</#if>
			</select>
		</div>
	</div>
	<#-- 消息模块 -->
	<div class="module long clearfix" id="mMsg">
		<h5 class="module_title">今日待办事项</h5>
		<div class="msg_box" id="msgBox">
		</div>
	</div>
	<#-- 数据监控模块 -->
	<div class="module mid cleafix" id="mMonitor">
		<h5 class="module_title">实时监控<a href="${monitorRootUrlUtil("")}/monitor/index" class="module_link green">实时监控<i class="fa fa-chevron-right"></i></a></h5>
		<div class="chart_container">
			<span class="chart_title">人均<br/>负载</span>
			<div class="highchart_box" id="chart1"></div>
		</div>
		<div class="chart_container">
			<span class="chart_title">手工<br/>派单</span>
			<div class="highchart_box" id="chart2"></div>
		</div>
		<div class="chart_container">
			<span class="chart_title">超时<br/>单率</span>
			<div class="highchart_box" id="chart3"></div>
		</div>
		<div class="chart_container">
			<span class="chart_title">开工<br/>比例</span>
			<div class="highchart_box" id="chart4"></div>
		</div>
	</div>
	<#-- 天气模块 -->
	<div class="module mid clearfix" id="mWeather">
		<h5 class="module_title">日历</h5>
		<div class="we_box" id="weBox"></div>
	</div>
	<#-- 运营数据模块 -->
	<div class="module mid clearfix" id="mBiz"></div>
</div>
<script type="text/javascript">
	window.bm_modules = ''
	<#if modules??>
		window.bm_modules = '${modules}';
	</#if>
</script>

<#-- 消息模块 -->
<script id="J_msg_tpl" type="text/template">
	<p class="green" style="margin-bottom: 5px;">有<span id="unreadCount" class="red big">{{unreadCount}}</span>条未读通知 <a class="module_link green" href="/msg/r/list?type=1"><small>去阅读<i class="fa fa-chevron-right"></i></small></a></p>
	<ul class="msg_list">
		{{each notices as notice}}
		<li class="msg_item">
			<a class="msg_link {{notice.read == 0 ? 'unread' : ''}}" href="{{notice.url}}" target="_blank">
				{{if notice.read == 0}}
					<span class="msg_icon">未读</span>
				{{/if}}
				<p class="msg_title">{{notice.title}}</p>
				<small class="msg_time">{{notice.time}}</small>
			</a>
		</li>
		{{/each}}
	</ul>
</script>
<#-- 天气模块 -->
<script id="J_we_none_tpl" type="text/template">
	<p style="text-align: center;font-size: 16px;">暂无天气数据</p>
	<p style="text-align: center;"><a class="green" target="_blank" href="http://www.weather.com.cn/weather/101010100.shtml">中国天气网</a></p>
</script>
<script id="J_we_single_tpl" type="text/template">
	<p class="we_today dark">
		<span>{{weatherDate}}</span>
		<span>{{dayOfWeek}}</span>
	</p>
	<h5 class="we_today">{{currentTemperature}}&deg;</h5>
	<div style="overflow: hidden;padding: 0 5px;">
		<span class="fl">今天</span>
		<span class="fr">{{minTemperature}}&deg; - {{maxTemperature}}&deg;</span>
	</div>
	<div class="we_table_line"></div>
	<table class="we_table">
		<tr>
		{{each weatherHours as wea}}
			<td>{{wea.dateHour.split(/\s/)[1].split(':')[0]}}时</td>
		{{/each}}
		</tr>
		<tr>
		{{each weatherHours as wea}}
			<td><i class="wi" data-we="{{wea.weather}}"></i></td>
		{{/each}}
		</tr>
		<tr>
		{{each weatherHours as wea}}
			<td>{{wea.temperature}}&deg;</td>
		{{/each}}
		</tr>
	</table>
	<div class="we_table_line"></div>
	<table class="we_tomorrow">
		{{each weatherDays as wea}}
		<tr>
			<td width="20%">{{wea.dayOfWeek}}</td>
			<td width="40%"><i class="wi wi_day" data-we="{{wea.weatherDaytime}}"></i></td>
			<td width="40%"><i class="wi wi_night" data-we="{{wea.weatherNight}}"></i></td>
			<td class="tool_tip_hover" data-title="最高温度">{{wea.maxTemperature}}&deg;</td>
			<td class="tool_tip_hover" data-title="最低温度">{{wea.minTemperature}}&deg;</td>
		</tr>
		{{/each}}
	</table>
</script>
<script id="J_we_multi_tpl" type="text/template">
	<p class="we_today dark">
		<span>{{date}}</span>
		<span>{{week}}</span>
	</p>
	<div class="we_title">
		<span>今天实时天气</span>
		<span>明天预测天气</span>
	</div>
	<div class="we_table_wrapper">
		<table class="we_table">
			{{each list as li}}
			<tr>
				<td class="dark">{{li.cityName}}</td>
				{{each li.weatherDays as day}}
				<td><i class="wi" data-we="{{day.weatherDaytime}}"></i></td>
				<td>{{day.weatherDaytime}}</td>
				<td class="tool_tip_hover" data-title="最高温度">{{day.maxTemperature}}&deg;</td>
				<td class="tool_tip_hover" data-title="最低温度">{{day.minTemperature}}&deg;</td>
				{{/each}}
			</tr>
			{{/each}}
		</table>
	</div>
</script>
<#-- 运营数据模块 -->
<script id="J_biz_tpl" type="text/template">
	<h5 class="module_title">
		昨日运营概况
		{{if redirect == 1}}
			<a href="{{url}}" class="module_link green" target="_blank">运营指标监测<i class="fa fa-chevron-right"></i></a>
		{{/if}}
	</h5>
	<div id="bizBox" class="clearfix">
	{{each list as li}}
		<div class="biz_board">
			<p class="biz_title">{{li.name}}</p>
			<div class="biz_num tool_tip_hover" data-title="昨日数据">
				{{#li.yesterday}}
			</div>
			<div class="biz_tip">
				<span>{{li.compare}}</span>
				<i class="fa {{li.compareIcon}}"></i>
				<small class="tool_tip_hover" data-title="昨天比上周差值">比上周</small>
			</div>
		</div>
	{{/each}}
	</div>
</script>