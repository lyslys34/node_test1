<#include "../config.ftl">
<#include "../widgets/sidebar.ftl">

<title>组织管理</title>
<content tag="css">/home</content>
<content tag="javascript">/home.js</content>

<div id="main-container">
	<#if errorMsg?exists ><div class="alert alert-danger" role="alert">${errorMsg!""}</div></#if>

	<div><a class="btn btn-success" href="javascript:location.href = document.referrer;">返回上一页</a></div>
</div>

<script>document.title = '错误页面';</script>
