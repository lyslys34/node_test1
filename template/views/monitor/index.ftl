<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">

<link rel="stylesheet" type="text/css" href="/static/js/lib/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.css" />

<#if unAuthMsg?exists >
<div id="main-container">
    <div class="alert alert-danger" role="alert">${unAuthMsg}</div>
    <div><a class="btn btn-success" href="javascript:location.href = document.referrer;">返回上一页</a></div>
</div>
<#else>
<content tag="cssmain">/monitor/monitor.css</content>
<content tag="javascript">/monitor/controller/monitor.js</content>
<!-- main-container -->
<div id="main-container">
    <div class="main-header clearfix">
        <div class="page-title">
            <div class="btn-group-vertical">
                <div class="btn-group city-wrap" id='city'>
                    <select name="cityId" class="form-control input-sm js_cityId">
                        <option value="-1" data-cityId="-1" selected="selected">全部城市</option>
                        <#if cityViewList?exists && (cityViewList?size > 0) >
                            <#list cityViewList as cityView>
                                <option data-cityId='${cityView.cityId}'>${cityView.cityName}</option>
                            </#list>
                        </#if>
                    </select>
                    <span class="totalstation">共<#if cityViewList?exists>${cityViewList?size}<#else>0</#if>个城市</span>
                </div>
                <em class="hor-bar"> - </em>
                <div class="btn-group station-wrap" id='station'>
                    <select name="orgId" class="form-control input-sm js_orgId">
                        <option value="-1" data-orgId="-1" selected="selected">全部站点</option>
                        <#if orgViewList?exists && (orgViewList?size > 0) >
                            <#list orgViewList as orgView>
                                <option data-cityId='${orgView.city_id}' data-orgId='${orgView.id}'>${orgView.org_name}</option>
                            </#list>
                        </#if>
                    </select>
                    <span class="totalstation">共<#if orgListSize?exists>${orgListSize}<#else>0</#if>个站点</span>
                </div>
            </div>
        </div>
        <span class="refresh-time-wrap"><em class="refresh-time">60</em>s后自动刷新&nbsp;</span>
        <i class="fa fa-refresh fa-lg btn-refresh" title="刷新"></i>
        <!-- /page-stats -->
        <!-- /main-header -->
        <div class="shortcut-wrapper" style="float:right;">
            <a target="_blank" <#if waybillAuditClick?exists && (waybillAuditClick==1)>href="/partner/pWaybillAudit/list" </#if> class="shortcut-link click-num">
                <span class="shortcut-icon auditCount-wrap">
                    <i class="fa fa-envelope-o"></i>
                <#--<span id="auditCount" class="shortcut-alert">-->
                <#--</span>-->
                </span>
                <span  class="text">订单改价</span>
            </a>
            <a target="_blank" href="/partner/dispatch/home#-10" class="shortcut-link click-num">
                <span class="shortcut-icon assignCount-wrap">
                    <i class="fa fa-list"></i>
                <#--<span id="assignCount" class="shortcut-alert">-->
                <#--</span>-->
                </span>
                <span  class="text">指派任务</span>
            </a>
        </div>
    </div>
    <!-- /grey-container -->
    <div class="padding-md">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="panel-stat3 bg-green">
                    <h2 class="m-top-none" id="riderCount">0</h2>
                    <h5>在岗骑手数</h5>
                    <div class="info-wrap"> <i class='fa fa-question-circle fa-lg'></i>
                    <ul class='info'>
                        <li>在岗或忙碌状态的骑手数，包含掉线</li>
                        <li>变化率定义：1- （当前在岗骑手/总骑手数 ）/ （历史在岗骑手/总骑手数）</li>
                        <li>绿色：相比昨日及上周同期的变化率均小于10%</li>
                        <li>黄色：相比昨日及上周同期的变化率均大于10%</li>
                        <li>橙色：相比昨日及上周同期的变化率均大于15%</li>
                        <li>红色：相比昨日及上周同期的变化率均大于20%</li>
                        <li>注：周二~周五对比昨日，周六~周一对比上周今日</li>
                    </ul>
                    </div><br>
                    <i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs"></span>
                    <div class="stat-icon">
                        <i class="fa fa-group fa-3x"></i>
                    </div>
                    <div class="refresh-button" data-target="rider">
                        <i class="fa fa-refresh"></i>
                    </div>
                    <div class="loading-overlay">
                        <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-6 col-md-3">
                <div class="panel-stat3 bg-yellow">
                    <h2 class="m-top-none"><span id="timeoutCount">0.0</span>%</h2>
                    <h5>超时单占比</h5>
                    <div class="info-wrap"><i class='fa fa-question-circle fa-lg'></i>
                    <ul class='info'>
                        <li>只计算未完成订单</li>
                        <li>立即单超时：当前时间 - 下单时间 > 55分钟</li>
                        <li>预约单超时：当前时间 - 期望时间 > 15分钟</li>
                        <li>绿色：超时单占比 <10%</li>
                        <li>黄色：超时单占比 ≥10%</li>
                        <li>黄色：超时单占比 ≥15%</li>
                        <li>红色：超时单占比 ≥20%</li>
                    </ul>
                    </div><br>
                    <i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs"></span>
                    <div class="stat-icon">
                        <i class="fa fa-flag fa-3x"></i>
                    </div>
                    <div class="refresh-button" data-target="timeoutOrder">
                        <i class="fa fa-refresh"></i>
                    </div>
                    <div class="loading-overlay">
                        <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-6 col-md-3">
                <div class="panel-stat3 bg-orange">
                    <h2 class="m-top-none" id="loadCount">0.00</h2>
                    <h5>人均负载</h5>
                    <div class="info-wrap"><i class='fa fa-question-circle fa-lg'></i>
                    <ul class='info'>
                        <li>反映人均未完成任务量</li>
                        <li>计算公式：（未取餐×1 + 已取餐×0.5）/ 在岗骑手数</li>
                        <li>绿色：负载 <3 </li>
                        <li>黄色：负载 ≥3</li>
                        <li>橙色：负载 ≥5</li>
                        <li>红色：负载 ≥7</li>
                    </ul>
                    </div><br>
                    <i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs"></span>
                    <div class="stat-icon">
                        <i class="fa fa-dashboard fa-3x"></i>
                    </div>
                    <div class="refresh-button" data-target="perLoad">
                        <i class="fa fa-refresh"></i>
                    </div>
                    <div class="loading-overlay">
                        <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-6 col-md-3">
                <div class="panel-stat3 bg-red">
                    <h2 class="m-top-none" id="totalCount">0</h2>
                    <h5>今日订单量</h5>
                    <div class="info-wrap"><i class="fa fa-question-circle fa-lg"></i>
                        <ul class='info'>
                            <li>今日累计订单量，不区分订单状态</li>
                            <li>绿色：近十五分钟单量同比[昨日or上周] <20%</li>
                            <li>黄色：近十五分钟单量同比[昨日or上周] ≥20%</li>
                            <li>橙色：近十五分钟单量同比[昨日or上周] ≥40%</li>
                            <li>红色：近十五分钟单量同比[昨日or上周] ≥60%</li>
                            <li>注：周二~周五对比昨日，周六~周一对比上周今日</li>
                        </ul>
                    </div><br>
                    <i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs"></span>
                    <div class="stat-icon">
                        <i class="fa fa-bar-chart-o fa-3x"></i>
                    </div>
                    <div class="refresh-button" data-target="totalOrder">
                        <i class="fa fa-refresh"></i>
                    </div>
                    <div class="loading-overlay">
                        <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.padding-md -->

    <!-- tong 运力气泡图（散列图） -->
    <div id="scatter" class="highchart"></div>
    <!-- tong 进单量曲线图 -->
    <div id="plotLines" class="highchart"></div>

</div>
<#--浮层-->
<div class="cover-container">
    <div class="cover"></div>
</div>
<!-- Colorbox -->
<script src='/static/js/lib/jquery.colorbox.min.js'></script>
<!-- Popup Overlay -->
<script src='/static/js/lib/jquery/popupoverlay.js'></script>
<!-- Slimscroll -->
<script src='/static/js/lib/jquery/slimscroll.js'></script>
<!-- highcharts -->
<script src='/static/js/lib/highcharts-4.1.5/highcharts.js'></script>
<script src='/static/js/lib/highcharts-4.1.5/highcharts-more.js'></script>
<!-- Modernizr -->
<script src='/static/js/lib/modernizr.js'></script>
<!-- Endless -->
<script src="/static/js/lib/template.js"></script>
</#if>

<script>
    document.title = '实时监控';
    $('select').select2();
</script>