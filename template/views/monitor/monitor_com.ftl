<script>
(function() {
  // 异步加载js,css
  window.dyncLoad = function(arr) {
      if (typeof arr == 'string') arr = [arr];
      if ("[object Array]" == Object.prototype.toString.call(arr) && arr.length) {
          var ver = Math.floor(new Date().getTime() / 600000);
          $.each(arr, function(i, item) {
              if (/\.js$/.test(item)) {
                  var script = document.createElement("script");
                  script.type = "text/javascript";
                  script.src = item + "?ver=" + ver;
                  document.body.appendChild(script);
              } else if (/\.css$/.test(item)) {
                  var link = document.createElement("link");
                  link.rel = "stylesheet";
                  link.href = item + "?ver=" + ver;
                  document.getElementsByTagName("head")[0].appendChild(link);
              }
          });
      }
  };
  dyncLoad([url_config.monitor + "/static/js/page/monitor/controller/monitor_com.js", url_config.monitor + "/static/css/page/monitor/monitor_com.css"]);
})();
</script>
