<#include "train_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/train/examinee.js</content>
<content tag="cssmain">/train/examinee.css</content>
<div id="main-container" class="container hide">
    <h2 class="exam-title"></h2>

    <h3>
        <span>距离自动交卷仅剩
            <span class="hour"></span>时
            <span class="minutes"></span>分
            <span class="second"></span>秒
            &nbsp请您合理安排时间 ^_^
        </span>
    </h3>


    <div class="score-analysis hide">
        <h4>成绩分析</h4>
        <table>
            <tr>
                <td>得分: <span class="score"></span>分</td>
                <td>答对: <span class="true-answer"></span>题</td>
            </tr>
            <tr>
                <td>满分: 100分</td>
                <td>题数: <span class="question-count"></span>题</td>
            </tr>
        </table>
    </div>
    <div id="exam-container">

    </div>
    <div class="submit">
        <button class="btn btn-success save">保存</button>
        <button class="btn btn-success commit">交卷</button>
        <button class="btn btn-success close-button hide">关闭考卷</button>
    </div>
    <!--弹出返回提示层-->
    <div class="modal fade" id="showReturnRes">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="alert_msg" style="text-align:center">

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="http://xs01.meituan.net/cdn/moment/2.10.6/moment.min.js"></script>