<#include "train_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/train/train_edit.js</content>
<content tag="cssmain">/train/train_edit.css</content>
<link rel="stylesheet" type="text/css" media="all" href="/static/css/lib/daterangepicker-bs3.css" />
<div id="main-container">
    <h4 style="padding-left:15px;">新建考试</h4>
    <input id="trainId" type="hidden" value="${trainId}" />
    <div class="panel panel-default table-responsive">
		<div class="container">
			<div class="row clearfix">
				<div class="col-md-12 column">
					<div class="row clearfix">
						<div class="col-md-12 column sub_title">
						 	考试基本信息
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-12 column">
							<div class="row clearfix">
								<div class="col-md-2 column">
									名称：
								</div>
								<div class="col-md-10 column">
									<input id='train_title' name="name" type="text" style="width:40%"/>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-2 column train-content">
									App展示文案
								</div>
								<div class="col-md-10 column">
									<input id='train_content' name="content" type="text" style="width:40%"/>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-2 column train-desc">
									备注
								</div>
								<div class="col-md-10 column">
									<input id='train_desc' placeholder="展示后端，可对本次考试进行简要说明" name="desc" type="text" style="width:40%"/>
								</div>
							</div>
							<div class="row clearfix org-type">
								<div class="col-md-2 column">
									组织类型
								</div>
								<div class="col-md-2 column">
									<input id="org_type_crew" checked=true orgtype="1" name="org_type" type="radio"/>
									<label for='org_type_crew'>众包APP</label>
								</div>
								<div class="col-md-2 column">
									<input id="org_type_rider" orgtype="4" name="org_type" type="radio"/>
									<label for='org_type_rider'>骑手APP</label>
								</div>
								<div class="col-md-2 column">
                  <input id="org_type_self_construct" orgtype="2" name="org_type" type="radio"/>
                  <label for='org_type_self_construct'>自建</label>
                </div>
								<div class="col-md-2 column">
									<input id="org_type_affiliate" orgtype="3" name="org_type" type="radio"/>
									<label for='org_type_affiliate'>加盟</label>
								</div>
							</div>
							<div class="row clearfix limit">
								<div class="col-md-2 column">
									是否限制抢单
								</div>
								<div class="col-md-2 column">
									<input id="force_false" checked=true name="grab" type="radio"/>
									<label for='force_false' style='padding-left:30px'>否</label>
								</div>
								<div class="col-md-2 column">
									<input id="force_true" name="grab" type="radio"/>
									<label for='force_true' style='padding-left:30px;'>是</label>
								</div>
							</div>
							<div class="row clearfix apply-role">
                <div class="col-md-2 column">
                  考生角色
                </div>
                <div class="col-md-8 column checkbox-wrapper">
                  <input value="1001" type="checkbox"/>
                  <label for="role">众包骑手</label>
                </div>
              </div>
              <div class="row clearfix exam-time app-item">
                <div class="col-md-2 column">
                  考试周期
                </div>
                <div class="col-md-4 column">
                  <div class="reportrange">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                      <input type="text" /> - <input type="text" /> <b class="caret"></b>
                  </div>
                </div>
              </div>
              <div class="row clearfix exam-time app-item">
                <div class="col-md-2 column">
                  考试时间
                </div>
                <div class="col-md-2 column">
                  <input name="exam-time" type="number" min="0"/><label>分钟</label>
                </div>
              </div>
						</div>
					</div>
				</div>
				<div class="col-md-2 column">
				</div>
			</div>
		</div>
    </div>

    <div class="panel panel-default table-responsive">
		<div class="train-material crew-item container">
			<div class="row clearfix">
				<div class="col-md-12 column">
					<div class="row clearfix">
						<div class="col-md-12 column sub_title">
							培训材料
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-2 column">
							请上传培训资料，支持批量上传，格式：png/jpeg
						</div>
						<div class="col-md-3 column">
							<button id="upload-button" class="hupload btn btn-primary" style="width:100px">上传图片</button>
						</div>
						<div class="col-md-7 column hupload-container">

						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-12 column">
							<button id="batch-del" class="btn btn-danger btn-block bu-lw" style="width:100px">批量删除</button>
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-12 column">
							<table class="table table-striped" style="margin-bottom:0px">
								<thead>
				                    <tr>
				                    	<th width="5%"></th>
				                        <th width="5%">编号</th>
				                        <th width="60%">图片</th>
				                        <th width="30%">操作</th>
				                    </tr>
				                </thead>
							</table>
							<div class='tbody'>
								<table id="pic-table" class="table table-striped" style="margin-bottom:0">
					                <tbody class="tbody-pic" id="tbody-pic">
					                <#if trainContents?exists && (trainContents?size>0)>
					                <#list trainContents as c>
					                    <tr rel="${c.id}">
					                    	<td width="10%" class="check_box"><input class="singleSelect" type="checkbox" /></td>
					                        <td width="10%" class="t_no">${c_index + 1}</td>
					                        <td width="40%" class="t_pic">
												<a target="_blank" class='t_url' href="${c.url!''}">${c.name !''}</a>
					                        <td width="40%" class="t_op">
					                            <input type="button" value="修改"  class="btn btn-primary btn-warning js_submitbtn modify" /> &nbsp
					                            <input type="button" value="删除"  class="btn btn-primary btn-danger js_submitbtn del" />&nbsp
					                            <input type="button" value="上移"  class="btn btn-primary btn-info js_submitbtn up" />&nbsp
					                            <input type="button" value="下移"  class="btn btn-primary btn-info js_submitbtn down" />
					                        </td>
					                    </tr>
					                </#list>
					                </#if>
					                </tbody>
				            	</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	<div class="panel panel-default table-responsive">
		<div class="container">
			<div class="row clearfix">
				<div class="col-md-12 column">
					<div class="row clearfix">
						<div class="col-md-12 column sub_title">
							考试题目
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-12 column">
							<button id="createSubject" type="button" class="btn btn-primary bu-w" style="width:100px">添加新题目</button>
						  <p class="other-item exam-grade">满分100分,已出<span class="exam-count">0</span>题,每题<span class="average-grade">0</span>分</p>
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-12 column">
							<table class="table table-striped" style="margin-bottom:0px">
								<thead>
				                    <tr>
				                    	<th width="10%">编号</th>
				                        <th style="text-align:left" width="45%">题目</th>
				                        <th width="15%">题目类型</th>
				                        <th width="30%">操作</th>
				                    </tr>
				                </thead>
							</table>
							<div class="tbody">
								<table id="subject_table" class="table table-striped" style="margin-bottom:0px">
					                <tbody id="tbody-dispatcher-list">
					                <#if examSubjects?exists && (examSubjects?size>0)>
					                <#list examSubjects as s>
					                    <tr>
					                        <td width="5%" class="t_no">${s_index + 1}</td>
					                        <td width="50%" class="t_content">${s.name !''}</td>
					                        <td width="15%" class="t_type">${s.type !''}</td>
					                        <td width="30%" class="t_op">
					                            <input type="button" value="修改"  trainId="${s.id !''}" class="btn btn-primary btn-warning js_submitbtn modify" />&nbsp
					                            <input type="button" value="删除"  trainId="${s.id !''}" class="btn btn-primary btn-danger js_submitbtn del" />&nbsp
					                            <input type="button" value="上移"  trainId="${s.id !''}" class="btn btn-primary btn-info js_submitbtn up" />&nbsp
					                            <input type="button" value="下移"  trainId="${s.id !''}" class="btn btn-primary btn-info js_submitbtn down" />
					                        </td>
					                    </tr>
					                </#list>
					                </#if>
					                </tbody>
					            </table>
				        	</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default table-responsive">
		<div class="container">
			<div class="row clearfix">
				<div class="col-md-2 column">
				</div>
				<div class="col-md-4 column">
					<button id='cancel_train' class="btn btn-danger center-block bu-w" style="width:100px">取消</button>
				</div>
				<div class="col-md-4 column">
					<button id="commit_train" class="btn btn-primary center-block bu-w" style="width:100px">提交</button>
				</div>
				<div class="col-md-2 column">
				</div>
			</div>
		</div>
	</div>

    <#-- 弹出确认层 -->
    <div class="modal fade" id="showConfirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" asria-hidden="true">&times;</button>
                    <h4 id="showConfirmTitle"></h4>
                    <div class="modal-footer">
                        <input type="submit" value="确定" class="btn btn-info btn-sm" id="confirm" style="width:100px;"/>
                        <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" style="width:100px;">再想想</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--弹出返回提示层-->
    <div class="modal fade" id="showReturnRes">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="alert_msg" style="text-align:center">

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="showCreate">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" style="text-align:center">
                	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 id='subject_title' >添加新题目</h4>
                    <div class="row clearfix">
                    	<div id="subject-contain" class="col-md-12 column">
							<div class="row clearfix">
								<input id="subject_id" type="hidden" value="" />
								<div class="col-md-2 column text-right">
									类型:
								</div>
								<div class="col-md-2 column">
									<input id="type_single" name="subject_type" class="subject_type single" type="radio" /> <label class="check_lab">单选题</label>
								</div>
								<div class="col-md-2 column">
									<input id="type_multi" name="subject_type" class="subject_type multi" type="radio" /> <label class="check_lab">多选题</label>
								</div>
							</div>
							<div class="row clearfix" style="border-bottom:1px solid #F3F3F3;padding-bottom:15px">

								<div class="col-md-2 column text-right">
									题目:
								</div>
								<div class="col-md-9 column">
									<input id="desc" name="desc" type="text" style="width:100%"/>
								</div>
								<div class="col-md-1 column"></div>
							</div>
							<div class="row clearfix answer text-center center-block">
								<input class="answerId" name="answerId" type="hidden" value=""/>
								<div class="col-md-2 column answer_d text-right">
									答案<span class="ans_tag">A</span>
								</div>
								<div class="col-md-4 column" style="height:34px">
									<input type="text" class="center-block answer-content" style="width:100%;margin-top:5px"/>
								</div>
								<div class="col-md-3 column">
									<button  class="add-answer btn btn-success">+</button>
									<button  class="rm-answer btn btn-danger">-</button>
								</div>
								<div class="col-md-3 column text-left answer_d">
									<input name="answer_c" class="answer_c" type="radio" style="margin-top:8px" />
									<label class="check_lab">正确答案</label>
								</div>
							</div>
						</div>
					</div>
                    <div class="modal-footer">
                        <button class="btn btn-info btn-sm" id="confirmCreate" style="width:60px;"/>确认</button>
                        <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" style="width:60px;">再想想</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="loading" >
	<img border="0" id="loadingPic" align="right" src="/static/imgs/loading_trans.gif" style="width:700px;height:600px"/>
</div>

<script>
    document.title = '新建培训流程';
    //$('select').select2();
</script>
<script type="text/javascript" src="http://xs01.meituan.net/cdn/moment/2.10.6/moment.min.js"></script>
<script type="text/javascript" src="http://xs01.meituan.net/cdn/daterangepicker/1.3.21/daterangepicker.js"></script>
