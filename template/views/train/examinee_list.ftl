<#include "train_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/train/examinee_list.js</content>
<content tag="cssmain">/train/examinee_list.css</content>
<div id="main-container" class="container">
    <div class="exam-notification">
        <img src="/static/imgs/exam_notification.png">
        <span>如何应对考试?请关注近期新上线的产品功能与业务通知哦~</span>
    </div>
    <div class="exam-manage page-container">
      <table id="table" class="table table-striped">
          <thead>
              <tr>
                  <th>卷名</th>
                  <th>考试周期</th>
                  <th>交卷时间/考试时间(分钟)</th>
                  <th>分数</th>
                  <th>错题数</th>
                  <th>状态</th>
                  <th>操作</th>
              </tr>
          </thead>
          <tbody class="tbody-dispatcher-list">

          </tbody>
      </table>
      <div class="pager-wrap">
          <div class="pager"></div>
      </div>
    </div>


    <#-- 弹出确认层 -->
    <div class="modal fade" id="showConfirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 id="showConfirmTitle"></h4>
                    <div class="modal-footer">
                        <input type="submit" value="确定" class="btn btn-info btn-sm" id="confirm" style="width:60px;"/>
                        <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" style="width:60px;">再想想</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--弹出返回提示层-->
    <div class="modal fade" id="showReturnRes">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="alert_msg" style="text-align:center">

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.title = '培训系统';
    //$('select').select2();
</script>
<script type='text/template' id="examListTemplate">
  <tr>
    <td class="t_name">{title}</td>
    <td class="t_cycle">{cycle}</td>
    <td class="t_time">{time}</td>
    <td class="t_score">{score}</td>
    <td class="t_wrong">{wrong}</td>
    <td class="t_status">{status}</td>
    <td class="t_operate">{operate}</td>
  </tr>
</script>
<script type="text/javascript" src="/static/js/lib/jqueryPlugs/jquery.twbsPagination.min.js"></script>
<script type="text/javascript" src="http://xs01.meituan.net/cdn/moment/2.10.6/moment.min.js"></script>
