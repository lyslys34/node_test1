<#include "train_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/train/train_detail.js</content>
<content tag="cssmain">/train/train_edit.css</content>
<div id="main-container">
    <h4 style="padding-left:15px;">考试详情</h4>
    <div class="panel panel-default table-responsive">
		<div class="container">
			<div class="row clearfix">
				<div class="col-md-12 column">
					<div class="row clearfix">
						<div class="col-md-12 column sub_title">
						 	考试基本信息
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-12 column">
							<div class="row clearfix">
								<div class="col-md-2 column">
									名称：
								</div>
								<div class="col-md-10 column">
									<input id='train_title' name="name" type="text" style="width:40%"/>
								</div>
							</div>
              <div class="row clearfix">
								<div class="col-md-2 column train-content">
									App展示文案
								</div>
								<div class="col-md-10 column">
									<input id='train_content' name="content" type="text" style="width:40%"/>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-2 column train-desc">
									备注
								</div>
								<div class="col-md-10 column">
									<input id='train_desc' placeholder="展示后端，可对本次考试进行简要说明" name="desc" type="text" style="width:40%"/>
								</div>
							</div>
							<div class="row clearfix org-type">
								<div class="col-md-2 column">
									组织类型
								</div>
								<div class="col-md-2 column">
									<input id="org_type_crew" checked=true orgtype="1" name="org_type" type="radio"/>
									<label for='org_type_crew'>众包APP</label>
								</div>
								<div class="col-md-2 column">
									<input id="org_type_rider" orgtype="4" name="org_type" type="radio"/>
									<label for='org_type_rider'>骑手APP</label>
								</div>
								<div class="col-md-2 column">
                  <input id="org_type_self_construct" orgtype="2" name="org_type" type="radio"/>
                  <label for='org_type_self_construct'>自建</label>
                </div>
								<div class="col-md-2 column">
									<input id="org_type_affiliate" orgtype="3" name="org_type" type="radio"/>
									<label for='org_type_affiliate'>加盟</label>
								</div>
							</div>
							<div class="row clearfix limit">
								<div class="col-md-2 column">
									是否限制抢单
								</div>
								<div class="col-md-2 column">
									<input id="force_false" checked=true name="grab" type="radio"/>
									<label for='force_false' style='padding-left:30px'>否</label>
								</div>
								<div class="col-md-2 column">
									<input id="force_true" name="grab" type="radio"/>
									<label for='force_true' style='padding-left:30px;'>是</label>
								</div>
							</div>
							<div class="row clearfix apply-role">
                <div class="col-md-2 column">
                  考生角色
                </div>
                <div class="col-md-8 column checkbox-wrapper">
                  <input value="1001" type="checkbox"/>
                  <label>众包骑手</label>
                </div>
              </div>
              <div class="row clearfix exam-time app-item">
                <div class="col-md-2 column">
                  考试周期
                </div>
                <div class="col-md-4 column">
                  <div class="reportrange">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                      <input type="text" /> - <input type="text" /> <b class="caret"></b>
                  </div>
                </div>
              </div>
              <div class="row clearfix exam-time app-item">
                <div class="col-md-2 column">
                  考试时间
                </div>
                <div class="col-md-2 column">
                  <input name="exam-time" type="number" min="0"/><label>分钟</label>
                </div>
              </div>
						</div>
					</div>
				</div>
				<div class="col-md-2 column">
				</div>
			</div>
		</div>
    </div>

    <div class="panel panel-default table-responsive">
		<div class="train-material crew-item container">
			<div class="row clearfix">
				<div class="col-md-12 column">
					<div class="row clearfix">
						<div class="col-md-12 column sub_title">
							培训材料
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-2 column">
						</div>
						<div class="col-md-3 column">
						</div>
						<div class="col-md-7 column hupload-container">

						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-12 column">
							<table class="table table-striped" style="margin-bottom:0px">
								<thead>
				                    <tr>
				                    	<th width="5%"></th>
				                        <th width="5%">编号</th>
				                        <th width="90%">图片</th>
				                    </tr>
				                </thead>
							</table>
							<div class='tbody'>
								<table id="pic-table" class="table table-striped" style="margin-bottom:0">
					                <tbody class="tbody-pic" id="tbody-pic">
					                <#if trainContents?exists && (trainContents?size>0)>
					                <#list trainContents as c>
					                    <tr rel="${c.id}">
					                    	<td width="10%" class="check_box"><input class="singleSelect" type="checkbox" /></td>
					                        <td width="10%" class="t_no">${c_index + 1}</td>
					                        <td width="40%" class="t_pic">
												<a target="_blank" class='t_url' href="${c.url!''}">${c.name !''}</a>
					                        </td>
					                    </tr>
					                </#list>
					                </#if>
					                </tbody>
				            	</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	<div class="panel panel-default table-responsive">
		<div class="container">
			<div class="row clearfix">
				<div class="col-md-12 column">
					<div class="row clearfix">
						<div class="col-md-12 column sub_title">
							考试题目
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-12 column">
						  <p class="other-item exam-grade">满分100分,已出<span class="exam-count">0</span>题,每题<span class="average-grade">0</span>分</p>
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-12 column">
							<table class="table table-striped" style="margin-bottom:0px">
								<thead>
				                    <tr>
				                    	<th width="10%">编号</th>
				                        <th style="text-align:left" width="75%">题目</th>
				                        <th width="15%">题目类型</th>
				                    </tr>
				                </thead>
							</table>
							<div class="tbody">
								<table id="subject_table" class="table table-striped" style="margin-bottom:0px">
					                <tbody id="tbody-dispatcher-list">
					                <#if examSubjects?exists && (examSubjects?size>0)>
					                <#list examSubjects as s>
					                    <tr>
					                        <td width="5%" class="t_no">${s_index + 1}</td>
					                        <td width="50%" class="t_content">${s.name !''}</td>
					                        <td width="15%" class="t_type">${s.type !''}</td>
					                    </tr>
					                </#list>
					                </#if>
					                </tbody>
					            </table>
				        	</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default table-responsive">
		<div class="container">
			<div class="row clearfix">
					<button id='cancel_train' class="btn btn-danger center-block bu-w" style="width:125px">返回考试管理</button>
			</div>
		</div>
	</div>

<script>
    document.title = '新建培训流程';
    //$('select').select2();
</script>
<script type="text/javascript" src="http://xs01.meituan.net/cdn/moment/2.10.6/moment.min.js"></script>
<script type="text/javascript" src="http://xs01.meituan.net/cdn/daterangepicker/1.3.21/daterangepicker.js"></script>
