<#include "train_config.ftl">
<#include "../widgets/sidebar.ftl">
<content tag="javascript">/train/train.js</content>
<content tag="cssmain">/train/train.css</content>
<div id="main-container" class="container">
    <ul class="exam-nav">
      <li class="active" data-label="exam-manage">考试管理</li>
      <li data-label="exam-pager-manage">成绩管理</li>
    </ul>
    <div class="exam-manage page-container">
      <div class="panel exam-create panel-default">
          <form class="no-margin" id="formValidate" data-validate="parsley" novalidate="" action="/rider/getBlacklistList" method="get" style="padding-bottom:0px;">
              <div class="panel-body">
                  <div class="row" style="margin-bottom:10px">
                      <input type="button" value="添加试卷" id="create" class="btn btn-danger js_submitbtn" style="float:left;margin-left:15px;"/>
                  </div>
              </div>
          </form>
      </div>

      <p class="data-count">共<span class="train-count"></span>条记录</p>
      <div class="panel panel-default table-responsive">
          <table class="table table-striped">
              <thead>
                  <tr>
                      <th>名称</th>
                      <th>App展示文案</th>
                      <th>备注</th>
                      <th>应用组织</th>
                      <th>创建时间</th>
                      <th>操作人</th>
                      <th>操作</th>
                  </tr>
              </thead>
              <tbody class="tbody-dispatcher-list">

              </tbody>
          </table>
          <div class="pager"></div>
      </div>
    </div>
    <div class="exam-pager-manage page-container">
      <div class="search exam-pager-manage-search">
          <input class="input-small" name="content" placeholder="卷名" type="text"/>
          <button class="btn btn-success search-button">查询</button>
      </div>
      <p class="data-count">共<span class="train-count"></span>条记录</p>
      <div class="panel panel-default table-responsive">
          <table class="table table-striped">
              <thead>
                  <tr>
                      <th>卷名</th>
                      <th>App展示文案</th>
                      <th>备注</th>
                      <th>应用组织</th>
                      <th>考生角色</th>
                      <th>平均分</th>
                      <th>考生成绩</th>
                      <th>错题Top5</th>
                  </tr>
              </thead>
              <tbody class="tbody-dispatcher-list">

              </tbody>
          </table>
          <div class="pager-wrap exam-pager-manage-pager">
              <div class="pager"></div>
          </div>
      </div>
    </div>


    <#-- 考试成绩 -->
    <div class="modal fade" id="scoreDialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5>考生成绩</h5>
                </div>
                <div class="modal-body">
                    <div class="search">
                        <input class="input-small" name="userName" placeholder="请输入考生姓名" type="text"/>
                        <input class="input-small" name="cityName" placeholder="请输入城市名称" type="text"/>
                        <input class="input-small" name="roleName" placeholder="请选择职位" type="text"/>
                        <button class="btn btn-success search-button">查询</button>
                        <button class="btn btn-success export-button">导出</button>
                    </div>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>姓名</th>
                            <th>城市</th>
                            <th>所在组织</th>
                            <th>职位</th>
                            <th>分数</th>
                            <th>错题数</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div class="pager-wrap">
                        <div class="pager"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <#-- 骑手端考试 -->
    <div class="modal fade" id="appScoreDialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5>考生成绩</h5>
                </div>
                <div class="modal-body">
                    <div class="search">
                        <input class="input-small" name="cityName" placeholder="城市" type="text"/>
                        <select name="orgType">
                            <option value="">全部</option>
                            <option value="1">自建</option>
                            <option value="2">加盟</option>
                        </select>
                        <select name="status">
                            <option value="">全部</option>
                            <option value="0">未考试</option>
                            <option value="1">未通过</option>
                            <option value="2">通过</option>
                        </select>
                        <button class="btn btn-success search-button">查询</button>
                        <button class="btn btn-success export-button">导出</button>
                    </div>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>姓名</th>
                            <th>手机号</th>
                            <th>城市</th>
                            <th>所在组织</th>
                            <th>组织类型</th>
                            <th>成绩</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div class="pager-wrap">
                        <div class="pager"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <#-- 弹出确认层 -->
    <div class="modal fade" id="showConfirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 id="showConfirmTitle"></h4>
                    <div class="modal-footer">
                        <input type="submit" value="确定" class="btn btn-info btn-sm" id="confirm" style="width:60px;"/>
                        <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" style="width:60px;">再想想</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <#-- 错题数 -->
    <div class="modal fade" id="wrongDialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5>错题Top5</h5>
                </div>
                <div class="modal-body" id="alert_msg" style="text-align:center">
                    <div class="search">
                        <input class="input-small" placeholder="请输入城市名称" type="text"/>
                        <button class="btn btn-success search-button">查询</button>
                    </div>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>城市</th>
                            <th>题目</th>
                            <th>答错</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div class="pager-wrap">
                        <div class="pager"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form method="post" id="params-form">
	<input type="hidden" name="trainId" id="trainId" value="">
</form>
<script>
    document.title = '考试系统';
</script>
<script type='text/template' id="examListTemplate">
  <tr>
    <td class="t_name">{title}</td>
    <td class="t_content">{content}</td>
    <td class="t_desc">{desc}</td>
    <td class="t_org">{org}</td>
    <td class="t_time">{time}</td>
    <td class="t_op_user">{operator}</td>
    <td class="t_op">
      <input type="button" value="{modifyText}" trainid="{trainId}" class="btn btn-primary btn-warning js_submitbtn modify">
      <input type="button" value="&nbsp;删&nbsp;除&nbsp;" trainid="{trainId}" class="btn btn-primary btn-danger js_submitbtn del">
      {button}
    </td>
  </tr>
</script>
<script type='text/template' id="examScoreTemplate">
  <tr>
    <td class="t_name">{title}</td>
    <td class="t_content">{content}</td>
    <td class="t_desc">{desc}</td>
    <td class="t_org">{org}</td>
    <td class="t_role">{role}</td>
    <td class="t_avg">{avg}</td>
    <td class="t_score">{score}</td>
    <td class="t_wrong">{wrong}</td>
  </tr>
</script>
<script type='text/template' id="studentScoreTemplate">
    <tr>
        <td class="t_name">{name}</td>
        <td class="t_city">{city}</td>
        <td class="t_org">{org}</td>
        <td class="t_role">{role}</td>
        <td class="t_score">{score}</td>
        <td class="t_wrong">{wrong}</td>
    </tr>
</script>
<script type='text/template' id="appScoreTemplate">
    <tr>
        <td class="t_name">{name}</td>
        <td class="t_phone">{phone}</td>
        <td class="t_city">{city}</td>
        <td class="t_org">{org}</td>
        <td class="t_role">{role}</td>
        <td class="t_score">{score}</td>
    </tr>
</script>
<script type='text/template' id="wrongTemplate">
    <tr>
        <td class="t_city">{city}</td>
        <td class="t_subject">{subject}</td>
        <td class="t_wrong">{wrong}</td>
    </tr>
</script>
<script type="text/javascript" src="/static/js/lib/jqueryPlugs/jquery.twbsPagination.min.js"></script>
