<#include "staff_config.ftl">
    <#include "../widgets/sidebar.ftl">
        <content tag="javascript">/riderstate/check.js</content>
        <content tag="cssmain">/delivery.css</content>

        <div id="wrapper">
            <!--#include virtual="/static/demos/components/top-nav.html"-->
            <!--#include virtual="/static/demos/components/sidebar.html"-->

            <div id="main-container">
                <div class="padding-md">

                    <div class="panel panel-default package-detail">
                        <div class="panel-heading clearfix">
                            <div class="col-md-6">
                                <div class="row f-row">
                                    <span class="fl strong">人员编号：</span>
                                    <span class="fl">${staff.userId!''}</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row f-row">
                                    <span class="fl strong">状态：</span>
                                    <span class="fl"><#if staff.checked==1>审核通过<#elseif staff.checked ==2>审核未通过<#else>待审核</#if>
                                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default package-detail">
                            <div class="panel-heading">
                                <span>基本信息</span>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <div class="row f-row">
                                        <span class="fl strong">姓名：</span>
                                        <span class="fl">${staff.name!''}</span>
                                    </div>
                                    <div class="row f-row">
                                        <span class="fl strong">身份证号：</span>
                                        <span class="fl">${staff.cardId!''}</span>
                                    </div>
                                    <div class="row f-row">
                                        <span class="fl strong">申请时间：</span>
                                        <span class="fl">${staff.ctime!''}</span>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="row f-row">
                                        <span class="fl strong">电话:</span>
                                        <span class="fl">${staff.phone!''}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="panel panel-default package-detail">
                        <div class="panel-heading">
                            <span>身份证信息</span>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-6">
                                <div class="row f-row">
                                    <span class="fl strong">手持身份证照</span>
                                </div>
                                <div class="row f-row">
                                    <span class="fl strong"><img src="${staff.selfPicPath!''}"></span>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="row f-row">
                                    <span class="fl strong">身份证正面照</span>
                                </div>
                                <div class="row f-row">
                                    <span class="fl strong"><img src="${staff.frontPicPath!''}"></span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default package-detail">
                        <div class="panel-heading">
                            <span>认证信息</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="fl strong">公安部认证：&nbsp;&nbsp;&nbsp;&nbsp;认证通过</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default package-detail">
                        <div class="panel-heading">
                            <span>审核记录</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>审核人</th>
                                            <th>审核结果</th>
                                            <th>IP地址</th>
                                            <th>审核时间</th>
                                            <th>审核备注</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <#if staff.checkRecordInfos??>
                                        <#list staff.checkRecordInfos as checkRecord >
                                            <tr>
                                                <td>${checkRecord.checkPerson!''}</td>
                                                <td>${checkRecord.checkResult!''}</td>
                                                <td>${checkRecord.ipAdress!''}</td>
                                                <td>${checkRecord.checkDate!''}</td>
                                                <td>${checkRecord.remark!''}</td>
                                            </tr>
                                        </#list>
                                        </#if>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                </div>
                <div align="center"><button id="approve" data="{id:${staff.recordId},staffId:${staff.userId},status:1}" class="btn btn-default" >审核通过</button>
                    <button id="disagree" class="btn btn-default" data="{id:${staff.recordId},staffId:${staff.userId},status:2}">审核不通过</button></div>
            </div>
        </div>
        </div>
        <script>
            document.title = '待审核配送员详情';
        </script>
