<#include "staff_config.ftl">
    <#include "../widgets/sidebar.ftl">

        <div id="main-container">

            <div class="panel panel-default table-responsive">
                <table class="table table-striped" id="responsiveTable">
                    <thead>
                    <tr>
                        <th>人员编号</th>
                        <th>姓名</th>
                        <th>身份证号</th>
                        <th>电话</th>
                        <th>状态</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#--循环-->
                        <#if userList??>
                        <#list userList as user>
                            <tr>
                                <td><a href="/rider/checkDetail/${user.userId!'-1'}">${user.userId!'-1'}</a></td>
                                <td>${user.name!''}</td>
                                <td>${user.cardId!''}</td>
                                <td>${user.phone!''}</td>
                                <td><#if user.checked==1>审核通过<#elseif user.checked ==2>审核未通过<#else>待审核</#if></td>
                            </tr>
                        </#list>
                        </#if>
                    </tbody>
                </table>

                <#import "../page/pager.ftl" as q>
                    <#if recordCount??>
                        <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/rider/checklist"/>
                    </#if>
            </div>
        </div>
        <script>document.title = '配送员审核';</script>