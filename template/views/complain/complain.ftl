<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<link rel="stylesheet" href="http://xs01.meituan.net/banma_finance_admin/5697519a/css/page/statistics/daily_data.css" />
<content tag="javascript">/complain/complain.js</content>
<content tag="cssmain">/complain/complain.css</content>
<title>配送员投诉</title>

<div class="loading-wrap" style="display: none;"><p></p></div>
<div id="main-container">
    <div class="modal fade" id="reply_modal" role="dialog">
        
    </div>
    <div style="width: 100%;height: 2px;background: #58B7B1;padding-top:0px"></div>
    <div class="container-fluid search-wrap">
        <form class="form-inline search-form" role="form">
        <div class="row">
                <div class="form-group col-md-3">
                    <label for="">配送员</label>
                    <input type="text" class="form-control" name="riderInfo" placeholder="请输入骑手姓名或手机号">
                </div>
                <div class="form-group col-md-3 text-center">
                    <label for="">订单号</label>
                    <input type="text" class="form-control" name="platformOrderId" placeholder="请输入订单号">
                </div>
                <div class="form-group col-md-3 text-center">
                    <label for="">商家名称</label>
                    <input type="text" class="form-control" name="poiInfo" placeholder="请输入商家名称或ID">
                </div>
                <div class="form-group col-md-3 text-right">
                    <label for="">是否回复</label>
                    <select class="form-control" name="replyed">
                        <option value="2">待回复</option>
                        <option value="-1">全部</option>
                        <option value="1">已回复</option>
                    </select>
                </div>
        </div>
        <div class="row">
                <div class="form-group col-md-3">
                    <label for="">开始时间</label>
                    <input type="text" class="form-control J-datepicker js_date_start" name="startTime" readonly="readonly">
                </div>
                <div class="form-group col-md-3  text-center">
                    <label for="">结束时间</label>
                    <input type="text" class="form-control J-datepicker js_date_end" name="endTime" readonly>
                </div>
                <div class="form-group col-md-3  text-center">
                    <label for="">订单状态</label>
                    <select class="form-control" name="waybillStatus">
                        <option value="-1">全部</option>
                        <option value="99">已取消</option>
                        <!-- <option value="">审核中</option> -->
                        <option value="50">已完成</option>
                        <!-- <option value="">审核不通过</option> -->
                        <!-- <option value="">驳回</option> -->
                        <!-- <option value="">仅发放垫付款</option> -->
                    </select>
                </div>
                <div class="form-group col-md-3  text-right">
                    <label for="">投诉类型</label>
                    <select class="form-control" name="complainType">
                        <option value="-1">全部</option>
                        <option value="1">顾客修改配送地址/时间</option>
                        <option value="2">商家未沟通取消订单</option>
                        <option value="4">商家出餐慢</option>
                        <option value="5">商家定位不准确</option>
                        <option value="6">顾客定位不准确</option>
                        <option value="0">其他</option>
                    </select>
                </div>
        </div>
        </form>
        <div class="row">
            <button type="button" class="btn btn-default btn-search pull-right">查询</button>
        </div>
    </div>
    <div class="container-fluid search-result-wrap">
        <div class="panel panel-default">
            <table class="table table-striped table-condensed">
                <thead>
                    <tr data-waybillId="" data-complainId="">
                        <th style="width:22px;">
                            <input class="checkAll" type="checkbox">
                        </th>
                        <th style="width:10%;">订单号</th>
                        <th style="width:20%;">投诉信息</th>
                        <th style="width:10%;">商家名称</th>
                        <th style="width:20%;">投诉内容</th>
                        <th style="width:10%;">订单状态</th>
                        <th style="width:30%;">操作</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="search-result-info"></div>
            <div class="panel-footer clearfix">
                <button type="button" class="btn btn-main btn-replay-batch pull-left" data-toggle="modal" disabled>批量回复</button>
                <div class="pagination-wrap">
                    <ul id="pagination" class="pagination pagination-xs m-top-none pull-right"></ul>
                </div>
            </div>
        </div>
    </div>
    <script src="http://peisong.meituan.com/static/js/lib/jquery.twbsPagination.min.js"></script>
    <script src="http://xs01.meituan.net/cdn/bootstrap/3.3.6/bootstrap.min.js"></script>
    <script src="http://xs01.meituan.net/cdn/underscore/1.8.3/underscore-min.js"></script>
