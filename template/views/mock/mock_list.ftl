<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<#setting datetime_format="yyyy-MM-dd HH:mm:ss"/>

<content tag="javascript">/mock/mock_list.js</content>
<content tag="cssmain">/mock/mock_list.css</content>

<div id="main-container">
    <div id="alert-error" class="alert alert-danger <#if !RequestParameters.msg??>hide</#if>" role="alert">${RequestParameters.msg!''}</div>
    <form class="form-inline" id="fm" method="get" action="/mock/mock_list">
        <div class="row">
        <div class="col-md-4">
        <div class="form-group">
            <label>操作时间</label>
            <input type="text" class="form-control input-sm time-input date-picker J-datepicker " id="beginTime" name="beginTime" value="${beginTime!''}">至<input type="text" class="form-control input-sm time-input date-picker J-datepicker " id="endTime" name="endTime" value="${endTime!''}" >
          </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>操作人</label>
                <input type="text" class="form-control input-sm j-mis-id" id="opMisId"  placeholder="请输入员工mis账号" autocomplete="off" value="${RequestParameters.opMisId!''}" >
                <div class="mis-search" style="padding-left: 53px;">
                    <input type="hidden" name="opMisId" id="op-mis-id" value="${RequestParameters.mockMisId!''}">
                    <ul class="mis-search-ul">
                    </ul>
                 </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>被模拟人</label>
                <input type="text" class="form-control input-sm j-mis-id" id="mockMisId"  placeholder="请输入员工mis账号" autocomplete="off" value="${RequestParameters.mockMisId!''}" >
                <div class="mis-search" style="padding-left: 66px;">
                    <input type="hidden"  name="mockMisId" id="mock-mis-id" value="${RequestParameters.mockMisId!''}">
                    <ul class="mis-search-ul">
                    </ul>
                 </div>
            </div>
        </div>
        <input type="submit" value="查询" class="btn btn-success js_submitbtn input-sm" id="beginMocking" />
        </div>
    </form>

    <hr/>
    <#if (page)?exists && (page.bmMockUserViewList)?exists && (page.bmMockUserViewList?size > 0) >
    <table class="table table-bordered table-striped" align="center" style="background-color:#fff">
        <thead>
            <tr style="">
                <th>开始时间</th>
                <th>结束时间</th>
                <th>操作人Mis Id</th>
                <th>被模拟人Mis Id</th>
                <th>被模拟人手机号</th>
                <th>被模拟人组织</th>
            </tr>
        </thead>
        <tbody>
            <#list page.bmMockUserViewList as m>
            <tr>
                <td>${(m.ctime *1000)?number?number_to_datetime}</td>
                <td><#if m.status=0>${(m.utime *1000)?number?number_to_datetime}</#if></td>
                <td>${m.currMisId!''}</td>
                <td>${m.mockMisId!''}</td>
                <td>${m.mockPhone!''}</td>
                <td>${m.mockOrgName!''}</td>
            </tr>
            </#list>
        </tbody>
    </table>
    <#if (page)?exists && (page.total)?exists && (page.pageSize)?exists && (page.pageNum)?exists && page.total gt 0 && page.pageSize gt 0 && page.pageNum gt 0  >
      <#if page.total??>
        <#import "/page/pager.ftl" as q>
        <@q.pager pageNo=page.pageNum pageSize=page.pageSize recordCount=page.total toURL="/mock/mock_list"/>
      </#if>
    </#if>
   <#else>
    <div style="clear:both;">暂无数据！</div>
   </#if>
</div>
<script>
    document.title = '模拟用户权限';
</script>