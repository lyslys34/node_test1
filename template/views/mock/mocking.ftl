<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<#setting datetime_format="yyyy-MM-dd HH:mm:ss"/>

<content tag="javascript">/mock/mocking.js</content>
<content tag="cssmain">/mock/mocking.css</content>

<div id="main-container">
    <div id="alert-error" class="alert alert-danger <#if !RequestParameters.msg??>hide</#if>" role="alert">${RequestParameters.msg!''}</div>
    <form class="header-form" id="fm" method="get" action="/mock/begin">
        <div class="col-md-3">
        <div class="form-group">
            <input type="text" class="form-control" id="mockMisId" autocomplete="off" value="${RequestParameters.mockMisId!''}" />
            <div class="mis-search">
                <input type="hidden" name="mockMisId" id="mis-id" value="${RequestParameters.mockMisId!''}" >
                <ul class="mis-search-ul">
                </ul>
             </div>
        </div>
        </div>
        <div class="col-md-2">
        <div class="form-group searchItem">
            <input type="submit" value="开始模拟" class="btn btn-success js_submitbtn" id="beginMocking" />
            <a class="btn btn-danger js_submitbtn" href="/mock/mock_list" target="_blank">查看记录</a>
        </div>
        </div>
        <div class="col-md-5">
        <div class="form-group" style="margin-top:10px;">
            提示：模拟A端用户，请输入员工mis账号。模拟P端用户，请输入用户手机号
        </div>
        </div>
    </form>

    <hr/>
    <#if (list)?exists && (list?size > 0) >
    <table class="table table-bordered table-striped" align="center" style="background-color:#fff">
        <thead>
            <tr style="">
                <th>正在登录用户</th>
                <th>被模拟人</th>
                <th>开始时间</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <#list list as m>
            <tr>
                <td>${m.currMisId}</td>
                <td>${m.mockMisId}</td>
                <td>${(m.ctime *1000)?number?number_to_datetime}</td>
                <td>
                    <a data-toggle="tooltip" title="" data-placement="top" href="/mock/stopOthers?id=${m.id}" name="edit" data-original-title="停止模拟">
                        <i class="fa fa-ban fa-lg opration-icon"></i>
                    </a>
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
    <#if (page)?exists && (page.total)?exists && (page.pageSize)?exists && (page.pageNum)?exists && page.total gt 0 && page.pageSize gt 0 && page.pageNum gt 0  >
      <#if page.total??>
        <#import "/page/pager.ftl" as q>
        <@q.pager pageNo=page.pageNum pageSize=page.pageSize recordCount=page.total toURL="/feedback/list"/>
      </#if>
    </#if>
   <#else>
    <div style="clear:both;">暂无数据！</div>
   </#if>
</div>
<script>
    document.title = '模拟用户权限';
</script>