<#include "rider_faq_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/faq/rider_faq_type.js</content>
<content tag="cssmain">/faq/rider_faq_type.css</content>

<style type="text/css">
    #main-container{
    padding: 10px 20px;
    }
    .col-md-4{width: 260px;}
</style>

<div id="main-container">
    <div class="panel panel-default">
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post"
              action="/faq/faqTypeList">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">展示平台</label>
                            <select class="form-control input-sm" id="channelType" name="channelType">
                                <option value="0"
                                <#if channelType == 0 > selected="selected"</#if>
                                >全部</option>
                                <option value="2"
                                <#if channelType == 2 > selected="selected"</#if>
                                >烽火台</option>
                                <option value="1"
                                <#if channelType == 1 > selected="selected"</#if>
                                >美团骑手</option>
                                <option value="3"
                                <#if channelType == 3 > selected="selected"</#if>
                                >众包骑手</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">类型</label>
                            <select class="form-control input-sm" id="typeCode" name="typeCode">
                                <option value="0"
                                <#if typeCode == 0 > selected="selected"</#if>
                                >全部</option>
                                <option value="1"
                                <#if typeCode == 1 > selected="selected"</#if>
                                >常见问题</option>
                                <option value="2"
                                <#if typeCode == 2 > selected="selected"</#if>
                                >系统操作指南</option>
                            </select>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">状态</label>
                            <select class="form-control input-sm" id="status" name="status">
                                <option value="-1"
                                <#if status == -1 > selected="selected"</#if>
                                >全部</option>
                                <option value="0"
                                <#if status == 0 > selected="selected"</#if>
                                >草稿</option>
                                <option value="1"
                                <#if status == 1 > selected="selected"</#if>
                                >发布</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-left">
                总计${recordCount!'0'}条记录
                &nbsp;&nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success">查询</button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="javascript:;" class="btn btn-success addFaqType">添加新问题分类</a>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>常见问题分类</th>
                <th>类型</th>
                <th>子问题个数</th>
                <th>展示平台</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
                <#list faqTypeList as faqType>
                    <tr>
                        <td>${faqType.typeDesc!''}</td>
                        <td>${faqType.typeCodeDesc!''}</td>
                        <td>${faqType.subFaqCount!''}</td>
                        <td>${faqType.channelType!''}</td>
                        <td>
                            <#if faqType.status?exists && faqType.status == 1>发布
                                <#elseif faqType.status?exists && faqType.status == 0>草稿
                            </#if>
                        </td>
                        <td typeDesc="${faqType.typeDesc!'0'}" typeId="${faqType.id!'0'} ">
                            <a data-toggle="tooltip" title="详情" data-placement="top"
                               href="/faq/faqInfoList?faqTypeId=${faqType.id!'0'}" class="js_info_faq_type" target="_blank">
                                <i class="fa fa-info-circle fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                            <a data-toggle="tooltip" title="修改" data-placement="top"
                               href="javascript:void(0)" class="js_update_faq_type">
                                <i class="fa fa-edit fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                            <a data-toggle="tooltip" title="删除" data-placement="top"
                               href="javascript:void(0)" class="js_del_faq_type">
                                <i class="fa fa-trash-o fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                        </td>
                    </tr>
                </#list>
            </tbody>
        </table>

        <#import "../page/pager.ftl" as q>
            <#if recordCount??>
                <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/faq/faqTypeList"/>
            </#if>
    </div>

    <!-- 新建问题分类弹框 -->
    <div class="modal fade" id="saveFaqTypeBox">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" id="faqTypeTitle">

                </div>
                <div class="modal-body">
                    <table style="width: 100%;font-size: 15px;line-height: 3;margin-left: 20px;">
                        <tr >
                            <td>展示平台</td>
                            <td>
                                <select class="form-control input-sm" id="channelTypeOfBox" style="width:200px;">
                                    <option value="2">烽火台</option>
                                    <option value="1">美团骑手</option>
                                    <option value="3">众包骑手</option>
                                </select>
                            </td>
                        </tr>
                         <tr >
                            <td>类型</td>
                            <td>
                                <select class="form-control input-sm" id="typeCodeOfBox" style="width:200px;">
                                    <option value="1">常见问题</option>
                                    <option value="2">系统操作指南</option>
                                </select>
                            </td>
                        </tr>
                        <tr >
                            <td>分类名称</td>
                            <td><input type="text"  id="typeDescOfBox" class="form-control input-sm" placeholder="内容不可为空，不超过30个字符" maxlength="30" style="width:200px;"/></td>
                        </tr>
                        <tr >
                            <input type="hidden" name="diaplayOrder" value="-1" id="diaplayOrder"/>
                            <td style="vertical-align:initial">排序位置</td>
                            <td id="sortOrder">

                            </td>
                        </tr>
                        <tr >
                            <td>发布状态</td>
                            <td>
                                <label class="radio-inline">
                                    <input type="radio" name="status" value="0" checked >
                                    草稿
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="status" value="1">
                                    发布
                                </label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="submit" id="confirmSave" value="确定" class="btn btn-danger btn-sm" />
                    <button class="btn btn-success btn-sm" id="cancel">取消</button>

                </div>

            </div>
        </div>
    </div>

    <!-- 确认删除弹出框 -->
    <div class="modal fade" id="delFaqTypeBox">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>删除问题分类</h4>
                </div>
                <div class="modal-body" style="text-align: center;">
                    <h4 id="showTitle"></h4>
                    <p style="font-size:16px;">确认后该类型下所有子问题都将被删除!</p>
                </div>
                <div class="modal-footer">
                    <input type="submit" id="confirmDel" value="确定" class="btn btn-danger btn-sm" />
                    <button class="btn btn-success btn-sm" id="cancelDel">取消</button>

                </div>

            </div>
        </div>
    </div>

</div>
<script>
    document.title = '骑手帮助中心';
    $('select').select2();
</script>