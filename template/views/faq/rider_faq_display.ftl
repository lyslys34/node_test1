<#include "rider_faq_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="cssmain">/faq/rider_faq_display.css</content>
<content tag="javascript">/faq/rider_faq_display.js</content>

<style type="text/css">
	#main-container{
		padding: 10px 20px;
	}
	.col-md-4{width: 260px;}
</style>

<div id="main-container">
	<div class="panel panel-default">
		<div class="panel-body">
			<div id="container_wrapper">
				<div class="lbox fixed">
					<h5 class="control-label paddingTop leftHead">${typeCodeDesc!''}</h5>
					<#if faqTypeList?? && (faqTypeList?size>0)>
					<ul id="faq_nav" class="paddingLeft liNavList">
						<#list faqTypeList as faqType>
						<#if faqInfoList?? && (faqInfoList?size>=faqType_index) && (faqInfoList[faqType_index]?size>0) >
						<li class="liNav paddingLeft">
							<a href="javascript:;" id="faq-${faqType.id}-a" data-link="#faq-${faqType.id}" class="faq_link ${(faqType_index==0)?string('on','')}">${faqType.typeDesc}</a>
						</li>
					</#if>
					</#list>
					</ul>
				</#if>
			</div>

			<div class="rbox" id="rbox">
				<#if faqTypeList?? && (faqTypeList?size>0)>
					<#list faqTypeList as faqType>
						<#if faqInfoList?? && (faqInfoList?size>=faqType_index) && (faqInfoList[faqType_index]?size>0) >
						<#-- <div class="collapse navbar-collapse"> -->
						<div class="faq_group" id="faq-${faqType.id}">
							<span class="faqType">${faqType.typeDesc}</span>
							<ul id="responsiveTable" class="ul-striped">
								<#list faqInfoList[faqType_index] as faqInfo>
                                    <li class="faqTitleTr" style="border:5px">
										<span class="faqTitleSpan">
                                            <a data-toggle="collapse" class="collapse-trigger hoverNoColor"
                                               href="#collapse${faqInfo.id!'0'}">
											${faqInfo_index +1 }.&nbsp;&nbsp;&nbsp;&nbsp;${faqInfo.question}
                                                <span id="arrowDown" class="glyphicon glyphicon-chevron-down" style="float:right;vertical-align:middle;margin-top:12px;padding-right:25px;" aria-hidden="true"></span>
											</a>
										</span>
                                        <#--<span id="arrowDown" href="#collapse${faqInfo.id!'0'}" class="glyphicon glyphicon-chevron-down" style="float:right;vertical-align:middle;margin-top:12px;padding-right:25px;" aria-hidden="true"></span>-->

									</li>
                                    <li id="collapse${faqInfo.id!'0'}" class="faqContentTr panel-collapse collapse">
                                        <span colspan="5" class="richtext">
										${faqInfo.answer!''}
                                        </span>
                                    </li>
								</#list>
                            </ul>
							<div class="faqTypeSplit"></div>

						</div>
						</#if>
					</#list>
				</#if>
			</div>
		</div>
	</div>
</div>
</div>
<script>
	document.title = '骑手帮助中心';
</script>