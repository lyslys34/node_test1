<#include "rider_faq_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/faq/rider_faq_info.js</content>

<content tag="cssmain">/faq/rider_faq_info.css</content>

<style type="text/css">
    #main-container{
    padding: 10px 20px;
    }
    .col-md-4{width: 260px;}
</style>

<div id="main-container">
    <h4>${typeDesc!''}</h4>
    <div class="panel panel-default">
        <input type="hidden" value="${channelType!'0'}" name="channelType" id="channelType" />
        <form class="no-margin" id="formValidate1" data-validate="parsley" novalidate="" method="post"
              action="/faq/faqInfoList">
            <input type="hidden" value="${faqTypeId!'0'}" name="faqTypeId" id="originFaqTypeId" />
            <input type="hidden" value="${faqTypeId!'0'}" name="tempFaqTypeId" id="tempFaqTypeId" />

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4 <#if channelType == 3>hidden</#if>">
                        <div class="form-group">
                            <label class="control-label">组织</label>
                            <select class="form-control input-sm" id="orgType" name="orgType">
                                <option value="0"
                                <#if orgType == 0 > selected="selected"</#if>
                                >全部</option>
                                <option value="1"
                                <#if orgType == 1 > selected="selected"</#if>
                                >自建</option>
                                <option value="2"
                                <#if orgType == 2 > selected="selected"</#if>
                                >加盟</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 <#if channelType == 2>hidden</#if>">
                        <div class="form-group">
                            <label class="control-label">操作系统</label>
                            <select class="form-control input-sm" id="osType" name="osType">
                                <option value="0"
                                <#if osType == 0 > selected="selected"</#if>
                                >全部</option>
                                <option value="1"
                                <#if osType == 1 > selected="selected"</#if>
                                >Android</option>
                                <option value="2"
                                <#if osType == 2 > selected="selected"</#if>
                                >IOS</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">状态</label>
                            <select class="form-control input-sm" id="status" name="status">
                                <option value="-1"
                                <#if status == -1 > selected="selected"</#if>
                                >全部</option>
                                <option value="0"
                                <#if status == 0 > selected="selected"</#if>
                                >草稿</option>
                                <option value="1"
                                <#if status == 1 > selected="selected"</#if>
                                >发布</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-left">
                总计${recordCount!'0'}条记录
                &nbsp;&nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success">查询</button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="/faq/faqTypeAdd?faqTypeId=${faqTypeId}" class="btn btn-success addFaqInfo" faqTypeDesc="${typeDesc!''}">添加子问题</a>
            </div>
        </form>
    </div>

    <div class="panel panel-default table-responsive">
        <table class="table table-striped" id="responsiveTable">
            <thead>
            <tr>
                <th>${typeDesc!''}</th>
                <#if channelType != 3>
                    <th>可见组织</th>
                </#if>
                <#if channelType != 2>
                    <th>操作系统</th>
                </#if>
                <th>状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#--循环-->
                <#list faqInfoList as faqInfo>
                    <div class="collapse navbar-collapse">
                        <tr>
                            <td class="custA"><a data-toggle="collapse" class="collapse-trigger" data-parent="#responsiveTable" href="#collapse${faqInfo.id!'0'}">Q:${faqInfo.question!''}</a></td>
                            <#if channelType != 3>
                                <td>${faqInfo.orgTypes!''}</td>
                            </#if>
                            <#if channelType != 2>
                                <td>${faqInfo.osTypes!''}</td>
                            </#if>
                            <td>
                                <#if faqInfo.status?exists && faqInfo.status == 1>发布
                                    <#elseif faqInfo.status?exists && faqInfo.status == 0>草稿
                                </#if>
                            </td>
                            <td  faqInfoId="${faqInfo.id!'0'}">
                                <a data-toggle="tooltip" title="修改" data-placement="top"
                                   href="/faq/faqInfoEdit?faqInfoId=${faqInfo.id!'0'}" class="js_update_faq_info">
                                    <i class="fa fa-edit fa-lg opration-icon"></i>
                                </a>&nbsp;&nbsp;
                                <a data-toggle="tooltip" title="删除" data-placement="top"
                                   href="javascript:void(0)" class="js_del_faq_info">
                                    <i class="fa fa-trash-o fa-lg opration-icon"></i>
                                </a>&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr id="collapse${faqInfo.id!'0'}" class="panel-collapse collapse">
                            <td colspan="5" class="richtext">${faqInfo.answer!''}
                                <#if faqInfo.answerExt??>
                                    <span class="answerExt">${faqInfo.answerExt}</span>
                                </#if>
                            </td>
                        </tr>
                    </div>
                </#list>
            </tbody>
        </table>

        <#import "../page/pager.ftl" as q>
            <#if recordCount??>
                <@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/faq/faqInfoList"/>
            </#if>
    </div>

    <!--添加子问题弹出框-->
    <div class="modal fade" id="saveFaqInfoBox">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" id="faqInfoTitle">

                </div>
                <div class="modal-body">
                    <input type="hidden" name="faqInfoId" id="faqInfoId" value="0">
                    <table style="width: 100%;font-size: 15px;line-height: 3;margin-left: 20px;">
                        <tr >
                            <td>问题分类</td>
                            <td id="faqTypeDetail" class="paddingStyle"></td>
                        </tr>
                        <tr>
                            <td>子问题</td>
                            <td><label style="float:left;font-size: 15px;width:20px;margin-top:4px;">Q:</label><input type="text" class="form-control" name="question" id="question" placeholder="内容不可为空，不超过30个字符" maxlength="30" style="width:400px;margin-top:8px;"/></td>
                        </tr>
                        <tr>
                            <td style="vertical-align:initial">问题解答</td>
                            <td><label style="float:left;font-size: 15px;width:20px;">A:</label><textarea class="form-control" rows="5" name="answer" id="answer" placeholder="内容不可为空，不可超过500个字符" style="width:400px;float:left;"></textarea></td>
                        </tr>
                        <tr>
                            <input type="hidden" name="diaplayOrder" value="-1" id="diaplayOrder"/>
                            <td style="vertical-align:initial">排序位置</td>
                            <td class="resetDisplayOrder paddingStyle">

                            </td>
                        </tr>
                        <tr>
                            <td>可见组织</td>
                            <td class="paddingStyle">
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" value="1" name="orgTypeCheckbox"> 自建
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox2" value="2" name="orgTypeCheckbox"> 加盟
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>操作系统</td>
                            <td class="paddingStyle">
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox3" value="1" name="osTypeCheckbox"> Android
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox4" value="2" name="osTypeCheckbox"> IOS
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>发布状态</td>
                            <td class="paddingStyle">
                                <label class="radio-inline">
                                    <input type="radio" name="status" value="0" checked >
                                    草稿
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="status" value="1">
                                    发布
                                </label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm" id="saveCancel">取消</button>
                    <input type="submit" id="saveConfirm" value="确认提交" class="btn btn-danger btn-sm" />
                </div>

            </div>
        </div>
    </div>

    <!-- 确认删除弹出框 -->
    <div class="modal fade" id="delFaqInfoBox">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" >
                    <h4>删除子问题</h4>
                </div>
                <div class="modal-body" style="text-align: center;">
                    <h4>确认删除该项子问题及解答?</h4>
                    <p style="font-size:16px;">确认后该项子问题及解答将同步从其展示平台删除!</p>
                </div>
                <div class="modal-footer">
                    <input type="submit" id="confirmDel" value="确定" class="btn btn-danger btn-sm" />
                    <button class="btn btn-success btn-sm" id="cancelDel">取消</button>
                </div>

            </div>
        </div>
    </div>

    <!-- 增加图片浏览功能 -->
    <div class='img-mask' id='imgMask'>
        <div class='img-tool-bar'>
            <span class='close-btn'></span>
        </div>
        <div class='img-container'>
            <div class='img-wrapper'>
            </div>
            <span class='img-mask-prev'></span>
            <span class='img-mask-next'></span>
        </div>
    </div>

</div>
<script>
    document.title = '骑手帮助中心';
    $('select').select2();
    $('.richtext span').each(function() {
        var answerExt = JSON.parse($(this).html());
        $(this).html(answerExt.linkUrl);
    });

</script>