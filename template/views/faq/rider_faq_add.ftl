<#include "rider_faq_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="javascript">/faq/rider_faq_add.js</content>
<content tag="cssmain">/faq/rider_faq_add.css</content>

<link rel="stylesheet" href="/static/css/lib/umeditor.min.css">
<script src="/static/js/lib/umeditor/umeditor.js"></script>
<script src="/static/js/lib/umeditor/umeditor.config.js"></script>


<style type="text/css">
    #main-container {
        padding: 10px 20px;
    }
    .col-md-4 {
        width: 260px;
    }
    .modal-footer .btnStyle {
        font-size:14 px;
        margin-right: 20px;
        padding: 5px 10px;
        line-height: 1.5;
        border-radius: 3px;
    }
    .edui-toolbar {
        line-height: 2;
    }
    #editor {
        line-height: 1.4;
    }
</style>

<div id="main-container">
    <!--添加子问题弹出框-->
    <div id="saveFaqInfoBox">
        <div class="amodal-dialog">
            <div class="amodal-content">
                <div class="amodal-header" id="faqInfoTitle">

                </div>
                <div class="amodal-body">
                    <input type="hidden" name="originFaqTypeId" id="originFaqTypeId" value=${faqTypeId}>
                    <input type="hidden" name="channelType" id="channelType" value=${channelType}>
                    <table style="width: 100%;font-size: 15px;line-height: 3;margin-left: 20px;">
                        <tr >
                            <td>所属分类</td>
                            <td id="faqTypeDetail" class="paddingStyle">${queryCategory}</td>
                        </tr>
                        <tr class="<#if channelType == 2>hidden</#if>">
                            <td>操作系统</td>
                            <td class="paddingStyle">
                                <label class="checkbox-inline w150">
                                    <input type="checkbox" id="inlineCheckbox3" value="1" name="osTypeCheckbox"> Android
                                </label>
                                <label class="checkbox-inline w150">
                                    <input type="checkbox" id="inlineCheckbox4" value="2" name="osTypeCheckbox"> IOS
                                </label>
                            </td>
                        </tr>
                        <tr class="<#if channelType == 3>hidden</#if>">
                                <td>可见组织</td>
                                <td class="paddingStyle">
                                    <label class="checkbox-inline w150">
                                        <input type="checkbox" id="inlineCheckbox1" value="1" name="orgTypeCheckbox"> 自建
                                    </label>
                                    <label class="checkbox-inline w150">
                                        <input type="checkbox" id="inlineCheckbox2" value="2" name="orgTypeCheckbox"> 加盟
                                    </label>
                                </td>
                        </tr>

                        <tr>
                            <input type="hidden" name="diaplayOrder" value="-1" id="diaplayOrder"/>
                            <td style="vertical-align:initial">排序位置</td>
                            <td class="resetDisplayOrder paddingStyle">
                            </td>
                        </tr>
                        <tr>
                            <td>子问题</td>
                            <td class="paddingStyle">
                                <input type="text" class="form-control" name="question" id="question" placeholder="内容不可为空，不超过30个字符" maxlength="30" style="width:600px;margin-top:8px;"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align:initial">问题解答</td>
                            <td class="paddingStyle paddingTop">
                                <div class="choice-type">
                                    <input type="radio" class="active" name="contentType" checked/><label>编辑内容</label>
                                    <input type="radio" name="contentType"/><label>插入外部链接</label>
                                </div>
                                <input type="text" class="form-control" id="answerExt" name="answerExt" />
                                <div id="editor" <#if message?? && message.body_type == 1>class="hide"</#if>>
                                    <textarea class="form-control hidden" rows="5" name="answer" id="answer" placeholder="内容不可为空，不可超过500个字符" style="width:600px;float:left;"></textarea>
                                <#-- umeditor -->
                                    <script id="umeditor" name="content" text="text/plain"></script>
                                <#-- umeditor end -->
                                </div>
                            </td>
                        </tr>


                        <tr>
                            <td class="hidden">发布状态</td>
                            <td class="paddingStyle hidden">
                                <label class="radio-inline hidden">
                                    <input type="radio" name="status" value="0" checked >
                                    草稿
                                </label>
                                <label class="radio-inline hidden">
                                    <input type="radio" name="status" value="1">
                                    发布
                                </label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer" style="text-align: left;">
                    <div style="margin-left:130px">
                        <input type="submit" id="publicConfirm" value="保存并发布" class="btnStyle btn btn-danger"/>
                        <input type="submit" id="draftConfirm" value="保存为草稿" class="btnStyle btn btn-danger"/>
                        <button class="btnStyle btn btn-success" id="saveCancel">取消</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.title = '骑手帮助中心';
    $('select').select2();
</script>