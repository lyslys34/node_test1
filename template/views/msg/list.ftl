<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<#setting datetime_format="yyyy-MM-dd HH:mm:ss"/>
<content tag="cssmain">/msg/list.css</content>

<div id="main-container">
    <div class="msg-container">
    <div class="msg-nav">
        <ul class="nav nav-tabs">
            <li role="presentation" <#if !RequestParameters?? || !RequestParameters.type?? || RequestParameters.type == "1">class="active"</#if>><a href="/msg/r/list?type=1">公告通知</a>
                <#if (beaconMessageUnReadCount>0)>
                    <span class='unread-count'>${beaconMessageUnReadCount}</span>
                </#if>
            </li>
            <li role="presentation" <#if RequestParameters?? && RequestParameters.type?? && RequestParameters.type == "2">class="active"</#if>><a href="/msg/r/list?type=2">系统消息</a>
                <#if (systemMessageUnReadCount>0)>
                    <span class='unread-count'>${systemMessageUnReadCount}</span>
                </#if>
            </li>
            <div class="form-group col-md-2 nav-option-select">
                <select class="form-control input-sm" name="">
                    <option value="-1">请选择分类名称</option>
                    <#if RequestParameters?? && RequestParameters.type?? && RequestParameters.type == "2">
                        <#list systemMessageTypes?keys as id>
                            <option value='${id}' <#if RequestParameters?? && RequestParameters.subType?? && RequestParameters.subType==id>selected</#if> >${systemMessageTypes[id]}</option>
                        </#list>
                    <#else>
                        <#list beaconMessageTypes?keys as id>
                            <option value='${id}' <#if RequestParameters?? && RequestParameters.subType?? && RequestParameters.subType==id>selected</#if>>${beaconMessageTypes[id]}</option>
                        </#list>
                    </#if>
                </select>
            </div>
        </ul>
    </div>

    <#if (page)?exists && (page.bmUserMsgViewList)?exists && (page.bmUserMsgViewList?size > 0) >
    <ul class='msg-list-wrap'>
        <#list page.bmUserMsgViewList as msg>
            <li class='msg-item' data-id='${msg.msgId}'>
                <div class='msg-img'>
                    <#if (msg.subType==105)>
                        <img src='/static/imgs/feiyewu.png'>
                        <span class='msg-info'>${beaconMessageTypes[msg.subType?string]!''}</span>
                    </#if>
                    <#if (msg.subType==104)>
                        <img src='/static/imgs/yewu.png'>
                        <span class='msg-info'>${beaconMessageTypes[msg.subType?string]!''}</span>
                    </#if>
                    <#if (msg.subType==103)>
                        <img src='/static/imgs/caiwu.png'>
                        <span class='msg-info'>${beaconMessageTypes[msg.subType?string]!''}</span>
                    </#if>
                    <#if (msg.subType==102)>
                        <img src='/static/imgs/zhengce.png'>
                        <span class='msg-info'>${beaconMessageTypes[msg.subType?string]!''}</span>
                    </#if>
                    <#if (msg.subType==101)>
                        <img src='/static/imgs/qita.png'>
                        <span class='msg-info'>${beaconMessageTypes[msg.subType?string]!''}</span>
                    </#if>
                    <#if (msg.subType==2100)>
                        <img src='/static/imgs/qita.png'>
                        <span class='msg-info'>其他</span>
                    </#if>
                    <#if (msg.type==200)>
                        <img src='/static/imgs/shangjia.png'>
                        <span class='msg-info'>${systemMessageTypes[msg.type?string]!''}</span>
                    </#if>
                    <#if (msg.type==300)>
                        <img src='/static/imgs/jiesuan.png'>
                        <span class='msg-info'>${systemMessageTypes[msg.type?string]!''}</span>
                    </#if>
                    <#if (msg.type==400)>
                        <img src='/static/imgs/renyuan.png'>
                        <span class='msg-info'>人员组织</span>
                    </#if>
                </div>
                <div class='msg-content'>
                    <h5 class='msg-title'>
                        <a href="/msg/r/detail?id=${msg.msgId}" title="${msg.msgTitle !''}"  target="_blank" class="title" <#if msg.isRead==0>data-unread='true'</#if>><#if msg.top gt 0><span class="msg-top"><img src='/static/imgs/zhiding.gif'></span></#if>${msg.msgTitle !''}<#if msg.isRead==0><span class="msg-new"><img src='/static/imgs/new.gif'></span></#if></a>
                    </h5>
                    <div class='op-block'>
                        <div class='t-right'></div>
                        <ul class='op-list'>
                            <#if msg.isRead==0><li class='read'>置为已读</li></#if>
                            <li class='del'>删除</li>
                        </ul>
                    </div>
                    <span class='msg-time'><#if (msg.createTime>0)>${(msg.createTime *1000)?number?number_to_datetime}</#if></span>
                </div>
            </li>
        </#list>
    </ul>
    <!--table class="table" style="margin-top:10px;">
        <thead style="background:#f7f7f7;">
            <tr>
                <td class="" style="width:15%;padding-left:55px;">所属类别</td>
                <td class="" style="padding-left:100px;">通知标题</td>
                <td style="width:30%;padding-left:100px;" >发布时间</td>
            </tr>
        </thead>
        <tbody>
        <#list page.bmUserMsgViewList as msg>
            <tr>
                <#if RequestParameters?? && RequestParameters.type?? && RequestParameters.type == "2">
                <td style="padding-left:55px;">${systemMessageTypes[msg.type?string]!''}</td>
                <#else>
                <td style="padding-left:55px;">${beaconMessageTypes[msg.subType?string]!''}</td>
                </#if>
                <td style="padding-left:100px;"><#if msg.top gt 0><span class="msg-top">置顶</span></#if><#if msg.isRead==0><span class="msg-new">新</span><#else><span class=""></span></#if><a href="/msg/r/detail?id=${msg.msgId}" title="${msg.msgTitle !''}" style="color:#06c;" target="_blank" class="title">${msg.msgTitle !''}</a></td>
                <td style="padding-left:100px;"><#if (msg.createTime>0)>${(msg.createTime *1000)?number?number_to_datetime}</#if></td>
			</tr>
        </#list>
        </tbody>
    </table-->
    <#if (page)?exists && (page.total)?exists && (page.pageSize)?exists && (page.pageNum)?exists && page.total gt 0 && page.pageSize gt 0 && page.pageNum gt 0  >
      <#if page.total??>
        <#import "/page/pager.ftl" as q>
        <@q.pager pageNo=page.pageNum pageSize=page.pageSize recordCount=page.total toURL="/msg/r/list"/>
      </#if>
    </#if>
   <#else>
    <div class='no-msg'>
        <img src='/static/imgs/nomessage.png' />没有新消息哦～
    </div>
   </#if>
   </div>
   <div class='tip-container'>
    <h3 class='tip-title'>通知中心小贴士</h3>
    <p>Q：在这会说什么事儿？<br>
    A：烽火台的新鲜功能，与亲相关的结算事儿，审核事儿，商家事儿，政策财务等等很多很多</p>
    <p>Q：通知消息好多，不需要的怎么办呢？<br>
    A：可以选择右上角的小箭头，进行删除哦~</p>
    <p>Q：累计了很多的未读消息，怎么办呢？<br>
    A：有些消息亲可以直接点击标题进行处理~也可以选择右上角的小箭头，置为已读哦~</p>
   </div>
</div>
<script>
    function getParameter(key){
        var value = window.location.search.match(new RegExp('[?&]'+key+'=([^&]*)(&?)','i'));
        return value?decodeURIComponent(value[1]):'';
    }

    $(".title[data-unread='true']").click(function(){
        $(this).parent().find(".msg-new").text("").removeClass("msg-new");
        $(this).parents('.msg-item').find(".read").remove();
        var $dom = $('.nav-tabs').find('.active').find('.unread-count')
        $dom.html($dom.html()-1);
    });
    $('.nav-option-select').find('select').bind('change',function(){
        var type = getParameter('type')?getParameter('type'):1
        if($(this).val()==-1){
            window.location.href = '/msg/r/list?type='+type;
        }else{
            window.location.href = '/msg/r/list?type='+type+'&subType='+$(this).val();
        }
    })
    $('.t-right').bind('mouseenter',function(){
        $(this).next().show();
    })
    $('.op-block').bind('mouseleave',function(){
        $(this).find('.op-list').hide();
    })
    $('.del').bind('click',function(){
        var $dom = $(this).parents('.msg-item');
        var id = $dom.attr('data-id');
        $.ajax({
            url:'/msg/r/deleteInboxMsg.ajax?id='+id,
            type:'post',
            dataType:'json'
        }).done(function(data){
            if($dom.find('.read').length>0){
                var $unreadDom = $('.nav-tabs').find('.active').find('.unread-count');
                $unreadDom.html($unreadDom.html()-1);
            }
            $dom.remove();
        })
    })
    $('.read').bind('click',function(){
        var $dom = $(this).parents('.msg-item');
        var id = $dom.attr('data-id');
        var $self = $(this);
        $.ajax({
            url:'/msg/r/readMsg.ajax?id='+id,
            type:'post',
            dataType:'json'
        }).done(function(data){
            $dom.find(".msg-new").text("").removeClass("msg-new");
            $dom.find('.op-list').hide();
            var $unreadDom = $('.nav-tabs').find('.active').find('.unread-count');
            $unreadDom.html($unreadDom.html()-1);
            $self.remove();
        })
    })
</script>