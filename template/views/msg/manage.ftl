<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<#include "../utils.ftl">
<#setting datetime_format="yyyy-MM-dd HH:mm:ss"/>
<#setting date_format="yyyy-MM-dd" />

<content tag="javascript">/msg/manage.js</content>
<content tag="cssmain">/msg/manage.css</content>

<div id="main-container">
    <div class="panel panel-body" style="margin-top:20px;">
        <form id="fm" method="get" action="/msg/manage" class="">
            <div class="form-group col-md-2">
                <label for="big_type">平台类型</label>
                <select class="form-control input-sm" id="big_type" name="big_type">
                    <option <#if RequestParameters.big_type?? && RequestParameters.big_type=="-1" >selected</#if> value="-1">全部</option>
                    <option <#if RequestParameters.big_type?? && RequestParameters.big_type=="102" >selected</#if> value="102">烽火台</option>
                    <option <#if RequestParameters.big_type?? && RequestParameters.big_type=="101" >selected</#if> value="101">美团骑手</option>
                    <option <#if RequestParameters.big_type?? && RequestParameters.big_type=="100" >selected</#if> value="100">美团众包</option>
                    <option <#if RequestParameters.big_type?? && RequestParameters.big_type=="103" >selected</#if> value="103">众包商家端</option>
                </select>
            </div>
            <div class="form-group col-md-2">
                <label for="os_type">操作系统</label>
                <select class="form-control input-sm" id="os_type" name="os_type">
                    <option <#if RequestParameters.os_type?? && RequestParameters.os_type=="-1" >selected</#if> value="-1">全部</option>
                    <option <#if RequestParameters.os_type?? && RequestParameters.os_type=="3" >selected</#if> value="3">PC</option>
                    <option <#if RequestParameters.os_type?? && RequestParameters.os_type=="2" >selected</#if> value="2">iOS</option>
                    <option <#if RequestParameters.os_type?? && RequestParameters.os_type=="1" >selected</#if> value="1">Android</option>
                </select>
            </div>
            <div class="form-group col-md-2">
                <label for="status">状态</label>
                <select class="form-control input-sm" id="status" name="status">
                    <option <#if RequestParameters.status?? && RequestParameters.status=="-1" >selected</#if> value="-1">全部</option>
                    <option <#if RequestParameters.status?? && RequestParameters.status=="1" >selected</#if>  value="1">草稿</option>
                    <option <#if RequestParameters.status?? && RequestParameters.status=="2" >selected</#if> value="2">已发布</option>
                    <option <#if RequestParameters.status?? && RequestParameters.status=="3" >selected</#if> value="3">已下线</option>
                </select>
            </div>
            <div class="form-group col-md-2">
                <label for="status">分类</label>
                <select class="form-control input-sm" id="type" name="sub_type">
                    <option value="-1">全部</option>
                    <#if types?exists>
                    <#list types?keys?sort as key>
                        <option <#if RequestParameters.sub_type?? && RequestParameters.sub_type==key >selected</#if> value=${key}>
                            ${types[key]}
                        </option>
                    </#list>
                    </#if>
                </select>
            </div>
            <div class="form-group col-md-2" style="padding-top:14px;">
            <input type="submit" value="查询" class="btn btn-success js_submitbtn" id="search" />
            <a href="/msg/edit" target="_blank" class="btn btn-success js_submitbtn">新建通知</a>
            </div>
        </form>
    </div>
    <#if (page)?exists && (page.messageVos)?exists && (page.messageVos?size > 0) >
    <table class="table table-striped">
        <thead>
        <tr style="font-weight: bolder;">
            <td>平台类型</td>
            <td>标题</td>
            <td>范围</td>
            <td>应发/未读</td>
            <td>操作系统</td>
            <td>分类</td>
            <td>状态</td>
            <td>发布人</td>
            <td>发布时间</td>
            <td>下线时间</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody>
            <#list page.messageVos as message>
                <tr>
                    <td>
                        ${message.platformName !''}
                    </td>
                    <td><a style="color:#06c;" href="/msg/detail?id=${message.id}" title="<#if message.body?? && message.body?length gt 100>${message.body?substring(0,100)}...<#else>${message.body}</#if>"><#if message.title?? && message.title?length gt 10>${message.title?substring(0,10)}...<#else>${message.title}</#if></a></td>
                    <td>
                        <#assign scope = "">
                        <#if message.area == 0 && message.poiArea == 0>
                            <#assign scope = "全国">
                        </#if>
                        <#if message.cityMap?exists>
                            <#list message.cityMap?keys as key>
                                <#assign scope = scope + message.cityMap[key]!''+" ">
                            </#list>
                        </#if>
                        <#if message.orgTypeMap?exists>
                            <#list message.orgTypeMap?keys as key>
                                <#assign scope = scope + message.orgTypeMap[key]!''+" ">
                            </#list>
                        </#if>
                        <#if message.orgMap?exists>
                            <#list message.orgMap?keys as key>
                                <#assign scope = scope + message.orgMap[key]!''+" ">
                            </#list>
                        </#if>
                        <#if message.userPhones?exists>
                            <#assign scope = scope + message.userPhones?replace(","," ")>
                        </#if>
                        <#if message.poiCityMap?exists>
                            <#list message.poiCityMap?keys as key>
                                <#assign scope = scope + message.poiCityMap[key]!''+" ">
                            </#list>
                        </#if>

                        <#if message.poiDeliveryAreaMap?exists>
                            <#list message.poiDeliveryAreaMap?keys as key>
                                <#assign scope = scope + message.poiDeliveryAreaMap[key]!''+" ">
                            </#list>
                        </#if>
                         <#if message.poiNameMap?exists>
                            <#list message.poiNameMap?keys as key>
                                <#assign scope = scope + message.poiNameMap[key]!''+" ">
                            </#list>
                        </#if>

                        <a title="${scope}">
                        <#if message.scopeKeywords??>
                            <#list message.scopeKeywords as keyword>
                                ${keyword!''}
                                <#if keyword_has_next><br></#if>
                            </#list>
                        </#if>
                    </a></td>
                    <td><#if message.sendCount gt 0><a style="color:#06c;" href="javascript:;" name="send" data=${message.id}>${message.sendCount}</a><#else>${message.sendCount}</#if>/<#if message.unReadCount gt 0><a style="color:#06c;" href="javascript:;" name="unread" data=${message.id}>${message.unReadCount}</a><#else>0</#if></td>
                    <td>
                        <#if message.osMap?exists>
                            <#list message.osMap?keys as key>
                                ${message.osMap[key]}
                                <#if key_has_next><br></#if>
                            </#list>
                        </#if>
                    </td>
                    <td>${message.subTypeName!''}</td>
                    <td>
                    <#if message.status == 1><span class="label badge label-disactive">${message.statusName !''}</span></#if>
                    <#if message.status == 2><span class="label badge label-success">${message.statusName !''}</span></#if>
                    <#if message.status == 21><span class="label badge label-primary">${message.statusName !''}</span></#if>
                    <#if message.status == 22><span class="label badge label-danger">${message.statusName !''}</span></#if>
                    <#if message.status == 3><span class="label badge label-warning">${message.statusName !''}</span></#if>
                    </td>
                    <td>${message.createrName !''}</td>
                    <td><a title="${(message.createTime *1000)?number?number_to_datetime}">${(message.createTime *1000)?number?number_to_date}</a></td>
                    <td><a title="<#if (message.expiryTime>0)>${(message.expiryTime *1000)?number?number_to_datetime}</#if>"><#if (message.expiryTime>0)>${(message.expiryTime *1000)?number?number_to_date}</#if></a></td>
                    <td id=${message.id} style="text-align:left;">
                        <#if message.status == 1>
                            <a data-toggle="tooltip" title="修改" data-placement="top" href="/msg/edit?id=${message.id}">
                                <i class="fa fa-edit fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                            <a data-toggle="tooltip" title="删除" data-placement="top" href="javascript:;" name="delete">
                                <i class="fa fa-trash-o fa-lg opration-icon"></i>
                            </a>
                        </#if>
                        <#if message.status == 2>
                            <a data-toggle="tooltip" title="修改" data-placement="top" href="/msg/edit?id=${message.id}">
                                <i class="fa fa-edit fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                            <a data-toggle="tooltip" title="删除" data-placement="top" href="javascript:;" name="delete">
                                <i class="fa fa-trash-o fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                            <a data-toggle="tooltip" title="下线" data-placement="top" href="javascript:;" name="offline">
                                <i class="fa fa-power-off fa-lg opration-icon"></i>
                            </a>&nbsp;&nbsp;
                            <#if message.top == 0>
                                <a data-toggle="tooltip" title="置顶" data-placement="top" href="javascript:;" name="top">
                                    <i class="fa fa-arrow-up fa-lg opration-icon"></i>
                                </a>
                            <#else>
                                <a data-toggle="tooltip" title="取消置顶" data-placement="top" href="javascript:;" name="cancelTop">
                                    <i class="fa fa-arrow-down fa-lg opration-icon"></i>
                                </a>
                            </#if>

                        </#if>
                        <#if message.status==3>
                            <a data-toggle="tooltip" title="删除" data-placement="top" href="javascript:;" name="delete">
                                <i class="fa fa-trash-o fa-lg opration-icon"></i>
                            </a>
                        </#if>
                    </td>
                </tr>
            </#list>
        </tbody>
    </table>
    <#if (page)?exists && (page.total)?exists && (page.pageSize)?exists && (page.pageNum)?exists && page.total gt 0 && page.pageSize gt 0 && page.pageNum gt 0  >
      <#if page.total??>
        <#import "/page/pager.ftl" as q>
        <@q.pager pageNo=page.pageNum pageSize=page.pageSize recordCount=page.total toURL="/msg/manage"/>
      </#if>
    </#if>
    <#else>
        暂无数据！
    </#if>

    <#-- 未读列表 -->
    <div id="unread-container" class="hide">
        <table class="table">
            <thead id='userHeader'>
            
            </thead>
            <tbody id="userList">

            </tbody>
        </table>
        <div class="panel-footer clearfix">
            <ul class="pagination pagination-xs m-top-none pull-right" id="userPage">
            </ul>
        </div>
    </div>



</div>
<script type='text/template' id='userHeaderTemplate'>
    <tr>
        <td>姓名</td>
        <td>手机号</td>
        <td>性别</td>
        <td>城市</td>
        <td>所在组织</td>
        <td>组织类型</td>
        <td>职位</td>
    </tr>
</script>

<script type='text/template' id='shopHeaderTemplate'>
    <tr>
        <td>商家ID</td>
        <td>商家名称</td>
        <td>城市</td>
        <td>配送区域</td>
        <td>商家电话</td>
    </tr>
</script>

<script type="text/javascript" src="/static/js/lib/My97DatePicker/WdatePicker.js"></script>
