<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<#setting datetime_format="yyyy-MM-dd HH:mm:ss"/>
<style>
	#umeditor-content{padding-left: 20px;}
	#umeditor-content ul{list-style-type: disc!important;}
	#umeditor-content ol{list-style-type: decimal!important;}
	#umeditor i{font-style: italic!important;}
</style>
<div id="main-container">
	<#if (message)?exists >
	    <h5>${message.title!''}</h5>
	    <h6>${(message.createTime *1000)?number?number_to_datetime}</h6>
	    <div id="umeditor-content"></div>
	<#else>
	    暂无数据！
	</#if>
</div>
<script>
    var umEditorHTML = "";
    <#if (message)?exists >
    	umEditorHTML = "${message.body!''}";
    </#if>
    if (umEditorHTML.length) {
    	$('#umeditor-content').html(decodeURIComponent(umEditorHTML));
    }
</script>