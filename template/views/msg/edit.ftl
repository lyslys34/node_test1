<#include "../base/home_config.ftl">
<#include "../widgets/sidebar.ftl">
<#include "../utils.ftl">
<#setting datetime_format="yyyy-MM-dd HH:mm:ss"/>
<content tag="javascript">/msg/edit.js</content>
<content tag="cssmain">/msg/edit.css</content>
<link rel="stylesheet" href="/static/css/lib/umeditor.min.css">
<script src="/static/js/lib/umeditor/umeditor.js"></script>
<script src="/static/js/lib/umeditor/umeditor.config.js"></script>

<div id="main-container">
        <form class="form-horizontal" action="/msg/save" method="post" id="fm">
        <div class="form-group">
            <label for="big_type" class="col-sm-1 control-label">平台类型:</label>
            <div class="col-sm-6" id="big_type">
                <label class="radio-inline w100"><input type="radio" name="big_type" value="102" <#if message?? && message.status != 1>disabled</#if> <#if !message?? || message.platformId == 102> checked</#if> />烽火台&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                <label class="radio-inline w100"><input type="radio" name="big_type" value="101" <#if message?? && message.platformId == 101>checked</#if> <#if message?? && message.status != 1>disabled</#if> />美团骑手&nbsp;&nbsp;</label>
                <label class="radio-inline w100"><input type="radio" name="big_type" value="100" <#if message?? && message.platformId == 100>checked</#if> <#if message?? && message.status != 1>disabled</#if> />美团众包</label>
                <label class="radio-inline w100"><input type="radio" name="big_type" value="103" <#if message?? && message.platformId == 103>checked</#if> <#if message?? && message.status != 1>disabled</#if> />众包商家端</label>
            </div>
        </div>
        <div class="form-group for-app <#if message?? && message.platformId != 102> />hide</#if>">
            <label for="os_type" class="col-sm-1 control-label">操作系统:</label>
            <div class="col-sm-6" id="os_type">
                <label class="checkbox-inline w150"><input type="checkbox" name="android" <#if message?? && message.osMap?? && message.osMap["1"]??>checked</#if> <#if message?? && message.status != 1>disabled</#if> />Android&nbsp;&nbsp;&nbsp;&nbsp;</label>
                <label class="checkbox-inline w150"><input type="checkbox" name="ios" <#if message?? && message.osMap?? && message.osMap["2"]??>checked</#if> <#if message?? && message.status != 1>disabled</#if> />iOS</label>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-1 control-label">通知标题:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="title" name="title" <#if message?? && message.status != 1>disabled</#if> value="<#if message??>${message.title!''}</#if>">
            </div>
        </div>
        <div class="form-group">
            <label for="body" class="col-sm-1 control-label">通知内容:</label>
            <div class="col-sm-6" id="body_type">
                <label class="radio-inline radio"><input type="radio" name="body_type" <#if !message?? || message.body_type == 0>checked</#if> value="0" <#if message?? && message.status == 3>disabled</#if> />编辑文本</label>
                <label class="radio-inline radio"><input type="radio" name="body_type" <#if message?? && message.body_type == 1>checked</#if> value="1" <#if message?? && message.status == 3>disabled</#if> />直接添加链接</label>
                <div id="editor" <#if message?? && message.body_type == 1>class="hide"</#if>>
                    <input type="hidden" name="body" id="edit_text" value="<#if message?? && message.body_type==0>${message.body!''}</#if>">
                    <#-- umeditor -->
                    <script id="umeditor" name="content" text="text/plain"></script>
                    <#-- umeditor end -->
                </div>
                <input  <#if !message?? || message.body_type == 0>type="hidden"<#else>text</#if> class="form-control" id="body_url" name="body_url" value="<#if message?? && message.body_type==1>${message.body!''}</#if>" <#if message?? && message.status == 3>disabled</#if>>
            </div>
        </div>
        <div class="form-group <#if message?? && message.platformId == 103>hide</#if>" id='areaWrap'>
            <label for="area" class="col-sm-1 control-label">发送范围:</label>
            <div class="col-sm-6" id="area">
                <label class="radio-inline radio"><input type="radio" name="area" value="1" <#if !message?? || message.area==1>checked</#if> <#if message?? && message.status != 1>disabled</#if> />部分区域</label>
                <label class="radio-inline radio"><input type="radio" name="area" value="0" <#if message?? && message.area==0>checked</#if> <#if message?? && message.status != 1>disabled</#if> />全国</label>   
                <div id="sub-area" <#if message?? && message.area==0>class="hide"</#if>>
                    <div id="orgs" class="select" style="clear:both;">
                        组织关系：
                        <#if !message?? || message.status == 1>
                        <span>
                            <select name="orgType" id="orgType" class="input-sm" style="width:auto;height:28px;">
                            </select>
                        </span>
                        <input type="button" class="btn-primary" value="添加" id="addOrg">
                        </#if>
                        <label class='checkbox-inline w150' style='padding-bottom:7px;'>
                            <input type='checkbox' name='notice_flag'
                            <#if message?? && message.sendHeadquartersStaff>
                             checked
                             </#if> 
                             <#if message?? && message.status != 1>disabled</#if>
                             /> 通知总部人员
                        </label>
                        <div>
                            <#--<textarea disabled row=3 class="form-control"><#if message?? && message.orgTypeMap??><#list message.orgTypeMap?keys as key>${message.orgTypeMap[key]}<#if key_has_next>，</#if></#list><#if message?? && message.orgMap??>，</#if></#if><#if message?? && message.orgMap??><#list message.orgMap?keys as key>${message.orgMap[key]}<#if key_has_next>，</#if></#list></#if></textarea>-->
                            <div class="select2 select2-container select2-container--default <#if message?? && message.status != 1>select2-container--disabled</#if>" style="width:100%;padding-top:3px;">
                                <div class="select2-selection select2-selection--multiple" style="width:100%;height:25px;">
                                    <ul id="selectedOrgs" class="select2-selection__rendered">
                                        <#if message?? && message.orgTypeMap??>
                                            <#list message.orgTypeMap?keys as key>
                                                <li class="selected"><#if !message?? || message.status == 1><a name="deleteOrgType" href="javascript:;" data=${key} class="select2-selection__choice__remove">x</a></#if>${message.orgTypeMap[key]}</li>
                                            </#list>
                                        </#if>
                                        <#if message?? && message.orgMap??>
                                            <#list message.orgMap?keys as key>
                                                <li class="selected"><#if !message?? || message.status == 1><a name="deleteOrg" href="javascript:;" data=${key} class="select2-selection__choice__remove">x</a></#if>${message.orgMap[key]}</li>
                                            </#list>
                                        </#if>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="select" id='cityArea' <#if !message?? || message.platformId == 102>class='hide'</#if>>
                        城市维度（可以给某个城市的全部骑手发送通知）：
                        <select name="cityIds" id="city" multiple="multiple" style="width:100%;" <#if message?? && message.status != 1>disabled</#if>>
                            <#if message?? && message.cityMap??>
                                <#list message.cityMap?keys as key>
                                    <option value="${key}">${message.cityMap[key]}</option>
                                </#list>
                            </#if>
                        </select>
                    </div>
                    <div class="select" style="clear:both;" id="userArea" <#if !message?? || message.platformId == 102>class='hide'</#if>>
                        自定义用户：
                        <textarea placeholder="请输入用户手机号，用英文逗号分隔" class="form-control" row=3 name="userPhones" id="userPhones" <#if message?? && message.status != 1>disabled</#if>
                        ><#if message?? && message.userPhones??>${message.userPhones!''}</#if></textarea>
                    </div>
                    <div class="select" style="clear:both;" id="userSelect2Area" <#if message?? && message.platformId != 102>class='hide'</#if>>
                        自定义用户：
                         <select name="bmUserIds" id="users" multiple="multiple" style="width:100%;" <#if message?? && message.status != 1>disabled</#if>>
                            
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group <#if !message?? || message.platformId != 103>hide</#if>" id='poiAreaWrap'>
            <label for="poiArea" class="col-sm-1 control-label">发送范围:</label>
            <div class="col-sm-6" id="poiArea">
                <label class="radio-inline radio"><input type="radio" name="poiArea" value="1" <#if !message?? || message.poiArea==1>checked</#if> <#if message?? && message.status != 1>disabled</#if> />部分区域</label>
                <label class="radio-inline radio"><input type="radio" name="poiArea" value="0" <#if message?? && message.poiArea==0>checked</#if> <#if message?? && message.status != 1>disabled</#if> />全国</label> 
                <div id='subPOIArea' <#if message?? && message.poiArea==0>class="hide"</#if>> 
                 <div class="select" id='poiCityArea'>
                    城市：
                    <select name="poiCityIds" id="poiCitySelect" multiple="multiple" style="width:100%;" <#if message?? && message.status != 1>disabled</#if>>
                        <#if message?? && message.poiCityMap??>
                            <#list message.poiCityMap?keys as key>
                                <option value="${key}">${message.poiCityMap[key]}</option>
                            </#list>
                        </#if>
                    </select>
                </div>
                <div class="select" id='poiAreaIds'>
                    众包配送区域：
                    <select name="poiDeliveryAreaIds" id="poiAreaSelect" multiple="multiple" style="width:100%;" <#if message?? && message.status != 1>disabled</#if>>
                        <#if message?? && message.poiDeliveryAreaMap??>
                            <#list message.poiDeliveryAreaMap?keys as key>
                                <option value="${key}">${message.poiDeliveryAreaMap[key]}</option>
                            </#list>
                        </#if>
                    </select>
                </div>
                <div class="select" id='poiShopIds'>
                    自定义商家：
                    <select name="poiIds" id="poiShopSelect" multiple="multiple" style="width:100%;" <#if message?? && message.status != 1>disabled</#if>>
                        <#if message?? && message.poiNameMap??>
                            <#list message.poiNameMap?keys as key>
                                <option value="${key}" selected='selected'>${message.poiNameMap[key]}</option>
                            </#list>
                        </#if>
                    </select>
                </div>
                </div>
            </div>
        </div>
        <div class="form-group <#if message?? && message.platformId != 102>hide</#if>" id="showType">
            <label for="type" class="col-sm-1 control-label">显示入口:</label>
            <div class="col-sm-6">
                <label class="checkbox-inline"><input type="checkbox" name="daxiang" <#if message?? && message.status != 1>disabled</#if> <#if message?? && message.pushDx>checked</#if> />大象</label>
                <label class="checkbox-inline"><input type="checkbox" name="promptBox" <#if message?? && message.status != 1>disabled</#if> <#if message?? && message.pushPromptBox>checked</#if> />烽火台弹框</label>
                <label class="checkbox-inline"><input type="checkbox" disabled checked />烽火台通知中心</label>
                <label class="checkbox-inline"><input type="checkbox" disabled checked />烽火台首页</label>
            </div>
        </div>
        <div class="form-group">
            <label for="type" class="col-sm-1 control-label">所属分类:</label>
            <div class="col-sm-6">
                <select class="form-control input-sm" style="width:40%;" name="sub_type" <#if message?? && message.status != 1>disabled</#if>>
                    <#if types?exists>
                    <#list types?keys?sort as key>
                        <option <#if message?? && message.subType?string==key >selected</#if> value=${key}>
                            ${types[key]}
                        </option>
                    </#list>
                    </#if>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="expiry_time" class="col-sm-1 control-label"> <#--<input type="checkbox" name="body_type"  />-->定时下线</label>
            <div class="col-sm-6">
                <input type="text" <#if message?? && message.status != 1>disabled</#if> value="<#if (message?? && message.expiryTime>0)>${(message.expiryTime *1000)?number?number_to_datetime}</#if>" name="expiry_time" id="expiry_time" style="cursor: pointer; width:40%" class="input-sm form-control" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-1 col-sm-6">
                <input type="hidden" value="<#if message?? && message.platformId==102||!message??>2<#else>1</#if>" name="channel_type" >
                <input type="hidden" value="1" name="status" id="status">
                <input type="hidden" value="" name="orgTypes">
                <input type="hidden" value="" name="orgIds">
                <input type='hidden' value='<#if message??>${message.sendHeadquartersStaff?string('true','false')}<#else>false</#if>' name='sendHeadquartersStaff'>
                <input type="hidden" value="<#if message??>${message.id}<#else>-1</#if>" name="id">
                <input type='hidden' value="" name="bmUserIds">
                <#if !message?? || message.status == 1>
                <button type="button" class="btn btn-success" id="send">保存并发布</button>
                <button type="button" class="btn btn-success" id="save">保存为草稿</button>
                </#if>
                <#if message?? && message.status == 2>
                    <button type="button" class="btn btn-success" id="send">保存并发布</button>
                </#if>
                <a type="button" class="btn btn-default" href="/msg/manage">取消</a>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="/static/js/lib/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
    var g_city_ids = [];
    var g_org_types = [];
    var g_org_ids = [];
    var g_user_ids = [];
    var g_poi_city_ids = [];
    var g_poi_area_ids = [];
    var g_poi_shop_ids = [];
    var g_get_sub_area = false;
    var g_get_sub_poi_area = false;
    var edit = false;
    var sendHeadquartersStaff = false;
    <#if message??>
        edit = true;
    </#if>
    <#if message??>
        sendHeadquartersStaff = ${message.sendHeadquartersStaff?string('true','false')};
    </#if>

    <#if message?? && message.userMap??>
        <#list message.userMap?keys as key>
            g_user_ids.push(${key});
        </#list>
    </#if>

    <#if message?? && message.poiCityMap??>
        <#list message.poiCityMap?keys as key>
            g_poi_city_ids.push(${key});
        </#list>
    </#if>

    <#if message?? && message.poiDeliveryAreaMap??>
        <#list message.poiDeliveryAreaMap?keys as key>
            g_poi_area_ids.push(${key});
        </#list>
    </#if>

    <#if message?? && message.poiNameMap??>
        <#list message.poiNameMap?keys as key>
            g_poi_shop_ids.push(${key});
        </#list>
    </#if>

    <#if message?? && message.cityMap??>
        <#list message.cityMap?keys as key>
            g_city_ids.push(${key});
        </#list>
    </#if>

    <#if message?? && message.orgTypeMap??>
        <#list message.orgTypeMap?keys as key>
            g_org_types.push(${key});
        </#list>
    </#if>
    <#if message?? && message.orgMap??>
        <#list message.orgMap?keys as key>
            g_org_ids.push(${key});
        </#list>
    </#if>
    <#if message?? && message.status == 1 && message.area == 1>
        g_get_sub_area = true;
    </#if>
    <#if message?? && message.status == 1 && message.poiArea == 1>
        g_get_sub_poi_area = true;
    </#if>
</script>