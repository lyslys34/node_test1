<#include "updateManage_config.ftl">
<#include "../widgets/sidebar.ftl">

<content tag="cssmain">/updateManage/updateManage.css</content>
<content tag="javascript">/updateManage/add.js</content>

<div id="main-container">
    <div  id="success"   style="display:none;    background-color: #65cea7; height:30px; font-size:28px ;   text-align:center;"> <span>发布成功</span></div>
    <div  id="fail" style="display:none;    background-color: #65cea7; height:30px; font-size:28px ;  padding-left: 400px;">  <span>发布失败</span></div>
   <div class="form-horizontal" style="width: 50%; margin-top: 10px;">

     <div class="form-group">
       <label class="col-sm-2 control-label">App类型</label>
       <div class="col-sm-10">
         <select class="form-control"  id="app_type" onchange="changeAppType()">
             <option value="-1">&nbsp&nbsp</option>
             <option value="0">代理</option>
             <option value="1">众包</option>
             <option value="3">自建</option>
             <option value="5">角马</option>
             <option value="6">众包商家</option>
         </select>
       </div>
     </div>
     <div class="form-group">
       <label class="col-sm-2 control-label">操作系统</label>
       <div class="col-sm-10">
         <select class="form-control" id="os_type">
             <option value="1">Android</option>
             <option value="2">IOS</option>
         </select>
       </div>
     </div>
          <div class="form-group">
        <label class="col-sm-2 control-label">更新日志</label>
        <div class="col-sm-10">
            <textarea type="text" rows="4" class="form-control" id="description" style=""></textarea>
        </div>
     </div>
         <div class="form-group" id="apk_upload">
        <label class="col-sm-2 control-label">文件包</label>
        <div class="col-sm-10">
            <span>apk/ipa : </span>
            <input type="file" name="file" id="apk-file" style="display: inline;" />
            <div class="btn btn-sm btn-primary" id="upload-btn" onclick="upload_file()" style="display: inline;">上传文件</div>
        </div>
     </div>  
      <div class="form-group">
        <label class="col-sm-2 control-label">版本号</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" placeholder="无需手动输入，上传apk或ipa后将自动填写" id="app_version" required="" style="">
        </div>
     </div>
     <div class="form-group">
        <label class="col-sm-2 control-label">发版code</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" placeholder="无需手动输入，上传apk或ipa后将自动填写" id="code" required="">
        </div>
     </div>
     <div class="form-group">
        <label class="col-sm-2 control-label">强制更新</label>
        <div class="col-sm-10">
            <label class="radio-inline">
              <input type="radio" style="opacity: 1;" name="force_update" id="force-update" value="1"> 是
            </label>
            <label class="radio-inline">
              <input type="radio" style="opacity: 1;" name="force_update" id="force-update" value="0" checked="true"> 否
            </label>
        </div>
     </div>
     <div class="form-group  input-change">
        <label class="col-sm-2 control-label">发版覆盖</label>
        <div class="col-sm-10">
            <label class="radio-inline">
              <input type="radio" style="opacity: 1;" name="release_type" id="gray-update" value="1">灰度
            </label>
            <label class="radio-inline">
              <input type="radio" style="opacity: 1;" name="release_type" id="full-update" value="0" checked="true"> 全量
            </label>
            <div id="gray" style="display: none;">
              <div id="device" class="item_wrapper" style="margin-bottom: 10px; margin-top:10px;">
                <span style="margin-bottom:5px;maigin-top:8px; display:inline-block;">&nbsp&nbsp机型</span>
                <select class="device-select" multiple="multiple" style="width: 100%;">
                    <option value="" id="device_option"></option>
                </select>
              </div>
              <div id="device-ios" style="display: none;margin-bottom:10px; margin-top:10px;">
                <span style="margin-bottom:5px;maigin-top:8px;">&nbsp&nbsp机型</span>
                <select class="device-select-ios" multiple="multiple" style="width: 100%;margin-top:10px;">
                    <option value="" id="device_option_ios"></option>
                </select>
              </div>
               <div style="margin-bottom:10px;" id="city-select-div">
                <span style="margin-bottom:5px; display:inline-block;">&nbsp&nbsp城市(众包请勿选择北京)</span>
                <select class="city-select" id="city-select" multiple="multiple" style="width: 100%;">
                    <option value="" id="city_option"></option>
                </select>
                </div>
               <div style="margin-bottom:10px;" id="station-select-div">
                <span style="margin-bottom:5px;">&nbsp&nbsp站点</span>
                <select class="station-select" id="station-select" multiple="multiple" style="width: 100%;">
                    <option value="" id="station_option"></option>
                </select>
             </div>
             <span>&nbsp&nbsp灰度骑手手机号码</span>
  <textarea type="text" rows="4" class="form-control" placeholder="请输入骑手手机号，多个请用英文逗号隔开" id="mobiles" ></textarea>
                <div style="width:100%;border-style: none;margin-top: 10px; padding-left:0px;padding-right:0px;" class="form-control">
                    <div class="col-md-3">
                        <label >灰度比例:</label>
                    </div>
                    <div class="col-md-3">
                        <span id="gray_percent" style="margin-top:10px">0.0%</span>
                    </div>
                    <div class="col-md-6" style="text-align:right;padding-right:0px;">
                        <button class="btn btn-default" id="percent-submit" style="margin-top:-6px"/>计算</div>

                </div>

    
            </div>
        </div>
     </div>
 
  
     <div class="form-group" id="down"  style="display:true">
        <label class="col-sm-2 control-label">下载链接</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="download_url" required="" style="">
        </div>
     </div>

 
     <div class="form-group">
       <div class="col-sm-offset-2 col-sm-10">
         <button type="submit" class="btn btn-default" id="add-new-submit">提交</button>
       </div>
     </div>
   </div>
</div>
<script>
    $('.city-select').select2();
    $('.device-select').select2();
    $('.device-select-ios').select2();
    $('.station-select').select2();
    document.title = '发布新版本';
</script>
