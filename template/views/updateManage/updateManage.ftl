<#include "updateManage_config.ftl">
	<#include "../widgets/sidebar.ftl">

		<content tag="cssmain">/updateManage/updateManage.css</content>
		<content tag="javascript">/updateManage/updateManage.js</content>
		<#list appList as app>
			<div id="modal${app.id!''}"  class="modal fade"  role="dialog">
				<div class="modal-dialog">
					<span style="display: none;" class="operator">${app.operator!''}</span>
					<span style="display: none;" class="app-id">${app.id}</span>
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 clss="modal-title">发版详情</h4>
						</div>
						<div class="modal-body" id="collapse${app.id!''}" >
							<#if app.os_type==1>
								<#assign system_type="安卓">
							</#if>
							<#if app.os_type==2>
								<#assign system_type="iOS">
							</#if>

							<#switch app.app_type>
								<#case 0>
									<#assign application_type="代理">
										<#break>
											<#case 1>
												<#assign application_type="众包">
													<#break>
														<#case 3>
															<#assign application_type="美团骑手">
																<#break>
																	<#case 5>
																		<#assign application_type="角马">
																			<#break>
																				<#case 6>
																					<#assign application_type="众包商家">
							</#switch>
							<div class="row">
								<div class="col-md-2 app-info">版本信息</div>
								<div class="col-md-9 app-info">${application_type!''}&nbsp&nbsp ${system_type!''}&nbsp&nbsp${app.app_version}&nbsp&nbsp${app.code!''}</div>
							</div>
							<div class="row">
								<div class="col-md-2">强制升级:</div>
								<span style="display:none;" class="force-up">${app.force_update!''}</span>
								<div class="col-md-9">
									<#if app.force_update == 0>否
									</#if>
									<#if app.force_update == 1>是
									</#if>
								</div>
							</div>
							<#if app.release_type == 1>
								<div class="row">
									<div class="col-md-2 app-info">覆盖范围:</div>
									<span style="display: none;" class="release-station">${app.release_station!''}</span>
									<div class="col-md-9 app-info gray-station"></div>
								</div>
								<div class="row">
									<div class="col-md-2 app-info">灰度机型:</div>
									<span style="display: none;" class="release-device">${app.release_device!''}</span>
									<div class="col-md-9 app-info gray-device"></div>
								</div>
							</#if>
							<div class="row">
								<div class="col-md-2 app-info">下载链接:</div>
								<div class="col-md-9 app-info"><a href="${app.download_url!''}">${app.download_url!''}</a></div>
							</div>
							<div class="row">
								<div class="col-md-2 app-info">更新日志:</div>
								<div class="col-md-9 app-info description">${app. description!''}</div>
							</div>
							<div class="row">
								<div class="col-md-2 app-info">操作记录:</div>
								<div class="col-md-9 app-info operation-log">
									<table class="table table-bordered">
										<tr>
											<th>姓名</th>
											<th>操作</th>
											<th>创建时间</th>
										</tr>
									</table>
								</div>
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default end_release" data-dismiss="modal" >关闭</button>
						</div>
					</div>
				</div>
			</div>
		</#list>
		<span style="display:none;" id="current-id"></span>
		<span style="display:none;" id="current-os"></span>
		<div id="main-container">
			<div class="modal fade" id="undoRelease" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">终止发版</h4>
						</div>
						<div class="modal-body">
							<label>输入终止发版的原因</label>
							<input id="undo_reason" style="width:100%" type="text"></input>
						</div>
						<div class="modal-footer">
							<button type="button" id="undo_button" class="btn btn-default" data-dismiss="modal">确认终止</button>
						</div>
					</div>

				</div>
			</div>
			<form id="select_form" class="no-margin"  data-validate="parsley" novalidate="" method="get" action="/updateManage">
			</form>
			<div class="alert alert-success" id="operation-alert" style="position: fixed;left: 40%;width: 400px;text-align: center;display: none;"><span></span></sp><button type="button" class="close" onclick="closeAlert()" <span="" aria-hidden="true">×</button></div>
			<div class="row">
				<div class="col-md-1 select-desc">
					<span class="select-span">APP类型</span>
				</div>
				<div class="col-md-2">

					<select id="application_type"  name="app_type" class="my-selector form-control " form="select_form">
						<option <#if search_app_type?? && search_app_type==-1>selected</#if> value="-1">全部</option>
						<option <#if search_app_type?? && search_app_type==0>selected</#if> value="0">代理</option>
						<option <#if search_app_type?? && search_app_type==5>selected</#if> value="5">角马</option>
						<option <#if search_app_type?? && search_app_type==1>selected</#if> value="1">众包</option>
						<option <#if search_app_type?? && search_app_type==3>selected</#if> value="3">美团骑手</option>
						<option <#if search_app_type?? && search_app_type==6>selected</#if> value="6">众包商家</option>
					</select>
				</div>
				<div class="col-md-1 select-desc">
					<span class="select-span">操作系统</span>
				</div>
				<div class="col-md-2 ">
					<select id="system_type" name="os_type" class="my-selector form-control" form="select_form">
						<option <#if search_os_type?? && search_os_type==-1>selected</#if> value="-1">全部</option>
						<option <#if search_os_type?? && search_os_type==1>selected</#if> value="1">Android</option>
						<option <#if search_os_type?? && search_os_type==2>selected</#if> value="2">iOS</option>
					</select>
				</div>
				<div class="col-md-1 select-desc">
					<span class="select-span">发版类型</span>
				</div>
				<div class="col-md-2">
					<select id="release_type" name="release_type" class="my-selector form-control" form="select_form">
						<option <#if search_release_type?? && search_release_type==-1>selected</#if> value="-1">全部</option>
						<option <#if search_release_type?? && search_release_type==0>selected</#if>  value="0">全量</option>
						<option <#if search_release_type?? && search_release_type==1>selected</#if>  value="1">灰度</option>
					</select>
				</div>
				<div class="col-md-1">
					<div class="btn btn-success" id="search-versions" style="margin:10px 0px;">搜索</div>
				</div>
				<div class="col-md-2">
					<div class="btn btn-success" id="btn-add-new-version" data-toggle="modal" data-target="#add_version_modal">添加新版本</div>
				</div>
			</div>
			<div class="panel panel-default ">
				<table class="talbe table-striped table-responsive" style="width:100%;">
					<thead>
					<tr style="height:30px;">
						<th>  	&nbsp;&nbsp;App类型</th>
						<th> 	&nbsp;&nbsp; 操作系统</th>
						<th> 	&nbsp;&nbsp; 版本号</th>
						<th> 	&nbsp;&nbsp; 发布时间</th>
						<th> 	&nbsp;&nbsp; 发布类型</th>
						<th> 	&nbsp;&nbsp; 操作</th>
					</tr>
					</thead>
					<tbody>
					<#list appList as app>
						<#if app.os_type == 1>
							<#assign os_type="安卓">
						</#if>
						<#if app.os_type == 2>
							<#assign os_type="iOS">
						</#if>
						<#switch app.app_type>
							<#case 0>
										<#assign app_type="代理">
											<#break>
										<#case 1>
											<#assign app_type="众包">
												<#break>
													<#case 3>
														<#assign app_type="美团骑手">
															<#break>
																<#case 5>
																	<#assign app_type="角马">
																		<#break>
																			<#case 6>
																				<#assign app_type="众包商家">
						</#switch>

						<tr  style="height:30px; font-size:13px;">

							<th> 	&nbsp;&nbsp; ${app_type!''}</th>
							<th> 	&nbsp;&nbsp; ${os_type!''}</th>
							<th> 	&nbsp;&nbsp; ${app.app_version!''}</th>
							<th> 	&nbsp;&nbsp; ${(app.ctime*1000)?number_to_datetime}</th>

							<th>	&nbsp;&nbsp;	<#if app.release_type==0>  全量</#if>
								<#if app.release_type==1>  灰度</#if>
							</th>

							<th>&nbsp&nbsp
							<a  onclick="saveDetailId($(this));"  class="collapse-trigger" data-parent="#accordion"  data-toggle="tooltip" title="详情" data-placement="top"  href="javascript:void(0)" datail_href="${app.id!''}"><i class="fa fa-info-circle fa-lg opration-icon"></i></a>
							<#if app.valid == 1 >		&nbsp;&nbsp;
								<a href="javascript:void(0)"  cityid="${app.id!''}" releasetype="${app.release_type!''}" app_version="${app.app_version!''}" ostype="${app.os_type!''}" app_type="${app.app_type!''}" mobile="${app.mobile!''}"  onclick="edit_config($(this));" data-toggle="tooltip" title="修改发版配置" data-placement="top"><i class="fa fa-edit fa-lg opration-icon"></i></a>
							</#if>
							<#if app.valid==0>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</#if>

							&nbsp;&nbsp;
							<#if app.valid == 1 >
								<a href="javascript:void(0)" app-id="${app.id!''}" onclick="undoRelease($(this))" data-toggle="tooltip" title="终止发版" data-placement="top"><i class="fa fa-power-off fa-lg opration-icon"></i></a>

							</#if>	   </th>

						</tr>
					</#list>
					</tbody>
					</tbody>
				</table>
			</div>
			<#import "../page/pager.ftl" as q>
				<#if recordCount??>
					<@q.pager pageNo=pageNo pageSize=pageSize recordCount=recordCount toURL="/updateManage/versionListByParam"/>
				</#if>
		</div>

		<div id="update" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">更新发版配置</h4>
					</div>
					<div class="modal-body">
						<div>
							<div id="release-range">
								<div class="form-group  no-all" id="device">
									<label>灰度机型</label>
									<select class="device-select form-control" multiple="multiple" style="width: 100%;">
										<option value="" id="device_option"></option>
									</select>
								</div>
								<div class="form-group  no-all" id="device-ios" style="display: none;">
									<label>灰度机型</label>
									<select class="device-select-ios form-control" multiple="multiple" style="width: 100%;">
										<option value="" id="device_option_ios"></option>
									</select>
								</div>
								<div class="form-group no-all">
									<label>灰度城市</label>
									<select class="city-select form-control" multiple="multiple" style="width: 100%;">
										<option value="" class="city_option" ></option>
									</select>
								</div>
								<div class="form-group  no-all">
									<label>灰度站点</label>
									<select class="station-select" multiple="multiple" style="width: 100%;">
										<option value="" class="station_option" ></option>
									</select>
								</div>
								<div class="form-group  no-all">
									<label>灰度骑手手机号</label>
									<textarea type="text" rows="4" class="form-control" placeholder="请输入骑手手机号，多个请用英文逗号隔开" id="mobiles-update" style=""></textarea>

								</div>
								<div class="form-group">
									<label>是否强制升级</label>
									<select name="" class="form-control" id="force_update">
										<option value="0">否</option>
										<option value="1" id="force-option">是</option>
									</select>
								</div>
								<div class="form-group">
									<label>更新日志</label>
									<textarea type="text" rows="4" class="form-control" id="description-update" style=""></textarea>
								</div>

								<div class="form-group">
									<div class="btn btn-default no-all"  id="grayPercentBtn">获取灰度比例</div>

									<span id="gray_percent" class="no-all">0.0%</span>

								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
									<button onclick="updateRelease(this);" class="btn btn-default">提交</button>
								</div>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
		</div>



		<script>
					document.title = '发版管理';
					var city_s = $('.city-select');
					var station_s = $('.station-select');
					var device_s = $('.device-select');
					var device_s_ios = $('.device-select-ios');
					$('.city-select').select2();
					$('.station-select').select2();
					$('.device-select').select2();
					$('.device-select-ios').select2();
				</script>