<!DOCTYPE HTML>
<html>

  <head>
    <meta charset="utf-8">
    <title>美团配送烽火台</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://xs01.meituan.net/banma_admin/imgs/logo.png">

    <#assign pageVersion = "540012f2">

    <#if page.properties['page.staticPath']?exists>
      <#assign staticPath = page.properties['page.staticPath']>
    <#else>
      <#assign staticPath = "/static">
    </#if>
    <script type="text/javascript">
      var MT = {};
      MT.BOOTSTAMP = new Date().getTime();
      MT.STATIC_ROOT = "${staticPath}";
      var pageVersion = "${pageVersion}";
    </script>
    <script type="text/javascript" src="/static/js/lib/jquery.js"></script>
    <script type="text/javascript" src="/static/js/lib/bootstrap.js"></script>

    
    <#-- 当前页面的引入的样式 start -->
    <#if page.properties['page.css']?exists>
      <link rel="stylesheet" href="${staticPath}/css/page${page.properties['page.css']}.css" />
    </#if>

    <#if page.properties['page.cssmain']?exists>
    <link rel="stylesheet" href="${staticPath}/css/page${page.properties['page.cssmain']}" />
    </#if>

    <#-- 新引入的css文件列表  end -->
  </head>

  <body>
    <div>
      <div id="top-nav" class="fixed skin-1">
        <span class="brand">
          <span>美团配送烽火台</span>
        </span>
        
      </div>