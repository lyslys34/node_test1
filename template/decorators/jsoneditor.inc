  <link href="/static/jsoneditor/jsoneditor.min.css" rel="stylesheet" type="text/css">
  <script src="/static/jsoneditor/jsoneditor.min.js"></script>
  <script>
  	<#if fmObj??>
	  	window.fmObj ='${fmObj}';
	</#if>
  </script>
  <script>
  	$().ready(function () {

      var apiUrl = '/api' + location.pathname;
  		var btnStr = '<button id="json-btn" class="btn btn-success" style="margin:10px 20px;">JSON</button>'
  		// 添加json editor的dialog入口
  		$('#top-nav').append(btnStr);
  		var ctn = $('#pageJson .modal-body').get(0);
  		var jEditor = new JSONEditor(ctn, {});
  		window.jsonEditor = jEditor;
  		var json = JSON.parse(window.fmObj);

  		$('#json-btn').on('click', function() {
  			$('#pageJson').modal();
  		});


      $('#pageJson #save').on('click', function() {
        var obj = jEditor.get();
        var data = {
          data : obj
        };
        
        // 这里需要好好区分form-data和json-data的区别
        // 以及在服务端处理的区别
        $.ajax({
            url: apiUrl,
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: 'application/json',
            success: function(data) {
              console.log(data);
              if (data && data.code === 'A00000') {
                location.reload();
              }
            }
        });
      });

  		jEditor.set(json);
  	});
  </script>
<div class="modal fade" id="pageJson">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>页面model设置</h4>
            </div>
            <div class="modal-body" style="">
                
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" id="save" class="btn btn-success btn-sm">保存</button>
                <button type="button" data-dismiss="modal" id="cancel" class="btn btn-normal btn-sm">隐藏</button>
            </div>
        </div>
    </div>
</div>