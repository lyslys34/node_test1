<!DOCTYPE HTML>
<html>

  <head>
    <meta charset="utf-8">
    <title>美团配送烽火台</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <#include "version.inc">
    <#include "../views/config.ftl">

    <script type="text/javascript">
      var MT = {};
      MT.STATIC_ROOT = "${staticPath}";
      var pageVersion = "${pageVersion}";
      var define = function(){};

      // 静态文件及请求所在服务器地址，可配置测试和生产环境
      var url_config = {
        'monitor': 'http://develop.monitor.banmaadmin.test.sankuai.info',
        'admin': 'http://develop.banmaadmin.test.sankuai.info'
      };
    </script>

    <link rel="shortcut icon" href="/static/imgs/banner.png">
    <link href="/static/css/lib/select2.min.css" rel="stylesheet" />
    <#-- 当前页面的引入的样式 start -->

    <link rel="stylesheet" href="${staticPath}/css/global.css" />

    <#if page.properties['page.cssmain']?exists>
    <link rel="stylesheet" href="${staticPath}/css/page${page.properties['page.cssmain']}?ver=${pageVersion}" />
    </#if>

    <#-- 新引入的css文件列表  end -->
    
    <script type="text/javascript" src="/static/js/lib/jquery.js"></script>
    <script type="text/javascript" src="/static/js/lib/bootstrap.js"></script>
    <script src="/static/js/lib/select2.full.min.js"></script>
    <script src="${staticPath}/js/lib/mod.udatas.js"></script>

    <#-- 兼容性 start -->
    <!--[if lte IE 9]>
    <script type="text/javascript" src="${staticPath}/js/lib/respond.js"></script>
    <script type="text/javascript" src="${staticPath}/js/lib/html5shiv.js"></script>
    <![endif]-->
    <meta name="renderer" content="webkit">
    <#-- 兼容性 end -->

  </head>

  <body>
    <div id="wrapper" class="">
      <div id="top-nav" class="fixed skin-7" style="z-index:1000;">

        <div class="home-wrapper">
          <a href="/" class="brand home-link">
            <span class="project-name">美团配送烽火台</span>
            <div class="title-content"></div>
          </a>
          <a class="btn toggle-btn j-toggle-menu" href="javascript:;">
            <i class="fa fa-bars"></i>
          </a>
        </div>

        <div class="pull-right header-info">
          <a href="/partner/download" class="app-download-wrapper pull-left">
            <span class="vertical-line"></span>
            <i class="fa fa-custom-download"></i>
            <span class="info-text">客户端</span>
          </a>
          <a id="<#if loginUser??>feedbackAdmin<#else>feedback</#if>" href="/feedback/addAndList" class="goto-feedback-wrapper pull-left">
            <span class="vertical-line"></span>
            <i class="fa fa-custom-chat"></i>
            <span class="info-text">反馈</span>
            <span id="brand-feedback" class="badge hide" style="background-color: red;color: #fff;">0</span>
          </a>
          <div class="user-info-wrapper pull-left">
            <a href="javascript:;" class="user-info-content pull-left">
              <span class="vertical-line"></span>
              <span class="info-content">
                <i class="fa fa-custom-user"></i>
                <#if loginUser??>
                  <span class="info-text">${loginUser.name!''}</span>
                <#else>
                  <span class="info-text user-name"></span>
                </#if>
              </span>
              <span class="custom-triangle">
                <i></i>
                <em></em>
              </span>
            </a>
            <ul>
              <li>
                <#if loginUser??>
                  <a href="/admin/logout" class="user-info-list logout logoutConfirm_open" data-popup-ordinal="1" id="adminLogout">
                    <i class="fa fa-power-off"></i>
                    <span class="info-text">退出</span>
                  </a>
                <#else>
                  <form action="${logoutUrl!''}" method="POST">
                    <a href="javascript:;" class="user-info-list logoutConfirm_open form-button"  data-popup-ordinal="1" id="logout _button">
                      <i class="fa fa-power-off"></i>
                      <span class="info-text">退出</span>
                    </a>
                  </form>
                </#if>
              </li>
              <#if loginUser??>
                <#if __mock__?? && __mock__>
                  <li>
                    <a href="/mock/stop" id="stopMock" class="user-info-list">
                      <i class="fa fa-pause"></i>
                      <span class="info-text">停止模拟</span>
                    </a>
                  </li>
                </#if>
              </#if>
            </ul>
          </div>
        </div>

        <!-- 监控指标 -->
        <#include "../views/monitor/monitor_com.ftl">
        <div class="ie-notification hide">
          建议使用chrome浏览器，
          <a href="http://xiazai.sogou.com/detail/34/8/6262355089742005676.html" target="_blank">立即下载》</a>
        </div>
      </div>

      <div id="topholder" style="height:60px">
      </div>
      <div id="toppadding"></div>
      <#if breadcrumb?exists && breadcrumb?size!=0>
        <div id="breadcrumb-bar" >
          <ul class="breadcrumb">
                <#list breadcrumb as menu>
                 <li style="color:#757986;font-size:14px;">${menu.title!''}<span class="<#if (menu_has_next)>divider<#else>active</#if>"></span></li>
                </#list>
          </ul>
        </div>
      </#if>

  <div id="full-screen" class="fixed"></div>
