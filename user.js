var http = require('http');
var url = require('url');
var sign = require('./sign.js');
var utils = require('./utils.js');


var getUserInfo = function(sid, callback) {

  var authInfo = {
    clientId: 'fe.peisong',
    clientSecret: '0b3d19aad3781d7dd6787b5d3fe5d08b',
    uri: '/api/session/' + sid,
    date: new Date(),
    method: 'GET',
  };

  var signMaker = new sign(authInfo);
  //console.log(signMaker);
  //console.log(sid);

  //var url = 'http://api.sso-in.sankuai.com/auth?service=http%3A%2F%2Flocalhost%3A8416%2F%3Fact%3Dlogin';

  var options = {
    hostname: 'api.sso-in.sankuai.com',
    port: 80,
    path: authInfo.uri,
    method: 'GET',
    headers: signMaker
  };

  var req = http.request(options, (res) => {
    // console.log(`STATUS: ${res.statusCode}`);
    // console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      data = chunk;
      // console.log(`BODY: ${chunk}`);
    });
    res.on('end', () => {

      if (callback) {
        callback(data);
      }
    })
  });

  req.on('error', (e) => {
    console.log(`problem with request: ${e.message}`);
  });
  req.end();
};

module.exports = getUserInfo;