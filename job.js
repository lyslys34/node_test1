var fs = require('fs');
var rd = require('rd');
var fse = require('fs-extra');

// 用于读写目录及文件内容的接口
var Job = function () { 
    this._init();
};

Job.prototype = {
    _init: function () {
        this.dirList = {};
    },
    
    dir: function (path) {
        var _this = this;
        var tPath = path;
        var svnReg = /\.svn/ig;
        var list = [];
        
        // 读文件的接口貌似不靠谱
        var dirList = fs.readdirSync(tPath);
        console.log('dirList:', fs.readdirSync(tPath));
        
        list = dirList.filter(function(item) {
            return !svnReg.test(item);
        });
        
        console.log(list);
        return list;
    },
    
    readFile: function (dirPath, fileName) {
        fileCnt = fs.readFileSync(dirPath + '/' + fileName, 'utf-8'); 
        return fileCnt
    },
    
    saveFile: function (dirPath, fileName, content) {
        fs.writeFileSync(dirPath + '/' + fileName, content);
    },
    
    copyDir: function (srcDir, destDir, callback) {
        // 由于在svn管理下，还得考虑过滤掉svn相关的文件
        fse.copy(srcDir, destDir, function (err) {
            if (err) return console.error(err);
            callback(err);
        });
    },
    
    copyFile: function (srcPath, destPath, callback) {
        fse.copy(srcPath, destPath, function (err) {
            if (err) return console.error(err);
            callback(err);
        });
    },
    
    remove: function (dir, callback) {
        fse.remove(dir, function (err) {
            if (err) return console.error(err);
            callback(err);
        });
    }
    
};


//var job = new Job();

//job.dir('admin');

module.exports = Job;
