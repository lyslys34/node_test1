// Freemarker的变量提取正则
const FM_VAR_REG = /\${([a-zA-Z\d._]+)/;
const FM_VAR_REG_G = /\${([a-zA-Z\d._]+)/g;

// Freemarker的IF提取正则
const FM_IF_REG = /<#if ([a-zA-Z\d._]+)|<#if !([a-zA-Z\d._]+)/;
const FM_IF_REG_G =  /<#if (.*?)>/g;

const FM_LIST_REG_G = /<#list ([a-zA-Z\d._]+) as ([a-zA-Z\d._]+)/g;
const FM_LIST_REG = /<#list ([a-zA-Z\d._]+) as ([a-zA-Z\d._]+)/;

const FTL_FILE_REG = /(.*)\/template\/views(.*).ftl$/;


module.exports = {
	FM_VAR_REG: FM_VAR_REG,
	FM_VAR_REG_G: FM_VAR_REG_G,
	FM_IF_REG: FM_IF_REG,
	FM_IF_REG_G: FM_IF_REG_G,
	FTL_FILE_REG: FTL_FILE_REG
};

