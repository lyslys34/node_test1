"use strict";
var crypto = require('crypto');
  
/*
美团验证的逻辑
Authorization = "MWS" + " " + client_id + ":" + signature;
signature = base64( HMAC-SHA1(( string_to_sign, client_secret ) ) );
string_to_sign = HTTP-Verb + " " + REQUEST_URI + "\n" + Date;
HTTP-Verb = GET | POST | DELETE | PUT
REQUEST_URI : 指请求URI，不包含 ? 以及 query_string
Date ：是 Http Header 里的 Date 字段；
*/
function createMTAuthHeaders(authInfo){
    /*
    authInfo = {
        clientId:client_id,
        clientSecret:client_secret,
        uri:REQUEST_URI,
        date:Date,
        method:HTTP-Verb,
    }
    */
    var stringToSign = authInfo.method + ' ' + authInfo.uri + '\n' + authInfo.date.toGMTString(),
        signature = crypto.createHmac('sha1',authInfo.clientSecret).update(stringToSign).digest('base64'),
        authorization = 'MWS ' + authInfo.clientId + ':' + signature;
    return {
        'Date': authInfo.date.toGMTString(),
        'Authorization': authorization,
    };
}

module.exports = createMTAuthHeaders;