'use strict'

const winston = require('winston');
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)(),
    new (winston.transports.File)({
      name: 'info-file',
      filename: 'routerlog-info.log',
      level: 'info'
    }),
    new (winston.transports.File)({
      name: 'error-file',
      filename: 'routerlog-error.log',
      level: 'error'
    })
  ]
});

const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');
const http = require('http');
const config = require('../config');
const cheerio = require('cheerio');
const utils = require('../utils');
const Freemarker = require('freemarker.js');
const filendir = require('filendir');
const jsonFormat = require('json-format');
const replace = require('replace');

const tpl_dir = config.tpl_dir;
const FTL_JSON_MODEL_DIR = path.join(__dirname, '../ftlmodel');

const CONST = require('../constants');

var bodyParser = require('body-parser');

var tpl_cache = [];

// 用于中转ftl的目录
const TMP_FTL_DIR = '/Users/liyishan/test1/ftl_temp';

const fm = new Freemarker({
  viewRoot: tpl_dir,
  options: {
    /** for fmpp */
  }
});

const JSON_MODEL_DIR = path.join(__dirname, '../mock')

// Freemarker的变量提取正则
const FM_VAR_REG = /\${([a-zA-Z\d._]+)/;
const FM_VAR_REG_G = /\${([a-zA-Z\d._]+)/g;

// Freemarker的IF提取正则
const FM_IF_REG = /<#if ([a-zA-Z\d._]+)|<#if !([a-zA-Z\d._]+)/;
const FM_IF_REG_G =  /<#if (.*?)>/g;

const FM_LIST_REG_G = /<#list ([a-zA-Z\d._]+) as ([a-zA-Z\d._]+)/g;
const FM_LIST_REG = /<#list ([a-zA-Z\d._]+) as ([a-zA-Z\d._]+)/;

const ftlFileReg = /(.*)\/views(.*).ftl$/;


var rd = require('rd');
var tplPath = config.tpl_dir;
var files = rd.readSync(tplPath);


// 启动时重新构建一下中转的目录
// utils.exists(tpl_dir, TMP_FTL_DIR, utils.copyDir);


function postProcessModel (obj, attributeName, parent) {
	if (utils.isEmptyObj(obj)) {

		if (parent && attributeName) {
			parent[attributeName] = null;
		}
		return;
	}

	for (let name in obj) {
		postProcessModel(obj[name], name, obj);
	}
}


function createFtlModelIntf(ftlPath, route) {
	let modelPath = path.join(FTL_JSON_MODEL_DIR, route +'.json');
	let apiRoute = '/api' + route;
	console.log(apiRoute);
	router.use(bodyParser.json());

	router.post(apiRoute, function(req, res) {
		// console.info(req);
		// console.info(req.params);
		console.info(req.body);
		let jsonObj = req.body;

		filendir.wa(modelPath, jsonFormat(jsonObj), function(err) {
			if (err) {
				logger.error(err);
				res.json({code: 'B00001', msg:'写model文件错误', err: err});
			}

			res.json({code: 'A00000'});
		});
	});
}

// TODO 后面改为异步调用
function getFtlModel(route) {
	let modelPath = path.join(JSON_MODEL_DIR, route +'.json');
	let obj = {};

	if (fs.existsSync(modelPath)) {
		console.log('存在ftl model文件');
		let cnt = fs.readFileSync(modelPath);
		obj = JSON.parse(cnt);
		console.log('直接读取到model文件', obj);
		// createFtlModelIntf(ftlPath, route);
		return obj;
	} 
	return {};
}

files.forEach((data, index) => {
	// console.log(data);
	// 满足ftl尾缀的文件才建立对应的路由
	if (ftlFileReg.test(data)) {
		var m = data.match(ftlFileReg);
		var filePath = m[0];
		var route = m[2];

		// console.log()

		// console.log(route);
	    // console.log(filePath);

		router.get(route, function (req, res) {
			console.log(route);
			console.log(filePath);

			// 处理所有模板文件中的<@auth标签
			// utils.exists(tpl_dir, TMP_FTL_DIR, utils.copyDir);
			var targetFiles = rd.readSync(tpl_dir + '/views');
			targetFiles.forEach((data) => {
				// console.log(data);
				if (ftlFileReg.test(data)) {
					var fPath = data.match(ftlFileReg)[2];
					var content = fs.readFileSync(data, 'utf8');
					
				}
			});

			var data = {
			    'funModuleList': [{
			      "funName": "fun1",
			      "icon": "/static/imgs/android.png",
			      "isOutLink": 0,  
			      "url": "www.taobao.com",
			      "weight": 0      },
			    {
			      "funName": "fun2",
			      "icon": "icon",
			      "isOutLink": 0,
			      "url": "www.baidu.com",
			      "weight": 1
			    },
			    {
			      "funName": "fun2",
			      "icon": "icon",
			      "isOutLink": 0,
			      "url": "www.baidu.com",
			      "weight": 1
			    },
			    {
			      "funName": "fun2",
			      "icon": "icon",
			      "isOutLink": 0,
			      "url": "www.baidu.com",
			      "weight": 1
			    },
			    {
			      "funName": "fun2",
			      "icon": "icon",
			      "isOutLink": 0,
			      "url": "www.baidu.com",
			      "weight": 1
			    },
			    {
			      "funName": "fun2",
			      "icon": "icon",
			      "isOutLink": 0,
			      "url": "www.baidu.com",
			      "weight": 1
			    },
			    {
			      "funName": "fun2",
			      "icon": "icon",
			      "isOutLink": 0,
			      "url": "www.baidu.com",
			      "weight": 1
			    },
			    {
			      "funName": "fun2",
			      "icon": "icon",
			      "isOutLink": 0,
			      "url": "www.baidu.com",
			      "weight": 1
			    },
			    {
			      "funName": "fun2",
			      "icon": "icon",
			      "isOutLink": 0,
			      "url": "www.baidu.com",
			      "weight": 1
			    },
			    {
			      "funName": "fun2",
			      "icon": "icon",
			      "isOutLink": 0,
			      "url": "www.baidu.com",
			      "weight": 1
			    }
			  ]};

			// data = "&lt;p&gt;&lt;strong&gt;呵呵&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;&lt;em&gt;&lt;span style=&quot;font-size:18px&quot;&gt;&lt;/span&gt;&lt;/em&gt;&lt;img src=&quot;http://p0.meituan.net/xianfu/53a61cce2458cd376f805274c7f6707c32768.png&quot; _src=&quot;http://p0.meituan.net/xianfu/53a61cce2458cd376f805274c7f6707c32768.png&quot;/&gt;&lt;span style=&quot;font-size:48px&quot;&gt;&lt;span&gt;&lt;span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;";

			// data = "&lt;ol&gt;&lt;li&gt;&lt;p style=&quot;font-family: 微软雅黑; margin: inherit; padding: 0px; color: rgb(82, 82, 82); line-height: 30px; font-size: medium; position: relative; font-variant-ligatures: normal; orphans: 2; white-space: normal; widows: 2;&quot;&gt;考场文开头技巧例谈&lt;br/&gt;&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p style=&quot;font-family: 微软雅黑; margin: inherit; padding: 0px; color: rgb(82, 82, 82); line-height: 30px; font-size: medium; position: relative; font-variant-ligatures: normal; orphans: 2; white-space: normal; widows: 2;&quot;&gt;　　俗话说：“万事开头难。”对于考场作文来说，写好作文开头尤为重要。因为老师阅卷时首先看的便是第一段，所以临考写作时一定要写好开头段，紧紧抓住老师的目光，让老师不由自主的随着你的思路走。总的来说开头的方法多种多样，有一条是最重要的，那就是：简洁明了，迅速入题。请看近年各地中考满分文的开头技巧。&lt;br/&gt;&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p style=&quot;font-family: 微软雅黑; margin: inherit; padding: 0px; color: rgb(82, 82, 82); line-height: 30px; font-size: medium; position: relative; font-variant-ligatures: normal; orphans: 2; white-space: normal; widows: 2;&quot;&gt;　　技巧一：设计悬念，吸引读者&lt;br/&gt;&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p style=&quot;font-family: 微软雅黑; margin: inherit; padding: 0px; color: rgb(82, 82, 82); line-height: 30px; font-size: medium; position: relative; font-variant-ligatures: normal; orphans: 2; white-space: normal; widows: 2;&quot;&gt;　　我快要死了——&lt;br/&gt;&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p style=&quot;font-family: 微软雅黑; margin: inherit; padding: 0px; color: rgb(82, 82, 82); line-height: 30px; font-size: medium; position: relative; font-variant-ligatures: normal; orphans: 2; white-space: normal; widows: 2;&quot;&gt;&lt;/p&gt;&lt;/li&gt;&lt;/ol&gt;&lt;p style=&quot;font-family: 微软雅黑; margin: inherit; padding: 0px; color: rgb(82, 82, 82); line-height: 30px; font-size: medium; position: relative; font-variant-ligatures: normal; orphans: 2; widows: 2;&quot;&gt;　　我躺在病床上，四周黑漆漆的一片，十分寂静，偌大的房间里，只能听得见我微弱的呼吸声。护士只有到吃药、打针的时候才会进来，而且很少和我说话。我已经习惯了，我不会有太多的报怨，因为我知道我快要死了。我凝视着窗外，告诉自己要坦然面对死亡。（选自2002年陕西省中考满分文《感受生活之美》）&lt;br/&gt;&lt;/p&gt;&lt;p style=&quot;font-family: 微软雅黑; margin: inherit; padding: 0px; color: rgb(82, 82, 82); line-height: 30px; font-size: medium; position: relative; font-variant-ligatures: normal; orphans: 2; widows: 2;&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p style=&quot;white-space: normal;&quot;&gt;商品退/换货&lt;/p&gt;&lt;ul&gt;&lt;li&gt;&lt;p style=&quot;white-space: normal;&quot;&gt;退换货时限：客户自签收之日起7日内可申请无理由退换货；&lt;br/&gt;&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p style=&quot;white-space: normal;&quot;&gt;退换货的商品不能影响其二次销售，即如同商品签收时完好无损 ；&lt;br/&gt;&lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;p style=&quot;white-space: normal;&quot;&gt;注意事项&lt;/p&gt;&lt;ul&gt;&lt;li&gt;&lt;p style=&quot;white-space: normal;&quot;&gt;图片因拍摄问题可能有一定色差，请以实物为准；&lt;br/&gt;&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p style=&quot;white-space: normal;&quot;&gt;目前仅支持以下情况的退换货：若一个订单内有多个商品，则订单内所有物品需要同时进行退换货；&lt;br/&gt;&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p style=&quot;white-space: normal;&quot;&gt;物料领取时间：周一至周五，具体以自提点安排为准；&lt;br/&gt;&lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;"

			// data =  "%3Cp%3E%E6%B4%BB%E5%8A%A8%E8%AF%B4%E6%98%8E%26nbsp%3B%3C%2Fp%3E%3Cp%3E1.%E6%9C%AA%E9%A2%86%E5%8F%96%E8%BF%87%E6%B4%BB%E5%8A%A8%E5%85%8D%E8%B4%B9%E5%8F%91%E6%94%BE%E7%89%A9%E6%96%99%E7%9A%84%E9%AA%91%E6%89%8B%E5%8F%AF%E5%8F%82%E4%B8%8E%E4%BC%98%E6%83%A0%26nbsp%3B%3C%2Fp%3E%3Cp%3E2.%E9%A6%96%E4%BB%B6%E8%B4%AD%E4%B9%B0%E5%8F%AF%E5%8F%82%E4%B8%8E%E4%BC%98%E6%83%A0%3C%2Fp%3E%3Cp%3E%26nbsp%3B%E2%80%BB%E9%9C%80%E5%90%8C%E6%97%B6%E6%BB%A1%E8%B6%B3%E4%BB%A5%E4%B8%8A%E4%B8%A4%E4%B8%AA%E6%9D%A1%E4%BB%B6%E6%96%B9%E5%8F%AF%E5%8F%82%E4%B8%8E%E4%BC%98%E6%83%A0%E6%B4%BB%E5%8A%A8%E2%80%BB%26nbsp%3B%3C%2Fp%3E%3Cp%3E%E5%93%81%E5%90%8D%EF%BC%9A%E7%BE%8E%E5%9B%A2%E4%BC%97%E5%8C%85%E5%AE%9A%E5%88%B6%E4%BF%9D%E6%B8%A9%E7%AE%B1%2F%E5%A4%96%E5%8D%96%E7%AE%B1%26nbsp%3B%3C%2Fp%3E%3Cp%3E%E5%B0%BA%E5%AF%B8%EF%BC%9A%E5%A4%96%E5%BE%84+50*37*37+%26nbsp%3B%E5%86%85%E5%BE%84+48*35*35%26nbsp%3B%3C%2Fp%3E%3Cp%3E%E5%AE%B9%E9%87%8F%EF%BC%9A58%E5%8D%87%26nbsp%3B%3C%2Fp%3E%3Cp%3E%E9%87%8D%E9%87%8F%EF%BC%9A2kg%26nbsp%3B%3C%2Fp%3E%3Cp%3E%E7%89%B9%E7%82%B9%EF%BC%9A%E4%BF%9D%E6%B8%A9%E4%BF%9D%E5%86%B7+%E6%98%93%E6%B8%85%E6%B4%97%0A%0A%E8%B4%AD%E4%B9%B0%E8%AF%B4%E6%98%8E%EF%BC%88%E5%95%86%E5%93%81%E9%9C%80%E8%87%AA%E6%8F%90+%26nbsp%3B%E8%AF%B7%E8%B0%A8%E6%85%8E%E9%80%89%E6%8B%A9%E8%87%AA%E6%8F%90%E7%82%B9%EF%BC%89%26nbsp%3B%3C%2Fp%3E%3Cp%3E1.%E5%BD%93%E9%9D%A2%E9%AA%8C%E6%94%B6%3C%2Fp%3E%3Cp%3E%E2%91%A0%E6%82%A8%E5%9C%A8%E7%AD%BE%E6%94%B6%E5%95%86%E5%93%81%E5%89%8D%EF%BC%8C%E5%BA%94%E5%AF%B9%E5%95%86%E5%93%81%E8%BF%9B%E8%A1%8C%E6%A3%80%E9%AA%8C%EF%BC%8C%E9%9C%80%E6%A3%80%E6%9F%A5%E5%95%86%E5%93%81%E5%A4%96%E5%8C%85%E8%A3%85%E3%80%81%E5%A4%96%E8%A7%82%E6%98%AF%E5%90%A6%E5%AE%8C%E5%A5%BD%E3%80%81%E9%85%8D%E4%BB%B6%E6%98%AF%E5%90%A6%E9%BD%90%E5%85%A8%EF%BC%8C%E8%AF%B7%E5%9C%A8%E5%95%86%E5%93%81%E5%AE%8C%E5%A5%BD%E6%97%A0%E6%8D%9F%E7%9A%84%E6%83%85%E5%86%B5%E4%B8%8B%EF%BC%8C%E5%86%8D%E5%B0%86%E5%95%86%E5%93%81%E6%8F%90%E8%B5%B0%EF%BC%9B%3C%2Fp%3E%3Cp%3E%E2%91%A1%E5%A6%82%E8%87%AA%E6%8F%90%E7%9A%84%E6%97%B6%E5%80%99%E6%A3%80%E9%AA%8C%E5%87%BA%E5%95%86%E5%93%81%E6%9C%89%E8%B4%A8%E9%87%8F%E9%97%AE%E9%A2%98%EF%BC%8C%E8%AF%B7%E5%9C%A8%E8%87%AA%E6%8F%90%E7%82%B9%E7%9B%B4%E6%8E%A5%E4%B8%8E%E8%B4%9F%E8%B4%A3%E4%BA%BA%E6%B2%9F%E9%80%9A%EF%BC%8C%E8%BF%9B%E8%A1%8C%E9%80%80%E6%8D%A2%E8%B4%A7%EF%BC%9B%26nbsp%3B%3C%2Fp%3E%3Cp%3E%E2%91%A2%E8%AF%B7%E6%82%A8%E8%AE%A4%E7%9C%9F%E9%AA%8C%E8%B4%A7%EF%BC%8C%E8%87%AA%E6%8F%90%E4%B9%8B%E5%90%8E%E4%B8%8D%E6%94%AF%E6%8C%81%E8%B4%A8%E9%87%8F%E9%97%AE%E9%A2%98%E7%9A%84%E9%80%80%E6%8D%A2%E8%B4%A7%E3%80%82%3C%2Fp%3E%3Cp%3E%26nbsp%3B2.%E5%95%86%E5%93%81%E9%80%80%2F%E6%8D%A2%E8%B4%A7%26nbsp%3B%3C%2Fp%3E%3Cp%3E%E2%91%A0%E9%80%80%E6%8D%A2%E8%B4%A7%E6%97%B6%E9%99%90%EF%BC%9A%E5%AE%A2%E6%88%B7%E8%87%AA%E7%AD%BE%E6%94%B6%E4%B9%8B%E6%97%A5%E8%B5%B77%E6%97%A5%E5%86%85%E5%8F%AF%E7%94%B3%E8%AF%B7%E6%97%A0%E7%90%86%E7%94%B1%E9%80%80%E6%8D%A2%E8%B4%A7%EF%BC%9B%3C%2Fp%3E%3Cp%3E%E2%91%A1%E9%80%80%E6%8D%A2%E8%B4%A7%E7%9A%84%E5%95%86%E5%93%81%E4%B8%8D%E8%83%BD%E5%BD%B1%E5%93%8D%E5%85%B6%E4%BA%8C%E6%AC%A1%E9%94%80%E5%94%AE%EF%BC%8C%E5%8D%B3%E5%A6%82%E5%90%8C%E5%95%86%E5%93%81%E7%AD%BE%E6%94%B6%E6%97%B6%E5%AE%8C%E5%A5%BD%E6%97%A0%E6%8D%9F+%EF%BC%9B%26nbsp%3B%3C%2Fp%3E%3Cp%3E3.%E6%B3%A8%E6%84%8F%E4%BA%8B%E9%A1%B9%26nbsp%3B%3C%2Fp%3E%3Cp%3E%E2%91%A0%E5%9B%BE%E7%89%87%E5%9B%A0%E6%8B%8D%E6%91%84%E9%97%AE%E9%A2%98%E5%8F%AF%E8%83%BD%E6%9C%89%E4%B8%80%E5%AE%9A%E8%89%B2%E5%B7%AE%EF%BC%8C%E8%AF%B7%E4%BB%A5%E5%AE%9E%E7%89%A9%E4%B8%BA%E5%87%86%EF%BC%9B%26nbsp%3B%3C%2Fp%3E%3Cp%3E%E2%91%A1%E7%9B%AE%E5%89%8D%E4%BB%85%E6%94%AF%E6%8C%81%E4%BB%A5%E4%B8%8B%E6%83%85%E5%86%B5%E7%9A%84%E9%80%80%E6%8D%A2%E8%B4%A7%EF%BC%9A%E8%8B%A5%E4%B8%80%E4%B8%AA%E8%AE%A2%E5%8D%95%E5%86%85%E6%9C%89%E5%A4%9A%E4%B8%AA%E5%95%86%E5%93%81%EF%BC%8C%E5%88%99%E8%AE%A2%E5%8D%95%E5%86%85%E6%89%80%E6%9C%89%E7%89%A9%E5%93%81%E9%9C%80%E8%A6%81%E5%90%8C%E6%97%B6%E8%BF%9B%E8%A1%8C%E9%80%80%E6%8D%A2%E8%B4%A7%EF%BC%9B%0A%E2%91%A2%E7%89%A9%E6%96%99%E9%A2%86%E5%8F%96%E6%97%B6%E9%97%B4%EF%BC%9A%E5%91%A8%E4%B8%80%E8%87%B3%E5%91%A8%E4%BA%94%EF%BC%8C%E5%85%B7%E4%BD%93%E4%BB%A5%E8%87%AA%E6%8F%90%E7%82%B9%E5%AE%89%E6%8E%92%E4%B8%BA%E5%87%86%EF%BC%9B%26nbsp%3B%3C%2Fp%3E%3Cp%3E%E2%91%A3%E9%A6%96%E5%8D%95%E9%80%80%E8%B4%A7%E5%90%8E%EF%BC%8C%E5%86%8D%E8%B4%AD%E4%B9%B0%E6%97%A0%E6%B3%95%E4%BA%AB%E5%8F%97%E4%BC%98%E6%83%A0%EF%BC%8C%E8%BF%98%E8%AF%B7%E8%B0%A8%E6%85%8E%E9%80%89%E6%8B%A9%E8%87%AA%E6%8F%90%E7%82%B9%E3%80%82%26nbsp%3B%3C%2Fp%3E%3Cp%3E4.%E9%80%80%E6%8D%A2%E8%B4%A7%E6%B5%81%E7%A8%8B%26nbsp%3B%3C%2Fp%3E%3Cp%3E%E2%91%A0%E9%9C%80%E9%AA%91%E6%89%8B%E6%90%BA%E5%B8%A6%E5%95%86%E5%93%81%E5%92%8C%E8%BA%AB%E4%BB%BD%E8%AF%81%EF%BC%8C%E5%89%8D%E5%BE%80%E8%B4%AD%E4%B9%B0%E6%97%B6%E6%89%80%E9%80%89%E7%9A%84%E8%87%AA%E6%8F%90%E7%82%B9%E8%BF%9B%E8%A1%8C%E9%80%80%E6%8D%A2%E8%B4%A7%EF%BC%9B%26nbsp%3B%3C%2Fp%3E%3Cp%3E%E2%91%A1%E7%94%B1%E8%87%AA%E6%8F%90%E7%82%B9%E8%B4%9F%E8%B4%A3%E4%BA%BA%E5%AF%B9%E5%95%86%E5%93%81%E8%BF%9B%E8%A1%8C%E6%A3%80%E6%9F%A5%EF%BC%8C%E5%9C%A8%E4%B8%8D%E5%BD%B1%E5%93%8D%E4%BA%8C%E6%AC%A1%E9%94%80%E5%94%AE%E4%B8%94%E5%95%86%E5%93%81%E5%9C%A8%E9%80%80%E6%8D%A2%E8%B4%A7%E6%97%B6%E9%99%90%E5%86%85%EF%BC%8C%E5%88%99%E5%8F%AF%E8%BF%9B%E8%A1%8C%E9%80%80%E6%8D%A2%E8%B4%A7%EF%BC%8C%E5%90%A6%E5%88%99%E6%97%A0%E6%B3%95%E5%8A%9E%E7%90%86%EF%BC%9B%26nbsp%3B%3C%2Fp%3E%3Cp%3E%E2%91%A2%E8%8B%A5%E9%AA%91%E6%89%8B%E4%B8%8D%E8%AE%A4%E5%8F%AF%E8%87%AA%E5%B7%B1%E7%9A%84%E5%95%86%E5%93%81%E5%BD%B1%E5%93%8D%E4%BA%8C%E6%AC%A1%E9%94%80%E5%94%AE%E8%80%8C%E4%BA%A7%E7%94%9F%E7%BA%A0%E7%BA%B7%EF%BC%8C%E5%8F%AF%E8%81%94%E7%B3%BB%E5%AE%A2%E6%9C%8D%E4%BA%BA%E5%91%98%E5%8D%8F%E5%8A%A9%E5%A4%84%E7%90%86%E3%80%82%3C%2Fp%3E%3Cp%3E%E2%91%A3%E5%90%84%E5%9F%8E%E5%B8%82%E8%B4%9F%E8%B4%A3%E4%BA%BA%E7%94%B3%E8%AF%B7%E9%80%80%E6%AC%BE%E6%97%B6%E9%97%B4%E6%88%AA%E6%AD%A2%E6%AF%8F%E5%91%A8%E5%9B%9B15%3A00%EF%BC%8C%E9%80%80%E6%AC%BE%E6%97%B6%E9%97%B4%EF%BC%9A%E5%BD%93%E5%91%A8%E5%91%A8%E4%BA%94%E2%80%94%E6%AC%A1%E5%91%A8%E5%91%A8%E4%B8%80%EF%BC%9B%E6%88%AA%E6%AD%A2%E6%97%B6%E9%97%B4%E4%BB%A5%E5%90%8E%E7%94%B3%E8%AF%B7%E7%9A%84%EF%BC%8C%E9%80%80%E6%AC%BE%E6%97%B6%E9%97%B4%E5%BE%80%E5%90%8E%E6%8E%A8%E4%B8%80%E5%91%A8%EF%BC%8C%E8%BF%98%E8%AF%B7%E7%9F%A5%E6%82%89%E3%80%82%3C%2Fp%3E";


			let obj = {
				code: 0,
				data: JSON.stringify(data),
				resultCode: 1,
				bmTrainId: 1113
			};


			obj = Object.assign(obj, {
					auth: 1,
					app: {
						type: '1',
						type2: '2'
					}
			});

			try {
				obj = getFtlModel(route);
			} catch (e) {
				console.error("获取ftl model 错误 e: ", e);
				obj = {};
				return;
			}
			

			// console.info('FTL模板对应的mock model is: ', obj);
			
			// console.log('/views' + route + '.ftl')
			fm.render('/views' + route + '.ftl', obj, function (err, html, output) {
				console.log(output);
				res.send(html);
			
			});
		});
	}

});

module.exports = router;