
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;">

<head>
	<#include "../common-header.ftl" />
	<title>申请开通</title>
	<link href="/static/css/page/account/common.css" rel="stylesheet" type="text/css"/>
	<link href="/static/css/page/account/zb_open_ctl.css" rel="stylesheet" type="text/css"/>
</head>
	
<body style="background-color: #F5F5F5">

<div class="container-fluid" style="height:100%;">
	<div class="row" style="margin-top:30px;height:20px">
		<div class="col-xs-12 big-f pic-left">申请成功</div>
	</div>	
	<div class="row" style="margin-top:22px;">
		<div class="col-xs-12 center-text mid-f" style="color:#959595;padding-left:35px">快拉上更多伙伴加入吧，人气越高，我们越快到来哦~</div>
	</div>	

	<div class="row" style="height:44px">
		<div class="col-xs-12">
			<div style="color: red; height: 25px; line-height: 20px; font-size: 10pt; width: 100%; text-align: center" id="error-msg"></div>
		</div>
	</div>
	

	<div class="row">
		<div class="col-xs-12">
			<button id="confirm" type="button" class="confirm btn green huge-f bu">邀请好友</button>
		</div>
	</div>




	<#include "../common-params.ftl" />

	<script type="text/javascript" >
	$(document).ready(function() {
		$("#confirm").click(function(){
			$("#common-form").attr("action","/recommend/invite");
			$("#common-form").submit();
		});
		
	});
	</script>
</body>

</html>

