
<!DOCTYPE html>
<html lang="zh-CN"style="height: 100%;" >

<head>

<#include "../common-header.ftl" />
    <title>提现记录</title>
</head>

<body style="height: 100%;background-color: #F5F5F5">

<script type="text/javascript" src="/static/js/page/account/common.js"></script>
<link href="/static/css/page/account/common.css" rel="stylesheet" type="text/css"/>
<link href="/static/css/page/account/payment.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/static/js/lib/iscroll-probe.js"></script>
<script type="text/javascript" src="/static/js/page/account/payment_list.js"></script>

<div class="container-fluid" style="height:100%">
	<div class="row wrap-border-b" style="height:15px"></div>
	
<#if payments?exists && payments?size != 0>
    <div id="iscroll-wrapper">
        <div id="scroller">
        	<div id="page" >
        	<#list payments as payment>
	        	<div class="row info smal-f">
					<div class="col-xs-6 left" style="color:#999999">${(payment.time*1000)?number_to_date} ${(payment.time*1000)?number_to_time}</div>
					<div class="col-xs-6 right" style="color:#999999">
						${payment.statusMsg!''}
					</div>
				</div>
				<div class="row detail item-border-b mid-f">
					<div class="col-xs-6 left">提现</div>
					<div class="col-xs-6 right">${payment.money?string("0.00")}</div>
				</div>
			</#list>
			</div>
            <div id="load-indicator"></div>
        </div>
    </div>
<#else>
    <div style="width: 100%; margin-top: 60%; text-align: center; font-size: 1.2em; color: GrayText">
        暂无提现记录
    </div>

</#if>





<#include "../common-params.ftl" />

</div>

</body>

</html>

