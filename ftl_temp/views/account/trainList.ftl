
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;" >

<head>

    <#include "../common-header.ftl" />
    <title>培训课堂</title>
    
</head>

<body style="height: 100%;background-color: #F5F5F5">
	<link href="/static/css/page/account/account_info.css" rel="stylesheet">
	<link href="/static/css/page/account/common.css" rel="stylesheet">
	<script type="text/javascript" src="/static/js/page/train/imgClick.js"></script>
    <script type="text/javascript" src="/static/js/page/account/common.js"></script>
	<style>
	.container-fluid:before{ display: none;
    content:""; }
	</style>

<div class="container-fluid" style="height:83px  " >
<#if courseList??>
<#list courseList as course>
	<div class="row item wrap-border-t  img_container" statusCode="${course.statusCode!""}" style=" height:83px ;line-height:83px" courseId="${course.courseId!""}" >
		<div class="col-xs-5 i-key ">
			
           <div class="row-xs-5 big-f  ">${course.title!""}</div>
           <div class="row-xs-7 i-val mid-f" style="text-align:left;line-height:25px;">${course.content!""}</div>

		</div>
		<div class="col-xs-7 i-val  big-f" style="height:83px ;line-height:83px;float:right;" }>
			<font color=${course.color!""}>${course.status!""}</font>
			<div class="col-xs-1 img_container"  statusCode="${course.statusCode!""}" style="float:right;"><img  src="/static/imgs/next.png" ></div>
		</div>
		
	</div>



</#list>
</#if>
</div>


</body>
<#include "../common-params.ftl" />
</html>