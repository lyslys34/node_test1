
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;">

<head>

<#include "../common-header.ftl" />
    <title>选择银行</title>
</head>

<body style="background-color: #F5F5F5">

<link href="/static/css/page/account/common.css" rel="stylesheet" type="text/css"/>
<link href="/static/css/page/account/bank_list.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/static/js/page/account/common.js"></script>
<script type="text/javascript" src="/static/js/page/account/bank_list.js"></script>

<div class="container-fluid" style="height:100%;padding:0">
	<div class="row wrap-border-b" style="height:15px"></div>
	<#if banks?exists>
            <#list banks as bank>
				<div class="item-r item-border-b big-f">
					<div class="col-xs-12 account-list-entry-value" id="${bank.id !''}">
						<span>${bank.name !''}</span>
						<div class="img"><img src="/static/imgs/forward_mark.png" ></div>
					</div>
				</div>
			</#list>
	</#if>
<#include "../common-params.ftl" />

</div>

</body>

</html>

