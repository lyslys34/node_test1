
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;">

<head>

<#include "../common-header.ftl" />
    <title>选择开户城市</title>
</head>

<body style="background-color: #F5F5F5">

<link href="/static/css/page/account/city_list.css" rel="stylesheet" type="text/css"/>
<link href="/static/css/page/account/common.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/static/js/page/account/common.js"></script>

<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/jquery-ui-1.10.4.js"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.position.js"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.menu.js"></script>
<script type="text/javascript" src="/static/js/lib/jquery-ui-1.10.4/js/ui/jquery.ui.autocomplete.js"></script>

<script type="text/javascript" src="/static/js/page/account/city_list.js"></script>

<style>
.clear-pad {
    border-bottom: 1px solid #DCDCDC;
}
</style>
<div class="container-fluid" style="height:100%;background-color:F5F5F5;padding:0 16px">
	<div class="row wrap-border-b" style="height:48px">
		<div class="col-xs-12 search-contain">
			<div id="search-input-contain" class="search search-ip big-f">
				<input type="text" style="width:100%;height: 100%; border: none;" placeholder="搜索" id="item-search" />
			</div>
			<div id="cancel-i" class="cancel cancel-i">&nbsp</div>
			<span id="cancel-bu" class="cancel cancel-bu">取消</span>
		</div>
	</div>
	
	
	<div class="row  search-control" style="color:#000000;background-color:#FFFFFF  ">
		<div class="col-xs-12 center-block">
			<#if cities.hotCities?exists>
			<div class="row hot-c">
				<div class="col-xs-12 smal-f">热门城市</div>
			</div>
			<div class="row item-contain wrap-border-b">
				<#list cities.hotCities as city>
				<div class="col-xs-3 " style="margin-bottom:5px;padding-left:0px;">
					<div class="c-item  account-list-entry-value smal-f" id="${city.id !''}">${city.name !''}</div>
				</div>
				</#list>
			</div>
			</#if>
			<div class="row" style="color: #959595;margin-top:22px">
				<div class="col-xs-12 smal-f">全部城市</div>
			</div>
			<#if cities.citiesMap?exists>
				<#list cities.citiesMap?keys as key>
					<#assign list = cities.citiesMap[key]>
					<div class="row city-id">
						<div class="col-xs-12 smal-f">${key}</div>
					</div>
					<div class="row item-contain">
					<#list list as city>
						<div class=" clear-pad ">
							<div class="city-line  account-list-entry-value smal-f"  style="width:100%" id="${city.id !''}">${city.name !''}</div>
						</div>
					</#list>
					</div>
				</#list>
			</#if>
		</div>			
    </div>

    <div class="row  search-hint" style="color:#000000;background-color:#FFFFFF  ">
		<div class="col-xs-12 center-block">
<#if cities.citiesMap?exists>
				<#list cities.citiesMap?keys as key>
					<#assign list = cities.citiesMap[key]>
					<div class="row item-contain">
					<#list list as city>
						<div class=" clear-pad  cityitem-container   " style="100%">
							<div class="city-line  city-hint account-list-entry-value smal-f " style="width:100%" id="${city.id !''}">${city.name !''}</div>
						</div>
					</#list>
					</div>
				</#list>
			</#if>
		</div>
	</div>

<#include "../common-params.ftl" />

</div>

</body>

</html>

