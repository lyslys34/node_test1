
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;" >

<head>

<#include "../common-header.ftl" />
    <title>结算信息</title>
</head>

<body style="background-color: #F5F5F5">

<link href="/static/css/page/account/settle_info.css" rel="stylesheet" type="text/css"/>
<link href="/static/css/page/account/common.css" rel="stylesheet" type="text/css"/>


<div class="container-fluid" style="height:100%;background-color:F5F5F5">
    <div class="row" style="background-color:#FFB157;color:#FFFFFF">
        <div class="col-xs-12 mid-f hide" id="top-tip" style="padding:10px 10px;">

        </div>
    </div>
    <div class="row" style="height:55px">
        <div class="col-xs-8 info-h mid-f">请绑定您本人的银行卡</div>
        <div class="col-xs-4" style="padding-top: 15px; padding-left: 50px">
            <button id="edit" class="btn green">修改</button>
        </div>
    </div>
    <div class="row item wrap-border-t">
        <div class="col-xs-6 i-key big-f">持卡人</div>
        <div class="col-xs-6 i-val smal-f" id="user" style="color:#959595"></div>
    </div>
    <div class="row item item-border-t">
        <div class="col-xs-4 i-key big-f">卡号</div>
        <div id="card" class="col-xs-8 i-val smal-f""></div>
    </div>
    <div id="city" class="row item item-border-t">
        <div class="col-xs-5 i-key big-f">银行所在地</div>
        <div id="cityName" class="col-xs-7 i-val smal-f"></div>
    </div>
    <div id="bank" class="row item wrap-border-b item-border-t">
        <div class="col-xs-3 i-key big-f">开户行</div>
        <div class="col-xs-9">
            <div class="bank-if i-val">
                <div id="bankName" ></div>
                <div id="branchName" ></div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 20px">
        <div class="col-xs-12">
            <div style="color: #ff5a5a; line-height: 25px; width: 100%; text-align: center">
                注意：若信息录入有误，将影响款项到账。
            </div>
        </div>
    </div>
    

    <div class="row">
        <div class="col-xs-12"><div style="color: #ff5a5a;line-height: 20px; font-size: 10pt; width: 100%; text-align: center" id="error-msg"></div></div>
    </div>
<#include "../common-params.ftl" />

</div>

<script type="text/javascript" src="/static/js/page/account/common.js"></script>
<script type="text/javascript" src="/static/js/page/account/settle_info.js?t=03021"></script>

</body>

</html>

