
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;" >

<head>

<#include "../common-header.ftl" />
    <title>绑定银行卡</title>
</head>

<body style="background-color: #F5F5F5">

<link href="/static/css/page/account/bank_info.css" rel="stylesheet" type="text/css"/>
<link href="/static/css/page/account/common.css" rel="stylesheet" type="text/css"/>

<div class="container-fluid" style="height:100%;background-color:F5F5F5">

	<#if status?exists>
		<#if status == 0 >
		<div class="row" style="background-color:#FFB157;color:#FFFFFF">
	        <div class="col-xs-12 mid-f" style="padding:10px 10px;">
	        	请尽快绑定银行卡，以免影响打款!
	        </div>
	    </div>
		<#elseif status == 1>
		<div class="row" style="background-color:#FFB157;color:#FFFFFF">
	        <div class="col-xs-12 mid-f" style="padding:10px 10px;">
	        	银行卡正在校验中，校验期间不可修改结算信息。
	        </div>
	    </div>
		<#elseif status == 2>
		<div class="row" style="background-color:#FFB157;color:#FFFFFF">
	        <div class="col-xs-12 mid-f" style="padding:10px 10px;">
	        	您绑定的银行卡信息有误，请尽快修改!
	        	<#if errorMsg?exists>
	        		<br/>原因：${errorMsg!}
	        	</#if>
	        </div>
	    </div>
		<#else>
		</#if>
	</#if>
	
	<div class="row" style="height:55px">
		<div class="col-xs-12 info-h mid-f">请绑定您本人的银行卡</div>
	</div>
	<div class="row item wrap-border-t">
		<div class="col-xs-6 i-key big-f">持卡人</div>
		<div class="col-xs-6 i-val item-val-f" >
			<input id="user" placeholder="请输入姓名" style="height:32px; line-height:32px; width: 100%; color:#959595; border:none; outline:medium" value="">
		</div>
	</div>
	<div class="row item item-border-t">
		<div class="col-xs-4 i-key big-f">卡号</div>
		<div class="col-xs-8 i-val item-val-f" style="padding-left:0">
			<input id="card"  class="ui-cardnum-show" placeholder="请输入银行卡号" style="height:32px;line-height:32px;width: 100%; color:#959595; border:none; outline:medium" value="" />
		
		</div>
	</div>
	<div id="city" class="row item item-border-t">
		<div class="col-xs-5 i-key big-f">银行所在地</div>
		<div class="col-xs-7 i-val item-val-f">
		<span id="cityId" style="color:#959595">
                   请选择开户城市
			</span>
			<span class=""><img src="/static/imgs/forward_mark.png" ></span>
		</div>
	</div>
	<div id="bank" class="row item wrap-border-b item-border-t">
		<div class="col-xs-3 i-key big-f">开户行</div>
		<div class="col-xs-8">
		<div class="bank-if item-val-f" style="color:#959595">
        <div id="bankId"></div><div id="branchId"></div>
        <div class="select-bank" >请选择银行</div>
		</div>
		</div>
		<div class="col-xs-1 i-val" style="padding-left:0">
		<span><img src="/static/imgs/forward_mark.png" ></span>
		</div>
	</div>
	<div class="row" style="height:25px">
		<div class="col-xs-12"><div style="color: red;height: 25px; line-height: 20px; font-size: 10pt; width: 100%; text-align: center" id="error-msg"></div></div>
	</div>
	<style>
.btn:hover, .btn:focus, .btn.focus {
    color: #FFFFFF;
    text-decoration: none;
}
	</style>

	<div class="row">
		<div class="col-xs-12" style="color:red;padding-bottom:10px">
			提交后将进入系统校验，校验期间将无法修改结算信息，请保证提交的信息正确。
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button id="finish" type="button" class="btn green huge-f bu">确认提交</button>
		</div>
	</div>

			<div class="modal fade" id="confirmModal" style="display: none;">
		    <div class="modal-dialog modal-sm">
		        <div class="modal-content">
		            <div class="modal-body">
	                	提交失败
		            </div>
		             <div class="modal-footer">
	                    <a href="javascript:void(0);" class="btn btn-sm ok">确认</a>
	                </div>
		        </div>
		    </div>
		</div>



    <#include "../common-params.ftl" />

</div>
<script type="text/javascript" src="/static/js/page/account/common.js"></script>
<script type="text/javascript" src="/static/js/page/account/bank_info.js?t=03021"></script>

</body>

</html>

