
<!DOCTYPE html>
<html lang="zh-CN">

<head>

<#include "../common-header.ftl" />
    <title>收入明细</title>
</head>

<body style="background-color: #F5F5F5">

<script type="text/javascript" src="/static/js/page/account/common.js"></script>
<link href="/static/css/page/account/common.css" rel="stylesheet" type="text/css"/>
<link href="/static/css/page/account/income.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/static/js/lib/iscroll-probe.js"></script>
<script type="text/javascript" src="/static/js/page/account/income_list.js"></script>

<div class="container-fluid">
	<div class="row wrap-border-b" style="height:15px"></div>
<#if incomes?exists && incomes?size != 0>
    <div id="iscroll-wrapper">
        <div id="scroller">
        	<div id="page">
			<#list incomes as income>
				<!--<div class="row entry item-border-b">
					<div class="col-xs-5 left msg big-f">${income.showMsg !''}</div>
					<div class="col-xs-7 right">
						<div class="row time smal-f"><div class="col-xs-12">${(income.time*1000)?number_to_date} ${(income.time*1000)?number_to_time}</div></div>
						<div class="row money huge-f"><div class="col-xs-12">+${income.money?string("0.00")}</div></div>
					</div>
				</div>-->
				<div class="row entry">
					<div class="col-xs-5 left top_r small-f">运单号:${income.waybillId !''}</div>
					<div class="col-xs-7 right top_r small-f">
						${(income.time*1000)?number_to_date} ${(income.time*1000)?number_to_time}
					</div>
				</div>
				<div class="row entry-b item-border-b">
					<div class="col-xs-6 left msg big-f" >${income.showMsg !''}</div>
					<div class="col-xs-6 right money big-f">
						<#if (income.money?? && income.money >= 0 )>+</#if>${income.money?string("0.00")}
					</div>
				</div>
			</#list>
			</div>
            <div id="load-indicator"></div>
        </div>
    </div>
<#else>
    <div style="width: 100%; margin-top: 60%; text-align: center; font-size: 1.2em; color: GrayText">
        暂无收入明细
    </div>
</#if>





<#include "../common-params.ftl" />

</div>

</body>

</html>

