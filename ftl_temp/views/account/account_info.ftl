
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;" >

<head>

    <#include "../common-header.ftl" />
    <title>账户余额</title>
</head>

<body style="height: 100%;background-color: #F5F5F5">
	<link href="/static/css/page/account/account_info.css" rel="stylesheet">
	<link href="/static/css/page/account/common.css" rel="stylesheet">
    <script type="text/javascript" src="/static/js/page/account/common.js"></script>
    <script type="text/javascript" src="/static/js/page/account/account_info.js"></script>

<div class="container-fluid" style="height:100%">
	<div class="row" style="height:15px"></div>
	
	<div class="row item wrap-border-t">
		<div class="col-xs-6 i-key big-f">余额</div>
		<div class="col-xs-6 i-val mid-f">${balance?string("0.00")}元</div>
	</div>
	<div id="withDraw" class="row item item-border-t">
		<div class="col-xs-6 i-key big-f">申请提现</div>
		<div class="col-xs-6 i-val">
			<span class=""><img src="/static/imgs/forward_mark.png" ></span>
		</div>
	</div>
	<div id="payment" class="row item item-border-t">
		<div class="col-xs-6 i-key big-f">提现记录</div>
		<div class="col-xs-6 i-val"><span class=""><img src="/static/imgs/forward_mark.png" ></span></div>
	</div>	
	<div id="income" class="row item item-border-t wrap-border-b">
		<div class="col-xs-6 i-key big-f">收入明细</div>
		<div class="col-xs-6 i-val"><span class=""><img src="/static/imgs/forward_mark.png" ></span></div>
	</div>

    <#include "../common-params.ftl" />

</div>

</body>

</html>

