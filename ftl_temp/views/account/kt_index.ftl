
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;">

<head>
	<#include "../common-header.ftl" />
	<title>申请开通</title>
	<link href="/static/css/page/account/common.css" rel="stylesheet" type="text/css"/>
	<link href="/static/css/page/account/zb_open_ctl.css" rel="stylesheet" type="text/css"/>
</head>
	
<body style="background-color: #F5F5F5">

<div class="container-fluid" style="height:100%;">
	<div class="row item item-border-b bg-white">
		<div class="col-xs-4 i-key big-f">城市</div>
		<div id="selectCity" class="col-xs-7 i-val big-f selectCity">
			<span id = "cityName"></span>
		</div>
		<div class="col-xs-1 i-val img selectCity">
			<span>
				<img src="/static/imgs/forward_mark.png" >
			</span>
		</div>
	</div>	
	<div class="row item bg-white">
		<div class="col-xs-4 i-key big-f">区域</div>
		<div id="selectArea" class="col-xs-7 i-val big-f selectArea">
			<span id = "areaName"></span>
		</div>
		<div class="col-xs-1 i-val img selectArea">
			<span><img src="/static/imgs/forward_mark.png" ></span>
		</div>
	</div>

	<div class="row" style="height:25px">
		<div class="col-xs-12">
			<div style="color: red; height: 25px; line-height: 20px; font-size: 10pt; width: 100%; text-align: center" id="error-msg"></div>
		</div>
	</div>
	

	<div class="row">
		<div class="col-xs-12">
			<button id="confirm" disabled type="button" class="confirm btn grey huge-f bu">确认申请</button>
		</div>
	</div>




	<#include "../common-params.ftl" />

	<script type="text/javascript" src="/static/js/page/account/common.js"></script>
	<script type="text/javascript" >
	$(document).ready(function() {

		var cityName;
		var areaName;
		var form = $("#common-form");
	
		/** 检查输入是否完整 */
		var check = function(){
			if(!!cityName && !!areaName){
				return true;
			}
			return false;
		}

		/** 初始化城市和区域 */
		var init = function(){
			cityName = $.trim(form.find("input[name=cityName]").val());
			areaName = $.trim(form.find("input[name=areaName]").val());
			if(!cityName){
				$("#cityName").text('点击选择城市');
			}else{
				$("#cityName").text(cityName);
			}
			if(!areaName){
				$("#areaName").text('点击选择区域');
			}else{
				$("#areaName").text(areaName);
			}
			var confirmBu = $(".confirm");
			if(check()){
				confirmBu.removeAttr("disabled");
				confirmBu.removeClass("grey");
				confirmBu.addClass("green");
			}
		}();

		/** 点击选择城市 */
		$(".selectCity").click(function() {
			form.attr('action','/account/voteZBSelectCityBegin');
		    form.submit();
		});
		
		/** 点击选择区域 */
		$(".selectArea").click(function() {
			form.attr('action','/account/voteZBSelectAreaBegin');
			form.submit();
		});

		/** 确认申请 */
		$("#confirm").click(function(){
			if(!!cityName && !!areaName){
				form.attr('action','/account/voteZBSelectEnd');
				form.submit();
			}else{
				confirmBu.attr("disabled","disabled");
				confirmBu.removeClass("green");
				confirmBu.addClass("grey");
			}
		});
	});
	</script>
</body>

</html>

