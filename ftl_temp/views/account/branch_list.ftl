<!DOCTYPE html>
<html lang="zh-CN">

<head>

<#include "../common-header.ftl" />
    <title>选择支行</title>
</head>

<body style="background-color: #F5F5F5">

<link href="/static/css/page/account/branch_list.css" rel="stylesheet" type="text/css"/>
<link href="/static/css/page/account/common.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/static/js/page/account/common.js"></script>
<script type="text/javascript" src="/static/js/page/account/branch_list.js"></script>

<div class="container-fluid" style="height:100%;padding:0 16px;background-color:F5F5F5">
	<div class="row wrap-border-b" style="height:48px">
		<div class="col-xs-12 search-contain">
			<div id="search-input-contain" class="search search-ip big-f">
				<input type="text" style="width:100%;height: 100%; border: none;" placeholder="搜索" id="item-search" />
			</div>
			<div id="cancel-i" class="cancel cancel-i">&nbsp</div>
			<span id="cancel-bu" class="cancel cancel-bu">取消</span>
		</div>
	</div>
	
	
	<div class="row" style="background-color:#FFFFFF">
		<#if branches?exists && branches?size != 0>
			<#list branches as branch>
				<div class="item-r bank-branch item-border-b mid-f">			
					<div class="col-xs-12 account-list-entry-value" id="${branch.id !''}">
						<span class="br-name">${branch.name !''}</span>
					</div>
				</div>
			</#list>
		<#else>
			<div class="item-r item-border-b">					
				<div class="col-xs-12 text-center">
					没找到对应的支行？请联系客服。
				</div>
			</div>
		</#if>
			<div class="item-r item-border-b">					
				<div class="col-xs-12 text-center">
					 客服电话：010-56114919
				</div>
			</div>
	</div>

<#include "../common-params.ftl" />

</div>

</body>

</html>

