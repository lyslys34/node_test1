
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;" >

<head>

    <#include "../common-header.ftl" />
    <title>申请提现</title>
</head>

<body style="height: 100%;background-color: #F5F5F5">
	<link href="/static/css/page/account/common.css" rel="stylesheet">
	<link href="/static/css/page/account/withDraw.css" rel="stylesheet">

<div class="container-fluid" style="height:100%">
	<div class="row" style="height:15px"></div>

	<div id="bank" class="row item wrap-border-t">
		<div class="col-xs-4 i-key big-f">银行卡</div>
		<div class="col-xs-8 i-val mid-f" style="height:52px">
			<span id="bankTip">
			</span>
			<span class=""><img src="/static/imgs/forward_mark.png" ></span>
		</div>
	</div>
	<div class="row item item-border-t wrap-border-b">
		<div class="col-xs-4 i-key big-f">提现余额</div>
		<div class="col-xs-8 i-val mid-f" >
		<input id="amount" data="${balance?string("0.00")}" placeholder="本次最多可提现${balance?string("0.00")}元" style="line-height:32px;height:32px;text-align:right;width: 100%; color:#BFBFBF; border:none; outline:medium" value="" /></div>
	</div>

	<div class="row" style="height:25px">
		<div class="col-xs-12"><div style="color: red; height: 25px; line-height: 20px; font-size: 10pt; width: 100%; text-align: center" id="error-msg"></div></div>
	</div>


	<div class="row">
		<div class="col-xs-12">
			<button id="finish" type="button" class="btn green huge-f bu">确认提现</button>
		</div>
	</div>

	<div class="row info">
		<div class="col-xs-12">
			<p class="info-h mid-f">提现说明：</p>
	        <p class="smal-f">1. 最低提现金额为100元。</p>
	        <p class="smal-f">2. 打款成功至实际到账，视银行不同，会有1-3个工作日延迟，请耐心等待。</p>
	    </div>
	</div>


    <#include "../common-params.ftl" />

</div>

	<script type="text/javascript" src="/static/js/page/account/common.js"></script>
	<script type="text/javascript" src="/static/js/page/account/withDraw.js"></script>

</body>
<#include "../common-footer.ftl" />

</html>
