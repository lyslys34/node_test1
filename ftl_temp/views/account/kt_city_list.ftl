
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;">

<head>
	<#include "../common-header.ftl" />
    <title>城市</title>
	<link href="/static/css/page/account/common.css" rel="stylesheet" type="text/css"/>
	<link href="/static/css/page/account/zb_open_area.css" rel="stylesheet" type="text/css"/>
	<style>
		
	</style>

</head>

<body style="background-color: #F5F5F5">
	<div class="container-fluid" style="height:100%;">
		<div class="row wrap-border-b" style="height:48px">
			<div class="col-xs-9 search-contain" style="padding-right:4px">
				<div id="search-input-contain" class="search search-ip big-f">
					<input id="search_input" type="text" class="search-in" placeholder="请输入城市名称" id="item-search" />
				</div>
			</div>
			<div class="col-xs-3 search-contain search-con">
				<button id="confirm" disabled class="btn search-bu grey" style="font-size:12pt">确定</button>
			</div>
		</div>
		
		<div id="itemList">
		</div>

	</div>
	

	<#include "../common-params.ftl" />

	<script type="text/javascript" src="/static/js/page/account/zb_area_data.js"></script>
	<script type="text/javascript" src="/static/js/page/account/common.js"></script>
	<script type="text/javascript">
		field_name = "cityName";
		$(document).ready(function(){
			var init = function(){
				var container = $("#itemList");
				for(var i in citys){
					container.append('<div class="row item item-border-b"><div class="col-xs-12 mid-f itemDetail">'+citys[i]+'</div></div>');
				}
				item_total = $("#itemList .item");
			}();
			
		})

		valueChangeCallbackFunc = function(oldVal,newVal){
			console.log(oldVal+":"+newVal);
			//若不同删除区域选择
			if(!!oldVal && !!newVal && oldVal.indexOf(newVal)<0){
				$("#common-form").find("input[name=areaName]").remove();
			}
		}

	</script>
	<script type="text/javascript" src="/static/js/page/account/zb_open_area_search.js"></script>
</body>

</html>

