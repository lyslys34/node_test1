
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;" >

<head>

    <#include "../common-header.ftl" />
    <title>申请提现</title>
</head>

<body style="height: 100%;background-color: #F5F5F5">
	<link href="/static/css/page/account/withDraw.css" rel="stylesheet">
	<link href="/static/css/page/account/common.css" rel="stylesheet">
    <script type="text/javascript" src="/static/js/page/account/common.js"></script>
    <script type="text/javascript" src="/static/js/page/account/withDrawSuc.js"></script>

<div class="container-fluid" style="height:100%">
	<div class="row" style="height:5%"></div>

	<div class="row">
		<div class="col-xs-12" style="font-size: 12pt">提现申请已提交，等待银行处理</div>
	</div>
	<div class="row" style="margin-top: 8px">
		<div class="col-xs-12" style="margin-top: 10px;font-size: 10pt;color: #BCBCBC;">预计3个工作日到账，可去“提现记录”中查看进度</div>
	</div>
	<div class="row" style="margin-top: 23px">
	<div class="col-xs-12">
		<button id="finish" type="button" class="btn green bu huge-f" style="">确认</button>
	</div>
	</div>
</div>

</body>
<#include "../common-footer.ftl" />
</html>
