    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/static/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="/static/css/basic/ui-dialog.css" rel="stylesheet" >
    <script type="text/javascript" src="/static/js/lib/jquery.min.js"></script>
    <script type="text/javascript" src="/static/js/lib/bootstrap.min.js"></script>
    <script src="/static/js/module/dialog-min.js"></script>
