
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;font-size:100px; font-size:31.25vw;" >

<head>

    <#include "../common-header.ftl" />
    <title>培训课堂</title>

</head>

<body style="height: 100%;background-color: #F5F5F5;margin:0;">
	<link href="/static/css/page/train/train_list.css" rel="stylesheet">
	<link href="/static/css/page/account/common.css" rel="stylesheet">
	<script type="text/javascript" src="/static/js/page/train/imgClick.js"></script>
    <script type="text/javascript" src="/static/js/page/account/common.js"></script>

<div class="main-list">
<#if courseList??>
<#list courseList as course>
    <div class="item img_container" statusCode="${course.statusCode!""}" courseId="${course.courseId!""}" >
    	<hr>
        <div class="left">
           <div class="title">${course.title!""}</div>
           <div class="desc">${course.content!""}</div>
        </div>
        <div class="right" >
            <div class="status"><font color=${course.color!""}>${course.status!""}</font></div>
            <div class="arrow" statusCode="${course.statusCode!""}" ><img src="/static/imgs/next.png" ></div>
        </div>
    </div>

</#list>
</#if>
</div>


</body>
<#include "../common-params.ftl" />
</html>
