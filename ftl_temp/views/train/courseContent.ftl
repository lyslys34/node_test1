<!DOCTYPE html>
<html lang="zh-CN">
<head>
<#include "../common-header.ftl" />
<meta charset="utf-8">
<title>入门学习</title>

<link href="/static/css/lib/swiper.min.css" rel="stylesheet">
<link href="/static/css/page/train/train.css" rel="stylesheet">

<script type="text/javascript" src="/static/js/lib/swiper.min.js"></script>
<script type="text/javascript" src="/static/js/page/account/common.js"></script>
<script type="text/javascript" src="/static/js/page/train/examClick.js"></script>
 
</head>
<body>


<div class="container-fluid" style="height:100%">
<div class="row header ">
<div class="float-contain col-xs-12 big-f">
<div><img style="vertical-align:inherit" src="/static/imgs/icons_44.png"></div>
<div style="color:#06C1AE;padding-left:7px">入门学习</div>
<div style="padding:0 15px"><img style="vertical-align:inherit ;width:6px;height:12px" class="forward" src="/static/imgs/forward_mark.png"></div>
<div><img style="vertical-align:inherit" src="/static/imgs/icons_64.png"></div>
<div style="color:#959595;padding-left:7px">在线考试</div>
</div>
</div>

    <!-- Swiper -->
    <div class="swiper-container">
        <div class="swiper-wrapper">	
          <#if contentList ??>
            <#list contentList as content>
            <div class="swiper-slide"> 
            <img  src="${content.url!""}"></img>
              </div>
            </#list>
        </#if>            
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>


    <!-- Initialize Swiper -->
    <script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        onReachEnd :function(){
          $("#exam").show();
        }
    });
    </script>
    
    <div class="row" style="margin-top:20px">
<div class="col-xs-12">
<button id="exam" type="button" class="btn green huge-f bu" <#if (contentList?size ==1)>style="display:block"</#if> courseId=${contentList[0].courseId} >开始考试</button>
</div>
</div>
    </div>
    <#include "../common-params.ftl" />
</body>
</html>