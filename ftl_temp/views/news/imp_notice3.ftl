<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
	<style type="text/css">
		.title{
			font-size: 16pt;
			text-align: center;
		}
		.fz_normal{
			font-size: 14pt;
		}
		.fz_small{
			font-size: 12pt;
		}

		.content1{
			padding-left:20px;
		}
		body{
			padding:5px 10px;
			line-height:1.2;
		}
	</style>
	<body>
		<p class="title"><b>重要通知</b></p>
		<p class="fz_normal">亲爱的配送员：</p>
		<p class="fz_normal">&nbsp;&nbsp;&nbsp;&nbsp;美团众包提醒您，在配送过程中，请遵守美团众包管理规定，规范配送操作：</p>
		<p class="fz_small">1、抢单后不取餐，且没有与商家沟通取消订单的配送员， 被商家投诉经证实后，该配送员会被拉黑7天。</p>
		<p class="fz_small">2、上周拉黑名单：</p>
		<p class="content1 fz_small">1) 商家投诉，抢单后不取货，要求商家取消的配送员：王发挥、司猛、李海涛、李茂兴；</p>
		<p class="content1 fz_small">2) 多次配送不符合美团众包规定的配送员：张志博、侯继霞、刘干；</p>
		<p class="fz_normal">请大家按规定准时，规范配送！祝大家工作愉快！</p>
	</body>
	<#include "../common-footer.ftl" />
</html>
