<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<style type="text/css">
    .title{
        font-size: 16pt;
        text-align: center;
    }
    .fz_big{
        font-size: 16pt;
    }
    .fz_normal{
        font-size: 14pt;
    }
    .fz_small{
        font-size: 12pt;
    }

    .content1{
        padding-left:20px;
    }
    body{
        padding:5px 10px;
        line-height:1.2;
    }
</style>
<body>
<p class="fz_normal">亲爱的美团骑手：</p>
<p class="fz_normal">骑手客户端v2.1版本已正式发布，此次主要更新内容如下：</p>
<p class="fz_normal">1、品牌Logo全新升级</p>
<p class="fz_small"><img border="0" style="width:80px;height:80px"src="/static/imgs/zj-icon.png" />升级为<img border="0" style="width:80px;height:80px"src="/static/imgs/meituanqishou80x80.png" /></p>
<p class="fz_normal">2、完善新订单提醒功能</p>
<p class="fz_small">保证新订单提醒的有效性，避免消息延迟导致的有声音没订单等影响。</p>
<p class="fz_normal">3、增加【我的定位】展示页面</p>
<p class="fz_small">（1）可查看最近10条定位记录，GPS定位时无中文地址信息，若定位精确度小于200米，将直接显示当前定位较精确。</p>
<p class="fz_small">（2）若最近一次定位距当前时间大于5分钟，可点击手动定位恢复（注意：无需频繁手动定位，以免消耗较多流量）。</p>
<p class="fz_normal">4、新增【系统通知】功能</p>
<p class="fz_small">之后有重要通知将直接发给各位骑手，有新通知时，首页菜单项-【系统通知】右侧将显示小红点。</p>
<p class="fz_normal">5、增加【关于美团骑手】页面</p>
<p class="fz_small">（1）【常见问题说明】骑手可以直接获取常见问题解决办法，之后将持续更新该页面；</p>
<p class="fz_small">（2）【意见反馈】功能移入该页面下；</p>
<p class="fz_small">（3）【上传操作日志】当骑手发现客户端异常或出现问题时，可点击上传操作日志，以便技术人员快速定位并修复问题。</p>
<p class="fz_normal">6、更多新功能和优化等你发现......</p>
<p class="fz_normal">若您有好的意见或建议可直接在意见反馈中反馈，或加QQ号：3299402907（添加时请备注：骑手姓名、所属站点）。</p>
</body>
<#include "../common-footer.ftl" />
</html>
