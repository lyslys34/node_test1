<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<link href="/static/css/page/news/zj_2_2_news_list.css" rel="stylesheet" type="text/css"/>

<div class="article-wrap">
    <div class="title-wrap">【通知】美团骑手v2.2版本正式发布</div>
    <div class="meta-list">
        <div class="time-wrap">2015-10-28</div>
        <div class="author-wrap">美团骑手</div>
    </div>
    <div class="main-wrap">
        <div class="summary-wrap">美团骑手v2.2版本正式上线通知</div>
        <div class="content-wrap"><p class="p1" style="white-space: normal;"><span class="s1">亲爱的美团骑手：</span></p><p class="p1" style="white-space: normal;"><span class="s1">骑手客户端v2.2版本正式发布，此次主要更新内容如下，</span></p><p class="p1" style="white-space: normal;"><span class="s1"><br/></span></p><p class="p2" style="white-space: normal;"><span style="font-size: 24px;">1、新任务列表排序优化</span></p><p class="p1" style="white-space: normal;"><span class="s1">1）新任务智能排序：优先将调度推荐的同店顺路订单、附近商家订单靠前展示；</span></p><p class="p1" style="white-space: normal;"><span class="s1">2）默认排序：最新订单将排在前面；</span></p><p class="p1" style="white-space: normal;"><span class="s1">以上排序方式骑手可按需求自行设置，方便骑手快速接单。</span></p><p class="p3" style="white-space: normal;"><span class="s1"></span><br/></p><p class="p2" style="white-space: normal;"><span style="font-size: 24px;">2、支持查看7天内历史订单</span></p><p class="p4" style="white-space: normal;"><span class="s1">可查看一周内的历史订单，包括一周内已送达、已取消的订单。</span></p><p class="p5" style="white-space: normal;"><span class="s1"></span><br/></p><p class="p6" style="white-space: normal;"><span style="font-size: 24px;">3、标记跨天预约订单</span></p><p class="p4" style="white-space: normal;"><span class="s1">跨天预约单指顾客几天前下的预约单。</span></p><p class="p5" style="white-space: normal;"><span class="s1"></span><br/></p><p class="p6" style="white-space: normal;"><span style="font-size: 24px;">4、实付实收修改入口统一</span></p><p class="p4" style="white-space: normal;"><span class="s1">实付实收修改入口统一，去掉了单独修改实收的入口。</span></p><p class="p5" style="white-space: normal;"><span class="s1"></span><br/></p><p class="p6" style="white-space: normal;"><span style="font-size: 24px;">5、显示垫付款打款失败的原因</span></p><p class="p4" style="white-space: normal;"><span class="s1">若垫付款打款失败，将显示打款失败的原因。</span></p><p class="p5" style="white-space: normal;"><span class="s1"></span></p><p class="p5" style="white-space: normal;"><br/></p><p class="p5" style="white-space: normal;"><br/></p><p class="p7" style="white-space: normal;"><em><span class="s1">若您有好的意见或建议可直接在意见反馈中反馈！或加QQ号：3299402907（添加时请务必备注：骑手姓名、所属站点，备注格式如：骑手-张三-北京西站）。</span></em></p></div>
    </div>
</div>

<#include "../common-params.ftl">

</body>

</html>