<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>消息中心</title>
</head>

<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        <#if message??>
            <h3>${message.msgTitle!''}</h3>
            <div id="umeditor-content"></div>
        <#else>
            暂无数据
        </#if>
        </div>
    </div>

</div>

<script>
    var umEditorHTML = "";
    <#if message?exists >
    umEditorHTML = "${message.msgBody!''}";
    </#if>
    if (umEditorHTML.length) {
        document.getElementById("umeditor-content").innerHTML = decodeURIComponent(umEditorHTML);
    }
</script>

<#include "../common-params.ftl">

</body>

</html>