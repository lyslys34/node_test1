<!DOCTYPE html>
<html lang="zh-CN">
<head>
<#include "../common-header.ftl" />
    <title>系统通知</title>
</head>

<body>

<div class="container-fluid">
    <div class="row border-b" style="height:1px"></div>
<#if error??><p align="center" style="font-size:16px">${error}</p>
<#elseif (newsList??)&&(newsList?size>0)>
    <div id="page">
        <#list newsList as news>
            <div class="row appnews-item <#if news.read == 1>msg-read</#if>" msgId=${news.msgId!''} >
                <div class="col-xs-10 overHide appnews-content" >
                    <span class="i-key">${news.msgTitle!''}<br /></span>
                    <span class="i-val sub-tit">&nbsp;&nbsp;${(news.createTime*1000)?number_to_date} ${(news.createTime*1000)?number_to_time}</span>
                </div>
                <div class="col-xs-2 overHide text-center margin-top-17"><img border="0" id="forwardarror"  style="height:22px" src="/static/imgs/forward_mark.png" /></div>
            </div>
        </#list>
    </div>
<#else>
    <p align="center" style="font-size:16px">暂无系统通知！</p>
</#if>
</div>

<#include "../common-params.ftl">

<link href="/static/css/page/news/news_list_homebrew.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/static/js/page/news/common.js"></script>
<script type="text/javascript" src="/static/js/page/news/news_list.js"></script>

</body>
</html>
