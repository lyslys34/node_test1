<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<#include "../common-header.ftl" />
    <title>消息中心</title>
</head>

<body>


<div class="container-fluid">
	<div class="row border-b" style="height:15px"></div>
	<#if error??><p align="center" style="font-size:16px">${error}</p>
	<#elseif (newsList??)&&(newsList?size>0)>	
		<#list newsList as news>
			<div class="row appnews-item <#if news.read == 1>msg-read</#if>" msgId=${news.msgId!''}>
				<div class="col-xs-7 i-key overHide">
					<#--<a style="color:#313131" href="${news.content!''}">-->
						<#--[${news.type!''}]&nbsp;-->
					${news.msgTitle!''}
					<#--</a>-->
				</div>
				<div class="col-xs-5 i-val overHide text-right">${(news.createTime*1000)?number_to_date}</div>
			</div>
		</#list>
	<#else><p align="center" style="font-size:16px">目前没有消息！</p>
	</#if>
</div>

	<#include "../common-params.ftl">

    <link href="/static/css/page/news/news_list_crowdsource.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/static/js/page/news/common.js"></script>
    <script type="text/javascript" src="/static/js/page/news/news_list.js"></script>

</body>

</html>