<!DOCTYPE html>
<html lang="zh-CN">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            美团众包协议变更通知
        </title>
        <style type="text/css">
            .title{ font-size:14pt; text-align:center; margin-bottom:6px } .para{
            font-size:12pt; padding:2px 2px; line-height:25px } p{ margin:0 0; } .sub-para{
            padding: 0 15px; }
            .red{color:red} .mt-8{margin-top:10px} .mt-5{margin-top:7px}
        </style>
    </head>

    <body>
        <div class="para mt-8">
            <p>
                尊敬的用户：
            </p>
            <p class="mt-8">
                因平台运营调整，美团众包对协议进行了修订，并于9月21日正式生效，请您及时查看。在协议生效日期后继续使用美团众包服务的，即表明您已仔细阅读并同意该协议。具体协议内容详见附件1《美团众包平台服务协议》和附件2《劳务协议》。
            </p>
            <p class="mt-8">
                附件1：美团众包平台服务协议
            </p>
            <p>
                欢迎您与众包平台共同签署本《众包平台服务协议》（以下简称“本协议”）并使用美团众包平台服务。
            </p>
            <p class="mt-8">
                【审慎阅读】为维护您的自身权益，您在申请注册流程中点击同意本协议之前，应当认真阅读本协议。请您务必认真阅读、充分理解各条款内容，特别是责任范围与责任限制的条款、法律适用和争议解决的条款。上述条款将以粗体标识，您应重点阅读。
            </p>
            <p class="mt-8">
                【签约动作】当您按照注册页面提示填写信息、阅读并同意本协议且完成全部注册程序后，即表示您已充分阅读、理解并接受本协议的全部内容。阅读本协议的过程中，如果您不同意本协议或其中任何条款的约定，您应立即停止注册程序。
            </p>
            <p class="mt-8">
               一、定义
            </p>
            <p>
                1.1 美团众包平台：以下简称“众包平台”，是指为商户提供劳务需求信息展示，并通过该平台展示劳务需求信息、单笔劳务费用、及服务完成确认等信息的手机APP信息平台。众包员可通过众包平台自主选择接收任务事项，并在事项完成后获得众包平台显示的劳务费及相应奖励（如有）。
            </p>
            <p>
               1.2 商户：是指通过众包平台完成全部注册程序后，通过众包平台发布任务事项，并支付报酬的经营者。
            </p>
            <p>
                1.3 众包员：即本协议中的“您”，是指接受并同意本协议、劳务协议全部条款及众包平台发布的其他全部服务条款和操作规则（以下简称“各类规则”），申请注册并经众包平台审核通过后，通过众包平台自主选择、完成任务事项，并在事项完成后获得相应报酬的完全民事行为能力人。
            </p>
            <p>
                1.4 劳务需求信息：有劳务需求的商户通过美团众包平台发布的，需提供劳务的外卖产品信息、配送地址、劳务报酬等信息。
            </p>
            <p class="mt-8">
               二 、服务条款的确认和接受
            </p>
            <p>
                2.1 在您使用众包平台的服务之前，请您务必认真阅读全部条款内容。如您对条款有任何疑问的，应及时停止注册或使用并向众包平台咨询。一旦您完全同意所有服务条款并完成注册程序，或在其后使用众包平台服务的，即表示您已充分阅读、理解并接受本协议的全部内容，本协议即对您产生约束力。届时您不应以未阅读本协议的内容为由，主张本协议无效，或要求撤销本协议。
            </p>
            <p>
               2.2 众包平台有权根据国家法律法规的规定更新、维护交易秩序，调整服务规则，并不时地修改本协议或各类规则，修改后的协议或各类规则（以下简称“变更事项”）将以网站公示的方式通知您。如您不同意变更事项，您有权于变更事项确定的生效之日起停止使用众包平台服务，则变更事项对您不产生效力；如您在变更事项生效后仍继续使用众包平台服务的，即视为您同意已生效的变更事项，变更事项对您产生效力。
            </p>
            <p>
               2.3 众包平台保留在中华人民共和国法律允许的范围内独自决定拒绝提供服务、关闭您的账户或取消任务事项的权利。
            </p>
            <p class="mt-8">
               三 、服务简介
            </p>
            <p>
               3.1 众包平台作为服务平台，是为商户、劳务公司和众包员之间提供互通信息的平台。主要体现在商户可以服务需求方身份在众包平台上发出劳务需求信息，平台予以展示，由您自主选择是否接收前述服务事项并代劳务公司完成委托事项。
            </p>
            <p>
               3.2 美团众包平台目前为您提供的服务是免费的，但是众包平台保留未来对相关服务收取费用的权利。
            </p>
            <p>
               3.3 众包员自行准备如下设备并承担如下费用：
            </p>
            <p>
              3.3.1上网设备，包括并不限于手机、电脑或者其他上网终端、调制解调器及其他必备的上网装置；
            </p>
            <p>
              3.3.2上网开支，包括并不限于网络接入费、上网设备租用费、手机流量费等。
            </p>
            <p class="mt-8">
              四、注册
            </p>
            <p>
              4.1 注册资格：您需年满十八周岁、身体健康持有健康证，并具有完全民事行为能力和相应的劳动能力。如果您不具备前述条件，众包平台有权注销您已注册完成的账户。对您之前的行为，您及您的监护人应依照法律规定承担相应后果。
            </p>
            <p>
              4.2 您应按照众包平台要求提供注册资料（包括但不限于身份证、健康证，并上传提供委托服务的地理位置），并保证前述资料真实、准确、完整、合法、有效。如您的注册资料有所变动，您应及时更新注册资料。 否则，您应承担相应责任及后果。
            </p>
            <p>
              4.3 您注册成功后，应谨慎合理的保存、使用您的用户名和密码，并对通过您的账户实施的所有行为、活动及事件负全责。您若发现账号存在安全漏洞等情况，应立即以有效方式通知众包平台，要求众包平台暂停相关服务，必要时向公安机关报案。您理解众包平台对您的请求采取行动需要合理时间，并需要您提供有效资料证明您为账户的所有者。众包平台对在采取行动前已经产生的后果（包括但不限于您的任何损失）不承担任何责任。
            </p>
            <p>
              4.4 您不得将在众包平台注册获得的账户借（租）给他人使用，否则对此产生的一切损失您应与实际使用人承担连带责任。
            </p>
            <p>
              4.5 您同意，众包平台拥有通过邮件、短信等形式，向您发送告知信息的权利。
            </p>
            <p>
              4.6 您同意，众包平台有权使用您的注册信息、用户名、密码等信息，登陆进入您的注册账户，进行证据保全，具体方式包括但不限于公证、见证等。
            </p>
            <p>
              4.7 您知悉并认可：众包平台可能会与第三方合作而向您提供相关的网络服务，在此情况下，如该第三方同意承担与众包平台同等的保护您隐私的责任，则众包平台有权将您的注册资料等提供给该第三方。另外，在不透露您隐私资料的前提下，众包平台有权对与您账户相关的整个数据库进行分析并对相关的数据库进行商业上的利用。
            </p>
            <p>
            	4.8您了解并同意，众包平台有权应政府部门（包括司法及行政部门）的要求，向其提供您在众包平台填写的注册信息和发布纪录等必要信息。
            </p>
            <p class="mt-8">
            	五、责任范围与责任限制
            </p>
            <p>
            5.1您个人明确同意对网络服务的使用承担风险。众包平台对此不作任何类型的明示或默示担保。包括但不限于：不担保服务一定能满足您的要求，也不担保服务不会被中断，对服务的及时性、安全性、不出错都不作担保；对在众包平台上的任何服务或交易进程，不作担保；对众包平台服务所涉及的技术及信息的有效性、准确性、正确性、可靠性、稳定性、完整性和及时性不作出任何承诺和保证；对众包平台服务的适用性、没有错误或疏漏、持续性、准确性、可靠性或适用于某一特定用途不作担保。
            </p>
            <p>
            5.2 您理解并接受：众包平台作为信息发布服务平台，无法控制每一任务事项所涉及的物品的质量、安全或合法性，任务事项内容的真实性或准确性，以及任务事项所涉各方履行任务事项的能力。
            </p>
            <p>
            5.3 您了解并同意，众包平台不对因下述情况而导致的任何损害赔偿承担责任，包括但不限于利润、商誉、使用、数据等方面的损失或其它无形损失的损害赔偿：
            </p>
            <p>
            5.3.1 使用或未能使用众包平台的服务；
            </p>
            <p>
            5.3.2 您对众包平台服务的误解；
            </p>
            <p>
            5.3.3 任何非因众包平台的原因而引起的与众包平台服务有关的其它损失。
            </p>
            <p>
            5.4 如因不可抗力或其他众包平台无法控制的原因使众包平台系统崩溃或无法正常使用导致网上交易无法完成或丢失有关的信息、记录等，众包平台不承担责任。
            </p>
            <p>
            5.5 您同意在发现众包平台任何内容不符合法律规定，或不符合本协议约定的，您有义务及时通知众包平台。如果您发现个人信息被盗用或者其他权利被侵害，请将此情况反馈众包平台并按照众包平台的要求提供相应的证明材料。
            </p>
            <p>
            5.6 您应当严格遵守本协议及平台的各类规则，因您违反本协议或各类规则的行为给第三方、众包平台造成损失的，您应当承担相应责任。
            </p>
            <p class="mt-8">
            六、对您信息的管理及限制
            </p>
            <p>
            	6.1 您账户被注销后，众包平台没有义务为您保留或向您披露账户中的任何信息，也没有义务向您或第三方转发任何您未曾阅读或发送过的信息。
            </p>
            <p>
            6.2 您同意，与众包平台的协议关系终止后，众包平台仍享有下列权利：
            </p>
            <p>
            6.2.1 继续保存您未及时删除的注册信息及使用众包平台的服务期间发布的所有信息至法律规定的记录保存期满。
            </p>
            <p>
            6.2.2 您在使用众包平台的服务期间存在违法行为或违反本协议和/或各类规则的行为的，众包平台仍可依据本协议向您主张权利。
            </p>
            <p class="mt-8">
            七、网络服务内容的所有权 
            </p>
            <p>
            7.1 众包平台各项电子服务的所有权和运作权归北京三快科技有限公司所有。
            </p>
            <p>
            7.2 众包平台在与您合作中提供的平台服务内容包括文字、软件、声音、图片、录像、图表、广告等的全部内容的知识产权均归众包平台所有，您在使用众包平台过程中所产生内容的知识产权均归众包平台所有。
            </p>
            <p>
            7.3 除另有特别声明外，众包平台所依托软件的著作权、专利权及其它知识产权均归众包平台或众包平台关联公司所有。
            </p>
            <p>
            7.4 众包平台在提供平台服务过程中所使用的“美团众包”“美团众包平台”“美团众包员”等商业标识，其著作权或商标权归众包平台或众包平台关联公司所有。
            </p>
            <p>
            八、平台服务使用规范
            </p>
            <p>
            8.1 在使用众包平台服务的过程中，您承诺遵守以下约定：
            </p>
            <p>
            8.1.1 在使用众包平台服务的过程中实施的所有行为均遵守国家法律、法规、规范性文件的规定及本协议、各类规则的约定和要求，不违背社会公共利益或公共道德，不损害他人的合法权益，不违反本协议及各类规则。您如果违反前述承诺，产生任何法律后果的，您应以自己的名义独立承担所有的法律责任，并确保众包平台免于因此产生任何损失。
            </p>
            <p>
            8.1.2 不发布国家禁止发布的任务事项信息（除非取得国家相关部门合法且足够的许可），不发布涉嫌侵犯他人知识产权或其它合法权益的信息，不发布违背社会公共利益或公共道德、公序良俗的信息，不发布其它涉嫌违法或违反本协议及各类规则的信息。
            </p>
            <p>
            8.1.3 不使用任何装置、软件或例行程序干预或试图干预众包平台的正常运作或正在众包平台上进行的任何活动。您不得采取任何将导致不合理的庞大数据负载加诸众包平台网络设备的行动。
            </p>
            <p>
            8.1.4 不发表、传送、传播、储存侵害他人知识产权、商业秘密权等合法权利的内容或包含病毒、木马、定时炸弹等可能对众包平台系统造成伤害或影响其稳定性的内容。
            </p>
            <p>
            8.1.5 不进行危害计算机网络安全的行为，包括但不限于：使用未经许可的数据或进入未经许可的服务器帐号；不得未经允许进入公众计算机网络或者他人计算机系统并删除、修改、增加存储信息；不得未经许可，企图探查、扫描、测试众包平台系统或网络的弱点或其它实施破坏网络安全的行为；不得企图干涉、破坏众包平台系统或网站的正常运行。
            </p>
            <p>
            8.1.6 其它违反法律法规或者众包平台各类规则的行为。
            </p>
            <p>
            8.2 您了解并同意：
            </p>
            <p>
            8.2.1 违反上述承诺时，众包平台有权依据本协议的约定，做出相应处理或终止向您提供服务，且无须征得您的同意或提前通知予您。
            </p>
            <p>
            8.2.2 根据相关法令的指定或者各类规则的判断，您的行为涉嫌违反法律法规的规定或违反本协议和/或各类规则的条款的，众包平台有权采取相应措施，包括但不限于直接屏蔽、删除侵权信息、降低信用值或直接停止提供服务，因此给众包平台造成损失的您应承担赔偿责任。
            </p>
            <p>
            8.2.3 对于您在众包平台上实施的行为，包括未在众包平台上实施但已经对众包平台产生影响的行为，众包平台有权单方认定该行为的性质及是否构成对本协议和/或各类规则的违反，并据此采取相应的必要的处理措施。
            </p>
            <p>
            8.2.4 对于您涉嫌违反承诺的行为对任意第三方造成损害的，您均应当以自己的名义独立承担相应法律责任，并应确保众包平台免于承担因此产生的损失或增加的费用。
            </p>
            <p>
            8.2.5 如您涉嫌违反有关法律规定或者本协议之约定，使众包平台受到任何行政管理部门的处罚，或遭受到任何第三方的索赔请求，使众包平台遭受任何损失，您应当赔偿众包平台因此造成的损失及发生的费用，包括合理的律师费用。
            </p>
            <p class="mt-8">
            九、法律适用和争议解决
            </p>
            <p>
            9.1 本协议的订立、执行、解释及争议的解决均应适用中华人民共和国法律。
            </p>
            <p>
            9.2 如双方就本协议内容或其执行发生任何争议，双方应尽力友好协商解决；协商不成时，任何一方均可向被告所在地有管辖权的人民法院提起诉讼。
            </p>
            <p class="mt-8">
            附件2：劳务协议
            </p>
            <p>
            提示条款 ：
            </p>
            <p class="sub-para">
            欢迎您与上海正东人力资源有限公司（详见定义条款）共同签署本《劳务协议》（下称“本协议”）并使用美团众包平台服务。
            </p>
            <p class="sub-para">
            【审慎阅读】为维护您的自身权益，在您申请注册流程中点击同意本协议之前，请认真阅读本协议，务必审慎阅读、充分理解各条款内容，特别是权利义务条款、法律适用和管辖条款。上述条款以粗体标识，您应重点阅读。
            </p>
            <p class="sub-para">
            【签约】当您按照注册页面提示填写信息、阅读并同意本协议且完成全部注册程序后，即表示您已充分阅读、理解并接受本协议的全部内容，并与上海正东人力资源有限公司达成一致，成为上海正东人力资源有限公司的劳务人员。阅读本协议的过程中，如果您不同意本协议或其中任何条款约定，请您立即停止注册程序。
            </p>
            <p class="sub-para">
            您与上海正东人力资源有限公司通过本协议建立劳务关系，适用《合同法》、《民法通则》和其他民事法律，不适用《劳动合同法》。
            </p>
            <p class="mt-8">
            一、定义
            </p>
            <p class="sub-para">
            1.1上海正东人力资源有限公司：是指与您签署劳务合同，形成劳务关系、并向您发放劳务报酬的公司（以下简称“劳务公司”）。
            </p>
            <p class="sub-para">
            1.2美团众包平台：以下简称“众包平台”，是指为商户提供劳务需求信息展示，并通过该平台展示劳务需求信息、单笔劳务费用、及服务完成确认等信息的手机APP信息平台。众包员可通过众包平台自主选择接收任务事项，并在事项完成后获得众包平台显示的劳务费及相应奖励（如有）。
            </p>
            <p class="sub-para">
            1.3商户：与美团签署服务协议，入驻美团平台进行信息展示及交易的经营者。
            </p>
            <p class="sub-para">
            1.4劳务需求信息：有劳务需求的商户通过众包平台发布的，需提供劳务的外卖产品信息、配送地址、劳务报酬等信息。
            </p>
            <p class="mt-8">
            二、协议内容
            </p>
            <p>
            2.1您同意与劳务公司签订劳务合同，按照众包平台展示的劳务需求信息内容、要求、标准，自主选择接收服务事项，及时完成并收取劳务公司支付的劳务费及平台奖励（如有）。
            </p>
            <p>
            2.2您保证能按时并且安全的完成劳务需求相关的配送事宜，因您的原因导致的任何第三方的损失，您应该承担包括但不限于赔偿的责任。
            </p>
            <p class="mt-8">
            三、劳务费
            </p>
            <p class="sub-para">
            3.1【计价标准】劳务费及平台奖励的计算标准以众包平台显示的数据为准。
            </p>
            <p class="sub-para">
            3.2【结款周期】当您提出结算申请后，经核实您提供的劳务服务符合标准且无误后，劳务公司将在您提出结算申请的3个工作日内将服务费及平台奖励等费用支付到您向平台提交的银行账号上。
            </p>
            <p class="mt-8">
            四、注册资格
            </p>
            <p class="sub-para">
            请保证，您在注册时年满十八周岁、身体健康拥有健康证，并具有完全民事行为能力和相应劳动能力。若您不具备前述民事行为能力及身体状态，您及您的监护人应依照法律规定承担因此而导致的一切后果。
            </p>
            <p class="mt-8">
            账号说明
            </p>
            <p class="sub-para">
            5.1【账户获得】当您按照注册页面提示填写信息、阅读并同意本协议且完成全部注册程序后，您可获得众包平台账户，成为众包员。
            </p>
            <p class="sub-para">
            5.2【账号安全】您的账户为您自行设置及保管，众包平台任何时候均不会主动要求您提供您的账户。因此，建议您务必妥善使用您的账户信息，保管好您的账户密码。账户因您泄露或遭受他人攻击、诈骗等行为导致的损失及后果，均由您自行承担。
            </p>
            <p class="mt-8">
            六、劳务公司义务
            </p>
            <p class="sub-para">
            6.1 劳务公司应根据商户在众包平台展示或另行与众包平台实际经营者确认的服务标准要求制定服务作业计划和作业要求，并督促众包员按照服务作业计划和作业要求进行服务作业；
            </p>
            <p class="sub-para">
            6.2 劳务公司负责处理您执行劳务服务期间发生的所有用工问题；
            </p>
            <p class="sub-para">
            6.3劳务公司应及时向您支付劳务报酬并承担劳务服务相关的税费。
            </p>
            <p class="mt-8">
            七、您的义务
            </p>
            <p class="sub-para">
            7.1您应该确保提供配送服务环节中配送商品的安全。
            </p>
            <p class="sub-para">
            7.2您应该确保注册信息的真实性、完整性、合法性。
            </p>
            <p class="sub-para">
            7.3您应该确保提供配送服务环节中行车的安全，不侵犯任何第三方的权益。如自驾机动车辆，应保证证件齐全。
            </p>
            <p class="sub-para">
            7.4任何期间均不得使用或借用众包平台名义雇佣人员，也不得将在众包平台接受的任务事项转交他人完成。
            </p>
            <p class="sub-para">
            7.5在接受、完成任务事项过程中获取的任何有关众包平台或任何第三方的信息，包括但不限于收货人姓名、联系方式、电话、地址等均应严格保密，不得传播、泄露、披露，不得在为完成本协议任务事项之外的目的自行或允许他人使用。
            </p>
            <p class="sub-para">
            7.6您应该遵守交通规则。
            </p>
            <p class="mt-8">
            八、法律适用与管辖
            </p>
            <p class="sub-para">
            8.1 本条款的订立、执行和解释及争议的解决均应适用中国法律。
            </p>
            <p class="sub-para">
            8.2如您与劳务公司就本协议内容或其执行发生任何争议，双方应尽力友好协商解决；协商不成时，任何一方均可将争议提交至众包平台调停及裁判，双方同意自愿遵守众包平台的调停及裁判结果。如不认可众包平台的调停结果的，任何一方均可向合同签订地（众包平台所在地）人民法院提起诉讼。
            </p>
        </div>
    </body>
  	<#include "../common-footer.ftl" />

</html>
