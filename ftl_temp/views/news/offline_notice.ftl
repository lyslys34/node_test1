<!DOCTYPE html>
<html lang="zh-CN">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            消息中心
        </title>
        <style type="text/css">
            .title{ font-size:14pt; text-align:center; margin-bottom:6px } .para{
            font-size:10pt; padding:2px 2px; line-height:25px } p{ margin:0 0; } .sub-para{
            padding: 0 15px; }
            .red{color:red} .mt-8{margin-top:10px} .mt-5{margin-top:7px}
        </style>
    </head>

    <body>
        <div class="title">
            <span class="s1">
                关于［美团众包］<span class="red">望京区域</span>培训线下报名的通知
            </span>
        </div>
        <div class="para mt-8">
            <p>
                亲爱的配送员：
            </p>
            <p class="sub-para">
                ［美团众包］终于迎来和大家见面的机会-［美团众包］配送员线下培训课堂！
            </p>
            <p class="red">
                配送员参加培训后有机会抢高价订单；
            </p>
            <p class="red">
                配送员不参加培训后期会影响抢单；
            </p>
            <p class="mt-5">
                本次培训设置两个场次：
            </p>
            <p>
                1. 培训时间：9月20日下午14:30-15:30  集合地点：［望京］望京东路6号望京国际研发园东门（培训开始前30分钟签到）；
            </p>
            <p>
                2. 培训时间：9月20日下午16:00-17:00  集合地点：［望京］望京东路6号望京国际研发园东门（培训开始前30分钟签到）；
            </p>
            <p class="mt-5">
                报名／取消截止时间： 9月18日18:00
            </p>
            <p class="mt-5">
                报名方式：
            </p>
            <p>
                添加微信号：<span class="red">mtpeisong</span>
            </p>
            <p>
                发送文本：“<span class="red">姓名＋手机号＋报名场次（1或者2）</span>”进行报名，得到回复确认后方可来参加培训；
            </p>
            <p class="mt-5">
                取消报名方式：
            </p>
            <p>
                发送文本：“<span class="red">［取消］＋姓名＋手机号＋报名场次（1或者2）</span>”
            </p>
            <p>
               ［注］参加培训前必须完成手中所有配送订单；报名后缺席会被拉黑3天；
            </p>
            <p class="mt-5">
                欢迎大家积极踊跃报名！
            </p>
        </div>
    </body>
  	<#include "../common-footer.ftl" />
</html>
