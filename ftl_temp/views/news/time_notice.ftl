<!DOCTYPE html>
<html lang="zh-CN">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            消息中心
        </title>
        <style type="text/css">
            .title{ font-size:15pt; text-align:center; margin-bottom:6px } .para{
            font-size:11pt; padding:2px 2px; line-height:25px } p{ margin:0 0; } .sub-para{
            padding: 0 15px; }
        </style>
    </head>

    <body>
        <div class="title">
            <span class="s1">
                关于订单配送时间的说明
            </span>
        </div>
        <div class="para">
            <p>
                1. 即时订单抢单到取货时间不能超过15分钟；
            </p>
            <p class="sub-para">
                即时订单请在抢单后60分钟内完成配送；
            </p>
            <p class="sub-para">
                （例如：即时订单抢单时间为10:00，您需要在10:15之前到商家取货，且在11:00之前完成配送。否则无法获得奖励！）
            </p>
        </div>
        <div style="text-align:center">
            <img width="260px" src="/static/imgs/time_notice1.png" />
        </div>
        <div class="para">
            <p>
                2. 预定单超时送达或提前送达不能超过20分钟；
            </p>
            <p class="sub-para">
                （例如：预定单客户的期望送达时间为12:00，众包配送员需要在11:40—12:20之间完成配送。否则无法获得奖励！）
            </p>
        </div>
        <div>
            <p class="p1" style="text-align: center;">
                <img width="260px" src="/static/imgs/time_notice2.png" />
            </p>
        </div>
        <div class="para">
            <p>
                请大家一定按照规范操作！！！
            </p>
        </div>
    </body>
  	<#include "../common-footer.ftl" />
</html>
