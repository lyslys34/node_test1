<!DOCTYPE HTML>
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <link href="/static/css/lib/bootstrap.min.css" rel="stylesheet">

	    <script type="text/javascript" src="/static/js/lib/jquery.js"></script>
	    <#--<script type="text/javascript" src="/static/js/lib/angular.js"></script>-->
	    <script type="text/javascript" src="/static/js/lib/bootstrap.js"></script>
	    <#--<script type="text/javascript" src="/static/js/lib/require.js"></script>-->
	    <title style="font-size:20px">众包平台服务协议</title>
	    <style>
	    li{
	    	font-size:15px;
	    	list-style-type:none;
	    }
	    </style>
	</head>
	<body style="padding:10px 15px 10px 0px">
		<ul>
			<li>欢迎您与众包平台共同签署本《众包平台服务协议》（以下简称“本协议”）并使用美团众包平台服务。</li>
			<br>
			<li>【审慎阅读】为维护您的自身权益，<strong>您在申请注册流程中点击同意本协议之前，应当认真阅读本协议。请您务必认真阅读、充分理解各条款内容，
			特别是责任范围与责任限制的条款、法律适用和争议解决的条款。上述条款将以粗体标识，您应重点阅读</strong>。</li>
			<br>
			<li>【签约动作】当您按照注册页面提示填写信息、阅读并同意本协议且完成全部注册程序后，即表示您已充分阅读、理解并接受本协议的全部内容。
			<strong>阅读本协议的过程中，如果您不同意本协议或其中任何条款的约定，您应立即停止注册程序</strong>。</li>
		</ul>
		<strong style="font-size:20px;margin-left:40px">一、定义 </strong>
		<ul>
			<li>1.1 美团众包平台：以下简称“众包平台”，是指为商户提供劳务需求信息展示，并通过该平台展示劳务需求信息、单笔劳务费用、及服务完成确认等信息的手机APP信息平台。
			众包员可通过众包平台自主选择接收任务事项，并在事项完成后获得众包平台显示的劳务费及相应奖励（如有）。</li>
			<li>1.2 商户：是指通过众包平台完成全部注册程序后，通过众包平台发布任务事项，并支付报酬的经营者。</li>
			<li>1.3 众包员：即本协议中的“您”，是指接受并同意本协议、劳务协议全部条款及众包平台发布的其他全部服务条款和操作规则（以下简称“各类规则”），
			申请注册并经众包平台审核通过后，通过众包平台自主选择、完成任务事项，并在事项完成后获得相应报酬的完全民事行为能力人。</li>
			<li>1.4劳务需求信息：有劳务需求的商户通过美团众包平台发布的，需提供劳务的外卖产品信息、配送地址、劳务报酬等信息。</li>
		</ul>
		<strong style="font-size:20px;margin-left:40px">二 、服务条款的确认和接受</strong>
		<ul>
			<li>2.1 在您使用众包平台的服务之前，请您务必认真阅读全部条款内容。如您对条款有任何疑问的，应及时停止注册或使用并向众包平台咨询。
			一旦您完全同意所有服务条款并完成注册程序，或在其后<strong>使用众包平台服务的，即表示您已充分阅读、理解并接受本协议的全部内容，本协议即对您产生约束力。
			届时您不应以未阅读本协议的内容为由，主张本协议无效，或要求撤销本协议</strong>。</li>
			<li>2.2 众包平台有权根据国家法律法规的规定更新、维护交易秩序，调整服务规则，并不时地修改本协议或各类规则，修改后的协议或各类规则（以下简称“变更事项”）
			将以网站公示的方式通知您。<strong>如您不同意变更事项，您有权于变更事项确定的生效之日起停止使用众包平台服务，则变更事项对您不产生效力；
			如您在变更事项生效后仍继续使用众包平台服务的，即视为您同意已生效的变更事项，变更事项对您产生效力</strong>。</li>
			<li>2.3 众包平台保留在中华人民共和国法律允许的范围内独自决定拒绝提供服务、关闭您的账户或取消任务事项的权利。</li>
		</ul>
		<strong style="font-size:20px;margin-left:40px">三、服务简介</strong>
		<ul>
			<li>3.1 众包平台作为服务平台，是为商户、劳务公司和众包员之间提供互通信息的平台。主要体现在商户可以服务需求方身份在众包平台上发出劳务需求信息，
			平台予以展示，由您自主选择是否接收前述服务事项并代劳务公司完成委托事项。</li>
			<li>3.2 美团众包平台目前为您提供的服务是免费的，但是众包平台保留未来对相关服务收取费用的权利。</li>
			<li>3.3 众包员自行准备如下设备并承担如下费用：</li>
			<li>3.3.1上网设备，包括并不限于手机、电脑或者其他上网终端、调制解调器及其他必备的上网装置；</li>
			<li>3.3.2上网开支，包括并不限于网络接入费、上网设备租用费、手机流量费等。</li>
		</ul>
		<strong style="font-size:20px;margin-left:40px">四、注册</strong>
		<ul>
			<li>4.1 <strong>注册资格：您需年满十八周岁且不超过五十五周岁，身体健康持有健康证，并具有完全民事行为能力和相应的劳动能力。
			如果您不具备前述条件，众包平台有权注销您已注册完成的账户或停止向您提供服务。对您之前的行为，您及/或您的监护人应依照法律规定承担相应后果</strong>。</li>
			<li>4.2 您应按照众包平台要求提供注册资料（包括但不限于身份证、健康证，并上传提供委托服务的地理位置），并保证前述资料真实、准确、完整、合法、有效。
			如您的注册资料有所变动，您应及时更新注册资料。 否则，您应承担相应责任及后果。</li>
			<li>4.3 您注册成功后，应谨慎合理的保存、使用您的用户名和密码，并对通过您的账户实施的所有行为、活动及事件负全责。您若发现账号存在安全漏洞等情况，
			应立即以有效方式通知众包平台，要求众包平台暂停相关服务，必要时向公安机关报案。您理解众包平台对您的请求采取行动需要合理时间，
			并需要您提供有效资料证明您为账户的所有者。众包平台对在采取行动前已经产生的后果（包括但不限于您的任何损失）不承担任何责任。 </li>
			<li>4.4 您不得将在众包平台注册获得的账户借（租）给他人使用，否则对此产生的一切损失您应与实际使用人承担连带责任。</li>
			<li>4.5 您同意，众包平台拥有通过向您注册时预留的地址、邮箱、电话等任一方式发送消息，或者通过众包平台向您发送站内信息，即视为众包平台向您履行了送达、通知义务。</li>
			<li>4.6 <strong>您同意，众包平台有权使用您的注册信息、用户名、密码等信息，登陆进入您的注册账户，进行证据保全，具体方式包括但不限于公证、见证等</strong>。</li>
			<li>4.7 您知悉并认可：众包平台可能会与第三方合作而向您提供相关的网络服务，在此情况下，如该第三方同意承担与众包平台同等的保护您隐私的责任，则众包平台有权将您的注册资料等提供给该第三方。
			另外，在不透露您隐私资料的前提下，众包平台有权对与您账户相关的整个数据库进行分析并对相关的数据库进行商业上的利用。</li>
			<li>4.8您了解并同意，众包平台有权应政府部门（包括司法及行政部门）的要求，向其提供您在众包平台填写的注册信息和发布纪录等必要信息。</li>
		</ul>
		<strong style="font-size:20px;margin-left:40px">五、责任范围与责任限制</strong>
		<ul>
			<li>5.1<strong>您个人明确同意对网络服务的使用承担风险。众包平台对此不作任何类型的明示或默示担保</strong>。包括但不限于：不担保服务一定能满足您的要求，
			也不担保服务不会被中断，对服务的及时性、安全性、不出错都不作担保；对在众包平台上的任何服务或交易进程，不作担保；对众包平台服务所涉及的技术及信息的有效性、
			准确性、正确性、可靠性、稳定性、完整性和及时性不作出任何承诺和保证；对众包平台服务的适用性、没有错误或疏漏、持续性、准确性、可靠性
			或适用于某一特定用途不作担保。</li>
			<li>5.2 <strong>您理解并接受：众包平台作为信息发布服务平台，无法控制每一任务事项所涉及的物品的质量、安全或合法性，
			任务事项内容的真实性或准确性，以及任务事项所涉各方履行任务事项的能力</strong>。</li>
			<li>5.3 <strong>您了解并同意，众包平台不对因下述情况而导致的任何损害赔偿承担责任，包括但不限于利润、商誉、使用、数据等方面的损失或其它无形损失的损害赔偿:</strong> </li>
			<li>5.3.1 使用或未能使用众包平台的服务；</li>
			<li>5.3.2 您对众包平台服务的误解；</li>
			<li>5.3.3 任何非因众包平台的原因而引起的与众包平台服务有关的其它损失。</li>
			<li>5.4 <strong>如因不可抗力或其他众包平台无法控制的原因使众包平台系统崩溃或无法正常使用导致网上交易无法完成或丢失有关的信息、记录等，众包平台不承担责任</strong>。</li>
			<li>5.5 您同意在发现众包平台任何内容不符合法律规定，或不符合本协议约定的，您有义务及时通知众包平台。如果您发现个人信息被盗用或者其他权利被侵害，
			请将此情况反馈众包平台并按照众包平台的要求提供相应的证明材料。</li>
			<li>5.6 <strong>您应当严格遵守本协议及平台的各类规则，因您违反本协议或各类规则的行为给第三方、众包平台造成损失的，您应当承担相应责任</strong>。</li>
		</ul>
		<strong style="font-size:20px;margin-left:40px">六、对您信息的管理及限制</strong>
		<ul>
			<li>6.1 您账户被注销后，众包平台没有义务为您保留或向您披露账户中的任何信息，也没有义务向您或第三方转发任何您未曾阅读或发送过的信息。</li>
			<li>6.2 您同意，与众包平台的协议关系终止后，众包平台仍享有下列权利：</li>
			<li>6.2.1 继续保存您未及时删除的注册信息及使用众包平台的服务期间发布的所有信息至法律规定的记录保存期满。</li>
			<li>6.2.2 您在使用众包平台的服务期间存在违法行为或违反本协议和/或各类规则的行为的，众包平台仍可依据本协议向您主张权利。</li>
		</ul>
		<strong style="font-size:20px;margin-left:40px">七、网络服务内容的所有权</strong>
		<ul>
			<li>7.1 众包平台各项电子服务的所有权和运作权归北京三快科技有限公司所有。</li>
			<li>7.2 众包平台在与您合作中提供的平台服务内容包括文字、软件、声音、图片、录像、图表、广告等的全部内容的知识产权均归众包平台所有，
			您在使用众包平台过程中所产生内容的知识产权均归众包平台所有。</li>
			<li>7.3 除另有特别声明外，众包平台所依托软件的著作权、专利权及其它知识产权均归众包平台或众包平台关联公司所有。</li>
			<li>7.4 众包平台在提供平台服务过程中所使用的“美团众包”“美团众包平台”“美团众包员”等商业标识，其著作权或商标权归众包平台或众包平台关联公司所有。</li>
		</ul>
		<strong style="font-size:20px;margin-left:40px">八、平台服务使用规范</strong>
		<ul>
			<li><strong>8.1 在使用众包平台服务的过程中，您承诺遵守以下约定：<strong></li>
			<li>8.1.1 在使用众包平台服务的过程中实施的所有行为均遵守国家法律、法规、规范性文件的规定及本协议、各类规则的约定和要求，不违背社会公共利益或公共道德，
			不损害他人的合法权益，不违反本协议及各类规则。您如果违反前述承诺，产生任何法律后果的，您应以自己的名义独立承担所有的法律责任，并确保众包平台免于因此产生任何损失。</li>
			<li>8.1.2 不发布国家禁止发布的任务事项信息（除非取得国家相关部门合法且足够的许可），不发布涉嫌侵犯他人知识产权或其它合法权益的信息，
			不发布违背社会公共利益或公共道德、公序良俗的信息，不发布其它涉嫌违法或违反本协议及各类规则的信息。</li>
			<li>8.1.3 不使用任何装置、软件或例行程序干预或试图干预众包平台的正常运作或正在众包平台上进行的任何活动。
			您不得采取任何将导致不合理的庞大数据负载加诸众包平台网络设备的行动。</li>
			<li>8.1.4 不发表、传送、传播、储存侵害他人知识产权、商业秘密权等合法权利的内容或包含病毒、木马、定时炸弹等
			可能对众包平台系统造成伤害或影响其稳定性的内容。 </li>
			<li>8.1.5 不进行危害计算机网络安全的行为，包括但不限于：使用未经许可的数据或进入未经许可的服务器帐号；
			不得未经允许进入公众计算机网络或者他人计算机系统并删除、修改、增加存储信息；不得未经许可，企图探查、扫描、测试众包平台系统
			或网络的弱点或其它实施破坏网络安全的行为；不得企图干涉、破坏众包平台系统或网站的正常运行。</li>
			<li>8.1.6 不进行以获取不正当利益为目的的任何形式的虚假配送，包括但不限于：
			自行或协同他人伪造订单，或在已知无配送需求情况下通过平台进行虚假配送套利；
			发现商家或其他众包配送员进行虚假众包配送，不及时向众包平台反馈或通过任何形式参与其违规行为；
			其它通过众包平台获取配送任务却未发生真实配送的行为。</li>
			<li>8.1.7 其它违反法律法规或者众包平台各类规则的行为。</li>
			<li>8.2 您了解并同意：</li>
			<li>8.2.1 违反上述承诺时，众包平台有权依据本协议的约定，做出相应处理或终止向您提供服务，且无须征得您的同意或提前通知予您。 </li>
			<li>8.2.2 根据相关法令的指定或者各类规则的判断，您的行为涉嫌违反法律法规的规定或违反本协议和/或各类规则的条款的，
			众包平台有权采取相应措施，包括但不限于直接屏蔽、删除侵权信息、降低信用值或直接停止提供服务，因此给众包平台造成损失的您应承担赔偿责任。</li>
			<li>8.2.3 对于您在众包平台上实施的行为，包括未在众包平台上实施但已经对众包平台产生影响的行为，
			众包平台有权单方认定该行为的性质及是否构成对本协议和/或各类规则的违反，并据此采取相应的必要的处理措施。</li>
			<li>8.2.4 对于您涉嫌违反承诺的行为对任意第三方造成损害的，您均应当以自己的名义独立承担相应法律责任，并应确保众包平台免于承担因此产生的损失或增加的费用。</li>
			<li>8.2.5 如您涉嫌违反有关法律规定或者本协议之约定，使众包平台受到任何行政管理部门的处罚，或遭受到任何第三方的索赔请求，使众包平台遭受任何损失，
			您应当赔偿众包平台因此造成的损失及发生的费用，包括合理的律师费用。</li>
		</ul>
		<strong style="font-size:20px;margin-left:40px">九、法律适用和争议解决</strong>
		<ul>
			<li>9.1 本协议的订立、执行、解释及争议的解决均应适用中华人民共和国法律。</li>
			<li><strong>9.2 如双方就本协议内容或其执行发生任何争议，双方应尽力友好协商解决；协商不成时，任何一方均可向被告所在地有管辖权的人民法院提起诉讼</strong>。</li>
		</ul>
	</body>
	<#include "../common-footer.ftl" />
</html>
