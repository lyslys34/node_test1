<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/>
		<meta name="format-detection" content="telephone=no">
		<title>奖励明细</title>


		<link rel="stylesheet" href="/static/css/h5/h5_main.css" />
		<style>
			body {
				color: #bfbfbf;
				font-weight: 300;
			}

			.ps-split-box {
				padding: .15rem !important;
			}

			.vertical-item .title {
				font-size: 0.3rem !important;
				color: #f0b44a;
			    margin-bottom: .07rem !important;
			}

			.vertical-item .desc {
				font-size: .115rem;
			}

			.ps_list_item_end {
				display: -webkit-box;
				font-size: .135rem !important;
				padding-left: .12rem !important;
				color: #959595 !important;
			}

			.ps_list .ps_list_item:before,
			.ps_list .ps_list_item_end:before {
    			left: .12rem !important;
			}

			.ps-list-item-left {
				display: -webkit-box;
			    -webkit-box-orient: vertical;
			    -webkit-box-flex: 4;
			    width: 70%;
			}

			.ps-list-item-left .line {
				padding: .028rem 0;
			}

			.ps-list-item-left label {
				margin-right: .02rem;
				font-weight: 300;
			}

			.ps-list-item-right {
				display: -webkit-box;
				-webkit-box-flex: 1;
				-webkit-box-align: center;
			}

			.phone {
				color: #313131;
			}

			.status-granted .ps-list-item-right {
				color: #313131;
			}

			.status-granted .status-desc {
				display: none;
			}

			.status-desc {
				margin-top: .045rem;
			}

			.status-desc label {
				background: #e1f9f7;
				border: 1px solid #a0dad2;
				padding: .015rem .055rem;
				border-radius: 4px;
			}

			.status-invalid {
				background-color: #f7f9fb;
			}

			.status-invalid .status-desc,
			.status-granting .status-desc,
			.status-granting .ps-list-item-right {
				color: #313131;
			}

			.status-invalid .phone {
				color: #959595;
			}

			.status-invalid .ps-list-item-right {
				color: #959595;
			}

			.status-granted .status-desc {
				color: #313131;
			}

			.status-granted .pay-time {
				display: none !important;
			}

			.yen {
				margin-right: -.07rem;
			}

			.no-result-icon {
				width: .72rem;
				height: .72rem;
				margin: 0 auto;
				background-image: url("http://xs01.meituan.net/banma_all/imgs/icons_no_invite.png");
				background-size: contain;
				margin-top: 1.3rem;
				margin-bottom: .32rem;
			}

			.no-result-text {
				font-size: .14rem;
				color: #959595;
			}
		</style>

		<script>
			<#if code??>
		    	window.code = ${code};
		    </#if>
			<#if data??>
				window.detailInfo = '${data}';
			</#if>
		</script>

		<script type="text/javascript" src="/static/js/lib/zepto.min.js"></script>
	</head>
	<body>
		<div class="no-result" style="display: none;">
			<div class="no-result-icon">
			</div>

			<div class="no-result-text text-center">
				暂无邀请
			</div>
		</div>
		<div class="have-result">
			<section>
				<div class="ps-split-box">
					<div class="vertical-item">
						<div class="title" data-widget-id="invite-number">
							6
						</div>
						<div class="desc">
							已邀请人数
						</div>
					</div>
					<div class="vertical-item">
						<div class="title">
							 <em class="yen">&#165;</em>
							 <span data-widget-id="profit-money" class="profit-money">10</span>
						</div>
						<div class="desc">
							累积获得奖励
						</div>
					</div>
				</div>
			</section>
			<section>
				<ul class="ps_list" data-widget-id="reward-list">
				</ul>
			</section>
		</div>

	</body>
	<script type="text/template" id="listTpl">
		<li class="ps_list_item_end status-{statusClass}">
			<div class="ps-list-item-left">
				<div class="line phone">
					<label>用户手机:</label>
					<span>{phone}</span>
				</div>
				<div class="line">
					<label>邀请时间:</label>
					<span>{inviteTime}</span>
				</div>
				<div class="line">
					<label>用户首单消费时间:</label>
					<span>{firstConsumeTime}</span>
				</div>
				<div class="pay-time line" style="display: none;">
					<label>奖励发放时间:</label>
					<span>{payTime}</span>
				</div>
				<div class="status-desc line">
					<label>原因</label>
					<span>{statusDsec}</span>
				</div>
			</div>
			<div class="ps-list-item-right">
				{status}
			</div>
		</li>
	</script>
	<script type="text/javascript" src="/static/js/h5/sunflower/reward_detail.js"></script>
</html>
