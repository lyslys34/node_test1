<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/>
		<meta name="format-detection" content="telephone=no">
		<title>向日葵拉新活动详情</title>

		<link rel="stylesheet" href="/static/css/h5/h5_main.css" />
		<link rel="stylesheet" href="/static/css/h5/sunflower/sunflower_invite.css" />

		<script type="text/javascript" src="/static/js/lib/qrcode.min.js"></script>
		<script type="text/javascript" src="/static/js/lib/zepto.min.js"></script>
		<script>
			<#if code??>
		    	window.code = ${code};
		    </#if>
			<#if data??>
				window.detailInfo = '${data}';
			</#if>
		</script>
	</head>
	<body>
		<div>
			<img data-widget-id="banner-img" style="width: 100%;" src="http://xs01.meituan.net/banma_all/imgs/sunflower_banner.png" />
		</div>
		<section class="text-center mt20">
			<div class="activity-header">邀请新用户注册美团外卖</div>
			<div class="activity-header">TA按规定完成首单消费后，你将获得
				<span class="red"><em class="yen">&#165;</em><span data-widget-id="profit-money" class="profit-money">10</span></span>
				奖励
			</div>
		</section>

		<section class="section-box">
			<div class="ps-section-title">
				邀请方式
			</div>
			<div class="bm pb10">
				<div class="ps-box-items sm mt10">
					1. 面对面邀请:新用户扫描下方二维码，领取红包
				</div>
				<section class="ps-carousel">
					<div class="carousel-box">
		        		<ul class="list-carousel" data-node="mapsCon">
		                	<li data-widget-id="qr-code" style="left: 0;">
		            		</li>
		                </ul>
		    		</div>
	    		</section>
	    		<div class="ps-box-items pt10 text-center exchange-code" data-widget-id="exchange-code">
	    			红包兑换码：010101010
	    		</div>
	    	</div>
	    	<div>
	    		<div class="ps-box-items sm mt10">
	    			2. 宣传单邀请:新用户扫描宣传单二维码并填写兑换码，领取红包。
	    		</div>
	    		<div class="ps-box-items sm"
	    			 style="color: #313131;">
	    			 宣传单领取地点
	    		</div>

	    		<div class="ps-box-items pt0 pr10" data-widget-id="pickup-address-list">
		    		<section class="ps-content bm pb10">
		    			<div class="left">
			    			<div class="address">
						    	地址：南湖中园二区101号楼801室(超市内)
						    </div>
						    <div class="phone">
						    	电话：010-241423412412
						    </div>
					    </div>
					    <div class="right">
					   		<a class="call-phone" href="tel:18514088813">
					   			<div class="righticon">
						    	</div>
					    	</a>
				        </div>
		            </section>
		            <section class="ps-content bm pb10">
		            	<div class="left">
			            	<div class="address">
			            		地址：南湖中园二区101号楼801室(超市内)南湖中园二区101号楼801室(超市内)南湖中园二区101号楼801室(超市内)
			            	</div>
			            	<div class="phone">
						    	电话：010-241423412412
						    </div>
					    </div>
					    <div class="right">
					    	<a class="call-phone" href="tel:18514088813">
						    	<div class="righticon">
						    	</div>
					    	</a>
					    </div>
		            </section>
	            </div>
	            <div class="show-more">
	            	<span class="show-more-text">显示更多</span>
	            	<i class="icon icon-down"></i>
	            </div>
	         </div>
        </section>


		<section class="section-box" id="activity-rule-section">
			<div class="ps-section-title">
				活动规则
			</div>
			<div class="ps-box-items sm activity-rule" data-widget-id="activity-rule">
				1. 邀请新用户扫描活动二维码，注册成为美团外卖新用户，新用户会领到大额红包<br>
				2. 用户在48小时之内通过线上付款完成首单（单均消费20元以上），且在5小时内未发生退款<br>
			</div>
		</section>
	</body>
	<script type="text/template" id="list-template">
		<section class="ps-content bm pb10 {isHide}">
	    	<div class="left">
	        	<div class="address">
	        		地址：{address}
	        	</div>
	        	<div class="phone">
			    	电话：{phone}
			    </div>
		    </div>
		    <div class="right">
		    	<a class="call-phone" href="tel:{call-phone}">
			    	<div class="righticon">
			    	</div>
		    	</a>
		    </div>
        </section>
	</script>
	<script type="text/javascript" src="/static/js/h5/sunflower/sunflower_invite.js"></script>
</html>
