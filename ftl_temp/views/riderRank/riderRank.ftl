<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/>
    <meta name="format-detection" content="telephone=no">
    <title>考核评级</title>

    <link rel="stylesheet" href="http://xs01.meituan.net/banma_all/css/h5/h5_main.css" />
    <script src="http://xs01.meituan.net/cdn/zepto/1.1.6/zepto.min.js"></script>
    <style>
      body {
        color: #bfbfbf;
        font-weight: 300;
      }

      .ps-split-box {
        padding: .15rem !important;
      }

      .vertical-item .title {
        font-size: .14rem !important;
        color: #bfbfbf;
        margin-bottom: .15rem !important;
      }

      .vertical-item .desc {
        font-size: .14rem;
        color: #666;
      }

      th{
        text-align: center;
      }

      td{
        text-align: center;
        border: .01rem solid #e6e6e6;
        color: #666;
      }

      table {
        border-collapse: collapse;
      }

      td {
        background: #fff;
        padding: .08rem;
      }

      .error {
        display: -webkit-box;
        -webkit-box-align: center;
        -webkit-box-pack: center;
        font-size: .16rem;
        margin-top: 2rem;
      }

    </style>
    <script>
      <#if code??>
        window.code = ${code};
      </#if>
      <#if data??>
        window.data = '${data}';
      </#if>
    </script>
  </head>
  <body>
  <div>
    <section>
      <div class="ps-split-box">
        <div class="vertical-item whole-score">
          <div class="title">
            03月综合得分
          </div>
          <div class="desc">
            86.6分
          </div>
        </div>
        <div class="vertical-item">
          <div class="title city-rank">
             <span>本市排名</span>
          </div>
          <div class="desc">
            前8.6%
          </div>
        </div>
         <div class="vertical-item">
          <div class="title check-result">
            <span>考核结果</span>
          </div>
          <div class="desc">
            优秀
          </div>
        </div>
      </div>
    </section>

    <section style="margin-top: .1rem;">
      <table id="checkTable" border="0" width="100%">
        <tr class="header">
          <td class="grey">指标</th>
          <td class="grey">指标值</th>
          <td class="grey">单项总分</th>
          <td class="grey">单项得分</th>
        </tr>
      
    </table>
    </section>
  </div>
  </body>
  <script type="text/template" id="list-tpl">
    <tr>
      <td>{name}</td>
      <td>{index}</td>
      <td>{fullScore}</td>
      <td>{score}</td>
    </tr>
  </script>
  <script> 
  (function() {
    var data;

    function getData() {
      if (window.code == '0') {
        // var mockStr = '{"month":"04","riderTotalScore":"23.0","rankRatio":"200%","grade":"30","indicators":[{"indicatorName":"日均单量","fullScore":19.5,"score":"4分","value":"45单"},{"indicatorName":"出勤天数","fullScore":20,"score":"34分","value":"45天"},{"indicatorName":"顾客评分","fullScore":50,"score":"654分","value":"212分"},{"indicatorName":"送货距离","fullScore":10.5,"score":"78分","value":"9公里"},{"indicatorName":"客诉单量","fullScore":0,"score":"645分","value":"324单"}]}';
        var resStr = window.data.replace(/&quot;/g, '"').replace(/\r|\n/g, '<br>');
        // var resStr = mockStr.replace(/&quot;/g, '"').replace(/\r|\n/g, '<br>');
        resStr = resStr.replace(/"{/g, '{').replace(/}"/g, '}');

        var res = JSON.parse(resStr);
        var rules = res;
        return rules;
      } else {
        // 异常处理
        document.body.innerHTML = '<div class="error">信息获取失败，请稍后重试</div>'
        return null;
      }
    }

    function renderHeader() {
      var gradeMap = {
        10: '优秀',
        20: '良好',
        30: '合格',
        40: '不合格'
      };
      var month = data.month;
      var grade = gradeMap[data.grade];
      var rankRatio = data.rankRatio;
      var score = data.riderTotalScore;

      $('.whole-score .title').html(month + '月综合得分');
      $('.whole-score .desc').html(score + '分');

      $('.city-rank').next().html(rankRatio);
      $('.check-result').next().html(grade);
    }

    function renderTable() {
      var list = data.indicators;
      var tpl = $('#list-tpl').html();
      var $table = $('#checkTable');

      // 把总分放到表格的最后一行
      list.push({
        score: data.riderTotalScore
      });

      var renderStr = $table.html();
      list.forEach(function (item, i) {
        var config = {
          name: item.indicatorName || '',
          index: item.value || '',
          fullScore: item.fullScore || '',
          score: item.score || ''
        }

        // 如果是最后一项，需要单独处理
        if (i === list.length - 1) {
          config.score = '总分：' + config.score; 
        }

        var tplStr = tpl;
        for (key in config) {
          var reg = new RegExp('{' + key + '}', 'g');
          tplStr = tplStr.replace(reg, config[key]);
        }

        renderStr += tplStr;
      });

      $table.html(renderStr);
    }

    function init() {
      data = getData();
      if (!data) {
        return;
      }

      // 适配数据
      renderHeader();
      renderTable();
    }

    init();
  })();
  </script>
  <#include "../common-footer.ftl" />
</html>
