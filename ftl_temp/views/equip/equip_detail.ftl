<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/>
		<meta name="format-detection" content="telephone=no">
		<title>装备详情</title>
		<#include  "../config.ftl">
		<link rel="stylesheet" href="${staticPath}/css/lib/h5lib/swiper.min.css" rel="stylesheet">
		<link rel="stylesheet" href="${staticPath}/css/h5/equip/equip_detail_veree2f10c.css" />
		<script type="text/javascript" src="${staticPath}/js/lib/h5lib/zepto.min.js"></script>
		<script type="text/javascript" src="${staticPath}/js/lib/h5lib/swiper.min.js"></script>
		<script type="text/javascript" src="${staticPath}/js/lib/h5lib/fastclick.js"></script>
		<script>
			<#if data??>
		    	window.serviceData = '${data}';
		    </#if>
		</script>
	</head>
	<body>
		<nav class="dev-nav">
			<a href="javascript:;" class="nav-btn">
				<span class="nav-icon"></span>
			</a>
			<div class="nav-title text-center">装备详情</div>
		</nav>
		<div class="dev-nav-hide" style="height: .44rem;opacity: 0"></div>
		<div class="swiper-container">
			<!-- <div class="swiper-button-prev"></div> -->
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="ps nopadding">
						<section class="ps-carousel nopadding">
							<div class="carousel-box">
			            		<ul class="list-carousel" data-node="mapsCon">
			                    	<li style="left: 0;">
			                    		<a href="javascript: void(0);" style="background-image:url(/static/imgs/equip_box_2.png);">     
			                    		</a>
			                		</li>                                
			                    </ul>
			        		</div>
						</section>
					</div>
					<div class="ps-box-items bm">
						<section class="ps-title">
			                <h3 class="ps-title-des">装备说明</h3>
			            </section>
			            <section class="ps-content equip-desc">
			            	<span>长：50cm</span>
			            	<span>宽：40cm</span>
			            	<span>高：40cm</span>            	
			            </section>
					</div>
					<div class="ps-box-items bm">
						<section class="ps-title">
			                <h3 class="ps-title-des">本期领取条件</h3>
			            </section>
			            <section class="ps-content batch-desc">
			            	截止至2015年12月17日         	
			            </section>
			            <!-- <section class="ps-content">
			            	累计送单量≥100单；且配送评分≥4分
			            </section> -->
					</div>
					<div class="ps-box-items" id="getInfoItem">
						<section class="ps-title">
			                <h3 class="ps-title-des">领取信息</h3>
			            </section>
			            <section id="status-desc" class="ps-content">
			            	抱歉您暂未达到领取条件，请加油送单并保持良好的服务质量哟~
			            </section>
			            
					</div>
					<div class="ps-box-items type-five">
						<section class="ps-content">
			            </section>
					</div>
					<div id="btn-ctn" class="ps-box-items" style="margin:0 .16rem;">
						<a class="ps-btn" href="javascript:void(0);">选择领取地点</a>
					</div>
				</div>
				<div class="swiper-slide">
					<section>
						<ul id="cityList" class="ps_list">
							<li class="ps_list_item">
								<a href="#" data-city-id="1" class="ps_link">北京</a>
							</li>
							<li class="ps_list_item">
								<a href="#" data-city-id="2" class="ps_link">北京</a>
							</li>
						</ul>
					</section>
				</div>
				<div class="swiper-slide">
					<section>
						<ul id="addressList" class="ps_list">
							<li class="ps_list_item_end">
								<a href="#" class="ps_link">北京</a>
							</li>
							<li class="ps_list_item_end">
								<a href="#" class="ps_link">北京</a>
							</li>
							<li class="ps_list_item_end">
								<a href="#" class="ps_link">北京</a>
							</li>
						</ul>
					</section>
				</div>
				<div class="swiper-slide">
					<section class="apply-success">
						<div class="ps-box-items bm bk-white">
							<section class="ps-title" >
				                <h3 class="ps-title-des margin">	
									<!-- &radic; &nbsp;&nbsp;&nbsp; -->
									<span class="icon-success"></span>
									<span class="vertical-middle">申请成功，快去领装备吧</span>
								</h3>
				            </section>
						</div>
						<div class="ps-box-items bm bk-white" style="margin-top: .1rem;border-top: 1px solid #e9e9e9">
				            <section class="ps-content">
				            	<p>
					            	<span>领取地址：</span>
					            	<span class="receive-addr">北京市融泽家园2号院</span>
				            	</p>
				            	<p>
					            	<span>联&nbsp;&nbsp;系&nbsp;&nbsp;人：</span>
					            	<span class="receive-man">黄艳苹</span>
				            	</p>
				            	<p>
					            	<span>联系电话：</span>
					            	<span class="receive-phone">18649718219</span>
				            	</p>
				            	<p class="receive-remark">
					            	<!-- <span style="float: left;">领取时间：</span>
					            	<div style="margin-left: 0.64rem;padding-top: 3px;line-height: 0.16rem;">每周一至周五10：00 - 18：00（法定节假日除外）</div> -->
				            	</p>
				            </section>
				            <section class="ps-content">
				            	<p style="padding-bottom: 0.02rem;">
				            		<span>注：请在<span class="receive-deadline color-bright">******日期</span>，尽快领取。逾期未领将视为自动放弃。如需装备，需重新申请。</span>
				            	</p>
				            </section>
						</div>
					</section>
				</div>
			</div>
		</div>

		<div class="modal fade" id="confirmModal">
		    <div class="modal-dialog modal-sm">
		        <div class="modal-content">
		        	<!-- <div class="modal-header" style="text-align: center;">
		        		<h4>请确认您的领取信息</h4>
		        	</div> -->
		            <div class="modal-body">
	                	<section id="dlg-address" class="ps-content" style="line-height: .2rem;">
			            	<p>
			            		<span class="color-grey">领取地址：</span>
			            		<span class="dlg-address-info"></span>
			            	</p>
			            	<p>
			            		<span class="color-grey">领取时间：</span>
			            		<span class="dlg-address-date"></span>
			            	</p>
			            	<p class="color-grey">
			            		注：确认后地址不可更改;逾期未领者，视为自动放弃。
			            	</p>
			            </section>
			            <!-- <section id="dlg-grant-tip" class="ps-content">
			            	确认后地址不可更改，请在{grant-deadline}之前到所选地点领取装备
			            </section> -->
		            </div>
		             <div class="modal-footer">
	                    <!-- <button type="button" data-dismiss="modal" class="btn btn-success btn-sm cancel">取消</button>
	                    <button type="button" class="btn btn-success btn-sm ok">确认</button> -->
	                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-success btn-sm cancel">取消</a>
	                    <a href="javascript:void(0);" class="btn btn-success btn-sm ok">确认</a>
	                </div>
		        </div>
		    </div>
		</div>
		<div class="modal-backdrop fade"></div>
		<div class="toast-container">
			<div class="toast">此处装备已经用完</div>
		</div>
		<div class="preloader-modal fade">
			<span class="preloader preloader-white"></span>
		</div>
		<#include "../common-params.ftl">
	</body>
	<script type="text/javascript" src="${staticPath}/js/h5/equip/equip_detail_vere3e0852.js"></script>
	<script type="text/javascript">
		//安卓返回键调用的函数 
		//window.pageBack();
		// 客户端提供接口，写exit退出
		// window.exit=function(){
		// 	alert('exit')
		// };
	</script>
	
</html>