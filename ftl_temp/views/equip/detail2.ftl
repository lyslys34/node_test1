<!DOCTYPE html>
<html lang="zh-CN">

<head>

<#include "../common-header.ftl" />
    <title>装备详情</title>
</head>

<body>
  <link href="/static/css/page/equip/detail.css" rel="stylesheet">
  <div class="container-fluid reward">
    <div class="row pic" style="height:250px"></div>
    <div class="row">
      <div class="col-xs-12 pt16 mt30">尺寸说明</div>
    </div>
    <div class="row pt14 bb mt20 pb30">
      <div class="col-xs-5">外径&nbsp;&nbsp;50*37*37</div>
      <div class="col-xs-5">内径&nbsp;&nbsp;48*35*35</div>
      <div class="col-xs-2"></div>
    </div>
    <div class="row mt30">
      <div class="col-xs-12 pt16">本期领取条件</div>
    </div>
    <div class="row bb mt20 pb30">
      <div class="col-xs-12 pt14">
        <p>截止至<span tyle="color:#56E7D8">2016年1月24日-2月22日期间，</span></p>
        <p class="mt14 lh24">订单量≥100单；历史综合评分≥4分；历史综合评分数量≥10个；<br><br>说明：由于本期箱子的数量有限，并非所有满足条件的骑手都能领取，领取的优先级按照订单量进行排序。更多装备马上到货，感谢您对美团众包的支持。</p>
      </div>
    </div>

    <div class="row mt30">
      <div class="col-xs-12 pt16">领取信息</div>
    </div>
    <div class="row pt14">
      <div class="col-xs-12 mt20 mb20">
        <#if applyStation?exists>
          <#if applyStation.equipApplyStatus == 1 >
          <p class="">恭喜您已达到领取条件，请于30日之内，持本人身份证到指定地点领取。逾期未领取者视为自动放弃。</p>
          <p class="mt14">地&nbsp;&nbsp;&nbsp;&nbsp;址:&nbsp;&nbsp;${applyStation.address!}</p>
          <p class="mt14">联系人:&nbsp;&nbsp;${applyStation.name!}</p>
          <p class="mt14">电&nbsp;&nbsp;&nbsp;&nbsp;话:&nbsp;&nbsp;${applyStation.phone!}</p>
          <p class="mt14">注：领取前需致电联系人，预约具体领取时间。</p>
          <#else>
            <p>${applyStation.equipApplyMsg!}</p>
          </#if>
        <#else>
          <p>抱歉您暂未达到领取条件，请加油送单并保持良好的服务质量哟~</p>
        </#if>
      </div>
    </div>
  </div>

  <#include "../common-params.ftl">
</body>

</html>
