<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/>
		<meta name="format-detection" content="telephone=no">
		<title>取消规则</title>

		<link rel="stylesheet" href="/static/css/h5/h5_main.css" />
		<style>
			.bull {
			    font-size: .25rem;
			    margin-right: .05rem;
			    height: -0.83rem;
			    display: inline-block;
			    vertical-align: bottom;
			}

			.highlight {
				color: black;
				margin-right: 0 !important;
			}
			.ps-title-des{
				font-size: .16rem;
				line-height: .24rem;
			}
			.ps-content {
				font-size: .16rem;
				line-height: .24rem;
			}
		</style>
	</head>
	<script>
	<#--
	页面显示的规则，可以翻译成如下格式：
	“initiator取消订单，取消订单的时间发生在status状态下，type(全部/大于/小于)time分钟，
	取消订单的责任人是response，惩罚人是punishment，补偿人是compesation"
	例如文案和对应的数据如下：
	"配送员接单(status=21)超过(type=1)10分钟，商家(initiator=1)由于自身原因(response=1)取消订单将支付5元/单违约款(punishment=1)给到配送员账户中作为补偿(compesation=2)。"

	var VALUES = {
		"ROLE_ALL": 0,
		"ROLE_SHOP": 1,
		"ROLE_RIDER": 2,
		"BEFORE_ORDER": 20,
		"AFTER_ORDER": 21,
		"BEFORE_FOOD": 30,
		"AFTER_FOOD": 31,
		"TYPE_ALL": 0,
		"TYPE_GT": 1,
		"TYPE_LT": 2
	}

	/*
	 * Mock data
	 * 开发调试用的js，把所有的惩罚情况列举下来
	 * =============================
	 */
 	var rules = [{}];
	// 枚举值
	var enums = {
		"initiator": [1, 2],		// (商家/骑手)
		"status": [20, 21, 30, 31],	// (接单前/接单后/取餐前/取餐后)
		"type": [0, 1, 2],			// (全部/大于/小于)
		"responsible": [0,1,2],		// (全部/商家/骑手）
		"punishment": [1, 2], 		// (商家/骑手)
		"compensation": [1, 2]		// (商家/骑手)
	};
	// 构造所有惩罚可能
	for( var key in enums ) {
		var values = enums[key];
		var addParamRules = [];
		for( var i = 0, l = values.length ; i < l ; i ++ ) {
			var toAddParamRule = (function copyArray( list ){
					var newList = [];
					for( var i = 0, l = list.length ; i < l ; i ++ ) {
						var o = list[i];
						var _o = {};
						for( var k in o ) {
							if( o.hasOwnProperty( k ) ) {
								_o[k] = o[k];
							}
						}
						newList.push( _o );
					}
					return newList;
				})(rules);
			for( var ii = 0 ; ii < toAddParamRule.length ; ii ++ ) {
				toAddParamRule[ii][key] = values [i];
			}
			addParamRules.push( toAddParamRule );
		}
		rules = [];
		for( var i = 0, l = addParamRules.length ; i < l ; i ++ ) {
			rules = rules.concat( addParamRules[i] );
		}
	}

	for( var i = 0; i < rules.length ; i ++ ) {
		rules[i]["compensationMoney"] = (Math.random()*10).toFixed(2);
		rules[i]["punishmentMoney"] = (Math.random()*10).toFixed(2);
		rules[i]["time"] = (Math.random()*10).toFixed(0);
	}

	/*
	 * =============================
	 * 完成mock data
	 **/
	var rulesStr = "[{&quot;compensation&quot;:2,&quot;compensationMoney&quot;:0,&quot;initiator&quot;:1,&quot;punishment&quot;:1,&quot;punishmentMoney&quot;:0,&quot;responsible&quot;:0,&quot;status&quot;:20,&quot;time&quot;:0,&quot;type&quot;:0},{&quot;compensation&quot;:2,&quot;compensationMoney&quot;:1,&quot;initiator&quot;:1,&quot;punishment&quot;:1,&quot;punishmentMoney&quot;:1,&quot;responsible&quot;:0,&quot;status&quot;:21,&quot;time&quot;:0,&quot;type&quot;:2},{&quot;compensation&quot;:1,&quot;compensationMoney&quot;:0,&quot;initiator&quot;:1,&quot;punishment&quot;:2,&quot;punishmentMoney&quot;:1,&quot;responsible&quot;:2,&quot;status&quot;:21,&quot;time&quot;:0,&quot;type&quot;:1},{&quot;compensation&quot;:2,&quot;compensationMoney&quot;:1,&quot;initiator&quot;:1,&quot;punishment&quot;:1,&quot;punishmentMoney&quot;:1,&quot;responsible&quot;:1,&quot;status&quot;:21,&quot;time&quot;:0,&quot;type&quot;:1},{&quot;compensation&quot;:2,&quot;compensationMoney&quot;:1,&quot;initiator&quot;:1,&quot;punishment&quot;:1,&quot;punishmentMoney&quot;:1,&quot;responsible&quot;:2,&quot;status&quot;:31,&quot;time&quot;:0,&quot;type&quot;:0},{&quot;compensation&quot;:1,&quot;compensationMoney&quot;:0,&quot;initiator&quot;:1,&quot;punishment&quot;:2,&quot;punishmentMoney&quot;:1,&quot;responsible&quot;:2,&quot;status&quot;:31,&quot;time&quot;:0,&quot;type&quot;:0},{&quot;compensation&quot;:1,&quot;compensationMoney&quot;:0,&quot;initiator&quot;:2,&quot;punishment&quot;:2,&quot;punishmentMoney&quot;:1,&quot;responsible&quot;:0,&quot;status&quot;:21,&quot;time&quot;:0,&quot;type&quot;:2},{&quot;compensation&quot;:1,&quot;compensationMoney&quot;:0,&quot;initiator&quot;:2,&quot;punishment&quot;:2,&quot;punishmentMoney&quot;:1,&quot;responsible&quot;:0,&quot;status&quot;:21,&quot;time&quot;:0,&quot;type&quot;:1}]";

	console.log( "服务器", rules );
	 /**
	  * 规则对应的数据结构
	  */
	  var RIDER_PUNISHMENT_RULE_1 = {	// 配送员取消订单扣款规则1
		  "initiator":   VALUES.ROLE_RIDER,
  		  "status": 	 VALUES.AFTER_ORDER,
  		  "type": 		 VALUES.TYPE_LT,
  //  		  "responsible": VALUES.ROLE_RIDER,
  		  "punishment":  VALUES.ROLE_RIDER
	  },
	  RIDER_PUNISHMENT_RULE_2 = {	// 配送员取消订单扣款规则2
		  "initiator":   VALUES.ROLE_RIDER,
  		  "status": 	 VALUES.AFTER_ORDER,
  		  "type": 	 	 VALUES.TYPE_GT,
  //  		  "responsible": VALUES.ROLE_RIDER,
  		  "punishment":  VALUES.ROLE_RIDER
	  },
	  SHOP_COMPENSATION_RULE_1 = {	// 商家取消订单补偿规则1
		  "initiator": 	  VALUES.ROLE_SHOP,
  		  "status": 	  VALUES.AFTER_ORDER,
  		  "type": 		  VALUES.TYPE_LT,
  // 		  "responsible":  VALUES.ROLE_ALL,
		  "punishment":   VALUES.ROLE_SHOP,
		  "compensation": VALUES.ROLE_RIDER
	  },
	  SHOP_COMPENSATION_RULE_2 = {	// 商家取消订单补偿规则2
		  "initiator": 	  VALUES.ROLE_SHOP,
  		  "status": 	  VALUES.AFTER_ORDER,
  		  "type": 		  VALUES.TYPE_GT,
  		  "responsible":  VALUES.ROLE_SHOP,
		  "punishment":   VALUES.ROLE_SHOP,
		  "compensation": VALUES.ROLE_RIDER
	  },
	  SHOP_COMPENSATION_RULE_3 = {	// 商家取消订单补偿规则3
		  "initiator": 	  VALUES.ROLE_SHOP,
  		  "status": 	  VALUES.AFTER_FOOD,
  		  "type": 		  VALUES.TYPE_ALL,
  		  "responsible":  VALUES.ROLE_SHOP,
		  "punishment":   VALUES.ROLE_SHOP,
		  "compensation": VALUES.ROLE_RIDER
	  },
	  SHOP_PUNISHMENT_RULE_1 = {	// 商家取消订单扣款规则1	CHECK
		  "initiator": 	  VALUES.ROLE_SHOP,
  		  "status": 	  VALUES.AFTER_ORDER,
  		  "type": 		  VALUES.TYPE_GT,
  		  "responsible":  VALUES.ROLE_RIDER,
		  "punishment":   VALUES.ROLE_RIDER
	  },
	  SHOP_PUNISHMENT_RULE_2 = {	// 商家取消订单扣款规则2	CHECK
	   	  "initiator": 	  VALUES.ROLE_SHOP,
  		  "status": 	  VALUES.AFTER_FOOD,
  		  "type": 		  VALUES.TYPE_ALL,
  		  "responsible":  VALUES.ROLE_RIDER,
		  "punishment":   VALUES.ROLE_RIDER
	  },
	  displayRules = [
		  RIDER_PUNISHMENT_RULE_1,
		  RIDER_PUNISHMENT_RULE_2,
		  SHOP_COMPENSATION_RULE_1,
		  SHOP_COMPENSATION_RULE_2,
		  SHOP_COMPENSATION_RULE_3,
		  SHOP_PUNISHMENT_RULE_1,
		  SHOP_PUNISHMENT_RULE_2
	  ];
	  console.log( "页面", displayRules );
	  for( var i in displayRules ) {
		  var displayRule = displayRules[i];
		  for( var j in rules ) {
			  var checkRule = rules[j];
			  var found = true;
			  for( var attr in displayRule ) {
				  if( displayRule[attr] != checkRule[attr] ) {
					  found = false;
					  break;
				  }
			  }
			  if( found ) {
				  console.log( i, j, "found" );
				  for( var attr in checkRule ) {
					  displayRule[attr] = checkRule[attr]
				  }
				  break;
			  }
		  }
		  if( !found ) {
			  console.log( i, "not found");
		  } else {
			//   rules.splice( j, 1 );
		  }
	  }
	  console.log( rules );
	  -->
		var rulesStr = "${rules!"[]"}";
		rulesStr = rulesStr.replace(/&quot;/g, "\"");
		var rules = JSON.parse(rulesStr ); //;
		var RIDER_PUNISHMENT_RULE_1 = rules[6]
		,	RIDER_PUNISHMENT_RULE_2 = rules[7]
		,	SHOP_COMPENSATION_RULE_1 = rules[1]
		,	SHOP_COMPENSATION_RULE_2 = rules[3]
		,	SHOP_COMPENSATION_RULE_3 = rules[4]
		,	SHOP_PUNISHMENT_RULE_1 = rules[2]
		,	SHOP_PUNISHMENT_RULE_2 = rules[5]
		;
	</script>
	<body>
		<div class="ps-box-items">
			<section class="ps-title">
                <h3 class="ps-title-des">
                	<span class="bull">&bull;</span>配送员取消订单扣款规则
                </h3>
            </section>
            <section id="status-desc" class="ps-content">
            	1. 配送员接单后<span class="highlight"><script>document.write(RIDER_PUNISHMENT_RULE_1.time);</script>分钟内</span>取消订单，会从配送员的账户余额中扣除<span class="highlight"><script>document.write(RIDER_PUNISHMENT_RULE_1.punishmentMoney);</script>元/单</span>违约款。<br>
            	2. 配送员接单后<span class="highlight">超过<script>document.write(RIDER_PUNISHMENT_RULE_2.time);</script>分钟</span>取消订单，会从配送员的账户余额中扣除<span class="highlight"><script>document.write(RIDER_PUNISHMENT_RULE_2.punishmentMoney);</script>元/单</span>违约款。
            </section>
		</div>
		<div class="ps-box-items">
			<section class="ps-title">
                <h3 class="ps-title-des">
                	<span class="bull">&bull;</span>商家取消订单补偿规则
                </h3>
            </section>
            <section id="status-desc" class="ps-content">
            	1. 配送员接单<span class="highlight"><script>document.write(SHOP_COMPENSATION_RULE_1.time);</script>分钟以内</span>，商家取消订单将支付<span class="highlight"><script>document.write(SHOP_COMPENSATION_RULE_1.compensationMoney);</script>元/单</span>违约款给到配送员账户中作为补偿。<br>
            	2. 配送员接单<span class="highlight">超过<script>document.write(SHOP_COMPENSATION_RULE_2.time);</script>分钟</span>，商家由于<span class="highlight">自身原因</span>取消订单将支付<span class="highlight"><script>document.write(SHOP_COMPENSATION_RULE_2.compensationMoney);</script>元/单</span>违约款给到配送员账户中作为补偿。<br>
            	3. 配送员<span class="highlight">取餐后</span>，商家由于<span class="highlight">自身原因</span>取消订单将支付<span class="highlight"><script>document.write(SHOP_COMPENSATION_RULE_3.compensationMoney);</script>元/单</span>违约款给到配送员账户中作为补偿。
            </section>
		</div>

		<div class="ps-box-items">
			<section class="ps-title">
                <h3 class="ps-title-des">
                	<span class="bull">&bull;</span>商家取消订单扣款规则
                </h3>
            </section>
            <section id="status-desc" class="ps-content">
            	1. 配送员接单<span class="highlight">超过<script>document.write(SHOP_PUNISHMENT_RULE_1.time);</script>分钟</span>，商家由于<span class="highlight">配送员原因</span>取消订单，会从配送员的账户余额中扣除<span class="highlight"><script>document.write(SHOP_PUNISHMENT_RULE_1.punishmentMoney);</script>元/单</span>违约款。<br>
            	2. 配送员<span class="highlight">取餐后</span>，商家由于<span class="highlight">配送员原因</span>取消订单，会从配送员的账户余额中扣除<span class="highlight"><script>document.write(SHOP_PUNISHMENT_RULE_2.punishmentMoney);</script>元/单</span>违约款。<br>
            </section>
		</div>

		<div class="ps-box-items">
			<section class="ps-title">
                <h3 class="ps-title-des">
                	<span class="bull">&bull;</span>申诉规则
                </h3>
            </section>
            <section id="status-desc" class="ps-content">
            	1. 如果对产生扣款有异议，可根据页面提示方式进行申诉。<br>
            	2. 请在取消次日24点前进行申诉，超过此时间将不再做处理。<br>
            	3. 首次申诉处理结果将作为最终处理结果。
            </section>
		</div>
	</body>
	<#include "../common-footer.ftl" />
</html>
