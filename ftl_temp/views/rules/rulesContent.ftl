<!DOCTYPE HTML>
<html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <style>
        span{
          color:red;
        }
        th{
          text-align:left;
        }
        td{
          text-align:left;
        }
        li{
          margin-left:-20px;
        }
        .grey{
          background-color:#D9D9D9
        }
        .bold {
          font-weight: bold;
        }

      </style>
      <title>管理细则</title>
  </head>
  <body style="padding:10px 15px 10px 15px; line-height: 22px;">
    <p style="font-size:17px"><b>一、配送操作规范要求</b></p>
    <p>1. 必须在商家处点击取货；</p>
    <p>2. 必须在顾客收货地点点击送达；</p>
    <p>3.异常情况操作规范要求：</p>
    <table border="0" cellspacing="1" cellpadding="1"  width="100%">
      <tr>
        <td class="grey">异常情况</th>
        <td class="grey">操作要求</th>
      </tr>
      <tr>
       <td>商家出餐慢（取餐时）</td>
       <td>可上报到店，且必须在商家处点击到店；</td>
      </tr>
      <tr>
        <td>联系不上顾客（送达时）</td>
        <td>1）需上报异常，且必须在顾客收货处点击上报；<br>
2）上报异常后30分钟内顾客要求二次配送，骑手必须进行配送；<br>
3）骑手未将餐品送至顾客手中前，不可点击“我已送达”按钮；<br>
4）送达超时的异常订单系统会自动生成系统申诉，2个工作日内进行重审，骑手无需再手动申诉；</td>
      </tr>
    </table>
    <p style="font-size:17px"><b>二、时间要求</b></p>
    <p style="" class="bold">1. 即时单:</p>
    <p>
      到店时间<15分钟（即接单开始到点击“我已取餐”或“上报到店”的时间）</br>
      配送时长 （即接单开始到点击“我已送达”的时间）
    </p>
     <table border="0" cellspacing="1" cellpadding="1"  width="100%">
      <tr>
        <td class="grey">配送距离</th>
        <td class="grey">时间要求</th>
      </tr>
      <tr>
       <td>3公里以内</td>
       <td><45分钟</td>
      </tr>
      <tr>
        <td>3-5公里</td>
        <td><55分钟</td>
      </tr>
      <tr>
        <td>5-10公里</td>
        <td><70分钟</td>
      </tr>
      <tr>
        <td>10公里以上</td>
        <td><90分钟</td>
      </tr>
    </table>
    <p style="" class="bold">2. 预订单: </p>
    <p style="padding-left:15px;">超时送达或提前送达时间<15分钟</p>
    <p style="font-size:17px"><b>三、奖励发放规则</b></p>
     <p style="" class="bold">1. 获得部分奖励 
    <p style="">所有订单必须按照平台规定操作，未在取货地点点击“取货”按钮、未在送货地点点击“确认送达”按钮、到店及配送时间不符合平台要求的均只获得部分奖励。不符合配送规范的订单将按照一定比例扣除您的奖励，扣款原因会在订单详情中展示，扣款规则如下：</p>
    <p>
    <table border="0" cellspacing="1" cellpadding="1"  width="100%">
      <tr>
        <td class="grey">违规行为</th>
        <td class="grey">行为说明</th>
         <td class="grey">扣款比例</th>
      </tr>
      <tr>
       <td>取餐违规</td>
       <td>
        1）未在商家处点击取餐<br/>
        2）未在商家处点击上报到店（若骑手上报到店）<br>
        3）到店超时<br/>
        其中一项未满足即扣款 
       </td>
       <td>
        30%
       </td>
      </tr>
      <tr>
         <td>送达违规</td>
       <td>
        1）未在顾客处点击送达<br/>
        2）未在顾客处点击上报异常（若骑手上报异常）<br>
        3）送达超时<br/>
        其中一项未满足即扣款 
       </td>
       <td>
        50%
       </td>
      </tr>
      <tr>
       <td>仅送达</td>
       <td>
        真实配送且取餐违规、<br/>送达违规均违规
       </td>
       <td>
        80%
       </td>
      </tr>
    </table>
    <p style="" class="bold">2. 无法获得任何奖励</p>
    <p>
      <ul>
        <li>经核实被商家或顾客投诉配送问题的无法获得奖励。</li>
        <li>因配送员问题导致顾客退餐的无法获得奖励。</li>
        <li>由于配送员个人操作失误（如忘记点取货、不小心点错送达等）导致订单审核不通过的无法获得奖励。</li>
        <li>只接同一商家的订单视为商家小时工行为，无法获得奖励。</li>
        <li>配送员自己下单自己抢单进行配送，无法获得奖励。 </li>
      </ul>
    </p>
    <p style="font-size:17px"><b>四、平台管理规则</b></p>
    <style>
      table {
        background: #948E8E;
      }
      td {
        background: #fff;
      }
    </style>
    <table border="0" cellspacing="1" cellpadding="1"  width="100%">
      <tr>
        <td class="grey">一般违规行为</th>
        <td class="grey">处罚方式</th>
      </tr>
      <tr>
        <td>使用非本人账号抢单，或抢单后非本人完成整个配送过程</td>
        <td rowspan="7">1次拉黑7天；累计2次拉黑14天；累计3次永久拉黑，<br/>涉及违规\餐品损失部分金额扣除，余额正常结算。</td>
      </tr>
      <tr>
        <td>不配合平台工作人员协调取证或提供虚假信息</td>
      </tr>
      <tr>
        <td>抢单后未取餐直接点击送达</td>
      </tr>
      <tr>
        <td>取餐后未联系顾客直接点击上报异常</td>
      </tr>
      <tr>
        <td>联系不上顾客直接点击送达</td>
      </tr>
      <tr>
        <td>不愿配送、挑单从而诱导商家/顾客取消订单</td>
      </tr>
      <tr>
        <td>每个自然周内只接同一个商户的订单</td>
      </tr>
      <tr>
        <td class="grey">严重违规行为</th>
        <td class="grey">处罚方式</th>
      </tr>
      <tr>
        <td>配送员自己下单自己抢送或诱导堂食顾客在平台下单产生虚假配送</td>
        <td>1次拉黑14天；累计2次永久拉黑，违规部分奖励扣除，<br/>余额正常结算。</th>
      </tr>
      <tr>
        <td>发现商家刷单但未举报或配合商家直接点击送达</td>
        <td>1次拉黑7天；累计2次拉黑14天；累计3次永久拉黑，<br/>违规部分奖励扣除，余额正常结算。</th>
      </tr>
      <tr>
        <td>联合或盗用商家账户发送虚假订单骗取平台补贴</td>
        <td>一经发现永久拉黑，冻结所有余额且扣除不正当获取的奖励。</th>
      </tr>
      <tr>
        <td>配送员身份信息在公安系统中有不良记录</td>
        <td>一经发现永久拉黑，余额正常提现。</th>
      </tr>
      <tr>
        <td>任何损害美团平台诚信与信誉的行为</td>
        <td>根据情况处以不同程度拉黑。</th>
      </tr>
    </table>
    <p style="font-size:17px"><b>五、服务管理规则</b></p>
    <table border="0" cellspacing="1" cellpadding="1"  width="100%">
      <tr>
        <td class="grey">违规行为</th>
        <td class="grey">处罚方式</th>
      </tr>
      <tr>
        <td>商家/顾客投诉服务态度差、沟通困难</td>
        <td rowspan="12">1次拉黑7天；累计2次拉黑14天；累计3次永久拉黑，<br/>涉及违规\餐品损失部分金额扣除，余额正常结算。</td>
      </tr>
      <tr>
        <td>商家/顾客投诉送餐工作期间个人卫生差、穿着不整洁</td>
      </tr>
      <tr>
        <td>商家/顾客投诉送餐工作期间说脏话、抽烟、随地吐痰等不文明行为</td>
      </tr>
      <tr>
        <td>顾客地址没有门禁、没有保安阻拦情况下，无理由要求客户下楼或出小区自取</td>
      </tr>
      <tr>
        <td>未与用户电话沟通，自行将餐品放在门禁处或前台等位置后离开</td>
      </tr>
      <tr>
        <td>抢单后不上门取货且没有联系商家取消配送</td>
      </tr>
      <tr>
        <td>提前点送达或针对非异常订单联系顾客要求提前点送达（实际送餐未到达）</td>
      </tr>
      <tr>
        <td>因配送员原因导致送餐超时、餐品损坏/撒漏、态度恶劣从而顾客产生退餐</td>
      </tr>
      <tr>
        <td>送餐期间私自打开餐品和偷吃餐品的行为</td>
      </tr>
      <tr>
        <td>送错餐（A用户餐品送到B用户）且未及时弥补相关损失</td>
      </tr>
      <tr>
        <td>取餐后未送达且无法联系，后期未主动赔偿商家及顾客损失</td>
      </tr>
      <tr>
        <td>上报异常后30分钟内顾客要求送餐但拒绝配送导致顾客投诉</td>
      </tr>
      <tr>
        <td>在工作时间私自向用户兜售物品等，造成商家/顾客投诉</th>
        <td rowspan="2">1次拉黑14天；累计2次永久拉黑，并按照法律范围内赔偿顾客损失，平台保留追究法律责任的权利。</th>
      </tr>
      <tr>
        <td>完成配送后私自联系客户(骚扰客户)</td>
      </tr>
      <tr>
        <td>送餐期间，因配送员原因与顾客/商家发生争吵打骂</td>
        <td rowspan="2">一经发现永久拉黑，并按照法律范围内赔偿顾客损失，平台保留追究法律责任的权利。</th>
      </tr>
      <tr>
        <td>偷盗、毁坏、抢劫商家/顾客财物</td>
      </tr>
    </table>
    <p>
      备注：
      <ol>
        <li>针对所有处罚均保留双方24小时申诉期限；</li>
        <li>所有规则的解释权为美团众包平台所有，不接受任何异议。<br/><br/></li>
      </ol>
    </p>
    <p style="font-size:17px"><b>六、骑手保险说明</b></p>
    <p>
为使劳务服务过程中的人身安全有所保障，一旦众包服务人员当日成功接单，将直接在首单收入中支付2元用于购买保险，具体保障如下：</p>
    <p>A、意外伤残：15万元人民币；<br/>
      B、意外医疗：2万元人民币，无免赔，医保范围内100%报销；<br/>
      C、误工费：100元/天，无免赔；<br/>
      D、劳务服务人员造成他人伤害：2万元人民币，无免赔，100%报销；<br/>
      E、劳务服务人员造成的他人财产损失：1万元，500元免赔或者5%免赔，取较高者；<br/>
      F、附加一次性伤残就业补助金，按照伤残等级给予补助（执行上海地方标准）。
    </p>
    <p><b>说明：</b></p>
    <ol>
      <li>
        新注册的众包服务人员接单首日，其所属劳务公司将免费为其购买当日保险。
      </li>
      <li>
        接单首日之后，众包服务人员每日首次成功接单后，需自行承担上述保险费用（2元/天）。上述费用由劳务公司从应结算给众包服务人员的服务费中代为支付；若众包服务人员当日接单不成功，则不购买。
      </li>
      <li>
        若众包服务人员在劳务服务工作过程中发生意外事故或其他人身及财物损害，应立即报警处理，并致电客服电话【4000800610】，以便劳务公司及时备案及协助处理保险相关事宜。
      </li>
    </ol>
  </body>
  <#include "../common-footer.ftl" />
</html>