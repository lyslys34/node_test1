
<!DOCTYPE html>
<html lang="zh-CN">

<head>

    <#include "../common-header.ftl" />
    <title>我的考试</title>
    <style>
			.modal {
			    display: -webkit-box;
			    position: fixed;
			    top: 0;
			    bottom: 0;
			    left: 0;
			    right: 0;
			    -webkit-box-align: center;
			    -webkit-box-pack: center;
			    -webkit-overflow-scrolling: touch;
			}

			.fade {
			    opacity: 0;
			    -webkit-transition: opacity .15s linear;
			    -o-transition: opacity .15s linear;
			    transition: opacity .15s linear;
			}

			.modal.in {
			    z-index: 1000;
			}

			.modal .modal-dialog {
			    display: -webkit-box;
			}

			.modal.fade .modal-dialog {
			    -webkit-transition: -webkit-transform .3s ease-out;
			    -o-transition: -o-transform .3s ease-out;
			    transition: transform .3s ease-out;
			    -webkit-transform: translate(0,-25%);
			    -ms-transform: translate(0,-25%);
			    -o-transform: translate(0,-25%);
			    transform: translate(0,-25%);
			}

			.modal.in .modal-dialog {
			    -webkit-transform: translate(0,0);
			    -ms-transform: translate(0,0);
			    -o-transform: translate(0,0);
			    transform: translate(0,0);
			}

			.modal-dialog {
			    width: 100%;   
			    margin: .25rem 0;
			}

			.modal-content {
			    position: relative;
			    background-color: #fff;
			    -webkit-background-clip: padding-box;
			    background-clip: padding-box;
			    border: 1px solid #999;
			    border: 1px solid rgba(0,0,0,.2);
			    /*border-radius: 6px;*/
			    outline: 0;
			    -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
			    box-shadow: 0 3px 9px rgba(0,0,0,.5);
			    width: 84%;
			    margin: 0 auto;
			}

			.modal-header {
			    min-height: .16rem;
			    padding: .1rem;
			    border-bottom: 1px solid #e5e5e5;
			}

			.modal-title {
			    margin: 0;
			    line-height: 1.42857143;
			}

			.modal-body {
			    position: relative;
			    padding: .5rem;
			    font-size: .3rem;
			    text-align: center;
			    color: #333;
			}

			.modal-footer {
			    display: -webkit-box;
			    border-top: 1px solid #e3e3e3;
			    padding: 0 !important;
			}

			.modal-footer a {
			    display: block;
			    text-align: center;
			    -webkit-box-align: center;
			    font-size: 0.32rem;
			    -webkit-box-flex: 1;
			    padding: .1rem 0 !important;
			    background-color: #fafafa;
			    color: #333;
			}

			.modal-footer a.ok {
			    /*background-color: #0079db;*/
			    color: #313131;
			    /*border-bottom-right-radius: 4px;*/
			}
			.modal-footer a:active{
			    background: #0079db;
			    border-top-left-radius: 0px;
			    border-top-right-radius: 0;
			}

			.modal-backdrop.fade {
			    filter: alpha(opacity=0);
			    opacity: 0;
			}

			.fade.in {
			    opacity: 1;
			}

			.modal-backdrop {
			    position: fixed;
			    top: 0;
			    right: 0;
			    bottom: 0;
			    left: 0;
			    background-color: #000;
			}

			.modal-backdrop.in {
			    filter: alpha(opacity=50);
			    opacity: .5;
			    z-index: 999;
			}

		
    </style>
</head>

<body>
	<link href="/static/css/page/exam/exam4Homebrew.css" rel="stylesheet">

<div class="container-fluid" style="height:100%">
	<div class="row header title">
		<div style="color:#333;">
			<h2 class="hide">【<span></span>】</h2>
			<p><i></i><span></span></p>
		</div>
	</div>
	<div id="subject-contain">
		
	</div>
	<div class="row errorTip" style="height:20px">
		<div class="col-xs-12 text-center center-block">
			<span style="color:red;line-height:20px;font-size:15px"></span>
		</div>
	</div>
	<div class="row" style="margin-bottom:20px">
		<div class="col-xs-12">
			<button id="finish" type="button" class="btn">提交试卷</button>
		</div>
	</div>

	<div class="modal fade" id="confirmModal" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
              	提交失败
            </div>
             <div class="modal-footer">
                  <a href="javascript:void(0);" class="btn btn-sm ok">确认</a>
              </div>
        </div>
    </div>
	</div>
	
    <#include "../common-params.ftl" />

	</div>

<script type="text/javascript" src="/static/js/page/exam/common.js"></script>
    <script type="text/javascript" src="/static/js/page/exam/exam4Homebrew.js"></script>
</body>

</html>

