
<!DOCTYPE html>
<html lang="zh-CN">

<head>

    <#include "../common-header.ftl" />
    <title>在线考试</title>
</head>

<body style="background-color: #F5F5F5">
	<link href="/static/css/page/exam/exam.css" rel="stylesheet">

<div class="container-fluid" style="height:100%">
	<div class="row header">
		<div class="float-contain col-xs-12 big-f">
			<div><img style="vertical-align:inherit" src="/static/imgs/icons_63.png"></div>
			<div style="color:#959595;padding-left:7px">入门学习</div>
			<div style="padding:0 15px"><img style="vertical-align:inherit;height:12px" src="/static/imgs/forward_mark.png"></div>
			<div><img style="vertical-align:inherit" src="/static/imgs/icons_46.png"></div>
			<div style="color:#06C1AE;padding-left:7px">在线考试</div>
		</div>
	</div>
	<div id="subject-contain">
		
	</div>
	<div class="row" style="height:20px">
		<div class="col-xs-12 text-center center-block">
			<span id = "errorTip" style="color:red;line-height:20px;font-size:15px"></span>
		</div>
	</div>
	<div class="row" style="margin-bottom:20px">
		<div class="col-xs-12">
			<button id="finish" type="button" class="btn green huge-f bu">提交</button>
		</div>
	</div>
	
    <#include "../common-params.ftl" />

	</div>
	<div id="showMsg" class="over center-block text-center">
		<div>加载中,请稍后..</div>
	</div>

	<script type="text/javascript" src="/static/js/page/exam/common.js"></script>
    <script type="text/javascript" src="/static/js/page/exam/exam.js"></script>
</body>

</html>

