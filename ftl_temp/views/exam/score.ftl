
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;" >

<head>

    <#include "../common-header.ftl" />
    <title>在线考试</title>
</head>

<body style="height: 100%;background-color: #F5F5F5">
	<link href="/static/css/page/exam/exam.css" rel="stylesheet">
    <script type="text/javascript" src="/static/js/page/exam/common.js"></script>
    <script type="text/javascript" src="/static/js/page/exam/score.js"></script>

<div class="container-fluid" style="height:100%;">
	<div class="row" style="background-color:#FFFFFF;padding-top:30px;padding-bottom:20px">
		<div class="text-center col-xs-12 big-f" style="color:#313131;">
			考试成绩
		</div>
	</div>
	<div class="row" style="background-color:#FFFFFF">
		<div class="col-xs-12 big-f">
			<table class="score" style="width:100%">
				<thead>
					<tr>
						<th>答对题数<img src="/static/imgs/icons_144.png"></th>
						<th>答错题数<img src="/static/imgs/icons_139.png"></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>${correctNum !''}</td>
						<td style="color:#FE4D3D">${wrongNum !''}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row" style="padding-top:40px;padding-bottom:15px;background-color:#FFFFFF;border-bottom:1px solid #C9C9C9">
		<div class="col-xs-12 mid-f text-center" style="line-height:20px;height:20px;color:#959595">
			<img style="height: 20px;padding: 0 7px 3px 0;" src="/static/imgs/remaind.png">
			<#if resultCode?exists && resultCode == 1>
				<span id="passInfo" style="color:#06C1AE">恭喜您通过在线考试，可以开始抢单啦!</span> 
			<#else>
				<span id="rejectInfo">很遗憾您未通过在线考试，请重新答题</span>
			</#if>
			</div>
	</div>

	<div class="row" style="margin-top:20px">
		<div class="col-xs-12">
			<#if resultCode?exists && resultCode == 1>
				<button id="finish" resultCode="${resultCode !''}" type="button" class="btn green huge-f bu" >完成</button>
			<#else>
				<button id="finish" resultCode="${resultCode !''}" type="button" class="btn green huge-f bu" >重新答题</button>
			</#if>
		</div>
	</div>
	

    <#include "../common-params.ftl" />

</div>
	<div class="over center-block text-center">
		<div>加载中,请稍后..</div>
	</div>
</body>

</html>

