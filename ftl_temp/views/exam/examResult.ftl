
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;" >

<head>

    <#include "../common-header.ftl" />
    <title>考试结果</title>
        <style>
      .modal {
          display: -webkit-box;
          position: fixed;
          top: 0;
          bottom: 0;
          left: 0;
          right: 0;
          -webkit-box-align: center;
          -webkit-box-pack: center;
          -webkit-overflow-scrolling: touch;
      }

      .fade {
          opacity: 0;
          -webkit-transition: opacity .15s linear;
          -o-transition: opacity .15s linear;
          transition: opacity .15s linear;
      }

      .modal.in {
          z-index: 1000;
      }

      .modal .modal-dialog {
          display: -webkit-box;
      }

      .modal.fade .modal-dialog {
          -webkit-transition: -webkit-transform .3s ease-out;
          -o-transition: -o-transform .3s ease-out;
          transition: transform .3s ease-out;
          -webkit-transform: translate(0,-25%);
          -ms-transform: translate(0,-25%);
          -o-transform: translate(0,-25%);
          transform: translate(0,-25%);
      }

      .modal.in .modal-dialog {
          -webkit-transform: translate(0,0);
          -ms-transform: translate(0,0);
          -o-transform: translate(0,0);
          transform: translate(0,0);
      }

      .modal-dialog {
          width: 100%;   
          margin: .25rem 0;
      }

      .modal-content {
          position: relative;
          background-color: #fff;
          -webkit-background-clip: padding-box;
          background-clip: padding-box;
          border: 1px solid #999;
          border: 1px solid rgba(0,0,0,.2);
          /*border-radius: 6px;*/
          outline: 0;
          -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
          box-shadow: 0 3px 9px rgba(0,0,0,.5);
          width: 84%;
          margin: 0 auto;
      }

      .modal-header {
          min-height: .16rem;
          padding: .1rem;
          border-bottom: 1px solid #e5e5e5;
      }

      .modal-title {
          margin: 0;
          line-height: 1.42857143;
      }

      .modal-body {
          position: relative;
          padding: .5rem;
          font-size: .3rem;
          text-align: center;
          color: #333;
      }

      .modal-footer {
          display: -webkit-box;
          border-top: 1px solid #e3e3e3;
          padding: 0 !important;
      }

      .modal-footer a {
          display: block;
          text-align: center;
          -webkit-box-align: center;
          font-size: 0.32rem;
          -webkit-box-flex: 1;
          padding: .1rem 0 !important;
          background-color: #fafafa;
          color: #333;
      }

      .modal-footer a.ok {
          /*background-color: #0079db;*/
          color: #313131;
          /*border-bottom-right-radius: 4px;*/
      }
      .modal-footer a:active{
          background: #0079db;
          border-top-left-radius: 0px;
          border-top-right-radius: 0;
      }

      .modal-backdrop.fade {
          filter: alpha(opacity=0);
          opacity: 0;
      }

      .fade.in {
          opacity: 1;
      }

      .modal-backdrop {
          position: fixed;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          background-color: #000;
      }

      .modal-backdrop.in {
          filter: alpha(opacity=50);
          opacity: .5;
          z-index: 999;
      }

      .exam-confirm .modal-content {
        max-width: 75%;
      }
      .exam-confirm .modal-body {
        padding: .3rem;
      }
      .exam-confirm .logo {
        width: 2.4rem;
        height: 2.1rem;
        margin-top: .1rem;
      }
      .exam-confirm .modal-text {
        margin-top: .3rem;
        font-size: .3rem;
      }
      .exam-confirm .modal-button {
        color: #000;
        background: #fff;
        font-size: .32rem;
      }
      .exam-confirm .modal-button:first-child:after {
        content: none;
      }
      .exam-confirm .modal-button.modal-button-bold {
        background: #06C1AE;
        color: #fff;
        display: -webkit-box;
        -webkit-box-align: center;
        -webkit-box-pack: center;
      }
      .exam-confirm .modal-footer {
        height: .75rem;
      }
    </style>
</head>

<body style="height: 100%;">
	<style>
		h2 {
		font-weight: normal;
		}
	</style>
	<link href="/static/css/page/exam/exam4Homebrew.css" rel="stylesheet">
    <script type="text/javascript" src="/static/js/page/exam/common.js"></script>
    <script type="text/javascript" src="/static/js/page/exam/examResult.js"></script>

    <input id="code" data-ftl-val="${code !''}" style="display: none" />
    <input id="msg" data-ftl-val="${msg !''}" style="display: none" />
    <input id="resultCode" data-ftl-val="${resultCode !''}" style="display: none" />

<div class="container-fluid" style="height:100%;">
	<div class="row title" style="">
		<div style="color:#333;">
			<#if resultCode?exists && resultCode == 1>
        <h2 style="color:#06C1AE;padding: 0 0 0 0.25rem;;height: 0.9rem;line-height: 0.9rem;">全部答对,恭喜您通过本次考试!</h2>
      <#else>
        <h2 style="color:#f47f32;padding: 0 0 0 0.25rem;;height: 0.9rem;line-height: 0.9rem;">答错<span></span>题,正确答案参考如下:</h2>
      </#if>
		</div>
	</div>
	<div id="subject-contain">

  </div>

	<div class="row">
		<div class="col-xs-12">
			<#if resultCode?exists && resultCode == 1>
			<#else>
				<button id="finish" resultCode="${resultCode !''}" type="button" class="btn" >重答错题</button>
			</#if>
		</div>
	</div>

  <div class="modal fade" id="confirmModal" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                提交失败
            </div>
             <div class="modal-footer">
                  <a href="javascript:void(0);" class="btn btn-sm ok">确认</a>
              </div>
        </div>
    </div>
  </div>

  
  <div class="modal fade exam-confirm" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-title">
                  <img class="logo" src="/static/imgs/exam_pass.png">
                </div>
                <div class="modal-text">
                  恭喜通过考试！
                </div>
            </div>
             <div class="modal-footer">
                  <a href="javascript:void(0);" class="modal-button modal-button-bold ok">确认</a>
              </div>
        </div>
    </div>
  </div>
	

    <#include "../common-params.ftl" />

</div>

</body>

</html>

