
<!DOCTYPE html>
<html lang="zh-CN">

<head>

    <#include "../common-header.ftl" />
    <title>在线考试</title>
</head>

<body style="background-color: #F5F5F5">
	<link href="/static/css/page/exam/documentDetail.css" rel="stylesheet">
	<div id="subject-contain">
	<#escape content as common.content?html>
    <script>document.write(decodeURIComponent("${content}"));</script>
  </#escape>
	</div>
</body>
<script>
window.onload = function() {
  $('img').each(function(index, img) {
    if (img.width > (window.innerWidth - 16)) {
      img.style.height = 'auto';
      img.style.maxWidth = "100%";
    }
  });
};
</script>
<#include "../common-footer.ftl" />
</html>
