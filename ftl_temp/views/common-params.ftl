<form method="post" id="params-form">
    <#if params??>
    <#list params?keys as key>
        <input type="hidden" name="${key}" value="${params[key]}" >
    </#list>
    </#if>
</form>

<form method="post" id="common-form">
    <#if common??>
        <#list common?keys as key>
            <input type="hidden" name="${key}" value="${common[key]}">
        </#list>
    </#if>
</form>

<#include "./common-footer.ftl" />
