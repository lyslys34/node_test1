
<!DOCTYPE html>
<html lang="zh-CN">

<head>

    <#include "../clean-header.ftl" />
    <title>帮助</title>

    <link href="/static/css/page/faq/type.css" rel="stylesheet">
</head>

<body >

	<div id="loading">加载中</div>

    <div id="content" class="hidden"></div>

    <input type="hidden" id="mtUserId" value="${mtUserId!'-1'}" />
    <input type="hidden" id="osType" value="${osType!'-1'}" />
    <input type="hidden" id="appType" value="${appType!'-1'}"/>

    <script type="text/javascript" src="/static/js/page/faq/type.js"></script>
</body>
<#include "../common-footer.ftl" />
</html>
