<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="full-screen" content="yes">
    <meta name="browsermode" content="application">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>商品详情</title>
    <style>
        *{margin:0px;padding:0px;}
        html {
            width: 100%;
            font-size: 20px;
            background-color: #f5f5f5; 
        }

        body {
            width: 100%;
        }

        .container,
        .content {
            width: 100%;
        }

        img {
            width: 100%;
        }

        .error {
            margin: 0 auto;
            text-align: center;
            margin-top: 7.9rem;
        }

        img.no-network {
            height: 3.55rem;
            width: 3.55rem;
        }

        .tip {
            margin-top: 1.6rem;
            font-size: .8rem;
            color: #959595;
        }

        .reload-btn {
            margin: 0 auto;
            margin-top: 1rem;
            height: 2.2rem;
            width: 7.85rem;
            font-size: .9rem;
            color: #7d7d7d;
            border: 1px solid #C9C9C9;
            background: #fff;
            border-radius: 4px;
            line-height: 2.2rem;
        }
    </style>
    <script>
      <#if code??>
        window.code = ${code};
      </#if>
      <#if data??>
        window.data = "${data}";
      </#if>
    </script>
</head>
<body>

<div class="container">
    <#if data?? && code == 0>
        <div id="content">
            
        </div>
    <#else>
        <div class="error">
            <img class="no-network" src="/static/imgs/no_network.png" />
            <div class="tip">
                网络错误，请重新加载
            </div>
            <div class="reload-btn">
                重新加载
            </div>
        </div>
    </#if>
</div>

<div id="ctn" style="display: none;">

</div>

<script type="text/javascript" src="/static/js/lib/zepto.min.js"></script>
<script>
   (function() {
        if (data && code === 0) {
            var frag = decodeURIComponent(data);
            var ctn = document.getElementById('ctn');
            ctn.innerHTML = frag;
            var html = ctn.innerText;
            document.getElementById('content').innerHTML = html;
        }
        $('.reload-btn').on('click', function() {
            location.reload();
        });
    })();   
</script>

<#include "../common-params.ftl">

</body>

</html>