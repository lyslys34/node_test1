<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>推荐有奖活动</title>
	<meta charset="utf-8" />
	<style type="text/css">
	*{margin: 0;padding: 0;}
	.title{
		font-size: 18pt;
		text-align: center;
	}
	.c1{

		font-size: 14pt;}
	.img1{
		width:80%;
        margin: 0px auto;
	}
	.img2{
		width:80%;
	    margin: 0px auto;
	}
	img{ width: 100%; }

	.c2{
		width: 100%;
		font-size: 14pt;
		word-spacing: 1px;
	    word-wrap: break-word;
        word-break: normal;
	}
	body{
		padding: 2% 2%;
	}
	.c3{
		width: 100%;
		font-size: 14pt;
		word-spacing: 1px;
	    word-wrap: break-word;
        word-break: normal;
	}
	p{
		margin: 5px 0
	}
	</style>
</head>
<body
>
<div>
 <h1 class="title">推荐有奖活动</h1>
 <p class="c1" style="line-height: 40px;">亲爱的美团众包配送员：</p>
 <p class="c2" >众包现推出“推荐有奖“活动。通过点击美团众包主页左上角［任务列表］－>［推荐有奖］。参与“推荐有奖”活动。</p>

 <div class="img1"><img src="/static/imgs/recommend_new1.png" ></div>
 <div class="img2"><img src="/static/imgs/recommend_news2.png"></div>
<p class="c3">通过1分享到其他社交软件2复制链接3直接扫码三种方式，邀请好友注册［美团众包］， 他完成一单任务后，你们可以同时获得20元奖励！快来参加，和小伙伴一起赚钱吧！</p>
</div>
</body>
<#include "../common-footer.ftl" />
</html>
