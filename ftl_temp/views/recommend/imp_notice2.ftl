<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<style type="text/css">
.title{
	font-size: 20pt;
	text-align: center;
}
.content3{
	font-size: 16pt;
}
.content1{
	text-indent: 20px;
	font-size: 14pt;
}
.content2{
	font-size: 14pt;

}
img{
	width:100%;
}
.img{
		width:80%;
	    margin: 0px auto;
}
.red{
	color: red;
}
.tail{
	font-size: 14pt;
}
	</style>
	<body>
	<p class="title"><b>自动提现操作方法</b></p>
	<p  class="content3">一.注意事项:</p>

	<p  class="content2">1.新版本增加［<span class="red">自动提现</span>］功能，每位配送员都可以随时进行提现操作啦！最低提现金额为<span class="red">100</span>元。</p>
   <p  class="content2">2.提现成功后视银行不同，款项会在<span class="red">1-3个工作日</span>到账，请耐心等待。
</p>
   <p class="content2">3.<span class="red">8月12日</span>之前没有绑定银行卡的配送员，请及时绑定<span class="red">本人开户名的银行卡</span>，美团众包会在<span class="red">本周</span>安排一次打款。</p>
  <p class="content3">二.自动提现功能的操作步骤:</p>
  <p class="content2">1.点击左侧菜单栏->账户余额</p>
  <div class="img"><img src="/static/imgs/notice1.png" /></div>
  <p class="content2">2.点击申请提现</p>
  <div class="img"><img src="/static/imgs/notice2.jpg"/></div>
  <p class="content2">3.输入提现金额->点击确认提现</p>
  <div class="img"><img src="/static/imgs/notice3.jpg"/></div>
</body>
<#include "../common-footer.ftl" />
</html>
