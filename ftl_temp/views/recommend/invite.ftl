
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;">

<head>

<#include "../common-header.ftl" />
    <title>推荐有奖</title>


<link href="/static/css/page/recommend/invite.css" rel="stylesheet">
<style>
  html {
    font-size: 100px;
    font-size: 31.25vw
  }
  
	.tt_sub2 {
    	color: #959595;
	    font-size: 11pt;
	}

	.h-box {
		display: -webkit-box;
		margin: 0 18%;
	}

	.h-box-item {
		-webkit-box-flex: 1;
	}

	.tips {
    	background-size: 100% 100%;
    	margin-right: 2px;
	}

  .money,
  .gold {
    color: #ffb437;
  }

  @media screen and (max-width: 300px) {
    .tt_inv {
      font-size: .17rem;
    }
  }
</style>

</head>

<body style="height: 100%;">

<script type="text/javascript" src="/static/js/page/recommend/invite.js"></script>

<div class="container-fluid clear_pad" style="height: 100%;">
  <div class="row bb" style="height:20%">
  	<div class="col-xs-12 invite-text text-center clear_pad tt_inv mt10p" style="padding:0 10px; font-size: .17rem;">邀请好友注册，TA完成1单<span class="gold">普通任务</span><br>各自获得<span class="money">${money!}元</span>奖励</div>
    <p class="address" style="text-align: center; color: #666; display: none;">（活动暂仅限福州、北京、厦门地区）</p>
    <p class="no-activity" style="text-align: center; color: #666; display: none;">（暂无推荐有奖活动）
  	<div style="display: none;">
    	<div class="col-xs-12 text-center clear_pad tt_inv mt10p" style="padding:0 20px">邀请好友加入美团众包</div>
    	<div class="col-xs-12 text-center tt_sub2" style="padding-top: 1%">（一期推荐有奖活动已结束）</div>
    </div>
  </div>
  <div class="row gray" style="height:80%">
  	<div class="col-xs-12 clear_pad" style="height:100%;padding-top: 4.6%">
	  <div class="row text-center" style="height:9%">
	    <div class="col-xs-1"></div>
	    <div class="col-xs-10 tt_sub">通过以下方式邀请好友</div>
	    <div class="col-xs-1"></div>
	  </div>
	  <div id="link" class="text-center" style="height:29%">
	  	<div class="bb" style="height:90%">
	  		<div class="h-box center-block">
			    <div class="h-box-item" style="padding-right:0px">
			      <div class="share android center-block" rel="5"></div>
			      <div class="tt_pic" style="width:100%;overflow:hidden;padding-top:5px">分享</div>
			    </div>
			    <div class="h-box-item" style="padding-left:0px">
			      <div class="share copy center-block" rel="4"></div>
			      <div class="tt_pic" style="width:100%;overflow:hidden;padding-top:5px">复制链接</div>
			    </div>
	  		</div>
	  	</div>
	  </div>
	  <div class="row text-center" style="height: 8.5%">
	    <div class="col-xs-12 text-center tt_sub" style="padding-top: 2%">或邀请好友<span style="color:#313131">扫一扫</span>下面的二维码</div>
	    <div class="col-xs-12 text-center tt_sub2" style="padding-top: 1%">（长按可保存二维码到手机）</div>
	  </div>
	  <div class="row" style="height: 38%">
	    <div class="col-xs-2"></div>
	    <div id="qrCodeDiv" align="center" class="col-xs-8 center-block" style="padding-top: 5px">
	      <img border="0" id="qrCode" style="width:160px;height:160px"src="/static/imgs/zb-min.png" />
	    </div>
	    <div class="col-xs-2"></div>
	  </div>
	
	  <div class="row text-center" style="padding-top: 3vh">
	    <div id="declare" class="col-xs-12 tt_dc">
	    	<span class="tips"></span>
	    	活动说明
	    </div>
	  </div>
	</div>
  </div>
</div>

<#if versionCode??>
<input type="hidden" name="versionCode" value="${versionCode}">
</#if>
<form method="post" id="shareInfo-form">
    <#if shareInfo??>
        <#list shareInfo?keys as key>
            <input type="hidden" name="${key}" value="${shareInfo[key]}">
        </#list>
    </#if>
</form>
<script>
var money = ${money!};
if (money === 0) {
  $('.invite-text').text('邀请好友加入美团众包');
  $('.no-activity').show();
  $('#declare').hide();

}
</script>

<#include "../common-params.ftl">

</body>

</html>

