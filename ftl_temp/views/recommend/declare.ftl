
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;">

<head>
  <title>活动说明</title>
  <meta charset="utf-8" />
  <meta content="telephone=no" name="format-detection" />
 	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />
 	<style>
 			*{margin:0;padding:0;-webkit-tap-highlight-color:rgba(0,0,0,0);word-wrap:break-word;word-break:break-all;}
 	 		html,body {
 	 			width:100%;
 	 			background-color: #f6efde;
 	 			font-size: 312.5%;
 	 		}
      body{-webkit-touch-callout:none;-webkit-user-select:none;-webkit-tap-highlight-color:rgba(0,0,0,0);}
 		 .notice h1{font-size: 0.4rem;text-align: center;line-height: 1.2rem;}
     .notice p{font-size: 0.3rem; line-height: 0.4rem}
     .notice {
		    padding: 0 0.6rem;
			}
		 .emp{
		 	color: red;
		 }
 	</style>
</head>

<body>

<div class="notice">
    <h1>活动说明</h1>
    <p>第一步：被邀请用户在邀请页面输入手机号（被邀请用户必须是未注册过美团众包的新用户，每个手机号仅限一次）。</p>
    <p>第二步：被邀请用户下载美团众包App，并用邀请页面输入的手机号注册美团众包账号。</p>
    <p>第三步：被邀请用户按规则完成1单<span class='emp'>普通订单</span>配送，且首个通过审核的订单必须为<span class='emp'>普通订单</span>。</p>
    <p>第四步：邀请人和被邀请人同时获得相应金额奖励。</p>

    <p style="margin-top: .3rem">其他说明</p>
    <p>1. 本期活动结束时间以平台通知时间为准。</p>
    <p>2. 最终奖励以被邀请人完成订单所在的城市当日活动金额为准发放。</p>
    <p>3. 使用非正常途径或手段获得的现金奖励无效。</p>
    <p>4. 美团众包可根据活动举办的实际情况，在法律允许的范围内，对本活动的规则进行变动或调整，相关变动或调整将公布在活动页面上。</p>
    <p class="ios" style="display: none;">5. 该活动与苹果公司无关</p>

</div>
<script>
  var u = navigator.userAgent;
  var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
  if (isIOS) {
    document.querySelector('.ios').style.display = 'block';
  }
</script>

</body>
<#include "../common-footer.ftl" />
</html>
