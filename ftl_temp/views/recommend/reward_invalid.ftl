
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8" />
    <meta content="telephone=no" name="format-detection" />
    <!-- 处理一下微信分享的降级方案 -->
    <title>
        <#if shareInfo??>
            ${shareInfo.shareTitle?default('领取美团众包新人奖励')}
        <#else>
            领取美团众包新人奖励
        </#if>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />
    <style>
        *{margin:0;padding:0;-webkit-tap-highlight-color:rgba(0,0,0,0);word-wrap:break-word;word-break:break-all;}
        ul,li{list-style:none}
        a,a:focus,a:active{outline:0;text-decoration:none}
        html,body{width:100%;background-color: #f6efde}
        body{-webkit-touch-callout:none;-webkit-user-select:none;-webkit-tap-highlight-color:rgba(0,0,0,0);
            position:relative;opacity:0}
        .appBg{width:100%;display:block}
        .desc{position: absolute; top:4.7rem; font-size: 0.3rem; width: 100%; text-align:center; color: #fff}
        .desc span{color:#facd1f;padding: 0.03rem; display: inline-block;}
        .desc p{line-height: 0.4rem}
        .desc b{font-weight: normal;}
        .form{position: absolute; top:6.3rem; text-align: center; width: 100%}
        .form p{width: 4.6rem;margin: 0 auto;}
        .form input{border: 1px solid #eaeaea; height: 0.7rem; display: block;  margin-bottom: 0.22rem; font-size: 0.26rem; padding:0 0.2rem; width: 4.2rem;border-radius: 0.05rem;}
        .form .code{position: relative; display: none;}
        .code a{position: absolute;top: 0.1rem;right: 0.3rem;font-size: 0.26rem;line-height: 0.5rem;border-left: 1px solid #ccc;padding-left: 0.3rem;color: #e24634;}
        .code button{position: absolute;display: none;top: 0.01rem;right: -0.01rem;width: 1.95rem;height: 0.71rem;border: 0;background-color: #dcdcdc;font-size: 0.2rem;color: #898989;}
        .btn{width: 4.6rem;height: 0.7rem;display: block;font-size: 0.3rem;background-color: #e24634;color: #fff;line-height: 0.7rem;border-radius: 0.05rem;}
        .memo img{width: 1.29rem; display: block; margin: 0.6rem auto;}
        .notice{padding: 0 0.6rem; display: none;}
        .notice h1{font-size: 0.4rem;text-align: center;line-height: 1rem;}
        .notice p{font-size: 0.22rem; line-height: 0.4rem}
        .notice a{font-size: 0.22rem; text-align: center; display: block; width: 2rem;height: 0.5rem;display: block;background-color: #e24634;color: #fff;line-height: 0.5rem;border-radius: 0.05rem; margin: 0.5rem auto;}
        .down{display: none;}
       .alert{ font-size: 0.28rem;height: 0.6rem;line-height: 0.6rem; width: 5.4rem; position: fixed; margin-left:0.5rem; text-align: center; background-color: #000; opacity: 0.8; color:#fff; border-radius: 0.06rem; top:5%; display: none;}
       .money{font-family: "Arial"}
       .bigmoney{position: absolute;top: 2.4rem;font-size: 1.4rem;color: #EC4F38;
    left: 0;
    font-family: Arial;
    width: 100%;
    text-align: center;}

    .desc {
        font-size: .29rem;
    }

    .offline-ctn {
        font-size: .29rem;
        color: #959595;
        text-align: center;
        margin-top: -1.3rem;
    }
    .offline-desc {
        line-height: .4rem;
    }

    .emp {
        color: red;
    }
    </style>
</head>
<body style="opacity: 1">
 <!-- 处理一下微信分享的降级方案 -->

<img src="/static/imgs/share_icon_300.png" style="display:none" />

<div class="input">
    <img src="/static/imgs/act/act_bg.png" class="appBg"/>
    <img src="/static/imgs/act/act_2.png" class="appBg"/>
    <div class="bigmoney">&#165;0</div>
    <div class="desc">
        <p>邀请你加入美团众包</p>
        <p>送您<span class="money">0</span>元现金奖励！</p>
    </div>

     <div class="offline-ctn">
        <p class="offline-desc">该链接已失效。</p>
        <p class="offline-desc">请让邀请人重新发送邀请链接，</p>
        <p class="offline-desc">即可查看当下活动。</p>
       
    </div>
</div>
<script>
      function t(){var e=window.innerWidth;document.getElementsByTagName("html")[0].style.fontSize=.9765625*e+"%",document.getElementsByTagName("body")[0].style.opacity=1}window.onresize=t,window.onorientationchange = function(){setTimeout(t,500)},setTimeout(t,100);
</script>
</body>
</html>