<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<style type="text/css">
.title{
	font-size: 20pt;
	text-align: center;
}
.content3{
	font-size: 16pt;
}
.content1{
	text-indent: 20px;
	font-size: 14pt;
}
.content2{
	font-size: 14pt;

}
.red{
	color: red;
}
.tail{
	font-size: 14pt;
}
	</style>
	<body>
	<p class="title"><b>重要通知</b></p>
	<p  class="content3">亲爱的美团配送员:</p>
	<p class="content1">美团众包提醒配您，在配送过程中，请遵守美团众包管理规定，规范配送操作：</p>
	<p  class="content2">1、抢单到取餐时间不能超过<span  class="red">15分钟</span>，否则无法获得补贴。</p>
   <p  class="content2">2、抢单后不取货且要求商家取消订单的配送员，请及时和商家做好沟通工作，双方协商解决，如果美团众包配送员被商家<span class="red">投诉</span>，美团众包平台会将该配送员<span class="red">拉黑三天</span>！
</p>
   <p class="content2">3、每个美团众包配送员<span class="red">每次</span>抢单<span class="red">上限为8单</span>，请大家仔细挑选进行配送！</p>
   <p class="content2">4、美团众包订单的审核时间为送单完成后的<span class="red">两个</span>工作日（周末顺延到下一个工作日）</p>
   <p class="content2">5、美团众包配送员配送过程中，一定要规范操作，在<span class="red">取货地点</span>点击“我已取货按钮”并且在<span class="red">送达地址</span>点击“我已送达”按钮，否则无法获得补贴。</p>
   <p class="tail">请大家按规定准时，规范配送！祝大家工作愉快！</p>
</body>
<#include "../common-footer.ftl" />
</html>
