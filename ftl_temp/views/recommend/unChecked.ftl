
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;">

<head>

<#include "../common-header.ftl" />

    <title>推荐有奖</title>
    <link href="/static/css/page/recommend/invite.css" rel="stylesheet">
	<style>
    html {
      font-size: 100px;
      font-size: 31.25vw
    }

		.tt_sub2 {
	    	color: #959595;
		    font-size: 11pt;
		}

		.h-box {
			display: -webkit-box;
			margin: 0 18%;
		}

		.h-box-item {
			-webkit-box-flex: 1;
		}

		.tips {
	    	background-size: 100% 100%;
	    	margin-right: 2px;
		}

    .money,
    .gold {
      color: #ffb437;
    }

    @media screen and (max-width: 300px) {
      .tt_inv {
        font-size: .17rem;
      }
    }

	</style>
</head>

<body style="height: 100%;">
<script type="text/javascript" src="/static/js/page/recommend/uncheck.js"></script>

<div class="container-fluid clear_pad" style="height: 100%;">
  <div class="row bb" style="height:20%">
  <div class="col-xs-12 invite-text text-center clear_pad tt_inv mt10p" style="padding:0 10px; font-size: .17rem;">邀请好友注册，TA完成1单任务后,<br>你们可同时获得<span class="money">${money!}元</span>奖励</div>
   <p class="no-activity" style="text-align: center; color: #666; display: none;">（暂无推荐有奖活动）
  <div style="display: none;">
     <div class="col-xs-12 text-center clear_pad tt_inv mt10p" style="padding:0 20px">邀请好友加入美团众包</div>
    <div class="col-xs-12 text-center tt_sub2" style="padding-top: 1%">（一期推荐有奖活动已结束）</div>
    </div>
  </div>
  <div class="row gray" style="height:80%">
  	<div class="col-xs-12 clear_pad gray" style="height:100%;padding-top:4%">
	  <div class="row center-block" style="height:25%;margin-top:4%">
	    <div class="col-xs-4"></div>
	    <div id="up_con" class="col-xs-4">
	    	<img border="0" id="u_info" align="center" src="/static/imgs/uncheck.png" />
	    </div>
	    <div class="col-xs-4"></div>
	  </div>
		
	  <div class="row text-center" style="height:65%">
		<div class="col-xs-1"></div>
		<div class="col-xs-10">
			<p class="tt_sub">您尚未通过身份验证</p>
			<p class="tt_pic">通过身份验证后即可使用此功能</p> 
		</div>
		<div class="col-xs-1"></div>
	  </div>
	  
      <div class="row text-center" style="display: none;">
	    <div id="declareCon" class="col-xs-12 tt_dc">
	    	<span class="tips"></span>
	    	活动说明
	    </div>
	  </div>
	</div>
  </div>
</div>
<script>
  var money = ${money!};
  if (money === 0) {
    $('.invite-text').text('邀请好友加入美团众包');
    $('.no-activity').show();
    $('#declare').hide();

  }
</script>

<#include "../common-params.ftl">

</body>

</html>

