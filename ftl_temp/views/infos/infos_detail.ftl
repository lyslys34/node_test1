<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>帮助详情</title>
</head>

<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        <#if questionAndDetailAnswer??>
            <h3>${questionAndDetailAnswer.title!''}</h3>
            <div id="content"></div>
        <#else>
            暂无数据
        </#if>
        </div>
    </div>

</div>

<script>
    var umEditorHTML = "";
    <#if questionAndDetailAnswer?exists >
    umEditorHTML = "${questionAndDetailAnswer.content!''}";
    </#if>
    if (umEditorHTML.length) {
        document.getElementById("content").innerHTML = decodeURIComponent(umEditorHTML);
    }
</script>


</body>

</html>