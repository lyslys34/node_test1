
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%;">

<head>

<#include "../common-header.ftl" />
    <title>我的统计</title>
</head>

<body style="height: 100%;">

<link href="/static/css/page/statistics/delivery_capability.css" rel="stylesheet" type="text/css"/>


<div class="statistics-container" style="font-size: 12pt; color: #656565; background-color: #fffdfe; height: 100%;" >

    <table class="statistics-table" style="height: 55%;">
        <tr class="statistics-row">
            <td class="statistics-cell">
                <span id="table-total-count" class="span-normal span-large-font">0</span>
                <br/>
                <span id="table-count-category">昨日单量</span>
            </td>
            <td>
                <div class="line-vertical"></div>
            </td>
            <td class="statistics-cell">
                <span id="table-rank" class="span-normal span-large-font">-</span>
                <br/>
                站内排名
            </td>
        </tr>
        <tr class="statistics-row">
            <td class="statistics-cell">
                <span id="table-normal-count" class="span-normal span-large-font">0</span>
                <br/>
                普通单
            </td>
            <td >
                <div class="line-vertical"></div>
            </td>
            <td class="statistics-cell">
                <span id="table-normal-delay" class="span-normal span-large-font">0</span>
                <br/>
                大于60分钟
            </td>
        </tr>
        <tr class="statistics-row">
            <td class="statistics-cell">
                <span id="table-booked-count" class="span-normal span-large-font">0</span>
                <br/>
                预订单
            </td>
            <td >
                <div class="line-vertical"></div>
            </td>
            <td class="statistics-cell">
                <span id="table-booked-delay" class="span-normal span-large-font">0</span>
                <br/>
                超时15分钟以上
            </td>
        </tr>
        <tr class="statistics-row border-top-thick" >
            <td class="statistics-cell">
                <span id="table-feedback-score" class="span-normal span-large-font">0</span>
                <br/>
                <span id="table-feedback-count">配送评分(0份)</span>
            </td>
            <td >
                <div class="line-vertical"></div>
            </td>
            <td class="statistics-cell">
                <span id="table-feedback-5star" class="span-normal span-large-font">0%</span>
                <br/>
                五星好评率
            </td>
        </tr>
    </table>

    <div class="line-horizontal"></div>
    <div class="rating-bar" style="height: 8%">
        <div class="rating-bar-item" id="rating-1">
            好评(0)
        </div>
        <div class="rating-bar-item" id="rating-2">
            中评(0)
        </div>
        <div class="rating-bar-item" id="rating-3">
            差评(0)
        </div>
        <div style="clear: both"></div>
    </div>
    <div class="line-horizontal"></div>


    <div id="iscroll-wrapper">
        <div id="scroller">
            <table id="feedback-detail-table" style="color: GrayText; width: 100%">
            </table>
            <div id="load-indicator"></div>
            <div style="height: 2.4em"></div>
        </div>
    </div>



    <table id="bottom-table" style="width: 100%; font-size: 1.2em; background-color: #06c1ae; color: #ffffff; position: fixed; bottom: 0; height: 2.4em" >
        <tr>
            <td class="tabbar-item" id="index-0" >
                昨日
            </td>
            <td class="tabbar-item" id="index-1" >
                本月
            </td>
            <td class="tabbar-item" id="index-2" >
                上月
            </td>
        </tr>
    </table>

    <div id="loading-shade" style="height: 100%; width: 100%; position: fixed; top: 0; background-color: #666666; text-align: center; opacity: 0.8;" >
        <div id="load-failed-msg" style="height: 10%; width: 100%; top: 75%; position: absolute; text-align: center; color: #000000" hidden="hidden">加载失败，点击屏幕重试</div>
        <img id="loading-img" src="/static/imgs/loading.gif" style="height: 5%; top: 47.5%; left: 47.5%; position: absolute">

    </div>

</div>

<script type="text/javascript" src="/static/js/lib/iscroll-probe.js"></script>
<script type="text/javascript" src="/static/js/page/statistics/delivery_capability.js"></script>
<#include "../common-params.ftl">

</body>

</html>

