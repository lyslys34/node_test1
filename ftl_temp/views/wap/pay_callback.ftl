<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/>
    <meta name="format-detection" content="telephone=no">
    <title>支付成功跳转</title>
    <style>
      html {
        font-size: 100px;
        font-size: 31.25vw;
        width: 100%;
        height: 100%;
      }
      body {
        color: #bfbfbf;
        font-weight: 300;
        display: -webkit-box;
        -webkit-box-pack: center;
        height: 100%;
      }

      .error {
        display: -webkit-box;
        -webkit-box-align: center;
        -webkit-box-pack: center;
        font-size: .16rem;
      }

    </style>
    <script>
      <#if code??>
        window.code = ${code};
      </#if>
      <#if data??>
        window.data = '${data}';
      </#if>
    </script>
  </head>
  <body>
    <div class="error">跳转中...</div>
  </body>
  <script> 
    window.addEventListener('load', function() {
      var u = navigator.userAgent;
      var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
      var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);

      if (isAndroid) {
        window.jsInterface.payResult();
      } else if (isIOS) {
        setTimeout(function() {
          window.payResult();
        }, 1000);
      }
  });
  </script>
  <#include "../common-footer.ftl" />
</html>
