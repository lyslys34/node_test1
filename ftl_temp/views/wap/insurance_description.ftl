<!DOCTYPE HTML>
<html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>骑手保险说明</title>
      <style>

      html {
        -webkit-text-size-adjust: none;
        -ms-text-size-adjust: none
    }

      html {
        font-size: 100px;
        font-size: 31.25vw
    }

    ul,ol,li,pre,form,fieldset,legend,button,input,textarea,select,th,td {
        margin: 0;
        padding: 0;
        -webkit-tap-highlight-color: rgba(0,0,0,0)
    }

    ul {
      margin: .15rem 0;
    }

      li{
        font-size:.158rem;
        list-style-type:none;
        line-height: 0.24rem;
      }

      .section-title {
        font-size:.16rem;
        font-weight: bold;
        margin: .16rem 0;
      }

      .title {
        font-size: .18rem;
        font-weight: bold;
        margin-bottom: .12rem;
      }

      a {
        color: #00abe4;
        text-decoration: initial;
      }
      </style>
  </head>
  <body style="padding:.05rem .15rem;">
    <ul>
      <li style="text-align: center;">
        <div class="title">《骑手保险说明》</div>
      </li>
      <li>为使劳务服务过程中的人身安全有所保障，一旦众包服务人员当日成功接单，将直接在首单收入中支付2元用于购买保险，具体保障如下：</li>
    </ul>

    <ul>
      <li>A、意外伤残：15万元人民币；</li>
      <li>B、意外医疗：2万元人民币，无免赔，医保范围内100%报销；</li>
      <li>C、误工费：100元/天，无免赔；</li>
      <li>D、劳务服务人员造成他人伤害：2万元人民币，无免赔，100%报销；</li>
      <li>E、劳务服务人员造成的他人财产损失：1万元，500元免赔或者5%免赔，取较高者；</li>
      <li>F、附加一次性伤残就业补助金，按照伤残等级给予补助（执行上海地方标准）。</li>
    </ul>

    <ul>
      <div class="section-title">说明：</div>
      <li>1. 新注册的众包服务人员接单首日，其所属劳务公司将免费为其购买当日保险。</li>
      <li>2. 接单首日之后，众包服务人员每日首次成功接单后，需自行承担上述保险费用（2元/天）。上述费用由劳务公司从应结算给众包服务人员的服务费中代为支付；若众包服务人员当日接单不成功，则不购买。</li>
      <li>3. 若众包服务人员在劳务服务工作过程中发生意外事故或其他人身及财物损害，应立即报警处理，并致电客服电话【4000800610】，以便劳务公司及时备案及协助处理保险相关事宜。</li>
      <li>
        4. 美团众包仅提供信息服务，具体保险购买及理赔等相关事宜，请众包服务人员自行与所属劳务公司确认电话【王女士 13811448430，陈先生 13811712031，孙先生 13810509203】。
      </li>

    </ul>
  </body>
  <#include "../common-footer.ftl" />
</html>
