<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="full-screen" content="yes">
    <meta name="browsermode" content="application">
    <meta charset="utf-8">
    <title>更多</title>
    <style>

      html {
        font-size: 20px;
      }

      body {
          margin: 0;
      }

      body {
          position: absolute;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          font-family: "Helvetica Neue",Helvetica,sans-serif;
          font-size: .85rem;
          line-height: 1.5;
          color: #3d4145;
          background: #eee;
          overflow: hidden;
      }

      .error {
        margin-top: 12.5rem;
        text-align: center;
        height: 100%;
        font-size: 0.8rem;
        color: #666;
      }

      .main {
          background-color: #efefef;
          box-sizing: border-box;
          position: absolute;
          left: 0;
          top: 0;
          width: 100%;
          height: 100%;
          background: #efeff4;
          display: block;
          overflow-y: auto;
      }

      a, button, input, select, textarea {
          outline: 0;
      }

      a {
          color: #0894ec;
          text-decoration: none;
          -webkit-tap-highlight-color: transparent;
      }

      #menu {
          overflow: hidden;
          background: #fff;
      }

      #menu .menu-i {
          position: relative;
          width: 50%;
          float: left;
          height: 5.85rem;
          color: #333;
          font-size: .7rem;
          margin: 0;
          -webkit-box-sizing: border-box;
          box-sizing: border-box;
      }

      #menu .menu-i img {
          display: block;
          margin: 0.9rem auto 0;
          width: 2.65rem;
          height: 2.65rem;
      }

      #menu .menu-i div.pic-holder {
          display: block;
          margin: 0.9rem auto 0;
          width: 2.65rem;
          height: 2.65rem;
          border: 1px solid #c0c0c0;
          background-color: #f1f1f1;
          border-radius: 4px;
      }

      #menu .menu-i span {
          font-size: .8rem;
          display: block;
          width: 100%;
          text-align: center;
          margin-top: .75rem;
          margin-bottom: .75rem;
          height: .8rem;
          line-height: .8rem;
          color: #666;
          font-weight: 300;
      }

      .menu-line {
          position: absolute;
          bottom: 0;
          left: 0;
          width: 100%;
          height: 1px;
          padding-left: 0rem;
      }

      .menu-line:after {
          content: ' ';
          display: block;
          width: 100%;
          height: .05rem;
          background: #e0e0e0;
      }

      #menu .menu-i:nth-child(odd):after {
          content: ' ';
          display: block;
          position: absolute;
          right: 0;
          top: 0;
          height: 100%;
          border-right: 0.05rem solid #e0e0e0;
      }

    </style>
    <script>
      <#if code??>
        window.code = ${code};
      </#if>
      <#if data??>
        window.data = '${data}';
      </#if>
    </script>
  </head>
  <body>
    <div class="main">
      <div id="menu">
      </div>

    </div>
    <script type="text/javascript" src="/static/js/lib/zepto.min.js"></script>
    <script>
      (function() {
        if (window.code !== 0) {
          $('body').html('<div class="error">数据获取失败</div>');
          return;
        }

        var dataArr;
        try {
          // var str = mockStr.replace(/&quot;/g, '"');
          var str = window.data.replace(/&quot;/g, '"');
          // 处理字符串出现的换行
          str = str.replace(/\r|\n/g, '<br>');
          dataArr = JSON.parse(str).funModuleList;
          // dataObj = JSON.parse(window.dataInfo);
        } catch (e) {
          $('body').html('<div class="error">数据获取失败</div>');
          $('body').css('background-color', '#fff');
          return;
        }

        var htmlStr = '';

        dataArr.forEach(function(item) {
          var url = 'http://' + item.url;
          htmlStr += '<a class="menu-i" href=' + url + '>' + 
            '<div class="pic-holder"></div>' + 
            '<img style="display: none;" src="' + item.icon + '" alt />' +
            '<span>' + item.funName + '</span>' + 
            '<div class="menu-line"></div></a>';
        });

        if (dataArr.length % 2) {
          htmlStr += '<a class="menu-i" href="#">' + 
            '<span></span>' + 
            '<div class="menu-line"></div></a>';
        }


        $('#menu').html(htmlStr);

        // 图片加载成功后显示
        $('img').on('load', function() {
          $(this).show();
          $(this).prev().hide();
        });

      })();
     
    </script>
    <#include "../common-footer.ftl" />
  </body>
</html>
